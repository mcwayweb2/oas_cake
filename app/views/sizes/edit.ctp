<?php echo $this->element('js/jquery/add_form_styles'); ?>
<?php echo $html->script(array('jquery/alphanumeric'), false); ?>
<script type="text/javascript">
	$(function() {
		$('#SizeTitle').alpha({ allow: " -" });
		$('#SizeSize, #SizeBasePrice, #SizeStandardTopping, #SizePremiumTopping, #SizeExtraCheese, #SizeExtraSauce').numeric({ allow: "." });
		
		$('#btnMenuEditSize').click( function () {
			var success = true;
			
			if($('#SizeTitle').val() == '') {
				$('#SizeTitleText').html('Please enter a size title!');
				$('#SizeTitle').addClass('ui-state-highlight'); success = false;
			}
			
			if($('#SizeSize').val() == '') {
				$('#SizeSizeText').html('Please enter a size title!');
				$('#SizeSize').addClass('ui-state-highlight'); success = false;
			}
			 
			if($('#SizeBasePrice').val() == '') {
				$('#SizeBasePriceText').html('Please enter a base price!');
				$('#SizeBasePrice').addClass('ui-state-highlight'); success = false;
			}
			
			if($('#SizeToppingPrice').val() == '') {
				$('#SizeToppingPriceText').html('Please enter a base price!');
				$('#SizeToppingPrice').addClass('ui-state-highlight'); success = false;
			}
			 
			if($('#SizeExtraCheese').val() == '') {
				$('#SizeExtraCheeseText').html('Please enter a price for extra cheese!');
				$('#SizeExtraCheese').addClass('ui-state-highlight'); success = false;
			}
			 
			if($('#SizeExtraSauce').val() == '') {
				$('#SizeExtraSauceText').html('Please enter a price for extra sauce!');
				$('#SizeExtraSauce').addClass('ui-state-highlight'); success = false;
			}  

			if(success) { $('#formMenuEditSize').submit(); }
			return false;
		});
	});
</script>
<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Update Pizza Size</legend>
	<?php echo $form->create('Size', array('action' => 'edit', 'id' => 'formMenuEditSize')); ?>
	<?php echo $form->hidden('location_id', array('value' => $this->data['Size']['location_id'])); ?>
	<?php echo $form->input('id'); ?>
	
	<table class="form-table">
		<tr>
			<td class="label-req">Size Title: <span class="required">*</span></td>
			<td><?php echo $form->input('title', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
			<span class="base orange">(e.g. Medium, Lg, X-Large, etc.)</span>
			<br /><span class="base b red" id="SizeTitleText"></span></td>
		</tr>
		<tr>
			<td class="label-req">Size: <span class="required">*</span></td>
			<td><?php echo $form->input('size', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
			<span class="base orange">(Actual pizza size, measured in inches.)</span>
			<br /><span class="base b red" id="SizeSizeText"></span></td>
		</tr>
		<tr>
			<td class="label-req">Base Price: <span class="required">*</span></td>
			<td><?php echo $form->input('base_price', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text',
															'value'=>number_format($this->data['Size']['base_price'], 2)))?>
			<br /><span class="base b red" id="SizeBasePriceText"></span></td>
		</tr>
		<tr>
			<td class="label-req">Topping Price: <span class="required">*</span></td>
			<td><?php echo $form->input('topping_price', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text',
															   'value'=>number_format($this->data['Size']['topping_price'], 2)))?><br />
			<span class="base orange">(Base price for each topping.)</span>
			<br /><span class="base b red" id="SizeToppingPriceText"></span></td>
		</tr>
		<tr>
			<td class="label-req">Extra Cheese Price: <span class="required">*</span></td>
			<td><?php echo $form->input('extra_cheese', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text',
															  'value'=>number_format($this->data['Size']['extra_cheese'], 2)))?>
			<br /><span class="base b red" id="SizeExtraCheeseText"></span></td>
		</tr>
		<tr>
			<td class="label-req">Extra Sauce Price: <span class="required">*</span></td>
			<td><?php echo $form->input('extra_sauce', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text',
															 'value'=>number_format($this->data['Size']['extra_sauce'], 2)))?>
			<br /><span class="base b red" id="SizeExtraSauceText"></span></td>
		</tr>
		<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Pizza Size', null, null, 'btnMenuEditSize', '135'); ?></td></tr>
		</form>
	</table>
</fieldset>