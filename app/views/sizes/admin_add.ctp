<div class="sizes form">
<?php echo $form->create('Size');?>
	<fieldset>
 		<legend><?php __('Add Size');?></legend>
	<?php
		echo $form->input('title');
		echo $form->input('size');
		echo $form->input('base_price');
		echo $form->input('extra_toppings');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Sizes', true), array('action'=>'index'));?></li>
	</ul>
</div>
