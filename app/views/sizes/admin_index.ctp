<div class="sizes index">
<h2><?php __('Sizes');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('size');?></th>
	<th><?php echo $paginator->sort('base_price');?></th>
	<th><?php echo $paginator->sort('extra_toppings');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($sizes as $size):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $size['Size']['id']; ?>
		</td>
		<td>
			<?php echo $size['Size']['title']; ?>
		</td>
		<td>
			<?php echo $size['Size']['size']; ?>
		</td>
		<td>
			<?php echo $size['Size']['base_price']; ?>
		</td>
		<td>
			<?php echo $size['Size']['extra_toppings']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $size['Size']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $size['Size']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $size['Size']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $size['Size']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<?php '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Size', true), array('action'=>'add')); ?></li>
	</ul>
</div>
