<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?php echo $html->css('jquery/shortscroll'); ?>
	<?php echo $html->script(array('jquery/shortscroll', 'mousewheel'))?>
	<?=$scripts_for_layout?>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<body>
<div id="superdrop">
<div id="container">
	<div id="header" class="clearfix">
    	<div id="logo" class="left">
        	<h2><a href="/">Order A Slice</a></h2>
        </div>
        <?php if($session->check('Admin')) { ?>
    	<div id="tagline" class="right">
    		<div class="ui-helper-clearfix">
    			<div class="right"><?php echo $html->image('devil-ico.gif', array('style'=>'height:40px;', 'class'=>'pad-l10'))?></div>
    			<div class="right"><h3>All Access To All Things!</h3></div>
    			
    		</div>
        </div>
        <?php } ?>
    </div>
    <div id="nav" class="clear clearfix">
    <?php if($session->check('Admin')) { ?>
    <ul>
    	<li><?php echo $html->link('Admin Dashboard', '/admin/advertisers/dashboard'); ?></li>
    	<li><?php echo $html->link('Restaurants', '/admin/locations/index'); ?></li>
    	<li><?php echo $html->link('Leads/Prospects', '/admin/prospects/index'); ?></li>
    	<li><?php echo $html->link('Orders', '/admin/orders/index'); ?></li>
    	<li><?php echo $html->link('Billing', '/admin/advertisers/billing'); ?></li>
    	<li><?php echo $html->link('Tutorials', '/admin/tutorials/index'); ?></li>
    	<li><?php echo $html->link('Logout', '/admin/users/logout'); ?></li>
    </ul>
    <?php } ?>
    </div>
    <div id="" class="clear clearfix marg-t10" style="padding-left:80px;">
 		<?php echo $content_for_layout; ?>    
    </div>
</div>
</div>
<div id="superfooter" class="clear">
	<div id="footer" class="clearfix">
		<?php if($session->check('Admin')) {
			echo $html->link('Admin Dashboard', '/admin/advertisers/dashboard')." | ";
			echo $html->link('Manage Advertisers', '/admin/advertisers/index')." | ";
			echo $html->link('Billing Manager', '/admin/advertisers/billing')." | ";
			echo $html->link('Admin Profile', '/admin/users/edit')." | ";
			echo $html->link('Admin Logout', '/admin/users/logout');
		}
		?>
	    <p>&copy; <?=date('Y')?> Order A Slice. All rights reserved. Developed By McWay Web Development.</p>
	</div>
</div>
</body>
</html>