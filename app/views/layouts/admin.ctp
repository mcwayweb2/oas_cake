<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<script>
	$(function() { 

	});
	</script>
	<?=$scripts_for_layout?>
</head>
<body>
<div id="superdrop">
<div id="container">
	<div id="header" class="clearfix">
    	<div id="logo" class="left">
        	<h2><a href="/">Order A Slice</a></h2>
        </div>
        <?php if($session->check('Admin')) { ?>
    	<div id="tagline" class="right">
    		<div class="ui-helper-clearfix">
    			<div class="right"><?php echo $html->image('devil-ico.gif', array('style'=>'height:40px;', 'class'=>'pad-l10'))?></div>
    			<div class="right"><h3>All Access To All Things!</h3></div>
    			
    		</div>
        </div>
        <?php } ?>
    </div>
    <div id="nav" class="clear clearfix">
    <?php if($session->check('Admin')) { ?>
    <ul>
    	<li><?php echo $html->link('Admin Dashboard', '/admin/advertisers/dashboard'); ?></li>
    	<li><?php echo $html->link('Restaurants', '/admin/locations/index'); ?></li>
    	<li><?php echo $html->link('Leads/Prospects', '/admin/prospects/index'); ?></li>
    	<li><?php echo $html->link('Orders', '/admin/orders/index'); ?></li>
    	<li><?php echo $html->link('Billing', '/admin/advertisers/billing'); ?></li>
    	<li><?php echo $html->link('Tutorials', '/admin/tutorials/index'); ?></li>
    	<li><?php echo $html->link('Logout', '/admin/users/logout'); ?></li>
    </ul>
    <?php } ?>
    </div>
    <div id="wrapper" class="clear clearfix">
    	<?php if($session->check('Admin')) { ?>
    	<div id="sidebar" class="left">
        	<div id="side-features">
				<h2 class="icon-feature">Admin Account</h2>
				<div class="orange-border">
            	<div class="ui-widget ui-widget-content ui-corner-all">
            		<div class="ui-helper-clearfix">
            			<div class="left b">
            				<span class="orange">Logged In As Admin:</span><br />
            				<span class="italic"><?php echo $session->read('Admin.fname')." ".$session->read('Admin.lname'); ?></span>
            			</div>
            			<div class="right">
							<?php echo $this->element('btn-toolbar', array('btns' => array(array('/admin/users/edit', '22-settings-gear.png', 'Edit Admin Profile'),
																						   array('/admin/users/logout', '22-exit-logout.png', 'Admin Logout')))); ?>
						</div>
            		</div>
            		
            		<?php if($session->check('Advertiser')) { ?>
            		<hr />
            		<div class="ui-helper-clearfix">
            			<div class="left b">
            				<span class="orange">Logged In As Advertiser:</span><br />
            				<span class="italic"><?php echo $session->read('Advertiser.name'); ?></span>
            			</div>
            			<div class="right">
							<?php echo $this->element('btn-toolbar', array('btns' => array(array('/advertisers/dashboard', '22-desktop.png', 'Restaurant Dashboard'),
																						   array('/admin/advertisers/logout', '22-exit-logout.png', 'Advertiser Logout')))); ?>
						</div>
            		</div>
            		<?php } ?>
            	</div>
            </div>
			</div>
			<!--
            <div id="side-search">
        		<h2 class="icon-search">Search Restaurants</h2>
        		<?php //echo $form->create('Location', array('action' => 'admin_index'));?>
        		<div class="green-border">
				<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
					<div class="left" style="width:200px;">
						<div class="marg-b5">
	                		<span class="b orange">By Zip Code, or City:</span><br />
	                		<?php //echo $form->input('Search.zip', array('label'=>false,'div'=>false,'class'=>'form-text', 'style'=>'width:185px;'))?>
	                	</div>
	                	
						<div class="marg-b5">
							<span class="b orange">By Business, or Keyword:</span><br />
							<?php //echo $form->input('Search.name', array('label'=>false,'div'=>false,'class'=>'form-text', 'style'=>'width:185px;'))?>
						</div>
	                	
	                	<div class="marg-b5">
							<?php //echo $form->select('Search.food_cat_id', $food_cats, null, array('label'=>false,'div'=>false,'class'=>'form-text'), 'Search All Food Cuisines')?>
						</div>
					</div>
					<div class="right" style="width:65px;">
						<div><?php //echo $html->image('orange-search-restaurants.png', array('style'=>'width:70px;padding:5px 10px 9px 0;'))?></div>
						<div><?php //echo $form->end(array('label' => 'btn-search.gif', 'style' => 'width:65px;')); ?></div>	
					</div>
				</div>
				</div>
        	</div>
        	-->
        </div>
        <?php }
        echo $this->element('main_col', array('content' => $content_for_layout)); 
        ?>    
    </div>
</div>
</div>
<div id="superfooter" class="clear">
	<div id="footer" class="clearfix">
		<?php if($session->check('Admin')) {
			echo $html->link('Admin Dashboard', '/admin/advertisers/dashboard')." | ";
			echo $html->link('Manage Advertisers', '/admin/advertisers/index')." | ";
			echo $html->link('Billing Manager', '/admin/advertisers/billing')." | ";
			echo $html->link('Admin Profile', '/admin/users/edit')." | ";
			echo $html->link('Admin Logout', '/admin/users/logout');
		}
		?>
	    <p>&copy; <?=date('Y')?> Order A Slice. All rights reserved. Developed By McWay Web Development.</p>
	</div>
</div>
</body>
</html>