<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?=$scripts_for_layout?>
	<?php echo $this->element('js/jquery/accordion', array('name' => 'favsAccordion',
														   'opts' => array('autoHeight' => 'false'))); ?>
	<script>
	$(function() {
		$('.remove-fav-link').click( function () {
			var favId = $(this).attr('id');
			$('#ajax-fav-remove').load($(this).attr('href'), { id: favId[7] });
			$('#fav-drawer-'+favId[7]+', #fav-header-'+favId[7]).slideUp();
			return false;
		});
	});
	</script>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<?php echo (isset($body_onload_for_layout)) ? "<body onload=\"".$body_onload_for_layout."\">" : "<body>"; ?>
<div id="superdrop">
<div id="container">
	<div id="header" class="clearfix">
    	<div id="logo" class="left">
        	<h2><?=$html->link("Order A Slice", "/")?></h2>
        </div>
    	<div id="tagline" class="right">
        	<h3>Hungry? Why Call, When You Can Just Click!</h3>
        </div>
    </div>
    <div id="nav" class="clear clearfix">
    	<?=$this->element('nav') ?>
    </div>
    <div id="wrapper" class="clear clearfix">
    	<div id="sidebar" class="left">
        	<div id="side-features" >
            	<h2 class="icon-feature"><?=$session->read('User.fname')." ".$session->read('User.lname')?></h2><br />
            	<div class="ui-helper-clearfix">
            		<div class="left" style="width:185px;">
            			<?php if(isset($default_addr)) { ?>
            				<div class="orange-border">
            				<div class="ui-widget ui-widget-content ui-corner-all">
            					<div class="orange b">Current Delivery Address:</div><hr />
            					<div class="base">
            						<div class="green b"><?php echo $default_addr['UsersAddr']['name']; ?></div>
            						<div><?php echo $default_addr['UsersAddr']['address']; ?></div>
            						<div><?php echo $default_addr['UsersAddr']['city'].", ".$default_addr['State']['abbr']." ".$default_addr['UsersAddr']['zip']; ?></div>
            					</div>
            				</div>
            				</div>
            			<?php } else { 
            				echo $this->element('error-msg-box', array('msg'=>$html->link('Add Delivery Address', '/user-dash/addresses')));
            			} ?>
            		</div>
            		<div class="left"><?php echo $html->image('current-delivery-address.png', array('style'=>'width:87px;', 'class'=>'pad-l10'))?></div>
            	</div>
            	<div class="orange-border marg-t10">
            	<div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix">
            		<div class="left pad-r20" style="width:130px;"><?php echo $misc->jqueryBtn('/user-dash/addresses', 'ui-icon-home', 'Change Address'); ?></div>
            		<div class="left"><?php echo $jquery->btn('/user-dash/profile', 'ui-icon-person', 'Edit Profile', null, null, null, '105'); ?></div>
            		<div class="left pad-r20" style="width:130px;"><?php echo $misc->jqueryBtn('/restaurants/index', 'ui-icon-cart', 'Browse Menus'); ?></div>
            		<div class="left"><?php echo $jquery->btn('/user-dash/payment_profiles', 'ui-icon-suitcase', 'Pay Profiles', null, null, null, '105'); ?></div>
            	</div>
            	</div>
            </div>
         	<br />
         	<?php 
         	if(!isset($pay_profile_check)) {
         		echo $this->element('warning-msg-box', array('msg' => 'NOTE: Please setup a '.$html->link('Payment Profile', '/user-dash/payment_profiles').'.'));
         	}
         	?>
         	<h1 class="marg-b10">My Favorite Restaurants</h1><hr />
         	<div id="ajax-fav-remove"></div>
         	<?php
         	//echo debug($my_favs);
         	if(sizeof($my_favs) > 0) {
         		?><div class="ui-widget ui-accordion" id="favsAccordion"><?php 
         		foreach ($my_favs as $fav):
         			?>
         			<h2 class="ui-widget-header ui-accordion-header" id="fav-header-<?php echo $fav['Location']['id']; ?>">
         				<a href="#"><?php echo $fav['Location']['name']; ?></a>
         			</h2>
         			<div class="ui-widget-content ui-accordion-content" id="fav-drawer-<?php echo $fav['Location']['id']; ?>">
         				<div class="ui-helper-clearfix marg-b10">
	         				<div class="left pad-r10" style="width: 80px;"><?=$html->link($html->image("../files/logos/".$fav['Location']['logo'], 
	         																						   array('class'=>'ui-corner-all bordered-logo')), 
	         																			  '/menu/'.$fav['Location']['slug'], 
	         																			  array('escape'=>false))?></div>
	         				<div class="left" style="width: 150px;">
	         					<?php 
	         					if($fav['currently_open'] == 'open') {
	         						echo $html->link($html->image('btn-currently-open.png', array('style'=>'width:150px;', 'class'=>'marg-b5')),
	         										 '/menu/'.$fav['Location']['slug'],
	         										 array('escape' => false));
	         					} else {
	         						echo $html->image('btn-currently-closed.png', array('style'=>'width:150px;'));
	         					}
	         					echo $misc->jqueryBtn('/users/remove_favorite', 'ui-icon-close', 'Remove Favorite', 
											  'Are you sure you would like to remove this restaurant as a favorite?', 
											  'remove-fav-link', 'remFav_'.$fav['Location']['id']);
	         					?>
	         				</div>
         				</div>
         				<hr>
         				<div class="b base">
         					<span>Ordered (<?=$fav['num_orders']?>) Times so far.</span>
         					<?php if($fav['num_orders'] > 0) { 
         						echo "<br />Last ordered on: ".$time->nice($fav['last_ordered']);
         					} ?>
         				</div>
         			</div>
         			<?php
         		endforeach;
         		?></div><?php 
         	} else {
         		echo $this->element('warning-msg-box', array('msg'=>'You have no restaurants saved as favorites.'));
         	}
         	?>
        </div>
        <?php echo $this->element('main_col', array('content' => $content_for_layout)); ?>    
    </div>
</div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer', array('hide_bg' => true)) ?>
</div>
</body>
</html>