<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?=$scripts_for_layout?>
</head>
<body class="inside">
<div id="header2" class="clearfix" style="background:url(/img/banner-pizzazpiza.jpg);">
    <p>4585 University Ave. San Diego, CA 92125</p>
</div>
<div id="container">
    <div id="nav" class="clear clearfix"><?=$this->element('nav') ?></div>
    
    <div id="wrapper" class="clear clearfix">
    	<div id="sidebar" class="left">
    	<?php
		//is a user signed in?
		if($session->check('User')) {
			//logged in user, check if a order has been started.
			if($session->check('User.Order')) {
				//order alreadt started, show shopping cart
				?>
				<div id="side-guestcheck">
					<form action="" method="post">
					<h2 class="icon-feature">My Order</h2>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="gc-cart">
				    <thead>
				    	<tr>
				    		<th>QTY</th>
				    		<th>Item</th>
				            <th>Price</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	<?php
				    	if(sizeof($session->read('User.Order.items')) > 0) {
				    		foreach ($session->read('User.Order.items') as $item):
				    			if($item['type'] == 'item') {
				    				?>
					    			<tr>
					    				<td><?=$item['qty']?></td>
					    				<td><?=$item['MenusCatsItem']['title']?></td>
					    				<td>$<?=number_format($item['qty'] * $item['MenusCatsItem']['price'], 2)?></td>
					    			</tr>
					    			<?php
				    			} else if($item['type'] == 'special'){
				    				?>
					    			<tr>
					    				<td><?=$item['qty']?></td>
					    				<td><?=$item['Special']['title']?></td>
					    				<td>$<?=number_format($item['qty'] * $item['price'], 2)?></td>
					    			</tr>
					    			<?php
				    			} else {
				    				//type is custom pizza
				    				?>
					    			<tr>
					    				<td><?=$item['qty']?></td>
					    				<td>Custom <?=$item['size']?>" Pizza:<br />
					    				<ul>
				    					<?
										if(sizeof($item['toppings']) > 0) {
											foreach ($item['toppings'] as $id => $value):
												$id_parts = explode("_", $id);
												?><li><?=$id_parts[0] ?> - <?=$value?></li><?
											endforeach;
										} else {
											echo "Cheese Pizza";	
										}
										if($item['sauce'] != 'Regular Sauce') { 
											?><li><?=$item['sauce']?></li><?	
										}
										if($item['cheese'] != 'Regular Cheese') { 
											?><li><?=$item['cheese']?></li><?	
										}
					    				?>
					    				</ul>
					    				</td>
					    				<td>$<?=number_format($item['qty'] * $item['price'], 2)?></td>
					    			</tr>
					    			<?
				    			}
				    			
				    		endforeach;	
				    	} else {
				    		?>
				    		<tr>
				    			<td colspan="3">Your order is empty.</td>
				    		</tr>
				    		<?php
				    	}
				    	?>	
				    </tbody>  
				    </table>
				    
				    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="gc-total">
				      <tr>
				        <td><strong>Subtotal</strong></td>
				        <td>$<?=number_format($session->read('User.Order.subtotal'), 2)?></td>
				      </tr>
				      <tr>
				        <td style="border-bottom:1px solid #d4d4d4;"><strong>Tax</strong></td>
				        <td style="border-bottom:1px solid #d4d4d4;">$<?=number_format($session->read('User.Order.tax'), 2)?></td>
				      </tr>
				      <tr>
				        <td><strong class="xlarge red">TOTAL</strong></td>
				        <td>$<?=number_format($session->read('User.Order.total'), 2)?></td>
				      </tr>
				    </table>
				    </form>
				    <?=$html->link('Review Order', '/orders/review')?> | 
				    <?=$html->link('Cancel Order', '/orders/kill/'.$locationid_for_layout)?>
				</div> 
				<?php
			} else {
				//show link to start an order
				echo "<p>Ready to start ordering? Click this button to begin.</p>";
				echo $html->link('Start Order Now', '/orders/start/'.$locationid_for_layout);
			}
		} else {
			//not a user yet, show a join now link, and a login link
			echo "Not a member yet?".$form->button('Join Now', array('onclick' => "location.href='/users/register'"))."<br />"; 			
			echo "Already a member?".$form->button('Login', array('onclick' => "location.href='/users/login'")); 
		}
    	echo debug($location);
    	?>
    	
    	
    	
    	
    	
   

</div>
    
    
        <?php echo $this->element('main_col', array('content' => $content_for_layout)); ?>    
    </div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer') ?>
</div>
</body>
</html>