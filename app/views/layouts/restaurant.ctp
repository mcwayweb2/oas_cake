<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<script type="text/javascript">
	$(document).ready(function() {
		// make the categories accordian style
		$('#sidebar-cats-div').accordion({
			header:     'span',
			autoheight: false
		});
	});
	</script>
	<?php echo $html->css(array('../js/sifr/sifr'), false); ?>
	<?php echo $html->script(array('sifr/sifr', 'sifr/sifr-config')); ?>
	<?=$scripts_for_layout?>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<?php echo (isset($body_onload_for_layout)) ? "<body class='inside' onload=\"".$body_onload_for_layout."\">" : "<body class='inside'>"; ?>

<?php if(isset($location)) { ?>
	<!-- restaurant banner -->
	<div id="header2" style="background-image:url(<?php echo '/img/banners/'.$location['Banner']['filename']; ?>);">
		<div class="ui-helper-clearfix">
			<div class="left pad-r10 red"><p><?php echo $location['Advertiser']['name']." : ".$location['Location']['name']?></p></div>
			<div class="left pad-r20">
				<?php 
				$text = $location['Location']['address1'];
				if(!empty($location['Location']['address2'])) $text .= ' '.$location['Location']['address2'];
				$text .= ', '.$location['Location']['city'].', '.$location['ZipCode']['id'];
				?>
			    <p><?php echo $text; ?></p>
			</div>
		</div><?php 
		
		if($location['Location']['use_logo_in_banner'] == '2') { 
			//display advertiser logo ?>
			<div style="width:440px; padding-left:50px; padding-top:5px;">
				<?php echo $html->image('../files/logos/'.$location['Advertiser']['logo'], 
		 								array('style'=>'height:115px;', 'class'=>'green-border pad2')); ?>
			</div><?php 
		} else if($location['Location']['use_logo_in_banner'] == '1' && !empty($location['Location']['logo'])) {
			//display restaurant logo ?>
			<div style="width:440px; padding-left:50px; padding-top:5px;">
				<?php echo $html->image('../files/logos/'.$location['Location']['logo'], 
		 								array('style'=>'height:115px;', 'class'=>'green-border pad2')); ?>
			</div><?php 
		} else {
			//display sifr flash text
			$pad = (strlen($location['Location']['name']) > 27) ? '5' : '30'; ?>
			<div style="width:440px; padding-left:50px; padding-top:<?php echo $pad; ?>px;"><h1 class="banner-sifr"><?php echo $location['Location']['name']; ?></h1></div>
			<?php if(!empty($location['Location']['slogan'])) { ?>
				<div style="width:440px; padding-left:50px;"><h2 class="slogan-sifr"><?php echo $location['Location']['slogan']; ?></h2></div>
			<?php }	
		}
		?>
	</div>
<?php } else { ?>
	<!-- advertiser banner -->
	<div id="header2" style="background-image:url(<?php echo '/img/banners/'.$session->read('Advertiser.Banner.filename'); ?>);">
		<div class="ui-helper-clearfix">
			<div class="left pad-r20"><p>Logged in as advertiser: <span class="italic"><?php echo $session->read('Advertiser.username'); ?></span></p></div>
		</div>
		<?php if($session->read('Advertiser.use_logo_in_banner') == '1') { ?>
			<div style="width:440px; padding-left:50px; padding-top:5px;">
		 		<?php echo $html->image('../files/logos/'.$session->read('Advertiser.logo'), 
		 								array('style'=>'height:115px;', 'class'=>'green-border pad2')); ?>
			</div>
		<?php } else { ?>
			<div style="width:440px; padding-left:50px; padding-top:30px;"><h1 class="banner-sifr"><?php echo $session->read('Advertiser.name'); ?></h1></div>
		<?php } ?> 
	</div>
<?php } ?>

<div id="container">
    <div id="nav" class="clear clearfix"><?=$this->element('nav') ?></div>
    <div id="wrapper" class="clear clearfix">
    	<div id="sidebar" class="left">
			<?php
			//if an advertiser is logged in, display widgets for advertiser dashboard
			if($session->check('Advertiser')) {
				echo $this->element('advertisers/dashboard', array('ad_locations'=>$ad_locations));
			} else if($session->check('User')) {
				//else, if a user is logged in, display the shopping cart
				echo $this->element('users/shopping-cart', array('loc_id' => $locationid_for_layout));
			} else if(isset($advertiser_login_token)) {
				?>
				<div class="marg-t10">
            	<div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix b">
            		<div class="left pad-r10 orange">Login here to access your account...<div style="width:140px;" class="marg-t5"><?php echo $misc->jqueryBtn('/advertisers/login', 'ui-icon-locked', 'Restaurant Login'); ?></div></div>
            	</div>
            	</div>
				
				<?php
			} else { ?>
				<div class="marg-t10">
            	<div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix b">
            		<div class="left pad-r10 orange">Not a Member Yet...<div style="width:130px;" class="marg-t5"><?php echo $misc->jqueryBtn('/users/register', 'ui-icon-pencil', 'Register'); ?></div></div>
            		<div class="left orange">Already a Member...<div style="width:130px;" class="marg-t5"><?php echo $misc->jqueryBtn('#', 'ui-icon-locked', 'Login', null, 'loginLink'); ?></div></div>
            	</div>
            	</div>
            	<?php	
			}
			?>
    	</div>
    	<?php echo $this->element('main_col', array('content' => $content_for_layout)); ?>    
    </div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer') ?>
</div>
</body>
</html>