<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?php echo $html->css(array('../js/sifr/sifr'), false); ?>
	<?php echo $html->script(array('sifr/sifr', 'sifr/sifr-config')); ?>
	<?=$scripts_for_layout?>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<?php echo (isset($body_onload_for_layout)) ? "<body class='inside' onload=\"".$body_onload_for_layout."\">" : "<body class='inside'>"; ?>

<!-- restaurant banner -->
<div id="header2" style="background-image:url(<?php echo '/img/banners/'.$location['Banner']['filename']; ?>);">
	<div class="ui-helper-clearfix">
		<div class="left pad-r10 red"><p><?php echo $location['Advertiser']['name']." : ".$location['Location']['name']?></p></div>
		<div class="left pad-r20">
			<?php 
			$text = $location['Location']['address1'];
			if(!empty($location['Location']['address2'])) $text .= ' '.$location['Location']['address2'];
			$text .= ', '.$location['Location']['city'].', '.$location['ZipCode']['id'];
			?>
		    <p><?php echo $text; ?></p>
		</div>
	</div><?php 
	
	if($location['Location']['use_logo_in_banner'] == '2') { 
		//display advertiser logo ?>
		<div style="width:440px; padding-left:50px; padding-top:5px;">
			<?php echo $html->image('../files/logos/'.$location['Advertiser']['logo'], 
	 								array('style'=>'height:115px;', 'class'=>'green-border pad2')); ?>
		</div><?php 
	} else if($location['Location']['use_logo_in_banner'] == '1' && !empty($location['Location']['logo'])) {
		//display restaurant logo ?>
		<div style="width:440px; padding-left:50px; padding-top:5px;">
			<?php echo $html->image('../files/logos/'.$location['Location']['logo'], 
	 								array('style'=>'height:115px;', 'class'=>'green-border pad2')); ?>
		</div><?php 
	} else {
		//display sifr flash text
		$pad = (strlen($location['Location']['name']) > 27) ? '5' : '30'; ?>
		<div style="width:440px; padding-left:50px; padding-top:<?php echo $pad; ?>px;"><h1 class="banner-sifr"><?php echo $location['Location']['name']; ?></h1></div>
		<?php if(!empty($location['Location']['slogan'])) { ?>
			<div style="width:440px; padding-left:50px;"><h2 class="slogan-sifr"><?php echo $location['Location']['slogan']; ?></h2></div>
		<?php }	
	}
	?>
</div>

<div id="container">
    <div id="nav" class="clear clearfix"><?php echo $this->element('nav'); ?></div>
    <div id="wrapper" class="clear clearfix">
    	<div id="sidebar" class="left">
			<?php echo $this->element('users/order-review-sidebar', array('loc_id' => $location['Location']['id'])); ?>
    	</div>
    	
    	<?php echo $this->element('main_col', array('content' => $content_for_layout)); ?>    
    </div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer') ?>
</div>
</body>
</html>