<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?php echo $scripts_for_layout; ?>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<?php echo (isset($body_onload_for_layout)) ? "<body onload=\"".$body_onload_for_layout."\">" : "<body>"; ?>
<div id="superdrop">
<div id="container">
	<div id="header" class="clearfix">
    	<div id="logo" class="left">
        	<h2><a href="/">Order A Slice</a></h2>
        </div>
    	<div id="tagline" class="right">
        	<h3>Hungry? Why Call When You Can Just Click!</h3>
        </div>
    </div>
    <div id="nav" class="clear clearfix"><?php echo $this->element('nav'); ?></div>
    <div id="wrapper" class="clear clearfix">
    	<div id="sidebar" class="left">
        	<?php
			//include the featured restaurants sidebar
			if(sizeof($featured_restaurants) > 0) {
				CakeLog::write('debug', 'featured restaurants: '.print_r($featured_restaurants, true));
				echo $this->element('featured', array('featured_restaurants', $featured_restaurants));	
			}
        	?>
            <div id="side-search">
        		<h2 class="icon-search">Search Restaurants</h2>
        		<?=$form->create('Location', array('url' => '/restaurants/browse-menus'));?>
        		<div class="green-border">
				<div class="ui-widget-content ui-corner-all">
					<div class="ui-helper-clearfix marg-b5">
						<div class="left" style="width:130px;">
	                		<span class="b base orange">By Zip Code, or City:</span><br />
	                		<?php echo $form->input('Search.zip', array('label'=>false,'div'=>false,'class'=>'form-text', 'style'=>'width:115px;'))?>
						</div>
						<div class="right" style="width:135px;">
							<span class="b base orange">By Business Keyword:</span><br />
							<?php echo $form->input('Search.name', array('label'=>false,'div'=>false,'class'=>'form-text', 'style'=>'width:125px;'))?>
						</div>
					</div>
				
					<?php $order_opts = array('Delivery Only' => 'Delivery Only', 'Pickup Only' => 'Pickup Only'); ?>
					<div class="ui-helper-clearfix marg-b5">
						<div class="left" style="width:130px;">
							<?php echo $form->select('Search.food_cat_id', $food_cats, null, array('label'=>false,'div'=>false,'style'=>'width:120px', 
																								'class'=>'form-text ui-state-default'), 'All Food Types')?>
						</div>
						<div class="right" style="width:135px;">
							<?php echo $form->select('Search.delivery_method', $order_opts, null, array('label'=>false,'div'=>false,'style'=>'width:135px', 
																								     'class'=>'form-text ui-state-default'), 'All Order Methods')?>
						</div>
					</div>
					
					<div class="ui-helper-clearfix">
						<div class="left pad-r5"><?php echo $html->image('pizza-delivery-restaurant-search.png', array('style'=>'width:30px;', 'alt'=>'Pizza Delivery Restaurant Search'))?></div>
						<div class="right"><?php echo $form->end(array('label' => 'btn-search.gif', 'style' => 'width:65px;')); ?></div>
						<div class="right b pad-r10 pad-t5"><?php echo $html->link('Browse Menus', '/restaurants/browse-menus'); ?></div>
					</div>
				</div>
				</div>
        	</div>
        	<div id="side-home-ad" class="gray-border ui-corner-all"><?=$html->image('order-pizza-online.png', array('alt' => 'Order Pizza Online with Order a Slice'))?></div>
        </div>
        <?php echo $this->element('main_col', array('content' => $content_for_layout)); ?>    
    </div>
</div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer') ?>
</div>
</body>
</html>