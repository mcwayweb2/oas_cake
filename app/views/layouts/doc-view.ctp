<?=$html->docType('xhtml-trans');?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->element('js/headers'); ?>
	<?php echo $scripts_for_layout; ?>
	<?php echo $html->script(array('google.analytics')); ?>
</head>
<?php echo (isset($body_onload_for_layout)) ? "<body onload=\"".$body_onload_for_layout."\">" : "<body>"; ?>
<div id="superdrop">
<div id="container">
	<div id="header" class="clearfix">
    	<div id="logo" class="left">
        	<h2><a href="/">Order A Slice</a></h2>
        </div>
    	<div id="tagline" class="right">
        	<h3>Hungry? Why Call When You Can Just Click!</h3>
        </div>
    </div>
    <div id="nav" class="clear clearfix"><?php echo $this->element('nav'); ?></div>
   <div id="" class="clear clearfix marg-t10" style="padding-left:80px;">
 		<?php echo $content_for_layout; ?>    
    </div>
</div>
</div>
<div id="superfooter" class="clear">
	<?=$this->element('footer', array('hide_bg' => true)) ?>
</div>
</body>
</html>