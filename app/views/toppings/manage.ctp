<?=$html->script(array('prototype', 'scriptaculous'), false)?>
<div>
	<h1>Manage Toppings</h1><hr />
	<p>Here you can manage the toppings that your restaurants offer. If you have multiple locations, choose whether your locations all offer the same toppings, or if you would 
	like to manage each locations available toppings individually. You can update this in your <?=$html->link('Account Settings', '/advertisers/edit')?>.</p>
	<hr />
	<?	
	$topping_text = ($session->read('Advertiser.universal_toppings') == 1) ? "Your locations currently all offer the same toppings." : "Managing toppings for location: ".$location['Location']['name'];
	?>
	<h4><?=$topping_text?></h4>
	<br /><br />
	<div id="toppings_div">
	<fieldset>
		<legend>Meat Toppings</legend>
		<div class="clearfix">
			<div class="left" style="width:250px;">
			<?php
			echo $form->create('Topping', array('action'=>'manage'));
			echo $form->hidden('Location.id', array('value'=>$location['Location']['id']));
			
			$count = 0;
			foreach ($meat_toppings as $topping):
				//split the toppings up into two columns
				if($count == round($num_meat_toppings / 2)) {
					echo "</div><div class='left' style='width:250px;'>";
				}
			
				$params = array('type'=>'checkbox', 'label'=>$topping['Topping']['title']);
				if(in_array($topping['Topping']['id'], $current_toppings)) {
					$params['checked'] = 'checked';
				}
				
				echo $form->input('Topping.'.$topping['Topping']['id'], $params);
				$count++;
			endforeach;
			?>
			</div>
		</div>
		<div class="clear"></div>
	</fieldset><br />
	<fieldset>
		<legend>Standard Toppings</legend>
		<div class="clearfix">
			<div class="left" style="width:250px;">
			<?
			$count = 0;
			foreach ($standard_toppings as $topping):
				//split the toppings up into two columns
				if($count == round($num_standard_toppings / 2)) {
					echo "</div><div class='left' style='width:250px;'>";
				}
			
				$params = array('type'=>'checkbox', 'label'=>$topping['Topping']['title']);
				if(in_array($topping['Topping']['id'], $current_toppings)) {
					$params['checked'] = 'checked';
				}
				
				echo $form->input('Topping.'.$topping['Topping']['id'], $params);
				$count++;
			endforeach;
			?>
			</div>
		</div>
		<div class="clear"></div>
	</fieldset>
	</div>
	<br />
	<div class="clearfix">
		<div style="float:left;width:150px;">
		<?=$form->end('Submit')?>
		</div>
		<div style="float:right; width:250px;">
			<h4>Having a topping not in the list? Add it!</h4>
			<?=$ajax->form('/ajaxAdd', 'post', array('update'=>'toppings_div'))?>
			<?=$form->hidden('location_id', array('value'=>$location['Location']['id']))?>
			<p><?=$form->input('title', array('label'=>'', 'class'=>'form-text'))?></p>
			<p style="padding-top:10px;"><?=$form->select('type', array('Meat'    =>'Meat Topping',
											  'Standard'=>'Standard Topping'), null, array('label'=>false,'class'=>'form-text'), false)?></p>
			<p><?=$form->end('Add')?></p>
		</div>
	</div>
	<div class="clear"></div>
</div>