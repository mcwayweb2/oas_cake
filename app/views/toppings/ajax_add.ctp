<fieldset>
	<legend>Meat Toppings</legend>
	<div class="clearfix">
		<div class="left" style="width:250px;">
		<?
		echo $form->create('Topping', array('action'=>'manage'));
		echo $form->hidden('Location.id', array('value'=>$location_id));
		
		$count = 0;
		foreach ($meat_toppings as $topping):
			//split the toppings up into two columns
			if($count == round($num_meat_toppings / 2)) {
				echo "</div><div class='left' style='width:250px;'>";
			}
		
			$params = array('type'=>'checkbox', 'label'=>$topping['Topping']['title']);
			if(in_array($topping['Topping']['id'], $current_toppings)) {
				$params['checked'] = 'checked';
			}
			
			echo $form->input('Topping.'.$topping['Topping']['id'], $params);
			$count++;
		endforeach;
		?>
		</div>
	</div>
	<div class="clear"></div>
</fieldset>
<br />
<fieldset>
	<legend>Standard Toppings</legend>
	<div class="clearfix">
		<div class="left" style="width:250px;">
		<?
		$count = 0;
		foreach ($standard_toppings as $topping):
			//split the toppings up into two columns
			if($count == round($num_standard_toppings / 2)) {
				echo "</div><div class='left' style='width:250px;'>";
			}
		
			$params = array('type'=>'checkbox', 'label'=>$topping['Topping']['title']);
			if(in_array($topping['Topping']['id'], $current_toppings)) {
				$params['checked'] = 'checked';
			}
			
			echo $form->input('Topping.'.$topping['Topping']['id'], $params);
			$count++;
		endforeach;
		?>
		</div>
	</div>
	<div class="clear"></div>
	</fieldset>