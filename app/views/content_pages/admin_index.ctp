<div class="contentPages index">
<div class="clearfix">
	<div class="left"><h2><?php __('Content Pages');?></h2></div>
	<div class="right"><?=$html->link('Add Content Page', array('action'=>'add')) ?></div>
</div>
<div class="clear"></div>
<hr />
<table cellpadding="0" cellspacing="0" class="records-table">
<tr style="text-align: left;">
	<th><?php echo $paginator->sort('title');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<tr><td colspan="2" class="accent-row" style="padding: 2px;"></td></tr>
<?php
$i = 0;
foreach ($contentPages as $contentPage):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $contentPage['ContentPage']['title']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $contentPage['ContentPage']['id']), array('target'=>'_blank')); ?> | 
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $contentPage['ContentPage']['id'])); ?> | 
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $contentPage['ContentPage']['id']), null, 'Are you ure you would like to completely remove this content page?'); ?>
		</td>
	</tr>
<?php endforeach; ?>
<tr><td colspan="2" class="accent-row" style="padding: 2px;"></td></tr>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<?php '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
