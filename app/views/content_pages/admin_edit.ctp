<?=$html->script(array('ckeditor/ckeditor', 'ckfinder/ckfinder'), false);?>
<script type="text/javascript">
  var ck_newsContent = CKEDITOR.replace( 'content' );
  ck_newsContent.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
  CKFinder.SetupCKEditor( ck_newsContent, 'js/ckfinder') ;
</script>


<h2>Edit Content Page</h2><hr />
<div class="contentPages form">
<?php echo $form->create('ContentPage', array('action'=>'edit'));?>
<?=$form->input('id') ?>
	<fieldset>
 		<legend><?php __('Edit Content');?></legend>
 		<p><span class="label-req">Content Page Title: *</span><br />
 		<?=$form->input('title', array('label'=>'', 'class'=>'form-text', 'div'=>false, 'style'=>'width:800px'))?></p>
 		<p><span class="label-req">Page Content: *</span><br />
 		<?=$form->input('content', array('label'=>'', 'class'=>'ckeditor', 'div'=>false))?></p>
	</fieldset><br />
	<fieldset>
		<legend>Page SEO Info</legend>
		<p><span class="label-req">SEO Description: *</span><br />
 		<?=$form->input('seo_description', array('label'=>'', 'class'=>'form-text', 'div'=>false, 'style'=>'width:800px'))?></p>
 		<p><span class="label-req">SEO Keywords: *</span><br />
 		<?=$form->input('seo_keywords', array('label'=>'', 'class'=>'form-text', 'div'=>false, 'style'=>'width:800px'))?></p>
	</fieldset>
<?php echo $form->end('Update');?>
</div>
<?="webroot: ".$this->webroot ?>