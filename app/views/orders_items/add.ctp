<script>
$(function() {
	$('.submitProgress').hide();
	$('.btnAddToOrderItem').click(function() {
		$('.submitBtn').hide();
		$('.submitProgress').show();
		$('#formAddToOrderItem').submit();
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_styles'); ?>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="b lg">Add Item To Order</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b20">
	<div class="ui-helper-clearfix">
	
		<?php if($item['Location']['show_menu_images'] == '1') { ?>
			<div class="left" style="width:220px;">
			<?php 
				echo $html->image($misc->calcImagePath($item['Item']['image'], 'files/'), 
								  array('class'=>'gray-border dbl-border', 'style'=>'width:200px;'));
			?>
			</div>
			<div class="left" style="width:325px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left"><h2><?=$item['Item']['title']?></h2></div>
					<div class="right">
						<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
						<div class="submitBtn"><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Add Item To Order', null, 'btnAddToOrderItem'); ?></div>
					</div>
				</div>
			
		<?php } else { ?>
			<div class="pad-l10">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left"><h2 class="xlg"><?=$item['Item']['title']?></h2></div>
					<div class="right">
						<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
						<div class="submitBtn"><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Add Item To Order', null, 'btnAddToOrderItem'); ?></div>
					</div>
				</div>
			
			
		<?php } ?>
			
			<?php if(!empty($item['Item']['description'])) { ?>
				<div class="orange marg-b10"><?php echo $item['Item']['description']; ?></div>
			<?php } ?>
			<div class="b"><?php 
				if(isset($item['ItemsOption'])) {
					//we have multiple pricing options
					?><div class="base">Starting at:</div><?php 
				} 
				echo "$".number_format($item['Item']['price'], 2);	
				?>
			</div>
		</div>
	</div> <!-- end ui-helper-clearfix div -->
	
	<?=$form->create('OrdersItem', array('action' => 'add', 'id'=>'formAddToOrderItem'))?>
	<?=$form->hidden('item_id', array('value'=>$item['Item']['id']))?>
	<?=$form->hidden('order_id', array('value'=>$session->read('User.order_id')))?>
	<fieldset class="marg-b10 marg-t10">
		<legend>Item Details</legend>
		
		<?php if(isset($item['ItemsOption'])) { 
			foreach($item['ItemsOption'] as $add_group_id => $opts):
				if($add_group_id > 0) {
					//display select boxes 
					?><div class="marg-b5"><?php 
					for($i = 1; $i <= $opts['add_group_limit']; $i++) { ?>
						<div class="ui-helper-clearfix marg-b10">
							<div class="left txt-r label-req pad-r5" style="width:180px;">
								<?php echo ($opts['add_group_limit'] > 1) ? 'Selection '.$i.':' : '&nbsp;'; ?>
							</div>
							<div class="left pad-l5">
								<?php echo $form->select('ItemsOption.'.$add_group_id.'.'.$i, $opts['OptList'], null, 
														 array('class'=>'form-text','label'=>''), false); ?>
							</div>
						</div><?php	
					} 
					?></div><?php 
				} else {
					//display individual checkboxes for each
					foreach($opts['OptList'] as $option_id => $opt): ?>
						<div class="ui-helper-clearfix marg-b5">
							<div class="left txt-r" style="width:215px;"><?php echo $form->checkbox('ItemsOption.'.$add_group_id.'.'.$option_id); ?></div>
							<div class="left pad-l5" style="width:300px;"><?php echo $opt; ?></div>
						</div>
						<?php 
					endforeach;
					
				}
			endforeach;
		} ?>
		
		<?php if(isset($item['CatsOption'])) { 
			foreach($item['CatsOption'] as $add_group_id => $opts):
				if($add_group_id > 0) {
					//display select boxes 
					?><div class="marg-b5"><?php 
					for($i = 1; $i <= $opts['add_group_limit']; $i++) { ?>
						<div class="ui-helper-clearfix marg-b10">
							<div class="left txt-r label-req pad-r5" style="width:180px;">
								<?php echo ($opts['add_group_limit'] > 1) ? 'Selection '.$i.':' : '&nbsp;'; ?>
							</div>
							<div class="left pad-l5">
								<?php echo $form->select('CatsOption.'.$add_group_id.'.'.$i, $opts['OptList'], null, 
														 array('class'=>'form-text','label'=>''), false); ?>
							</div>
						</div><?php	
					} 
					?></div><?php 
				} else {
					//display individual checkboxes for each
					foreach($opts['OptList'] as $option_id => $opt): ?>
						<div class="ui-helper-clearfix marg-b5">
							<div class="left txt-r" style="width:215px;"><?php echo $form->checkbox('CatsOption.'.$add_group_id.'.'.$option_id); ?></div>
							<div class="left pad-l5" style="width:300px;"><?php echo $opt; ?></div>
						</div>
						<?php 
					endforeach;
					
				}
			endforeach;
		} ?>
		
		<table class="form-table">
			<tr>
				<td class="label-req">Qty: <span class="required">*</span></td>
				<td><?php echo $form->select('qty', $misc->qtys(), null, array('class'=>'form-text','label'=>''), false); ?></td>
			</tr>
			<tr>
				<td class="label-req">Special Instructions: </td>
				<td><?=$form->textarea('comments',array('class'=>'form-text','label'=>''))?><br />
				<span class="base orange">(e.g. Ingredients to remove.)</span></td>
			</tr>
		</table>
	</fieldset>
	
	<div class="ui-helper-clearfix">
		<div class="right">
			<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
			<div class="submitBtn"><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Add Item To Order', null, 'btnAddToOrderItem'); ?></div>
		</div>
	</div>
	<?php //echo debug($item); ?>
</div>
<div class="topping-hr"></div>