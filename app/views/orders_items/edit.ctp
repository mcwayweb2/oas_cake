<?php echo $jquery->scrFormSubmit('#btnEditOrderItem', '#formEditOrderItem'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>

<h2>Modify Order Item</h2>
<div class="marg-b20 pad10 green-border">
<div class="ui-widget-content ui-corner-all">
	<div class="ui-helper-clearfix">
		<div class="left" style="width:220px;">
		<?php 
			echo $html->image($misc->calcImagePath($item['Item']['image'], 'files/'), 
							  array('class'=>'gray-border dbl-border', 'style'=>'width:200px;'));
		?>
		</div>
		<div class="left" style="width:160px;">
			<h2><?=$item['Item']['title']?></h2>
			<span class="orange"><?=$item['Item']['description']?></span><br />
			<div class="b marg-t10"><?php echo $number->currency($item['Item']['price']); ?></div>
		</div>
		<div class="right"><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Update Order Item', null, null, 'btnEditOrderItem'); ?></div>
	</div>
	
	<hr class="marg-t5" />
	
	<?=$form->create('OrdersItem', array('action' => 'edit', 'id'=>'formEditOrderItem'))?>
	<?php echo $form->input('id'); ?>
	<?php echo $form->hidden('item_id', array('value'=>$item['Item']['id'])); ?>
	<?php echo $form->hidden('total'); ?>
	<?=$form->hidden('order_id', array('value'=>$session->read('User.order_id')))?>
	<fieldset>
		<legend>Item Details</legend>
		<table class="form-table">
			<tr>
				<td class="label-req">Qty: <span class="required">*</span></td>
				<td><?php echo $form->select('qty', $misc->qtys(), null, array('class'=>'form-text','label'=>''), false); ?></td>
			</tr>
			<tr>
				<td class="label-req">Special Instructions: </td>
				<td><?=$form->textarea('comments',array('class'=>'form-text','label'=>''))?><br />
				<span class="base orange">(e.g. Ingredients to remove.)</span></td>
			</tr>
		</table>
	</fieldset>
</div>
</div>
