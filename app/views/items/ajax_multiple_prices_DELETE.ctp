<hr class="orange"/>
<table class="form-table" style="width:100%;">
<?php for($x = 0; $x < $qty; $x++) { ?>
	<tr>
		<td style="width:210px; padding-bottom:5px;"><span class="b base">Enter Option Label:</span><br />
			<?php echo $form->input('labels.'.$x, array('class'=>'form-text ui-state-default ui-corner-all option-labels', 'div'=>false, 'label'=>false)); ?><br />
			<span class="base orange">(e.g. "Large", or "With Cheese").</span>
		</td>
		<td class="valign-t"><span class="b base">Enter Option Price:</span><br />
			<?php echo $form->input('prices.'.$x, array('class'=>'form-text ui-state-default ui-corner-all option-prices', 'div'=>false, 'label'=>false)); ?>
		</td>
	</tr>
<?php } ?>
</table>
<script>
$(function() {
	$('.option-labels').alphaNumeric({ allow: " &" });
	$('.option-prices').numeric({ allow: "." });
});
</script>