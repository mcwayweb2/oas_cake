<script>
$(function() {
	$('.ItemNewSeriesQty').change(function() {
		$('.progressSeriesLimit').show(); 
		/* update choice limit drop down with new number of max choices */
		$('#ItemNewSeriesLimit').load('/items/ajax_update_series_limit', { qty: $(this).val() }, function() {
			$('#uniform-LocationsCatNewSeriesLimit span:first').text('1');
		});
		/* update div with appropriate number of labels */
		$('#divChoiceSeriesLabels').load('/items/ajax_update_series_labels', { qty: $(this).val() }, function() {
			$('#progressSeriesLimit').hide();
		});
	});
});
</script>

<table>
	<tr>
		<td class="b txt-r pad-r5">How many choices will be in this series?</td>
		<td>
			<div class="ui-helper-clearfix">
				<div class="left pad-r5">
					<?php echo $form->select('new_series_qty', $misc->qtys(), null, array('class'=>'form-text ItemNewSeriesQty')); ?>
				</div>
				<div class="left" style="width:25px;"><div class="progressSeriesLimit"><?php echo $html->image('ajax-loader-sm-circle.gif', array('width'=>'25')); ?></div></div>
			</div>
		</td>
	</tr>
	<tr>
		<td class="b">How many of these choices can the customer select?</td>
		<td><?php echo $form->select('new_series_limit', array(), null, array('class'=>'form-text'), false); ?></td>
	</tr>
</table>
<div class="divChoiceSeriesLabels"></div>