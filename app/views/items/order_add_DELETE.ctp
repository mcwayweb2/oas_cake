<div>
	<h1>Add To Order</h1>
	<br />
	<div class="clearfix">
		<div class="left" style="width:250px;">
		<?php if(!empty($item['MenusCatsItem']['image'])) {
			echo $html->image('../files/'.$item['MenusCatsItem']['image'], array('width'=>200));	
		}?>
		</div>
		<div class="left">
			<h3><?=$item['MenusCatsItem']['title']?></h3>
			<p><?=$item['MenusCatsItem']['description']?></p>
			<p>$<?=number_format($item['MenusCatsItem']['price'], 2)?></p>
		</div>
	</div>
	<div class="clear"></div>
	<br /><hr />
	<p>Enter the quantity you would like to order, along with any special instructions for the order item (e.g. Ingredients to remove).</p>
	<br />
	<?=$form->create('Item', array('action' => 'add'))?>
	<?=$form->hidden('item_id', array('value'=>$item['MenusCatsItem']['id']))?>
	<fieldset>
		<legend>Item Details</legend>
		<table>
			<tr>
				<td>Qty: *</td>
				<td>
					<?
					$options = array();
					for($x = 1; $x <= 10; $x++) {
						$options[$x] = $x;
					}
					echo $form->select('qty', $options, null, array('class'=>'form-text','label'=>''), false);
					?>
				</td>
			</tr>
			<tr>
				<td>Special Instructions: </td>
				<td><?=$form->textarea('instructions',array('class'=>'form-text','label'=>''))?></td>
			</tr>
		</table>
	</fieldset>
	<br />
	<?=$form->end('Add To Order')?>
</div>