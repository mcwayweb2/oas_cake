<script>
$(function() {
	$('.img-upload').uniform();
	$('#itemAddTabs').tabs();
	$('#addItemSubmitDiv').show();
});
</script>
<div id="itemAddTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<?php 
		for($x = 1; $x <= $qty; $x++) {
			$active = ($x == 1) ? 'ui-tabs-selected ui-state-active' : '';
			?><li class="ui-state-default ui-corner-top <?php echo $active; ?>">
			<a href="#addItem<?php echo $x; ?>">Add Item <?php echo $x; ?></a></li><?php
		}
		?>
	</ul>
<?php for($x = 1; $x <= $qty; $x++) { ?>
	<div id="addItem<?php echo $x; ?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<table class="form-table" style="width:100%;">
			<tr>
				<td class="label-req" style="width:220px;">Title: <span class="required">*</span></td>
				<td><?=$form->input('items.'.$x.'.title',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false, 'div'=>false))?><br />
				<span class="red base b">(If the title is left empty, this item will not be added.)</span></td>
			</tr>
			<tr>
				<td class="label-req">Description:</td>
				<td><?=$form->input('items.'.$x.'.description',
						array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false, 'div'=>false, 'rows' => '3'))?><br />
				<span class="base orange">(List any toppings, ingredients, etc.)</span></td>
			</tr>
			<tr>
				<td class="label-req">Item Photo:</td>
				<td><?=$form->file('items.'.$x.'.image',array("class"=>"form-text img-upload", 'label'=>false))?></td>
			</tr>
			<tr>
				<td class="label-req">Price: <span class="required">*</span></td>
				<td><?=$form->input('items.'.$x.'.price',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false, 'div'=>false))?><br />
				<span class="red base b">(If the price is left empty, this item will not be added.)</span></td>
			</tr>
		</table>
	</div>
<?php } ?>
</div>