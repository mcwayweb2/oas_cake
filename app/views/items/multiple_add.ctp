<?php echo $html->script('jquery/alphanumeric'); ?>
<script>
$(function() {
	$('#progressNumItems, #addItemSubmitDiv').hide();
	
	$('#ItemQty').change(function() { 
		$('#progressNumItems').show();
		$('#ajaxItemsDiv').load('/items/ajax_multiple_items', { numOpts: $(this).val() }, function() {
			$('#progressNumItems').hide();
		});
		return false;
	});
});

</script>
<?php echo $jquery->scrFormSubmit('#btnMultipleAddItem', '#formMultipleAddItem'); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Add Multiple Menu Items</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/locations/edit_menu/'.$location['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to Menu Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b10">
	<?php echo $form->create('Item', array('action' => 'multiple_add', 'id'=>'formMultipleAddItem', 'type'=>'file')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
	
	<p>Use this tool, if you would like to add multiple items to the same category at once. This tool may only be used on menu 
	categories that have already been set up for this location. If your desired category does not show up on the drop down list, 
	visit the <?php echo $html->link('Category Manager', '/locations/manage_categories'); ?> to add your desired menu category, 
	then revisit this page when you are ready.</p>
	
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Add Details</legend>
		
		<table class="form-table" style="width:100%;">
			<tr>
				<td class="label-req" style="width:220px;">Select Menu Category: <span class="required">*</span></td>
				<td><?=$form->select('cat_id', $cat_list, null, array("class"=>"form-text", 'label'=>false), false)?></td>
			</tr>
			<tr>
				<td class="label-req" style="width:220px;">How many items will you be adding: <span class="required">*</span></td>
				<td>
					<div class="ui-helper-clearfix">
						<div class="left"><?=$form->select('qty', $misc->qtys(true, 20), null, array("class"=>"form-text", 'label'=>false), false)?></div>
						<div class="left" id="progressNumItems"><?php echo $html->image('ajax-loader-med-bar.gif', array('style'=>'')); ?></div>
					</div>				
				</td>
			</tr>
		</table>
	</fieldset>
	
	<div id="ajaxItemsDiv"></div>
	<div class="ui-helper-clearfix" id="addItemSubmitDiv">
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Menu Items', null, null, 'btnMultipleAddItem'); ?></div>
	</div>
	</form>
	
	<?php //echo debug($this->data); ?>
</div>