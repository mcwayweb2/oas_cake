<?php $num = rand(); ?>
<td style="width:210px; padding-bottom:5px;"><span class="b base">Enter Option Label:</span><br />
	<?php echo $form->input('labels.'.$num, array('class'=>'form-text ui-state-default ui-corner-all option-labels', 
												  'div'=>false, 'label'=>false)); ?><br />
	<span class="base orange">(e.g. "Large", or "With Cheese").</span>
</td>
<td class="valign-t"><span class="b base">Enter Option Price:</span><br />
	<?php echo $form->input('prices.'.$num, array('class'=>'form-text ui-state-default ui-corner-all option-prices', 
												  'div'=>false, 'label'=>false)); ?>
</td>
<td style="width:25px;">
	<?php echo $jquery->btn('#', 'ui-icon-circle-close', '', null, 'classRemovePricingOpt', null, '25', null, 'Remove This Pricing Option'); ?>
</td>
<td style="width:25px;">
	<div class="progressAddPricingOpt"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
</td>
<script>
$(function() { 
	$('.progressAddPricingOpt').hide();

	$('.classRemovePricingOpt').click(function() { 
		//$(this).closest('tr').find('.progressAddPricingOpt:first').show();
		$(this).closest('tr').remove();
		return false;
	});
});
</script>