<?php $rand_class = 'ajax-item-opt-select-'.mt_rand(); ?>

<?php echo $this->element('js/jquery/add_form_text_events'); ?>

<?php echo $this->element('advertisers/item_opts', array('ajax_item'      => $ajax_item,
														 'rand_class'     => $rand_class,
														 'combos_item_id' => $combos_item_id,
														 'array_index'    => $array_index)); ?>
<script>
$(function() {
	//only apply the uniform class to form elements that have not already had it added...
	$(".<?php echo $rand_class; ?>").uniform();
});
</script>