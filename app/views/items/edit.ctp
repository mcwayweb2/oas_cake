<div id="ajaxEditItemFormContent">
<?php echo $html->script('jquery/alphanumeric'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>
<script>
$(function() { 
	$('.progressAddPricingOpt, #progressSeriesLimit, #divAddNewSeries').hide();
	$('#ItemImageTemp, #ItemCatId').uniform();
	$('#ItemPrice').numeric({ allow: "." });

	$('#btnHideEditItemForm').click( function () {
		$('#ajaxEditItemFormContent').empty();
		$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
	});

	$('#btnEditItem').click( function () {
		var success = true;
		if($('#ItemTitle').val() == '') {
			$('#ItemTitleText').html('Please enter a menu item title!');
			$('#ItemTitle').addClass('ui-state-highlight'); success = false;
		}
		if($('#ItemPrice').val() == '') {
			$('#ItemPriceText').html('Please enter a menu item price!');
			$('#ItemPrice').addClass('ui-state-highlight'); success = false;
		}
		if(success) {
			$('#formEditItem').submit();
		}
		return false;
	});

	//add another pricing option to the form
	$('#ajaxAddPricingOpt').click(function() { 
		$('html, body').animate( { scrollTop: 220 }, 'fast' );
		$('.rowPricingOpt:last').after($('<tr class="rowPricingOpt">').load('/items/ajax_add_one_pricing_opt'));
	});

	$('.classRemovePricingOpt').click(function() { 
		//$(this).closest('tr').find('.progressAddPricingOpt:first').show();
		$(this).closest('tr').remove();
		return false;
	});

	$('#ItemNewSeriesQty').change(function() {
		$('#progressSeriesLimit').show(); 
		/* update choice limit drop down with new number of max choices */
		$('#ItemNewSeriesLimit').load('/items/ajax_update_series_limit', { qty: $(this).val() }, function() {
			$('#uniform-LocationsCatNewSeriesLimit span:first').text('1');
		});
		/* update div with appropriate number of labels */
		$('#divChoiceSeriesLabels').load('/items/ajax_update_series_labels', { qty: $(this).val() }, function() {
			$('#progressSeriesLimit').hide();
		});
	
	});
	
	$('.linkAddNewSeries').toggle(function() { 
		$('#divAddNewSeries').show();
	}, function() {
		$('#divAddNewSeries').hide();
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Update Menu Item</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('#', 'ui-icon-circle-close', 'Hide This Form', null, null, 'btnHideEditItemForm'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b10">
	<?=$form->create('Item', array('action' => 'edit', 'id'=>'formEditItem', 'type'=>'file'))?>
	<?php echo $form->input('id'); ?>
	<?php echo $form->hidden('location_id', array('value'=>$this->data['Item']['location_id'])); ?>
	<a class="b green">Update the information for your menu item below:</p>
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Menu Item Details</legend>
		
		<div class="ui-helper-clearfix">
			<div class="left" style="width:220px;">
				<?
				echo $html->image($misc->calcImagePath($this->data['Item']['image_path'], 'files/'), 
								  array('class'=>'gray-border pad2', 'style'=>'width:200px;'));
				?>
			</div>
			<div class="left">
				<table class="form-table" style="width:100%;">
					<tr>
						<td class="label-req">Menu Category: <span class="required">*</span></td>
						<td><?=$form->select('cat_id', $cat_list, null, array("class"=>"form-text", 'label'=>false), false)?></td>
					</tr>
					<tr>
						<td class="label-req">Title: <span class="required">*</span></td>
						<td><?=$form->input('title',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false))?>
						<span class="base b red" id="ItemTitleText"></span></td>
					</tr>
					<tr>
						<td class="label-req">Description:</td>
						<td><?=$form->input('description',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false, 'div'=>false))?><br />
						<span class="base orange">(List any toppings, ingredients, etc.)</span></td>
					</tr>
					<tr>
						<td class="label-req">Item Photo:</td>
						<td><?=$form->file('image_temp',array("class"=>"form-text", 'label'=>false))?></td>
					</tr>
					<tr>
						<td class="label-req">Price: <span class="required">*</span></td>
						<td><?php echo $form->input('price', array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false,
																"value"=>number_format($this->data['Item']['price'], 2))); ?>
						<span class="base b red" id="ItemPriceText"></span></td>
					</tr>
				</table>					
			</div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left"><?php echo $html->link('Add a Selection Series...', '#', array('class'=>'linkAddNewSeries')); ?></div>
		</div>
		
		<div class="ui-widget-content ui-corner-all marg-t5" id="divAddNewSeries">
			<table>
				<tr>
					<td class="b txt-r pad-r5">How many choices will be in this series?</td>
					<td>
						<div class="ui-helper-clearfix">
							<div class="left pad-r5"><?php echo $form->select('new_series_qty', $misc->qtys(), null, array('class'=>'form-text')); ?></div>
							<div class="left" style="width:25px;"><div id="progressSeriesLimit"><?php echo $html->image('ajax-loader-sm-circle.gif', array('width'=>'25')); ?></div></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="b">How many of these choices can the customer select?</td>
					<td><?php echo $form->select('new_series_limit', array(), null, array('class'=>'form-text'), false); ?></td>
				</tr>
			</table>
			<div id="divChoiceSeriesLabels"></div>
		</div>
		
		<?php if(sizeof($this->data['ItemsOption']) > 0) { ?>
		<div class="ui-widget-content ui-corner-all marg-t5"><?php 
			$current = false;
			foreach($this->data['ItemsOption'] as $opt):
				if($opt['add_group_id'] != 0 && $opt['add_group_id'] != '0') {
					//part of a selection series
					if($opt['add_group_id'] != $current) {
						//still on the same series
						?>
						<div class="ui-helper-clearfix marg-t10">
							<div class="left b green">Comes with your choice of (<?php echo $opt['add_group_limit']; ?>):</div>
							<div class="right">
								<?php echo $jquery->btn('/items/delete_series/'.$opt['id'], 
														'ui-icon-circle-close', 'Remove Selection Series', 
														'Are you sure you want to Remove this Selection Series from this Menu Item?'); ?>
							</div>
						</div>
						<?php 
					} 
					?>
					<div class="ui-helper-clearfix pad-l20">
						<div class="left pad-r5"><?php echo $html->image('list-green-check.gif'); ?></div>
						<div class="left pad-b5" style="width:375px;"><?php 
							echo $opt['label']; 
							if($opt['price'] > 0) { 
								echo '<span class="b base pad-l5">(+$'.number_format($opt['price'], 2).')</span>'; 
							} else if($opt['price'] < 0) {
								echo '<span class="b base pad-l5">($'.number_format($opt['price'], 2).')</span>';
							}
						?></div>
					</div><?php 
				} else {
					//individual add on option
					if($opt['add_group_id'] != $current) {
						//still on the same series
						?>
						<div class="ui-helper-clearfix marg-t10 marg-b10">
							<div class="left b green">Additional Add-On Options Available:</div>
						</div>
						<?php 
					} 
					?>
					<div class="ui-helper-clearfix pad-l20">
						<div class="left pad-r5"><?php echo $html->image('list-green-check.gif'); ?></div>
						<div class="left pad-b5" style="width:375px;"><?php 
							echo $opt['label']; 
							if($opt['price'] > 0) { 
								echo '<span class="b base pad-l5">(+$'.number_format($opt['price'], 2).')</span>'; 
							} else if($opt['price'] < 0) {
								echo '<span class="b base pad-l5">($'.number_format($opt['price'], 2).')</span>';
							}	
						?></div>
						<div class="right">
							<?php echo $jquery->btn('/items/delete_series/'.$opt['id'], 
														'ui-icon-circle-close', 'Remove Option', 
														'Are you sure you want to Remove this Add-On Option from this Menu Category?'); ?>
						</div>
					</div><?php 	
				} 
				$current = $opt['add_group_id'];
			endforeach; ?>
		</div>
		<?php } ?>
	</fieldset>
	
	<div class="ui-helper-clearfix">
		<div class="left"></div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Item', null, null, 'btnEditItem', '120'); ?></div>
	</div>
	</form>
	<?php //echo debug($this->data); ?>
</div>
</div>