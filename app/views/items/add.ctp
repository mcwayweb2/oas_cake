<div id="ajaxAddItemFormContent">
<?php echo $html->script('jquery/alphanumeric'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>
<script>
$(function() {
	$('#ajaxNumPricingOpts, #progressNumPricingOpts, #progressSeriesLimit, #divAddNewSeries').hide();
	$('#ItemImage, #ItemCatId, #selectNumOpts').uniform();
	$('#ItemPrice').numeric({ allow: "." });

	$('#btnHideAddItemForm').click( function () {
		$('#ajaxAddItemFormContent').empty();
		$('html, body').animate( { scrollTop: 220 }, 'fast' ); 
	});
	/*
	$('#selectNumOpts').change(function() { 
		$('#progressNumPricingOpts').show();
		$('#ajaxPriceDiv').load('/items/ajax_multiple_prices', { numOpts: $(this).val() }, function() {
			$('#progressNumPricingOpts').hide();
		});
		return false;
	});
	*/

	$('#btnAddItem').click( function () {
		var success = true;
		if($('#ItemTitle').val() == '') {
			$('#ItemTitleText').html('Please enter a menu item title!');
			$('#ItemTitle').addClass('ui-state-highlight'); success = false;
		}
		if($('#ItemPrice').val() == '') {
			$('#ItemPriceText').html('Please enter a menu item price!');
			$('#ItemPrice').addClass('ui-state-highlight'); success = false;
		}
		if(success) {
			$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
			$('#formAddItem').submit();
		}
		return false;
	});
	
	$('#ItemNewSeriesQty').change(function() {
		$('#progressSeriesLimit').show(); 
		/* update choice limit drop down with new number of max choices */
		$('#ItemNewSeriesLimit').load('/items/ajax_update_series_limit', { qty: $(this).val() }, function() {
			$('#uniform-LocationsCatNewSeriesLimit span:first').text('1');
		});
		/* update div with appropriate number of labels */
		$('#divChoiceSeriesLabels').load('/items/ajax_update_series_labels', { qty: $(this).val() }, function() {
			$('#progressSeriesLimit').hide();
		});
	
	});
	
	$('.linkAddNewSeries').toggle(function() { 
		$('#divAddNewSeries').show();
	}, function() {
		$('#divAddNewSeries').hide();
	});

	
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Add Menu Item</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('#', 'ui-icon-circle-close', 'Hide This Form', null, null, 'btnHideAddItemForm'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b10">
	<?=$form->create('Item', array('action' => 'add', 'id'=>'formAddItem', 'type'=>'file'))?>
	<?php echo $form->hidden('location_id', array('value'=>$location_id)); ?>
	
	<a class="">Enter the information for your new menu item below. If the item you are adding has 
	<?php echo $html->link('multiple options', '#', array('class'=>'linkAddNewSeries')); ?>, such as more than one available size, 
	click the <?php echo $html->link('Add a Selection Series', '#', array('class'=>'linkAddNewSeries')); ?> link, to set up a group of options 
	for the customer to choose from. You may add multiple Selection Series to an item, (such as a size selection, and a side preference), 
	but they must be added one Series at a time. To add multiple Series, add the initial Series now, while adding the item to your menu. After 
	the item has been saved, visit the items' <span class="italic b">"Update"</span> page, to add additional Selection Series.<br />
	When you are finished, click the <span class="italic b">"Add New Menu Item"</span> button to save your new item and continue.</p>
	
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Menu Item Details</legend>
		<table class="form-table" style="width:100%;">
			<tr>
				<td class="label-req" style="width:210px;">Menu Category: <span class="required">*</span></td>
				<td><?=$form->select('cat_id', $cat_list, null, array("class"=>"form-text", 'label'=>false), false)?></td>
			</tr>
			<tr>
				<td class="label-req">Title: <span class="required">*</span></td>
				<td><?=$form->input('title',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false))?>
				<span class="base b red" id="ItemTitleText"></span></td>
			</tr>
			<tr>
				<td class="label-req">Description:</td>
				<td><?=$form->input('description',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false, 'div'=>false))?><br />
				<span class="base orange">(List any toppings, ingredients, etc.)</span></td>
			</tr>
			<tr>
				<td class="label-req">Item Photo:</td>
				<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false))?></td>
			</tr>
		</table>
		<div id="ajaxPriceDiv">
			<table class="form-table" style="width:100%;">
				<tr>
					<td class="label-req" style="width:210px;">Price: <span class="required">*</span></td>
					<td><?=$form->input('price',array("class"=>"form-text ui-state-default ui-corner-all", 'label'=>false))?>
					<span class="base b red" id="ItemPriceText"></span></td>
				</tr>
			</table>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="right"><?php echo $html->link('Add a Selection Series...', '#', array('class'=>'linkAddNewSeries')); ?></div>
		</div>
		
		<div class="ui-widget-content ui-corner-all marg-t5" id="divAddNewSeries">
			<table>
				<tr>
					<td class="b txt-r pad-r5">How many choices will be in this series?</td>
					<td>
						<div class="ui-helper-clearfix">
							<div class="left pad-r5"><?php echo $form->select('new_series_qty', $misc->qtys(), null, array('class'=>'form-text')); ?></div>
							<div class="left" style="width:25px;"><div id="progressSeriesLimit"><?php echo $html->image('ajax-loader-sm-circle.gif', array('width'=>'25')); ?></div></div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="b">How many of these choices can the customer select?</td>
					<td><?php echo $form->select('new_series_limit', array(), null, array('class'=>'form-text'), false); ?></td>
				</tr>
			</table>
			<div id="divChoiceSeriesLabels"></div>
		</div>
	</fieldset>
	
	<div class="ui-helper-clearfix">
		<div class="left">
			<?php echo $html->link('Need to add Multiple Items to a Category?', 
								   '/items/multiple_add/'.$location_id, array('class'=>'')); ?>
		</div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add New Menu Item', null, null, 'btnAddItem', '155'); ?></div>
	</div>
	</form>
</div>
</div>