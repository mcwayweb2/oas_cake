<?php echo $jquery->scrFormSubmit('#btnAddToOrderPizza', '#formAddToOrderPizza'); ?>
<script>
$(function() { 
	$('.submit-loaders').hide();
	$('.btnAddToOrderPizza').click( function () {
		$('.btnAddToOrderPizza').hide();
		$('.submit-loaders').show();	
		$('#formAddToOrderPizza').submit();
		return false;
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_styles'); ?>
 
 <?=$form->create('Pizza', array('action'=>'add', 'id'=>'formAddToOrderPizza')) ?>
 <?php echo $form->hidden('order_id', array('value'=>$session->read('User.order_id'))); ?>
 <div class="marg-t10 pad10 gray-border">
 	<div class="ui-helper-clearfix">
 		<div class="right">
 			<?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Pizza To Order', null, 'btnAddToOrderPizza', null, 150); ?>
 			<div class="submit-loaders"><?php echo $html->image('ajax-loader-med-bar.gif'); ?></div>
 		</div>
 	</div>
 
 	<fieldset class="marg-b10"><legend>Choose Pizza Size...</legend>
 		<div class="ui-helper-clearfix" style="padding-left:150px;"><div class="">
 		<?
		$opts = array();
		foreach($sizes as $size):
			$opts[$size['Size']['id']] = "  $".number_format($size['Size']['base_price'], 2)." - ".
										$size['Size']['title']." (".$size['Size']['size']."\")";
		endforeach;
		 echo $form->radio('size_id', $opts, 
						   array('legend'=>false,'separator'=>'<br />','value'=>$sizes[0]['Size']['id']));
		?>
		</div></div>
 	</fieldset>
 	<a class="base orange">NOTE: Half Toppings will still be charged the same price as a full topping.</p>
 
 	<fieldset class="marg-b10">
 		<legend>Pizza Options...</legend>
 		<table class="form-table">
 			<tr>
 				<td class="label-req">Crust Type: <span class="required">*</span></td>
 				<td><?php echo $form->select('locations_crust_id', $crusts, null, array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Sauce Options: <span class="required">*</span></td>
 				<td><?php echo $form->select('sauce', $misc->sauces(), 'Regular Sauce', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Cheese Type: <span class="required">*</span></td>
 				<td><?php echo $form->select('cheese', $misc->cheeses(), 'Regular Cheese', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
			<tr>
 				<td class="label-req">Quantity: <span class="required">*</span></td>
 				<td><?php echo $form->select('qty', $misc->qtys(), null, array('class'=>'form-text','label'=>''), false); ?></td>
 			</tr>
			<tr>
				<td class="label-req">Additional Instructions:</td>
				<td><?=$form->textarea('comments',array('class'=>'form-text ui-state-default ui-corner-all','label'=>''))?></td>
			</tr>
 		</table>
 	</fieldset>
 	
 	<?php echo $this->element('advertisers/toppings', array('toppings' => $toppings)); ?>
 	
 	<div class="ui-helper-clearfix">
 		<div class="right">
 			<?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Pizza To Order', null, 'btnAddToOrderPizza', null, 150); ?>
 			<div class="submit-loaders"><?php echo $html->image('ajax-loader-med-bar.gif'); ?></div>
 		</div>
 	</div>
 	</form>
 </div>