<script>
$(function() {
	$('.cat-item-select').change(function() {
		var container = $(this).closest('.cat-item-container');
		container.find('.ajax-item-loader').show();

		var itemOptsDiv = container.find('.item-opts-div'); 
		var idParts = itemOptsDiv.attr('id').split('_');
		itemOptsDiv.load('/items/ajax_get_item_opts', { id           : $(this).val(),
														array_index  : idParts[2],
														combosItemId : idParts[1]}, function() {
			container.find('.ajax-item-loader').hide();
		});
	});

	$('.submitProgress').hide();
	$('.btnAddToOrderCombo').click(function() {
		$('.submitBtn').hide();
		$('.submitProgress').show();
		$('#formAddToOrderCombo').submit();
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_styles'); ?>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="b lg">Add Combo To Order</div>
</div>
<div class="ui-widget-content ui-corner-all marg-b20">
	<div class="ui-helper-clearfix">
		<?php if($combo['Location']['show_menu_images'] == '1') { ?>
			<div class="left" style="width:220px;">
			<?php 
				echo $html->image($misc->calcImagePath($combo['Combo']['image'], 'files/'), 
								  array('class'=>'gray-border pad2 dbl-border', 'style'=>'width:200px;'));
			?>
			</div>
			<div class="left" style="width:165px;">
				<h2><?=$combo['Combo']['title']?></h2>
				<?php if(!empty($combo['Combo']['description'])) { ?>
					<div class="orange marg-b10"><?php echo $combo['Combo']['description']; ?></div>
				<?php } ?>
				<div class="b"><?php echo $number->currency($combo['Combo']['price']); ?></div>
			</div>		
		<?php } else { ?>
			<div class="left" style="width:385px;">
				<h2><?=$combo['Combo']['title']?></h2>
				<?php if(!empty($combo['Combo']['description'])) { ?>
					<div class="orange marg-b10"><?php echo $combo['Combo']['description']; ?></div>
				<?php } ?>
				<div class="b"><?php echo $number->currency($combo['Combo']['price']); ?></div>
			</div>
		<?php } ?>
	</div>
	
	<hr class="marg-t5" />
	
	<?=$form->create('OrdersCombo', array('action' => 'add', 'id'=>'formAddToOrderCombo'))?>
	<?=$form->hidden('combo_id', array('value'=>$combo['Combo']['id']))?>
	<?=$form->hidden('order_id', array('value'=>$session->read('User.order_id')))?>
	<fieldset class="marg-b10">
		<legend>Combo Details</legend>
		<table class="form-table">
			<tr>
				<td class="label-req" style="width:200px;">Qty: <span class="required">*</span></td>
				<td><?php echo $form->select('qty', $misc->qtys(), null, array('class'=>'form-text','label'=>''), false); ?></td>
			</tr>
			<tr>
				<td class="label-req">Special Instructions: </td>
				<td><?php echo $form->textarea('comments',array('class'=>'form-text','label'=>''));?></td>
			</tr>
		</table>
	</fieldset>
	
	<?php 
	if(sizeof($combo['CombosPizza']) > 0) { 
		foreach($combo['CombosPizza'] as $p):
			for($x = 1; $x <= $p['CombosPizza']['qty']; $x++):
				?>
				<fieldset class="marg-b10">
					<legend><?php echo $p['Size']['size'].'" '.$p['Size']['title']; ?> Pizza #<?php echo $x; ?> Details</legend>
					<!--
					<div class="ui-helper-clearfix">
						<div class="right"><?php //echo $misc->jqueryBtn('/orders_specials_sizes/ajax_extra_toppings', 'ui-icon-wrench', 'Add Toppings', null, null, 'btnMoreToppings');?></div>
					</div>-->
					
					<?php if(sizeof($p['Topping']) > 0) { $ct = 0; ?>
						<div class="ui-helper-clearfix"><div class="left pad-r5 b pad-l10">Pizza comes with:</div>
							<?php foreach ($p['Topping'] as $t): ?>
								<div class="left italic"><?php
									if($ct++ > 0) echo ", "; 
									echo $t['Topping']['title']; 
								?></div>
							<?php endforeach; ?>
						</div>
						<div class="pad-l10">Select any additional toppings/options below.</div>
						<hr />
					<?php } ?>
					
					<table class="form-table"><?php 
						if($p['CombosPizza']['allowed_toppings'] > 0) {
							//are there any user selectable toppings 
							for($i = 1; $i <= $p['CombosPizza']['allowed_toppings']; $i++) {
								?>
								<tr><td class="label-req" style="width:200px;">Topping <?php echo $i; ?>:</td>
									<td><?php
										$nm = "data[CombosPizza][".$p['CombosPizza']['id']."][".$x."][toppings][".$i."]"; 
										echo $form->select($nm, $toppings_list, null, 
														   array('class'=>'form-text ui-state-default ui-corner-all', 'name'=>$nm, 'id'=>$misc->prefixNameToId($nm)), 'No Topping'); ?>
									</td>
								</tr><?php 
							}
						}	
						?>
			 			<tr>
			 				<td class="label-req" style="width:200px;">Crust Type: <span class="required">*</span></td>
			 				<td><?php echo $form->select('CombosPizza.'.$p['CombosPizza']['id'].'.'.$x.'.locations_crust_id', $crusts, null, array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
			 			</tr>
			 			<tr>
			 				<td class="label-req">Sauce Options: <span class="required">*</span></td>
			 				<td><?php 
			 					$sauces = $misc->sauces();
			 					if($p['Size']['extra_sauce'] > 0) {
			 						$sauces['Extra Sauce'] = 'Extra Sauce (+$'.number_format($p['Size']['extra_sauce'], 2).')';
			 					}
			 					echo $form->select('CombosPizza.'.$p['CombosPizza']['id'].'.'.$x.'.sauce', $sauces, 'Regular Sauce', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false);
			 					?>
			 				</td>
			 			</tr>
			 			<tr>
			 				<td class="label-req">Cheese Type: <span class="required">*</span></td>
			 				<td><?php 
								$cheeses = $misc->cheeses();
			 					if($p['Size']['extra_cheese'] > 0) {
			 						$cheeses['Extra Cheese'] = 'Extra Cheese (+$'.number_format($p['Size']['extra_cheese'], 2).')';
			 					}
			 					echo $form->select('CombosPizza.'.$p['CombosPizza']['id'].'.'.$x.'.cheese', $cheeses, 'Regular Cheese', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); 
			 				?></td>
			 			</tr>
						<tr>
							<td class="label-req">Additional Instructions:</td>
							<td><?=$form->textarea('CombosPizza.'.$p['CombosPizza']['id'].'.'.$x.'.comments', 
												   array('class'=>'form-text ui-state-default ui-corner-all','label'=>''))?></td>
						</tr>
			 		</table>
				</fieldset><?php 
			endfor;
		endforeach;
	}
	
	//are there any specialty pizzas in this combo to display
	if(sizeof($combo['CombosSpecialsSize']) > 0) {
		foreach($combo['CombosSpecialsSize'] as $p):
			for($x = 1; $x <= $p['CombosSpecialsSize']['qty']; $x++): ?>
				<fieldset class="marg-b10"><?php 
					if(!empty($p['CombosSpecialsSize']['specials_size_id'])) {
						//combo includes a specific specialty pizza
						?><legend><?php 
						echo $p['Size']['size'].'" '.$p['Size']['title'].' '.$p['Special']['title'].' Specialty Pizza';
						if($p['CombosSpecialsSize']['qty'] > 1) echo ' #'.$x; 
						?></legend><?php
						echo $form->hidden('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.specials_size_id', 
										   array('value' => $p['CombosSpecialsSize']['specials_size_id']));
					} else {
						//combo includes a specialty pizza selection
						?><legend><?php 
						echo $p['Size']['size'].'" '.$p['Size']['title'].' Specialty Pizza of Choice'; 
						if($p['CombosSpecialsSize']['qty'] > 1) echo ' #'.$x;
						?></legend><?php
					}
					?>
					<table class="form-table">
						<?php if(empty($p['CombosSpecialsSize']['specials_size_id'])) { ?>
							<tr><td class="label-req" style="width:200px;">Choose Specialty Pizza: <span class="required">*</span></td>
								<td><?php echo $form->select('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.specials_size_id', $p['SpecialsSize'], null, array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
							</tr>
						<?php } ?>
			 			<tr>
			 				<td class="label-req" style="width:200px;">Crust Type: <span class="required">*</span></td>
			 				<td><?php echo $form->select('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.locations_crust_id', $crusts, null, array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
			 			</tr>
			 			<tr>
			 				<td class="label-req">Sauce Options: <span class="required">*</span></td>
			 				<td><?php 
			 					$sauces = $misc->sauces();
			 					if($p['Size']['extra_sauce'] > 0) {
			 						$sauces['Extra Sauce'] = 'Extra Sauce (+$'.number_format($p['Size']['extra_sauce'], 2).')';
			 					}
			 					echo $form->select('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.sauce', $sauces, 'Regular Sauce', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false);
			 					?>
			 				</td>
			 			</tr>
			 			<tr>
			 				<td class="label-req">Cheese Type: <span class="required">*</span></td>
			 				<td><?php 
								$cheeses = $misc->cheeses();
			 					if($p['Size']['extra_cheese'] > 0) {
			 						$cheeses['Extra Cheese'] = 'Extra Cheese (+$'.number_format($p['Size']['extra_cheese'], 2).')';
			 					}
			 					echo $form->select('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.cheese', $cheeses, 'Regular Cheese', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); 
			 				?></td>
			 			</tr>
						<tr>
							<td class="label-req">Additional Instructions:</td>
							<td><?=$form->textarea('CombosSpecialsSize.'.$p['CombosSpecialsSize']['id'].'.'.$x.'.comments', 
												   array('class'=>'form-text ui-state-default ui-corner-all','label'=>''))?></td>
						</tr>
			 		</table>
				</fieldset><?php 
			endfor;
		endforeach;
	}
	
	//are there any other menu items in this combo to display
	if(sizeof($combo['Item']) > 0) {
		$add_group_id = null;
		foreach($combo['Item'] as $index => $i):
			if($i['CombosItem']['add_type'] == 'Single') {
				for($x = 1; $x <= $i['CombosItem']['qty']; $x++):
					?>
					<fieldset class="marg-b10"><legend class="">(#<?php echo $x; ?>) <?php echo $i['Item']['Item']['title']; ?> Details</legend>
						<div class="ui-helper-clearfix marg-b10 marg-t10">
							<?php if($combo['Location']['show_menu_images'] == '1') { ?>
								<div class="left txt-r pad-r20" style="width:150px;">
									<?
									echo $html->image($misc->calcImagePath($i['Item']['Item']['image'], 'files/'), 
													  array('class'=>'gray-border pad2', 'style'=>'width:120px;'));
									?>
								</div>
								<div class="left pad-l5" style="width:350px;">
							<?php } else { ?>
								<div class="left pad-l20 pad-r20" style="width:500px;">	
							<?php } ?>
							
							<div class="green marg-b10 lg">
								<?php echo $i['Item']['Cat']['title']; ?> > <span class="b"><?php echo $i['Item']['Item']['title']; ?></span>
							</div>
							<?php if(!empty($i['Item']['Item']['description'])) { ?>
								<div class="orange marg-b10"><?php echo $i['Item']['Item']['description']; ?></div>
							<?php } ?>
							
							<?php echo $this->element('advertisers/item_opts', array('ajax_item'      => $i['Item'],
																					 'array_index'    => $x,
																					 'combos_item_id' => $i['CombosItem']['id'])); ?>
							<?php echo $form->hidden('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.item_id', 
													 array('value' => $i['Item']['Item']['id'])); ?>
							<div class="ui-helper-clearfix">
								<div class="left txt-r label-req pad-r5" style="width:180px;">Additional Instructions:</div>
								<div class="left pad-l5">
									<?php echo $form->textarea('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.comments', 
											   				array('class'=>'form-text ui-state-default ui-corner-all','label'=>'')); ?>
								</div>
							</div>
							
							</div>
						</div>
					</fieldset>
					<?php
				endfor; 
			} else if($i['CombosItem']['add_type'] == 'Cat') { ?>
				<fieldset class="marg-b10">
				 	<legend><?php echo '('.$i['CombosItem']['add_group_limit'].") ".$i['Cat']['title']." of Choice"; ?></legend>
				 	<?php for($x = 1; $x <= $i['CombosItem']['add_group_limit']; $x++): ?>
					 	<div class="cat-item-container">
					 		<div class="ui-helper-clearfix marg-b5">
					 			<div class="left txt-r label-req pad-r5" style="width:200px;">Choice #<?php echo $x; ?>:</div>
					 			<div class="left">
					 				<?php echo $form->select('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.item_id', 
														    $i['SeriesList'], null, 
														    array('class'=>'form-text ui-state-default ui-corner-all cat-item-select','label'=>''), 'Make Your Selection'); ?>
					 			</div>
					 			<div class="ajax-item-loader" style="display:none;"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style' => 'height:25px;')); ?></div>
					 		</div>
					 		
					 		<div class="item-opts-div" id="<?php echo 'DivCombosItem_'.$i['CombosItem']['id'].'_'.$x; ?>" style="margin-left:15px;"></div>
					 		
					 		<div class="ui-helper-clearfix marg-b5">
					 			<div class="left txt-r label-req pad-r5" style="width:200px;">Additional Instructions:</div>
					 			<div class="left">
					 				<?=$form->textarea('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.comments', 
													   array('class'=>'form-text ui-state-default ui-corner-all','label'=>'')); ?>
					 			</div>
					 		</div>
					 	</div>			 		
				 	<?php endfor; ?>
				</fieldset><?php 
			} else {
				//type is a choice series
				if($add_group_id != $i['CombosItem']['add_group_id']) {
					//Series has  been displayed already
					?><fieldset class="marg-b10"><?php
					$text = ($i['CombosItem']['add_group_limit'] > 1) ? 
							"Choice of (".$i['CombosItem']['add_group_limit'].") ".$i['Cat']['title'] : $i['Cat']['title']." Choice";
					?><legend><?php echo $text; ?></legend>
					
					<?php for($x = 1; $x <= $i['CombosItem']['add_group_limit']; $x++): ?>
						<div class="cat-item-container">
					 		<div class="ui-helper-clearfix marg-b5">
					 			<div class="left txt-r label-req pad-r5" style="width:200px;">Choice #<?php echo $x; ?>:</div>
					 			<div class="left">
					 				<?php echo $form->select('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.item_id', 
														    $i['SeriesList'], null, 
														    array('class'=>'form-text ui-state-default ui-corner-all cat-item-select','label'=>''), 'Make Your Selection'); ?>
					 			</div>
					 			<div class="ajax-item-loader" style="display:none;"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style' => 'height:25px;')); ?></div>
					 		</div>
					 		
					 		<div class="item-opts-div" id="<?php echo 'DivCombosItem_'.$i['CombosItem']['id'].'_'.$x; ?>" style="margin-left:15px;"></div>
					 		
					 		<div class="ui-helper-clearfix marg-b5">
					 			<div class="left txt-r label-req pad-r5" style="width:200px;">Additional Instructions:</div>
					 			<div class="left">
					 				<?=$form->textarea('CombosItem.'.$i['CombosItem']['id'].'.'.$x.'.comments', 
													   array('class'=>'form-text ui-state-default ui-corner-all','label'=>'')); ?>
					 			</div>
					 		</div>
					 	</div><?php
					endfor; 
					?></fieldset><?php
				} 
				$add_group_id = $i['CombosItem']['add_group_id'];
			}
		endforeach;
	}
	
	//any text items specific to this combo?
	if(sizeof($combo['CombosTextItem']) > 0) {
		foreach($combo['CombosTextItem'] as $i):
			?><fieldset class="marg-b10">
				<legend>(<?php echo $i['CombosTextItem']['qty']; ?>) <?php echo $i['CombosTextItem']['text']?></legend>
				<div class="ui-helper-clearfix marg-b5">
		 			<div class="left txt-r label-req pad-r5" style="width:200px;">Additional Instructions:</div>
		 			<div class="left">
		 				<?=$form->textarea('CombosTextItem.'.$i['CombosTextItem']['id'].'.comments', 
										   array('class'=>'form-text ui-state-default ui-corner-all','label'=>'')); ?>
		 			</div>
		 		</div>
			</fieldset><?php 
		endforeach;
	}
	
	
	?>
	<div class="ui-helper-clearfix">
		<div class="right">
			<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
			<div class="submitBtn"><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Add Combo To Order', null, 'btnAddToOrderCombo'); ?></div>
		</div>
	</div>
	</form>
</div>
<div class="topping-hr"></div>
<?php //echo debug($this->data); ?>
<?php echo debug($combo); ?>