<?php echo $jquery->scrFormSubmit('#btnMenuEditCrust', '#formMenuEditCrust'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>
<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Update Crust Type</legend>
	<p>Update your crust information below, and press the "Update" button.</p>
	
	<?php echo $form->create('LocationsCrust', array('id'=>'formMenuEditCrust', 'action'=>'edit')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$this->data['LocationsCrust']['location_id'])); ?>
	<?php echo $form->input('id'); ?>
	<table class="form-table">
		<tr>
			<td class="label-req">Crust Type: <span class="required">*</span></td>
			<td><?php echo $form->select('crust_id', $crust_list, null, array('class'=>'form-text', 'label'=>false, 'disabled'=>'disabled', 'div'=>false), false); ?></td>
		</tr>
		<tr>
			<td class="label-req">Price:</td>
			<td><?php echo $form->input('price', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
			<span class="base orange">(Leave empty for no charge.)</span></td>
		</tr>
		<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Crust', null, null, 'btnMenuEditCrust', '115'); ?></td></tr>
		</form>
	</table>
</fieldset
>