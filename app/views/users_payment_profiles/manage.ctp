<script>
	$(function() {
		var opts = { autoHeight : false, 
				 	 active     : <?php echo (sizeof($profiles) > 0) ? '1' : '0'; ?> };
		$('#UserPayProfilesAccordion').accordion(opts);
	});
</script>
<?php echo $jquery->scrFormSubmit('#btnUserPayProfileAdd', '#formUserPayProfileAdd'); ?>
<div>
	<h1>Manage Payment Profiles</h1>
	<?php //if(isset($resp)) echo debug($resp); ?>
	<hr />
	<?php 
	if(sizeof($profiles) <= 0) {
		echo $this->element('error-msg-box', array('msg'=>'NOTE: You must add a Payment Profile to get started.'));
	}
	?>
	<div class="ui-widget ui-accordion" id="UserPayProfilesAccordion">
		<h2 class="ui-widget-header ui-accordion-header"><a href="#">Add New Payment Profile</a></h2>
		<div class="ui-widget-content ui-accordion-content">
			<p>Use the following form below to add a payment profile to your account. you may set up as many profiles as you would like.</p>   
			<?=$form->create('UsersPaymentProfile', array('action'=>'manage', 'id'=>'formUserPayProfileAdd'))?>
			<?=$form->hidden('user_id', array('value'=>$session->read('User.id'))) ?>
			<fieldset class="marg-b10">
				<legend>Profile Details</legend>
				<table class="form-table">
					<tr>
						<td class="label-req" style="width:150px;">Give this profile a name: <span class="required">*</span></td>
						<td><?=$form->input('name', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
						<span class="base orange">(Ex. Company Visa, or Default Amex)</span></td>
					</tr>
					<tr>
						<td class="label-req"></td>
						<td class="b"><?=$form->checkbox('default')?>Mark this card as my default payment method?</td>
					</tr>
				</table>
			</fieldset>
		
			<fieldset class='marg-b10'>
				<legend>Credit Card Billing Details</legend>
				
				<div class="ui-helper-clearfix">
					<div class="left">
						<table class="form-table">
							<tr>
								<td class="label-req" style="width:150px;">First Name on card: <span class="required">*</span></td>
								<td><?=$form->input('fname', array('class'=>'form-text', 'label'=>''))?></td>
							</tr>
							<tr>
								<td class="label-req">Last Name on card: <span class="required">*</span></td>
								<td><?=$form->input('lname', array('class'=>'form-text', 'label'=>''))?></td>
							</tr>
							<tr>
					 			<td class="label-req">Address: <span class="required">*</span></td>
								<td><?=$form->input('address', array('label'=>'','class'=>'form-text'))?></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">City: <span class="required">*</span></td>
					 			<td><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">State: <span class="required">*</span></td>
					 			<td><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-text'), false)?></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">Zip: <span class="required">*</span></td>
					 			<td><?=$form->input('zip', array('label'=>'','class'=>'form-text'))?></td>
					 		</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
								<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>   
								</td>
							</tr>
							<tr>
								<td class="label-req">Card Number: <span class="required">*</span></td>
								<td><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>''))?></td>
							</tr>
							<tr>
								<td class="label-req">Expiration Month: <span class="required">*</span></td>
								<td><?=$form->month('month', null, array('class'=>'', 'style'=>'width:100px;'), false)?></td>
							</tr>
							<tr>
								<td class="label-req">Expiration Year: <span class="required">*</span></td>
								<td><?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select'), false)?></td>
							</tr>
						</table>
					</div>
					<div class="right">
						<div class="marg-b10"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
						<div style="padding-left:25px;"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
					</div>
				</div>
			</fieldset>
			
			<div class="ui-helper-clearfix">
				<div class="right"><?php echo $jquery->btn('#', 'ui-icon-suitcase', 'Submit', null, null, 'btnUserPayProfileAdd', '80'); ?></div>
			</div>
			<?php echo $form->end(); ?>
		</div>
		
		
		<?php if(sizeof($profiles) > 0) { ?>
			<?php foreach($profiles as $profile): ?>
				<h2 class="ui-widget-header ui-accordion-header"><a href="#"><?=$profile['UsersPaymentProfile']['name'] ?></a></h2>
				<div class="ui-widget-content ui-accordion-content">
					<div class="ui-helper-clearfix">
						<div class="left" style="width:200px;">
							<div class="italic marg-b5"><?php echo $profile['UsersPaymentProfile']['card_suffix']; ?></div>
							<p><span class="b">Billing Info:</span><br />
							<?php echo $profile['UsersPaymentProfile']['fname']." ".$profile['UsersPaymentProfile']['lname']; ?><br />
							<?php echo $profile['UsersPaymentProfile']['address']?><br />
							<?php echo $profile['UsersPaymentProfile']['city'].', '.$profile['UsersPaymentProfile']['state'].' '.$profile['UsersPaymentProfile']['zip']; ?></p>
						</div>
						<div class="left" style="width:200px;">
							<div class="b">Profile Added:</div>
							<div><?php echo $time->nice($profile['UsersPaymentProfile']['timestamp']); ?></div>
						</div>
						<div class="right valign-t"><?php 
							if($profile['UsersPaymentProfile']['default'] == '1') { 
								echo $this->element('warning-msg-box', array('msg'=>'Default Pay Profile'));
							} else {
								echo $jquery->btn('/users_payment_profiles/remove/'.$profile['UsersPaymentProfile']['id'], 'ui-icon-circle-close', 'Remove Pay Profile', 'Are you sure you would like to completely remove this payment profile?');
								echo $jquery->btn('/users_payment_profiles/set_default/'.$profile['UsersPaymentProfile']['id'], 'ui-icon-check', 'Set As Default');	
							}
							?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		<?php } ?>
	</div>
 </div>
 
 <?php //echo debug($profiles); ?>