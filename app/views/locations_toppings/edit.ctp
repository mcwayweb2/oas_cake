<?php echo $jquery->scrFormSubmit('#btnMenuEditTopping', '#formMenuEditTopping'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>
<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Update Topping</legend>
	<p>Update your topping information below, and press the "Update" button.</p>
	
	<?php echo $form->create('LocationsTopping', array('id'=>'formMenuEditTopping', 'action'=>'edit')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$this->data['LocationsTopping']['location_id'])); ?>
	<?php echo $form->input('id'); ?>
	<table class="form-table">
		<tr>
			<td class="label-req">Topping: <span class="required">*</span></td>
			<td><?php echo $form->select('topping_id', $topping_list, null, array('class'=>'form-text', 'label'=>false, 'disabled'=>'disabled', 'div'=>false), false); ?></td>
		</tr>
		<tr>
			<td class="label-req">Label: <span class="required">*</span></td>
			<td><?php echo $form->select('topping_type_id', $topping_types, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?><br />
			<span class="base orange">(Used to group your toppings.)</span></td>
		</tr>
		<tr>
			<td class="label-req">Price Alter:</td>
			<td><?php echo $form->input('price_alter', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
			<span class="base orange">(Leave empty for no price change.)</span></td>
		</tr>
		<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Topping', null, null, 'btnMenuEditTopping', '130'); ?></td></tr>
		</form>
	</table>
</fieldset>