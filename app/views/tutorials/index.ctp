<?php echo $jquery->scrFormSubmit('#btnTutorialAdminIndex', '#formTutorialAdminIndex'); ?>
<script>
$(function() {
	$('#tutsAccordion').accordion({
		autoHeight: false
	});
	
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="pad-l5"><h1>Help Tutorials</h1></div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<div class="ui-widget ui-accordion" id="tutsAccordion">
		<?php foreach($tutorials as $title => $tuts): ?>
		<?php if(sizeof($tuts) > 0) { $i = 0; ?>
			<h2 class="ui-widget-header ui-accordion-header" id="tut-header-<?php echo $tuts[0]['Tutorial']['type_id']; ?>">
         		<a href="#"><?php echo $title?></a>
         	</h2>
         	<div class="ui-widget-content ui-accordion-content" id="tut-drawer-<?php echo $tuts[0]['Tutorial']['type_id']; ?>">
        		<ul class="list-tomato">
        		 	<?php foreach($tuts as $t): 
						$class = ($i++ % 2 == 0) ? "alt-row" : "";
						?>
						<li><?php echo $html->link($t['Tutorial']['title'], '/tutorials/view/'.$t['Tutorial']['slug']); ?></li>

					<?php endforeach; ?>
					<?php //echo debug($tuts)?>
				</ul>
			</div>
		<?php } ?>
		<?php endforeach; ?>
	</div>
</div>

<?php //echo debug($tutorials);?>
