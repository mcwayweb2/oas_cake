<?php echo $jquery->scrFormSubmit('#btnTutorialAdminIndex', '#formTutorialAdminIndex'); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="pad-l5"><h1>Tutorial Manager</h1></div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Tutorial', array('action' => 'admin_index', 'id' => 'formTutorialAdminIndex')); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b5"><legend class="ui-corner-all">Add New Tutorial</legend>
		<div class="ui-helper-clearfix">
			<div class="left pad-r20">
				<div class="b base">Title:</div>
				<div><?php echo $form->input('title', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false)); ?></div>
			</div>
			
			<div class="left pad-r20">
				<div class="b base">Tutorial Category:</div>
				<div><?php echo $form->select('type_id', $types, null, array(), false); ?></div>
			</div>
			
			<div class="left" style="padding-top: 8px;"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Tutorial', null, null, 'btnTutorialAdminIndex'); ?></div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">gDoc Url:</div>
				<div><?php echo $form->input('url', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false, 'style' => 'width:390px;')); ?></div>
			</div>
		</div>
	</fieldset>
	</form>
	<hr />
	
	<?php if(sizeof($tutorials) > 0) { $i = 0; ?>
		<div class="ui-widget-header ui-corner-top">
			<div class="ui-helper-clearfix pad2 b">
				<div class="left pad-l5" style="width:280px;padding-left:45px;">Title</div>
				<div class="left" style="width:145px;">Category</div>
			</div>
		</div>
		<div class="ui-widget-content ui-corner-bottom">
		<?php foreach($tutorials as $t): 
			$class = ($i++ % 2 == 0) ? "alt-row" : "";
			?>
			<div class="ui-helper-clearfix marg-b5 pad5 <?php echo $class; ?>">
				<?php echo $this->element('js/jquery/drag-handle'); ?>
				<div class="left pad-l10" style="width:280px;">
					<div class="b"><?php echo $t['Tutorial']['title']; ?></div>
				</div>
				
				<div class="left" style="width:145px;">
					<?php echo $t['TutorialsType']['title']; ?>
				</div>
				
				<div class="right">
					<?php echo $this->element('btn-toolbar', array('btns' => array(array('/admin/tutorials/view/'.$t['Tutorial']['slug'], '22-preview-file.png', 'View Tutorial'),
																				   array('/admin/tutorials/delete/'.$t['Tutorial']['id'], '22-delete-x.png', 'Delete Tutorial Record', 'Are you sure?')))); ?>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php } else { ?>
		<a class="b red">There are no tutorials yet in this category.</p>
	<?php } ?>
</div>


<?php //echo debug($tutorials); ?>