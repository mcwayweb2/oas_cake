<div class="specials index">
<h2><?php __('Specials');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('location_id');?></th>
	<th><?php echo $paginator->sort('title');?></th>
	<th><?php echo $paginator->sort('subtext');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($specials as $special):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $special['Special']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($special['Location']['name'], array('controller'=> 'locations', 'action'=>'view', $special['Location']['id'])); ?>
		</td>
		<td>
			<?php echo $special['Special']['title']; ?>
		</td>
		<td>
			<?php echo $special['Special']['subtext']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $special['Special']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $special['Special']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $special['Special']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $special['Special']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<?php '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Special', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Locations', true), array('controller'=> 'locations', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Location', true), array('controller'=> 'locations', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Sizes', true), array('controller'=> 'sizes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Size', true), array('controller'=> 'sizes', 'action'=>'add')); ?> </li>
	</ul>
</div>
