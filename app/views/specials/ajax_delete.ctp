<?php
if(sizeof($specials) > 0) {
	?>
	<table width="100%">
		<tr>
			<th></th>
			<th>Title</th>
			<?php
			foreach ($sizes as $size):
				echo "<th>".$size['Size']['title']."</th>";
			endforeach;
			?>
			<th>Options</th>
		</tr>
	<?php
	foreach($specials as $special):
		?>
		<tr>
			<td><?=$html->image('../files/specialtyPizzas/'.$special['Special']['image'], array('width'=>80))?></td>
			<td><?=$special['Special']['title']?></td>
			<?php
			foreach($special['Size'] as $size):
				echo "<td>$".number_format($size['SpecialsSize']['price'], 2)."</td>";
			endforeach;
			?>
			<td>
				<?=$html->link('Edit', '/specials/edit/'.$special['Special']['id'])?> | 
				<?=$ajax->link('Delete', '/specials/delete/'.$special['Special']['id'], array('update'=>'specials_div', null, false))?>
			</td>
		</tr>
		<?php
	endforeach;	
	echo "</table>";
} else {
	?>
	<p>You have not added any specialty pizzas yet.</p>
	<?php
}
?>