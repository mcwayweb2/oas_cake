<?=$html->script(array('prototype', 'scriptaculous'), false)?>
<div>
	<h1>Manage Specialty Pizzas</h1><hr />
	<p>Here you can manage the specialty pizzas that your restaurants offer. List the ingredients that come on that specialty pizza in the subtext field. 
	If you have multiple locations, choose whether your locations all offer the same specialty pizzas, or if you would 
	like to manage each locations available specialty pizzas individually. You can update this in your <?=$html->link('Account Settings', '/advertisers/edit')?>.</p>
	
	<fieldset>
		<legend>Add Specialty Pizza</legend>
		<?=$form->create('Special', array('action'=>'manage', 'type'=>'file'))?>
		<?=$form->hidden('loc_id', array('value'=>$loc_id));?>
		<table width="100%">
			<tr>
				<td width="40%">Title: *</td>
				<td width="60%"><?=$form->input('title', array('label'=>'', 'class'=>'form-text'))?></td>
			</tr>
			<tr>
				<td valign="top">Subtext: <span style="font-size:80%;">(e.g. list ingredients)</span> *</td>
				<td><?=$form->input('subtext', array('label'=>'', 'class'=>'form-text', 'type' => 'textarea'))?></td>
			</tr>
			<tr>
				<td>Image: *</td>
				<td><?=$form->input('image', array('label'=>'', 'class'=>'form-text', 'type'=>'file'))?></td>
			</tr>
		</table>
		<hr />
		<table width="100%">
			<tr>
				<td colspan="2"><h3>Prices</h3></td>
			</tr>
		<?php
		foreach ($sizes as $size):
			?>
			<tr>
				<td width="40%"><?=$size['Size']['title']?>:</td>
				<td width="60%">
					<?php
					$fieldname = 'Price.'.$size['Size']['size'];
					?>
					<?=$form->input($fieldname, array('label'=>'', 'class'=>'form-text'))?>
				</td>
			</tr>
			<?php
		endforeach;
		?>
		</table>
	</fieldset>
	<br />
	<div class="clearfix">
		<div class="left"><h1>Your Specialty Pizzas</h1></div>
		<div class="right"><?php echo $form->end('Add');?></div>
	</div>
	<div class="clearfix"></div>
	<br />
	<div id="specials_div">
	<?php
	if(sizeof($specials) > 0) {
		?>
		<table width="100%">
			<tr>
				<th></th>
				<th>Title</th>
				<?php
				foreach ($sizes as $size):
					echo "<th>".$size['Size']['title']."</th>";
				endforeach;
				?>
				<th>Options</th>
			</tr>
		<?php
		foreach($specials as $special):
			?>
			<tr>
				<td><?=$html->image('../files/specialtyPizzas/'.$special['Special']['image'], array('width'=>80))?></td>
				<td><?=$special['Special']['title']?></td>
				<?php
				foreach($special['Size'] as $size):
					echo "<td>$".number_format($size['SpecialsSize']['price'], 2)."</td>";
				endforeach;
				?>
				<td>
					<?php //echo $html->link('Edit', '/specials/edit/'.$special['Special']['id']); ?>
					<?=$ajax->link('Delete', '/specials/delete/'.$special['Special']['id'], array('update'=>'specials_div', null, false), 'Are you sure you want to delete this special?')?>
				</td>
			</tr>
			<?php
		endforeach;	
		echo "</table>";
	} else {
		?>
		<p>You have not added any specialty pizzas yet.</p>
		<?php
	}
	?>
	</div>
</div>