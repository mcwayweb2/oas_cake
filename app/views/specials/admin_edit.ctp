<div class="specials form">
<?php echo $form->create('Special');?>
	<fieldset>
 		<legend><?php __('Edit Special');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('location_id');
		echo $form->input('title');
		echo $form->input('subtext');
		echo $form->input('Size');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Special.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Special.id'))); ?></li>
		<li><?php echo $html->link(__('List Specials', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Locations', true), array('controller'=> 'locations', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Location', true), array('controller'=> 'locations', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Sizes', true), array('controller'=> 'sizes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Size', true), array('controller'=> 'sizes', 'action'=>'add')); ?> </li>
	</ul>
</div>
