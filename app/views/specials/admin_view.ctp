<div class="specials view">
<h2><?php  __('Special');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $special['Special']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Location'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($special['Location']['name'], array('controller'=> 'locations', 'action'=>'view', $special['Location']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $special['Special']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Subtext'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $special['Special']['subtext']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Special', true), array('action'=>'edit', $special['Special']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Special', true), array('action'=>'delete', $special['Special']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $special['Special']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Specials', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Special', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Locations', true), array('controller'=> 'locations', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Location', true), array('controller'=> 'locations', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Sizes', true), array('controller'=> 'sizes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Size', true), array('controller'=> 'sizes', 'action'=>'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Sizes');?></h3>
	<?php if (!empty($special['Size'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Location Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Size'); ?></th>
		<th><?php __('Base Price'); ?></th>
		<th><?php __('Extra Toppings'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($special['Size'] as $size):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $size['id'];?></td>
			<td><?php echo $size['location_id'];?></td>
			<td><?php echo $size['title'];?></td>
			<td><?php echo $size['size'];?></td>
			<td><?php echo $size['base_price'];?></td>
			<td><?php echo $size['extra_toppings'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'sizes', 'action'=>'view', $size['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'sizes', 'action'=>'edit', $size['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'sizes', 'action'=>'delete', $size['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $size['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Size', true), array('controller'=> 'sizes', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
