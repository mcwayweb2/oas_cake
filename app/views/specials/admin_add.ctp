<div class="specials form">
<?php echo $form->create('Special');?>
	<fieldset>
 		<legend><?php __('Add Special');?></legend>
	<?php
		echo $form->input('location_id');
		echo $form->input('title');
		echo $form->input('subtext');
		echo $form->input('Size');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Specials', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Locations', true), array('controller'=> 'locations', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Location', true), array('controller'=> 'locations', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Sizes', true), array('controller'=> 'sizes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Size', true), array('controller'=> 'sizes', 'action'=>'add')); ?> </li>
	</ul>
</div>
