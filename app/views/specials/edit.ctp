<?php echo $jquery->scrFormSubmit('#btnEditSpecialtyPizza', '#formEditSpecialtyPizza'); ?>
<script>
$(function() { 
	$('#SpecialImage').uniform();
});

</script>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Edit Specialty Pizza</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/locations/edit_menu/'.$this->data['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to Menu Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?=$form->create('Special', array('action' => 'edit', 'id'=>'formEditSpecialtyPizza', 'type'=>'file'))?>
	<?php echo $form->input('id'); ?>
	<a class="b green">Enter the information for your specialty pizza below</p>
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Special Details</legend>
		
		<div class="ui-helper-clearfix">
			<div class="left" style="width:220px;">
				<?php
				echo $html->image($misc->calcImagePath($special['Special']['image'], 'files/specialtyPizzas/'), 
								  array('class'=>'gray-border', 'style'=>'width:200px;'));
				?>
			</div>
			<div class="left">
				<table class="form-table" style="width:100%;">
					<tr>
						<td class="label-req">Title: <span class="required">*</span></td>
						<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false))?></td>
					</tr>
					<tr>
						<td class="label-req">Description:</td>
						<td><?=$form->input('subtext',array("class"=>"form-text", 'label'=>false, 'div'=>false, 'rows' => '3'))?><br />
						<span class="base orange">(List the toppings, etc.)</span></td>
					</tr>
					<tr>
						<td class="label-req">Pizza Photo:</td>
						<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false))?></td>
					</tr>
				</table>
			</div>
		</div>
	</fieldset>
	
	<fieldset class="ui-widget-content ui-corner-all marg-b10">
		<legend class="ui-corner-all">Select Sizes</legend>
		<a class="b green">Check the sizes that you would like to associate with this specialty pizza. If you select a size, and do not enter a 
		price, the base price you have set up for that size will be used.</p>
		<?php foreach($sizes as $size): ?>
			<div class="ui-helper-clearfix marg-b5">
				<div class="left" style="width:150px;">
					<?php $opts = (array_key_exists($size['Size']['id'], $current_sizes)) ? array('checked'=>'checked') : array(); ?>
					<?php echo $form->checkbox('sizes.'.$size['Size']['id'], $opts); ?>
					<span class="pad-l10"><?php echo $size['Size']['size']?>" <?php echo $size['Size']['title']; ?></span>
				</div>
				<div class="left">
					<?php $value = (array_key_exists($size['Size']['id'], $current_sizes)) ? $current_sizes[$size['Size']['id']] : null; ?>
					<?php echo $form->input('prices.'.$size['Size']['id'], array('class'=>'form-text', 'div'=>false, 'label'=>false, 
																				 'value'=>number_format($value, 2))); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</fieldset>
	
	<?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Specialty Pizza', null, null, 'btnEditSpecialtyPizza', '165'); ?>
	</form>
</div>