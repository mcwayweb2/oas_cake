<?php 
	$js_block = "
	$(function() {
		$('#btnUpdateUserInfo').click(function() {
			$('#formUpdateUserInfo').submit();	
		});
		$('#btnUpdateUserPw').click(function() {
			$('#formUpdateUserPw').submit();	
		});
	});";
	echo $html->scriptBlock($js_block, array('inline'=> false));
?>
<div class="ui-widget ui-corner-all">
	<div class="ui-widget ui-widget-header ui-corner-top pad5">
		<h1 class="pad-l5">Manage Admin Profile</h1>
	</div>
	<div class="ui-widget-content ui-corner-bottom">
		<?php echo $form->create('User', array('id'=>'formUpdateUserInfo'));?>
		<fieldset class="marg-t10">
		 	<legend><?php __('Account Settings');?></legend>
		 	<table class="form-table">
		 		<tr>
		 			<td class="label-req" style="width:100px;">First Name: <span class="required">*</span></td>
		 			<td><?=$form->input('fname',array('class'=>'form-text','label'=>''))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Last Name: <span class="required">*</span></td>
		 			<td><?=$form->input('lname',array('class'=>'form-text','label'=>''))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Phone: <span class="required">*</span></td>
		 			<td><?=$form->input('phone', array('class'=>'form-text','label'=>''));?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Email: <span class="required">*</span></td>
		 			<td><?=$form->input('email', array('class'=>'form-text','label'=>''));?></td>
		 		</tr>
		 	</table>
		</fieldset>
		
		<div class="ui-helper-clearfix marg-t10">
			<div class="right"><?php echo $misc->jqueryBtn('#', 'ui-icon-pencil', 'Update Profile', null, null, 'btnUpdateUserInfo'); ?></div>
		</div>
		</form>
		
		<?=$form->create('User', array("action"=>"updatepw", 'id'=>'formUpdateUserPw'));?>
		<fieldset class="marg-b10">
		 	<legend><?php __('Update Password');?></legend>
		 	<table class="form-table">
		 		<tr>
		 			<td class="label-req" style="width:100px;">New Password: <span class="required">*</span></td>
		 			<td><?=$form->input('pass1', array('class' => 'form-text','label'=>'', 'type' => 'password'))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Confirm: <span class="required">*</span></td>
		 			<td><?=$form->input('pass2', array('class' => 'form-text','label'=>'', 'type' => 'password'))?></td>
		 		</tr>
		 	</table>
		</fieldset>
		<div class="ui-helper-clearfix marg-t10">
			<div class="right"><?php echo $misc->jqueryBtn('#', 'ui-icon-pencil', 'Update Password', null, null, 'btnUpdateUserPw'); ?></div>
		</div>
		</form>
	</div>
</div>