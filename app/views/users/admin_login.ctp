<?php 
	$js_block = "
	$(function() {
		$('#btnUserLogin3').click(function() {
			$('#UserLoginForm3').submit();	
		});
	});";
	echo $html->scriptBlock($js_block, array('inline'=> false));
?>
<?php echo $form->create('User', array("action"=>"admin_login", 'id'=>'UserLoginForm3'));?>
<div class="ui-widget">
	<div class="ui-corner-top ui-widget-header pad5">Admin Login</div>
	<div class="ui-corner-bottom ui-widget-content">
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('username',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('password',array("class"=>"form-text"))?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo $misc->jqueryBtn('#', 'ui-icon-locked', 'Login', null, null, 'btnUserLogin3', '70'); ?></td>
			</tr>
		</table>
	</div>
</div>
</form>