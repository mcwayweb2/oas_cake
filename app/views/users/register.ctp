<?php echo $jquery->scrFormSubmit('#btnUserRegister', '#formUserRegister'); ?>
<script>
$(function() {
	$('.ajax-loader-sm-circle').hide();
	$('#UserUsername').blur(function() {
		$('.ajax-loader-sm-circle').show();
		 
		$('#ajaxUsernameContent').load('/users/ajax_check_username', { username: $(this).val() }, function() {
			$('.ajax-loader-sm-circle').hide();
		});
		return false;
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<h1>Register with Order a Slice</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
<p>Fill out the form below and your on your way to Ordering a Slice! Your account is free to create, and always free to use, so what are you waiting for?</p>
<div class="users form">
	<div class="clearfix">
		<div class="left" style="width:100%;">
			<?php echo $form->create('User', array('action'=>'register', 'id'=>'formUserRegister'));?>
			<fieldset>
		 		<legend><?php __('User Information');?></legend>
		 		<table width="100%" class="form-table">
		 			<tr>
		 				<td class="label-req">Desired Username: <span class="required">*</span></td>
		 				<td>
		 					<div class="ui-helper-clearfix">
		 						<div class="left"><?=$form->input('username', array('class'=>'form-text','label'=>''));?></div>
		 						<div class="left ajax-loader-sm-circle">
		 							<?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?>
		 						</div>
		 					</div>
		 					<div id="ajaxUsernameContent"></div>
						</td>						
		 			</tr>
		 			<tr>
		 				<td class="label-req" style="width:150px;">First Name: <span class="required">*</span></td>
		 				<td><?=$form->input('fname', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
					<tr>
		 				<td class="label-req">Last Name: <span class="required">*</span></td>
		 				<td><?=$form->input('lname', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Phone: <span class="required">*</span></td>
		 				<td><?=$form->input('phone', array('class'=>'form-text','label'=>'', 'div'=>false));?><br />
		 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Email: <span class="required">*</span></td>
		 				<td><?=$form->input('email', array('class'=>'form-text','label'=>''));?>
		 				<span class="basefont required" style="padding-left: 5px;">This is necessary to retrieve a lost password.</span></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Password: <span class="required">*</span></td>
		 				<td><?=$form->input('pass1', array('type'  => 'password','label'=>'','class'=>'form-text'));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Confirm Password: <span class="required">*</span></td>
		 				<td><?=$form->input('pass2', array('type'  => 'password','label'=>'','class'=>'form-text'));?></td>
		 			</tr>
		 			<tr>
		 				<td></td>
		 				<td>
		 					<div class="ui-helper-clearfix">
		 						<div class="left pad-r5"><?php echo $form->checkbox('newsletter', array('checked'=>'checked')); ?></div>
		 						<div class="left base b" style="width:180px;">Check here to receive special offers from us via email from time to time.</div>
		 					</div>
		 				</td>
		 			</tr>
		 			<tr>
		 				<td></td>
		 				<td><?php echo $jquery->btn('#', 'ui-icon-person', 'Register', null, 'txt-r', 'btnUserRegister', '90'); ?></td>
		 			</tr> 		
		 		</table>
			</fieldset>
		</div>
		<!--  
		<div class="left ui-corner-all pad3 gray-border" style="width:160px;height:600px;border: 1px solid;" >
			<?php //echo $html->image('pizza-ad-160x600-2.jpg'); ?>
		</div>-->
	</div>
	<div class="clear"></div>
	<?=$this->element('js/highlight_fields') ?>
</div>
</div>
