<?php 
	$js_block = "
	$(function() {
		$('#btnUserLogin2').click(function() {
			$('#UserLoginForm2').submit();	
		});
	});";
	echo $html->scriptBlock($js_block, array('inline'=> false));
	?>
<?php echo $form->create('User', array("action"=>"login", 'id'=>'UserLoginForm2'));?>
<div class="ui-widget">
	<div class="ui-corner-top ui-widget-header pad5">Customer Login</div>
	<div class="ui-corner-bottom ui-widget-content">
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('loginUsername',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('loginPassword',array("class"=>"form-text"))?></td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="ui-helper-clearfix basefont required">
						<div style="margin-left:130px;" class="left pad-r5"><?php echo $misc->jqueryBtn('#', 'ui-icon-locked', 'Login', null, null, 'btnUserLogin2'); ?></div>
						<div class="left pad-r5"><?php echo $misc->jqueryBtn('/users/register', 'ui-icon-pencil', 'Register'); ?></div>
						<div class="left"><?php echo $misc->jqueryBtn('/users/password_reset', 'ui-icon-unlocked', 'Forgot Password'); ?></div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>
</form>