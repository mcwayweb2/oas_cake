<?php echo $jquery->scrFormSubmit('#btnUserForgotPw', '#formUserForgotPw'); ?>
<div class="ui-widget-header pad5 ui-corner-top">Reset Customer Password</div>
<div id="userForgotPw" class="ui-widget-content ui-corner-bottom">
	
	<?=$form->create('User', array('action' => 'password_reset', 'id' => 'formUserForgotPw'))?>
	<p>Forgot your password? Use the form below to reset your password. An email will be sent to your registered email 
	address with your new password. <?php echo $html->link('Forget your username?', '/users/recover_username'); ?></p>
	<table class="form-table">
		<tr>
			<th class="label-req" style="width:100px;">Username: <span class="required">*</span></th>
			<td><?=$form->input('username', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<th class="label-req">Email: <span class="required">*</span></th>
			<td><?=$form->input('email', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr><td></td><td>
		<div style="width:135px;"><?php echo $misc->jqueryBtn('#', 'ui-icon-unlocked', 'Forgot Password', null, null, 'btnUserForgotPw'); ?></div></td></tr>
	</table>
</div> 