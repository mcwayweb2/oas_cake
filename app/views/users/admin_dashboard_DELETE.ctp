<script>
$(function() {
	$('#adminDashTabs').tabs();
});
</script>
<div id="adminDashTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a href="#pending-menus">Pending Menus <span class="red">(2)</span></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#customer-support">Pending Customer Support <span class="red">(2)</span></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#todays-orders">Today's Orders <span class="red">(2)</span></a>
		</li>
	</ul>
	<div id="pending-menus" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php echo debug($pending_menus); ?>
	</div>
	<div id="customer-support" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php echo debug($pending_support); ?>
	</div>
	<div id="todays-orders" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php echo debug($todays_orders); ?>
	</div>
</div>