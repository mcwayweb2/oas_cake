<div class="ui-widget-header ui-corner-top pad5 b lg">Order History for <?php echo $user['User']['fname']." ".$user['User']['lname']; ?></div>
<div class="ui-widget-content ui-corner-bottom"><?php
	foreach ($orders as $order): ?>
		<div class="ui-helper-clearfix marg-b5">
			<div class="left green b" style="width:200px;"><?php echo $order['Location']['name']; ?></div>
			<div class="left"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
			<div class="right"><?php echo $jquery->btn('/orders/details/'.$order['Order']['id'], 'ui-icon-script', 'Order Details'); ?></div>
		</div>
		<hr /><?php
	endforeach;
	?>
</div>