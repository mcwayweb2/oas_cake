<?php 
if(!$pay_profile_check) {
	echo $this->element('warning-msg-box', array('msg' => 'NOTE: Please setup a '.$html->link('Payment Profile', '/user-dash/payment_profiles').' in order to use credit cards for order payment.'));
}
?>

<div class="ui-widget ui-corner-all">
	<div id="userNav" class="ui-widget ui-widget-header ui-corner-top">
		<div class="clearfix">
			<div class="left"><?php echo $html->image('btn-my-orders-active.png'); ?></div>
			<div class="right basefont">
				<?php echo $html->link($html->image('btn-contact-profile.png', array('id' => 'btnContactProfile')), 
									'/user-dash/profile', array('escape'=>false)); ?> 
				<?php echo $html->link($html->image('btn-manage-addr.png', array('id' => 'btnManageAddr')), 
									'/user-dash/addresses', array('escape'=>false)); ?>  
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="ui-widget ui-widget-content ui-corner-bottom">
		<p>Welcome to your dashboard. View previous orders, edit account information, and manage your favorites.</p>
		<?php
		//echo debug($orders);
		if(sizeof($orders) > 0) {
			$i = 0;
			?>
			<div class="orange-border marg-b10">
			<table class="ui-table list-table">
				<tr>
					<th></th>
					<th><?php echo $paginator->sort('Location', 'Location.name');?></th>
					<th><?php echo $paginator->sort('Date', 'Order.timestamp');?></th>
					<th></th>
				</tr>
				<?php 
				echo $this->element('table_accent_row', array('cols' => '4'));
				foreach ($orders as $order):
				$class = ($i++ % 2 == 0) ? "class='alt-row'" : "";
				?>
				<tr class="pad20">
					<td <?php echo $class?> style="padding:2px 8px; width:50px;"><?php 
		    			if($order['Location']['use_logo_in_banner'] == '2' && !empty($order['Advertiser']['logo'])) {
		    				//attempt to display advertiser logo
		    				echo $html->link($html->image('../files/logos/'.$order['Advertiser']['logo'], 
		    										      array('style'=>'height:40px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$order['Location']['slug'], 
		    							 	 array('escape'=>false));
		    			} else if(!empty($order['Location']['logo'])) {
		    				//attempt to display location logo
		    				echo $html->link($html->image('../files/logos/'.$order['Location']['logo'], 
		    										  	  array('style'=>'height:40px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$order['Location']['slug'],
		    								 array('escape'=>false));
		    			} else { 
		    				//display order a slice logo, to make sure we have something displayed
		    				echo $html->link($html->image('orderaslice_logo_sm.png', 
		    										      array('style'=>'height:40px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$order['Location']['slug'], 
		    							 	 array('escape'=>false)); 
		    			}
		    		?>
					</td>
					<td <?php echo $class?>>
						<?=$html->link($order['Location']['name'], '/menu/'.$order['Advertiser']['slug'].'/'.$order['Location']['slug'])?>
					</td>
					<td <?php echo $class?>><?php 
						if($order['Order']['refunded'] == '1') {
							?><div class="base b red">Refunded:</div>
							<div><?php echo $time->nice($order['Order']['refunded_timestamp']); ?></div><?php 
						} else if($order['Order']['order_started'] == '1') {
							?><div class="base b green">Started:</div>
							<div><?php echo $time->nice($order['Order']['order_started_timestamp']); ?></div><?php 
						} else {
							?><div class="base b green">Placed:</div>
							<div><?php echo $time->nice($order['Order']['timestamp']); ?></div><?php
						}
					?></td>
					<td <?php echo $class?> style="text-align:right; width:120px; padding: 5px 8px 0px 0px;">
					<?php echo $misc->jqueryBtn('/orders/details/'.$order['Order']['id'], 'ui-icon-search', 'Order Details');?></td>
				</tr>
				<?php
				endforeach;	
			?></table></div><?php 
		} else {
			echo $this->element('warning-msg-box', array('msg'=>'You have not placed any orders yet.')); 
		}
		if(sizeof($orders) > 0) echo $this->element('pagination_links'); ?>
	</div>
	<?php //echo debug($my_favs); ?>
</div>
<script>
	$(function() {
		/*
		$('#btnManageAddr').hover(function() {
			$('#btnContactProfile').attr('src', '/img/btn-contact-profile-active.png'));	
		}, function() {
			$('#btnContactProfile').attr('src', '/img/btn-contact-profile.png'));
		});
		*/
	});
</script>