<?php echo $jquery->scrFormSubmit('#btnContactForm', '#formContactForm'); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left pad-l5"><h1>Contact Us</h1></div>
		<div class="right"><?php echo $misc->jqueryBtn('/request-a-restaurant', 'ui-icon-comment', 'Request a Restaurant'); ?></div>
	</div>
</div>

<div class="ui-widget-content ui-corner-bottom">
<p>Do you have a question or comment for us? Maybe a suggestion to improve the site...or a business with a question about signing up? Fill out 
our contact form below and we will be in contact with you shortly.</p>

<div class="contacts form">
	<div class="clearfix">
		<div class="left pad-r10" style="width:390px;">
			<?php echo $form->create('Contact', array('action'=>'contactus', 'id'=>'formContactForm'));?>
			<fieldset style="width:370px;">
		 		<legend>Contact Info</legend>
		 		<table width="100%" class="form-table">
		 			<tr>
		 				<td class="label-req">First Name: <span class="required">*</span></td>
		 				<td><?=$form->input('fname', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Last Name: <span class="required">*</span></td>
		 				<td><?=$form->input('lname', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<?php
					$types = array('Customer'=>'Restaurant Customer','Restaurant Owner'=>'Restaurant Owner');
		 			?>
					<tr>
		 				<td class="label-req">Are you a ... </td>
		 				<td><?=$form->radio('type', $types, array('class'=>'form-text','legend'=>false, 'separator'=>'<br />', 'value'=>'Customer'));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Email: <span class="required">*</span></td>
		 				<td><?=$form->input('email', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Phone:</td>
		 				<td><?=$form->input('phone', array('class'=>'form-text','label'=>'', 'div'=>false));?><br />
		 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">City:</td>
		 				<td><?=$form->input('city', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">State:</td>
		 				<td><?=$form->select('state_id', $states, null, array('class'=>'form-text','label'=>false), 'Select your State');?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req" valign="top">Questions/Comments: <span class="required">*</span></td>
		 				<td><?=$form->input('comments', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td></td>
		 				<td><?php echo $jquery->btn('#', 'ui-icon-mail-closed', 'Submit', null, 'txt-r', 'btnContactForm', '80'); ?></td>
		 			</tr>
		 		</table>
		 		</form>
			</fieldset>
		</div>
		<div class="left ui-corner-all pad10" style="width:160px;height:366px; border: 1px solid #DDD;" >
			<div class="marg-t5 green b">By Email:</div>
			<div class="base">customer-service@orderaslice.com</div>
			
			<div class="marg-t20 green b">By Snail Mail:</div>
			<div>Po Box 2762<br />La Mesa, CA 91943</div>
			
			<div class="marg-t20 green b">By Phone/Fax:</div>
			<div>(619) 46-SLICE<br />Fax: (619) 639-0337</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</div>