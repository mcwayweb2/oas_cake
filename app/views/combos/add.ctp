<?php echo $jquery->scrFormSubmit('#btnAddComboSpecial', '#formAddComboSpecial'); ?>
<script>
$(function() { 
	$('#ComboImage').uniform();
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Create New Combo Special</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/restaurants/edit_menu/'.$location_slug, 'ui-icon-circle-arrow-w', 'Back to Menu Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Use the Combo Builder below to build a new Combo Special. A Combo Special can be any combination of pizzas or items on your menu, which 
	you can then offer for a single price. Use these specials to offer more incentive to your customers to order multiple items from your restaurant.</p>
	
	<fieldset class="ui-widget-content ui-corner-all  marg-b10"><legend class="ui-corner-all">Combo Special Details</legend>
		<p>To start, enter the basic information for your new combo below:</p>
	
		<?php echo $form->create('Combo', array('action'=>'add', 'id'=>'formAddComboSpecial', 'type'=>'file')); ?>
		<?php echo $form->hidden('location_id', array('value'=>$location_id)); ?>
		<table class="form-table" style="width:100%;">
			<tr>
				<td class="label-req">Title: <span class="required">*</span></td>
				<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false))?></td>
			</tr>
			<tr>
				<td class="label-req">Description:</td>
				<td><?=$form->textarea('description',array("class"=>"form-text", 'label'=>false, 'div'=>false, 'rows'=>'3'))?><br />
				<span class="base orange">(List combo details here.)</span></td>
			</tr>
			<tr>
				<td class="label-req">Price: <span class="required">*</span></td>
				<td><?=$form->input('price',array("class"=>"form-text", 'label'=>false))?></td>
			</tr>
			<tr>
				<td class="label-req">Photo:</td>
				<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false))?></td>
			</tr>
			<tr><td></td>
				<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Create Combo', null, null, 'btnAddComboSpecial', '120')?></td>
			</tr>
		</table>
	</fieldset>
</div>