<?php echo $html->script(array('jquery/jeditable')); ?>
<script>
$(function() {
	$('#comboBuilderEditImageForm, #ajaxLoadingProgress').hide();
	$('#ComboImage').uniform();

	$('#linkUpdateComboPhoto').toggle(function() { 
		//show edit pic form, change link text
		$('#ajaxComboBuilderContent').empty();
		$('#comboBuilderEditImageForm').show();
		$(this).text('Hide Edit Picture Form');
		return false;	
	}, function() { 
		//remove pic form, revert link text
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').empty();
		$(this).text('Change Combo Picture');
		return false;
	});

	$('#btnAddPizzaToCombo').toggle(function() { 
		//show add pizza form, change button text
		$('#ajaxLoadingProgress').show();
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').load($(this).attr('href'), null, function() {
			$('#btnAddPizzaToCombo div.ui-icon-text').text('Hide Form');
			$('#ajaxLoadingProgress').hide();
		});
		return false;
	}, function() { 
		$('#ajaxComboBuilderContent').empty();
		$('#comboBuilderEditImageForm').hide();
		$('#btnAddPizzaToCombo div.ui-icon-text').text('Add Custom Pizza');
		return false;
	});

	$('#btnAddSpecialToCombo').toggle(function() { 
		//show add specialty pizza form, change button text
		$('#ajaxLoadingProgress').show();
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').load($(this).attr('href'), null, function() {
			$('#btnAddSpecialToCombo div.ui-icon-text').text('Hide Form');
			$('#ajaxLoadingProgress').hide();
		});
		return false;
	}, function() { 
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').empty();
		$('#btnAddSpecialToCombo div.ui-icon-text').text('Add Specialty Pizza');
		return false;
	});

	$('#btnAddItemToCombo').toggle(function() { 
		//show add comb item form, change button text
		$('#ajaxLoadingProgress').show();
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').load($(this).attr('href'), null, function() {
			$('#btnAddItemToCombo div.ui-icon-text').text('Restore');
			$('#ajaxLoadingProgress').hide();
		});
		return false;
	}, function() { 
		$('#comboBuilderEditImageForm').hide();
		$('#ajaxComboBuilderContent').empty();
		$('#btnAddItemToCombo div.ui-icon-text').text('Add Item');
		return false;
	});

	$('.edit-field').editable('/combos/ajax_edit', { 
		tooltip   : 'Click to Edit...',
		indicator : '<?php echo $html->image('ajax-loader-med-bar.gif'); ?>',
		id        : 'data[Combo][id]',
		width     : '100',
		name      : 'data[Combo][field]'
	});
});
</script>
<?php echo $jquery->scrFormSubmit('#btnComboBuilderEditImage', '#formComboBuilderEditImage'); ?>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Combo Builder</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/locations/edit_menu/'.$combo['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to Menu Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Use the Combo Builder below to build your Combo Special. A Combo Special can consist of any combination of pizzas and items on your menu, which 
	you can then offer for a single price. Use these specials to offer more incentive to your customers to order multiple items from your restaurant. 
	To edit the fields on your combo, click on the text for the field you would like to edit. When you are finished editing the field text, press 
	<span class="b">'Enter'</span> to auto-save your new field text.</p>
	
	<div class="ui-widget-content ui-corner-all  marg-b10">
		<div class="ui-helper-clearfix">
			<div class="left" style="width:180px;">
				<?
				echo $html->image($misc->calcImagePath($combo['Combo']['image'], 'files/combos/'), 
								  array('class'=>'gray-border marg-b5', 'style'=>'width:160px;'));
				?>
				<span><?php echo $html->link('Change Combo Picture', '/combos/edit_image/'.$combo['Combo']['id'], array('id'=>'linkUpdateComboPhoto', 'class'=>'base')); ?></span>
			</div>
			<div class="left" style="width:380px;">
				<div class="lg b orange marg-b5 edit-field" id="EditCombo_title_<?=$combo['Combo']['id']?>"><?php echo $combo['Combo']['title']; ?></div>
				<div class="green marg-b5 edit-field"  id="EditCombo_description_<?=$combo['Combo']['id']?>"><?php echo !empty($combo['Combo']['description']) ? $combo['Combo']['description'] : "<span style='text-decoration:underline;'>(Click here to enter a combo description.)</span>"; ?></div>
				
				<div class="ui-helper-clearfix b">
					<div class="left">$</div>
					<div class="left edit-field" id="EditCombo_price_<?=$combo['Combo']['id']?>"><?php echo number_format($combo['Combo']['price'], 2); ?></div>
				</div>
				<hr />
				<div class="ui-helper-clearfix" style="width:375px;">
					<div class="left pad-r5"><?php echo $jquery->btn('/combos_pizzas/add/'.$combo['Combo']['id'], 'ui-icon-circle-plus', 'Add Custom Pizza', null, null, 'btnAddPizzaToCombo'); ?></div>
					<div class="left pad-r5"><?php echo $jquery->btn('/combos_items/add/'.$combo['Combo']['id'], 'ui-icon-circle-plus', 'Add Item', null, null, 'btnAddItemToCombo'); ?></div>
					<div class="left"><?php echo $jquery->btn('/combos_specials_sizes/add/'.$combo['Combo']['id'], 'ui-icon-circle-plus', 'Add Specialty Pizza', null, null, 'btnAddSpecialToCombo'); ?></div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="ajaxLoadingProgress" class="txt-c"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
	<div id="ajaxComboBuilderContent"></div>
	<div id="comboBuilderEditImageForm" class="ui-widget-content ui-corner-all marg-b10">
		<fieldset class="ui-widget-content ui-corner-all  marg-b10"><legend class="ui-corner-all">Combo Special Details</legend>
			<?php echo $form->create('Combo', array('action'=>'edit_image', 'id'=>'formComboBuilderEditImage', 'type'=>'file')); ?>
			<?php echo $form->hidden('id', array('value'=>$combo['Combo']['id'])); ?>
			<table class="form-table">
				<tr>
					<td class="label-req">Photo:</td>
					<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false, 'id'=>'ComboImage'))?></td>
				</tr>
				<tr><td></td>
					<td><?php echo $jquery->btn('#', 'ui-icon-pencil', 'Update Image', null, null, 'btnComboBuilderEditImage', '115')?></td>
				</tr>
			</table>
		</fieldset>
	</div>
	
	<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Combo Includes:</legend>
		<?php if(sizeof($combo['CombosPizza']) <= 0 && sizeof($combo['CombosSpecialsSize']) <= 0 && sizeof($combo['Item']) <= 0) { ?>
			<span class="b green">This combo is currently empty.</span>
		<?php } else { ?>
			<table class="list-table ui-table marg-t5 ui-corner-all">
				<?php if(sizeof($combo['CombosPizza']) > 0) { 
					foreach($combo['CombosPizza'] as $p): ?>
						<tr style="height:60px;">
							<td>
								<div class="b green marg-b5"><?php 
									echo "(".$p['CombosPizza']['qty'].") ".$p['Size']['title'];
									echo ($p['CombosPizza']['qty'] > 1) ? " Pizzas" : " Pizza";
								?></div>
								
								<?php 
								if(sizeof($p['Topping']) > 0 || $p['CombosPizza']['allowed_toppings'] > 0) {
									echo "<ul class='base pad-l5' style='list-style:none;'>";
									
									if(sizeof($p['Topping']) > 0) {
										$count = 0;
										echo "<li>Included Toppings: ";
										foreach($p['Topping'] as $t):
											if($count++ > 0) echo ", ";
											echo $t['Topping']['title'];
										endforeach;
										echo "</li>";
									}
									
									if($p['CombosPizza']['allowed_toppings'] > 0) {
										echo "<li>(+".$p['CombosPizza']['allowed_toppings'].") Topping(s) of Choice.</li>";	
									}
									echo "</ul>";
								}?>
							</td>
							<td style="width:160px;">
								<?php echo $jquery->btn('/combos_pizzas/delete/'.$p['CombosPizza']['id'], 'ui-icon-circle-close', 'Remove From Combo',
														'Are you sure that you would like to remove this from the current order combo?', null, null, '155')?>
							</td>
						</tr>
					<?php endforeach; 
				}
				
				if(sizeof($combo['CombosSpecialsSize']) > 0) { ?>
					<?php foreach($combo['CombosSpecialsSize'] as $p): ?>
						<tr style="height:60px;">
							<td>
								<div class="b green marg-b5"><?php 
									echo "(".$p['CombosSpecialsSize']['qty'].") ".$p['Size']['title'];
									if(isset($p['Special'])) {
										echo " ".$p['Special']['title']." Specialty ";
										echo ($p['CombosSpecialsSize']['qty'] > 1) ? " Pizzas" : " Pizza";	
									} else {
										echo " Specialty ";
										echo ($p['CombosSpecialsSize']['qty'] > 1) ? "Pizzas of Choice" : " Pizza of Choice";
									}
								?></div>
							</td>
							<td style="width:160px;">
								<?php echo $jquery->btn('/combos_specials_sizes/delete/'.$p['CombosSpecialsSize']['id'], 'ui-icon-circle-close', 'Remove From Combo',
														'Are you sure that you would like to remove this from the current order combo?', null, null, '155')?>
							</td>
						</tr><?php
					endforeach; 
				}
				
				$current_add_group = false;
				if(sizeof($combo['Item']) > 0) {
					foreach($combo['Item'] as $i):
						if($current_add_group == false || $i['CombosItem']['add_group_id'] != $current_add_group || empty($i['CombosItem']['add_group_id'])) { ?>
							<tr style="height:60px;">
								<td>
									<div class="b green marg-b5"><?php
										if($i['CombosItem']['add_type'] == 'Single') {
											//single item
											if(!empty($i['CombosItem']['qty'])) echo "(".$i['CombosItem']['qty'].") ";
											echo $i['Item']['Item']['title'];
											if(!empty($i['CombosItem']['qty']) && $i['CombosItem']['qty'] > 1) echo "s";
											if(!empty($i['Item']['ItemsOption']['label'])) echo " (".$i['Item']['ItemsOption']['label'].")";
											if(!empty($i['Item']['Item']['description'])) echo "<div class='orange base' style='width:380px;'>".$i['Item']['Item']['description']."</div>"; 
										} else if($i['CombosItem']['add_type'] == 'Series') {
											//choice series
											echo "Choice of (".$i['CombosItem']['add_group_limit'].") items in category: ".$i['Cat']['title'];
										} else {
											//choice series with entire cat
											echo "Choice of any (".$i['CombosItem']['add_group_limit'].") items in category: ".$i['Cat']['title'];
										}
									?></div>
								</td>
								<td style="width:160px;">
									<?php echo $jquery->btn('/combos_items/delete/'.$i['CombosItem']['id'], 'ui-icon-circle-close', 'Remove From Combo',
															'Are you sure that you would like to remove this from the current order combo?', null, null, '155')?>
								</td>
							</tr>
						<?php } 
						$current_add_group = $i['CombosItem']['add_group_id'];
					endforeach; 
				} 
				
				if(sizeof($combo['CombosTextItem']) > 0) { ?>
					<?php foreach($combo['CombosTextItem'] as $i): ?>
						<tr style="height:60px;">
							<td>
								<div class="b green marg-b5">
									<?php echo "(".$i['CombosTextItem']['qty'].") ".$i['CombosTextItem']['text']; ?>
								</div>
							</td>
							<td style="width:160px;">
								<?php echo $jquery->btn('/combos_text_items/delete/'.$i['CombosTextItem']['id'], 'ui-icon-circle-close', 'Remove From Combo',
														'Are you sure that you would like to remove this item from the current order combo?', null, null, '155')?>
							</td>
						</tr><?php
					endforeach; 
				} ?>
			</table>
		<?php } ?>
	</fieldset>
</div>
<?php //echo debug($combo); ?>