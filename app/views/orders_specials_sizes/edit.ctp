<?php echo $jquery->scrFormSubmit('#btnEditOrderSpecial', '#formEditOrderSpecial'); ?>
<?php echo $this->element('js/jquery/add_form_styles'); ?>

<script>
$(function() {
	$('#ajaxMoreToppingsLoading').hide();
	$('#btnMoreToppings').click(function() {
		$('#ajaxMoreToppingsLoading').show();
		$('#ajaxMoreToppingsContent').load($(this).attr('href'), null, function() {
			$('#ajaxMoreToppingsLoading').hide();
		});
		return false;
	});
});
</script>


<h2>Modify Specialty Pizza</h2>
<?=$form->create('OrdersSpecialsSize', array('action' => 'edit', 'id'=>'formEditOrderSpecial'))?>
<?php echo $form->hidden('order_id'); ?>
<?php echo $form->hidden('special_size_id'); ?>
<?php echo $form->hidden('id'); ?>
<?php echo $form->hidden('total'); ?>
<div class="marg-b20 pad10 ui-widget-content gray-border">
	<div class="ui-helper-clearfix">
		<div class="left" style="width:220px;">
			<?
			echo $html->image($misc->calcImagePath($special['Special']['image'], 'files/specialtyPizzas/'), 
							  array('class'=>'gray-border dbl-border', 'style'=>'width:200px;'));
			?>
		</div>
		<div class="left">
			<h2><?=$special['Special']['title']?></h2>
			<span class="orange"><?=$special['Special']['subtext']?></span>
		</div>
		<div class="right">
			<div><?php echo $misc->jqueryBtn('#', 'ui-icon-circle-plus', 'Update Specialty Pizza', null, null, 'btnEditOrderSpecial');?></div>
			<div><?php echo $misc->jqueryBtn('/orders_specials_sizes/ajax_extra_toppings', 'ui-icon-wrench', 'Add More Toppings', null, null, 'btnMoreToppings');?></div>
		</div>
	</div>
	
	<hr class="marg-t5" />
	
	<fieldset class="ui-widget-content ui-corner-all marg-b10">
		<legend class="ui-corner-all">Choose A Size</legend>
		<table>
			<?
			$options = array();
			foreach($special['Size'] as $size):
				$options[$size['SpecialsSize']['id']] = "  $".number_format($size['SpecialsSize']['price'], 2)." - ".
														$size['title']." (".$size['size']."\")";
			endforeach;
			?>
			<div class="ui-helper-clearfix pad10">
				<div class="size-label">
					<?php echo $form->radio('specials_size_id', $options, array('legend'=>false, 'separator'=>'</div><div class="size-label">',
																				'value'=>$special['Size'][0]['SpecialsSize']['id'])); ?>
				</div>
			</div>
		</table>
	</fieldset>
	
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Special Details</legend>
		<table class="form-table" style="width:100%;">
			<tr>
 				<td class="label-req">Crust Type: <span class="required">*</span></td>
 				<td><?php echo $form->select('locations_crust_id', $crusts, null, array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Sauce Options: <span class="required">*</span></td>
 				<td><?php echo $form->select('sauce', $misc->sauces(), 'Regular Sauce', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Cheese Type: <span class="required">*</span></td>
 				<td><?php echo $form->select('cheese', $misc->cheeses(), 'Regular Cheese', array('class'=>'form-text ui-state-default ui-corner-all','label'=>''), false); ?></td>
 			</tr>
			<tr>
 				<td class="label-req">Quantity: <span class="required">*</span></td>
 				<td><?php echo $form->select('qty', $misc->qtys(), null, array('class'=>'form-text','label'=>''), false); ?></td>
 			</tr>
			<tr>
				<td class="label-req">Special Instructions:</td>
				<td><?=$form->textarea('comments',array('class'=>'form-text ui-state-default ui-corner-all','label'=>''))?><br />
				<span class="base orange">(e.g. Ingredients to remove.)</span></td>
			</tr>
		</table>
	</fieldset>
	
	<div id="ajaxMoreToppingsLoading" class="txt-c"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
	<div id="ajaxMoreToppingsContent" class></div>
	</form>
</div>
<?php //echo debug($special); ?>