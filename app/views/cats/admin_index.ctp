<div class="cats index">
<div class="actions">
	<ul>
		<?php
		$thickbox->setProperties(array('id'=>'addNewCat','type'=>'ajax','ajaxUrl' => '/admin/cats/add'));
		$thickbox->setPreviewContent("Add New Category");
		?>
		<li><?=$thickbox->output();?></li>
	</ul>
</div>
<h2><?php __('Manage Categories');?></h2>

<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('title');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($cats as $cat):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $cat['Cat']['title']; ?>
		</td>
		<td class="actions">
		<?php
		$ajaxurl = '/admin/cats/edit/' . $cat['Cat']['id'];
		$thickbox->setProperties(array('id'=>'addNewCat','type'=>'ajax','ajaxUrl' => $ajaxurl));
		$thickbox->setPreviewContent("Edit");
		?>
		<?=$thickbox->output();?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $cat['Cat']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $cat['Cat']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<?php '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

