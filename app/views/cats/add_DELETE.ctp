<div class="cats form">
<?php echo $form->create('Cat');?>
	<fieldset>
 		<legend><?php __('Add Cat');?></legend>
	<?php
		echo $form->input('title');
		echo $form->input('Menu');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Cats', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Menus', true), array('controller'=> 'menus', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Menu', true), array('controller'=> 'menus', 'action'=>'add')); ?> </li>
	</ul>
</div>
