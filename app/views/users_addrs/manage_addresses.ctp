<?php echo $this->element('js_googlemap_headers'); ?>
<?php echo $html->script(array('jquery/alphanumeric'), false); ?>
<?php echo $jquery->scrFormSubmit('#btnUserAddAddress', '#formUserAddAddress'); ?>
<script>
	// header option: "#addrAccordion > div:nth-child(odd)"
	$(function() {
		$('#addrAccordion').accordion();
		$('.numeric-only').numeric();
		$('.alpha-only').alpha({ allow: " " });
	});
</script>
<div>
	<?php 
	if(!isset($default_addr)) {
		echo $this->element('warning-msg-box', array('msg'=>'WARNING: You have no delivery address set up!'));	
	}
	?>
	<div id="userNav" class="ui-widget-header ui-corner-top">
		<div class="clearfix">
			<div class="left"><?php echo $html->image('btn-manage-addr-active.png'); ?></div>
			<div class="right basefont">
				<?php echo $html->link($html->image('btn-my-orders.png', array('id' => '')), 
									'/user-dash/orders', array('escape'=>false)); ?> 
				<?php echo $html->link($html->image('btn-contact-profile.png', array('id' => '')), 
									'/user-dash/profile', array('escape'=>false)); ?>  
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="ui-widget-content ui-corner-bottom">
		<div class="clearfix">
			<div class="left" style="width:340px;padding: 0px 10px;">
				<div class="ui-widget ui-accordion" id="addrAccordion">
					<h2 class="ui-widget-header ui-accordion-header"><a href="#">Add New Delivery Address</a></h2>
					<div class="ui-widget-content ui-accordion-content">
						<?php echo $form->create('UsersAddr', array('action'=>'manage_addresses', 'id'=>'formUserAddAddress'));?>
					 		<table class="form-table" style="margin-bottom: 20px;">
					 			<tr>
					 				<td class="label-req">Location Name: <span class="required">*</span></td>
					 				<td><?=$form->input('name', array('class'=>'form-text','label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">Address: <span class="required">*</span></td>
					 				<td><?=$form->input('address', array('class'=>'form-text','label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">City: <span class="required">*</span></td>
					 				<td><?=$form->input('city', array('class'=>'form-text alpha-only','label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">State: <span class="required">*</span></td>
					 				<td><?=$form->input('state_id', array('class'=>'form-text','label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">Zip: <span class="required">*</span></td>
					 				<td><?=$form->input('zip', array('class'=>'form-text numeric-only','label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">Special Instructions:</td>
					 				<td><?=$form->input('special_instructions', array('class'=>'form-text','label'=>'', 'div'=>false));?><br />
					 				<span class="basefont required">(e.g. "Door bell does not work.")</span></td>
					 			</tr>
					 			<tr>
					 				<td class="label-req">Make default location?</td>
					 				<td><?=$form->checkbox('default', array('label'=>''));?></td>
					 			</tr>
					 			<tr>
					 				<td></td>
					 				<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Address', null, null, 'btnUserAddAddress', '110'); ?></td>
					 			</tr>
					 		</table>
					 		</form>
					</div>
					<?php if(sizeof($addrs) > 0) { ?>
						<?php foreach($addrs as $index => $a): ?>
						<?php if($a['UsersAddr']['default'] == '1') { $default_index = $index; } ?>
						<h2 class="ui-widget-header ui-accordion-header"><a href="#"><?=$a['UsersAddr']['name']?></a></h2>
						<div class="ui-widget-content ui-accordion-content ui-helper-clearfix">
							<div class="left">
								<p><?=$a['UsersAddr']['address']?><br /><?=$a['UsersAddr']['city'].", ".$a['State']['abbr']." ".$a['UsersAddr']['zip'] ?></p>
							</div>
							<div class="right">
								<?php 
								echo $misc->jqueryBtn('/users_addrs/delete/'.$a['UsersAddr']['id'],
													  'ui-icon-close', 'Remove Address',
													  'Are you sure you would like to remove this delivery address?');
								
								if($a['UsersAddr']['default'] == 1) { ?>
			         				<div class="ui-state-highlight ui-helper-clearfix txt-right ui-corner-all pad3">
			         					<div class="ui-icon ui-icon-circle-check left"></div>
			         					<div class="ui-icon-text">Default Address</div>
			         				</div>
								<?php } else {
									echo $misc->jqueryBtn('/users_addrs/set_default/'.$a['UsersAddr']['id'],
														  'ui-icon-circle-close', 'Set as Default',
														  'Set this location as your default delivery address?');
									
								} ?>
		         			</div>
						</div>
						<?php endforeach; ?>
					<?php } ?>
				</div>
				<?php 
				if(sizeof($addrs) > 0) {
					?>
					</div>
					<div class="right" id="map_placeholder">
					<?php
					    $default = array('type'=>'0','zoom'=>12,'lat'=>$addrs[$default_index]['UsersAddr']['latitude'],'long'=>$addrs[$default_index]['UsersAddr']['longitude']);
						echo $googleMap->map(	$default, 
												$style = 'width:220px; height: 368px;', 
												false, 
												'ui-corner-all gray-border', 
												array(
													'points'=> $addrs,
													'icon'	=> 'map-house-icon.png', 
													'sizes' => array('30','34','6')
												));
					?>
					<?php /*echo $ajax->link('Address not showing correctly on the map?', 
								   '/users/ajax_geocode_problem/'.$session->read('User.id'), 
								   array('update'=>'geocode_problem_div', 'class'=>'basefont right'));  */?>
					<div id="geocode_problem_div">
						
					</div>
				</div>	
				<?php } ?>
		</div>
		<div class="clear"></div>
	
	</div>
</div>