<script>
$(function() { 
	$('#ajaxGettingCatItems').hide();
	$('#CombosItemItemId, #CombosItemCatId, #CombosItemQty').uniform();

	$('#CombosItemCatId').change(function() { 
		$('#ajaxGettingCatItems').show();
		$('#uniform-CombosItemItemId span:first').text('Choose Menu Item');
		$('#CombosItemItemId').load('/combos_items/ajax_populate_items_add', { catId:   $(this).val(),
																			   comboId: $('#CombosItemComboId').val() }, function() { 
			$('#CombosItemItemId').removeAttr('disabled');
			$('#uniform-CombosItemItemId').removeClass('disabled');
			$('#ajaxGettingCatItems').hide();
		});
	});

	$('#btnComboBuilderAddItem').click( function () {
		if($('#CombosItemItemId').val() == '' || $('#CombosItemItemId').val() == null) {
			$('#CombosItemItemIdText').text('Please select a menu item!');
		} else {
			$('#formComboBuilderAddItem').submit();
		}
		return false;
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_text_events'); ?>

<?php echo $form->create('CombosItem', array('action'=>'add', 'id'=>'formComboBuilderAddItem')); ?>
<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
<?php echo $form->hidden('add_type', array('value'=>'Single')); ?>
	
<hr class="orange" />
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Choose Menu Category:</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('cat_id', $cats, null, array('class'=>'form-text', 'id'=>'CombosItemCatId'), 'Select a Menu Category')?></div>
			<div class="left" id="ajaxGettingCatItems"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
		</div></td>
	</tr>
	<tr>
		<td class="label-req" style="width:200px;">Choose Menu Item:</td>
		<td><?=$form->select('CombosItem.item_id', array(), null, array('class'=>'form-text', 'id'=>'CombosItemItemId', 'disabled'=>'disabled'), false)?>
		<br /><span class="base red b" id="CombosItemItemIdText"></span></td>
	</tr>
	<tr>
		<td class="label-req">Quantity:</td>
		<td><?php echo $form->select('CombosItem.qty', $misc->qtys(), null, array('class' => 'form-text'), false); ?></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Menu Item', null, null, 'btnComboBuilderAddItem', '130')?></td>
	</tr>
</table>
</form>