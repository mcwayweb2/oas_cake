<script>
$(function() { 
	$('#progressComboBuilderAddItem').hide();

	$('#ajaxRadioType').change(function() {
		$('#progressComboBuilderAddItem').show();
		$('#ajaxToggleContent').empty(); 
		if($(this).val() == 'single') {
			$('#ajaxToggleContent').load('/combos_items/ajax_single_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddItem').hide();
			});	
		} else if($(this).val() == 'series'){
			$('#ajaxToggleContent').load('/combos_items/ajax_series_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddItem').hide();
			});
		} else if($(this).val() == 'text'){
			$('#ajaxToggleContent').load('/combos_text_items/ajax_text_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddItem').hide();
			});
		} else {
			$('#ajaxToggleContent').load('/combos_items/ajax_cat_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddItem').hide();
			});
		}
	});
});
</script>

<div class="ui-widget-content ui-corner-all marg-b10">
<fieldset class="ui-widget-content ui-corner-all  marg-b10"><legend class="ui-corner-all">Add a Menu Item to your Combo...</legend>
	<p>Use this form to add a Menu Item to your current Combo. You can add a specific item from your menu, or create a new a item specifically for 
	use in this combo.</p> 
	<p>You may also create a Choice Series, composed 
	of a set of menu items, which the customer then chooses from upon ordering. You may create a Choice Series from any combination of your 
	current menu items, or by adding an entire menu category, for the customer to choose from. To get started, select your desired method for 
	adding an item to your combo below.</p>
	<hr />
	<?php $opts = array('single' => 'Choose a specific menu item to add',
						'series' => 'Create a Choice Series for the customer to choose from',
						'cat'    => 'Add an entire Menu Category as a Choice Series',
						'text'	 => 'Create a New Text Item Specifically for this Combo'); ?>
						
	<div class="ui-helper-clearfix">
		<div class="left pad-r10"><?php echo $form->select('type', $opts, null, array('id'=>'ajaxRadioType', 'class'=>'ui-widget-content ui-corner-all ui-state-default'), 'How will you add this Menu Item?');?></div>
		<div class="left" id="progressComboBuilderAddItem"><?php echo $html->image('ajax-loader-med-bar.gif', array('style'=>'width:100px;')); ?></div>
	</div>
	
	<div id="ajaxToggleContent"></div>
</fieldset>
</div>
<script>
//$(function() { $('#uniform-ajaxRadioType').width('400'); });
</script>