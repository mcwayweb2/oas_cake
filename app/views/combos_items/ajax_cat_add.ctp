<script>
$(function() { 
	$('#ajaxGettingCatItems').hide();
	$('#CombosItemCatId, #CombosItemAddGroupLimit').uniform();
	
	$('#CombosItemCatId').change(function() { 
		//repopulate the qty select dropdown with the actual max number of items in the chosen category
		$('#ajaxGettingCatItems').show();
		$('#CombosItemAddGroupLimit').load('/combos_items/ajax_populate_qtys_add', { catId:          $(this).val(),
																			   	     comboId:        $('#CombosItemComboId').val() }, function() { 
			$('#uniform-CombosItemAddGroupLimit span:first').empty();
			$('#CombosItemAddGroupLimit').removeAttr('disabled');
			$('#uniform-CombosItemAddGroupLimit').removeClass('disabled');
			$('#ajaxGettingCatItems').hide();
		});
	});
	
	$('#btnComboBuilderAddItem').click( function () {
		if($('#CombosItemAddGroupLimit').val() == null || $('#CombosItemAddGroupLimit').val() <= 0) {
			$('#CombosItemAddGroupLimitText').text('Please select an add limit!');
		} else {
			$('#formComboBuilderAddItem').submit();
		}
		return false;
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_text_events'); ?>

<?php echo $form->create('CombosItem', array('action'=>'add', 'id'=>'formComboBuilderAddItem')); ?>
<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
<?php echo $form->hidden('add_type', array('value'=>'Cat')); ?>
<hr class="orange" />
<p>Choose the menu category that you would like to allow the customer to select items from. Then select the number of items, they are allowed to
choose from this category, when ordering this Menu Combo.</p>
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Choose Menu Category:</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('temp_cat_id', $cats, null, array('class'=>'form-text', 'id'=>'CombosItemCatId'), 'Select a Menu Category')?></div>
			<div class="left" id="ajaxGettingCatItems"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
		</div></td>
	</tr>
	<tr>
		<td class="label-req" style="width:150px;">How many of these items can the customer choose?</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('add_group_limit', $misc->qtys(), null, array('class'=>'form-text', 'id'=>'CombosItemAddGroupLimit', 'disabled'=>'disabled'), 'Select a Quantity')?><br />
			<span class="base b red" id="CombosItemAddGroupLimitText"></span></div>
		</div></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Choice Series', null, null, 'btnComboBuilderAddItem', '140'); ?></td>
	</tr>
</table>
</form>