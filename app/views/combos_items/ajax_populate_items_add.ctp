<?php 
	if($show_checkboxes) {
		//echo debug($items);
		foreach ($items as $item):
			?>
			<div class="ui-helper-clearfix marg-b5">
				<div class="left pad-r10" style="width:20px;"><?php echo $form->checkbox('items.'.$item['Item']['id'], array('class'=>'my-checkbox')); ?></div>
				<div class="left" style="width:300px;"><?php 
					echo '<b>'.$item['Item']['title'].'</b>';
					if(!empty($item['Item']['description'])) echo " (".$text->truncate($item['Item']['description'], 80).")";
				?></div>
			</div>
			<?php		
		endforeach;
	} else {
		//populate select dropdown
		echo "<option value='' disabled='disabled'></option>";
		foreach ($items as $item):
			echo "<option value='".$item['Item']['id']."'>".$item['Item']['title']."</option>";	
		endforeach;
	}
?>