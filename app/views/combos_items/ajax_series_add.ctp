<script>
$(function() { 
	$('#ajaxGettingCatItems').hide();
	
	$('#CombosItemCatId, #CombosItemAddGroupLimit').uniform();
	
	$('#CombosItemCatId').change(function() { 
		//repopulate the sizes select dropdown with the available sizes for the chosen specialty pizza
		$('#ajaxGettingCatItems').show();
		$('#uniform-CombosItemItemId span:first').text('Choose Menu Item');
		$('#CombosItemItemIdDiv').load('/combos_items/ajax_populate_items_add', { catId:          $(this).val(),
																			   	  showCheckboxes: true,
																			      comboId:        $('#CombosItemComboId').val() }, function() { 
			$('#CombosItemItemId').removeAttr('disabled');
			$('#uniform-CombosItemItemId').removeClass('disabled');
			$('#ajaxGettingCatItems').hide();
		});
	});
	
	$('#btnComboBuilderAddItem').click( function () {
		if($('.my-checkbox').filter(":checked").length < 2) {
			$('#CombosItemItemIdText').text('Please select at least 2 menu items!');
		} else if($('.my-checkbox').filter(":checked").length < $('#CombosItemAddGroupLimit').val()) {
			$('#CombosItemItemIdText').text('Please select more items for this add limit.');
		} else {
			$('#formComboBuilderAddItem').submit();
		}
		return false;
	});
	
});
</script>
<?php echo $this->element('js/jquery/add_form_text_events'); ?>

<?php echo $form->create('CombosItem', array('action'=>'add', 'id'=>'formComboBuilderAddItem')); ?>
<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
<?php echo $form->hidden('add_type', array('value'=>'Series')); ?>
<hr class="orange" />
<p>Start by choosing the menu category that you would like to select items from. Then check the boxes next to the items that you would like to 
include in the Choice Series.</p>
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Choose Menu Category:</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('cat_id', $cats, null, array('class'=>'form-text', 'id'=>'CombosItemCatId'), 'Select a Menu Category')?></div>
			<div class="left" id="ajaxGettingCatItems"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
		</div></td>
	</tr>
	<tr>
		<td class="label-req" style="width:200px;">Choose Menu Items:</td>
		<td><div id="CombosItemItemIdDiv"></div>
		<span class="base red b" id="CombosItemItemIdText"></span></td>
	</tr>
	<tr>
		<td class="label-req" style="width:150px;">How many of these items can the customer choose?</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('add_group_limit', $misc->qtys(), null, array('class'=>'form-text', 'id'=>'CombosItemAddGroupLimit'), 'Select a Quantity')?></div>
		</div></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Choice Series', null, null, 'btnComboBuilderAddItem', '140'); ?></td>
	</tr>
</table>
</form>