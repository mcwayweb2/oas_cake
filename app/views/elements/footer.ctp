<div id="footer" class="ui-helper-clearfix">
	<div class="left pad-r20"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
	<div class="left pad-r20"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
	
	<div class="right">
		<?=$html->link('Request a Restaurant', '/request-a-restaurant')?> 
		<?php //echo $html->link('About Us', '/about-us'); ?>
		<?php if(!$session->check('Advertiser')) { ?>
		 | <?=$html->link('Restaurant Login', '#', array('id'=>'adLoginLink'))?>
		 | <?=$html->link('Restaurant Registration', '/registration')?>
		<?php } else { ?>
			
		<?php } ?> 
	    <p class="marg-t5">&copy; <?=date('Y')?> Order A Slice. All rights reserved. Developed By McWay Web Development.<br />
	    PO Box 2762, La Mesa, CA 91943 | (619) 467-5423 | customer-service@orderaslice.com</p>
	    <?php if(!isset($hide_bg)) { ?>
	    <div id="footer-pizza"></div>
	    <?php } ?>
	</div>
</div>