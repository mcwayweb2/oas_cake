<?php if(!isset($msg)) { ?>
<script>
$(function() {
	$('.delay-msg').delay(8000).slideUp();
});
</script>
<?php } ?>

<div class="marg-b10 <?php if(!isset($msg)) echo "delay-msg"; ?>">
<div class="ui-widget ui-widget-content ui-state-highlight ui-corner-all">
	<div class="ui-helper-clearfix">
		<div class="left" style="width:20px;">
			<?php if(empty($no_icon)) { ?><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><?php } ?>
		</div>
		<div class="left b">
			<?php 
				if(isset($msg)) { echo $msg; }
				else { echo $session->flash(); } 
			 ?>
		</div>
	</div> 
</div> 
</div>