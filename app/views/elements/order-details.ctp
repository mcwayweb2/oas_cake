<div class="orange-border ui-widget-content">
<table class="list-table ui-table shopping-cart-list">
    <tr>
	    <th style="width:25px;"></th>
	    <th><span class="pad-l5">Item</span></th>
        <th style="width:80px;" class="txt-l"><span class="pad-l5">Price</span></th>
    </tr>
    <?php
    echo $this->element('table_accent_row', array('cols'=>3));
    if(sizeof($order['OrdersItem']) > 0) {
	    foreach($order['OrdersItem'] as $item): ?>
	    	<tr>
	    		<td>(<?=$item['OrdersItem']['qty']?>)</td>
	    		<td>
	    			<div class="green b marg-b5"><?=$item['Item']['title']?></div>
	    			<div class=""><?=$item['Item']['description']?></div>
	    			<?php 
	    				if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
	    					?><ul class='no-list base marg-t5 red'><?php 
	    					foreach($item['ItemsOption'] as $opt):
								echo '<li>'.$opt['label'];
								if($opt['OrdersItemsItemsOption']['price'] > 0) {
									echo '<span class="">(+$'.number_format($opt['OrdersItemsItemsOption']['price'], 2).')</span>';	
								}	
	    					endforeach;
	    					
	    					foreach($item['LocationsCatsOption'] as $opt):
								echo '<li>'.$opt['label'];
								if($opt['OrdersItemsLocationsCatsOption']['price'] > 0) {
									echo '<span class="pad-l5">(+$'.number_format($opt['OrdersItemsLocationsCatsOption']['price'], 2).')</span>';	
								}	
	    					endforeach;
	    					?></ul><?php
	    				}
	    				
	    				if(!empty($item['OrdersItem']['comments'])) {
	    					?><div class="marg-t5 ">Additional Instructions:<span class="italic pad-l5"><?php echo $item['OrdersItem']['comments']; ?></span></div><?php 
	    				}
	    			?>
	    		</td>
	    		<td><?php echo $number->currency($item['OrdersItem']['total']); ?></td>
	    	</tr><?php
	    endforeach;
    }
    
	if(sizeof($order['Pizza']) > 0) {
	    foreach($order['Pizza'] as $pizza): ?>
	    	<tr>
	    		<td>(<?=$pizza['qty']?>)</td>
	    		<td>
	    			<div class="green b">
	    				<?php echo $pizza['Size']['size'].'" '.$pizza['Size']['title'].' Pizza'; ?>
	    			</div>
    				<?php 
    				$lines = array();
					if($pizza['Crust']['LocationsCrust']['price'] > 0) {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
    				} else {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
    				}
    				
    				if($pizza['sauce'] == 'Extra Sauce') {
    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
    				
					if($pizza['cheese'] == 'Extra Cheese') {
    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
    				
    				if(sizeof($pizza['Topping']) > 0) {
						foreach($pizza['Topping'] as $topping):
							$l = 'Add '.$topping['Topping']['title'];
							if($topping['PizzasTopping']['placement'] != 'Whole') {
								$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
							}
							
							if($topping['PizzasTopping']['price'] > 0) {
								$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
							}
							
							$lines[] = $l;
						endforeach;
    				}
    				
    				if(sizeof($lines) > 0) {
    					echo "<div class='base red marg-t5'><ul class='no-list'>";
						foreach($lines as $line):
							echo "<li>".$line."</li>";
						endforeach;
    					echo "</ul></div>";
    				}
	    				 
					if(!empty($pizza['comments'])) {
	    				?><div class="marg-t5 ">Additional Instructions:<span class="italic pad-l5"><?php echo $pizza['comments']; ?></span></div><?php 
	    			} ?>
	    		</td>
	    		<td><?php echo $number->currency($pizza['total']); ?></td>
	    	</tr><?php 	
	    endforeach;
    }
    
    if(sizeof($order['OrdersSpecialsSize']) > 0) {
    foreach($order['OrdersSpecialsSize'] as $pizza): ?>
    	<tr>
    		<td>(<?=$pizza['qty']?>)</td>
    		<td>
    			<div class="green b">
    				<?php echo $pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Specialty Pizza"; ?>
    			</div>
    			<?php 
    				$lines = array();
    				if($pizza['sauce'] == 'Extra Sauce') {
    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
    				
					if($pizza['cheese'] == 'Extra Cheese') {
    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
    				
    				if($pizza['Crust']['LocationsCrust']['price'] > 0) {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
    				} else {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
    				}
    				/*
    				if(sizeof($pizza['Topping']) > 0) {
						foreach($pizza['Topping'] as $topping):
							$l = 'Add '.$topping['Topping']['title'];
							if($topping['PizzasTopping']['placement'] != 'Whole') {
								$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
							}
							
							if($topping['PizzasTopping']['price'] > 0) {
								$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
							}
							
							$lines[] = $l;
						endforeach;
    				} */
    				
    				if(sizeof($lines) > 0) {
    					echo "<div class='base red marg-t5'><ul class='no-list'>";
						foreach($lines as $line):
							echo "<li>".$line."</li>";
						endforeach;
    					echo "</ul></div>";
    				}
	    				 
					if(!empty($pizza['comments'])) {
	    				?><div class="marg-t5 ">Additional Instructions:<span class="italic pad-l5"><?php echo $pizza['comments']; ?></span></div><?php 
    			} ?>
    		</td>
    		<td><?php echo $number->currency($pizza['total']); ?></td>
    	</tr><?php	
    endforeach;
    }
    
    if(sizeof($order['OrdersCombo']) > 0) {
    	foreach($order['OrdersCombo'] as $combo_index => $combo): ?>
    		<tr>
	    		<td>(<?=$combo['qty']?>)</td>
	    		<td>
	    			<div class="green b marg-b5"><?=$combo['OrdersCombo']['Combo']['title']?> Combo</div>
	    			<div class=""><?=$combo['OrdersCombo']['Combo']['description']?></div>
	    			<?php
						if(sizeof($combo['OrdersCombo']['OrdersCombosPizza']) > 0) {
							foreach($combo['OrdersCombo']['OrdersCombosPizza'] as $pizza):
								$lines = array();
			    				if($pizza['sauce'] == 'Extra Sauce') {
			    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
			    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
			    				
								if($pizza['cheese'] == 'Extra Cheese') {
			    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
			    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
			    				
			    				if($pizza['crust_price'] > 0) {
			    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
			    				} else {
			    					$lines[] = $pizza['Crust']['title'].' Crust';
			    				}
			    				
			    				if(sizeof($pizza['Topping']) > 0) {
									foreach($pizza['Topping'] as $topping):
										$l = 'Add '.$topping['title'];
										if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
											$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
										}
										
										if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
											$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
										}
										
										$lines[] = $l;
									endforeach;
			    				}
			    				
		    					echo "<div class='pad-l20 marg-t10 green'>".$pizza['Size']['Size']['size']."\" ".$pizza['Size']['Size']['title']." Pizza</div>";
		    					echo "<div class='pad-l20 base red'><ul class='no-list'>";
								foreach($lines as $line):
									echo "<li>".$line."</li>";
								endforeach;
		    					echo "</ul></div>";
							endforeach;
						}
	    			
						if(sizeof($combo['OrdersCombo']['OrdersCombosSpecialsSize']) > 0) {
							foreach($combo['OrdersCombo']['OrdersCombosSpecialsSize'] as $pizza):
								$lines = array();
			    				if($pizza['sauce'] == 'Extra Sauce') {
			    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
			    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
			    				
								if($pizza['cheese'] == 'Extra Cheese') {
			    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
			    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
			    				
			    				if($pizza['crust_price'] > 0) {
			    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
			    				} else {
			    					$lines[] = $pizza['Crust']['title'].' Crust';
			    				}
			    				/*
			    				if(sizeof($pizza['Topping']) > 0) {
									foreach($pizza['Topping'] as $topping):
										$l = 'Add '.$topping['title'];
										if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
											$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
										}
										
										if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
											$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
										}
										
										$lines[] = $l;
									endforeach;
			    				}
			    				*/
			    				
		    					echo "<div class='pad-l20 marg-t10 green'>".$pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Pizza</div>";
		    					if(!empty($pizza['Special']['subtext'])) {
		    						echo "<div class='pad-l20 base orange'>".$pizza['Special']['subtext']."</div>";	
		    					}
		    					echo "<div class='pad-l20 base red'><ul class='no-list'>";
								foreach($lines as $line):
									echo "<li>".$line."</li>";
								endforeach;
		    					echo "</ul></div>";
							endforeach;
						}
						
    					if(sizeof($combo['OrdersCombo']['OrdersCombosItem']) > 0) {
							foreach($combo['OrdersCombo']['OrdersCombosItem'] as $item): ?>
								<div class='pad-l20 marg-t10 green'><?php echo $item['Item']['title']; ?></div>
								<div class="pad-l20 base orange"><?php echo $item['Item']['description']; ?></div><?php

		    					if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
			    					?><div class='pad-l20'><ul class='no-list base red'><?php 
			    					foreach($item['ItemsOption'] as $opt):
										echo '<li>'.$opt['label'];
										if($opt['OrdersCombosItemsItemsOption']['price'] > 0) {
											echo '<span class="">(+$'.number_format($opt['OrdersCombosItemsItemsOption']['price'], 2).')</span>';	
										}	
			    					endforeach;
			    					
			    					foreach($item['LocationsCatsOption'] as $opt):
										echo '<li>'.$opt['label'];
										if($opt['OrdersCombosItemsLocationsCatsOption']['price'] > 0) {
											echo '<span class="pad-l5">(+$'.number_format($opt['OrdersCombosItemsLocationsCatsOption']['price'], 2).')</span>';	
										}	
			    					endforeach;
			    					?></ul></div><?php
			    				}
							endforeach;
						}
					
	    				if(!empty($combo['OrdersCombo']['OrdersCombo']['comments'])) {
	    					?><div class="marg-t10 ">Additional Instructions:<span class="italic pad-l5"><?php echo $combo['OrdersCombo']['OrdersCombo']['comments']; ?></span></div><?php 
	    				}
	    				
	    			?>
	    		</td>
	    		<td><?php echo $number->currency($combo['OrdersCombo']['OrdersCombo']['total']); ?></td>
	    	</tr><?php
    	endforeach;
    }
    
    if(sizeof($order['OrdersItem']) <= 0 && sizeof($order['Pizza']) <= 0 && sizeof($order['OrdersSpecialsSize']) <= 0 && sizeof($order['Combo']) <= 0) {
    	?><tr><td colspan="3">Your order is empty.</td></tr><?php 
    } 
    ?>
    </table>
    <!-- list-table ui-table shopping-cart-list -->
    <table class="list-table  marg-t10">
	    <?php echo $this->element('table_accent_row', array('cols'=>2)); ?>
	    <?php $tax = ($order['Location']['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal']; ?>
	    <tr>
	    	<td><strong>Subtotal</strong></td>
	        <td style="width:80px;"><?php echo $number->currency($order['Order']['subtotal']); ?></td>
	    </tr>
	    <tr>
	        <td><strong>Tax (<?php echo $order['Location']['ZipCode']['tax_rate']; ?>%)</strong></td>
	        <td><?php echo $number->currency($tax); ?></td>
	    </tr>
	    <?php echo $this->element('table_accent_row', array('cols'=>2)); ?>
    </table>
    
    <?php if($order['Order']['order_method'] == 'Pickup') { ?>
    	<!-- if order is for pickup -->
	    <div id="pickupInfoDiv">
	    	<table class="list-table marg-t10">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr>
				        <td class="red">
				        	<span>Discount Applied (<?php echo $order['DiscountCode']['code']?>)</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td class="red" style="width:80px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr>
			    	<td><span class="b">Order Fee</span></td>
			    	<td style="width:80px;">$<?php echo number_format($order['Order']['oas_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr>
			        <td><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
    <?php } else { ?>
	    <!-- if order is for delivery -->
	    <div id="deliveryInfoDiv">
	    	<table class="list-table marg-t10">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] + $order['Location']['delivery_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr>
				        <td class="red">
				        	<span>Discount Applied (<?php echo $order['DiscountCode']['code']?>)</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td class="red" style="width:80px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr>
			    	<td><span class="b">Delivery Fee</span></td>
			    	<td style="width:80px;">$<?php echo number_format($order['Order']['oas_charge'] + $order['Location']['delivery_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr>
			        <td><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
    <?php } ?>
</div>