 <html>
<body>
<p>Dear <?=$name?>,</p>
<p>Welcome to OrderaSlice.com!  You have just made a smart decision that will change the way you order pizza for life! Never again wait on hold 
when you're hungry. Jump onto your computer and order exactly what you want, from where you want, with a few clicks of your mouse.</p>
<p>To get started, <?php echo $html->link('login to your account', 'http://orderaslice.com/users/login')?> now!</p>
<p>Best Regards,<br />The OrderaSlice Team<br /><?php echo $html->link('contact@orderaslice.com', 'mailto:contact@orderaslice.com'); ?></p>
<?php echo $html->image('orderaslice_logo_trans.png'); ?>
</body>
</html>