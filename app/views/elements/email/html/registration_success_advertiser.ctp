<html>
<body>
<p>Dear <?=$ad['Advertiser']['contact_fname']." ".$ad['Advertiser']['contact_lname'] ?>,</p>
<p>Welcome to OrderASlice.com, the most sophisticated online ordering system available. You are now ready to 
<?=$html->link('login', 'http://www.orderaslice.com/advertisers/login') ?> and start adding your restaurant locations for <b><?=$ad['Advertiser']['name']?></b>.</p>

<p>Your username login: <b><?=$ad['Advertiser']['username'] ?></b></p>

<p>Best Regards,<br />
The Order a Slice Team<br />
<?=$html->link('customer-service@orderaslice.com', 'mailto:customer-service@orderaslice.com') ?></p> 
</body>
</html>