<?php echo $this->element('email/html/templates/standard_hdr', array('title' => 'Your Online Menu Has Been Created!')); ?>				


<style type="text/css">
#outlook a {
	padding: 0;
}

body {
	width: 100% !important;
}

.ReadMsgBody {
	width: 100%;
}

.ExternalClass {
	width: 100%;
}

body {
	-webkit-text-size-adjust: none;
}

body {
	margin: 0;
	padding: 0;
}

img {
	border: 0;
	height: auto;
	line-height: 100%;
	outline: none;
	text-decoration: none;
}

table td {
	border-collapse: collapse;
}

#backgroundTable {
	height: 100% !important;
	margin: 0;
	padding: 0;
	width: 100% !important;
}

body,#backgroundTable {
	background-color: #FAFAFA;
}

#templateContainer {
	border: 1px solid #F67D07;
}

h1,.h1 {
	color: #202020;
	display: block;
	font-family: Arial;
	font-size: 40px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h2,.h2 {
	color: #404040;
	display: block;
	font-family: Arial;
	font-size: 18px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h3,.h3 {
	color: #606060;
	display: block;
	font-family: Arial;
	font-size: 16px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h4,.h4 {
	color: #808080;
	display: block;
	font-family: Arial;
	font-size: 14px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

#templatePreheader {
	background-color: #FAFAFA;
}

.preheaderContent div {
	color: #707070;
	font-family: Arial;
	font-size: 10px;
	line-height: 100%;
	text-align: left;
}

.preheaderContent div a:link,.preheaderContent div a:visited,.preheaderContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

#social div {
	text-align: right;
}

#templateHeader {
	background-color: #FFFFFF;
	border-bottom: 5px solid #505050;
}

.headerContent {
	color: #202020;
	font-family: Arial;
	font-size: 34px;
	font-weight: bold;
	line-height: 100%;
	padding: 10px;
	text-align: right;
	vertical-align: middle;
}

.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

#headerImage {
	height: auto;
	max-width: 600px !important;
}

#templateContainer,.bodyContent {
	background-color: #FDFDFD;
}

.bodyContent div {
	color: #505050;
	font-family: Arial;
	font-size: 14px;
	line-height: 150%;
	text-align: justify;
}

.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.bodyContent img {
	display: inline;
	height: auto;
}

#templateSidebar {
	background-color: #FDFDFD;
}

.sidebarContent {
	border-right: 1px solid #DDDDDD;
}

.sidebarContent div {
	color: #505050;
	font-family: Arial;
	font-size: 10px;
	line-height: 150%;
	text-align: left;
}

.sidebarContent div a:link,.sidebarContent div a:visited,.sidebarContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.sidebarContent img {
	display: inline;
	height: auto;
}

#templateFooter {
	background-color: #FAFAFA;
	border-top: 3px solid #909090;
}

.footerContent div {
	color: #707070;
	font-family: Arial;
	font-size: 11px;
	line-height: 125%;
	text-align: left;
}

.footerContent div a:link,.footerContent div a:visited,.footerContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.footerContent img {
	display: inline;
}

#social {
	background-color: #FFFFFF;
	border: 0;
}

#social div {
	text-align: left;
}

#utility {
	background-color: #FAFAFA;
	border-top: 0;
}

#utility div {
	text-align: left;
}

#monkeyRewards img {
	max-width: 170px !important;
}

h2,.h2 {
	color: #4F7A35;
}

h3,.h3 {
	color: #F67D07;
}

h4,.h4 {
	color: #F67D07;
}

.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts
	{
	font-weight: bold;
	color: #F67D07;
}
</style>

<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateBody">
<tr><td valign="top" class="bodyContent" style="border-collapse: collapse; background-color: #FDFDFD;"><!-- // Begin Module: Standard Content \\ -->
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tr><td valign="top" style="padding-left: 0; border-collapse: collapse;">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: justify;">
	<div style="margin-bottom:20px;">
		<strong>Welcome <?php echo $loc_name; ?>:</strong> Your new online menu has been created and is almost ready to accept online orders. 
		The final step before we can make your account active is to set up a Payment Profile so we can pay you for your incoming orders.
		Follow the short steps below to walk you through logging in to your new account, and setting up a Payment Profile.  
	</div>  

	<h3 class="h4" style="color: #4F7A35; display: block; font-family: Arial; font-size: 14px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Account Login
	</h3>
	<div style="margin-bottom:20px;">
		<a href="http://www.orderaslice.com/advertisers/login" style="color: #f67d07;">Log in to your account</a> using
		the username and password provided in your welcome email. (NOTE: If you have already logged into your account before and changed the password,
		then you must use your current password that you have specified.	
	</div>
	<?php echo $html->image('/img/emails/setting-up-billing-profile-image02.png', array('width' => '580', 'style' => 'border:1px solid #dddddd;')); ?>
	
	<h4 class="h4" style="color: #4F7A35; display: block; font-family: Arial; font-size: 14px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Change Default Password
	</h4>
	<div style="margin-bottom:20px;">
		The first time you log in to your account you will be asked to change your password from the default password that was given to
		you. This is a security measure to ensure that the account password in use was set by the restaurant owner and not the Order a Slice
		administrator who set up your account. Fill out the highlighted password fields and press the &ldquo;Update Settings&rdquo; button. After you
		change your password you will be redirected to the Billing Profile page.
	</div>
	<?php echo $html->image('/img/emails/setting-up-billing-profile-image00.png', array('width' => '580', 'style' => 'border:1px solid #dddddd;')); ?>
	
	<h4 class="h4" style="color: #4F7A35; display: block; font-family: Arial; font-size: 14px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Create Billing Profile
	</h4>
	<div style="margin-bottom:20px;">
		Fill out the form pictured below to create your billing profile.	
	</div>
	<?php echo $html->image('/img/emails/setting-up-billing-profile-image01.png', array('width' => '580', 'style' => 'border:1px solid #dddddd;')); ?>
</div>
</td>
</tr>
</table>
</td></tr>
</table>
<!-- // End Template Body \\ --></td>
				
<?php echo $this->element('email/html/templates/standard_ftr'); ?>