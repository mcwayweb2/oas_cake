<html>
<body>
<p>Dear <?=$location['Location']['contact_fname'] ?>,</p>
<p>Congratulations! You have successfully created your subscription for <b><?=$location['Location']['name'] ?></b>. This location is now ready 
to appear live on our site. You now must  
<?=$html->link('Publish', '/locations/publish/'.$location['Location']['id'])?> your menu to make your restaurant visible on our website. 
This step is necessary to let us know your menu is ready for the public to see. As long as your subscription remains active, you may publish, 
and unpublish your menu at any time. This is useful for making updates to your menu, without your customers seeing your changes 
in progress. Simply unpublish your menu to take it offline, make your changes, and publish it again when you're ready. Your online menu will be 
instantly updated with all your recent updates!</p>
<p>If you still need to set up your menu, visit the <?=$html->link('Menu Manager', '/menus/edit/'.$location['Location']['id']) ?>, before you 
publish your menu. If your menu is already set up, <?=$html->link('Publish', '/locations/publish/'.$location['Location']['id'])?> it now.</p>
<p>If you have any questions regarding the setup of your new restaurant menu, please contact us at 
<?=$html->link('customer-service@orderaslice.com', 'mailto:customer-service@orderaslice.com') ?>, and one of our representatives will be in contact 
with you in the next 24 hours.</p>
<p>Best Regards,<br >The Order A Slice Team</p>
</body>
</html> 