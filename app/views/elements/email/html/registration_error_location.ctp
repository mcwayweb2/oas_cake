<html>
<body>
<p>An error has occurred during a location subscription activation. The advertiser was charged, but the location was not set to active:</p>
<p><b>Advertiser Info:</b> (Advertiser Id: <?=$location['Advertiser']['id'] ?>)<br />
<?=$location['Advertiser']['name'] ?></p>
<p><b>Location Contact Info:</b>(Location Id: <?=$location['Location']['id'] ?>)<br />
<?=$location['Location']['name'] ?><br />
<?=$location['Location']['contact_fname']." ".$location['Location']['contact_lname'].", ".$location['Location']['contact_title'] ?><br />
<?=$location['Location']['contact_phone'] ?><br />
<?=$location['Location']['contact_email'] ?></p>
</body>
</html> 