<html>
<body>
<p>Dear <?=$name?>,</p>
<p>Your password has been reset for your advertiser account at <?=$html->link('orderaslice.com', 'http://orderaslice.com') ?></p>
<p>Your new temporary password is: <?=$password?></p>
<p><?=$html->link('Click here to login to your account.', 'http://orderaslice.com/advertisers/login')?></p>
<p>Sincerely,<br />The Order A Slice Team<br />customer-service@orderaslice.com</p>
</body>
</html>