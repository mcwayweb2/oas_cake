<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Your Order a Slice Menu has Been Created!</title>

</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
	  style="-webkit-text-size-adjust: none; margin: 0; padding: 0; background-color: #FAFAFA; width: 100% !important;">
<center>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable"
	style="margin: 0; padding: 0; background-color: #FAFAFA; height: 100% !important; width: 100% !important;">
	<tr>
		<td align="center" valign="top" style="border-collapse: collapse;"><!-- // Begin Template Preheader \\ -->
		<table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FAFAFA;">
			<tr>
				<td valign="top" class="preheaderContent"
					style="border-collapse: collapse;"><!-- // Begin Module: Standard Preheader \ -->
				<table border="0" cellpadding="10" cellspacing="0" width="100%">
					<tr>
						<td valign="top" style="border-collapse: collapse;">
						<div
							style="color: #707070; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;">Congratulations!
						Your online menu has been created! Follow the short steps listed
						below to set up a Billing Profile:</div>
						</td>
					</tr>
				</table>
				<!-- // End Module: Standard Preheader \ --></td>
			</tr>
		</table>
		<!-- // End Template Preheader \\ -->
		<table border="0" cellpadding="0" cellspacing="0" width="600"
			id="templateContainer"
			style="border: 1px solid #DDDDDD; background-color: #FDFDFD;">
			<tr>
				<td align="center" valign="top" style="border-collapse: collapse;">
				<!-- // Begin Template Header \\ -->
				<table border="0" cellpadding="0" cellspacing="0" width="600"
					id="templateHeader"
					style="background-color: #FFFFFF; border-bottom: 2px solid #f67d07;">
					<tr>
						<td class="headerContent"
							style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 10px; text-align: right; vertical-align: middle;">
						<div style="text-align: left;"><img
							src="http://gallery.mailchimp.com/8990687ccf19f7bcc2d20f9cf/images/orderaslice_logo_trans.1.1.png"
							alt="" border="0"
							style="margin: 0; padding: 0; max-width: 180px; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;"
							width="180px" height="92" id="headerImage campaign-icon"></div>
						</td>
						<td class="headerContent" width="100%"
							style="padding-left: 10px; padding-right: 20px; border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 10px; text-align: right; vertical-align: middle;">
						<div>
						<h2 style="color: #4F7A35; display: block; font-family: Arial; font-size: 18px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
						<?php echo $title; ?></h2>
						</div>
						</td>
					</tr>
				</table>
				<!-- // End Template Header \\ --></td>
			</tr>
			<tr>
				<td align="center" valign="top" style="border-collapse: collapse;">