<style type="text/css">
#outlook a {
	padding: 0;
}

body {
	width: 100% !important;
}

.ReadMsgBody {
	width: 100%;
}

.ExternalClass {
	width: 100%;
}

body {
	-webkit-text-size-adjust: none;
}

body {
	margin: 0;
	padding: 0;
}

img {
	border: 0;
	height: auto;
	line-height: 100%;
	outline: none;
	text-decoration: none;
}

table td {
	border-collapse: collapse;
}

#backgroundTable {
	height: 100% !important;
	margin: 0;
	padding: 0;
	width: 100% !important;
}

body,#backgroundTable {
	background-color: #FAFAFA;
}

#templateContainer {
	border: 1px solid #F67D07;
}

h1,.h1 {
	color: #202020;
	display: block;
	font-family: Arial;
	font-size: 40px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h2,.h2 {
	color: #404040;
	display: block;
	font-family: Arial;
	font-size: 18px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h3,.h3 {
	color: #606060;
	display: block;
	font-family: Arial;
	font-size: 16px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

h4,.h4 {
	color: #808080;
	display: block;
	font-family: Arial;
	font-size: 14px;
	font-weight: bold;
	line-height: 100%;
	margin-top: 2%;
	margin-right: 0;
	margin-bottom: 1%;
	margin-left: 0;
	text-align: left;
}

#templatePreheader {
	background-color: #FAFAFA;
}

.preheaderContent div {
	color: #707070;
	font-family: Arial;
	font-size: 10px;
	line-height: 100%;
	text-align: left;
}

.preheaderContent div a:link,.preheaderContent div a:visited,.preheaderContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

#social div {
	text-align: right;
}

#templateHeader {
	background-color: #FFFFFF;
	border-bottom: 5px solid #505050;
}

.headerContent {
	color: #202020;
	font-family: Arial;
	font-size: 34px;
	font-weight: bold;
	line-height: 100%;
	padding: 10px;
	text-align: right;
	vertical-align: middle;
}

.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

#headerImage {
	height: auto;
	max-width: 600px !important;
}

#templateContainer,.bodyContent {
	background-color: #FDFDFD;
}

.bodyContent div {
	color: #505050;
	font-family: Arial;
	font-size: 14px;
	line-height: 150%;
	text-align: justify;
}

.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.bodyContent img {
	display: inline;
	height: auto;
}

#templateSidebar {
	background-color: #FDFDFD;
}

.sidebarContent {
	border-right: 1px solid #DDDDDD;
}

.sidebarContent div {
	color: #505050;
	font-family: Arial;
	font-size: 10px;
	line-height: 150%;
	text-align: left;
}

.sidebarContent div a:link,.sidebarContent div a:visited,.sidebarContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.sidebarContent img {
	display: inline;
	height: auto;
}

#templateFooter {
	background-color: #FAFAFA;
	border-top: 3px solid #909090;
}

.footerContent div {
	color: #707070;
	font-family: Arial;
	font-size: 11px;
	line-height: 125%;
	text-align: left;
}

.footerContent div a:link,.footerContent div a:visited,.footerContent div a .yshortcuts
	{
	color: #336699;
	font-weight: normal;
	text-decoration: underline;
}

.footerContent img {
	display: inline;
}

#social {
	background-color: #FFFFFF;
	border: 0;
}

#social div {
	text-align: left;
}

#utility {
	background-color: #FAFAFA;
	border-top: 0;
}

#utility div {
	text-align: left;
}

#monkeyRewards img {
	max-width: 170px !important;
}

h2,.h2 {
	color: #4F7A35;
}

h3,.h3 {
	color: #F67D07;
}

h4,.h4 {
	color: #F67D07;
}

.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts
	{
	font-weight: bold;
	color: #F67D07;
}
</style>

				

<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateBody">
<tr><td valign="top" class="bodyContent" style="border-collapse: collapse; background-color: #FDFDFD;"><!-- // Begin Module: Standard Content \\ -->
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tr><td valign="top" style="padding-left: 0; border-collapse: collapse;">
<div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: justify;">
	<strong>Getting started:</strong> Customize your template by
	clicking on the style editor tabs up above. Set your fonts,
	colors, and styles. After setting your styling is all done you
	can click here in this area, delete the text, and start adding
	your own awesome content! <br>

	<h2 class="h2" style="color: #4F7A35; display: block; font-family: Arial; font-size: 18px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Heading 2
	</h2>
	<h3 class="h3" style="color: #F67D07; display: block; font-family: Arial; font-size: 16px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Heading 3
	</h3>
	<h4 class="h4" style="color: #F67D07; display: block; font-family: Arial; font-size: 14px; font-weight: bold; line-height: 100%; margin-top: 2%; margin-right: 0; margin-bottom: 1%; margin-left: 0; text-align: left;">
		Heading 4
	</h4>
	After you enter your content, highlight the text you want to
	style and select the options you set in the style editor in the
	"styles" drop down box. Want to <a
		href="http://www.mailchimp.com/kb/article/im-using-the-style-designer-and-i-cant-get-my-formatting-to-change"
		target="_blank"
		style="color: #F67D07; font-weight: bold; text-decoration: underline;">get
	rid of styling on a bit of text</a>, but having trouble doing
	it? Just use the "remove formatting" button to strip the text of
	any formatting and reset your style.
	
</div>
</td>
</tr>
</table>
</td></tr>
</table>
<!-- // End Template Body \\ --></td>