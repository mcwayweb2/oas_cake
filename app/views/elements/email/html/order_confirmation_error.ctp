<html>
<body>

<p>There was an error crediting a restaurant for the following order: </p>

<p><span style="font-weight:bold;">Restaurant:</span> <?=$order['Location']['name'] ?><br />
<span style="font-weight:bold;">Order Placed:</span> <?=$order['Order']['timestamp'] ?><br />
<span style="font-weight:bold;">Order Confirmed:</span> <?=$order['Order']['order_started_timestamp'] ?>
</p>

<p><span style="font-weight:bold;">Some things to check:</span><br />
<ul>
	<li>Does the restaurant have a default Payment Profile set?</li>
</ul>
</p>
</body>
</html>