<html>
<body>
<p>You have a new online order placed from Orderaslice.com ...</p>
<p><span style="font-weight: bold; padding-right: 5px; ">Order Placed:</span> <?=$order['Order']['timestamp']?></p>

<?php if($order['Location']['confirm_method'] == 'Email') { ?>
   	<p style="font-weight:bold;"><?php echo $html->link('Click Here To Confirm Your Order!', 'http://orderaslice.com/orders/confirm/'.$order['Order']['confirm_hash'], array('style' => 'color: #F67D07;')); ?></p>
<?php } else { ?>
   	<div style="font-weight:bold; color: red; font-size: 120%; text-align: center;">CALL 1-855-MY-SLICE TO CONFIRM THIS ORDER! 
   	( Enter Confirmation: <?php echo $order['Location']['id'].' * '.$order['Order']['confirm_hash'].'# )'; ?></div>
<?php } ?>	

<hr />
<p style="font-weight: bold;">Order For: <span style="color: #BB080D; padding-left: 5px; font-size:120%;"><?php echo $order['Order']['order_method']; ?></span></p>

<?php if($order['Order']['order_method'] == 'Delivery') { ?>
	<p><div style="font-weight: bold; margin-bottom:5px; ">Deliver To:</div>
	<span style="font-size:120%;">
		<span style=" font-weight: bold; "><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></span><br />
		"<?php echo $order['UsersAddr']['name']; ?>"<br />
		<?php echo $order['UsersAddr']['address']; ?><br />
		<?php echo $order['UsersAddr']['city'].', '.$order['UsersAddr']['zip']; ?><br />
		<?php echo $misc->formatPhone($order['User']['phone']); ?>
		
		<?php if(!empty($order['UsersAddr']['special_instructions'])) { ?>
			<div class="ui-helper-clearfix">
				<div class="left pad-r5 b">Special Instructions:</div>
				<div class="left"><?php echo $order['UsersAddr']['special_instructions']; ?></div>
			</div>
		<?php } ?>
	</span>
	</p>
	<?php if($order['Location']['acct_type'] > 1 && $order['Order']['order_method'] == 'Delivery') {  
		echo $html->link('Login for Delivery Directions', 'http://orderaslice.com/advertisers/login'); 
	} 	
} else { ?>
	<p><span style="font-size:120%;">
		<span style=" font-weight: bold; "><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></span><br />
		<?php echo $misc->formatPhone($order['User']['phone']); ?>
	</span></p>
<?php } ?>
<hr />

<div class="orange-border ui-widget-content">
	<table style="width: 100%;">
	    <tr>
		    <th style="text-align: left; width:60px;">Qty</th>
		    <th style="text-align: left;"><span>Item</span></th>
	        <th style="width:100px; text-align: left;"><span>Price</span></th>
	    </tr>
	    <?
	    echo $this->element('table_accent_row', array('cols'=>3));
	    if(sizeof($order['OrdersItem']) > 0) {
		    foreach($order['OrdersItem'] as $item): ?>
		    	<tr>
		    		<td style="font-weight: bold; vertical-align: top;">(<?=$item['OrdersItem']['qty']?>)</td>
		    		<td>
		    			<div style=" font-weight:bold; margin-bottom:5px;"><?=$item['Item']['title']?></div>
		    			<div style=""><?=$item['Item']['description']?></div>
		    			<?php 
		    				if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
		    					?><ul style="text-decoration:none;  margin-top: 5px; color: #BB080D; font-weight: bold;" ><?php 
		    					foreach($item['ItemsOption'] as $opt):
									echo '<li>'.$opt['label'];
									if($opt['OrdersItemsItemsOption']['price'] > 0) {
										echo '<span class=""> (+$'.number_format($opt['OrdersItemsItemsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					
		    					foreach($item['LocationsCatsOption'] as $opt):
									echo '<li>'.$opt['label'];
									if($opt['OrdersItemsLocationsCatsOption']['price'] > 0) {
										echo '<span style="padding-left: 5px;"> (+$'.number_format($opt['OrdersItemsLocationsCatsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					?></ul><?php
		    				}
		    				
		    				if(!empty($item['OrdersItem']['comments'])) {
		    					?><div style="margin-top: 5px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;"><?php echo $item['OrdersItem']['comments']; ?></span></div><?php 
		    				}
		    			?>
		    		</td>
		    		<td style="font-weight: bold; vertical-align: top;"><?php echo $number->currency($item['OrdersItem']['total']); ?></td>
		    	</tr><?
		    endforeach;
	    }
	    
		if(sizeof($order['Pizza']) > 0) {
		    foreach($order['Pizza'] as $pizza): ?>
		    	<tr>
		    		<td style="font-weight: bold; vertical-align: top;">(<?=$pizza['qty']?>)</td>
		    		<td>
		    			<div style=" font-weight:bold;">
		    				<?php echo $pizza['Size']['size'].'" '.$pizza['Size']['title'].' Pizza'; ?>
		    			</div>
	    				<?php 
	    				$lines = array();
						if($pizza['Crust']['LocationsCrust']['price'] > 0) {
	    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
	    				} else {
	    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
	    				}
	    				
	    				if($pizza['sauce'] == 'Extra Sauce') {
	    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
	    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
	    				
						if($pizza['cheese'] == 'Extra Cheese') {
	    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
	    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
	    				
	    				if(sizeof($pizza['Topping']) > 0) {
							foreach($pizza['Topping'] as $topping):
								$l = 'Add '.$topping['Topping']['title'];
								if($topping['PizzasTopping']['placement'] != 'Whole') {
									$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
								}
								
								if($topping['PizzasTopping']['price'] > 0) {
									$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
								}
								
								$lines[] = $l;
							endforeach;
	    				}
	    				
	    				if(sizeof($lines) > 0) {
	    					echo "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
							foreach($lines as $line):
								echo "<li>".$line."</li>";
							endforeach;
	    					echo "</ul></div>";
	    				}
		    				 
						if(!empty($pizza['comments'])) {
		    				?><div style="margin-top: 5px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;"><?php echo $pizza['comments']; ?></span></div><?php 
		    			} ?>
		    		</td>
		    		<td style="font-weight: bold; vertical-align: top;"><?php echo $number->currency($pizza['total']); ?></td>
		    	</tr><?	
		    endforeach;
	    }
	    
	    if(sizeof($order['OrdersSpecialsSize']) > 0) {
	    foreach($order['OrdersSpecialsSize'] as $pizza): ?>
	    	<tr>
	    		<td style="font-weight: bold; vertical-align: top;">(<?=$pizza['qty']?>)</td>
	    		<td>
	    			<div style=" font-weight:bold;">
	    				<?php echo $pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Specialty Pizza"; ?>
	    			</div>
	    			<?php 
    				$lines = array();
    				if($pizza['sauce'] == 'Extra Sauce') {
    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
    				
					if($pizza['cheese'] == 'Extra Cheese') {
    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
    				
    				if($pizza['Crust']['LocationsCrust']['price'] > 0) {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
    				} else {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
    				}
    				/*
    				if(sizeof($pizza['Topping']) > 0) {
						foreach($pizza['Topping'] as $topping):
							$l = 'Add '.$topping['Topping']['title'];
							if($topping['PizzasTopping']['placement'] != 'Whole') {
								$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
							}
							
							if($topping['PizzasTopping']['price'] > 0) {
								$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
							}
							
							$lines[] = $l;
						endforeach;
    				} */
    				
    				if(sizeof($lines) > 0) {
    					echo "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
						foreach($lines as $line):
							echo "<li>".$line."</li>";
						endforeach;
    					echo "</ul></div>";
    				}
	    				 
					if(!empty($pizza['comments'])) {
	    				?><div style="margin-top: 5px;">Additional Instructions:<span style="padding-left: 5px; font-style: italic;"><?php echo $pizza['comments']; ?></span></div><?php 
	    			} ?>
	    		</td>
	    		<td style="font-weight: bold; vertical-align: top;"><?php echo $number->currency($pizza['total']); ?></td>
	    	</tr><?	
	    endforeach;
	    }
	    
	    if(sizeof($order['OrdersCombo']) > 0) {
	    	foreach($order['OrdersCombo'] as $combo_index => $combo): ?>
	    		<tr>
		    		<td style="font-weight: bold; vertical-align: top;">(<?=$combo['qty']?>)</td>
		    		<td>
		    			<div style=" font-weight:bold; margin-bottom: 5px; "><?=$combo['OrdersCombo']['Combo']['title']?> Combo</div>
		    			<div class=""><?=$combo['OrdersCombo']['Combo']['description']?></div>
		    			<?php
							if(sizeof($combo['OrdersCombo']['OrdersCombosPizza']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosPizza'] as $pizza):
									$lines = array();
				    				if($pizza['sauce'] == 'Extra Sauce') {
				    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
				    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
				    				
									if($pizza['cheese'] == 'Extra Cheese') {
				    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
				    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
				    				
				    				if($pizza['crust_price'] > 0) {
				    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
				    				} else {
				    					$lines[] = $pizza['Crust']['title'].' Crust';
				    				}
				    				
				    				if(sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['title'];
											if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
											}
											
											if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
												$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
											}
											
											$lines[] = $l;
										endforeach;
				    				}
				    				
			    					echo "<div style='padding-left: 20px; margin-top: 10px;  font-weight: bold;'>".$pizza['Size']['Size']['size']."\" ".$pizza['Size']['Size']['title']." Pizza</div>";
			    					echo "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
									foreach($lines as $line):
										echo "<li>".$line."</li>";
									endforeach;
			    					echo "</ul></div>";
								endforeach;
							}
		    			
							if(sizeof($combo['OrdersCombo']['OrdersCombosSpecialsSize']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosSpecialsSize'] as $pizza):
									$lines = array();
				    				if($pizza['sauce'] == 'Extra Sauce') {
				    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
				    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
				    				
									if($pizza['cheese'] == 'Extra Cheese') {
				    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
				    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
				    				
				    				if($pizza['crust_price'] > 0) {
				    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
				    				} else {
				    					$lines[] = $pizza['Crust']['title'].' Crust';
				    				}
				    				/*
				    				if(sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['title'];
											if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
											}
											
											if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
												$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
											}
											
											$lines[] = $l;
										endforeach;
				    				}
				    				*/
				    				
			    					echo "<div style='padding-left: 20px; margin-top: 10px;  font-weight: bold;'>".$pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Pizza</div>";
			    					if(!empty($pizza['Special']['subtext'])) {
			    						echo "<div style='padding-left: 20px;'>".$pizza['Special']['subtext']."</div>";	
			    					}
			    					echo "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
									foreach($lines as $line):
										echo "<li>".$line."</li>";
									endforeach;
			    					echo "</ul></div>";
								endforeach;
							}
							
	    					if(sizeof($combo['OrdersCombo']['OrdersCombosItem']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosItem'] as $item): ?>
									<div style='padding-left: 20px; margin-top: 10px;  font-weight: bold;'><?php echo $item['Item']['title']; ?></div>
									<div style='padding-left: 20px;'><?php echo $item['Item']['description']; ?></div><?php

			    					if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
				    					?><div style='padding-left:20px;'><ul style=' text-decoration: none; color: #BB080D; font-weight: bold;'><?php 
				    					foreach($item['ItemsOption'] as $opt):
											echo '<li>'.$opt['label'];
											if($opt['OrdersCombosItemsItemsOption']['price'] > 0) {
												echo '<span class=""> (+$'.number_format($opt['OrdersCombosItemsItemsOption']['price'], 2).')</span>';	
											}	
				    					endforeach;
				    					
				    					foreach($item['LocationsCatsOption'] as $opt):
											echo '<li>'.$opt['label'];
											if($opt['OrdersCombosItemsLocationsCatsOption']['price'] > 0) {
												echo '<span style="padding-left: 5px;"> (+$'.number_format($opt['OrdersCombosItemsLocationsCatsOption']['price'], 2).')</span>';	
											}	
				    					endforeach;
				    					?></ul></div><?php
				    				}
								endforeach;
							}
						
		    				if(!empty($combo['OrdersCombo']['OrdersCombo']['comments'])) {
		    					?><div style="margin-top: 10px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;"><?php echo $combo['OrdersCombo']['OrdersCombo']['comments']; ?></span></div><?php 
		    				}
		    				
		    			?>
		    		</td>
		    		<td style="font-weight: bold; vertical-align: top;"><?php echo $number->currency($combo['OrdersCombo']['OrdersCombo']['total']); ?></td>
		    	</tr><?
	    	endforeach;
	    }
	    ?>
    </table>
    <hr />
    
    <table style="width: 100%; margin-top: 10px; ">
	    <?php echo $this->element('table_accent_row', array('cols'=>3)); ?>
	    <?php $tax = ($order['Location']['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal']; ?>
	    <tr style="font-weight:bold;">
	    	<td colspan="2"><strong>Subtotal</strong></td>
	        <td style="width:100px;"><?php echo $number->currency($order['Order']['subtotal']); ?></td>
	    </tr>
	    <tr style="font-weight:bold;">
	        <td colspan="2"><strong>Tax (<?php echo $order['Location']['ZipCode']['tax_rate']; ?>%)</strong></td>
	        <td><?php echo $number->currency($tax); ?></td>
	    </tr>
	    <?php echo $this->element('table_accent_row', array('cols'=>3)); ?>
    </table>
    <hr />
    
    <?php if($order['Order']['order_method'] == 'Pickup') { ?>   
	    <!-- if order is for pickup -->
	    <div id="pickupInfoDiv">
	    	<table style="width: 100%; margin-top: 10px;">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr style="font-weight: bold;">
				        <td style="color: #BB080D;" colspan="2">
				        	<span>Discount Applied (<?php echo $order['DiscountCode']['code']?>)</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td style="color: #BB080D;" style="width:100px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr style="font-weight: bold;">
			    	<td colspan="2">Order Fee</td>
			    	<td style="width:100px;">$<?php echo number_format($order['Order']['oas_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr style="font-weight: bold; ">
			        <td colspan="2"><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
    <?php } else { ?>
	    <!-- if order is for delivery -->
	    <div id="deliveryInfoDiv">
	    	<table style="width: 100%; margin-top: 10px; ">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] + $order['Location']['delivery_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr style="font-weight: bold;">
				        <td style="color: #BB080D;" colspan="2">
				        	<span>Discount Applied</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td style="color: #BB080D;" style="width:100px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr style="font-weight: bold;">
			    	<td colspan="2"><span>Delivery Fee</span></td>
			    	<td style="width:100px;">$<?php echo number_format($order['Order']['oas_charge'] + $order['Location']['delivery_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr style="font-weight: bold; ">
			        <td colspan="2"><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
    <?php } ?>
    <hr /><hr style="margin-bottom: 20px;"/>
    <?php if($order['Location']['confirm_method'] == 'Email') { ?>
    	<p style="font-weight:bold;"><?php echo $html->link('Click Here To Confirm Your Order!', 'http://orderaslice.com/orders/confirm/'.$order['Order']['confirm_hash'], array('style' => 'color: #F67D07;')); ?></p>
    <?php } else { ?>
    	<div style="font-weight:bold; color: red; font-size: 130%; text-align:center; ">CALL 1-855-MY-SLICE TO CONFIRM THIS ORDER! 
    	( Enter Confirmation: <?php echo $order['Location']['id'].' * '.$order['Order']['confirm_hash'].'# )'; ?></div>
    <?php } ?>	
</div>
</body>
</html>