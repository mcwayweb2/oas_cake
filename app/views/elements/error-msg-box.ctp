<?php if(!isset($msg)) { ?>
<script>
$(function() {
	$('.delay-error').delay(8000).slideUp();
});
</script>
<?php } ?>

<div class="marg-b10 <?php if(!isset($msg)) echo "delay-error"; ?>">
<div class="ui-widget ui-widget-content ui-state-error ui-corner-all"> 
	<div class="ui-helper-clearfix">
		<div class="left">
			<?php if(empty($no_icon)) { ?><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span><?php } ?>
		</div>
		<div class="left b">
			<?php 
				if(isset($msg)) { echo $msg; }
				else { echo $session->flash(); } 
			 ?>
		</div>
	</div> 
</div>
</div>