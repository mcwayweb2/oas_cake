<fieldset><legend>Payment Information</legend>
	<?php echo $form->create('User', array('action' => 'charge_card')); ?>
	<table>
		<tr>
			<td class="label-req">First Name on card: *</td>
			<td><?=$form->input('billing_fname', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<td class="label-req">Last Name on card: *</td>
			<td><?=$form->input('billing_lname', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?=$html->image('creditcard_amex.gif')?>   
			</td>
		</tr>
		<tr>
			<td class="label-req">Card Number: *</td>
			<td><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<td class="label-req">Card Exp: *</td>
			<td><?=$form->month('month', null, array('class'=>'form-select'), true)?> <?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select'), true)?></td>
		</tr>
		<tr>
			<td></td>
			<td><?php echo $form->end('Process'); ?></td>
		</tr>
	</table>
</fieldset>