<div id="userNav">
	<div class="clearfix">
		<div class="left"><?php echo $html->link($html->image('btn-my-orders-active.png'), '/user-dash/orders', array('escape'=>false)); ?></div>
		<div class="right basefont">
			<?php echo $html->link($html->image('btn-contact-profile.png'), '/user-dash/profile', array('escape'=>false)); ?> 
			<?php echo $html->link($html->image('btn-manage-addr.png'), '/user-dash/addresses', array('escape'=>false)); ?>  
		</div>
	</div>
	<div class="clear"></div>
</div>