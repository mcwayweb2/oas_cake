<script>
$(function() {
	$(".ui-table tr").hover( function() {
		$(this).children("td").toggleClass("ui-row-hover");
	});
	$(".ui-table tr").click( function() {
		$(this).children("td").toggleClass("ui-row-highlight");
	});
	$(".ui-table tr:even td").addClass('alt-row');
});
</script>
<?php if(!isset($no_fieldset)) { ?>
<fieldset class="marg-b10 ui-widget-content ui-corner-all"><legend class="ui-corner-all">Choose Toppings...</legend>
<?php } ?>
 	<?php $options = array('Left' => '', 'Whole' => '', 'Right'  => '', 'None' => '');	?>
 	<table class="ui-table list-table">
 		<tr>
 			<th></th>
 			<th>Left Side Only</th>
 			<th>Whole Pizza</th>
 			<th>Right Side Only</th>
 			<th>None</th>
 		</tr>
 		<?php 
 		echo $this->element('table_accent_row', array('cols'=>5));
 		$current_type = null;
 		foreach($toppings as $topping):
 			//display topping type label above the first topping of its type
 			if($current_type != $topping['ToppingType']['title']) {
 				?>
 				<tr><td colspan="5" class="green"><?php echo $topping['ToppingType']['title']; ?></td></tr>
 				<?php 
 			}
 			?><tr><td class="base"><?php echo $topping['Topping']['title']; ?></td><td><?php 
 			echo $form->radio('topping.'.$topping['LocationsTopping']['topping_id'], $options, 
 							  array('legend'=>FALSE, 'separator'=>'</td><td>', 'value'=>'None'));
 			?></td></tr><?php
			$current_type = $topping['ToppingType']['title'];
 		endforeach;
 		echo $this->element('table_accent_row', array('cols'=>5));
 		?>
 	</table>
 <?php if(!isset($no_fieldset)) { ?></fieldset><?php } ?>