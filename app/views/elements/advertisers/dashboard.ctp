<?php 
	$opts = array('autoHeight' => 'false');
	//do we have a current index to set?	
	if(isset($current_loc_index)) $opts['active'] = $current_loc_index;
	
	echo $this->element('js/jquery/accordion', array('name' => 'advertiserLocationsAccordion', 'opts' => $opts));
?>
<script>
$(function() { 
	$('#JumpToMenuSelectProgress').hide();
	$('#JumpToMenuSelect').change(function() {
		$('#JumpToMenuSelectProgress').show();
		var url = '/restaurants/edit_menu/' + $(this).val();
		window.location = url;
	});
});
</script>


<div id="side-features" >
<?php //if(strlen($session->read('Advertiser.name')) <= 20)?>
	<h2 <?php echo (strlen($session->read('Advertiser.name')) <= 20) ? "class='icon-feature marg-b10'" : "style='padding-bottom: 4px'"; ?>>
		<?php echo $session->read('Advertiser.name'); ?>
	</h2>
    <div class="orange-border">
    	<div class="ui-widget ui-widget-content ui-corner-all">
            <div><span class="orange b">Primary Contact:</span><hr />
            	<span class="base">
            		<?php echo $session->read('Advertiser.contact_fname')." ".$session->read('Advertiser.contact_lname').", ".$session->read('Advertiser.contact_title')?><br />
            		<?=$html->formatPhone($session->read('Advertiser.contact_phone'));?><br />
            		<?=$session->read('Advertiser.email')?>
            	</span>
        	</div>
        </div>
    </div>
    
    <div class="orange-border marg-t10">
    <div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix">
    	<div class="left pad-r5" style="width:135px;">
    		<?php echo $misc->jqueryBtn('/advertisers/edit', 'ui-icon-person', 'Account Settings'); ?>
        	
    	</div>
        <div class="left" style="width:125px;">
        	<?php echo $misc->jqueryBtn('/advertisers/payment_profiles', 'ui-icon-suitcase', 'Billing Profiles'); ?>
        </div>
          
        <div class="left pad-r5" style="width:135px;">
        	<?php echo $misc->jqueryBtn('/discount_codes/manage', 'ui-icon-star', 'Promotion Codes', null, null, null, null); ?>
        </div>
        <div class="left" style="width:125px;">&nbsp;
        	<?php //echo $jquery->btn('/advertisers/manage_ads', 'ui-icon-image', 'Advertisements', null, null, null, null); ?>
        </div>
    </div>
    </div>
</div>

<div class="ui-helper-clearfix">
	<div class="left"><h1>My Restaurants</h1></div>
	<div class="right"><?php echo $misc->jqueryBtn('/restaurants/add', 'ui-icon-circle-plus', 'Add New Restaurant'); ?></div>
</div><hr />
<div class="ui-helper-clearfix marg-b5">
	<div class="left pad-r5">
		<?php echo $form->select('jump_to_menu', $ad_loc_list, null, array('class'=>'ui-state-default', 'id'=>'JumpToMenuSelect'), 'Jump To Restaurant Menu'); ?>
	</div>
	<div class="left" id="JumpToMenuSelectProgress"><?php echo $html->image('ajax-loader-sm-bar.gif', array('style'=>'')); ?></div>
</div>
<?php
//echo debug($ad_locations);
//if there is at least 1 location set up
if(sizeof($ad_locations) > 0) {
	?><div class="ui-widget ui-accordion" id="advertiserLocationsAccordion"><?php
	foreach ($ad_locations as $adloc):
		?>
		<h2 class="ui-widget-header ui-accordion-header" id="ad-location-hdr-<?php echo $adloc['Location']['id']; ?>">
         	<a href="#"><?php echo $adloc['Location']['name']; ?></a>
         </h2>
         <div class="ui-widget-content ui-accordion-content" id="ad-location-drawer-<?php echo $adloc['Location']['id']; ?>" style="height:200px;">
         	<?php
         	if($adloc['Location']['active'] == '1' && $adloc['Location']['menu_ready'] == '1') { 
         		echo $this->element('warning-msg-box', array('msg'=>'Menu is Online. '.$html->link('(Take Menu Offline.)', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/0', array(), 
         																						   'Are you sure that you want to take this menu completely Offline? If you do, you will not be able to receive any online orders until your menu is placed back Online.')));
         	} else {
         		//menu is not currently active. 
         		if($adloc['Location']['active'] == '1') {
         			//menu ready, but taken down by client
         			echo $this->element('error-msg-box', array('msg'=>'Menu is Offline: '.$html->link('Put Menu Online', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1')));		
         		} else if($adloc['Location']['menu_ready'] == '1') {
         			echo $this->element('error-msg-box', array('msg'=>'Menu is Offline: Pending Approval.'));	
         		} else {
         			echo $this->element('error-msg-box', array('msg'=>'Menu is Offline: Pending Setup.'));
         		}
         	} 
         	?>
			<div class="ui-helper-clearfix">
				<div class="left"><?php echo $jquery->btn('/restaurants/edit_menu/'.$adloc['Location']['slug'], 'ui-icon-contact', 'Menu Manager'); ?></div>
				<div class="right">
					<?php echo $jquery->btn('/restaurants/edit/'.$adloc['Location']['slug'], 'ui-icon-pencil', 'Edit Location', null, null, null, '110'); ?>
				</div>
				<!--<div class="right"><?php //echo $jquery->btn('/locations/manage_ads/'.$adloc['Location']['id'], 'ui-icon-image', 'Ad Manager', null, null, null, '110'); ?></div>-->
			</div>
			<hr class="marg-t10" />
         
         	<div class="ui-helper-clearfix marg-b5">
         		<div class="left pad-r10"><?php
         			echo $html->image($misc->calcImagePath($adloc['Location']['logo'], 'files/logos/'), array('style'=>'width:85px;', 'class'=>'orange-border pad2'));
         		?></div>
         		<div class="left">
         			<div class="ui-helper-clearfix">
						<div class="left" style="width:45px;">
						<strong>Phone:<br />Fax:<br />SMS:</strong>
						</div>
						<div class="left" style="width:100px;">
							<?php echo $html->formatPhone($adloc['Location']['phone']); ?><br />
							<?php echo (!empty($adloc['Location']['fax'])) ? $html->formatPhone($adloc['Location']['fax']) : 'None Listed'; ?><br />
							<?php echo (!empty($adloc['Location']['sms'])) ? $html->formatPhone($adloc['Location']['sms']) : 'None Listed'; ?>
						</div>
					</div>
         		</div>
         	</div>
         	<?php if($adloc['Location']['acct_type'] == '1') { 
         		echo $jquery->btn('/restaurants/upgrade', 'ui-icon-arrowreturnthick-1-n', 'Upgrade to Premium Account', null, null, null, '210');
			} ?>
		</div>
		<?php 
	endforeach;
	echo "</div>";
} else {
	?><p>Start by adding a restaurant location with the button above.</p><?php 	
}
?>