<?php if(sizeof($sizes) <= 0 || sizeof($crusts) <= 0 || sizeof($toppings) <= 0 || sizeof($specials) <= 0 || sizeof($cats) <= 0 || sizeof($cats[0]['Items']) <= 0) { ?>
<div id="menuChecklist" class="ui-widget-content ui-corner-all marg-b10">
	<p><b>Welcome</b> to your Menu Manager. Here, you will build your online menu, as well as manage all your menu settings in the future. To get your 
	menu online in as little time as possible, we have provided a list of items that you will need to setup, before you can publish your menu 
	live on the website.</p><hr />
	
	<div class="ui-helper-clearfix">
		<div class="left" style="width:280px;">
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($sizes) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($sizes) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '1) Setup Sizes and Prices', 'no_icon' => true)); ?></div>
			</div>
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($crusts) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($crusts) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '2) Setup Crust Types', 'no_icon' => true)); ?></div>
			</div>
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($toppings) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($toppings) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '3) Setup Available Toppings', 'no_icon' => true)); ?></div>
			</div>
		</div>
		<div class="right" style="width:280px;">
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($specials) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($specials) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '4) Setup Specialty Pizzas', 'no_icon' => true)); ?></div>
			</div>
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($cats) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($cats) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '5) Setup Menu Categories', 'no_icon' => true)); ?></div>
			</div>
			<div class="ui-helper-clearfix">
				<div class="left" style="width:90px;"><?php echo $html->image((sizeof($cats[0]['Items']) > 0) ? 'btn-complete-sm.png':'btn-incomplete-sm.png'); ?></div>
				<div class="left" style="width:180px;"><?php echo $this->element((sizeof($cats[0]['Items']) > 0) ? 'warning-msg-box':'error-msg-box', 
																				 array('msg' => '6) Setup Menu Items', 'no_icon' => true)); ?></div>
			</div>
		</div>
	</div>
</div>
<?php } ?>