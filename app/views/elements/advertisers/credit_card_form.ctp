<fieldset class='marg-b10'>
	<?php if(!$no_legend) { ?>
		<legend>Credit Card Billing Details</legend>
	<?php } ?>
	<div class="ui-helper-clearfix">
		<div class="left">
			<table class="form-table">
				<tr>
					<td class="label-req" style="width:150px;">First Name on Card: <span class="required">*</span></td>
					<td><?=$form->input('fname', array('class'=>'form-text cc-field', 'label'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Last Name on Card: <span class="required">*</span></td>
					<td><?=$form->input('lname', array('class'=>'form-text cc-field', 'label'=>''))?></td>
				</tr>
				<tr>
		 			<td class="label-req">Address: <span class="required">*</span></td>
					<td><?=$form->input('address', array('label'=>'','class'=>'form-text cc-field'))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">City: <span class="required">*</span></td>
		 			<td><?=$form->input('city', array('label'=>'','class'=>'form-text cc-field'))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">State: <span class="required">*</span></td>
		 			<td><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-text cc-field'), false)?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Zip: <span class="required">*</span></td>
		 			<td><?=$form->input('zip', array('label'=>'','class'=>'form-text cc-field'))?></td>
		 		</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
					<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?php //echo $html->image('creditcard_amex.gif')?>   
					</td>
				</tr>
				<tr>
					<td class="label-req">Card Number: <span class="required">*</span></td>
					<td><?=$form->input('cc_num', array('class'=>'form-text num-only cc-field', 'label'=>'', 'value'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Month: <span class="required">*</span></td>
					<td><?=$form->month('month', null, array('class'=>'cc-field', 'style'=>'width:100px;'), false)?></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Year: <span class="required">*</span></td>
					<td><?=$form->year('year', date("Y"), date("Y")+10, null, array('class'=>'form-select cc-field'), false)?></td>
				</tr>
			</table>
		</div>
		<div class="right marg-t20">
			<div class="marg-b10"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
			<div style="padding-left:25px;"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
		</div>
	</div>
</fieldset>