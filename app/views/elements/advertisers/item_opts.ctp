<?php
$nm_prefix = "data[CombosItem][".$combos_item_id."][".$array_index."]";
$class = (isset($rand_class)) ? $rand_class : ''; 
	
if(isset($ajax_item['ItemsOption'])) { 
	foreach($ajax_item['ItemsOption'] as $add_group_id => $opts):
		if($add_group_id > 0) {
			//display select boxes 
			?><div class="marg-b5"><?php 
			for($i = 1; $i <= $opts['add_group_limit']; $i++) { ?>
				<div class="ui-helper-clearfix marg-b10">
					<div class="left txt-r label-req pad-r5" style="width:180px;">
						<?php echo ($opts['add_group_limit'] > 1) ? 'Selection '.$i.':' : '&nbsp;'; ?>
					</div>
					<div class="left pad-l5">
						<?php 
						$name = $nm_prefix."[ItemsOption][".$add_group_id."][".$i."]";
						echo $form->select($name, $opts['OptList'], null, 
										   array('class'=>'form-text '.$class,'label'=>'', 'name'=>$name, 'id'=>$misc->prefixNameToId($name)), false); 												 
												 
						?>
					</div>
				</div><?php	
			} 
			?></div><?php 
		} else {
			//display individual checkboxes for each
			foreach($opts['OptList'] as $option_id => $opt): ?>
				<div class="ui-helper-clearfix marg-b5">
					<div class="left txt-r" style="width:215px;"><?php
						$name = $nm_prefix."[ItemsOption][".$add_group_id."][".$option_id."]";
						echo $form->checkbox($name, array('class' => $class, 'name'=>$name, 'id'=>$misc->prefixNameToId($name)));
					?></div>
					<div class="left pad-l5"  style="width:280px;"><?php echo $opt; ?></div>
				</div>
				<?php 
			endforeach;
			
		}
	endforeach;
} ?>

<?php if(isset($ajax_item['CatsOption'])) { 
	foreach($ajax_item['CatsOption'] as $add_group_id => $opts):
		if($add_group_id > 0) {
			//display select boxes 
			?><div class="marg-b5"><?php 
			for($i = 1; $i <= $opts['add_group_limit']; $i++) { ?>
				<div class="ui-helper-clearfix marg-b10">
					<div class="left txt-r label-req pad-r5" style="width:180px;">
						<?php echo ($opts['add_group_limit'] > 1) ? 'Selection '.$i.':' : '&nbsp;'; ?>
					</div>
					<div class="left pad-l5"><?php
						$name = $nm_prefix."[CatsOption][".$add_group_id."][".$i."]"; 
						echo $form->select($name, $opts['OptList'], null, 
										   array('class'=>'form-text '.$class,'label'=>'', 'name'=>$name, 'id'=>$misc->prefixNameToId($name)), false); 
					?></div>
				</div><?php	
			} 
			?></div><?php 
		} else {
			//display individual checkboxes for each
			foreach($opts['OptList'] as $option_id => $opt): ?>
				<div class="ui-helper-clearfix marg-b5">
					<div class="left txt-r" style="width:215px;"><?php 
						$name = $nm_prefix."[CatsOption][".$add_group_id."][".$option_id."]";
						echo $form->checkbox($name, array('class' => $class, 'name'=>$name, 'id'=>$misc->prefixNameToId($name))); ?>
					</div>
					<div class="left pad-l5" style="width:280px;"><?php echo $opt; ?></div>
				</div>
				<?php 
			endforeach;
		}
	endforeach;
} ?>
