<script>
$(function() {
	$('.ajax-loader-sm-circle').hide();
	$('#btnConfirmPromoCode').click(function() {
		$('.ajax-loader-sm-circle').show();
		 
		$('#ajaxConfirmPromoCodeContent').load('/discount_codes/ajax_validate_code', { code: $('#DiscountCodeCode').val() }, function() {
			//$('ajaxUpdateTotalsContent').load();
			$('.ajax-loader-sm-circle').hide();
		});
		return false;
	});

	$('#AlternatePayMethod').hide();
	$('#AlternatePayMethodLink').click(function() {
		$('#AlternatePayMethodToggle').hide();
		$('#OrderUsersPaymentProfileIdNew').attr('checked', 'checked');
		$('#AlternatePayMethod').slideDown('slow');
	});

	$('#OrderSaveProfile').click(function() {
		if($(this).attr('checked')) {
			$('#profileNameDiv').slideDown();
		} else {
			$('#profileNameDiv').hide();
		}  
		
	});

	$('.submitProgress').hide();
	$('.btnOrderFinalize').click(function() {
		$('.submitBtn').hide();
		$('.submitProgress').show();
		$('#formOrderFinalize').submit();
	});

	$('.order-method-class').click( function() {
		if($(this).val() == 'Pickup') {
			$('#deliveryInfoDiv, #deliveryAddrDiv').hide();
			$('#pickupInfoDiv').show();
		} else {
			$('#pickupInfoDiv').hide();
			$('#deliveryInfoDiv').show();
			$('#deliveryAddrDiv').slideDown('slow');
		}
	});

	<?php if($order['Location']['accept_deliveries'] == '1') { ?>
		$('#pickupInfoDiv').hide();
	<?php } else { ?>
		$('#deliveryInfoDiv').hide();
	<?php } ?>
	
});
</script>
<?php echo $jquery->scrFormSubmit('.btnOrderFinalize', '#formOrderFinalize'); ?>

<?php echo $form->create('Order', array('action'=>'finalize', 'id'=>'formOrderFinalize'))?>
<div id="side-guestcheck" class="ui-corner-all">
	<h2 class="icon-feature">My Order Options</h2>
	
	<div class="orange-border marg-b10">
	<div class="ui-widget-content ui-corner-all">
		<div class="orange b marg-b10">Choose Order Type:</div><hr />
		<?php if($order['Location']['accept_pickups'] == '1') { ?>
			<div class="ui-helper-clearfix b">
				<div class="left pad-r10">
					<input type="radio" id="OrderOrderMethodPickup" value="Pickup" name="data[Order][order_method]" checked="checked" class="order-method-class" />
				</div>
				<div class="left"><div class="green">Pickup Order</div></div>
			</div><hr />
		<?php } ?>
	
		<?php if($order['Location']['accept_deliveries'] == '1') { ?>
			<div class="ui-helper-clearfix b">
				<div class="left pad-r10">
					<input type="radio" id="OrderOrderMethodDelivery" value="Delivery" name="data[Order][order_method]" checked="checked" class="order-method-class" />
				</div>
				<div class="left"><div class="green">Delivery Order</div></div>
			</div><hr />
		
			<div id="deliveryAddrDiv">
				<div class="orange b marg-b10">Choose Delivery Address:</div><hr />
				<?php foreach($users as $addr): ?>
					<div class="ui-helper-clearfix b">
						<div class="left pad-r10">
							<input type="radio" id="OrderUsersAddrId<?php echo $addr['UsersAddr']['id']?>" 
												value="<?php echo $addr['UsersAddr']['id']?>" 
												name="data[Order][users_addr_id]" 
												<?php if($addr['UsersAddr']['default'] == '1') echo " checked='checked' "; ?> />
						</div>
						<div class="left">
							<div class="green"><?php echo $addr['UsersAddr']['name']; ?></div>
							<div class="orange base"><?php echo $addr['UsersAddr']['address'].', '.$addr['UsersAddr']['city'].', '.$addr['State']['abbr']; ?></div>
						</div>
					</div>
					<hr />
				<?php endforeach; ?>
			</div>
		<?php } ?>
	</div>
	</div>
	
	<div class="orange-border marg-b10">
	<div class="ui-widget-content ui-corner-all">
		<?php if(sizeof($profiles) > 0) { ?>
			<div class="orange b marg-b10">Choose Payment Option:</div>
			<?php foreach($profiles as $profile): ?>
				<hr />
				<div class="ui-helper-clearfix b">
					<div class="left pad-r10">
						<input type="radio" id="OrderUsersPaymentProfileId<?php echo $profile['UsersPaymentProfile']['id']?>" 
											value="<?php echo $profile['UsersPaymentProfile']['id']?>" 
											name="data[Order][users_payment_profile_id]" 
											<?php if($profile['UsersPaymentProfile']['default'] == '1') echo " checked='checked' "; ?> />
					</div>
					<div class="left">
						<div class="green"><?php echo $profile['UsersPaymentProfile']['name']; ?></div>
						<div class="base"><?php echo $profile['UsersPaymentProfile']['card_suffix']; ?></div>
					</div>
				</div>
			<?php endforeach; ?>
			<hr />
			<div class="ui-helper-clearfix" id="AlternatePayMethodToggle">
				<div class="left"><?php echo $html->link('Use Different Payment Method', '#', array('id' => 'AlternatePayMethodLink')); ?></div>
			</div>
			
			<div id="AlternatePayMethod">
				<div class="ui-helper-clearfix b marg-b5">
					<div class="left pad-r10">
						<input type="radio" id="OrderUsersPaymentProfileIdNew" 
											value="new" 
											name="data[Order][users_payment_profile_id]" />
					</div>
					<div class="left green">Use New Payment Method Below</div>
				</div>
				
				<div class="orange b marg-b10">Payment Card Billing Info:</div>
				
				<div class="ui-helper-clearfix">
					<div class="left">
						<div class="base b pad-l5">First Name on card: <span class="required">*</span></div>
						<div class="marg-b5"><?=$form->input('fname', array('class'=>'form-text', 'label'=>false, 'style'=>'width:120px'))?></div>
					</div>
					<div class="left pad-l5">
						<div class="base b pad-l5">Last Name on card: <span class="required">*</span></div>
						<div class="marg-b5"><?=$form->input('lname', array('class'=>'form-text', 'label'=>'', 'style'=>'width:120px'))?></div>
					</div>
				</div>
				
				<div class="base b pad-l5">Billing Address: <span class="required">*</span></div>
				<div class="marg-b5"><?=$form->input('address', array('label'=>'','class'=>'form-text'))?></div>
				
				<div class="base b pad-l5">City: <span class="required">*</span></div>
				<div class="marg-b5"><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></div>
				
				<div class="ui-helper-clearfix marg-b5">
					<div class="left">
						<div class="base b pad-l5">State: <span class="required">*</span></div>
						<div class=""><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-select no-uniform-style', 'style'=>'width:130px'), false)?></div>
					</div>
					<div class="left pad-l5">
						<div class="base b pad-l5">Zip: <span class="required">*</span></div>
						<div class=""><?=$form->input('zip', array('label'=>'','class'=>'form-text', 'style'=>'width:120px;'))?></div>
					</div>
				</div>
				
				<div class="ui-helper-clearfix marg-b5">
					<div class="left">
						<div class="base b pad-l5">Card Number: <span class="required">*</span></div>
						<div class="marg-b5"><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>'', 'style'=>'width:120px;'))?></div>
					</div>
					<div class="left pad-r5 pad-l10 marg-t10"><?=$html->image('creditcard_visa.gif')?></div>
					<div class="left pad-r5 marg-t10"><?=$html->image('creditcard_mc.gif')?></div>
					<div class="left marg-t10"><?=$html->image('creditcard_discover.gif')?></div>
				</div>
				
				<div class="ui-helper-clearfix marg-b10">
					<div class="left">
						<div class="base b pad-l5">Expiration Month: <span class="required">*</span></div>
						<div class=""><?=$form->month('month', null, array('class'=>'form-select no-uniform-style', 'style'=>'width:130px;'), false)?></div>
					</div>
					<div class="left pad-l5">
						<div class="base b pad-l5">Expiration Year: <span class="required">*</span></div>
						<div class=""><?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select no-uniform-style', 'style'=>'width:130px;'), false)?></div>
					</div>
				</div>

				<div class="ui-helper-clearfix">
					<div class="left"><?php echo $form->checkbox('save_profile', array('checked'=>'checked')); ?></div>
					<div class="left pad-l5 b">Save this new card to your profile?</div>
				</div>
				
				<div id="profileNameDiv" class="marg-t10">
					<div class="base b pad-l5">Give This Card a Name: <span class="required">*</span></div>
					<div class="marg-b5"><?=$form->input('name', array('class'=>'form-text', 'label'=>''))?></div>
				</div>
			</div>
		<?php } else { ?>
			<div class="orange b marg-b10">Payment Card Billing Info:</div>
			
			<div class="ui-helper-clearfix">
				<div class="left">
					<div class="base b pad-l5">First Name on card: <span class="required">*</span></div>
					<div class="marg-b5"><?=$form->input('fname', array('class'=>'form-text', 'label'=>false, 'style'=>'width:120px'))?></div>
				</div>
				<div class="left pad-l5">
					<div class="base b pad-l5">Last Name on card: <span class="required">*</span></div>
					<div class="marg-b5"><?=$form->input('lname', array('class'=>'form-text', 'label'=>'', 'style'=>'width:120px'))?></div>
				</div>
			</div>
			
			<div class="base b pad-l5">Billing Address: <span class="required">*</span></div>
			<div class="marg-b5"><?=$form->input('address', array('label'=>'','class'=>'form-text'))?></div>
			
			<div class="base b pad-l5">City: <span class="required">*</span></div>
			<div class="marg-b5"><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></div>
			
			<div class="ui-helper-clearfix marg-b5">
				<div class="left">
					<div class="base b pad-l5">State: <span class="required">*</span></div>
					<div class=""><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-select no-uniform-style', 'style'=>'width:130px'), false)?></div>
				</div>
				<div class="left pad-l5">
					<div class="base b pad-l5">Zip: <span class="required">*</span></div>
					<div class=""><?=$form->input('zip', array('label'=>'','class'=>'form-text', 'style'=>'width:120px;'))?></div>
				</div>
			</div>
			
			<div class="base b pad-l5">Card Number: <span class="required">*</span></div>
			<div class="marg-b5"><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>''))?></div>
			
			<div class="ui-helper-clearfix marg-b10">
				<div class="left">
					<div class="base b pad-l5">Expiration Month: <span class="required">*</span></div>
					<div class=""><?=$form->month('month', null, array('class'=>'form-select no-uniform-style', 'style'=>'width:130px;'), false)?></div>
				</div>
				<div class="left pad-l5">
					<div class="base b pad-l5">Expiration Year: <span class="required">*</span></div>
					<div class=""><?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select no-uniform-style', 'style'=>'width:130px;'), false)?></div>
				</div>
			</div>

			<div class="ui-helper-clearfix">
				<div class="left"><?php echo $form->checkbox('save_profile', array('checked'=>'checked')); ?></div>
				<div class="left pad-l5 b">Save this new card to your profile?</div>
			</div>
			
			<div id="profileNameDiv" class="marg-t10">
				<div class="base b pad-l5">Give This Card a Name: <span class="required">*</span></div>
				<div class="marg-b5"><?=$form->input('name', array('class'=>'form-text', 'label'=>''))?></div>
			</div>
		<?php } ?>
	</div>
	</div>
	</form>
	
	<div class="orange-border marg-b10">
	<div class="ui-widget-content ui-corner-all">
		<div class="orange b marg-b10">Promotion Code:</div>
		<?php echo $form->create('DiscountCode', array('action'=>'check_code')); ?>
		<div class="ui-helper-clearfix">
			<?php echo $form->input('code', array('class'=>'form-text', 'label'=>false, 'div'=>'left pad-r5', 'style'=>'width:160px;')); ?>
			<div class="left"><?php echo $misc->jqueryBtn('#', 'ui-icon-pencil', 'Apply', null, null, 'btnConfirmPromoCode'); ?></div>
			<div class="left ajax-loader-sm-circle"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
		</div>
		<div id="ajaxConfirmPromoCodeContent"></div>
		</form>
	</div>
	</div>
	
	<div class="orange-border marg-b10">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<div class="left"><?php echo $jquery->btn('/menu/'.$location['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to the Menu'); ?></div>
		
		<div class="right">
			<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
			<div class="submitBtn"><?php echo $jquery->btn('#', 'ui-icon-circle-check', 'Complete Order', null, 'btnOrderFinalize'); ?></div>
		</div>
	</div>
	</div>
</div>
