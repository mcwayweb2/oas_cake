<?
	//logged in user, check if a order has been started.
	if($session->check('User.order_id')) {
		//order alreadt started, show shopping cart
		?>
		<div id="side-guestcheck" class="ui-corner-all">
			<h2 class="icon-feature">My Order</h2>
			
			<div class="orange-border">
			<div class="ui-widget-content ui-corner-all">
            <table class="list-table ui-table shopping-cart-list">
            	<tr>
		    		<th style="width:15px;"></th>
		    		<th><span class="pad-l5">Item</span></th>
		            <th style="width:30px;" class="txt-l"><span class="pad-l5">Price</span></th>
		            <th style="width:15px;"></th>
		    	</tr><?
		    	echo $this->element('table_accent_row', array('cols'=>4));
		    	$empty_cart = true;
		    	
		    	if(sizeof($order['OrdersCombo']) > 0) {
		    		$empty_cart = false;
		    		//there are combos in the order to display
		    		foreach($order['OrdersCombo'] as $combo): ?>
		    			<tr>
		    				<td>(<?=$combo['qty']?>)</td>
		    				<td>
		    					<span class="green b"><?=$combo['OrdersCombo']['Combo']['title']?> Combo</span><br />
		    					<span class="base orange"><?=$combo['OrdersCombo']['Combo']['description']?></span>
		    				</td>
		    				<td><?php echo $number->currency($combo['total']); ?></td>
		    				<td><?php echo $misc->jqueryBtn('/orders_combos/delete/'.$combo['id'], 'ui-icon-trash', '', 'Remove this combo from your order?',
		    												null, null, null, null, 'Remove from Order'); ?></td>
		    			</tr><?
		    		endforeach;
		    	}
		    	
		    	if(sizeof($order['OrdersItem']) > 0) {
		    		$empty_cart = false;
		    		foreach($order['OrdersItem'] as $item): ?>
		    			<tr>
		    				<td>(<?=$item['OrdersItem']['qty']?>)</td>
		    				<td>
		    					<span class="green b"><?=$item['Item']['title']?></span><br />
		    					<span class="base orange"><?=$item['Item']['description']?></span>
		    				</td>
		    				<td><?php echo $number->currency($item['OrdersItem']['total']); ?></td>
		    				<td><?php echo $misc->jqueryBtn('/orders_items/delete/'.$item['OrdersItem']['id'], 'ui-icon-trash', '', 'Remove this item from your order?',
		    												null, null, null, null, 'Remove from Order'); ?></td>
		    			</tr><?
		    		endforeach;
		    	}
		    	
				if(sizeof($order['Pizza']) > 0) {
					$empty_cart = false;
		    		foreach($order['Pizza'] as $pizza): ?>
		    			<tr>
		    				<td>(<?=$pizza['qty']?>)</td>
		    				<td>
		    					<span class="green b">
		    						<?php echo $pizza['Size']['size'].'" '.$pizza['Size']['title'].' Pizza'; ?>
		    					</span><br />
		    					<span class="base orange">
		    						<?php 
		    						$lines = array();
		    						if($pizza['Crust']['Crust']['title'] != 'Original') $lines[] = $pizza['Crust']['Crust']['title'].' Crust';  
		    						if($pizza['sauce'] != 'Regular Sauce') $lines[] = $pizza['sauce'];
		    						if($pizza['cheese'] != 'Regular Cheese') $lines[] = $pizza['cheese'];
		    						
		    						if(sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['Topping']['title'];
											if($topping['PizzasTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
											}
											$lines[] = $l;
										endforeach;
		    						}
		    						if(sizeof($lines) > 0) {
		    							echo "<ul class='no-list'>";
										foreach($lines as $line):
											echo "<li>".$line."</li>";
										endforeach;
		    							echo "</ul>";
		    						}
		    						?>
		    					</span>
		    				</td>
		    				<td><?php echo $number->currency($pizza['total']); ?></td>
		    				<td><?php echo $jquery->btn('/pizzas/delete/'.$pizza['id'], 'ui-icon-trash', '', 'Remove this pizza from your order?', 
		    											null, null, null, null, 'Remove from Order'); ?></td>
		    			</tr><?	
		    		endforeach;
		    	}
		    	
		    	if(sizeof($order['OrdersSpecialsSize']) > 0) {
		    		$empty_cart = false;
		    		foreach($order['OrdersSpecialsSize'] as $pizza): ?>
		    			<tr>
		    				<td>(<?=$pizza['qty']?>)</td>
		    				<td>
		    					<span class="green b">
		    						<?php echo $pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Pizza"; ?>
		    					</span><br />
		    					<span class="base orange">
		    						<?php 
		    						$lines = array();
		    						if(!empty($pizza['Special']['subtext'])) $lines[] = $pizza['Special']['subtext'];
		    						
		    						if($pizza['Crust']['Crust']['title'] != 'Original') $lines[] = $pizza['Crust']['Crust']['title'].' Crust';  
		    						if($pizza['sauce'] != 'Regular Sauce') $lines[] = $pizza['sauce'];
		    						if($pizza['cheese'] != 'Regular Cheese') $lines[] = $pizza['cheese'];
		    						if(isset($pizza['Topping']) && sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['Topping']['title'];
											if($topping['OrdersSpecialsSizesTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['OrdersSpecialsSizesTopping']['placement'].' Side)';
											}
											$lines[] = $l;
										endforeach;
		    						}
		    						if(sizeof($lines) > 0) {
		    							echo "<ul class='no-list'>";
										foreach($lines as $line):
											echo "<li>".$line."</li>";
										endforeach;
		    							echo "</ul>";
		    						}
		    						?>
		    					</span>
		    				</td>
		    				<td><?php echo $number->currency($pizza['total']); ?></td>
		    				<td><?php echo $jquery->btn('/orders_specials_sizes/delete/'.$pizza['id'], 'ui-icon-trash', '', 'Remove this special from your order?', 
		    											null, null, null, null, 'Remove from Order'); ?></td>
		    			</tr><?	
		    		endforeach;
		    	}
		    	
		    	if($empty_cart) {
		    		?><tr><td colspan="4">Your order is empty.</td></tr><?
		    	} 
		    	?>
		    </table>
		    <table class="list-table marg-t10">
		    	<?php echo $this->element('table_accent_row', array('cols'=>3)); ?>
		    	<?php $tax = ($order['Location']['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal']; ?>
		    	<tr>
			    	<td><strong>Subtotal</strong></td>
			        <td style="width:30px;"><?php echo $number->currency($order['Order']['subtotal']); ?></td>
			    </tr>
			    <tr>
			        <td><strong>Tax (<?php echo $order['Location']['ZipCode']['tax_rate']; ?>%)</strong></td>
			        <td><?php echo '$'.number_format($tax, 2); ?></td>
			    </tr>
			    <?php if($order['Order']['delivery_charge'] > 0) { ?>
			    <tr>
			        <td><strong>Delivery</strong></td>
			        <td><?php echo $number->currency($order['Order']['delivery_charge']); ?></td>
			    </tr>
			    <?php } ?>
			    <?php echo $this->element('table_accent_row', array('cols'=>3)); ?>
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['delivery_charge']; ?>
			    <tr>
			        <td><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			    <?php 
			    if($order['Order']['subtotal'] >= $order['Location']['min_delivery_amt']) {
			    	$color = 'green';
			    	$content = '';
			    } else { 
			    	$color = 'red';
			    	$amt = $order['Location']['min_delivery_amt'] - $order['Order']['subtotal'];
			    	$content = $number->currency($amt);
			    }
			    ?>
			    <tr>
			        <td><span class="<?=$color?>">Order Minimum (<?php echo $number->currency($order['Location']['min_delivery_amt']); ?>)</span></td>
			        <td class="<?=$color?>"><?php echo $content; ?></td>
			    </tr>
            </table>
            </div>
            </div>
			
		    <div class="orange-border marg-t10">
            <div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix b pad-t5">
            	<div class="left pad-r20 pad-l5" style="width:120px;"><?php echo $misc->jqueryBtn('/locations/order_review', 'ui-icon-search', 'Review Order'); ?></div>
            	<div class="left" style="width:120px;"><?php echo $misc->jqueryBtn('/orders/kill', 'ui-icon-closethick', 'Cancel Order',
            																	   'Are you sure that you would like to cancel your entire order?'); ?></div>
            </div>
            </div>
		</div> 
		<?
		//echo debug($order);
	} else {
		?>
		<div class="marg-t10">
        <div class="ui-widget ui-widget-content ui-corner-all">
        	<h1 class="orange">Ready to Start Ordering?</h1>
        	<div class="marg-t10" style="width:130px;"><?php echo $misc->jqueryBtn('/orders/start/'.$loc_id, 'ui-icon-cart', 'Start Order Now!'); ?></div>    
        </div>
        </div>
		<?php 
	}
	//echo debug($order);
?>