
<div class="paging ui-corner-all ui-state-default ui-helper-clearfix pad5 <?=(isset($class)) ? $class : ""?>">
	<?php echo $paginator->prev('<div class="ui-icon ui-icon-seek-prev"></div>', array('escape' => false, 'class'=>'left', 'title'=>'Previous Page'), 
								'<div class="ui-icon ui-icon-seek-prev ui-state-disabled"></div>', array('escape' => false, 'class'=>'left'));?>
 	<div class="left pad-l5 pad-r5"><?php echo $paginator->numbers(array('modulus' => '26'));?></div>
	<?php echo $paginator->next('<div class="ui-icon ui-icon-seek-next"></div>', array('escape' => false, 'class'=>'left', 'title'=>'Next Page'), 
								'<div class="ui-icon ui-icon-seek-next ui-state-disabled"></div>', array('escape' => false, 'class'=>'left'));?>
</div>