 <table width="100%">
	<tr>
		<td width="60%">First Name on card: *</td>
		<td><?=$form->input('fname', array('class'=>'form-text', 'label'=>''))?></td>
	</tr>
	<tr>
		<td>Last Name on card: *</td>
		<td><?=$form->input('lname', array('class'=>'form-text', 'label'=>''))?></td>
	</tr>
			<tr>
 		<td>Address: *</td>
		<td><?=$form->input('address', array('label'=>'','class'=>'form-text'))?></td>
 	</tr>
 	<tr>
 		<td>City: *</td>
 		<td><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></td>
 	</tr>
 	<tr>
 		<td>State: *</td>
 		<td><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-text'), false)?></td>
 	</tr>
 	<tr>
 		<td>Zip: *</td>
 		<td><?=$form->input('zip', array('label'=>'','class'=>'form-text'))?></td>
 	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?=$html->image('creditcard_amex.gif')?>   
		</td>
	</tr>
	<tr>
		<td>Card Number: *</td>
		<td><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>''))?></td>
	</tr>
	<tr>
		<td>Card Exp: *</td>
		<td><?=$form->month('month', null, array('class'=>'form-select'), false)?> <?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select'), false)?></td>
	</tr>
</table>