<?=$html->script(array('tiny_mce/tiny_mce'), false) ?>
<script type="text/javascript">
	<?php 
	if($toolbar == 'simple' || $toolbar == 'advanced') {
		?>
		tinyMCE.init({
	        theme : "<?=$toolbar?>",
	        mode : "textareas",
	        convert_urls : false
	    });
		<?php 
	} else {
		?>
		tinyMCE.init({
	        theme : "advanced",
	        mode : "textareas",
	        convert_urls : false,
	        plugins : "paste",
	        theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
	        paste_auto_cleanup_on_paste : true,
	        paste_preprocess : function(pl, o) {
	            // Content string containing the HTML from the clipboard
	            alert(o.content);
	        },
	        paste_postprocess : function(pl, o) {
	            // Content DOM node containing the DOM structure of the clipboard
	            alert(o.node.innerHTML);
	        }

	    });
		<?php 
	}
	?>
</script>