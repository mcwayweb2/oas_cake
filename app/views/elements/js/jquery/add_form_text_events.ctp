<script>
$(function() {
	$('.form-text').hover(function() { $(this).addClass("ui-state-hover"); },
			  			  function() { $(this).removeClass("ui-state-hover"); });
	
	$('.form-text').bind({ 
		focusin: function() { $(this).toggleClass('ui-state-focus'); },
		focusout:function() { $(this).toggleClass('ui-state-focus'); }
	});
});
</script>