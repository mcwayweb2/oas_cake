<!-- 
Usage: 
<div>
    <input id="password-clear" type="text" value="Password" autocomplete="off" />
    <input id="password-password" type="password" name="password" value="" autocomplete="off" />
</div>
 -->
<?=$html->script('jquery', false) ?>
<script language="Javascript">
	$(document).ready(function() {
		$('#password-clear').show();
		$('#password-password').hide();
	
		$('#password-clear').focus(function() {
			$('#password-clear').hide();
			$('#password-password').show();
			$('#password-password').focus();
		});
		$('#password-password').blur(function() {
			if($('#password-password').val() == '') {
				$('#password-clear').show();
				$('#password-password').hide();
			}
		});
	});
</script>

<style type="text/css">
	<!--
	#password-clear {
	    display: none;
	}
	-->
</style>