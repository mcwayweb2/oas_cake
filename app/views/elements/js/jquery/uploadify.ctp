<?php echo $html->script(array('jquery/uploadify/swfobject', 'jquery/uploadify/jquery.uploadify.v2.1.0'), false); ?>
<?php echo $html->css(array('../js/jquery/uploadify/uploadify'), false); ?>
<script>
	$(function() { 
		$('#AdvertiserLogo').uploadify({
			'uploader'  : '/js/jquery/uploadify/uploadify.swf',
			'script'    : '/js/jquery/uploadify/uploadify.php',
			'cancelImg' : '/js/jquery/uploadify/cancel.png',
			'auto'      : true,
			'multi'	    : false,
			'folder'    : '/files/logos',
			'onComplete': function(event, queueID, fileObj, response, data) {
                window.location = "<?= $html->url("/advertisers/process_upload/")?>" + fileObj["name"];
            }
			});
	});
</script>