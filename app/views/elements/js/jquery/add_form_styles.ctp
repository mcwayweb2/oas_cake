<script>
$(function() {
	$("form").form();
	$("select, input:checkbox, input:radio, input:file").uniform();

	$('.ui-icon-link > div').hover(function() { $(this).toggleClass('ui-state-active'); }, 
	   		   					   function() { $(this).toggleClass('ui-state-active'); });

	$(".ui-table tr").hover( function() {
		$(this).children("td").toggleClass("ui-row-hover");
	});
	$(".ui-table tr").click( function() {
		$(this).children("td").toggleClass("ui-row-highlight");
	});
	$(".ui-table tr:even td").addClass('alt-row');
});	
</script>
<?php echo $this->element('js/jquery/add_form_text_events'); ?>