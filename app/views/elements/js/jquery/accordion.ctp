<script type="text/javascript">
	$(function() {
		var favOpts = {
			<?php 
			if(isset($opts)) { 
				$x = 0;
				foreach($opts as $opt => $value):
					echo $opt.": ".$value;
					if(++$x < sizeof($opts)) { echo ",\n"; }
				endforeach; 
			}
			?>
		};
		$('#<?=$name?>').accordion(favOpts);
	});
</script>