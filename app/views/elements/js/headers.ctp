<title><?php echo isset($title_for_layout) ? $title_for_layout : "Pizza Delivery - Order Pizza Online | Order a Slice"; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?=$html->css(array('layouts', 'jquery/themes/orderaslice/jquery-ui-1.8.4', 'jquery/uniform/uniform'))?>
<?=$html->script(array('jquery/jquery-1.4.2', 'jquery/jquery-ui-1.8.4', 'jquery/form-styles', 'jquery/uniform'));?>
<script>
$(function() {
	$("form").form();
	$("select, input:checkbox, input:radio").filter(":not(#SearchFoodCatId, #SearchDeliveryMethod, .no-uniform-style)").uniform();

	$('.ui-icon-link > div').hover(function() { $(this).toggleClass('ui-state-active'); }, 
		 	   					   function() { $(this).toggleClass('ui-state-active'); });

	$(".ui-table tr").hover( function() {
		$(this).children("td").toggleClass("ui-row-hover");
	});
	$(".ui-table tr").click( function() {
		$(this).children("td").toggleClass("ui-row-highlight");
	});
	$(".ui-table tr:even td").addClass('alt-row');

	$('.loginLink').click(function() {
		$('#loginDialog').dialog('open');
	});
	$('#adLoginLink').click(function() {
		$('#adLoginDialog').dialog('open');
	});

	$('.field-no-empty').focus(function() { $(this).val(''); });
	$('.field-no-empty').blur(function() { 
		if($(this).val() == '') {
			$(this).val($(this).attr('alt'));
		}
	});

	$('.btnSubmit').click(function() { 
		$(this).closest('form').submit();
		return false;
	});
	
	var dialogOpts = {
		autoOpen: false,
		modal: true,
		position: "top",
		buttons: {
			'Login': function() {
				 $('#AdvertiserLoginForm').submit();
			},
			'Register': function() { $(window.location).attr('href', 'http://orderaslice.com/advertisers/register'); },
			'Forgot Password': function() { $(window.location).attr('href', 'http://orderaslice.com/advertisers/password_reset'); }
		}
	};
	$('#adLoginDialog').dialog(dialogOpts);

	var dialogOpts = {
		autoOpen: false,
		modal: true,
		position: "center",
		buttons: {
			'Login': function() {
				 $('#UserLoginForm').submit();
			},
			'Register': function() { $(window.location).attr('href', 'http://orderaslice.com/users/register'); },
			'Forgot Password': function() { $(window.location).attr('href', 'http://orderaslice.com/users/password_reset'); }
		}
	};
	$('#loginDialog').dialog(dialogOpts);
});
</script>