<?php if(isset($highlight_fields)) { ?>
	<script>
	$(function() {
		<?php foreach($highlight_fields as $field): ?>
			$('#<?php echo $field; ?>').addClass('highlight-field');
		<?php endforeach; ?>
	});
	</script>
<?php } ?>