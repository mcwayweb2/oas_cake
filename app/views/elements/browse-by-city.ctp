<script>
$(function() {
	
	function closeBrowseCities() {
		$('#btn-toggle-browse-cities div.ui-icon').removeClass('ui-icon-circle-minus').addClass('ui-icon-circle-plus');
		$('#browse-cities-bottom').slideUp();
		$('#browse-cities-top').removeClass('ui-corner-top').addClass('ui-corner-all');
	}

	function openBrowseCities() {
		$('#btn-toggle-browse-cities div.ui-icon').removeClass('ui-icon-circle-plus').addClass('ui-icon-circle-minus');
		$('#browse-cities-top').removeClass('ui-corner-all').addClass('ui-corner-top');
		$('#browse-cities-bottom').slideDown();
	}

	<?php if(!$search_by_zip) { ?>
		openBrowseCities();
	<?php } ?>
	
	// assumes browse-cities starts open, first click will close.
	$('.toggle-browse-cities').toggle(function() { openBrowseCities(); }, function() { closeBrowseCities(); });

	$('span.city-list-sub-img').toggle(function() {
		//open the sub list
		$(this).html("<img src='/img/icons/16-red-cancel.png' />").parent().next('ul.city-list-sub').slideDown();	
	}, function() {
		//close the sub list
		$(this).html("<img src='/img/icons/16-green-circle-plus.png' />").parent().next('ul.city-list-sub').slideUp();	
	});
	
});
</script>

<style>
.state-div { padding-right: 50px; width: 150px; }
.state-lbl { font-weight: bold; margin-bottom: 3px; color: #4f7a35; font-size: 13px; }

ul.city-list li { padding-left: 15px; list-style-type: none; }
ul.city-list li a { text-decoration: none; }
ul.city-list li a:hover { text-decoration: underline; font-weight: bold; }

ul.city-list-sub { display:none; }  
ul.city-list-sub li { padding-left: 30px; list-style-type: none; }

.city-list-sub-img:hover { cursor: pointer; } 

</style>
<?php $prefix = '/restaurants/browse-menus'; ?>

<div class="marg-b20">
	<div class="ui-widget-header ui-corner-all ui-helper-clearfix" id="browse-cities-top">
		<div class="left pad-l10 pad-t5 lg b">Browse By City</div>
		<div class="right pad-r5 pad-t5"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', '', null, 'toggle-browse-cities', 'btn-toggle-browse-cities')?></div>
	</div>
	<div class="ui-widget-content ui-corner-bottom" id="browse-cities-bottom" style="display:none;">
		<div class="ui-helper-clearfix ">
			<div class="left state-div">
				<div class="state-lbl">California</div>
				<ul class="city-list">
					<li><?php echo $html->link('San Diego', $prefix.'/san-diego'); ?></li>
					<li>
						<span class="orange b valign-t">Coachella Valley</span>
						<span class="city-list-sub-img"><?php echo $html->image('icons/16-green-circle-plus.png'); ?></span>
					</li>
					<ul class="city-list-sub">
						<li><?php echo $html->link('Coachella', $prefix.'/coachella'); ?></li>
						<li><?php echo $html->link('Desert Hot Springs', $prefix.'/desert-hot-springs'); ?></li>
						<li><?php echo $html->link('Indio', $prefix.'/indio'); ?></li>
						<li><?php echo $html->link('Palm Desert', $prefix.'/palm-desert'); ?></li>
						<li><?php echo $html->link('Palm Springs', $prefix.'/palm-springs'); ?></li>
					</ul>
				</ul>
			</div>
	
			<div class="left state-div">
				<div class="state-lbl">Hawaii</div>
				<ul class="city-list">
					<li><?php echo $html->link('Kailua Kona', $prefix.'/kailua-kona'); ?></li>
	
				</ul>
			</div>
		</div>
	</div>
</div>