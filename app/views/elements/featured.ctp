<div id="side-features">
	<h2 class="icon-feature">Featured Restaurants</h2>
	<div class="orange-border">
	<div class="ui-widget-content ui-corner-all">
	<?php
	foreach($featured_restaurants as $restaurant):
		?>
		<div class="ui-helper-clearfix marg-b10 pad-b5" style="border-bottom:1px solid #4f7a35;">
			<div class="left ui-corner-all pad-r5" style="width:80px">									   
				<?php 
				if($restaurant['Location']['use_logo_in_banner'] == '2' && !empty($restaurant['Advertiser']['logo'])) {
					echo $html->link($html->image('../files/logos/'.$restaurant['Advertiser']['logo'], array('style'=>'width:75px;', 'class'=>'ui-corner-all bordered-logo')),
									 '/menu/'.$restaurant['Location']['slug'],
									 array('escape'=>false));
				} else if(!empty($restaurant['Location']['logo'])) {
					echo $html->link($html->image('../files/logos/'.$restaurant['Location']['logo'], array('style'=>'width:75px;', 'class'=>'ui-corner-all bordered-logo')),
									 '/menu/'.$restaurant['Location']['slug'],
									 array('escape'=>false));
				} else {
					echo '&nbsp;';
				}
				?>
			</div>
			<div class="left" style="width:180px;">
				<?php echo $html->link($restaurant['Location']['name'], '/menu/'.$restaurant['Location']['slug'],  
									array('class'=>'header-link'))?><br /> 
				<div class="base b green">
					<?php //echo '('.$html->link($restaurant['ZipCode']['city'].', '.$restaurant['State']['abbr'], '/city/'.$restaurant['ZipCode']['slug']).')'; ?>
					(<?php echo $restaurant['ZipCode']['city'].', '.$restaurant['State']['abbr']; ?>)
				</div>
				<div class="base b">Cuisine: <span class="red"><?php echo $restaurant['FoodCat']['title']?></span></div>
			</div>
		</div>
		<?php
	endforeach;
	?>
	</div>
	</div>
</div>