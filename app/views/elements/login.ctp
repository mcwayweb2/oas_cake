<?php 
	$js_block = "
	$(function() {
		$('#loginTabs').tabs().hide();

		$('#loginLink').click(function() {
			$('#loginTabs').slideDown();	
		});
	});";
	echo $html->scriptBlock($js_block, array('inline'=> false)); 
	
?>
<div id="loginTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#userLogin">Customer Login</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#restaurantLogin">Restaurant Login</a></li>
	</ul>
	<div id="userLogin" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php echo $form->create('User', array("action"=>"login"));?>
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('username',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('password',array("class"=>"form-text"))?></td>
			</tr>
			<tr>
				<td><div style="width:70px;" class="right"><?php echo $misc->jqueryBtn('#', 'ui-icon-locked', 'Login'); ?></div></td>
				<td>
					<div class="ui-helper-clearfix basefont required">
						<div class="left pad-r5"><?php echo $misc->jqueryBtn('/users/register', 'ui-icon-pencil', 'Register'); ?></div>
						<div class="left"><?php echo $misc->jqueryBtn('/users/password_reset', 'ui-icon-unlocked', 'Forgot Password'); ?></div>
					</div>
				</td>
			</tr>
		</table>
		</form>	
	</div>
	<div id="restaurantLogin" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
	</div>
</div>