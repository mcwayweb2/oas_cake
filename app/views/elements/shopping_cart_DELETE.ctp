<div id="sidebar" class="left">
<div id="side-guestcheck">
	<form action="" method="post">
	<h2 class="icon-feature">Guest Check</h2>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="gc-cart">
    <thead>
    	<tr>
    		<th>QTY</th>
    		<th>Item</th>
            <th>Price</th>
    	</tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Pizza</td>
        <td>33.50</td>
      </tr>
	  <tr>
        <td>2</td>
        <td>Pizza</td>
        <td>33.50</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Pizza</td>
        <td>33.50</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Pizza</td>
        <td>33.50</td>
      </tr>	
    </tbody>  
    </table>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="gc-total">
      <tr>
        <td><strong>Subtotal</strong></td>
        <td>$0.00</td>
      </tr>
      <tr>
        <td><strong>Tax</strong></td>
        <td>$0.00</td>
      </tr>
      <tr>
        <td style="border-bottom:1px solid #d4d4d4;"><strong>Delivery</strong></td>
        <td style="border-bottom:1px solid #d4d4d4;">$0.00</td>
      </tr>
      <tr>
        <td><strong class="xlarge red">TOTAL</strong></td>
        <td>$0.00</td>
      </tr>
    </table>
    
    <h3>Have a Coupon?</h3>
	<p><label>Enter Coupon Code: <input type="text" name="coupon_code" id="coupon_code" class="form-style2"/></label></p>
    
    <input type="image" src="images/btn-finalize-order.jpg" class="img-center" name="submit" />
    </form>
</div>    

</div>