<div class="pad1 green-border">
	<div class="ui-helper-clearfix ui-widget-content ui-corner-all" style="padding: 5px 0px 5px 5px; border: 1px solid #ddd;">
		<?php $ct = 0; ?>
		<?php foreach($btns as $b): ?>
			<div class="left pad-r5">
				<?php echo $html->link($html->image('icons/'.$b[1]), 
									   $b[0], 
									   array('escape' => false, 'title' => $b[2]),
									   ((isset($b[3])) ? $b[3] : null)); ?>
			</div>
		<?php endforeach; ?>	
	</div>
</div>