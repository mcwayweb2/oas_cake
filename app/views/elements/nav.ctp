<ul>
	<?php if($session->check('Admin')) { ?>
		<li class="first"><?=$html->link('Admin','/admin/advertisers/dashboard')?></li>
	<?php } ?>
	
	<?php if($session->check('Advertiser')) { ?>
		<li class="first"><?=$html->link('Dashboard', '/advertisers/dashboard')?></li>
		<li><?php echo $html->link('Billing Profiles', '/advertisers/payment_profiles'); ?></li>
		<!--<li><?php //echo $html->link('Advertisements', '/advertisers/manage_ads')?></li>-->
		<li><?=$html->link('Promotion Codes', '/discount_codes/manage')?></li>
		<!--<li><?php //echo $html->link('Premium Accounts', '/restaurants/upgrade'); ?></li>-->
		<li><?=$html->link('Account Settings', '/advertisers/edit')?></li>
		<li><?=$html->link('Tutorials', '/tutorials/index')?></li>
		<li><?=$html->link('Logout', '/advertisers/logout')?></li>
	<?php } else { 
		if($session->check('User.order_id')) { ?>
			<li class="first"><?=$html->link('Review Order', '/locations/order_review')?></li>
			<li><?=$html->link('Cancel Order', '/orders/kill/'.$session->read('User.order_id'), array(), 'Are you sure you would like to cancel this entire order?')?></li>
			<li><?=$html->link('Logout', '/users/logout')?></li>
		<?php } else { ?>
			<li class="first"><?=$html->link('Home', '/') ?></li>
		    <li><?=$html->link('Browse Menus', '/restaurants/browse-menus')?></li>
		    <?php if($session->check('User')) { ?>
		    	<li><?=$html->link('My Account', '/users/dashboard')?></li>
		    	<li><?=$html->link('Logout', '/users/logout')?></li>
		    <?php } else { ?>
		    	<li><?=$html->link('Register', '/users/register')?></li>
		    	<li><?=$html->link('Login', '#', array('class'=>'loginLink'))?></li>
		    <?php } ?>
		    <li><?=$html->link('Contact Us', '/contact-us')?></li>
		<?php }
	} ?>
	
</ul>