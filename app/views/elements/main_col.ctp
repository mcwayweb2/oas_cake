<div id="maincol" class="right">
    <?php 
    if($session->check('Message.flash')) {
    	echo ($session->check('show_warning')) ? $this->element('warning-msg-box') : $this->element('error-msg-box'); 
    } 
 	?>
	<div id="loginDialog" title="Customer Login" style="display:none;">
		<?php echo $form->create('User', array("action"=>"login"));?>
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('loginUsername',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('loginPassword',array("class"=>"form-text"))?></td>
			</tr>
		</table>
		</form>	
	</div>
	
	<div id="adLoginDialog" title="Advertiser Login" style="display:none;">
		<?php echo $form->create('Advertiser', array("action"=>"login"));?>
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('loginUsername',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('loginPassword',array("class"=>"form-text"))?></td>
			</tr>
		</table>
		</form>	
	</div>
	
	<div id="mainColContent"><?php echo $content; ?></div>
</div> 