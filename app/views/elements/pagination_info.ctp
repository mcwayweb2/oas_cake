<div class="ui-corner-all ui-state-default pad5 marg-b10 <?=(isset($class)) ? $class : ""?>">
	<?php
		if(isset($msg)) { echo $msg.' '; } 
		echo $paginator->counter(array('format' => __('Page %page% of %pages%, showing %current% records out of %count% total.', true))); 
	?>
</div>
