<script>
$(function() { 
	$('#CombosSpecialsSizeSizeIdTemp, #CombosSpecialsSizeQty').uniform();
});
</script>
<?php echo $jquery->scrFormSubmit('#btnComboBuilderAddSpecialtyPizza', '#formComboBuilderAddSpecialtyPizza'); ?>
<hr class="orange" />
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Choose Pizza Size:</td>
		<td><?=$form->select('CombosSpecialsSize.size_id_temp', $sizes, null, array('class'=>'form-text', 'id'=>'CombosSpecialsSizeSizeIdTemp'), false)?></td>
	</tr>
	<tr>
		<td class="label-req">Quantity:</td>
		<td><?=$form->select('CombosSpecialsSize.qty', $misc->qtys(), null, array('class'=>'form-text', 'id'=>'CombosSpecialsSizeQty'), false)?></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Specialty Pizza', null, null, 'btnComboBuilderAddSpecialtyPizza', '145')?></td>
	</tr>
</table>