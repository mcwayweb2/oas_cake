<script>
$(function() { 
	$('#ajaxGettingSpecialtyPizzaSizes').hide();
	$('#CombosSpecialsSizeSizeIdTemp, #CombosSpecialsSizeQty, #CombosSpecialsSizeSpecialId').uniform();

	$('#CombosSpecialsSizeSpecialId').change(function() { 
		//repopulate the sizes select dropdown with the available sizes for the chosen specialty pizza
		$('#ajaxGettingSpecialtyPizzaSizes').show();
		$('#CombosSpecialsSizeSizeIdTemp').load('/combos_specials_sizes/ajax_single_sizes_add', { specialId: $(this).val() }, function() { 
			$('#CombosSpecialsSizeSizeIdTemp').removeAttr('disabled');
			$('#uniform-CombosSpecialsSizeSizeIdTemp').removeClass('disabled');
			$('#ajaxGettingSpecialtyPizzaSizes').hide();
		});
	});
});
</script>
<?php echo $jquery->scrFormSubmit('#btnComboBuilderAddSpecialtyPizza', '#formComboBuilderAddSpecialtyPizza'); ?>
<hr class="orange" />
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Choose Specialty Pizza:</td>
		<td><div class="ui-helper-clearfix">
			<div class="left"><?=$form->select('special_id', $specials, null, array('class'=>'form-text', 'id'=>'CombosSpecialsSizeSpecialId'), 'Select a Specialty Pizza')?></div>
			<div class="left" id="ajaxGettingSpecialtyPizzaSizes"><?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?></div>
		</div></td>
	</tr>
	
	<tr>
		<td class="label-req" style="width:200px;">Choose Pizza Size:</td>
		<td><?=$form->select('CombosSpecialsSize.specials_size_id', $sizes, null, array('class'=>'form-text', 'id'=>'CombosSpecialsSizeSizeIdTemp', 'disabled'=>'disabled'), false)?></td>
	</tr>
	<tr>
		<td class="label-req">Quantity:</td>
		<td><?=$form->select('CombosSpecialsSize.qty', $misc->qtys(), null, array('class'=>'form-text', 'id'=>'CombosSpecialsSizeQty'), false)?></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Specialty Pizza', null, null, 'btnComboBuilderAddSpecialtyPizza', '145')?></td>
	</tr>
</table>