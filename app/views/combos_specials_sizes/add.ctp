<script>
$(function() { 
	$('#progressComboBuilderAddSpecialtyPizza').hide();

	$('#ajaxRadioType').change(function() {
		$('#progressComboBuilderAddSpecialtyPizza').show();
		$('#ajaxToggleContent').empty(); 
		if($(this).val() == 'any') {
			$('#ajaxToggleContent').load('/combos_specials_sizes/ajax_any_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddSpecialtyPizza').hide();
			});	
		} else {
			$('#ajaxToggleContent').load('/combos_specials_sizes/ajax_single_add/<?=$combo_id?>', null, function() { 
				$('#progressComboBuilderAddSpecialtyPizza').hide();
			});
		}
	});

	$('#btnAddChooseSpecificToppings').toggle(function() { 
		//show edit pic form, change link text
		$('#ajaxAddSpecificToppings').show();
		$('#btnAddChooseSpecificToppings div.ui-icon-text').text('Hide Toppings Form');
		return false;	
	}, function() { 
		//remove pic form, revert link text
		$('#ajaxAddSpecificToppings').hide();
		$('#btnAddChooseSpecificToppings div.ui-icon-text').text('Choose Specific Toppings');
		return false;
	});
});
</script>

<div class="ui-widget-content ui-corner-all marg-b10">
<fieldset class="ui-widget-content ui-corner-all  marg-b10"><legend class="ui-corner-all">Add a Specialty Pizza to your Combo...</legend>
	<p>Use this form to add a Specialty Pizza to your current Combo. You can add a specific Specialty Pizza from your menu, or allow the 
	customer to choose their own Specialty Pizza upon placing an order. To get started, select your desired method for adding a specialty pizza to 
	your combo below.</p>
	<hr />
	<?php $opts = array('any'    => 'Let customer choose any Specialty Pizza when ordering',
						'single' => 'Select a specific Specialty Pizza to add'); ?>
						
	<div class="ui-helper-clearfix">
		<div class="left pad-r10"><?php echo $form->select('type', $opts, null, array('id'=>'ajaxRadioType', 'class'=>'ui-widget-content ui-corner-all ui-state-default'), 'How will you add this Specialty Pizza?');?></div>
		<div class="left" id="progressComboBuilderAddSpecialtyPizza"><?php echo $html->image('ajax-loader-med-bar.gif', array('style'=>'width:100px;')); ?></div>
	</div>
	
	<?php echo $form->create('CombosSpecialsSize', array('action'=>'add', 'id'=>'formComboBuilderAddSpecialtyPizza')); ?>
	<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
	<div id="ajaxToggleContent"></div>
	</form>
</fieldset>
</div>
<script>
$(function() { $('#uniform-ajaxRadioType').width('400'); });
</script>