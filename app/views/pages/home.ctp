<?php echo $html->meta('description', "Browse local restaurant menus; order food online from dozens of your favorite local pizza restaurants. ", array(), false);?>

<script type="text/javascript">
	$(function() {
		$('#pageTabs').tabs();

		$('#restaurants').load("/advertisers/home_ajax_div_advertisers").addClass('ui-tabs-panel ui-widget-content');
	});
</script>

<div id="pageTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#customers">Hey Customers...</a></li>
		<li class="ui-state-default ui-corner-top"><a href="#restaurants">Hey Restaurants...</a></li>
	</ul>
	<div id="customers" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<h1 class="clear">Looking for Local Online Pizza Delivery in Your Area?</h1>
		<h2>Order pizza online from dozens of your favorite local restaurants...</h2>
		<p>Are you in the mood for <b>pizza delivery</b>, but you're sick of the <b>pizza delivery restaurant</b> chains that all taste like cardboard? No need to despair, with 
		Order a Slice we bring the delicious family-owned restaurant food that you love right to your door with the push of a button! With 
		Order a Slice you get the convenience that <b>online pizza ordering</b> brings without sacrificing the food that you love. We are quick, 
		convenient, secure, and we deliver the neighborhood restaurants that you love with the click of a mouse. You're 60 seconds away from 
		Ordering a Slice!</p>
			
		<ul class="list-green-check ui-helper-reset ui-state-default ui-corner-all" >
			<li class="ui-helper-clearfix">
		      	<div class="ui-icon ui-icon-check left"></div>
		       	<div class="left">Never Have to Call...Never Again will you Be Placed On Hold!</div>
		    </li>
		    <li class="ui-helper-clearfix">
		      	<div class="ui-icon ui-icon-check left"></div>
		       	<div class="left">Convenient Online Order Tracking, to Allow you to Track Your Order Progress.</div>
		    </li>
		    <li class="ui-helper-clearfix">
		       	<div class="ui-icon ui-icon-check left"></div>
		    	<div class="left">Find Discounts on Your Favorite Restaurants, only Available on Order a Slice.</div>
		    </li>
		    <li class="ui-helper-clearfix">
		    	<div class="ui-icon ui-icon-check left"></div>
		        <div class="left">Find New Local Restaurants that you Never Knew Existed!</div>
		    </li>
		    <li class="ui-helper-clearfix">
		        <div class="ui-icon ui-icon-check left"></div>
		        <div class="left">Manage your Online Orders from All your Favorite Restaurants in one Central Location.</div>
		    </li>
		</ul>
		<br />	
		<p><?=$html->link($html->image("btn-signup.gif", array('class'=>'valign=m', 'id'=>'idTestButton')),
						  array('action'=>'register', 'controller'=>'users'),
						  array('escape'=>false))?></p>
	
	</div>
	<div id="restaurants">
	</div>
</div>
<div id="latest-news">
	<div id="benefits-box" class="clearfix">
		<h2>Pizza Delivery News</h2>
		<ul class="left">
	    	<li>Stay up to date with the latest Order a Slice news...</li>
	    	<li><?php echo $html->link('Sign up', '/users/register', array('class'=>'green-link')); ?> for the Order a Slice newsletter.</li>
	    </ul>
	</div>
	<p>Look for us soon in select cities in California and Hawaii, and look for the 
	familiar, delicious restaurants you love in a city near you very soon.</p>
</div>