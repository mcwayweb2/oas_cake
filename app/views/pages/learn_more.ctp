<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left"><h1>How Do I Order a Slice?</h1></div>
		
	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<div class="ui-helper-clearfix">
		<div class="left b pad-l10"><?php echo $html->link('What are the Account Benefits?', '#benefits'); ?></div>
		<div class="right">
			<?=$html->link($html->image("btn-signup.gif", array('class'=>'valign=m')),
							  				  array('action'=>'register', 'controller'=>'advertisers'),
							  				  array('escape'=>false))?></div>
	</div>

	<ul class="list-tomato"><li class="b lg green marg-b10">Create Your Order a Slice Account...</li></ul>
	
	<p>Start by <?php echo $html->link('Creating an Order a Slice Account', '/registration');?>. There is absolutely 
	<span class="red b">No Cost</span> to your company to sign up.  
	There is <span class="red b">No Contract</span>, <span class="red b">No Commitment</span>, and you may cancel 
	your account at any time. <span class="red b">Use Your Account Risk Free</span>: you pay only a small fee for each order we send your way. 
	This means if we don't send you extra business, then You <span class="red b">Don't Pay a Dime!</span> It's that simple! 
	Signup for free, and <?php echo $html->link('Create Your Order a Slice Account Now!', '/registration');?></p>
	
	<ul class="list-tomato"><li class="b lg green marg-b10">Set Up Your Online Menu...</li></ul>
	
	<p>After your initial account is created, you will be allowed to start adding your restaurant(s). Just send us your restaurant menu 
	via email or the fax number we provide, and we can create your entire online menu for you. Processing time takes just a few days, depending on when we receive your menu. Can't Wait, and want to 
	<span class="red b">Start Accepting Orders Sooner?</span> Use our state of the art, Online Menu Creator to create your online menu yourself, and 
	<span class="red b">Start Using your Account Today!</span> (It will take approximately 30-60 minutes per menu to set it up yourself.)</p>
	
	<ul class="list-tomato"><li class="b lg green marg-b10">Watch Your Profits Soar...</li></ul>
	
	<p>Online Ordering is the wave of the future for the Restaurant Industry. Consumers love the ease of online ordering: it's fast, convenient, 
	and they are never put on hold. Business's love online ordering because it expands their potential customer base exponentially; you reach new 
	customers that your normal advertising campaign would not reach. <span class="b">Compete with larger businesses, gain new customers, and expand your local and online markets, 
	<span class="red">Without Lifting a Finger!</span> Here's How the Account Works:</span></p>
	
	<div style="" class="marg-b20">
		<?php echo $html->image('flow-chart-proof.png', array('style'=>'width:580px;', 'class'=>'')); ?>
	</div>
	
	<div id="benefits" class="marg-t10">
		<div class="b lg green marg-b10">Start Enjoying the Excellent Benefits of your Premiere Account Immediately...</div>
		
		<div class="ui-helper-clearfix marg-b10">
			<div class="left pad-r10" style="width:130px;">
				<?php echo $html->image('../files/ads/ad-320x250.png', array('class'=>'ui-corner-all bordered-logo', 'style'=>'width:120px;'));?>
			</div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Free Advertisement and Online Exposure</div>
				</div>
				<p>Let our company perform the difficult task of marketing and advertising your Restaurant's online menu presence. Our excellent Marketing 
				Department is always developing new local advertising campaigns and online marketing strategies to promote the companies we partner with, Free Of Charge.</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix marg-b10">
			<div class="left pad-r10" style="width:130px;"><?php echo $html->image('red-pushpin-on-map.jpg', array('class'=>'ui-corner-all bordered-logo', 
																							  					   'style'=>'width:120px;'));?></div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Generate Turn by Turn Driving Directions</div>
				</div>
				<p>Generate Instant Turn by Turn Driving Directions for your delivery drivers, complete with printable maps. With the click of a button you 
				will maximize employee productivity and save company time.</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix marg-b10">
			<div class="left pad-r10" style="width:130px;">
				<?php echo $html->image('human-hand-holding-laptop.jpg', array('class'=>'ui-corner-all bordered-logo', 'style'=>'width:120px;')); ?>
				&nbsp;
			</div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Instant Notification for Online Orders</div>
				</div>
				<p>Be notified immediately of your incoming online orders in a variety of ways to suit your needs. You can be notified of each incoming order 
				via your Order a Slice account, email, cell phone, fax, or any combination of the four.</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix marg-b10">
			<div class="left pad-r10" style="width:130px;">
				<?php echo $html->image('coupon-clippings.jpg', array('class'=>'ui-corner-all bordered-logo', 'style'=>'width:120px;'));?>
			</div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Use Promotion and Discount Codes</div>
				</div>
				<p>Use our Promotion Code Manager, to make setting up and managing promotion/discount codes for your site a breeze. Use promotion codes 
				to attract new customers, or reward repeat customers.</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix marg-b10">
			<div class="left pad-r10" style="width:130px;"><?php echo $html->image('credit-card-terminal.jpg', array('class'=>'ui-corner-all bordered-logo', 
																							  'style'=>'width:120px;'));?></div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Accept Payment by Credit Card for FREE</div>
				</div>
				<p>Accept Credit Card orders without having to rent expensive credit card processing hardware, or hassle with costly contracts with credit card 
				processing companies.</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10" style="width:130px;">
				<?php echo $html->image('access-info-anywhere.jpg', array('class'=>'ui-corner-all bordered-logo', 'style'=>'width:120px;')); ?>
				&nbsp;
			</div>
			<div class="left" style="width:400px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 pad-l10"><?php echo $html->image('icons/16-green-check.png');?></div>
					<div class="left b">Access Customer Contact Information Anywhere in the World</div>
				</div>
				<p>Streamline your restaurants order and customer contact information to one central location you can access anywhere in the world, 
				quickly and conveniently!</p>
			</div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="right"><?=$html->link($html->image("btn-signup.gif", array('class'=>'valign=m')),
							  				  array('action'=>'register', 'controller'=>'advertisers'),
							  				  array('escape'=>false))?></div>
		</div>
	</div>
</div>