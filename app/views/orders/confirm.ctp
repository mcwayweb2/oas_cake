<?php if(isset($order)) { ?>
	<div class="ui-widget-header ui-corner-top pad5"><h1>Order Confirmed</h1></div>
	<div class="ui-widget-content ui-corner-bottom">
		<div class="ui-helper-clearfix marg-b5">
			<div class="left" style="width:300px;">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r5 b">Order For:</div>
					<div class="left"><?php echo $order['Order']['order_method']; ?></div>
				</div>
			
			
				<!-- display the users info for the restaurant -->
				<div class="b" style="margin-bottom: 2px;">Customer Info:</div>
				<div class="b green"><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></div>
				 
				<?php if($order['Order']['order_method'] == 'Delivery') { ?>
					<div>"<?php echo $order['UsersAddr']['name']; ?>"</div>
					<div><?php echo $order['UsersAddr']['address']?></div>
					<div><?php echo $order['UsersAddr']['city'].', '.$order['UsersAddr']['zip']; ?></div>
				<?php } ?>
				
				<div class="marg-b5"><?php echo $misc->formatPhone($order['User']['phone']); ?></div>
				
				<?php if(!empty($order['UsersAddr']['special_instructions'])) { ?>
					<div class="ui-helper-clearfix marg-b5">
						<div class="left pad-r5 b">Special Instructions:</div>
						<div class="left"><?php echo $order['UsersAddr']['special_instructions']; ?></div>
					</div>
				<?php }
				
				if($order['Order']['order_method'] == 'Delivery') { 
					if($session->check('Advertiser')) {
						//advertiser is logged in already
						echo $html->link('Driving Directions', '/orders/delivery_directions/'.$order['Order']['id']);
					} else {
						echo $html->link('Login For Driving Directions', '/advertisers/login');
					}

				} ?>
			</div>
		
			<div class="left">
				<div class="ui-helper-clearfix marg-b5">
					<div class="left b" style="width:110px;">Order Placed:</div>
					<div class="left"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
				</div>
				
				<div class="ui-helper-clearfix marg-b5">
					<div class="left b red" style="width:110px;">Order Confirmed:</div>
					<div class="left"><?php echo $time->nice($order['Order']['order_started_timestamp']); ?></div>
				</div>
			</div>
		</div>
	
		<?php echo $this->element('order-details', array('order' => $order)); ?>
	</div>
<?php } ?>