<?php echo $jquery->scrFormSubmit('#btnOrderAdminIndex', '#formOrderAdminIndex'); ?>
<script>
$(function() {
	$('#OrderStartDate').datepicker({ 
		dateFormat: "yy-mm-dd"
	});

	$('#OrderEndDate').datepicker({ 
		maxDate: new Date(),
		dateFormat: "yy-mm-dd"
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="pad-l5"><h1>Order Manager</h1></div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Order', array('action' => 'admin_index', 'id' => 'formOrderAdminIndex')); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b5"><legend class="ui-corner-all">Search Orders</legend>
		<p>Enter a date range to search orders with:</p>
		<div class="ui-helper-clearfix">
			<div class="left pad-r20">
				<div class="b base">Start Date:</div>
				<div><?php echo $form->input('start_date', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left pad-r20">
				<div class="b base">End Date:</div>
				<div><?php echo $form->input('end_date', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left" style="padding-top: 8px;"><?php echo $jquery->btn('#', 'ui-icon-search', 'Search Orders', null, null, 'btnOrderAdminIndex'); ?></div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Filter by Order Status:</div>
				<?php $opts = array('Confirmed' => 'Confirmed', 'Unconfirmed' => 'Unconfirmed', 'Refunded' => 'Refunded')?>
				<div><?php echo $form->select('status', $opts, null, array(), ''); ?></div>
			</div>
			<div class="left pad-r20">
				<div class="b base">Filter by Restaurant:</div>
				<?php $opts = array('Confirmed' => 'Confirmed', 'Unconfirmed' => 'Unconfirmed', 'Refunded' => 'Refunded')?>
				<div><?php echo $form->select('location_id', $restaurant_list, null, array(), ''); ?></div>
			</div>
		</div>
	</fieldset>
	</form>
	<hr />
	
	<?php if(isset($hdr_txt)) { ?><div class="b marg-b5 pad-l5"><?php echo $hdr_txt; ?></div><?php } ?>
	
	<?php if(sizeof($orders) > 0) { $i = 0; ?>
		<div class="ui-widget-header ui-corner-top">
			<div class="ui-helper-clearfix pad2 b">
				<div class="left pad-l5" style="width:175px;">Restaurant</div>
				<div class="left pad-l5" style="width:145px;">Customer</div>
				<div class="left">Order Total</div>
			</div>
		</div>
		<div class="ui-widget-content ui-corner-bottom">
		<?php foreach($orders as $o): 
			$class = ($i++ % 2 == 0) ? "alt-row" : "";
			?>
			<div class="ui-helper-clearfix marg-b5 pad5 <?php echo $class; ?>">
				<div class="left" style="width:175px;">
					<div class="b"><?php echo $html->link($o['Location']['Advertiser']['name'], '/admin/advertisers/login/'.$o['Location']['Advertiser']['slug'], 
														  array('class'=>'green-link'), 'Are you sure you would like to login as: '.$o['Location']['Advertiser']['name'].'?'); ?></div>
					<div class="b"><?php echo $html->link($o['Location']['name'], '/admin/advertisers/login/'.$o['Location']['Advertiser']['slug'],
														  null, 'Are you sure you would like to login as: '.$o['Location']['Advertiser']['name'].'?'); ?></div>
					<div class="base b">(<?=$time->niceshort($o['Order']['timestamp'])?>)</div>
				</div>
				
				<div class="left" style="width:145px;">
					<div class="b green"><?php echo $html->link($o['User']['fname'].' '.$o['User']['lname'], '#'); ?></div>
				</div>
				
				<div class="right pad-l10"><?php 
					echo $jquery->btn('/admin/orders/details/'.$o['Order']['id'], 'ui-icon-pencil', 'Order Details', null, null, null, '135');
					if($o['Order']['order_started'] != '1') {
						echo $jquery->btn('/admin/orders/confirm/'.$o['Order']['id'], 'ui-icon-circle-check', 'Confirm Order', 
										  'Confirm this order?', null, null, '135');		
					}
					
					
					if($o['Order']['refunded'] != '1') {
						echo $jquery->btn('/admin/orders/refund/'.$o['Order']['id'], 'ui-icon-person', 'Refund Customer', 
											'Are you sure that you would like to refund this order?', null, null, '135');	
					}
					?>
				</div>
				
				<div class="right txt-l b valign-t" style="width:85px">
					<?php echo $number->currency($o['Order']['total']); ?>
					<?php if($o['Order']['refunded'] == '1') { ?>
						<div class="blue">REFUNDED</div>
					<?php } else if($o['Order']['order_started'] == '1') { ?>
						<div class="green">CONFIRMED</div>
					<?php } else { ?>
						<div class="red">UNCONFIRMED</div>
					<?php } ?>
					
					
					
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php } else { ?>
		<a class="b red">There are no orders within this date range.</p>
	<?php } ?>
</div>