<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left"><h1>Order Details</h1></div>
		<div class="right lg"><?php echo $order['Order']['transaction_id']; ?></div>
		<div class="right lg pad-r5">Transaction ID:</div>
	</div>
</div>

<div class="ui-widget-content ui-corner-bottom marg-b20">
	<div class="ui-helper-clearfix marg-b5">
		<div class="left" style="width:315px;">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left pad-r5 b">Order For:</div>
				<div class="left"><?php echo $order['Order']['order_method']; ?></div>
			</div>
		
			<div class="ui-helper-clearfix marg-b5">
				<div class="left" style="width:170px;">
					<div class="b" style="margin-bottom: 2px;">Restaurant Info:</div>
					<div class="b green"><?php echo $order['Location']['name']; ?></div>
					<div><?php echo $order['Location']['address1']?></div>
					<div><?php echo $order['Location']['address2']?></div>
					<div><?php echo $order['Location']['city'].', '.$order['Location']['zip_code_id']; ?></div>
					<div><?php echo $misc->formatPhone($order['Location']['phone']); ?></div>	
				</div>
				<div class="left">
					<div class="b" style="margin-bottom: 2px;">Customer Info:</div>
					<div class="b green"><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></div>
					<div class="marg-b5"><?php echo $misc->formatPhone($order['User']['phone']); ?></div>
				</div>
			</div>
		</div>
		<div class="left">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left b" style="width:110px;">Order Placed:</div>
				<div class="left"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
			</div>
			
			<?php if($order['Order']['refunded'] == '1') { ?>
				<div class="ui-helper-clearfix marg-b5 red">
					<div class="left b" style="width:110px;">Order Refunded:</div>
					<div class="left"><?php echo $time->nice($order['Order']['refunded_timestamp']); ?></div>
				</div>
			<?php } else if($order['Order']['order_started'] == '1') { ?>
				<!-- order has been confirmed -->
				<div class="ui-helper-clearfix marg-b5">
					<div class="left b" style="width:110px;">Order Confirmed:</div>
					<div class="left"><?php echo $time->nice($order['Order']['order_started_timestamp']); ?></div>
				</div>
				<?php if($order['Order']['admin_manual_confirm'] == '1') {
					echo $this->element('warning-msg-box', array('msg' => 'Confirmed Manully by Admin!'));
				} ?>
				
			<?php } else { ?>
				<!-- order has NOT been confirmed -->
				<?php echo $this->element('warning-msg-box', array('msg' => 'This order has NOT been confirmed!')); ?>
			<?php } ?>
		</div>
	
		
	</div>
	
	<?php echo $this->element('order-details', array('order' => $order)); ?>
</div>
<?php echo debug($order); ?>