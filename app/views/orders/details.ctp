<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left"><h1>Order Details</h1></div>
		<?php if($session->check('Advertiser')) { ?>
			<div class="right"><?php echo $jquery->btn('/locations/order_manager/'.$order['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back To Order Manager'); ?></div>
		<?php } ?>
	</div>
</div>

<div class="ui-widget-content ui-corner-bottom marg-b20">
	<div class="ui-helper-clearfix marg-b5">
		<div class="left" style="width:340px;">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left pad-r5 b">Order For:</div>
				<div class="left"><?php echo $order['Order']['order_method']; ?></div>
			</div>
		
			<?php if($session->check('User')) { ?>
				<!-- display the restaurants info for the user -->
				<div class="b" style="margin-bottom: 2px;">Restaurant Info:</div>
				<div class="b green"><?php echo $order['Location']['name']; ?></div>
				<div><?php echo $order['Location']['address1']?></div>
				<div><?php echo $order['Location']['address2']?></div>
				<div><?php echo $order['Location']['city'].', '.$order['Location']['zip_code_id']; ?></div>
				<div><?php echo $misc->formatPhone($order['Location']['phone']); ?></div>
			<?php } else if($session->check('Advertiser')) { ?>
				<!-- display the users info for the restaurant -->
				<div class="b" style="margin-bottom: 2px;">Customer Info:</div>
				<div class="b green"><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></div>
				
				<?php if($order['Order']['order_method'] == 'Delivery') { ?>
					<div>"<?php echo $order['UsersAddr']['name']; ?>"</div>
					<div><?php echo $order['UsersAddr']['address']?></div>
					<div><?php echo $order['UsersAddr']['city'].', '.$order['UsersAddr']['zip']; ?></div>
				<?php } ?>
				
				<div class="marg-b5"><?php echo $misc->formatPhone($order['User']['phone']); ?></div>
				
				<?php if(!empty($order['UsersAddr']['special_instructions'])) { ?>
					<div class="ui-helper-clearfix marg-b5">
						<div class="left pad-r5 b">Special Instructions:</div>
						<div class="left"><?php echo $order['UsersAddr']['special_instructions']; ?></div>
					</div>
				<?php }
				
				if($order['Order']['order_method'] == 'Delivery' && $order['Location']['acct_type'] > 1) { 
					echo $jquery->btn('/orders/delivery_directions/'.$order['Order']['id'], 'ui-icon-image', 'Delivery Directions', null, null, null, '140');
				}
			} ?>
		</div>
		
		<div class="left">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left b pad-r5">Order Placed:</div>
				<div class="left"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
			</div>
			
			<?php if($order['Order']['refunded'] == '1') { 
				echo $this->element('warning-msg-box', array('msg' => 'Order Has Been Refunded!'));
				
			}?>
		</div>
	</div>
	
	<?php echo $this->element('order-details', array('order' => $order)); ?>
</div>