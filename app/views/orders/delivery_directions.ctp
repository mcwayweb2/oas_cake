<?php echo $this->element('js_googlemap_headers'); ?>

<div class="ui-widget-header ui-corner-top pad5">
	<h1>Delivery Directions</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<div class="ui-helper-clearfix marg-b5">
		<div class="left pad-l10">
			<div class="b">Deliver To:</div>
			<div class="b green"><?php echo $order['User']['fname']." ".$order['User']['lname']; ?></div>
			<div>"<?php echo $order['UsersAddr']['name']; ?>"</div>
			<div><?php echo $order['UsersAddr']['address']?></div>
			<div><?php echo $order['UsersAddr']['city'].', '.$order['UsersAddr']['zip']; ?></div>
		</div>
		<div class="right pad-l10 pad-r10">
			<div class="ui-helper-clearfix marg-b5">
				<div class="right"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
				<div class="right b pad-r5">Order Placed:</div>
			</div>
		
			<!-- 
			<div class="ui-helper-clearfix">
				<div class="right"><?php //echo $jquery->btn('#', 'ui-icon-print', 'Print View', null, null, null); ?></div>
			</div> -->
		</div>
	</div>

	<div id="map_placeholder" class="ui-widget-content ui-corner-all">
		<?php
		$default = array('type'=>'0','zoom'=>4,'lat'=>$order['Location']['latitude'],'long'=>$order['Location']['longitude']);
		$addr_start = $order['Location']['address1'].", ".$order['Location']['city'].", ".$order['Location']['State']['abbr']." ".$order['Location']['zip_code_id'];
		
		if(!empty($order['UsersAddr'])) {
			$addr_end = $order['UsersAddr']['address'].", ".$order['UsersAddr']['city'].", ".$order['UsersAddr']['State']['abbr']." ".$order['UsersAddr']['zip'];
			echo $googleMap->directionsMap($default['lat'], $default['long'], $addr_start, $addr_end, $style = "width:565px; height: 300px;");			
		} else {
			?><a class="b red">Sorry. There are currently no delivery directions available for this order.</p><?php
		}
		?>
	</div>
</div>
<?php //echo debug($order); ?>