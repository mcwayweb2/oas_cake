<?php
/*
App::import('Vendor','xtcpdf'); 
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

$tcpdf->SetAuthor("OrderASlice at http://orderaslice.com");
$tcpdf->SetAutoPageBreak( false );
$tcpdf->setHeaderFont(array($textfont,'',12));
$tcpdf->SetHeaderData('pizza.JPG', 15, 'OrderASlice Customer Order', $location['Location']['name']);

$tcpdf->AddPage();
$tcpdf->Cell(90,14, "CUSTOMER INFO: ", 0,0,'L');
$tcpdf->Cell(0,14, "RESTAURANT INFO: ", 0,1,'R');

$tcpdf->Cell(90,0, $session->read('User.fname')." ".$session->read('User.lname'), 0,0,'L');
$tcpdf->Cell(0,0, $location['Location']['address1'], 0,1,'R');

$tcpdf->Cell(90,0, $session->read('User.address'), 0,0,'L');
$tcpdf->Cell(0,0, $location['Location']['city'].", ".$location['State']['abbr']." ".$location['Location']['zip'], 0,1,'R');

$tcpdf->Cell(90,0, $session->read('User.city').", ".$session->read('User.zip'), 0,0,'L');
$tcpdf->Cell(0,0, $location['Location']['phone'], 0,1,'R');

$tcpdf->Cell(90,0, "619-555-1212", 0,1,'L');

$tcpdf->Cell(0,14, "Order Time: ".date("M-d-Y g:i:sa"), 0,1,'L');

$headers = array('Qty', 'Item', 'Total');
$w = array(20, 150, 20);
$tcpdf->SetFillColor(255);
$tcpdf->SetTextColor(0); 
$tcpdf->SetLineWidth(0.3);
$tcpdf->SetFont('', 'B');

//create table header row
for($i = 0; $i < count($headers); $i++) {
	$tcpdf->Cell($w[$i], 7, $headers[$i], 1, 0, 'C', 1);	
}
$tcpdf->Ln();

// Color and font restoration
$tcpdf->SetFillColor(224, 235, 255);
$tcpdf->SetFont('');
$fill = 0;

//order item rows
foreach($session->read('User.Order.items') as $index => $item) {
    $tcpdf->Cell($w[0], 6, $item['qty'], 1, 0, 'C', $fill);
    
    switch($item['type']) {
		case 'item':
			$cell_content = $item['MenusCatsItem']['title'].": ".$item['MenusCatsItem']['description'];
			break;
			
    	case 'special':
    		$cell_content = $item['size']."\" Specialty Pizza: ".$item['Special']['title'];
			break;
			
		case 'custom':
			$cell_content = $item['size']."\" Custom Pizza: ";
    		if($item['sauce'] != 'Regular Sauce' && $item['cheese'] != 'Regular Cheese') { 
				$cell_content .= $item['sauce'].", ".$item['cheese'];	
			} else {
				if($item['cheese'] != 'Regular Cheese') { 
					$cell_content .= $item['cheese'];	
				}
				if($item['sauce'] != 'Regular Sauce') { 
					$cell_content .= $item['sauce'];	
				}
			}
			break;
    }
    $tcpdf->Cell($w[1], 6, $cell_content, 1, 0, 'L', $fill);
    $tcpdf->Cell($w[2], 6, "$".number_format($item['qty']*$item['price'], 2), 1, 0, 'L', $fill);
    $tcpdf->Ln();
    
    //check to see if we need to print out toppings
    if(isset($item['toppings'])) {
	    if(sizeof($item['toppings']) > 0) {
	    	$tcpdf->SetFontSize(8);
			foreach ($item['toppings'] as $id => $value):
				$id_parts = explode("_", $id);
				$tcpdf->Cell($w[0], 6, "", 1, 0, 'C', $fill);
		    	$tcpdf->Cell($w[1], 6, $id_parts[0]." - ".$value, 1, 0, 'L', $fill);
		    	$tcpdf->Cell($w[2], 6, "", 1, 1, 0, 'L', $fill);
			endforeach;
			$tcpdf->SetFontSize(12);
		} else {
			$tcpdf->Cell($w[0], 6, "", 1, 0, 'C', $fill);
		    $tcpdf->Cell($w[1], 6, "Cheese Pizza", 1, 0, 'L', $fill);
		    $tcpdf->Cell($w[2], 6, "", 1, 1, 0, 'L', $fill);	
		}	
    }

    //we need to add another row if there are any special instructions
    if(!empty($item['instructions'])) {
    	$tcpdf->Cell($w[0], 6, "", 1, 0, 'C', $fill);
    	$tcpdf->SetFontSize(8);
	    $tcpdf->Cell($w[1], 6, "Special instructions: ".$item['instructions'], 1, 0, 'L', $fill);
	    $tcpdf->SetFontSize(12);
	    $tcpdf->Cell($w[2], 6, "", 1, 1, 0, 'L', $fill);
    }
    $fill=!$fill;
}

//order total rows
$tcpdf->Cell($w[0]+$w[1], 6, "Subtotal:", 0, 0, 'R');
$tcpdf->Cell($w[2], 6, "$".number_format($session->read('User.Order.subtotal'),2), 0, 1, 'L');

$tcpdf->Cell($w[0]+$w[1], 6, "Tax:", 0, 0, 'R');
$tcpdf->Cell($w[2], 6, "$".number_format($session->read('User.Order.tax'),2), 0, 1, 'L');

$tcpdf->Cell($w[0]+$w[1], 6, "Total:", 0, 0, 'R');
$tcpdf->Cell($w[2], 6, "$".number_format($session->read('User.Order.total'),2), 0, 1, 'L');

$tcpdf->xfootertext = 'Copyright � %d OrderASlice.com. All rights reserved.';
$tcpdf->Output('/home/gaslamp/orderaslice.com/htdocs/app/webroot/orders/'.$filename, 'F');
?>

<?
if(file_exists('/home/gaslamp/orderaslice.com/htdocs/app/webroot/orders/'.$filename)) {
	$session->delete('User.Order');
	echo "<h2>Thank you for your order.</h2><p>To change or cancel your order, please call: ".$location['Location']['name']." - ".$location['Location']['phone']."</p>";
} else {
	echo "<h2>The website is experiencing temporary difficulties.</h2><p>Place a phone order by calling: ".$location['Location']['name']." - ".$location['Location']['phone']."</p>";
}
*/
echo debug($response);
echo debug($resp);

?>
