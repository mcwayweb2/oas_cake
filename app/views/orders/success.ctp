<?php echo $this->element('warning-msg-box', array('msg' => 'Thank You. Your order has been received.')); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left"><h1>Order Confirmation</h1></div>
	</div>
</div>

<div class="ui-widget-content ui-corner-bottom marg-b20">
	<div class="ui-helper-clearfix marg-b5">
		<div class="left" style="width:300px;">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left pad-r5 b">Order For:</div>
				<div class="left"><?php echo $order['Order']['order_method']; ?></div>
			</div>
		
			<!-- display the restaurants info for the user -->
			<div class="b" style="margin-bottom: 2px;">Restaurant Info:</div>
			<div class="b green"><?php echo $order['Location']['name']; ?></div>
			<div><?php echo $order['Location']['address1']?></div>
			<div><?php echo $order['Location']['address2']?></div>
			<div><?php echo $order['Location']['city'].', '.$order['Location']['zip_code_id']; ?></div>
			<div><?php echo $misc->formatPhone($order['Location']['phone']); ?></div>
		</div>
		
		<div class="left">
			<div class="ui-helper-clearfix marg-b5">
				<div class="left b">Order Placed:</div>
				<div class="left pad-l5 pad-r10"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
			</div>
		
			<!--  <div class="b red">Order Status:</div>-->
		</div>
	</div>
	
	<?php echo $this->element('order-details', array('order' => $order)); ?>
</div>