<?php echo $jquery->scrFormSubmit('#btnProspectAdd', '#formProspectAdd'); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left pad-l5"><h1>Request Your Favorite Restaurant</h1></div>
		<div class="right"><?php echo $misc->jqueryBtn('/contact-us', 'ui-icon-mail-closed', 'Contact Us'); ?></div>
	</div>
</div>

<div class="ui-widget-content ui-corner-bottom">
<p>Do you have a favorite restaurant this is not on our site? Fill out as much of the restaurants' contact information as you can, and submit our form. 
We will contact your restaurant, and do our best to get them to sign up with our website. You should be able order online with your chosen restaurant 
in no time!</p>

<div class="contacts form">
	<div class="clearfix">
		<div class="left"> <!--  style="width:390px; padding: 0px 10px;"-->
			<?php echo $form->create('Prospect', array('action'=>'add', 'id'=>'formProspectAdd'));?>
			<fieldset> <!--  style="width:370px;"-->
		 		<legend>Restarant Info</legend>
		 		<table width="100%" class="form-table">
		 			<tr>
		 				<td class="label-req">Restaurant Name: <span class="required">*</span></td>
		 				<td><?=$form->input('name', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Restaurant Contact:</td>
		 				<td><?=$form->input('contact', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Phone:</td>
		 				<td><?=$form->input('phone', array('class'=>'form-text','label'=>'', 'div'=>false));?><br />
		 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Website:</td>
		 				<td><?=$form->input('website', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Address:</td>
		 				<td><?=$form->input('address', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">City: <span class="required">*</span></td>
		 				<td><?=$form->input('city', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">State: <span class="required">*</span></td>
		 				<td><?=$form->select('state_id', $states, '5', array('class'=>'form-text','label'=>false), 'Select your State');?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req" valign="top">Zip Code:</td>
		 				<td><?=$form->input('zip', array('class'=>'form-text','label'=>''));?></td>
		 			</tr>
		 			<tr>
		 				<td></td>
		 				<td><?php echo $jquery->btn('#', 'ui-icon-comment', 'Submit Request', null, 'txt-r', 'btnProspectAdd', '130'); ?></td>
		 			</tr>
		 		</table>
		 		</form>
			</fieldset>
		</div><!--  
		<div class="left ui-corner-all pad3 gray-border" style="width:160px;height:600px;border: 1px solid;" >
			<?php //echo $html->image('pizza-ad-160x600-2.jpg'); ?>
		</div>-->
	</div>
	<div class="clear"></div>
</div>
</div>