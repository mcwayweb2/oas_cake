<div class="b red">(<?php echo sizeof($errors) + sizeof($already_in_db); ?>) Errors Adding Restaurant Prospects.</div>
<?php if(sizeof($errors) > 0) { ?>
	<ol>
		<?php foreach($errors as $p): ?>
			<li><span class="red">ERROR ADDING: </span><span class="b"><?php echo $p['Prospect']['name']; ?>: </span><?php echo $p['Prospect']['address'].', '.$p['Prospect']['city'].', '.$p['Prospect']['zip_code_id']; ?></li>
		<?php endforeach; ?>
	</ol>
<?php } ?>
<?php if(sizeof($already_in_db) > 0) { ?>
	<ol>
		<?php foreach($already_in_db as $p): ?>
			<li><span class="red">ALREADY IN DB: </span><span class="b"><?php echo $p['Prospect']['name']; ?>: </span><?php echo $p['Prospect']['address'].', '.$p['Prospect']['city'].', '.$p['Prospect']['zip_code_id']; ?></li>
		<?php endforeach; ?>
	</ol>
<?php } ?>

<div class="b green marg-t20">(<?php echo sizeof($success); ?>) Restaurant Prospects Added Successfully.
	<?php if($geocode_errors > 0) { ?><span class="red">(<?php echo $geocode_errors; ?>) Geocode Errors.</span><?php } ?>
</div>
<?php if(sizeof($success) > 0) { ?>
	<ol>
		<?php foreach($success as $p): ?>
			<li>
				<span class="b"><?php echo $p['Prospect']['name']; ?>: </span>
				<?php if($p['Prospect']['latitude'] == '0.00') { ?>
					<span class="red b">
				<?php } ?>
				
				<?php echo $p['Prospect']['address'].', '.$p['Prospect']['city'].', '.$p['Prospect']['zip_code_id']; ?>
				<?php if($p['Prospect']['latitude'] == '0.00') { ?> (GEOCODE ERROR!)</span><?php } ?>
			</li>
		<?php endforeach; ?>
	</ol>
<?php } ?>

<?php //echo debug($errors); ?>