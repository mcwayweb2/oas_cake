<?php echo $jquery->scrFormSubmit('#btnProspectAdminIndex', '#formProspectAdminIndex'); ?>
<?php echo $jquery->scrFormSubmit('#btnProspectAdminAdd', '#formProspectAdminAdd'); ?>
<?php echo $jquery->scrFormSubmit('#btnProspectAdminMultDelete', '#formProspectAdminMultDelete'); ?>
<?php echo $this->element('js_googlemap_headers'); ?>

<script>
$(function() {
	//$('#adminDashTabs').tabs();
	$('.progress-image, .progress-success').hide();
	<?php if(!$show_add_form) { ?>
		$('#addProspectFieldset').hide();	
	<?php } ?>

	$('.link-mark-contacted').click(function() {
		var contactDiv = $(this).parent();
		var idParts = $(this).attr('id').split('-');
		//replace button with loading animation
		contactDiv.html('<img src="/img/ajax-loader-med-bar.gif" />').
				   load($(this).attr('href'), { id: idParts[1] });
		return false;
	});

	$('.link-leave-comment').click(function() {
		//save admin comment via ajax
		var progressImage = $(this).next('.progress-image');
		progressImage.show();

		var commentField = $(this).nextAll('div.textarea').children('label').children('textarea');
		var idParts = commentField.attr('id').split('-');
		
		$(this).nextAll('.progress-success').load('/prospects/ajax_admin_comment_response', 
												  { id: idParts[1], comment: commentField.val() }, function() {
			progressImage.hide();
		});
		return false;
	});

	$('#btnToggleAdminAdd').toggle(function() {
		$('#addProspectFieldset').slideDown();
	}, function() {
		$('#addProspectFieldset').slideUp();
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left pad-l5"><h1>Leads Manager</h1></div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', '', null, null, 'btnToggleAdminAdd'); ?></div>
	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Prospect', array('action' => 'admin_add', 'id' => 'formProspectAdminAdd')); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b10" id="addProspectFieldset"><legend class="ui-corner-all">Add Restaurant Lead</legend>
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Restaurant Name:</div>
				<div><?php echo $form->input('name', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false)); ?></div>
			</div>
			
			<div class="left">
				<div class="b base">Address:</div>
				<div><?php echo $form->input('address', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false, 'style' => 'width:340px;')); ?></div>
			</div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Phone:</div>
				<div><?php echo $form->input('phone_temp', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false, 'style' => 'width:100px;')); ?></div>
			</div>
			
			<div class="left pad-r10">
				<div class="b base">Website:</div>
				<div><?php echo $form->input('website', array('class' => 'form-text ui-state-default ui-corner-all', 'label' => false, 'style' => 'width:170px;')); ?></div>
			</div>
			
			<div class="left pad-r10">
				<div class="b base">Region:</div>
				<div><?php echo $form->select('region_id', $regions, null, array('class' => 'no-uniform-style pad2', 'style' => 'width:120px;'), false); ?></div>
			</div>
			
			<div class="left">
				<div class="left" style="padding-top: 10px;"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Restaurant', null, null, 'btnProspectAdminAdd'); ?></div>
			</div>
		</div>
	</fieldset>
	</form>

	<?php echo $form->create('Prospect', array('action' => 'admin_index', 'id' => 'formProspectAdminIndex')); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b5"><legend class="ui-corner-all">Search Restaurant Leads</legend>
		<div class="ui-helper-clearfix">
			<div class="left pad-r20">
				<div class="b base">Search By Restaurant Keyword(s):</div>
				<div><?php echo $form->input('keywords', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left pad-r10">
				<div class="b base">Search By City, Zip, or Region:</div>
				<div><?php echo $form->input('city', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left" style="padding-top: 8px;"><?php echo $jquery->btn('#', 'ui-icon-search', 'Search Restaurants', null, null, 'btnProspectAdminIndex'); ?></div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Filter by Contact Status:</div>
				<?php $opts = array('0' => 'Not Contacted', '1' => 'Contacted'); ?>
				<div><?php echo $form->select('contacted', $opts, null, array(), 'Show All'); ?></div>
			</div>
			<div class="left pad-r5" style="width:100px;">
				<div class="b base">Records Per Pg:</div>
				<div><?php echo $form->input('page_limit', array('class' => 'form-text ui-state-default ui-corner-all', 'style' => 'width:85px', 'label' => false, 'value' => '50')); ?></div>
			</div>
			<div class="left pad-r5" style="width:100px;">
				<div class="b base">Search Type:</div>
				<?php $opts = array('Radial' => 'Radial', 'Exact' => 'Exact'); ?>
				<div><?php echo $form->select('search_type', $opts, null, array('style' => 'width:85px;', 'class' => 'no-uniform-style'), false); ?></div>
			</div>
			<div class="left">
				<div class="ui-helper-clearfix marg-t10">
					<div class="left"><?php echo $form->checkbox('show_map', array('checked' => 'checked')); ?></div>
					<div class="left b">Show on Map?</div>
				</div>
				<div class="ui-helper-clearfix marg-t10">
					<div class="left"><?php echo $form->checkbox('print_view'); ?></div>
					<div class="left b">Show in Print View?</div>
				</div>
			</div>
		</div>
	</fieldset>
	</form>
	<hr />
	
	<?php if(isset($show_map) && sizeof($prospects) > 0) { ?>
		<div id="map_placeholder">
			<?php
			$dimensions = ($print_view_mode) ? 'width:800px; height: 400px;' : 'width:585px; height: 300px;';
			$default = array('type'=>'0','zoom'=>6,'lat'=>$prospects[0]['Prospect']['latitude'],'long'=>$prospects[0]['Prospect']['longitude']);
			
			echo $googleMap->map(	
					$default,
					$dimensions,
					false,
					'ui-corner-all gray-border',
					array('points'=> $prospects));	
			?>
		</div>
	<?php } ?>
	
	<?php if(isset($hdr_txt)) { ?><div class="b marg-t10 marg-b5 pad-l5"><?php echo $hdr_txt; ?></div><?php } ?>
	
	<?php if(sizeof($prospects) > 0) { ?>
		<?php echo $form->create('Prospect', array('action' => 'admin_multiple_delete', 'id' => 'formProspectAdminMultDelete')); ?>
		<div class="ui-helper-clearfix marg-b10 b">
			<div class="left pad-l5 pad-r10">
				<?php echo $html->link('Delete Selected Lead(s)', '#', array('id' => 'btnProspectAdminMultDelete'), 'Are you sure you want to delete these Restaurants?'); ?>
			</div>
		</div>
		
		<div class="ui-widget-header ui-corner-top pad2">
			<div class="ui-helper-clearfix">
				<div class="left txt-r" style="width:100px;">Restaurant</div>
				<div class="left txt-r" style="width:270px;">Admin Comments</div>
			</div>
		
		</div>
		<div class="ui-widget-content ui-corner-bottom">
		<?php $p_ct = 1;?>
		<?php foreach($prospects as $p): ?>
			<div class="ui-helper-clearfix marg-b5 pad-b5" style="border-bottom:1px solid #F67D07;">
				<div class="left ui-corner-all pad-r5" style="width:20px">
					<?php echo $form->checkbox('locs.'.$p['Prospect']['id']); ?>									   
				</div>
				<div class="left b" style="width:235px;">
					<div class="orange"><?php echo $p_ct++.') '.$p['Prospect']['name']; ?></div>
					<div class="green base pad-b5"><?php echo $p['Prospect']['sic_desc']; ?></div>
					<div class="base">
						Contact: <?php echo $p['Prospect']['contact_fname'].' '.$p['Prospect']['contact_lname'].', '.$p['Prospect']['contact_title'].
											'<br />'.$p['Prospect']['address'].'<br />'.$p['Prospect']['city'].', '.$p['State']['abbr'].' '.$p['ZipCode']['id']?></div>
				</div>
				<div class="left b base" style="width:<?php echo (isset($print_view_mode) && $print_view_mode) ? '400px' : '150px'; ?>;">
					<div class="ui-helper-clearfix">
						<?php echo $html->link('Save Comment', '#', array('class'=>'link-leave-comment left', 'id'=>'')); ?>
						<div class="left progress-image"><?php echo $html->image('ajax-loader-sm-bar.gif', array('style'=>'width:40px;')); ?></div>
						<div class="left progress-success"></div>
						<?php echo $form->input('response_notes.'.$p['Prospect']['id'], 
											   array('rows' => '5', 'value'=>$p['Prospect']['response_notes'], 'style'=>'font-size:90%; width:'.((isset($print_view_mode) && $print_view_mode) ? '400px' : '150px').';', 
											   		 'class'=>'base ui-corner-all pad2 ui-state-default admin-comments-field',
											   		 'id'=>'comments-'.$p['Prospect']['id'], 'type' => 'textarea', 'label' => false)); ?>
					</div>
				</div>
				<div class="right" style="width:150px;">
					<?php echo $jquery->btn('/admin/prospects/delete/'.$p['Prospect']['id'], 'ui-icon-trash', 'Delete Lead', 
											'Completely Remove this Restauarant Lead from Database?', null, null, '145'); ?>
											
					<?php if($p['Prospect']['contacted'] == '1') { ?>
						<div class="base b pad-l5">Contacted:<br /><?php echo $time->nice($p['Prospect']['contacted_timestamp']); ?></div>
					<?php } else { ?>
						<div class="marg-b10 txt-c">
							<?php echo $jquery->btn('/prospects/ajax_admin_mark_contacted', 'ui-icon-circle-check', 'Mark as Contacted', 
													null, 'link-mark-contacted', 'contacted-'.$p['Prospect']['id'], '145'); ?>
							
						</div>
					<?php } ?>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
		</form>
	<?php } else { ?>
		<a class="b red">There are no Restaurant Leads that match your search criteria.</p>
	<?php } ?>
</div>
<div class="marg-t10"><?php echo $this->element('pagination_links'); ?></div>
<?php //echo debug($prospects); ?>