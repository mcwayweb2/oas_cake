<script>
$(function() {
	$('#adminDashTabs').tabs();
	$('.progress-image, .progress-success').hide();

	$('.link-leave-comment').click(function() {
		//save admin comment via ajax
		var progressImage = $(this).next('.progress-image');
		progressImage.show();
		
		var commentField = $(this).nextAll('.admin-comments-field');		
		var locIdParts = commentField.attr('id').split('-');

		$(this).nextAll('.progress-success').load('/advertisers/ajax_admin_comment_response', 
												  { id: locIdParts[1], comment: commentField.val() }, function() {
			progressImage.hide();
		});
		
		return false;
	});
});
</script>

<div id="adminDashTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a href="#pending-menus">Menus Pending Review <span class="red">(<?php echo sizeof($pending_menus); ?>)</span></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#customer-support">Pending Customer Support <span class="red">(<?php echo sizeof($cust_support); ?>)</span></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#unsettled-orders">Unsettled Orders <span class="red">(<?php echo sizeof($unsettled_orders); ?>)</span></a>
		</li>
	</ul>
	<div id="pending-menus" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php if(sizeof($pending_menus) > 0) { ?>
			<div class="ui-widget-header ui-corner-top pad2">
				<div class="ui-helper-clearfix">
					<div class="left txt-r" style="width:160px;">Restaurant</div>
					<div class="left txt-r" style="width:230px;">Admin Comments</div>
				</div>
			
			</div>
			<div class="ui-widget-content ui-corner-bottom">
			<?php foreach($pending_menus as $menu): ?>
				<div class="ui-helper-clearfix marg-b5 pad-b5" style="border-bottom:1px solid #F67D07;">
					<div class="left ui-corner-all pad-r5" style="width:80px">
						<?php 
						echo $html->image($misc->calcImagePath($menu['Location']['logo'], 'files/logos/'), 
										  array('class'=>'orange-border pad2', 'style'=>'width:75px;'));
						?>									   
					</div>
					<div class="left b" style="width:180px;">
						<div class="orange"><?php echo $menu['Advertiser']['name']; ?></div>
						<div class="green base"><?php echo $menu['Location']['name']; ?></div>
						<div style="width:150px;" class="marg-t5">
							<?php $msg = ($menu['Location']['menu_ready'] == '2') ? 'Pending Setup' : 'Pending Review'; ?>
							<?php echo $this->element('warning-msg-box', array('msg' => $msg)); ?>
							
							<?php if($menu['numPayProfiles'] <= 0) { 
								echo $this->element('error-msg-box', array('msg' => 'No Billing Profile!'));
							} ?>
						</div>
					</div>
					<div class="left b base" style="width:135px;">
						<div class="ui-helper-clearfix">
							<?php echo $html->link('Save Comment', '#', array('class'=>'link-leave-comment left', 'id'=>'')); ?>
							<div class="left progress-image"><?php echo $html->image('ajax-loader-sm-bar.gif', array('style'=>'width:40px;')); ?></div>
							<div class="left progress-success"></div>
							<?php echo $form->textarea('admin_comments.'.$menu['Location']['id'], 
													   array('rows' => '5', 'value'=>$menu['Location']['admin_comments'], 'style'=>'font-size:90%; width:125px;', 
													   		 'class'=>'base ui-corner-all pad2 ui-state-default admin-comments-field',
													   		 'id'=>'comments-'.$menu['Location']['id'])); ?>
						</div>
					
						
						
					</div>
					<div class="right" style="width:145px;">
						<?php echo $jquery->btn('mailto:'.$menu['Location']['contact_email'], 'ui-icon-mail-closed', 'Send Email', null, null, null, '140'); ?>
						<?php echo $jquery->btn('/admin/advertisers/login/'.$menu['Location']['slug'], 'ui-icon-image', 'Menu Manager', null, null, null, '140'); ?>
						
						<?php 
						if($menu['Location']['menu_ready'] == '2') {
							//menu is pending review
							echo $jquery->btn('/admin/locations/toggle_menu_ready/'.$menu['Location']['id'].'/1', 'ui-icon-search', 'Ready for Review', null, null, null, '140');
						} else {
							//menu is waiting to be approved
							echo $jquery->btn('/admin/locations/toggle_active/'.$menu['Location']['id'], 'ui-icon-circle-check', 'Approve', null, null, null, '140');
						} ?>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
		<?php } else { 
			echo $this->element('warning-msg-box', array('msg' => 'There are currently no pending menus.'));
		} ?>
	</div>
	<div id="customer-support" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php echo debug($pending_support); ?>
	</div>
	<div id="unsettled-orders" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<?php if(sizeof($unsettled_orders) > 0) { ?>
			<div class="ui-widget-header ui-corner-top pad2">
				<div class="ui-helper-clearfix">
					<div class="left pad-l10" style="width:250px;">Restaurant</div>
					<div class="left">Customer</div>
				</div>
			
			</div>
			<div class="ui-widget-content ui-corner-bottom">
			<?php foreach($unsettled_orders as $order): ?>
				<div class="marg-b5 pad-b5" style="border-bottom:1px solid #F67D07;">
					<div class="ui-helper-clearfix">
						<div class="left" style="width:255px;">
							<div class="ui-helper-clearfix marg-b5 red">
								<div class="left pad-r5 b">Order Placed:</div>
								<div class="left"><?php echo $time->nice($order['Order']['timestamp']); ?></div>
							</div>
							
							<div class="green b"><?php echo $order['Location']['name']; ?></div>
							<div class=""><span class="b pad-r5">Contact:</span> <?php echo $order['Location']['contact_fname'].' '.$order['Location']['contact_lname']; ?></div>
							<div style="padding-left:55px;"><?php echo $misc->formatPhone($order['Location']['contact_phone']); ?></div>
						</div>
						<div class="left">
							<div class="green b"><?php echo $order['User']['fname'].' '.$order['User']['lname']; ?></div>
							<div class=""><?php echo $misc->formatPhone($order['User']['phone']); ?></div>
						</div>
						<div class="right">
							<?php echo $jquery->btn('/admin/orders/confirm/'.$order['Order']['id'], 'ui-icon-circle-check', 'Confirm Order', null, null, null, '135'); ?>
							<?php echo $jquery->btn('/admin/orders/refund/'.$order['Order']['id'], 'ui-icon-person', 'Refund Customer', 
													'Are you sure that you would like to refund this order?', null, null, '135'); ?>
							<?php echo $jquery->btn('/admin/orders/details/'.$order['Order']['id'], 'ui-icon-pencil', 'Order Details', null, null, null, '135'); ?>
						</div>
					</div>	
				</div>
			<?php endforeach; ?>
			</div>
		<?php } else { 
			echo $this->element('warning-msg-box', array('msg' => 'There are currently no unsettled orders.'));
		} ?>
		<?php //echo debug($unsettled_orders); ?>
	</div>
</div>