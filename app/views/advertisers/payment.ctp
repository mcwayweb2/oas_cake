<h1>Restaurant Registration >> Payment Information</h1><hr />
<p>Please enter your primary billing information below. Once your account is created, you may setup and manage multiple 
payment profiles for your company. The following payment method will be used for all transactions unless otherwise specified.</p>
<?php echo $form->create('Advertiser', array("action"=>"payment")); ?>
<?php echo $form->input('id'); ?>
<fieldset>
	<legend>Primary Billing Information</legend>
	<table class="form-table">
		<tr>
			<td class="label-req">First Name on card: <span class="required">*</span></td>
			<td><?=$form->input('billing_fname', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<td class="label-req">Last Name on card: <span class="required">*</span></td>
			<td><?=$form->input('billing_lname', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
 			<td class="label-req">Address: <span class="required">*</span></td>
			<td><?=$form->input('billing_address', array('label'=>'','class'=>'form-text'))?></td>
 		</tr>
 		<tr>
 			<td class="label-req">City: <span class="required">*</span></td>
 			<td><?=$form->input('billing_city', array('label'=>'','class'=>'form-text'))?></td>
 		</tr>
 		<tr>
 			<td class="label-req">State: <span class="required">*</span></td>
 			<td><?=$form->select('billing_state', $states, null, array('label'=>'','class'=>'form-text'), false)?></td>
 		</tr>
 		<tr>
 			<td class="label-req">Zip: <span class="required">*</span></td>
 			<td><?=$form->input('billing_zip', array('label'=>'','class'=>'form-text'))?></td>
 		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?=$html->image('creditcard_amex.gif')?>   
			</td>
		</tr>
		<tr>
			<td class="label-req">Card Number: <span class="required">*</span></td>
			<td><?=$form->input('cc_num', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<td class="label-req">Card Exp: <span class="required">*</span></td>
			<td><?=$form->month('month', null, array('class'=>'form-select'), false)?> <?=$form->year('year', date("Y")+10, date("Y"), null, array('class'=>'form-select'), false)?></td>
		</tr>
	</table>
</fieldset>
<br />
<?php echo $form->end('Submit');?>