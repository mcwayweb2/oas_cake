<h1>Add New Business</h1><hr />
<div class="advertisers form">
<?php echo $form->create('Advertiser', array("type"=>"file"));?>
	<fieldset>
 		<legend><?php __('Business Info');?></legend>
	<?php
		echo $form->input('name', array('label' => 'Business Name'));
		echo $form->input('logo', array("label" => "Logo",
										"type"  => "file"));
		echo $form->input('active');
	?>
	</fieldset>
	<fieldset>
		<legend><?php __('Contact Info');?></legend>
		<?php
		echo $form->input('contact_fname', array('label' => 'First Name'));
		echo $form->input('contact_lname', array('label' => 'Last Name'));
		echo $form->input('contact_title', array('label' => 'Title'));
		echo $form->input('contact_phone', array('label' => 'Phone'));
		echo $form->input('email', array('label' => 'Email'));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Businesses', true), array('action'=>'index'));?></li>
	</ul>
</div>
