<?php echo $jquery->scrFormSubmit('#btnAdForgotPw', '#formAdForgotPw'); ?>
<div class="ui-widget-header pad5 ui-corner-top">Reset Advertiser Password</div>
<div id="userForgotPw" class="ui-widget-content ui-corner-bottom">
	
	<?=$form->create('Advertiser', array('action' => 'password_reset', 'id' => 'formAdForgotPw'))?>
	<p>Forgot your password? Use the form below to reset your password. An email will be sent to your registered email 
	address with your new password. <?php echo $html->link('Forget your username?', '/advertisers/recover_username'); ?></p>
	<table class="form-table">
		<tr>
			<th class="label-req" style="width:100px;">Username: <span class="required">*</span></th>
			<td><?=$form->input('username', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr>
			<th class="label-req">Email: <span class="required">*</span></th>
			<td><?=$form->input('email', array('class'=>'form-text', 'label'=>''))?></td>
		</tr>
		<tr><td></td><td>
		<div style="width:145px;"><?php echo $misc->jqueryBtn('#', 'ui-icon-unlocked', 'Recover Password', null, null, 'btnAdForgotPw'); ?></div></td></tr>
	</table>
</div> 