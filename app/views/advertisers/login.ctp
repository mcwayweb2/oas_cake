<?php 
$js_block = "
$(function() {
	$('#btnAdLogin2').click(function() {
		$('#AdLoginForm2').submit();	
	});
});";
echo $html->scriptBlock($js_block, array('inline'=> false));
echo $form->create('Advertiser', array("action"=>"login", 'id'=>'AdLoginForm2'));?>
<div class="ui-widget">
	<div class="ui-corner-top ui-widget-header pad5">Advertiser Login</div>
	<div class="ui-corner-bottom ui-widget-content">
		<table class="form-table">
			<tr>
				<th class="label-req">Username: <span class="required">*</span></th>
				<td><?=$form->input('loginUsername',array("class" => "form-text",
													 "label" => ""))?></td>
			</tr>
			<tr>
				<th class="label-req">Password: <span class="required">*</span></th>
				<td><?=$form->password('loginPassword',array("class"=>"form-text"))?></td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="ui-helper-clearfix basefont required">
						<div style="margin-left:130px;" class="left pad-r5"><?php echo $misc->jqueryBtn('#', 'ui-icon-locked', 'Login', null, null, 'btnAdLogin2'); ?></div>
						<div class="left pad-r5"><?php echo $misc->jqueryBtn('/advertisers/register', 'ui-icon-pencil', 'Register'); ?></div>
						<div class="left"><?php echo $misc->jqueryBtn('/advertisers/password_reset', 'ui-icon-unlocked', 'Forgot Password'); ?></div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>
</form>