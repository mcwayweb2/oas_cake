<div>
<?=$this->element('js_add_tabs') ?>
<div class="clearfix">
	<div class="left"><h1>Advertisement Manager</h1></div>
	<div class="right"><?=$html->link('Have Us Design Your Image Ad!', '/advertisers/custom_ad') ?></div>
</div>
<div class="clear"></div>
<hr />
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit tempus molestie. In pretium iaculis lorem id ultricies. Curabitur lobortis sapien 
ac magna ullamcorper faucibus. Aenean ac nibh diam. Donec id felis nunc, et bibendum erat. Nulla faucibus, lacus iaculis aliquet ultricies, diam nibh 
auctor risus, eget placerat est orci sed arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
<fieldset>
	<legend>Upload New Advertisement</legend>
	<?=$form->create('AdvertisersAd', array('controller'=>'advertisers', 'action'=>'manage_ads', 'type'=>'file')) ?>
	<?php $options = array('Home' => 'Home Page (320x250)',
					    'Contact' => 'Contact Page (120x240)',
				        'Register' => 'User Registration Page (160x600)'); ?>
	<table>
		<tr>
			<td class="bold">Title: <span class="required">*</span></td>
			<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false))?></td>
		</tr>
		<tr>
			<td class="bold">Ad Type: <span class="required">*</span></td>
			<td><?=$form->select('type', $options, false, array("class"=>"form-select"), null)?></td>
		</tr>
		<tr>
			<td class="bold">Ad Image: <span class="required">*</span></td>
			<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false))?></td>
		</tr>
		<tr>
			<td class="bold"></td>
			<td><?=$form->end('Upload')?></td>
		</tr>
	</table>
</fieldset>
<div id="example">
	<ul>
		<li><a href="#home-ads">Home Page Ads</a></li>
		<li><a href="#sub-ads">Sub Page Ads</a></li>
	</ul>
	<div id="home-ads">
		<?=debug($home_ads) ?>	
	</div>
	<div id="sub-ads">
		<?=debug($sub_ads) ?>	
	</div>
</div>
<p>Do you really want that extra edge, to stand out to our customers? We offer a couple of very affordable advertising options on our site, that will really get your company to stand out to our site users.
		<fieldset>
	 		<legend>Advertisement Billing</legend>
	 		 <p>Our most popular option, is ad space on our website. Ad space on our site can been placed in 3 different pages. The adspace on our 
	 		 <?=$html->link('Home Page', '/') ?> is 320x250, and our most popular option. It appears on the lower left of about a dozen of the pages on the site.</p>
	 		 
	 		 
	 		 The adspace on our <?=$html->link('User Registration Page', '/users/register') ?> is 160x600. 
	 		 The ad space on our <?=$html->link('Contact Page', '/contact-us') ?> is 120x240.</p>   
	 		 <p>The ad can contain anything to promote your company. Use our upload tool to upload you own ad images, or let our excellent graphic design 
	 		 team <?=$html->link('Create a Custom Ad', '/') ?> for you. You may upload and store as many images as you would like on our site for your use. 
	 		 Subscriptions start at just $19/month and you may have up to 6 ad subscriptions at a time.</p> 
	 		 <hr />
	 		 <p><b>Sign me up for:</b><br />
	 		 <?php $options = array(0,1,2,3); ?>
	 		 <?=$form->select('adspaces_home', $options, 1, array('class'=>'form-select'), false) ?> 
	 		 Home page ad space subscriptions.(<span class="basefont">$29/month, each subscription.</span>)<br />
	 		 <?=$form->select('adspaces', $options, 1, array('class'=>'form-select'), false) ?> 
	 		 Sub page ad space subscriptions.(<span class="basefont">$19/month, each subscription.</span>)
	 		 </p>
	 	</fieldset><br />
