<script>
$(function() {
	$('#btnAdRecoverUsername').click(function() {
		$('#formAdRecoverUsername').submit();	
	});
});
</script>

<div class="ui-widget-header pad5 ui-corner-top">Recover Account Username</div>
<div class="ui-widget-content ui-corner-bottom">
	<?=$form->create('Advertiser', array('action' => 'recover_username', 'id' => 'formAdRecoverUsername'))?>
	<p>Forgot your username? Use the form below to recover your username. An email will be sent to your registered email 
	address with the username on file.</p>
	<table class="form-table">
		<tr>
			<th class="label-req">Email: <span class="required">*</span></th>
			<td><?php echo $form->input('email', array('class'=>'form-text', 'label'=>'')); ?></td>
		</tr>
		<tr><td></td><td>
		<div style="width:145px;"><?php echo $misc->jqueryBtn('#', 'ui-icon-unlocked', 'Recover Username', null, null, 'btnAdRecoverUsername'); ?></div></td></tr>
	</table>
</div> 