<script type="text/javascript">
$(document).ready(function() {
	// make the categories accordian style
	$('#advertiserOrdersAccordion').accordion({
		autoHeight: true
	});
	//$('.no-display').removeAttr('style');
});
</script>

<?php if($session->read('Advertiser.is_billing_profile_set') == '0') { 
	echo $this->element('warning-msg-box', array('msg'=>'NOTE: You must '.$html->link('Add a Billing Profile', '/advertisers/payment_profiles').' before your restaurant(s) can accept online orders.'));
} 

if(sizeof($ad_locations) > 0) {
	//make sure advertiser has added at least one location
	echo $this->element('warning-msg-box', array('msg'=>'NOTE: You must '.$html->link('Add a Restaurant Location', '/locations/add').' to get started.'));
} else {
	//advertiser is ready...display current orders ?>
	<div class="ui-widget-header ui-corner-top pad5">
		<div class="pad-l5"><h1>Today's Orders</h1></div>
	</div>
	<div class="ui-widget-content ui-corner-bottom">
		<?php //echo debug($ad_locations); ?>
		<?php if(sizeof($ad_locations) > 0) { ?>
			<div class="ui-widget ui-accordion no-display" id="advertiserOrdersAccordion">
			<?php foreach ($ad_locations as $adloc): ?>
				<h2 class="ui-widget-header ui-accordion-header" id="ad-order-hdr-<?php echo $adloc['Location']['id']; ?>">
	         		<a href="#"><?php 
	         			echo $adloc['Location']['name']." ";
	         			if(isset($orders[$adloc['Location']['id']]) && $orders[$adloc['Location']['id']]['unsettled_ct'] > 0) { 
	         				?><span class="pad-l10 red">(<?php echo $orders[$adloc['Location']['id']]['unsettled_ct']; ?>) Pending Unconfirmed Orders</span><?php 
	         			} ?>
	         		</a>
	         	</h2>
				<div class="ui-widget-content ui-accordion-content" id="ad-order-drawer-<?php echo $adloc['Location']['id']; ?>">
					<?php if($adloc['Location']['active'] == '1' && $adloc['Location']['menu_ready'] == '1') { 
						if(isset($orders[$adloc['Location']['id']]) && sizeof($orders[$adloc['Location']['id']]) > 1) { 
							$i = 0;							
							?>
							<div class="ui-helper-clearfix marg-b5">
								<div class="right pad-r10 b"><?php echo $html->link('Search Previous Orders', '/locations/order_manager/'.$adloc['Location']['slug']); ?></div>
							</div>
							
							<div class="ui-widget-header ui-corner-top">
								<div class="ui-helper-clearfix pad2 b">
									<div class="left pad-l5" style="width:150px;">Customer</div>
									<div class="left">Order Total</div>
								</div>
							</div>
							<div class="ui-widget-content ui-corner-bottom"><?php
							
							foreach ($orders[$adloc['Location']['id']] as $index => $order):
								if(is_numeric($index)) {
									$class = ($i++ % 2 == 0) ? "alt-row" : "";
									if($order['Order']['order_started'] == '0') { $class .= " ui-state-highlight ui-corner-all"; }
									?>
									<div class="ui-helper-clearfix marg-b5 pad5 <?php echo $class; ?>">
										<div class="left" style="width:150px">
											<div class="b green"><?php echo $html->link($order['User']['fname'].' '.$order['User']['lname'], '/users/order_history/'.$order['User']['id']); ?></div>
											<div class="base b">(<?=$time->niceshort($order['Order']['timestamp'])?>)</div>
										</div>
										<div class="left b"><?php echo $number->currency($order['Order']['total']); ?></div>
										
										<?php if($order['Order']['order_started'] == '1') { ?>
											<div class="right pad-l10"><?php echo $jquery->btn('/orders/details/'.$order['Order']['id'], 'ui-icon-mail-closed', 'Order Details'); ?></div>
											<div class="right pad-l10" style="width:140px;">
												<?php if($order['Order']['order_method'] == 'Delivery' && $order['Location']['acct_type'] > 1) { ?>
													<?php echo $jquery->btn('/orders/delivery_directions/'.$order['Order']['id'], 'ui-icon-image', 'Delivery Directions'); ?>
												<?php } else { echo '&nbsp;'; } ?>
											</div>
										<?php } else { ?>
											<div class="right pad-l10" style="width:140px;"><?php echo $jquery->btn('/orders/confirm/'.$order['Order']['confirm_hash'].'/1', 'ui-icon-alert', 'Confirm Order'); ?></div>
										<?php } ?>
									</div>
									<?	
								}
							endforeach;	
							?></div><?php
						} else {
							//location menu is currently online, but has not received any orders for the day yet
							?><div style="height:50px;">
								<a class="b green">This location has not received any online orders today. <?php echo $html->link('Search Previous Orders', '/locations/order_manager/'.$adloc['Location']['slug']); ?></p>
								
							</div><?php
						}
					} else { 
						if($adloc['Location']['active'] == '1') {
							//menu is ready, but was taken down by client
							?>
							<div class="ui-helper-clearfix" style="height:220px;">
								<div class="left" style="width:350px;">
									<div class="b red marg-b5">Restaurant Menu is Currently Offline:</div>
									<div>You have taken this menu offline. You will not receive any new Online Orders until you 
									<?php echo $html->link('Put This Menu Online', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1'); ?>. 
									</div>
								</div>
								<div class="right"><?php 
									echo $jquery->btn('/restaurants/edit_menu/'.$adloc['Location']['slug'], 'ui-icon-contact', 'Menu Manager');
									echo $jquery->btn('/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1', 'ui-icon-circle-check', 'Start Receiving Orders');
								?></div>
							</div>
							<?		
							
						} else if($adloc['Location']['menu_ready'] == '1') {
							//menu ready, pending admin approval
							?>
								<?php if($session->read('Advertiser.is_billing_profile_set') == '1') { ?>
									<div style="height:60px;" class="b green">
										<p>Your Restaurant Menu is Set Up, and Pending Review.</p>
										<a class="">NOTE: You must <?php echo $html->link('Add a Billing Profile', '/advertisers/payment_profiles'); ?> before your restaurant(s) can accept online orders.</p>
									</div>
								<?php } else { ?>
									<div style="height:100px;" class="">
										<div class="marg-b10 red b " style="padding-left:30px;">Your Restaurant Menu is Set Up, and Pending Review.</div>
										
										<div class="ui-helper-clearfix marg-b5">
											<div class="left pad-r10"><?php echo $html->image('icons/22-preview-file.png'); ?></div>
											<div class="left" style="width:480px;">
												<?php echo $html->link('Review Your Online Menu', '/restaurants/edit_menu/'.$adloc['Location']['slug'], array('class'=>'b green')); ?> 
												for accuracy using the <?php echo $html->link('Menu Manager', '/restaurants/edit_menu/'.$adloc['Location']['slug'], array('class'=>'b green')); ?>, and make any changes if desired.
											</div>
										</div>
										
										<div class="ui-helper-clearfix">
											<div class="left pad-r10"><?php echo $html->image('icons/22-screen-checkbox.png'); ?></div>
											<div class="left" style="width:480px;">
												<?php echo $html->link('Verify Your Online Menu', '/locations/toggle_active/'.$adloc['Location']['id'], array('class'=>'b green')); ?> when you are ready to 
												<?php echo $html->link('Start Accepting Online Orders', '/locations/toggle_active/'.$adloc['Location']['id'], array('class'=>'b green')); ?>.
											</div>
										</div>	
									</div>
								<?php } ?>
							</div><?	
						} else {
							//menu not setup, either waiting for client to setup, or waiting for us to setup.
							//menu_active = (either 0, or 2)
							?>
							<div class="ui-helper-clearfix" style="height:220px;">
								<div class="left" style="width:350px;">
									<div class="b red marg-b5">Restaurant Menu is Currently Offline:</div>
									<?php if($adloc['Location']['menu_ready'] == '0') { ?>
										<p>This menu has not been set up. Get started by creating your Online Menu, using our 
										<?php echo $html->link('Menu Manager', '/locations/edit_menu/'.$adloc['Location']['slug']); ?>. When you are done, let us know 
										<?php echo $html->link('This Menu is Ready', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1'); ?>, by clicking the 
										appropriate button on the right.</p>
										<p>To have us to set up your Online Menu for you, send us your menu by email to: <?php echo $html->link('customer-service@orderaslice.com', 'mailto:customer-service@orderaslice.com'); ?>. 
										You may also fax it to us at (619) 639-0337. Once it has been sent, let us know by clicking 
										"<?php echo $html->link('We Sent Our Menu', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/2'); ?>".</p>
									<?php } else { ?>
										<div class="marg-b5">This menu has been submitted, and is <span class="b">Pending Setup</span> by our company. If you don't want to wait for 
										us to set up your Online Menu, you can expedite the entire process, by creating the Online Menu yourself, using our  
										<?php echo $html->link('Menu Manager', '/locations/edit_menu/'.$adloc['Location']['slug']); ?>.</div>
										<div>If you choose to create the Online Menu yourself, let us know when you are finished, by clicking 
										"<?php echo $html->link('This Menu is Ready', '/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1'); ?>".
										</div>
									<?php } ?>
								</div>
								<div class="right"><?php 
									echo $jquery->btn('/locations/edit_menu/'.$adloc['Location']['slug'], 'ui-icon-contact', 'Menu Manager');
									echo $jquery->btn('/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/1', 'ui-icon-circle-check', 'This Menu Is Ready...');
									if($adloc['Location']['menu_ready'] == '0') {
										echo $jquery->btn('/locations/toggle_menu_ready/'.$adloc['Location']['id'].'/2', 'ui-icon-lightbulb', 'We Sent Our Menu...');	
									}
								?></div>
							</div>
							<?		
						}
					} ?>
				</div><?php
			endforeach;
			?>
			</div>
			<?php
		} else {
			echo "Get started by ".$html->link('Setting Up A Location', '/locations/add').".";
		}
		?>
		</div>
	<?php 
}
?>
<?php //echo debug($orders); ?>
<?php //echo debug($ad_locations); ?>