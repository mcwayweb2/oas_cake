<div class="advertisers form"><hr />
<?php echo $form->create('Advertiser');?>
	<fieldset>
 		<legend><?php __('Edit '.$this->data['Advertiser']['name']);?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('contact_fname');
		echo $form->input('contact_lname');
		echo $form->input('contact_title');
		echo $form->input('contact_phone');
		echo $form->input('email', array('label' => 'Email'));
		echo $form->input('logo', array("label" => "Logo",
										"type"  => "file"));
		echo $form->input('active');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Advertiser.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Advertiser.id'))); ?></li>
		<li><?php echo $html->link(__('List Businesses', true), array('action'=>'index'));?></li>
	</ul>
</div>
