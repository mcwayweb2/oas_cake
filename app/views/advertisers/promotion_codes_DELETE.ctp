<?php echo $jquery->scrFormSubmit('#btnDiscountCodeAdd', '#formDiscountCodeAdd'); ?>
<script>
$(function() { 
	$('#ToggleAddFormContent').hide();
	
	$('#DiscountCodeExp').datepicker({
		minDate: new Date(),
		showOtherMonths: true
	});
	$('#btnToggleAddForm').toggle(function() {
		$('#ToggleAddFormContent').show();
		$('#btnToggleAddForm div.ui-icon-text').html('Hide Add Code Form');
	}, function() {
		$('#ToggleAddFormContent').hide();
		$('#btnToggleAddForm div.ui-icon-text').html('Add New Promotion Code');
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Promotion Code Manager</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('#', 'ui-icon-star', 'Add New Promotion Code', null, null, 'btnToggleAddForm'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Welcome to your promotion code manager. You can manage the existing promotion codes to any of your restaurants, as well as add new codes for use 
	with any/all of your restaurants.</p>
	
	<div id="ToggleAddFormContent">
		<?php echo $form->create('DiscountCode', array('action'=>'add', 'id'=>'formDiscountCodeAdd')); ?>
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">New Promotion Code Details</legend>
			<table class="form-table">
				<tr>
					<td class="label-req">Discount Code: <span class="required">*</span></td>
					<td><?php echo $form->input('code', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(Alphanumeric only please. 12 characters max.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Date:</td>
					<td><?php echo $form->input('exp', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'id'=>'DiscountCodeExp', 'type'=>'text'))?><br />
					<span class="base orange">(Leave empty for no expiration date.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Use Limit:</td>
					<td><?php echo $form->input('use_limit', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(Max times code can be used. Leave empty for no limit.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Minimum Order Amount:</td>
					<td><?php echo $form->input('min_order_amt', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For no minimum order amount leave empty.)</span></td>
				</tr>
				<?php $types = array('Whole' => 'Whole Amount', 'Percent' => 'Percentage'); ?>
				<tr>
					<td class="label-req">Discount Type: <span class="required">*</span></td>
					<td><?php echo $form->select('discount_type', $types, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
				</tr>
				<tr>
					<td class="label-req">Discount Amount: <span class="required">*</span></td>
					<td><?php echo $form->input('discount_amt', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For percentage amounts use a whole number. e.g. 27 = 27%)</span></td>
				</tr>
			</table>
		</fieldset>
		
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">Which restaurants are allowed to use this code?</legend>
			<p>Check the restaurants that can use this code. Only restaurant's that are subscribed to this service, are eligible to use promotion codes.</p>
			<div class="ui-corner-all ui-widget-content"> 
			<table class="list-table">
				<tr>
					<th colspan="2">Subscription Status</th>
					<th colspan="2">Restaurant Name</th>
				</tr>
			<?php 
			echo $this->element('table_accent_row', array('cols'=>'4'));
			foreach($locations as $loc):
				$disabled = ($loc['Location']['accept_discount_codes'] == '1') ? null : array('disabled'=>'disabled');
				?>
				<tr>
					<td style="width:120px;"><?php echo $html->image(($loc['Location']['accept_discount_codes'] == '1') ? 'btn-active-sm.png':'btn-inactive-sm.png'); ?></td>
					<td style="width:20px;"><?php echo $form->checkbox('locations.'.$loc['Location']['id'], $disabled); ?></td>
					<td class="orange"><?php echo $loc['Location']['name']; ?></td>
					<td style="width:160px;"><?php if($loc['Location']['accept_discount_codes'] != '1') echo $jquery->btn('/locations/subscription/'.$loc['Location']['id'], 'ui-icon-lightbulb', 'Activate This Service', null, null, null, '155'); ?></td>
				</tr>
				<?php 
			endforeach;	
			?>
			</table>
			</div>
		</fieldset>
		<?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Promotion Code', null, null, 'btnDiscountCodeAdd', 150); ?><hr class="orange"/>
	</div>
	
	<div class="ui-corner-all ui-widget-content marg-b10">
	<table class="list-table ui-table">
		<tr>
			<th>Code</th>
			<th>Expiration</th>
			<th>Use Limit</th>
			<th>Minimum Order Amt</th>
			<th>Discount Amt</th>
			<th></th>
		</tr>
		<?php 
		echo $this->element('table_accent_row', array('cols'=>'6'));

		foreach($discount_codes as $c):
			$class = ($c['DiscountCode']['active'] == 1) ? 'green' : 'red';
			$txt = ($c['DiscountCode']['active'] == 1) ? 'Active' : 'Inactive';
			?>
			<tr>
				<td><div class="marg-b5"><?php echo $c['DiscountCode']['code']; ?></div>
				<?php echo $html->image(($c['DiscountCode']['active'] == 1) ? 'btn-active-sm.png' : 'btn-inactive-sm.png'); ?></td>
				<td><?php echo ($c['DiscountCode']['exp'] == '0000-00-00') ? 'None' : $time->format('m-d-Y', $c['DiscountCode']['exp']); ?></td>
				<td><?php echo ($c['DiscountCode']['use_limit'] > 0) ? $c['DiscountCode']['times_used'].' of '.$c['DiscountCode']['use_limit'] : 'No Limit'; ?></td>
				<td><?php echo ($c['DiscountCode']['min_order_amt'] > 0) ? $number->currency($c['DiscountCode']['min_order_amt']) : 'No Minimum'; ?></td>
				<td><?php echo ($c['DiscountCode']['discount_type'] == 'Percent') ? ($c['DiscountCode']['discount_amt'] * 100).'%' : $number->currency($c['DiscountCode']['discount_amt']); ?></td>
				<td style="width:85px;"><?php 
					$disable_txt = ($c['DiscountCode']['active'] == 1) ? "Disable" : "Activate";
					echo $jquery->btn('/discount_codes/disable/'.$c['DiscountCode']['id'], 'ui-icon-power', $disable_txt, 'Are you sure you would like to '.$disable_txt.' this promotion code in the system.', null, null, 80);
					echo $jquery->btn('/discount_codes/manage/'.$c['DiscountCode']['id'], 'ui-icon-wrench', 'Manage', null, null, null, 80);
					echo $jquery->btn('/discount_codes/archive/'.$c['DiscountCode']['id'], 'ui-icon-circle-close', 'Remove', 'Are you sure you would like to remove this promotion code in the system.', null, null, 80);
				?></td>
			</tr>
			<?php 
		endforeach;
		
		?>
	</table>
	</div>
	<?php echo $this->element('pagination_links'); ?>
</div>
<?php echo debug($discount_codes); ?>