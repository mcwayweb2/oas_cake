 
 
 <div>
 	<h1>Manage Payment Profiles</h1>
	<hr />
	<?
	if(sizeof($profiles) > 0) {
		?>
		<table cellspacing="0" width="100%">
			<tr>
				<th>Profile Name</th>
				<th>Card Suffix</th>
				<th>Add Date</th>
				<th>Default Card</th>
				<th>Refund Card</th>
				<th>Actions<th>
			</tr>
			<?
			$i = 0;
			foreach($profiles as $profile):
				$class = ($i % 2 == 0) ? "class='alt-row'" : "";
				?>
				<tr>
					<td <?=$class?>><?=$profile['AdvertisersPaymentProfile']['name'] ?></td>
					<td <?=$class?>><?=$profile['AdvertisersPaymentProfile']['card_suffix']?></td>
					<td <?=$class?>><?=date("m-d-Y", strtotime($profile['AdvertisersPaymentProfile']['timestamp']))?></td>
					<td <?=$class?>><?
						if($profile['AdvertisersPaymentProfile']['default'] == 0) {
							echo $html->link('Mark Default', '/');
						} else { echo "Yes"; } ?></td>
					<td <?=$class?>><?
						if($profile['AdvertisersPaymentProfile']['refund'] == 0) {
							echo $html->link('Mark Default', '/');
						} else { echo "Yes"; } ?></td>
					<td <?=$class?>><?=$html->link('Remove', '/advertisers_payment_profiles/remove/'.$profile['AdvertisersPaymentProfile']['id'], array(), 'Are you sure you would like to delete this payment profile?' ) ?></td>
				</tr>
				<?
				$i++;
			endforeach;
			?>
		</table>
		<?
	} else {
		?><h2 class="error">You currently have no payment profiles set up.</h2><?
		echo $html->link('Add Payment Profile', '/advertisers_payment_profiles/add');
	}
	?>
	
	
 	<?php //echo debug($profiles); ?>
 
 
 </div>