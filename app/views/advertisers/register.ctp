<?php echo $jquery->scrFormSubmit('#btnAdvertiserRegister', '#formAdvertiserRegister'); ?>
<script>
$(function() {
	$('#AdvertiserLogo').uniform();

	$('.ajax-loader-sm-circle').hide();
	$('#AdvertiserUsername').blur(function() {
		$('.ajax-loader-sm-circle').show();
		 
		$('#ajaxAdvertiserUsernameContent').load('/advertisers/ajax_check_username', { username: $(this).val() }, function() {
			$('.ajax-loader-sm-circle').hide();
		});
		return false;
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<h1>Restaurant Merchant Account Registration</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Welcome to Order A Slice. Your restaurant is minutes away from your having its very own sophisticated Online Ordering System. 
	Accept Credit Card transactions from customers with ease. Make deliveries easy for your staff, with our delivery maps feature. 
	Instantly generate turn by turn directions, complete with a map, for any order. These are just a few of the many advantages you will enjoy 
	with your Order A Slice Account. Please fill out the form below to get started.</p> 
	<?php echo $form->create('Advertiser', array("type"=>"file", "action"=>"register", 'id'=>'formAdvertiserRegister')); ?>
	<fieldset class="marg-b10">
 		<legend><?php __('Business Info');?></legend>
 		<table width="100%" class="form-table">
 			<tr>
 				<td width="30%" class="label-req">Business Name: <span class="required">*</span></td>
 				<td><?=$form->input('name', array('label' => '', 'class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Logo: </td>
 				<td><?=$form->input('logo', array("label" => "",'class'=>'form-text', "type"  => "file", 'id'=>'AdvertiserLogo'))?></td>
 			</tr>
 			
 		</table>
	</fieldset>
		
	<fieldset class="marg-b10">
		<legend><?php __('Contact Info');?></legend>
		<table width="100%" class="form-table">
 			<tr>
 				<td width="30%" class="label-req">First Name: <span class="required">*</span></td>
 				<td><?=$form->input('contact_fname', array('label' =>'', 'class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Last Name: <span class="required">*</span></td>
 				<td><?=$form->input('contact_lname', array('label' => '', 'class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Title: <span class="required">*</span></td>
 				<td><?=$form->input('contact_title', array('label' => '', 'class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Phone: <span class="required">*</span></td>
 				<td><?=$form->input('contact_phone', array('label' => '', 'class'=>'form-text', 'div'=>false))?><br />
 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Email: <span class="required">*</span></td>
 				<td><?=$form->input('email', array('label' => '', 'class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Desired Username: <span class="required">*</span></td>
 				<td>
 					<div class="ui-helper-clearfix">
 						<div class="left"><?=$form->input('username', array('class'=>'form-text','label'=>''));?></div>
 						<div class="left ajax-loader-sm-circle">
 							<?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?>
 						</div>
 					</div>
 					<div id="ajaxAdvertiserUsernameContent"></div>
 				</td>
 			</tr>
 			<tr>
 				<td class="label-req">Password: <span class="required">*</span></td>
 				<td><?=$form->input('Pass.pass1', array('label' => '','class'=>'form-text',
										      'type'  => 'password'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Confirm Password: <span class="required">*</span></td>
 				<td><?=$form->input('Pass.pass2', array('label' => '','class'=>'form-text',
										      'type'  => 'password'))?></td>
 			</tr>
 		</table>
	</fieldset>
	<!-- 
	<p>If your company has more than one location, use the following settings to manage your multiple restaurant menus with ease. Making settings universal, allows you to manage all your locations simultaneously.If you change something related to that setting in any of your restaurants' Menu Manager, the saved changes will be made for all your other locations automatically.</p>
	<fieldset>
		<legend>Settings</legend>
		<table width="80%">
 			<tr>
 				<td width="80%" class="label-req" style="padding-right:20px;">Do all your locations offer the same toppings:</td>
 				<td><?php //echo $form->radio('universal_toppings', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>', 'value'=>'1'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req" style="padding-right:20px;">Do all your locations offer the same pizza sizes:</td>
 				<td><?php //echo $form->radio('universal_sizes', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>', 'value'=>'1'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req" style="padding-right:20px;">Do all your locations offer the same specialty pizzas:</td>
 				<td><?php //echo $form->radio('universal_specials', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>', 'value'=>'1'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req" style="padding-right:20px;">Do all your locations offer the same menu categories & menu items:</td>
 				<td><?php //echo $form->radio('universal_items', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>', 'value'=>'1'))?></td>
 			</tr>
 		</table>	
	</fieldset>
	 -->
		 
	<div class="ui-helper-clearfix">
 		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-pencil', 'Register', null, null, 'btnAdvertiserRegister', '90'); ?></div>
 	</div>
 	</form>
</div>