		<h1 style="font-size:17px;">The Restaurant Industries Premier Online Pizza Ordering Solution!</h1>
		<h2>Allow your Customers to Order Food Online Quickly and Easily...</h2>
		
		<p>Is your <b>pizza delivery restaurant</b> tired of losing business to big franchise chains that allow their customers to <b>order food online</b>? Now you don't have to! 
		Online Ordering is the wave of the future for the Restaurant Industry. Consumers love to <b>order pizza online</b>: it's fast, convenient, and 
		they are never put on hold. Business's love <b>online food ordering</b> because it expands their potential customer base exponentially; you reach new 
		customers that your normal local advertising campaign would not reach. Compete with Larger Businesses, Gain New Customers, and Expand your Market, 
		with a <span class="b">Premier Order a Slice Account!</span></p>
		
        <ul class="list-green-check ui-helper-reset ui-state-default ui-corner-all" >
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left red">No Contract, Investment, or Risk! If We Don't Send you Extra Business, you Don't Pay a Dime!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Free Advertising and Exposure to Hundreds of Thousands of Potential New Clients!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Generate Instant Turn by Turn Delivery Directions for your Drivers!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Use Discount and Promotion Codes to Promote More Orders!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Maximize Productivity by Freeing your Employees from Having to take Phone Orders!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Increase Order Accuracy, Reduce Guest Complaints, and Improve your Customers' Experience!</div>
            </li>
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Streamline your Customer Contact Information, Accessible Anywhere in the World!</div>
            </li>
        </ul>
		
		<div class=" marg-b10 marg-t10 pad-l10 b" style="font-size:13px;">Learn More About an Order a Slice Account and the <?php echo $html->link('Online Pizza Ordering', '/learn-more')?> Process!</div>
		<p><?=$html->link($html->image("btn-signup.gif", array('class'=>'valign=m')),
						  array('action'=>'register', 'controller'=>'advertisers'),
						  array('escape'=>false))?></p>