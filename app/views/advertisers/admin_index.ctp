<?php
	$html->addCrumb('Advertisers', '/admin/advertisers/index');
?>
<div class="advertisers index">
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Advertiser', true), array('action'=>'add')); ?></li>
	</ul>
</div>
<h2><?php __('Manage Advertisers');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th>Contact</th>
	<th><?php echo $paginator->sort('active');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($advertisers as $advertiser):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td style="width:70px; padding:5px;">
			<?=$html->image('/files/logos/'.$advertiser['Advertiser']['logo'], array("style" => "width:50px;vertical-align:top;"))?>
		</td>
		<td>
			<?=$html->link($advertiser['Advertiser']['name'], '/admin/locations/index/'.$advertiser['Advertiser']['id'])?>
		</td>
		<td class="smalltext">
			<?=$advertiser['Advertiser']['contact_fname']." ".$advertiser['Advertiser']['contact_lname'].", "?>
			<?=$advertiser['Advertiser']['contact_title']."<br />".$advertiser['Advertiser']['contact_phone']?>
		</td>
		
		<td>
			<?php
			$status = (($advertiser['Advertiser']['active'] == '1') ? "<?php style='color:green;'>ACTIVE</b>" : "<?php style='color:red;'>INACTIVE</b>");
			$status_text = (($advertiser['Advertiser']['active'] == '1') ? "Disable" : "Enable");
			echo $status;
			?>
		</td>
		<td class="actions" style="width:220px;">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $advertiser['Advertiser']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $advertiser['Advertiser']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $advertiser['Advertiser']['id'])); ?>
			<?php echo $html->link(__($status_text, true), array('action'=>'disable', $advertiser['Advertiser']['id'])); ?><br />
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<?php '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

