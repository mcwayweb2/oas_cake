<?php echo $jquery->scrFormSubmit('#btnAdvertiserUpdate', '#formAdvertiserUpdate'); ?>
<script>
$(function() {
	$('#AdvertiserLogo').uniform();
	$('#AdvertiserEditTabs').tabs();
});
</script>

<?php //echo debug($this->data); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<h1 class="pad-l5">Update Account Settings</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Advertiser', array("type"=>"file", "action"=>"edit", 'id'=>'formAdvertiserUpdate')); ?>
	<?php echo $form->input('id'); ?>
	
	<div id="AdvertiserEditTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#acct-profile">Account Profile</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#banner-image">Banner Image</a></li>
		</ul>
		<div id="acct-profile" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<fieldset class="marg-b10">
		 		<legend><?php __('Business Info');?></legend>
		 		<table width="100%" class="form-table">
		 			<tr>
		 				<td width="30%" class="label-req">Business Name: <span class="required">*</span></td>
		 				<td><?=$form->input('name', array('label' => '', 'class'=>'form-text'))?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Logo: </td>
		 				<td><?=$form->input('logo', array("label" => "",'class'=>'form-text', "type"  => "file", 'id'=>'AdvertiserLogo'))?></td>
		 			</tr>
		 			
		 		</table>
			</fieldset>
					
			<fieldset>
				<legend><?php __('Contact Info');?></legend>
				<table width="100%" class="form-table">
		 			<tr>
		 				<td width="30%" class="label-req">First Name: <span class="required">*</span></td>
		 				<td><?=$form->input('contact_fname', array('label' =>'', 'class'=>'form-text'))?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Last Name: <span class="required">*</span></td>
		 				<td><?=$form->input('contact_lname', array('label' => '', 'class'=>'form-text'))?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Title: <span class="required">*</span></td>
		 				<td><?=$form->input('contact_title', array('label' => '', 'class'=>'form-text'))?></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Phone: <span class="required">*</span></td>
		 				<td><?=$form->input('contact_phone', array('label'=>'','class'=>'form-text', 'div'=>false))?><br />
		 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
		 			</tr>
		 			<tr>
		 				<td class="label-req">Email: <span class="required">*</span></td>
		 				<td><?=$form->input('email', array('label' => '', 'class'=>'form-text'))?></td>
		 			</tr>
		 		</table>
			</fieldset>
					
			<!-- 
			<p>If your company has more than one location, use the following settings to manage your multiple restaurant menus with ease. Making settings universal, allows you to manage all your locations simultaneously.If you change something related to that setting in any of your restaurants' Menu Manager, the saved changes will be made for all your other locations automatically.</p>
			<fieldset>
				<legend>Settings</legend>
				<table width="80%">
			 		<tr>
			 			<td width="80%" class="label-req" style="padding-right:20px;">Do all your locations offer the same toppings:</td>
			 			<td><?php //echo $form->radio('universal_toppings', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>'))?></td>
			 		</tr>
			 		<tr>
			 			<td class="label-req" style="padding-right:20px;">Do all your locations offer the same pizza sizes:</td>
			 			<td><?php //echo $form->radio('universal_sizes', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>'))?></td>
			 		</tr>
			 		<tr>
			 			<td class="label-req" style="padding-right:20px;">Do all your locations offer the same specialty pizzas:</td>
			 			<td><?php //echo $form->radio('universal_specials', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>'))?></td>
			 		</tr>
			 		<tr>
			 			<td class="label-req" style="padding-right:20px;">Do all your locations offer the same menu categories & menu items:</td>
			 			<td><?php //echo $form->radio('universal_items', array('1'=>'Yes','0'=>'No'), array('legend'=>false, 'separator'=>'</td><td>'))?></td>
			 		</tr>
			 	</table>	
			</fieldset><br />
			 -->
			 
			<fieldset class="marg-t10">
			 	<legend><?php __('Update Password');?></legend>
			 	<?php if($this->data['Advertiser']['is_password_changed'] == '0') { ?>
				 	<div class="b red marg-b10 txt-c">Please update your password from the default one that was provided with your account!</div>
				 	<script>
				 	$(function() {
				 		$('#PassPass1, #PassPass2').addClass('ui-state-error');
					});
				 	</script>
			 	<?php } ?>
			 	<table class="form-table">
			 		<tr>
			 			<td width="30%" class="label-req">New Password:</td>
			 			<td><?=$form->input('Pass.pass1', array('label'=>'','class'=>'form-text', 'type'=>'password', 'value'=>''))?></td>
			 		</tr>
			 		<tr>
			 			<td class="label-req">Confirm Password:</td>
			 			<td><?=$form->input('Pass.pass2', array('label'=>'','class'=>'form-text', 'type'=>'password', 'value'=>''))?></td>
			 		</tr>
			 	</table>
			</fieldset>
		
		</div>
		<div id="banner-image" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<input name="data[Advertiser][banner_id]" id="LocationBannerId_" value="" type="hidden">
	 		<div class="ui-helper-clearfix">
	 			<?php foreach($banners as $id => $filename): ?>
	 			<?php $checked = ($this->data['Advertiser']['banner_id'] == $id) ? 'checked="checked"' : ''; ?>
	 			<div class="left marg-b10" style="width:270px;">
	 				<div class="ui-helper-clearfix">
	 					<div class="left"><input name="data[Advertiser][banner_id]" <?php echo $checked; ?> id="AdvertiserBannerId<?php echo $id; ?>" value="<?php echo $id; ?>" type="radio"></div>
	 					<div class="left"><?php echo $html->image('banners/'.$filename, array('style'=>'width:240px;', 'class'=>'orange-border pad2'))?></div>
	 				</div>
	 			</div>
	 			<?php endforeach; ?>
	 		</div>
		
			<hr />
			<div class="ui-helper-clearfix">
				<div class="left" style="width:450px;" style="">
					<div class="b marg-b5">Would you like to use your company logo in the banner?</div>
					<div class="txt-r"><span class="b green">Yes</span>, try using my logo in the banner:</div>
					<div class="txt-r"><span class="b green">No</span>, I prefer text:</div>
				</div>
				<div class="left txt-c" style="width:70px; margin-top:18px;">
					<input type="radio" id="AdvertiserUseLogoInBanner1" name="data[Advertiser][use_logo_in_banner]" value="1" 
					<?php if($this->data['Advertiser']['use_logo_in_banner'] == '1') echo 'checked="checked"'; ?> /><br />
					<input type="radio" id="AdvertiserUseLogoInBanner0" name="data[Advertiser][use_logo_in_banner]" value="0" 
					<?php if($this->data['Advertiser']['use_logo_in_banner'] == '0') echo 'checked="checked"'; ?> />
				</div>
			</div>
		</div>
	</div>

	<div class="ui-helper-clearfix">
		<div class="left"></div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-gear', 'Update Settings', null, 'txt-r', 'btnAdvertiserUpdate', '130'); ?></div>	
	</div>
	</form>
</div>