<?php echo $jquery->scrFormSubmit('#btnDiscountCodeAdd', '#formDiscountCodeAdd'); ?>
<script>
$(function() { 
	$('.ajax-loader-sm-circle').hide();
	<?php if(!$show_form) { ?>$('#ToggleAddFormContent').hide();<?php } ?>
	
	$('#DiscountCodeExp').datepicker({
		minDate: new Date(),
		showOtherMonths: true,
		dateFormat: "yy-mm-dd"
	});
	
	$('#btnToggleAddForm').toggle(function() {
		$('#ToggleAddFormContent').show();
		$('#btnToggleAddForm div.ui-icon-text').html('Hide New Code Form');
	}, function() {
		$('#ToggleAddFormContent').hide();
		$('#btnToggleAddForm div.ui-icon-text').html('Add New Promotion Code');
	});

	$('#DiscountCodeCode').blur(function() {
		$('.ajax-loader-sm-circle').show();
		 
		$('#ajaxDiscountCodeContent').load('/discount_codes/ajax_check_code', { code: $(this).val() }, function() {
			$('.ajax-loader-sm-circle').hide();
		});
		return false;
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Promotion Code Manager</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('#', 'ui-icon-star', 'Add New Promotion Code', null, null, 'btnToggleAddForm'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Welcome to your promotion code manager. You can manage the existing promotion codes to any of your restaurants, as well as add new codes for use 
	with any/all of your restaurants.</p>
	<!-- 
	<p><span class="b pad-r5">Please Note:</span>This feature is only available to restaurants that have upgraded to a 
	<?php //echo $html->link('Premium Membership Subscription', '/restaurants/upgrade', array('class'=>'b')); ?>. You may add or remove promotion codes at any time, 
	however you will only be able to assign them to restaurants with a <?php //echo $html->link('Premium Membership Subscription', '/restaurants/upgrade', array('class'=>'b')); ?>.</p>
	 -->
	
	<div id="ToggleAddFormContent">
		<?php echo $form->create('DiscountCode', array('action'=>'manage', 'id'=>'formDiscountCodeAdd')); ?>
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">New Promotion Code Details</legend>
			<table class="form-table">
				<tr>
	 				<td class="label-req">Discount Code: <span class="required">*</span></td>
	 				<td>
	 					<div class="ui-helper-clearfix">
	 						<div class="left"><?=$form->input('code', array('class'=>'form-text','label'=>'', 'div'=>false));?></div>
	 						<div class="left ajax-loader-sm-circle">
	 							<?php echo $html->image('ajax-loader-sm-circle.gif', array('style'=>'width:25px;')); ?>
	 						</div>
	 					</div>
	 					<div id="ajaxDiscountCodeContent"></div>
					</td>						
	 			</tr>
				<tr>
					<td class="label-req">Expiration Date:</td>
					<td><?php echo $form->input('exp', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'id'=>'DiscountCodeExp', 'type'=>'text'))?><br />
					<span class="base orange">(Leave empty for no expiration date.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Use Limit:</td>
					<td><?php echo $form->input('use_limit', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(Max times code can be used. Leave empty for no limit.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Minimum Order Amount:</td>
					<td><?php echo $form->input('min_order_amt', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For no minimum order amount leave empty.)</span></td>
				</tr>
				<?php $types = array('Whole' => 'Whole Amount', 'Percent' => 'Percentage'); ?>
				<tr>
					<td class="label-req">Discount Type: <span class="required">*</span></td>
					<td><?php echo $form->select('discount_type', $types, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
				</tr>
				<tr>
					<td class="label-req">Discount Amount: <span class="required">*</span></td>
					<td><?php echo $form->input('discount_amt_unconverted', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For percentage amounts use a whole number. e.g. 27 = 27%)</span></td>
				</tr>
			</table>
		</fieldset>
		
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">Which restaurants are allowed to use this code?</legend>
			<p>Check the boxes next to the restaurants that this promotion code can be used at. Only restaurants with a current <span class="b italic">Premium Membership Subscription"</span> 
			are eligible to use promotion codes.</p>
			<div class="ui-corner-all ui-widget-content"> 
			<table class="list-table">
				<tr><th></th>
					<th>Restaurant Name</th>
					<th>Menu Status</th>
					<th style="width:210px;">Account Type</th>
				</tr>
			<?php 
			echo $this->element('table_accent_row', array('cols'=>'4'));
			foreach($locations as $loc):
				$disabled = ($loc['Location']['acct_type'] == '1') ? array('disabled'=>'disabled') : array('checked'=>'checked');
				?>
				<tr>
					<td style="width:20px;"><?php echo $form->checkbox('locations.'.$loc['Location']['id'], $disabled); ?></td>
					<td class="orange"><?php echo $loc['Location']['name']; ?></td>
					<td>
						<?php if($loc['Location']['menu_ready'] == '1' && $loc['Location']['active'] == '1') { ?>
							<span class="b green">Menu Online.</span>
						<?php } else { ?>
							<span class="b red">Menu Offline.</span>
						<?php } ?>
					</td>
					<td style="width:225px;"><?php 
						switch($loc['Location']['acct_type']) {
							case '1':
								echo $jquery->btn('/restaurants/upgrade', 'ui-icon-arrowreturnthick-1-n', 'Upgrade to Premium Membership', null, null, null, '225');
								break;
							case '2':
								echo "Premium Membership";
								break;
							case '3':
								echo "Licensed Membership";
								break;
						}
					?></td>
				</tr>
				<?php 
			endforeach;	
			?>
			</table>
			</div>
		</fieldset>
		
		<div class="ui-helper-clearfix">
			<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Promotion Code', null, null, 'btnDiscountCodeAdd', 150); ?></div>
		</div>
		<?php echo $form->end(); ?>
		
		<hr class="orange marg-b20 marg-t20"/>
	</div>
	
	<div class="ui-corner-all ui-widget-content marg-b10">
	<table class="list-table ui-table">
		<tr>
			<th>Code</th>
			<th>Restrictions</th>
			<th>Discount</th>
			<th></th>
		</tr>
		<?php 
		echo $this->element('table_accent_row', array('cols'=>'4'));

		if(sizeof($discount_codes) > 0) {
			foreach($discount_codes as $c):
				$class = ($c['DiscountCode']['active'] == 1) ? 'green' : 'red';
				$txt = ($c['DiscountCode']['active'] == 1) ? 'Active' : 'Inactive';
				?>
				<tr>
					<td>
						<div class="pad-l5">	
							<div class="marg-b5"><?php echo $c['DiscountCode']['code']; ?></div>
							<?php echo $html->image(($c['DiscountCode']['active'] == 1) ? 'btn-active-sm.png' : 'btn-inactive-sm.png'); ?>
						</div>
					</td>
					<td>
						<div class="ui-helper-clearfix base">
							<div class="left b" style="width:140px;">Expiration Date:</div>
							<div class="left" style="font-weight:normal;"><?php echo ($c['DiscountCode']['exp'] == '0000-00-00') ? 'None' : $time->format('m-d-Y', $c['DiscountCode']['exp']); ?></div>
						</div>
						<div class="ui-helper-clearfix base">
							<div class="left b" style="width:140px;">Use Limit:</div>
							<div class="left" style="font-weight:normal;"><?php echo ($c['DiscountCode']['use_limit'] > 0) ? $c['DiscountCode']['times_used'].' of '.$c['DiscountCode']['use_limit'].' Uses' : 'No Limit'; ?></div>
						</div>
						<div class="ui-helper-clearfix base">
							<div class="left b" style="width:140px;">Minimum Order Amount:</div>
							<div class="left" style="font-weight:normal;"><?php echo ($c['DiscountCode']['min_order_amt'] > 0) ? $number->currency($c['DiscountCode']['min_order_amt']) : 'No Minimum'; ?></div>
						</div>
					</td>
					<td><?php echo ($c['DiscountCode']['discount_type'] == 'Percent') ? ($c['DiscountCode']['discount_amt'] * 100).'%' : $number->currency($c['DiscountCode']['discount_amt']); ?></td>
					
					<td style="width:85px;"><?php 
						$disable_txt = ($c['DiscountCode']['active'] == 1) ? "Disable" : "Activate";
						echo $jquery->btn('/discount_codes/disable/'.$c['DiscountCode']['id'], 'ui-icon-power', $disable_txt, 'Are you sure you would like to '.$disable_txt.' this promotion code in the system.', null, null, 80);
						echo $jquery->btn('/discount_codes/edit/'.$c['DiscountCode']['id'], 'ui-icon-wrench', 'Manage', null, null, null, 80);
						echo $jquery->btn('/discount_codes/archive/'.$c['DiscountCode']['id'], 'ui-icon-circle-close', 'Remove', 'Are you sure you would like to remove this promotion code in the system.', null, null, 80);
					?></td>
				</tr>
				<?php 
			endforeach;	
		} else {
			?><tr><td colspan="3" class="b red txt-c">You currently have no Promotion Codes setup.</td><?php 
		}
		?>
	</table>
	</div>
	<?php echo $this->element('pagination_links'); ?>
</div>