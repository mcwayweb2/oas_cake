<?php echo $jquery->scrFormSubmit('#btnDiscountCodeEdit', '#formDiscountCodeEdit'); ?>
<script>
$(function() { 
	$('#DiscountCodeExp').datepicker({
		minDate: new Date(),
		showOtherMonths: true,
		dateFormat: "yy-mm-dd"
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Promotion Code Manager</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/discount_codes/manage', 'ui-icon-circle-arrow-w', 'Back to Promotion Code Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<div id="ToggleAddFormContent">
		<?php echo $form->create('DiscountCode', array('action'=>'edit', 'id'=>'formDiscountCodeEdit')); ?>
		<?php echo $form->input('id'); ?>
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">New Promotion Code Details</legend>
			<table class="form-table">
				<tr>
					<td class="label-req">Discount Code: <span class="required">*</span></td>
					<td><?php echo $form->input('code', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(Alphanumeric only please. 12 characters max.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Date:</td>
					<td><?php echo $form->input('exp', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'id'=>'DiscountCodeExp', 'type'=>'text'))?><br />
					<span class="base orange">(Leave empty for no expiration date.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Use Limit:</td>
					<td><?php echo $form->input('use_limit', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(Max times code can be used. Leave empty for no limit.)</span></td>
				</tr>
				<tr>
					<td class="label-req">Minimum Order Amount:</td>
					<td><?php echo $form->input('min_order_amt', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For no minimum order amount leave empty.)</span></td>
				</tr>
				<?php $types = array('Whole' => 'Whole Amount', 'Percent' => 'Percentage'); ?>
				<tr>
					<td class="label-req">Discount Type: <span class="required">*</span></td>
					<td><?php echo $form->select('discount_type', $types, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
				</tr>
				<tr>
					<td class="label-req">Discount Amount: <span class="required">*</span></td>
					<td><?php echo $form->input('discount_amt_unconverted', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
					<span class="base orange">(For percentage amounts use a whole number. e.g. 27 = 27%)</span></td>
				</tr>
			</table>
		</fieldset>
		
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">Which restaurants are allowed to use this code?</legend>
			<p>Check the boxes next to the restaurants that this promotion code can be used at. Only restaurants with a current <span class="b italic">Premium Membership Subscription"</span> 
			are eligible to use promotion codes.</p>
			<div class="ui-corner-all ui-widget-content"> 
			<table class="list-table">
				<tr><th></th>
					<th>Restaurant Name</th>
					<th>Menu Status</th>
					<th style="">Account Type</th>
				</tr>
			<?php 
			echo $this->element('table_accent_row', array('cols'=>'4'));
			foreach($locations as $loc):
				$opts = ($loc['Location']['acct_type'] == '1') ? array('disabled'=>'disabled') : array();
				if(in_array($loc['Location']['id'], $current_locs)) $opts['checked'] = 'checked';
				?>
				<tr>
					<td style="width:20px;"><?php echo $form->checkbox('locations.'.$loc['Location']['id'], $opts); ?></td>
					<td class="orange"><?php echo $loc['Location']['name']; ?></td>
					<td>
						<?php if($loc['Location']['menu_ready'] == '1' && $loc['Location']['active'] == '1') { ?>
							<span class="b green">Menu Online.</span>
						<?php } else { ?>
							<span class="b red">Menu Offline.</span>
						<?php } ?>
					</td>
					<td style="width:225px;"><?php 
						switch($loc['Location']['acct_type']) {
							case '1':
								echo $jquery->btn('/restaurants/upgrade', 'ui-icon-arrowreturnthick-1-n', 'Upgrade to Premium Membership', null, null, null, '225');
								break;
							case '2':
								echo "Premium Membership";
								break;
							case '3':
								echo "Licensed Membership";
								break;
						}
					?></td>
				</tr>
				<?php 
			endforeach;	
			?>
			</table>
			</div>
		</fieldset>
		
		<div class="ui-helper-clearfix">
			<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Promotion Code', null, null, 'btnDiscountCodeEdit', 170); ?></div>
		</div>
	</div>
</div>