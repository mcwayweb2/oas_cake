<script>
$(function() { 
	$('#ajaxAddSpecificToppings').hide();
	$('select').uniform();

	$('#btnAddChooseSpecificToppings').toggle(function() { 
		//show edit pic form, change link text
		$('#ajaxAddSpecificToppings').show();
		$('#btnAddChooseSpecificToppings div.ui-icon-text').text('Hide Toppings Form');
		return false;	
	}, function() { 
		//remove pic form, revert link text
		$('#ajaxAddSpecificToppings').hide();
		$('#btnAddChooseSpecificToppings div.ui-icon-text').text('Choose Specific Toppings');
		return false;
	});

	/* Code to show an ajax loader, in place of submit button, after submitting form */
	/*
	$('.btnComboBuilderAddCustomPizza').click(function() {
		$(this).ne
		$('#formComboBuilderAddCustomPizza').submit();
	});
	*/
});
</script>
<?php echo $jquery->scrFormSubmit('.btnComboBuilderAddCustomPizza', '#formComboBuilderAddCustomPizza'); ?>



<div class="ui-widget-content ui-corner-all marg-b10">
<fieldset class="ui-widget-content ui-corner-all  marg-b10"><legend class="ui-corner-all">Add a Custom Pizza to your Combo...</legend>
	<p>Use this form to add a custom pizza to your current Combo. Choose the size of the pizza to add, as well as the maximum number of toppings 
	the customer is allowed to choose for the pizza when they order your Combo. You may also set the exact toppings that the pizza comes with, by 
	leaving the number of Customer Selectable toppings at zero, then manually adding the exact toppings the pizza is offered with.</p>
	<hr />
	
	<div class="ui-helper-clearfix">
		<div class="left"></div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-gear', 'Choose Specific Toppings', null, null, 'btnAddChooseSpecificToppings')?></div>
	</div>
	
	<?php echo $form->create('CombosPizza', array('action'=>'add', 'id'=>'formComboBuilderAddCustomPizza')); ?>
	<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
	<table class="form-table">
		<tr>
			<td class="label-req" style="width:200px;">Choose Pizza Size:</td>
			<td><?=$form->select('size_id', $sizes, null, array('class'=>'form-text'), false)?></td>
		</tr>
		<tr>
			<td class="label-req">Customer Selectable Toppings:</td>
			<td><?=$form->select('allowed_toppings', $misc->qtys($zerofill = true), null, array('class'=>'form-text'), false)?></td>
		</tr>
		<tr>
			<td class="label-req">How many of this pizza are you adding?</td>
			<td><?=$form->select('qty', $misc->qtys(), null, array('class'=>'form-text'), false)?></td>
		</tr>
		<tr><td></td>
			<td>
				<?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Custom Pizza', null, 'btnComboBuilderAddCustomPizza', null, '140')?>
			</td>
		</tr>
	</table>
	
	<div id="ajaxAddSpecificToppings" class="ui-widget-content ui-corner-all marg-t10">
		<?php echo $this->element('advertisers/toppings', array('toppings' => $toppings, 'no_fieldset' => true)); ?>
		
		<div class="ui-helper-clearfix">
			<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Custom Pizza', null, 'btnComboBuilderAddCustomPizza', null, '140'); ?></div>
		</div>
	</div>
	</form>
</fieldset>
</div>