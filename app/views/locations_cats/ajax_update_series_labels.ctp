<hr />

<?php for($x = 1; $x <= $qty; $x++) { ?>
<div class="ui-helper-clearfix marg-b5">
	<div class="left" style="padding-right:50px;">
		<span class="b base">Option Label <?php echo $x; ?>:</span><br />
		<?php echo $form->input('labels.'.$x, array('class'=>'form-text ui-corner-all ui-state-default', 'div' => false, 'label' => false)); ?>
	</div>
	<div class="left">
		<span class="b base">Additional Price <?php echo $x; ?>:</span><br />
		<?php echo $form->input('prices.'.$x, array('class'=>'form-text ui-corner-all ui-state-default', 'div' => false, 'label' => false)); ?><br />
		<span class="b base orange">(Leave empty for no additional charge.)</span>
	</div>
</div>	
<?php } ?>