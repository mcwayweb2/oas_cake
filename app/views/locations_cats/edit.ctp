<script>
	$(function() { 
		$('#progressSeriesLimit, #divAddNewSeries').hide();

		$('#LocationsCatNewSeriesQty').change(function() {
			$('#progressSeriesLimit').show(); 
			/* update choice limit drop down with new number of max choices */
			$('#LocationsCatNewSeriesLimit').load('/locations_cats/ajax_update_series_limit', { qty: $(this).val() }, function() {
				$('#uniform-LocationsCatNewSeriesLimit span:first').text('1');
			});
			/* update div with appropriate number of labels */
			$('#divChoiceSeriesLabels').load('/locations_cats/ajax_update_series_labels', { qty: $(this).val() }, function() {
				$('#progressSeriesLimit').hide();
			});
		});

		$('#linkAddNewSeries').toggle(function() { 
			$('#divAddNewSeries').show();
			$(this).text('Hide New Selection Series Form...');
		}, function() {
			$('#divAddNewSeries').hide();
			$(this).text('Add a New Selection Series...');
		});

		<?php if(empty($this->data['LocationsCat']['time_open'])) { ?>
			$('#availTimes').hide();
		<?php } ?>
		
		$('#LocationsCatAvailAllDay').change(function() {
			if($(this).attr('checked') === false) { $('#availTimes').slideDown('fast'); } 
			else { $('#availTimes').hide("fold"); }
		});
		
		$('.btnMenuEditCat').click(function() {
			$('#formMenuEditCat').submit();
			return false;
		});
	});
</script>
<?php //echo $this->element('js/jquery/add_form_styles'); ?>
<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Update Menu Category</legend>
	<p>To make this Menu Category only available during a specific portion of the day, (i.e. A Lunch Only Category), uncheck the appropriate 
	box, and fill out starting & ending times for when this category will be available to order from. Leave the box checked to make the 
	Menu Category available all day.</p>
	<p>When you are finished, press the <span class="italic b">"Update Menu Category"</span> button to save the Menu Item.</p>

	<?php echo $form->create('LocationsCat', array('id'=>'formMenuEditCat', 'action'=>'edit')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$this->data['LocationsCat']['location_id'])); ?>
	<?php echo $form->input('id'); ?>
	<table class="form-table">
		<tr>
			<td class="label-req" style="width:150px;">Category: <span class="required">*</span></td>
			<td><?php echo $form->select('cat_id', $cat_list, null, array('class'=>'form-text ui-corner-all', 'label'=>false, 'disabled'=>'disabled', 'div'=>false), false); ?></td>
		</tr>
		<tr>
			<td class="label-req">Subtext:</td>
			<td><?php echo $form->input('subtext', array('class'=>'form-text ui-corner-all ui-state-default', 'label'=>false, 'div'=>false, 'type'=>'textarea'))?><br />
			<span class="base orange">(Enter a brief category description.)</span></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<?php $checked = (empty($this->data['LocationsCat']['time_open'])) ? array('checked'=>'checked') : array(); ?>
				<div class="ui-helper-clearfix marg-b5">
					<div class="left pad-r10"><?php echo $form->checkbox('avail_all_day', $checked); ?></div>
					<div class="left b">Is Menu Category Served Entire Business Day?</div>
				</div>
			</td>
		</tr>
	</table>
	<div id="availTimes">
		<table class="form-table">
			<tr>
				<td class="label-req" style="width: 150px;">Start Serving Time:</td>
				<td><?php echo $form->input('time_open', array('class'=>'ui-corner-all ui-state-default', 'label'=>false, 'type'=>'time')); ?>
				</td>
			</tr>
			<tr>
				<td class="label-req">End Serving Time:</td>
				<td><?php echo $form->input('time_closed', array('class'=>'ui-corner-all ui-state-default', 'label'=>false, 'type'=>'time')); ?>
				</td>
			</tr>
		</table>
	</div>
	<table class="form-table">
		<tr>
			<td style="width:150px;"></td>
			<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Menu Category', null, 'btnMenuEditCat', null, '165'); ?></td>
		</tr>
	</table>
	
	<div class="ui-helper-clearfix">
		<div class="right base"><?php echo $html->link('Add a New Selection Series...', '#', array('id'=>'linkAddNewSeries')); ?></div>
	</div>
	<div class="ui-widget-content ui-corner-all marg-t5" id="divAddNewSeries">
		<table>
			<tr>
				<td class="b txt-r pad-r5">How many choices will be in this series?</td>
				<td>
					<div class="ui-helper-clearfix">
						<div class="left pad-r5"><?php echo $form->select('new_series_qty', $misc->qtys(), null, array('class'=>'form-text')); ?></div>
						<div class="left" style="width:25px;"><div id="progressSeriesLimit"><?php echo $html->image('ajax-loader-sm-circle.gif', array('width'=>'25')); ?></div></div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="b">How many of these choices can the customer select?</td>
				<td><?php echo $form->select('new_series_limit', array(), null, array('class'=>'form-text'), false); ?></td>
			</tr>
		</table>
		<div id="divChoiceSeriesLabels"></div>
	</div>
	</form>
	
	<?php if(sizeof($this->data['LocationsCatsOption']) > 0) { ?>
	<div class="ui-widget-content ui-corner-all marg-t5"><?php 
		$current = false;
		foreach($this->data['LocationsCatsOption'] as $opt):
			if($opt['add_group_id'] != 0 && $opt['add_group_id'] != '0') {
				//part of a selection series
				if($opt['add_group_id'] != $current) {
					//still on the same series
					?>
					<div class="ui-helper-clearfix marg-t10">
						<div class="left b green">Comes with your choice of (<?php echo $opt['add_group_limit']; ?>):</div>
						<div class="right">
							<?php echo $jquery->btn('/locations_cats/delete_series/'.$opt['id'], 
													'ui-icon-circle-close', 'Remove Selection Series', 
													'Are you sure you want to Remove this Selection Series from this Menu Category?'); ?>
						</div>
					</div>
					<?php 
				} 
				?>
				<div class="ui-helper-clearfix pad-l20">
					<div class="left pad-r5"><?php echo $html->image('list-green-check.gif'); ?></div>
					<div class="left pad-b5" style="width:375px;"><?php 
						echo $opt['label']; 
						if($opt['additional_price'] > 0) { 
							echo '<span class="b base pad-l5">(+$'.number_format($opt['additional_price'], 2).')</span>'; 
						} else if($opt['additional_price'] < 0) {
							echo '<span class="b base red pad-l5">($'.number_format($opt['additional_price'], 2).')</span>';
						}
					?></div>
				</div><?php 
			} else {
				//individual add on option
				if($opt['add_group_id'] != $current) {
					//still on the same series
					?>
					<div class="ui-helper-clearfix marg-t10 marg-b10">
						<div class="left b green">Additional Add-On Options Available:</div>
					</div>
					<?php 
				} 
				?>
				<div class="ui-helper-clearfix pad-l20">
					<div class="left pad-r5"><?php echo $html->image('list-green-check.gif'); ?></div>
					<div class="left pad-b5" style="width:375px;"><?php 
						echo $opt['label']; 
						if($opt['additional_price'] > 0) { 
							echo '<span class="b base pad-l5">(+$'.number_format($opt['additional_price'], 2).')</span>'; 
						} else if($opt['additional_price'] < 0) {
							echo '<span class="b base red pad-l5">($'.number_format($opt['additional_price'], 2).')</span>';
						}
					?></div>
					<div class="right">
						<?php echo $jquery->btn('/locations_cats/delete_series/'.$opt['id'], 
													'ui-icon-circle-close', 'Remove Option', 
													'Are you sure you want to Remove this Add-On Option from this Menu Category?'); ?>
					</div>
				</div><?php 	
			} 
			$current = $opt['add_group_id'];
		endforeach; ?>
	</div>
	
	<div class="ui-helper-clearfix marg-t5">
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Update Menu Category', null, 'btnMenuEditCat', null, '165'); ?></div>
	</div>
	<?php } ?>
</fieldset>