<script>
$(function() {
	$('.uniform-select').uniform();
	
	$('.create-new-cat').click(function() {
		var catDiv = $(this).closest('.new-cat-div');
		var indexParts = catDiv.attr('id').split('_');
		
		catDiv.html('<input type="text" class="form-text ui-corner-all ui-state-default" name="data[cats][' + indexParts[1] + '][cat_id]" ' + 
					'id="cats' + indexParts[1] + 'CatId" /><br />' + 
					'<span class="base b">Enter New Category Title: <span class="required">*</span></span>');
	});
});
</script>


<?php for($x = 1; $x <= $qty; $x++) { ?>
<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">New Category <?php echo $x; ?></legend>
	<div class="ui-helper-clearfix">
		<div class="left pad-l20" style="width:230px;">
			<div class="marg-b5 new-cat-div" id="div-index_<?php echo $x; ?>">
				<div class="marg-b5">
					<?php echo $form->select('cats.'.$x.'.cat_id', $cat_list, null, array('class'=>'form-text uniform-select'), 'Select a Category'); ?>
				</div>
				<?php echo $jquery->btn('#', 'ui-icon-extlink', 'Create New Category', null, 'create-new-cat', null, '155'); ?>
			</div>
		</div>
		<div class="left">
			<div><?php echo $form->input('cats.'.$x.'.subtext',array('class'=>'form-text ui-state-default ui-corner-all', 
																  'label'=>false, 'div'=>false, 'rows'=>'3', 'type'=>'textarea')); ?>
			</div><div class="base b orange">(Enter any Category Subtext.)</div>
		</div>
	</div>
</fieldset>
<?php } ?>