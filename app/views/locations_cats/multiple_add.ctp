<?php echo $html->script('jquery/alphanumeric'); ?>
<script>
$(function() {
	$('#progressNumCats, #addCatSubmitDiv').hide();
	
	$('#LocationsCatQty').change(function() { 
		$('#progressNumCats').show();
		$('#ajaxCatsDiv').load('/locations_cats/ajax_multiple_cats', { numCats: $(this).val() }, function() {
			$('#addCatSubmitDiv').show();
			$('#progressNumCats').hide();
		});
		return false;
	});
});

</script>
<?php echo $jquery->scrFormSubmit('#btnMultipleAddCat', '#formMultipleAddCat'); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Add Multiple Menu Categories</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/locations/edit_menu/'.$location['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to Menu Manager'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b10">
	<?php echo $form->create('LocationsCat', array('action' => 'multiple_add', 'id'=>'formMultipleAddCat')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
	
	<p>Use this tool, if you would like to add multiple menu categories to your Online Menu at the same time. Select each category you would like to 
	add from the drop down menus. In the event that you need to add a new category to our system, click the corresponding 
	<span class="italic b">"Create New Category"</span> button, and enter the new category in the field that is generated for you. <br />
	When you are finished, click the <span class="italic b">"Add New Menu Categories"</span> button to save your new categories.</p>
	
	<fieldset class="ui-widget-content ui-corner-all  marg-b10">
		<legend class="ui-corner-all">Add Details</legend>
		
		<table class="form-table" style="width:100%;">
			<tr>
				<td class="label-req" style="width:240px;">How many categories will you be adding: <span class="required">*</span></td>
				<td>
					<div class="ui-helper-clearfix">
						<div class="left"><?=$form->select('qty', $misc->qtys(false, 20), null, array("class"=>"form-text", 'label'=>false), 'Select Quantity')?></div>
						<div class="left" id="progressNumCats"><?php echo $html->image('ajax-loader-med-bar.gif', array('style'=>'width:115px;')); ?></div>
					</div>				
				</td>
			</tr>
		</table>
	</fieldset>
	
	<div id="ajaxCatsDiv"></div>
	<div class="ui-helper-clearfix" id="addCatSubmitDiv">
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Menu Categories', null, null, 'btnMultipleAddCat'); ?></div>
	</div>
	</form>
</div>