<?php
class MiscHelper extends AppHelper {
	var $helpers = array('Html');
	
    function googleDirections($addr, $state) {
    	$url = 'http://maps.google.com/maps?f=d&hl=en&geocode=&daddr='.
    		   urlencode($addr['address'].', '.$addr['city'].', '.$state.' '.$addr['zip']);
    	return $this->output($this->Html->link('Get Directions', $url, array('target' => '_blank')));
    }
    
    //formats a 10 digit Int into a proper phone number format.
	function formatPhone ($phone) {
    	return '('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone, 6);
    }
    
	function jqueryBtn($destination, $icon, $icon_txt = null, $confirm_msg = null, $class = null, $id = null, $div_width = true, $disabled = false, $title= null) {
    	$btn = '';
    	if($div_width) {
    		$btn .= (is_numeric($div_width)) ? '<div style="width:'.$div_width.'px;">' : '<div>';
    	}
    	$btn .=  '<a href="'.$destination.'"';
    	if($title) { $btn .= ' title="'.$title.'" '; }
    	if($confirm_msg) { $btn .= ' onclick="return confirm(\''.$confirm_msg.'\');" '; }
    	$btn .= ($class) ? ' class="ui-icon-link '.$class.'" ' : ' class="ui-icon-link" ';
    	if($id) { $btn .= ' id="'.$id.'" '; }
    	$btn .= '><div class="';
    	$btn .= ($disabled) ? 'ui-state-disabled' : 'ui-state-default'; 
    	
    	$btn .= ' ui-helper-clearfix ui-corner-all ui-icon-container">';
    	$btn .= '<div class="ui-icon '.$icon.' left"></div>';
    	if($icon_txt) { $btn .= '<div class="ui-icon-text">'.$icon_txt.'</div>'; }
    	$btn .= '</div></a>';
    	if($div_width) $btn .= '</div>';
    	return $btn;
    }
    
    function sauces() {
    	return array('Extra Sauce' => 'Extra Sauce', 'Regular Sauce'=>'Regular Sauce',
 					 'Light Sauce' => 'Light Sauce', 'No Sauce'=>'No Sauce');
    }
    
    function cheeses() {
    	return array('Extra Cheese' => 'Extra Cheese', 'Regular Cheese'=>'Regular Cheese',
 					 'Light Cheese' => 'Light Cheese', 'No Cheese'=>'No Cheese');
    }
    
    function qtys($zerofill = null, $max = null) {
    	$options = array();
    	if(!$max) $max = 10;
		for($x = (($zerofill) ? 0 : 1); $x <= $max; $x++) { $options[$x] = $x; }
    	return $options;
    }
    
    function calcImagePath($img, $path) {
    	return (!empty($img) && file_exists(WWW_ROOT.$path.$img)) ? '../'.$path.$img : 'image-not-available.jpg';
    }
    
    function hrs() {
    	return array('00' => '12am', '01' => '1am', '02' => '2am', '03' => '3am', '04' => '4am', '05' => '5am', '06' => '6am',
					 '07' => '7am', '08' => '8am', '09' => '9am', '10' => '10am', '11' => '11am', '12' => '12pm',
					 '13' => '1pm', '14' => '2pm', '15' => '3pm', '16' => '4pm', '17' => '5pm', '18' => '6pm',
					 '19' => '7pm', '20' => '8pm', '21' => '9pm', '22' => '10pm','23' => '11pm');
    }
    
    function mins() {
    	$mins = array('00' => '00', '01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', 
    				  '08' => '08', '09' => '09');
    	for($x = 10; $x <= 59; $x++) {
    		$mins[$x] = $x;
    	}
    	return $mins;
    }

	function timeToMilitary($time = null) {
		return (!$time) ? false : date("H:i:s", strtotime($time));
	}
	
	function timeFromMilitary($time = null) {
		return (!$time) ? false : date("g:i a", strtotime($time));
	}
	
	function prefixNameToId($name = null) {
		return isset($name) ? str_replace(array('[', ']', 'data'), '', $name) : false;
	}
	
	//calculate the difference between server time and local time offset
	function getLocalTimeOffset() {
		//for now just manually set back two hours for PST timezone
		return strtotime("-2 hours");
	}
}

?>