<?php
class GeoipHelper extends AppHelper {
	var $helpers = array('Html');
	
	//returns associated geoip location info by passed ip address
	function geoInfoByIp($ip_address = null) {
		include(WWW_ROOT . "inc/geoip.inc");
		include(WWW_ROOT . "inc/geoipcity.inc");

		$gi = geoip_open(WWW_ROOT . "csv/GeoLiteCity.dat", GEOIP_STANDARD);
		$record = geoip_record_by_addr($gi, $ip_address);
		geoip_close($gi);
		
		return $record;
	}
}
?>