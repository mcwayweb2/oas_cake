<?php
class JqueryHelper extends AppHelper {
	var $helpers = array('Html');
	
    function btn($destination, $icon, $icon_txt = null, $confirm_msg = null, $class = null, $id = null, $div_width = true, $disabled = false, $title= null) {
    	$btn = '';
    	if($div_width) {
    		$btn .= (is_numeric($div_width)) ? '<div style="width:'.$div_width.'px;">' : '<div>';
    	}
    	$btn .=  '<a href="'.$destination.'"';
    	if($title) { $btn .= ' title="'.$title.'" '; }
    	if($confirm_msg) { $btn .= ' onclick="return confirm(\''.$confirm_msg.'\');" '; }
    	$btn .= ($class) ? ' class="ui-icon-link '.$class.'" ' : ' class="ui-icon-link" ';
    	if($id) { $btn .= ' id="'.$id.'" '; }
    	$btn .= '><div class="';
    	$btn .= ($disabled) ? 'ui-state-disabled' : 'ui-state-default'; 
    	
    	$btn .= ' ui-helper-clearfix ui-corner-all ui-icon-container">';
    	$btn .= '<div class="ui-icon '.$icon.' left"></div>';
    	if($icon_txt) { $btn .= '<div class="ui-icon-text">'.$icon_txt.'</div>'; }
    	$btn .= '</div></a>';
    	if($div_width) $btn .= '</div>';
    	return $btn;
    }
    
    function scrFormSubmit($btn_id, $form_id) {
    	$scr = "
    	<script>
		$(function() {
			$('$btn_id').click( function () {
				$('$form_id').submit();
			});
		});	
		</script>";
    	return $scr;
    }
}

?>