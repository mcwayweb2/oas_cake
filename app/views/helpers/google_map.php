<?php
class GoogleMapHelper extends Helper {

	var $errors = array();

	function directionsMap($start_lat, $start_long, $addr_start, $addr_end, $style = "width:500px; height: 300px;", $class = 'ui-corner-all gray-border') {
		$out = "<div id=\"map_canvas\"";
		$out .= isset($style) ? "style=\"".$style."\"" : null;
		$out .= isset($class) ? " class=\"".$class."\"" : null;
		$out .= " ></div>";
		$out .= "<div id=\"route\"></div>";
		$out .= "
		<script type=\"text/javascript\">
		function initialize() {
			
		  	//var map = new google.maps.Map(
				//document.getElementById(\"map_canvas\"), 
				//{
				
				//}
			//);
			
		  map.setCenter(new google.maps.LatLng(".$start_lat.",".$start_long."), 15);
		  var directionsPanel = document.getElementById(\"route\");
		  var directions = new google.maps.Directions(map, directionsPanel);
		  directions.load(\"from: ".$addr_start." to: ".$addr_end."\");
		}
		</script>";
		return $out;
	}

	function map($default, $style = 'width: 350px; height: 470px', $show_controls = true, $class = 'ui-corner-all gray-border', $markers = null)
	{
		//if (empty($default)){return "error: You have not specified an address to map"; exit();}
		$out = "<div id=\"map\"";
		$out .= isset($style) ? "style=\"".$style."\"" : null;
		$out .= isset($class) ? " class=\"".$class."\"" : null;
		$out .= " ></div>";
		$out .= "
		<script type=\"text/javascript\">
			function initialize() {	
				var map = new google.maps.Map(
					document.getElementById(\"map\"), 
					{
						center:  	new google.maps.LatLng(".$default['lat'].", ".$default['long']."),
						zoom: 		".$default['zoom'].",
						mapTypeId: 	google.maps.MapTypeId.ROADMAP	
					}
				);
		";

		//add any passed markers
		if(!empty($markers)) {
			if(empty($markers['icon'])) $markers['icon'] = null;
			if(empty($markers['sizes'])) $markers['sizes'] = null;
			
			$out .= $this->addMarkers($markers['points'], $markers['icon'], $markers['sizes']);
		}
		
		$out .= "
			}
			 google.maps.event.addDomListener(window, 'load', initialize);
		</script>";

		return $out;
	}

	function addMarkers(&$data, $icon=null, $icon_size = null)
	{
		$output = "";
		if(is_array($data))
		{
			$i = 0;
			foreach($data as $n=>$m){
				$temp = $i + 1;
				$markerPath = ($icon) ? $this->webroot.'img/'.$icon : $this->webroot.'img/RedIcons/marker'.$temp.'.png'; 
				
				/*	
				if(!isset($icon_size)) { $icon_size = array('18','22','6'); }
				$out .= 'icon.iconSize = new google.maps.Size('.$icon_size[0].', 20);
				icon.shadowSize = new google.maps.Size('.$icon_size[1].', 20);
				icon.iconAnchor = new google.maps.Point('.$icon_size[2].', 20);
				icon.infoWindowAnchor = new google.maps.Point(5, 1);
				';
				*/
	
				$keys = array_keys($m);
				$point = $m[$keys[0]];
				if(!preg_match('/[^0-9\\.\\-]+/',$point['longitude']) && preg_match('/^[-]?(?:180|(?:1[0-7]\\d)|(?:\\d?\\d))[.]{1,1}[0-9]{0,15}/',$point['longitude']) && 
					!preg_match('/[^0-9\\.\\-]+/',$point['latitude']) && preg_match('/^[-]?(?:180|(?:1[0-7]\\d)|(?:\\d?\\d))[.]{1,1}[0-9]{0,15}/',$point['latitude']))
				{
					$output .= "
						var marker".$i." = new google.maps.Marker({
            				position: new google.maps.LatLng(".$point['latitude'].", ".$point['longitude']."),
            				map: map,
            				icon: '".$markerPath."'
      					});		
					";
					$i++;
				}
			}
		}
		return $output;
	}
	/*
	function addClick($var, $script=null)
	{
		$out = "
			<script type=\"text/javascript\">
			//<![CDATA[
			" 
			.$script
			.'GEvent.addListener(map, "click", '.$var.', true);'
			." 
				//]]>
			</script>";
		return $out;
	}	
	
	function addMarkerOnClick($innerHtml = null)
	{
		$mapClick = '
			var mapClick = function (overlay, point) {
				var point = new google.maps.Point(point.x,point.y);
				var marker = new google.maps.Marker(point,icon);
				map.addOverlay(marker)
				GEvent.addListener(marker, "click", 
				function() {
					marker.openInfoWindowHtml('.$innerHtml.');
				});
			}
		';
		return $this->addClick('mapClick', $mapClick);
		
	}	
	*/
}
?>