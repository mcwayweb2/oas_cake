<?php echo $html->script('jquery/alphanumeric'); ?>
<script>
	$(function() {	
		$('#loader-img').hide(); 
		//determine which drawer to open at page load
		var opts = { autoHeight: false, 
					 active:    <?php echo (sizeof($profiles) > 0) ? '1' : '0'; ?> };
		$('#AdvertiserPayProfilesAccordion').accordion(opts);

		$('.num-only').numeric();

		$('#btnAdvertiserPayProfileAdd').click(function() { 
			if($('#AdvertisersPaymentProfileAgreement').is(':checked')) {
				if($('#AdvertisersPaymentProfileTempType').val() == '') {
					$('#profileTypeText').text('Please Select a Profile Type!');
				} else {
					$('#formAdvertiserPayProfileAdd').submit();
				}
			} else {
				$('#AdvertisersPaymentProfileAgreementText').text('Please agree to the terms to proceed.');
				$('#agreementFieldset').addClass('ui-state-highlight');
			}
			return false;
		});
	});
</script>

<?php if(sizeof($profiles) <= 0) { 
	echo $this->element('warning-msg-box', array('msg'=>'NOTE: You must '.$html->link('Add a Billing Profile', '/advertisers/payment_profiles').' before your restaurants can accept online orders.'));
} ?>

<div>
	<div class="ui-widget-header ui-corner-top pad5">
		<div class="pad-l5"><h1>Manage Billing Profiles</h1></div>
	</div>
	<div class="ui-widget-content ui-corner-bottom">
		<?php //echo debug($prof); ?>
		<div class="ui-widget ui-accordion" id="AdvertiserPayProfilesAccordion">
			<h2 class="ui-widget-header ui-accordion-header"><a href="#">Add New Billing Profile</a></h2>
			<div class="ui-widget-content ui-accordion-content">
				<p>Use the following form below to add a payment profile to your account. You may set up as many Payment Profiles as you would like, but you must 
				have a <span class="b">"Default Billing Profile"</span> with us at all times in order for us to process your order payouts.</p>
				<p>Payouts for your online orders are credited to your <span class="b">"Default Billing Profile"</span> immediately after each order is 
				confirmed. Funds generally take	1-2 business days after processing to show up in your bank account.</p>
			   
				<?=$form->create('AdvertisersPaymentProfile', array('action'=>'manage', 'id'=>'formAdvertiserPayProfileAdd'))?>
				<?=$form->hidden('advertiser_id', array('value'=>$session->read('Advertiser.id'))) ?>
				<?=$form->hidden('profile_type', array('value'=>'Credit Card')) ?>
				<fieldset class="marg-b10">
					<legend>Profile Details</legend>
					<table class="form-table">
						<tr>
							<td class="label-req" style="width:150px;">Give this profile a name: <span class="required">*</span></td>
							<td><?=$form->input('name', array('class'=>'form-text', 'label'=>false, 'div'=>false))?><br />
							<span class="base orange">(Ex. Company Visa, or Checking Account)</span></td>
						</tr>
						<tr>
							<td class="label-req"></td>
							<td class="b"><?=$form->checkbox('default')?>Mark as Default Billing Profile?</td>
						</tr>
					</table>
				</fieldset>
			
				
				<fieldset class='marg-b10 ui-corner-all ui-widget-content'>
					<legend class="ui-corner-all">Credit Card Billing Details</legend>
					<div class="ui-helper-clearfix">
						<div class="left">
							<table class="form-table">
								<tr>
									<td class="label-req" style="width:150px;">First Name on Card: <span class="required">*</span></td>
									<td><?=$form->input('AdvertisersPaymentProfile.fname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
								</tr>
								<tr>
									<td class="label-req">Last Name on Card: <span class="required">*</span></td>
									<td><?=$form->input('AdvertisersPaymentProfile.lname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
								</tr>
								<tr>
						 			<td class="label-req">Address: <span class="required">*</span></td>
									<td><?=$form->input('AdvertisersPaymentProfile.address', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
						 		</tr>
						 		<tr>
						 			<td class="label-req">City: <span class="required">*</span></td>
						 			<td><?=$form->input('AdvertisersPaymentProfile.city', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
						 		</tr>
						 		<tr>
						 			<td class="label-req">State: <span class="required">*</span></td>
						 			<td><?=$form->select('AdvertisersPaymentProfile.state', $states, null, array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'), false)?></td>
						 		</tr>
						 		<tr>
						 			<td class="label-req">Zip: <span class="required">*</span></td>
						 			<td><?=$form->input('AdvertisersPaymentProfile.zip', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
						 		</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
									<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?php //echo $html->image('creditcard_amex.gif')?>   
									</td>
								</tr>
								<tr>
									<td class="label-req">Card Number: <span class="required">*</span></td>
									<td><?=$form->input('AdvertisersPaymentProfile.cc_num', array('class'=>'form-text num-only ui-state-default ui-corner-all', 'label'=>'', 'value'=>''))?></td>
								</tr>
								<tr>
									<td class="label-req">Expiration Month: <span class="required">*</span></td>
									<td><?=$form->month('AdvertisersPaymentProfile.month', null, array('class'=>'ui-state-default ui-corner-all form-text', 'style'=>'width:100px;'), false)?></td>
								</tr>
								<tr>
									<td class="label-req">Expiration Year: <span class="required">*</span></td>
									<td><?=$form->year('AdvertisersPaymentProfile.year', date("Y"), date("Y")+10, null, array('class'=>'form-text ui-state-default ui-corner-all'), false)?></td>
								</tr>
							</table>
						</div>
						<div class="right marg-t20">
							<div class="marg-b10"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
							<div style="padding-left:25px;"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
						</div>
					</div>
				</fieldset>
				
				<fieldset class="marg-b10" id="agreementFieldset">
					<legend>Authorization Agreement</legend>
					<div class="ui-helper-clearfix">
						<div class="left txt-r" style="width:160px;"><?php echo $form->checkbox('agreement'); ?></div>
						<div class="left" style="width:345px;">By checking this box, I am agreeing that I am legally authorized <br />by <span class="b italic">
						<?php echo $session->read('Advertiser.contact_fname')." ".$session->read('Advertiser.contact_lname'); ?></span> 
						to conduct business as <span class="b italic"><?php echo $session->read('Advertiser.name'); ?></span> with this billing information.
						<div id="AdvertisersPaymentProfileAgreementText" class="b red"></div>
						</div>
					</div>
				</fieldset>
				
				<?php echo $jquery->btn('#', 'ui-icon-suitcase', 'Submit', null, null, 'btnAdvertiserPayProfileAdd', '80'); ?>
				<?php echo $form->end(); ?>
			</div> <!-- end accordion drawer "Add New Payment Profile" -->
			
			
			<?php if(sizeof($profiles) > 0) { ?>
				<?php foreach($profiles as $profile): ?>
					<h2 class="ui-widget-header ui-accordion-header"><a href="#"><?=$profile['AdvertisersPaymentProfile']['name'] ?></a></h2>
					<div class="ui-widget-content ui-accordion-content">
						<div class="ui-helper-clearfix">
							<div class="left" style="width:200px;">
								<div class="italic marg-b5"><?php echo $profile['AdvertisersPaymentProfile']['card_suffix']; ?></div>
								<p><span class="b">Billing Info:</span><br />
								<?php echo $profile['AdvertisersPaymentProfile']['fname']." ".$profile['AdvertisersPaymentProfile']['lname']; ?><br />
								<?php echo $profile['AdvertisersPaymentProfile']['address']?><br />
								<?php echo $profile['AdvertisersPaymentProfile']['city'].', '.$profile['AdvertisersPaymentProfile']['state'].' '.$profile['AdvertisersPaymentProfile']['zip']; ?></p>
							</div>
							<div class="left" style="width:200px;">
								<div class="b">Profile Added:</div>
								<div><?php echo $time->nice($profile['AdvertisersPaymentProfile']['timestamp']); ?></div>
							</div>
							<div class="right valign-t"><?php 
								if($profile['AdvertisersPaymentProfile']['default'] == '1') { 
									echo $this->element('warning-msg-box', array('msg'=>'Default Pay Profile'));
								} else {
									echo $jquery->btn('/advertisers_payment_profiles/remove/'.$profile['AdvertisersPaymentProfile']['id'], 'ui-icon-circle-close', 'Remove Pay Profile', 
													  'Are you sure that you would like to completely remove this Billing Profile');
									echo $jquery->btn('/advertisers_payment_profiles/set_default/'.$profile['AdvertisersPaymentProfile']['id'], 'ui-icon-check', 'Set As Default');	
								}
							?>
							
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php } ?>
		</div>
	</div>
 </div>
 
 <?php //echo debug($default_prof); ?>