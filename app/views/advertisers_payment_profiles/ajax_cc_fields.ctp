<?php echo $this->element('js/jquery/add_form_text_events'); ?>
<script>
	$(function() {	
		$('.num-only').numeric();
	});
</script>

<fieldset class='marg-b10 ui-corner-all ui-widget-content'>
	<legend class="ui-corner-all">Credit Card Billing Details</legend>
	<div class="ui-helper-clearfix">
		<div class="left">
			<table class="form-table">
				<tr>
					<td class="label-req" style="width:150px;">First Name on Card: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.fname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Last Name on Card: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.lname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
				<tr>
		 			<td class="label-req">Address: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.address', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">City: <span class="required">*</span></td>
		 			<td><?=$form->input('AdvertisersPaymentProfile.city', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">State: <span class="required">*</span></td>
		 			<td><?=$form->select('AdvertisersPaymentProfile.state', $states, null, array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'), false)?></td>
		 		</tr>
		 		<tr>
		 			<td class="label-req">Zip: <span class="required">*</span></td>
		 			<td><?=$form->input('AdvertisersPaymentProfile.zip', array('label'=>'','class'=>'form-text ui-state-default ui-corner-all'))?></td>
		 		</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
					<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?php //echo $html->image('creditcard_amex.gif')?>   
					</td>
				</tr>
				<tr>
					<td class="label-req">Card Number: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.cc_num', array('class'=>'form-text num-only ui-state-default ui-corner-all', 'label'=>'', 'value'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Month: <span class="required">*</span></td>
					<td><?=$form->month('AdvertisersPaymentProfile.month', null, array('class'=>'ui-state-default ui-corner-all form-text', 'style'=>'width:100px;'), false)?></td>
				</tr>
				<tr>
					<td class="label-req">Expiration Year: <span class="required">*</span></td>
					<td><?=$form->year('AdvertisersPaymentProfile.year', date("Y"), date("Y")+10, null, array('class'=>'form-text ui-state-default ui-corner-all'), false)?></td>
				</tr>
			</table>
		</div>
	</div>
</fieldset>