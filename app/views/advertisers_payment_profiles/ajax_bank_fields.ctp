<?php echo $this->element('js/jquery/add_form_text_events'); ?>
<script>
	$(function() {	
		$('.num-only').numeric();
	});
</script>
<fieldset class="marg-b10 ui-corner-all ui-widget-content">
	<legend class="ui-corner-all">Bank Account Details</legend>
	
	<div class="ui-helper-clearfix">
		<div class="left">
			<table class="form-table">
				<tr>
					<td class="label-req" style="width:150px;">First Name on Account: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.fname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Last Name on Account: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.lname', array('class'=>'form-text ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req" style="width:150px;">Routing Number: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.routing', array('class'=>'form-text num-only ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
				<tr>
					<td class="label-req">Account Number: <span class="required">*</span></td>
					<td><?=$form->input('AdvertisersPaymentProfile.account', array('class'=>'form-text num-only ui-state-default ui-corner-all', 'label'=>''))?></td>
				</tr>
			</table>
		</div>
	</div>
</fieldset>