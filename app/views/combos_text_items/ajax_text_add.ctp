<script>
$(function() { 
	$('#CombosTextItemText, #CombosTextItemQty').uniform();

	$('#btnComboBuilderAddTextItem').click( function () {
		if($('#CombosTextItemText').val() == '' || $('#CombosTextItemText').val() == null) {
			$('#CombosTextItemTextText').text('Please enter the Item Text!');
		} else {
			$('#formComboBuilderAddTextItem').submit();
		}
		return false;
	});
});
</script>
<?php echo $this->element('js/jquery/add_form_text_events'); ?>

<?php echo $form->create('CombosTextItem', array('action'=>'add', 'id'=>'formComboBuilderAddTextItem')); ?>
<?php echo $form->hidden('combo_id', array('value'=>$combo_id)); ?>
	
<hr class="orange" />
<table class="form-table">	
	<tr>
		<td class="label-req" style="width:150px;">Enter Item Text:</td>
		<td><?php echo $form->input('CombosTextItem.text', array('class' => 'form-text', 'id' => 'CombosTextItemText', 'label' => false)); ?>
			<div class="base b red" id="CombosTextItemTextText"></div></td>
	</tr>
	<tr>
		<td class="label-req">Quantity:</td>
		<td><?php echo $form->select('CombosTextItem.qty', $misc->qtys(), null, array('class' => 'form-text'), false); ?></td>
	</tr>
	<tr><td></td>
		<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Combo Item', null, null, 'btnComboBuilderAddTextItem', '130')?></td>
	</tr>
</table>
</form>