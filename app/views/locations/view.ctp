<?=$this->element('js_googlemap_headers') ?>
<div class="ui-widget-content ui-corner-all">
<?php
	$location = $locations[0];
	$addr = $location['Location']['address1']."<br />";
	if(!empty($location['Location']['address2'])) { $addr .= $location['Location']['address2']."<br />"; }
	$addr .= $location['Location']['city'].", ". $location['State']['abbr']." ".$location['ZipCode']['id'];
?>

	<div class="ui-helper-clearfix">
	    <div class="left pad-r10" style="width:160px;">
	    <?php 
	    	if($location['Location']['use_logo_in_banner'] == '2' && !empty($location['Advertiser']['logo'])) {
	    		//attempt to display advertiser logo
	    		echo $html->link($html->image('../files/logos/'.$location['Advertiser']['logo'], 
	    								      array('style'=>'width:150px;', 'class'=>'ui-corner-all orange-border pad2')), 
				                 '/menu/'.$location['Location']['slug'], 
	    						 array('escape'=>false));
	    	} else if(!empty($location['Location']['logo'])) {
	    		//attempt to display location logo
	    		echo $html->link($html->image('../files/logos/'.$location['Location']['logo'], 
	    								      array('style'=>'width:150px;', 'class'=>'ui-corner-all orange-border pad2')), 
				                 '/menu/'.$location['Location']['slug'], 
	    						 array('escape'=>false));
	    	} else { echo '&nbsp;'; }
	    ?></div>
	    <div class="left">
	    	<h1><?php echo $location['Location']['name']?></h1>
	    	<div class="b green marg-b5"><?php echo $location['Location']['slogan']?></div>
	    	<div class="b">Cuisine: <?php echo $location['FoodCat']['title']?></div>
	    	<!--<div class="b base">(Approx. 3.12 miles from your location.)</div>-->
	    </div>
	    <div class="right" style="width:130px;">
			<?php 
			echo $misc->jqueryBtn('/menu/'.$location['Location']['slug'], 
								  'ui-icon-cart', 'View Menu');
			if($session->check('User')) {	
				echo $misc->jqueryBtn('/users/add_favorite', 'ui-icon-star', 'Mark as Favorite', 
									  null, 'add-fav-link', 'fav-'.$location['Location']['id']);
			}
			?>
			<div id="ajax-fav-update-<?=$location['Location']['id']?>"></div>
		</div>
	</div>
	<hr />
	    
	<div class="ui-helper-clearfix">
	    <div class="left">
	    	<?php if($location['Location']['accept_pickups'] == '1' && $location['Location']['currently_open'] == '1') { ?>
	    	<div class="ui-helper-clearfix marg-b5">
	    		<div class="left pad-r5">
	    			<?php echo $html->image('list-green-check.gif', array('class'=>'orange-border pad2', 'style'=>'width:15px;'));?>
	    		</div>
	    		<div class="left b red">Currently Accepting Pickup Orders</div>
	    	</div>
	    	<?php } ?>
	    	<?php if($location['Location']['accept_deliveries'] == '1' && $location['Location']['currently_open'] == '1') { ?>
	    	<div class="ui-helper-clearfix">
	    		<div class="left pad-r5">
	    			<?php echo $html->image('list-green-check.gif', array('class'=>'orange-border pad2', 'style'=>'width:15px;'));?>
	    		</div>
	    		<div class="left b red">Currently Accepting Delivery Orders</div>
	    	</div>
	    	<?php } ?>
	    </div>
	    <div class="right pad-r20 txt-r" style="width:220px;">
	    	<div class="ui-helper-clearfix marg-b5">
	    		<div class="left b" style="width:170px;">Minimum Delivery Order:</div>
	    		<div class="right" style="width:50px;"><?php echo $number->currency($location['Location']['min_delivery_amt']); ?></div>
	    	</div>
	    	<div class="ui-helper-clearfix">
	    		<div class="left b" style="width:170px;">Delivery Fee:</div>
	    		<div class="right" style="width:50px;">
	    			<?php echo $number->currency($location['Location']['delivery_charge'] + OAS_PER_ORDER_FEE); ?>
	    		</div>
	    	</div>
	    </div>
	</div>
	
	<hr />
	<?php
	$default = array('type'=>'0','zoom'=>14,'lat'=>$location['Location']['latitude'],'long'=>$location['Location']['longitude']);	
	?>
	<div id="map_placeholder">
		<?php
		//if(isset($default_addr)) {
			//$addr_end = $location['Location']['address1'].", ".$location['Location']['city'].", ".$location['State']['abbr']." ".$location['ZipCode']['id'];
			//$addr_start = $default_addr['UsersAddr']['address'].", ".$default_addr['UsersAddr']['city'].", ".$default_addr['State']['abbr']." ".$default_addr['UsersAddr']['zip'];
			//echo $googleMap->directionsMap($default['lat'], $default['long'], $addr_start, $addr_end, $style = "width:585px; height: 300px;");			
		//} else {
			echo $googleMap->map(	
					$default,
					$style = 'width:585px; height: 300px;',
					false,
					'ui-corner-all gray-border',
					array('points'=> $locations));	
		//}
		?>
	</div>
</div>