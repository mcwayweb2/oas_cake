<script>
$(function() {
	$('fieldset').addClass('ui-widget-content ui-corner-all marg-b10');
	$('legend').addClass('ui-corner-all');

	$('#btnLocationSubscriptionReview').click( function () {
		if($('#LocationAgree').is(':checked')) {
			$('#formLocationSubscriptionReview').submit();
		} else {
			$('#AgreeCheckboxContent').addClass('ui-state-highlight ui-corner-all');
			alert('Please agree to the terms and conditions!');
		}
		return false
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Review Subscription</h1></div>
 		<div class="right pad-r5"><h2 class="green"><?=$location['Advertiser']['name']." - ".$location['Location']['name']?></h2></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p>Please take a moment to review your subscription details one last time. If you would like to make any changes, you can go 
 	back to the <?=$html->link('subscription page', '/locations/subscription/'.$location['Location']['id']) ?>, and make any 
 	necessary adjustments. If you're ready to start this subscription, please read through the terms below, and press "Confirm Subscription" 
 	to complete your order.</p>
 	 <fieldset><legend>Subscription Details</legend>
 		<table class="list-table">
 			<tr>
 				<td>Monthly Base Fee:</td>
 				<td class="right">$39.00</td>
 			</tr>
 			<?php if($location['Location']['accept_cc'] == 1) { ?>
 			<tr>
 				<td>Accept Credit Card Transactions:</td>
 				<td class="right">$10.00</td>
 			</tr>
 			<?php } ?>
 			<?php echo $this->element('table_accent_row', array('cols'=>2)); ?>
 			<tr>
 				<td>Total Monthly Recurring Fee:</td>
 				<td class="right"><?php echo $number->currency($location['Location']['arb_amt'])?></td>
 			</tr>
 		</table>
 		<?php if($location['Location']['notify_sms'] == 1) { ?>
 			<span class="base orange">You have opted to receive SMS notifications for your orders.</span><br />
 		<?php } ?>
 		<?php if($location['Location']['notify_fax'] == 1) { ?>
 			<span  class="base orange">You have opted to receive Fax notifications for your orders.</span><br />
 		<?php } ?>
 		<?php if($location['Location']['accept_cc'] == 1) { ?>
 			<span  class="base orange">You have opted to accept credit card transactions for your orders.</span><br />
 		<?php } ?>
 		<?php if($location['Location']['per_order_amt'] > 0) { ?>
 			<span class="base green b">Your per order fee for the above options is: $<?=number_format($location['Location']['per_order_amt'], 2)?></span>
 		<?php } ?>
 		
 	</fieldset>
 	
 	<?=$form->create('Location', array('action'=>'subscription_review', 'id'=>'formLocationSubscriptionReview')) ?>
 	<?=$form->hidden('id', array('value'=>$location['Location']['id'])) ?>
 	<?=$form->hidden('order_amt', array('value'=>$location['Location']['per_order_amt'])) ?>
 	<fieldset>
 		<legend>Terms & Conditions</legend>
 		<div class="terms-agreement-div">
 			<p>By checking the agreement checkbox below, you authorize Order A Slice to set up a recurring monthly charge to the credit card 
 			profile you have selected, in the amount of <span class="orange b"><?php echo $number->currency($location['Location']['arb_amt']); ?></span>.</p>
	 		<?php if($location['Location']['per_order_amt'] > 0) { ?>
		 		<p>The order notification options you have selected incur a per transaction fee of <span class="orange b">
		 		$<?php echo number_format($location['Location']['per_order_amt'], 2); ?></span>. These fees accumulate until the end of each month, 
		 		and are billed seperately from your recurring monthly charge. Fees are billed to your default payment profile, on the first of 
		 		each month. By agreeing to these terms, you allow Order A Slice to automatically charge	the monthly accumulated fees to your 
		 		current primary payment profile	on record. A receipt for the transaction will be sent to the email address specified in your main 
		 		<?=$html->link('contact profile', '/advertisers/edit') ?>. You may change the payment profile this monthly amount 
		 		gets charged to at any time in your <?=$html->link('Account Settings', '/advertisers/edit')?> for this restaurant.</p>
	 		<?php } ?>
 		</div>
 		<div id="AgreeCheckboxContent" class="marg-b10"><?=$form->checkbox('agree')?><span class="pad-left10 green b">I agree to the above terms & conditions.</span></div>
 	</fieldset>

	<fieldset>
		<legend>Select a Payment Profile</legend>
		<div class="marg-t5 marg-b5"><?php echo $form->select('advertisers_payment_profile_id', $profiles, null, array(), false); ?></div>
	</fieldset>
	<?php echo $jquery->btn('#', 'ui-icon-circle-check', 'Confirm Subscription', null, null, 'btnLocationSubscriptionReview', '155')?>
	</form>
</div> 