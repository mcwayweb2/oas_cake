<?php echo $jquery->scrFormSubmit('#btnLocationSubscription', '#formLocationSubscription'); ?>
<?php if($location['Location']['active'] != 1) { 
	echo $this->element('error-msg-box', array('msg'=>'NOTE: This restaurant is currently inactive.'));
} ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Manage Subscription</h1></div>
 		<div class="right pad-r5"><h2 class="green"><?=$location['Advertiser']['name']." - ".$location['Location']['name']?></h2></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	 <?php if($location['Location']['active'] == 1) { ?>
 		<p>Welcome to your subscription manager.</p> 
 		<fieldset class="ui-widget-content ui-corner-all">
	 		<legend class="ui-corner-all">Subcription Billing</legend>
			<div class="ui-helper-clearfix">
				<div class="left pad-l10"><h2 class="green">This location is current active</h2></div>
				<div class="right"><?php echo $jquery->btn('/locations/unsubscribe', 'ui-icon-alert', 'Unsubscribe'); ?></div>
			</div>
			<a class="base orange">NOTE: If you cancel a subscription, it will still remain active through the end of the current paid billing month.</p>
	 	</fieldset>			
 	<?php } else { ?>
 		<p>To activate your restaurants' subscription, please fill out the form below.</p>  
 		<?=$form->create('Location', array('action'=>'subscription', 'id'=>'formLocationSubscription')) ?>
 		<?=$form->hidden('id', array('value'=>$location['Location']['id']))?>
		<fieldset class="marg-b10"><legend>Subcription Billing</legend>
			<p>To enable this location to start accepting orders, please fill out the information below to set up your subscription. Our low monthly charge is only $39 / month, and you can cancel it at any time.</p>
			<hr class="marg-t5"/>
			<p>Allow this restaurant to accept credit card transactions from customers. More convenient for your company and your customers. Secure your payment 
			before you start the order. For only an additional $10/month, you can add credit card processing capabilities to your online store. 
			<br /><span class="basefont">(A small per transaction fee will also apply to each credit card order in the amount of $0.50)</span></p>
			<?=$form->checkbox('accept_cc', array('checked'=>'checked')) ?>
			<span class="pad-l10 green b">Allow credit card transactions for this restaurant. <span class="basefont">(Add $10 / month).</span></span>
	 	</fieldset>	
	 	
	 	<fieldset class="marg-b10"><legend>Use Promotion Codes</legend>
	 		<p>To allow this restaurant to use and accept promotion codes, check the box below. Promotion codes are an excellent way to expand your business, by 
	 		significantly boosting traffic to your restaurant's online menu. By giving out promotion codes, you not only attract more new customers, but you also 
	 		encourage your existing customers to order more frequently from your restaurant!</p>
	 		<?=$form->checkbox('accept_discount_codes', array('checked'=>'checked')) ?>
			<span class="pad-l10 green b">Allow Promotion Codes to be used with this restaurant. <span class="basefont">(Add $10 / month).</span></span>
	 	</fieldset>
	 	
	 	
	 	<p>When each online order comes in for your store, the restaurant can be notified in a variety of ways. First an email is sent to the 
	 	location contacts email address; the order also appears in your Order A Slice online account. We recommend using your online account for viewing 
	 	orders as they come in, to take advantage of the convenient features the website offers it clients, such as instant step by step delivery directions to each delivery customer.</p>
	 	<fieldset class="marg-b10">
	 		<legend>Order Notification Options</legend>
	 		<p>We also offer a few additional advanced notification options, which we highly recommend signing up for. These options are integrated through 3rd party companies, so they charge just a few pennies per order, and are billed to you monthly.</p> 
			<hr />
			<?=$form->checkbox('notify_fax', array('checked'=>'checked')) ?>
			<span class="pad-l5 green b">Send complete orders to fax machine instantly.<span class="pad-l5 base">(This option costs 14 cents per order sent)</span></span><br />
			<?=$form->checkbox('notify_sms', array('checked'=>'checked')) ?>
			<span class="pad-l5 green b">Send order notification texts to your cell phone.<span class="pad-l5 base">(This option costs 9 cents per order sent)</span></span>
			
			<div class="ui-helper-clearfix marg-t20">
				<div class="left pad-r10 orange b">Powered By:</div>
				<div class="left pad-r10"><?php echo $html->image('efax-logo.gif', array('style'=>'width:100px;', 'class'=>'bordered-logo ui-corner-all')); ?></div>
				<div class="left"><?php echo $html->image('clickatell-logo1.gif', array('style'=>'width:100px;', 'class'=>'bordered-logo ui-corner-all')); ?></div>
			</div>
	 	</fieldset>
	 	<?php echo $jquery->btn('#', 'ui-icon-search', 'Review Subscription', null, null, 'btnLocationSubscription', '150')?>
	 	<?php echo $form->end(); ?>
	<?php } ?>
</div>