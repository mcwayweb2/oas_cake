<?php //echo $html->script(array('jquery/uploadify/swfobject', 'jquery/uploadify/jquery.uploadify.v2.1.0'), false); ?>
<?php //echo $html->css(array('../js/jquery/uploadify/uploadify'), false); ?>
<script>
$(function() {
	$('#AdExplanation').hide();
	$('#HomeAdImage, #SubAdImage, #BtnAdImage').uniform();
	$('#LocationAdvertisementTabs').tabs();

	$('#btnAddHomeAd').click(function() {
		var success = true; 
		$('#HomeAdTitleText, #HomeAdImageText').empty();
		if($('#HomeAdTitle').val() == false) {
			$('#HomeAdTitleText').text('Please enter a title for the advertisement!');
			$('#HomeAdTitle').addClass('ui-state-error'); success = false;
		} 
		if($('#HomeAdImage').val() == false) {
			$('#HomeAdImageText').text('Please select an image to upload!');
			$('#HomeAdImage').addClass('ui-state-error'); success = false;
		} 
		if(success){ $('#formAddHomeAd').submit(); }
		return false;
	});

	$('#btnAddSubAd').click(function() {
		var success = true; 
		$('#SubAdTitleText, #SubAdImageText').empty();
		if($('#SubAdTitle').val() == false) {
			$('#SubAdTitleText').text('Please enter a title for the advertisement!');
			$('#SubAdTitle').addClass('ui-state-error'); success = false;
		} 
		if($('#SubAdImage').val() == false) {
			$('#SubAdImageText').text('Please select an image to upload!');
			$('#SubAdImage').addClass('ui-state-error'); success = false;
		} 
		if(success){ $('#formAddSubAd').submit(); }
		return false;
	});

	$('#btnAddBtnAd').click(function() {
		var success = true; 
		$('#BtnAdTitleText, #BtnAdImageText').empty();
		if($('#BtnAdTitle').val() == false) {
			$('#BtnAdTitleText').text('Please enter a title for the advertisement!');
			$('#BtnAdTitle').addClass('ui-state-error'); success = false;
		} 
		if($('#BtnAdImage').val() == false) {
			$('#BtnAdImageText').text('Please select an image to upload!');
			$('#BtnAdImage').addClass('ui-state-error'); success = false;
		} 
		if(success){ $('#formAddBtnAd').submit(); }
		return false;
	});
	
	$('#AdExplanationLink').toggle(function() {
		$('#AdExplanation').show();
		$('#AdExplanationLink').html('Hide this explanation...');	
	}, function() {
		$('#AdExplanation').hide();
		$('#AdExplanationLink').html('What is the difference between the advertisement sizes?');
	});
	
	/*
	$('#HomeAdImage').uploadify({
		'uploader'  : '/js/jquery/uploadify/uploadify.swf',
		'script'    : '/js/jquery/uploadify/uploadify.php',
		'cancelImg' : '/js/jquery/uploadify/cancel.png',
		'auto'      : true,
		'multi'	    : false,
		'folder'    : '/files',
		'onComplete': function(event, queueID, fileObj, response, data) {
            window.location = "<?php echo $html->url("/locations/process_upload/HomeAd/")?>" + fileObj["name"];
        }}
    );
    */
});

</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Advertisement Manager</h1><span class="green pad-t10"><?php echo $loc['Location']['name']; ?></span></div>
 		<div class="right pad-r5"><?php echo $jquery->btn('/locations/custom_ad', 'ui-icon-image', 'Have Us Design Your Image Ad!'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<p style="margin-bottom:0px;">Welcome to your restaurant advertisement manager. Do you really want that extra edge, <span class="b orange">to stand 
	out</span> to our customers? Place advertisements for your business on our site, to <span class="b green">promote even more new traffic to your 
	restaurant.</span> We offer a variety of very affordable advertising options, that will really give your restaurant the marketing edge it needs, 
	<span class="b orange">to stand out</span> to the most website traffic.</p>
	<div class="txt-r marg-b5" style="width:100%;">
		<?php echo $html->link('What is the difference between the advertisement sizes?', '#', array('id'=>'AdExplanationLink', 'class'=>'green b'));?>
	</div>
	<div id="AdExplanation" class="ui-widget-content ui-corner-all marg-b10">
		<ul class="list-tomato">
			<li><span class="b green">Home Page Ad Space:</span> This is our most popular advertising option. It appears on the lower left of 
			about a dozen, of our most widely customer visited pages. <span class="b">(Dimensions: 320 x 250)</span></li>
			<li><span class="b green">Sub Page Ad Space:</span> This second option appears on about a half dozen of our most widely customer 
			visited pages. <span class="b">(Dimensions: 120 x 240)</span></li>
			<li><span class="b green">Button Ad Space:</span> These advertisements appear on the same website pages as the "Sub Page 
			Advertisements", however they are only about half the size. <span class="b">(Dimensions: 120 x 90)</span></li>
		</ul>
		<hr class="orange"/>
		<p>These advertisements can contain anything to promote your restaurant. Use our upload tool below, to upload you own advertisement images, 
		or let our excellent graphic design team <?=$html->link('Create a Custom Ad', '/') ?> for you. You may upload & store as many advertisements 
		as you would like, for use in rotation of your advertising campaign. Advertisement Subscriptions are billed to your account on a monthly basis, 
		and start at just $29 / month. You may purchase up to 6 subscriptions for each type.<br /> 
		<span class="base b">(NOTE: We reserve the right to use our discretion to remove any ads we feel are vulgar, obscene in any way, or 
		otherwise not related to your restaurant, or the restaurant industry.)</span></p>
	</div>

	<div id="LocationAdvertisementTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabHome">Home Page Ads</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#tabSub">Sub Page Ads</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#tabBtn">Button Ads</a></li>
		</ul>
		<div id="tabHome" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="ui-helper-clearfix">
				<div class="left"><h2>Home Page Advertisements</h2></div>
				<div class="right">
					<?php echo $this->element('warning-msg-box', array('msg'=>'Using '.$rotation_count['Home'].' of '.$loc['Location']['home_ads'].' Home Page Subscriptions.'));	?>
				</div>
			</div>
			
			<fieldset class="ui-widget-content ui-corner-all marg-b10">
				<legend class="ui-corner-all">Upload New Home Page Advertisement</legend>
				<?=$form->create('LocationsAd', array('action'=>'add', 'type'=>'file', 'id'=>'formAddHomeAd')) ?>
				<?php echo $form->hidden('location_id', array('value'=>$loc['Location']['id'])); ?>
				<?php echo $form->hidden('type', array('value'=>'Home')); ?>
				<table class="form-table">
					<tr>
						<td class="label-req">Image Title: <span class="required">*</span></td>
						<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false, 'div'=>false, 'id'=>'HomeAdTitle'))?><br />
						<span class="base b red" id="HomeAdTitleText"></span></td>
					</tr>
					<tr>
						<td class="label-req">Ad Image: <span class="required">*</span></td>
						<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false, 'id'=>'HomeAdImage'))?><br />
						<span class="base b red" id="HomeAdImageText"></span></td>
					</tr>
					<tr>
						<td></td>
						<td><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-n', 'Upload Advertisement', null, null, 'btnAddHomeAd', '160')?></td>
					</tr>
				</table>
				</form>
			</fieldset>
			
			<fieldset class="ui-widget-content ui-corner-all"><legend class="ui-corner-all">Home Page Advertisements</legend>
				<?php 
				if(sizeof($ads['Home']) > 0) {
					?><div class="ui-helper-clearfix marg-t10"><?php
					foreach($ads['Home'] as $a): ?>
						<div class="left pad-r10">
							<div class="ui-helper-clearfix">
								<div class="left b green"><?php echo $text->truncate($a['LocationsAd']['title'], 25); ?></div>
								<div class="right"><?php 
									echo $jquery->btn('/locations_ads/delete/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', '', 'Are you sure that you would like to completely delete this advertisement from the website?', 
											  		  null, null, '25', null, 'Click to Delete this Advertisement');
								?></div>
							</div>
							<?php echo $html->image('../files/ads/home/'.$a['LocationsAd']['image'], 
													array('class'=>'orange-border pad2 marg-b5', 'style'=>'width:160px; height:125px;'));
													 
							if($a['LocationsAd']['rotation'] == 1) {
								//currently in rotation
								echo $this->element('warning-msg-box', array('msg' => 'Currently in Rotation.'));
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', 'Remove from Rotation', 'Are you sure that you would like to remove this advertisement from live rotation?', 
												  'classToggleRotation', null, '160', null, 'Click to Remove This Ad from Rotation');
							} else if($rotation_count['Home'] < $loc['Location']['home_ads']) {
								//only display the button if they are not at ad type limit
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-check', 
												  'Put Ad in Rotation', null, 'classToggleRotation', null, '135', null, 'Click to Put This Ad Into Rotation');
							}
							?>
						</div><?php 
					endforeach;	
					?></div><?php
				} else {
					?><span class="b green">You currently have no Home Page Advertisements stored.</span><?php 
				}
				?>
			</fieldset>
		</div>
		
		<div id="tabSub" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="ui-helper-clearfix">
				<div class="left"><h2>Sub Page Advertisements</h2></div>
				<div class="right">
					<?php echo $this->element('warning-msg-box', array('msg'=>'Using '.$rotation_count['Sub'].' of '.$loc['Location']['sub_ads'].' Sub Page Subscriptions.'));	?>
				</div>
			</div>
			
			<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Upload New Sub Page Advertisement</legend>
				<?=$form->create('LocationsAd', array('action'=>'add', 'type'=>'file', 'id'=>'formAddSubAd')) ?>
				<?php echo $form->hidden('location_id', array('value'=>$loc['Location']['id'])); ?>
				<?php echo $form->hidden('type', array('value'=>'Sub')); ?>
				<table class="form-table">
					<tr>
						<td class="label-req">Image Title: <span class="required">*</span></td>
						<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false, 'div'=>false, 'id'=>'SubAdTitle'))?><br />
						<span class="base b red" id="SubAdTitleText"></span></td>
					</tr>
					<tr>
						<td class="label-req">Ad Image: <span class="required">*</span></td>
						<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false, 'id'=>'SubAdImage'))?><br />
						<span class="base b red" id="SubAdImageText"></span></td>
					</tr>
					<tr>
						<td></td>
						<td><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-n', 'Upload Advertisement', null, null, 'btnAddSubAd', '160')?></td>
					</tr>
				</table>
				</form>
			</fieldset>
			
			<fieldset class="ui-widget-content ui-corner-all"><legend class="ui-corner-all">Sub Page Advertisements</legend>
				<?php 
				if(sizeof($ads['Sub']) > 0) {
					?><div class="ui-helper-clearfix marg-t10"><?php
					foreach($ads['Sub'] as $a): ?>
						<div class="left pad-r10">
							<div class="ui-helper-clearfix">
								<div class="left b green"><?php echo $text->truncate($a['LocationsAd']['title'], 25); ?></div>
								<div class="right"><?php 
									echo $jquery->btn('/locations_ads/delete/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', '', 'Are you sure that you would like to completely delete this advertisement from the website?', 
											  		  null, null, '25', null, 'Click to Delete this Advertisement');
								?></div>
							</div>
							<?php echo $html->image('../files/ads/sub/'.$a['LocationsAd']['image'], 
													array('class'=>'orange-border pad2 marg-b5', 'style'=>'width:160px; height:320px;'));
													 
							if($a['LocationsAd']['rotation'] == 1) {
								//currently in rotation
								echo $this->element('warning-msg-box', array('msg' => 'Currently in Rotation.'));
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', 'Remove from Rotation', 'Are you sure that you would like to remove this advertisement from live rotation?', 
												  'classToggleRotation', null, '160', null, 'Click to Remove This Ad from Rotation');
							} else if($rotation_count['Sub'] < $loc['Location']['sub_ads']){
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-check', 
												  'Put Ad in Rotation', null, 'classToggleRotation', null, '135', null, 'Click to Put This Ad Into Rotation');
							}
							?>
						</div><?php 
					endforeach;	
					?></div><?php
				} else {
					?><span class="b green">You currently have no Sub Page Advertisements stored.</span><?php 
				}
				?>
			</fieldset>
		</div>
		<div id="tabBtn" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="ui-helper-clearfix">
				<div class="left"><h2>Button Advertisements</h2></div>
				<div class="right">
					<?php echo $this->element('warning-msg-box', array('msg'=>'Using '.$rotation_count['Btn'].' of '.$loc['Location']['btn_ads'].' Button Subscriptions.'));	?>
				</div>
			</div>
			
			<fieldset class="ui-widget-content ui-corner-all marg-b10">
				<legend class="ui-corner-all">Upload New Button Advertisement</legend>
				<?=$form->create('LocationsAd', array('action'=>'add', 'type'=>'file', 'id'=>'formAddBtnAd')) ?>
				<?php echo $form->hidden('location_id', array('value'=>$loc['Location']['id'])); ?>
				<?php echo $form->hidden('type', array('value'=>'Btn')); ?>
				<table class="form-table">
					<tr>
						<td class="label-req">Image Title: <span class="required">*</span></td>
						<td><?=$form->input('title',array("class"=>"form-text", 'label'=>false, 'div'=>false, 'id'=>'BtnAdTitle'))?><br />
						<span class="base b red" id="BtnAdTitleText"></span></td>
					</tr>
					<tr>
						<td class="label-req">Ad Image: <span class="required">*</span></td>
						<td><?=$form->file('image',array("class"=>"form-text", 'label'=>false, 'id'=>'BtnAdImage'))?><br />
						<span class="base b red" id="BtnAdImageText"></span></td>
					</tr>
					<tr>
						<td></td>
						<td><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-n', 'Upload Advertisement', null, null, 'btnAddBtnAd', '160')?></td>
					</tr>
				</table>
				</form>
			</fieldset>
			
			<fieldset class="ui-widget-content ui-corner-all"><legend class="ui-corner-all">Button Page Advertisements</legend>
				<?php 
				if(sizeof($ads['Btn']) > 0) {
					?><div class="ui-helper-clearfix marg-t10"><?php
					foreach($ads['Btn'] as $a): ?>
						<div class="left pad-r10">
							<div class="ui-helper-clearfix">
								<div class="left b green"><?php echo $text->truncate($a['LocationsAd']['title'], 25); ?></div>
								<div class="right"><?php 
									echo $jquery->btn('/locations_ads/delete/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', '', 'Are you sure that you would like to completely delete this advertisement from the website?', 
											  		  null, null, '25', null, 'Click to Delete this Advertisement');
								?></div>
							</div>
							<?php echo $html->image('../files/ads/btn/'.$a['LocationsAd']['image'], 
													array('class'=>'orange-border pad2 marg-b5', 'style'=>'width:160px; height:120px;'));
													 
							if($a['LocationsAd']['rotation'] == 1) {
								//currently in rotation
								echo $this->element('warning-msg-box', array('msg' => 'Currently in Rotation.'));
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-close', 'Remove from Rotation', 'Are you sure that you would like to remove this advertisement from live rotation?', 
												  'classToggleRotation', null, '160', null, 'Click to Remove This Ad from Rotation');
							} else if($rotation_count['Btn'] < $loc['Location']['btn_ads']){
								echo $jquery->btn('/locations_ads/toggle_rotation/'.$a['LocationsAd']['id'], 'ui-icon-circle-check', 
												  'Put Ad in Rotation', null, 'classToggleRotation', null, '135', null, 'Click to Put This Ad Into Rotation');
							}
							?>
						</div><?php 
					endforeach;	
					?></div><?php
				} else {
					?><span class="b green">You currently have no Button Advertisements stored.</span><?php 
				}
				?>
			</fieldset>
		</div>
	</div>
</div>
	<!--  	
<?php //echo $form->select('adspaces_home', $options, 1, array('class'=>'form-select'), false) ?> 
	 		 Home page ad space subscriptions.(<span class="basefont">$29/month, each subscription.</span>)<br />
	 		 <?php //echo $form->select('adspaces', $options, 1, array('class'=>'form-select'), false) ?> 
	 		 Sub page ad space subscriptions.(<span class="basefont">$19/month, each subscription.</span>) -->
