<script type="text/javascript">
	$(function() {
		$('.ajax-loading').hide();
		$('#menuTabs').tabs();

		$('#catsAccordion').accordion({
			autoHeight: false
		});

		$('.no-display').removeAttr('style');

		$('.btn-menu-add-special').click( function () {
			$('#ajax-loading2').show();
			var specialIdParts = $(this).attr('id').split('-');
			$('#ajaxOrderSpecialContent').load($(this).attr('href'), { id: specialIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('#ajax-loading2').hide();
			});
			return false;
		});

		$('.btn-menu-add-item').click( function () {
			$('#ajax-loading3').show();
			var itemIdParts = $(this).attr('id').split('-');
			$('#ajaxOrderItemContent').load($(this).attr('href'), { id: itemIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('#ajax-loading3').hide();
			});
			return false;
		});

		$('#btnBuildaPizza').click( function () { 
			$('#ajax-loading4').show();
			$('#ajaxOrderPizzaContent').load($(this).attr('href'), null, function() {
				$('#ajaxOrderSpecialContent').empty();
				$('#btnBuildaPizza, #ajax-loading4').hide();
			});
			return false;
		});

		$('.btn-menu-add-combo').click( function () {
			$('#ajax-loading').show();
			var comboIdParts = $(this).attr('id').split('-');
			$('#ajaxOrderComboContent').load($(this).attr('href'), { id: comboIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('#ajax-loading').hide();
			});
			return false;
		});
	});
</script>

<div id="menuTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#mnuCats">Menu Categories (<?php echo sizeof($cats); ?>)</a></li>
		<?php if(isset($combos) && sizeof($combos) > 0) { ?>
			<li class="ui-state-default ui-corner-top"><a href="#mnuCombos">Combo Specials (<?php echo sizeof($combos); ?>)</a></li>
		<?php } ?>
		<?php if(isset($specials) && sizeof($specials) > 0) { ?>
			<li class="ui-state-default ui-corner-top"><a href="#mnuPizzas">Specialty Pizzas (<?php echo sizeof($specials); ?>)</a></li>
		<?php } ?>
		<?php if($location['Location']['offer_pizza'] == '1') { ?>
			<li class="ui-state-default ui-corner-top"><a href="#mnuBuildaPizza">Build Your Own Pizza</a></li>
		<?php } ?>
	</ul>
	<div id="mnuCats" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<div id="ajaxOrderItemContent"></div>
		<div class="txt-c ajax-loading" id="ajax-loading3"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
	
	
		<div class="b green marg-b5">Click on a category title or description to expand the category.</div>
		
		<div class="ui-widget ui-accordion no-display" id="catsAccordion" style="display:none;">
			<?php foreach($cats as $cat): ?>
			<?php if(sizeof($cat['Items']) > 0) { ?>
				<h2 class="ui-widget-header ui-accordion-header" id="cat-header-<?php echo $cat['Cat']['id']; ?>">
	         		<a href="#">
	         			<div class="ui-helper-clearfix">
	         				<div class="left pad-r10 orange"><?php echo $cat['Cat']['title'].' ('.sizeof($cat['Items']).')'; ?> </div>
	         				<div class="left base"><?php echo $cat['LocationsCat']['subtext']; ?></div>
	         			</div>
	         		</a>
	         	</h2>
	         	<div class="ui-widget-content ui-accordion-content" id="cat-drawer-<?php echo $cat['Cat']['id']; ?>">
					<div class="gray-border marg-b10 pad3">
	         		<table class="list-table ui-table">
					<?php 
					//list all menu items in that category
					foreach($cat['Items'] as $item): ?>
						<tr>				
							<?php if($location['Location']['show_menu_images'] == '1') { ?>
								<td style="width:100px;">
									<?
									echo $html->image($misc->calcImagePath($item['Item']['image'], 'files/'), 
													  array('class'=>'tbl-photo orange pad2')); ?>
								</td>
								<td class="valign-t" style="width:220px;">
									<span class="orange"><?=$item['Item']['title']?></span>
									<?php if(!empty($item['Item']['description'])) { ?>
										<br /><span class="green base"><?=$item['Item']['description']?></span>
									<?php } ?>
								</td>
							<?php } else { ?>
								<td colspan="2" style="width:320px; padding-left:5px;">
									<span class="orange"><?=$item['Item']['title']?></span>
									<?php if(!empty($item['Item']['description'])) { ?>
										<br /><span class="green base"><?=$item['Item']['description']?></span>
									<?php } ?>
								</td>
							
							<?php } ?>
				
							
							<td><?php
								if(sizeof($item['ItemsOption']) > 0) {
									//we have multiple pricing options
									?><div class="base orange">Starting at:</div><?php 
								} 
								echo "$".number_format($item['Item']['price'], 2);
							?>
							</td>
							<td style="width:115px;"><?php
								if($session->check('User.order_id')) {
									echo $misc->jqueryBtn('/orders_items/add/'.$item['Item']['id'], 'ui-icon-circle-plus', 'Add To Order',
														  null, 'btn-menu-add-item', 'menu_item-'.$item['Item']['id']); 
								}
								?>
							</td>	
						</tr>
						<?php
					endforeach; ?>
					</table>
					</div>
				</div> <!-- end accordion drawer div -->
			<?php } ?>
		<?php endforeach; ?>
	</div> <!-- end catsAccordion -->
	</div>
	
	<?php if(isset($combos) && sizeof($combos) > 0) { ?>
	<div id="mnuCombos" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
		<div id="ajaxOrderComboContent"></div>
		<div id="ajax-loading" class="txt-c ajax-loading"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
		
		<div class="gray-border marg-b10 ">
			<table class="list-table ui-table">
				<tr><th></th>
					<th>Current Combos</th>
					<th>Price</th>
					<th></th>
				</tr>
				<?php 
				echo $this->element('table_accent_row', array('cols'=>'4'));
				
				foreach($combos as $combo): ?>
					<tr>
						<?php if($location['Location']['show_menu_images'] == '1') { ?>
							<td style="width:100px;"><?php echo $html->image($misc->calcImagePath($combo['Combo']['image'], 'files/combos/'), 
														  				  array('class'=>'tbl-photo pad2 orange'));?></td>
							<td>
								<span class="orange"><?=$combo['Combo']['title']?></span><br />
								<span class="basefont green"><?=$combo['Combo']['description']?></span>
							</td>
						<?php } else { ?>
							<td colspan="2">
								<span class="orange pad-l10"><?=$combo['Combo']['title']?></span><br />
								<span class="basefont green pad-l10"><?=$combo['Combo']['description']?></span>
							</td>
						<?php } ?>
						
						<td style="width:60px;"><?php echo $number->currency($combo['Combo']['price']); ?></td>
						<td style="width:115px;"><?php 
							if(isset($order)) {
								echo $misc->jqueryBtn('/orders_combos/add/'.$combo['Combo']['id'], 'ui-icon-circle-plus', 'Add To Order', 
													  null, 'btn-menu-add-combo', 'menu_combo-'.$combo['Combo']['id']);  
							}
							?>
						</td>
					</tr><?php 
				endforeach;	
				
				echo $this->element('table_accent_row', array('cols'=>'4')); ?>
			</table>
		</div>
	</div>
	<?php } ?>
	
	<?php if(isset($specials) && sizeof($specials) > 0) { ?>
		<div id="mnuPizzas" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<div id="ajaxOrderSpecialContent"></div>
			<div id="ajax-loading2" class="txt-c ajax-loading"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
			<div id="orderContentTab1">
				
			<div id="orderSpecialContent">
				<div class="gray-border marg-b10">
					<table class="list-table ui-table">
						<tr>
							<th colspan="2"></th>
							<th colspan="2"><span style="padding-left:45px;">Available Sizes</span></th>
						</tr>
						<?php echo $this->element('table_accent_row', array('cols' => '4')); ?>
						<?php foreach($specials as $special): ?>
							<tr>
								<?php if($location['Location']['show_menu_images'] == '1') { ?>
									<td style="width:100px;"><?php echo $html->image($misc->calcImagePath($special['Special']['image'], 'files/specialtyPizzas/'), 
																  				  array('class'=>'tbl-photo pad2 orange'));?></td>
									<td style="width:190px;">
										<span class="orange"><?=$special['Special']['title']?></span><br />
										<span class="basefont green"><?=$special['Special']['subtext']?></span>
									</td>
								<?php } else { ?>
									<td style="width:290px; padding:5px 0px 5px 10px;" colspan="2">
										<span class="orange"><?=$special['Special']['title']?></span><br />
										<span class="basefont green"><?=$special['Special']['subtext']?></span>
									</td>
								<?php } ?>
								
								<td>
								<?php foreach($special['Special']['SpecialsSize'] as $size): ?>
									<div class="ui-helper-clearfix base">
										<div class="left txt-r" style="width:80px;"><?php echo $size['Size']['title']; ?></div>
										<div class="left pad-l20"><?php echo $number->currency($size['SpecialsSize']['price']); ?></div>
									</div>
								<?php endforeach; ?>
								</td>
								<td style="width:115px;"><?php 
									if(isset($order)) {
										echo $misc->jqueryBtn('/orders_specials_sizes/add/'.$special['Special']['id'], 'ui-icon-circle-plus', 'Add To Order', 
															  null, 'btn-menu-add-special', 'menu_special-'.$special['Special']['id']);  
									}
								?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
			
			
			</div> <!-- end menuContent div -->
		</div>
	<?php } ?>
	<?php if($location['Location']['offer_pizza'] == '1') { ?>
		<div id="mnuBuildaPizza" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<div id="orderPizzaContent">
				<div class="ui-helper-clearfix">
					<div class="right">
						<?php
						if(isset($order)) {
							?><div style="width:170px;"><?php echo $misc->jqueryBtn('/pizzas/add', 'ui-icon-wrench', 'Start Building A Pizza...', null, null, 'btnBuildaPizza'); ?></div><?php 	
						}?>
					</div>
				</div>
				<div id="ajax-loading4" class="txt-c ajax-loading"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
				
				<div class="gray-border pad10">
					<div class="ui-helper-clearfix marg-b10">
						<div class="left" style="width:150px;">
							<?=$html->image('build-a-pizza2.jpg', array('style'=>'width:130px;', 'class'=>'ui-corner-all gray-border'))?>
						</div>
						<div class="left gray-border" style="width:390px;">
							<table class="list-table ui-table">
								<tr>
									<th style="width:150px;"></th>
									<th>Cheese Price</th>
									<th>Extra Toppings</th>
								</tr>
								<?php echo $this->element('table_accent_row', array('cols'=>'4'))?>
								
								<?php foreach ($sizes as $size): ?>
									<tr>
										<td class="b green"><div class="pad-l5"><?php echo $size['Size']['size']."\" ".$size['Size']['title']; ?></div></td>
										<td>$<?php echo number_format($size['Size']['base_price'], 2); ?></td>
										<td>$<?php echo number_format($size['Size']['topping_price'], 2); ?></td>
									</tr>
								<?php endforeach; ?>
								<?php echo $this->element('table_accent_row', array('cols'=>'4'))?>
							</table>
						</div>
					</div>
				</div><!-- end build your own pizza div -->
				</div><!-- end orderPizzaContent div -->
				<div id="ajaxOrderPizzaContent"></div>
		</div>
	<?php } ?>
</div>