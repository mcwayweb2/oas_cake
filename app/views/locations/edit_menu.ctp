<?php echo $html->script(array('jquery/alphanumeric'), false); ?>
<script type="text/javascript">
	$(function() {
		$('.ajax-loading, .ajax-loading2, #ajaxAddNewCrustForm, #ajaxAddNewToppingForm, #ajaxAddNewCatForm').hide();
		$('#CrustTitle, #ToppingTitle').alpha({ allow: " " });
		$('#menuTabs, #managePizzaSettingsTabs').tabs();
		$('#catsAccordion').accordion({
			autoHeight: false
		});

		$('.showNewCrustForm').click( function () { 
			$('html, body').animate( { scrollTop: 0 }, 'fast' );
			$('.ajax-loading').show();
			$('#ajaxAddExistingCrustForm').hide();
			$('#ajaxAddNewCrustForm').show();
			$('.ajax-loading').hide();
			return false;
		});

		$('#showMyCrustForm').click( function () {
			$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
			$('.ajax-loading').show();
			$('#ajaxAddExistingCrustForm').show();
			$('#ajaxAddNewCrustForm, .ajax-loading').hide();
			return false;
		});

		$('#btnMenuAddNewCrust').click( function () {
			if($('#CrustTitle').val() == '') {
				$('#CrustTitleText').html('Please enter a crust title!');
				$('#CrustTitle').addClass('ui-state-highlight');
			} else {
				$('#formMenuAddNewCrust').submit();
			}
			return false;
		});

		$('.btn-menu-edit-crust').click( function () {
			$('.ajax-loading').show();
			var crustIdParts = $(this).attr('id').split('-');
			$('#crustFormFieldset').load($(this).attr('href'), { id: crustIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading').hide();
			});
			return false;
		});

		$('.showNewToppingForm').click( function () { 
			$('html, body').animate( { scrollTop: 0 }, 'fast' );
			$('.ajax-loading').show();
			$('#ajaxAddExistingToppingForm').hide();
			$('#ajaxAddNewToppingForm').show();
			$('.ajax-loading').hide();
			return false;
		});

		$('#showMyToppingForm').click( function () {
			$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
			$('.ajax-loading').show();
			$('#ajaxAddExistingToppingForm').show();
			$('#ajaxAddNewToppingForm, .ajax-loading').hide();
			return false;
		});

		$('#btnMenuAddNewTopping').click( function () {
			if($('#ToppingTitle').val() == '') {
				$('#ToppingTitleText').html('Please enter a topping title!');
				$('#ToppingTitle').addClass('ui-state-highlight');
			} else {
				$('#formMenuAddNewTopping').submit();
			}
			return false;
		});

		$('.btn-menu-edit-topping').click( function () {
			$('.ajax-loading').show();
			var toppingIdParts = $(this).attr('id').split('-');
			$('#toppingFormFieldset').load($(this).attr('href'), { id: toppingIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading').hide();
			});
			return false;
		});
		//-------------------------
		$('#btnToggleCatsDiv').click(function() {
			$('.ajax-loading2').show();
			$('#ajaxManageMenuItemContent').load($(this).attr('href'), null, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading2').hide();
			});
			return false;
		});
		//------------------

		$('.btn-menu-edit-size').click( function () {
			$('.ajax-loading').show();
			var sizeIdParts = $(this).attr('id').split('-');
			$('#ajaxMenuSizeFormContent').load($(this).attr('href'), { id: sizeIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading').hide();
			});
			return false;
		});

		$('#btnToggleSizeForms').toggle( function () {
			//load add size form via ajax, update btn text
			$('.ajax-loading').show();
			$('#ajaxMenuSizeFormContent').load($(this).attr('href'), null, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading').hide();
			});
		}, function () {
			//remove form, restore btn text
			$('#ajaxMenuSizeFormContent').empty();
			$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
		});

		$('#btnMenuAddItem').click( function () {
			$('.ajax-loading2').show();
			
			$('#ajaxManageMenuItemContent').load($(this).attr('href'), null, function() {
				$('html, body').animate( { scrollTop: 220 }, 'fast' );
				$('.ajax-loading2').hide();
			});
			return false;
		});

		$('.btn-menu-edit-item').click( function () {
			$('.ajax-loading2').show();
			var itemIdParts = $(this).attr('id').split('-');
			$('#ajaxManageMenuItemContent').load($(this).attr('href'), { id: itemIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 220 }, 'fast' );
				$('.ajax-loading2').hide();
			});
			return false;
		});

		//hide current add topping form, show multiple add topping form
		$('#toppingManageToolLink').click(function() { 
			$('.ajax-loading').show();
			$('#toppingFormFieldset').hide();
			
			$('#toppingFormFieldset2').load($(this).attr('href'), null, function() {
				$('.ajax-loading').hide();
			});
			return false;
		});

		$('.sort-list').sortable({ 
			containment: 'parent',
			update: function() {
				$('#ajaxItemSortResult').load('/items/ajax_order', $(this).sortable('serialize'));
			} 
		});

		$('.combo-sort-list').sortable({ 
			containment: 'parent',
			update: function() {
				$('#ajaxComboSortResult').load('/combos/ajax_order', $(this).sortable('serialize'));
			} 
		});
	});
</script>
<?php //echo $this->element('advertisers/menu_warnings'); ?>

<?php if($location['Location']['offer_pizza'] == '1') { ?>
<?php echo $jquery->scrFormSubmit('#btnMenuAddCrust', '#formMenuAddCrust'); ?>
<?php echo $jquery->scrFormSubmit('#btnMenuAddTopping', '#formMenuAddTopping'); ?>

<div id="page-loader" style="padding-left:250px; padding-top:100px; "><?php echo $html->image('ajax-loader-lg-circle.gif');?></div>
<div id="content-wrapper" style="display:none !important; ">

<div id="managePizzaSettingsTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a href="#settingSizes"><?php echo (sizeof($sizes) > 0) ? "Sizes and Prices (".sizeof($sizes).")" : "<span class='red'>Sizes and Prices (0)</span>"; ?></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#settingCrusts"><?php echo (sizeof($crusts) > 0) ? "Crust Types (".sizeof($crusts).")" : "<span class='red'>Crust Types (0)</span>"; ?></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#settingToppings"><?php echo (sizeof($toppings) > 0) ? "Toppings (".sizeof($toppings).")" : "<span class='red'>Toppings (0)</span>"; ?></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#mnuPizzas"><?php echo (sizeof($specials) > 0) ? "Specialty Pizzas (".sizeof($specials).")" : "<span class='red'>Specialty Pizzas (0)</span>"; ?></a>
		</li>
	</ul>
	<div id="settingSizes" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<div class="ajax-loading txt-c" style="display:none;"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
		<div id="ajaxMenuSizeFormContent"></div>
	
		<div class="ui-helper-clearfix">
			<div class="left"><h3 class="orange lg pad-l20">My Offered Pizza Sizes</h3></div>
			<div class="right"><?php echo $jquery->btn('/sizes/add/'.$location['Location']['id'], 'ui-icon-circle-plus', 'Add a New Pizza Size', null, null, 'btnToggleSizeForms', '155')?></div>	
		</div>
		<div class="gray-border">
			<table class="list-table ui-table">
				<tr>
					<th style="width:150px;"></th>
					<th>Cheese<br />Price</th>
					<th>Topping<br />Price</th>
					<th>Extra<br />Cheese</th>
					<th>Extra<br />Sauce</th>
					<th colspan="2"></th>
				</tr>
				<?php echo $this->element('table_accent_row', array('cols' => '7')); ?>
				<?php if(sizeof($sizes) > 0) { ?>
					<?php foreach ($sizes as $size): ?>
						<tr>
							<td class="b green"><div class="pad-l5"><?php echo $size['Size']['size']."\" ".$size['Size']['title']; ?></div></td>
							<td>$<?php echo number_format($size['Size']['base_price'], 2); ?></td>
							<td>$<?php echo number_format($size['Size']['topping_price'], 2); ?></td>
							<td>$<?php echo number_format($size['Size']['extra_cheese'], 2); ?></td>
							<td>$<?php echo number_format($size['Size']['extra_sauce'], 2); ?></td>
							<td style="width:25px;">
							<?php echo $misc->jqueryBtn('/sizes/edit', 'ui-icon-pencil', '', 
																null, 'btn-menu-edit-size', 'menu_size-'.$size['Size']['id'], null, null, 'Edit'); ?>
							</td>
							<td style="width:25px;">
								<?php echo $misc->jqueryBtn('/sizes/delete/'.$size['Size']['id'], 'ui-icon-circle-close', '',
															'Are you sure that you would like to remove this pizza size from your menu? NOTE: Removing this pizza size, will also delete the size from any specialty pizzas using it.', 
															null, null, null, null, 'Delete'); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php } else { ?>
					<tr><td colspan="7">
						<div class="txt-c b green">You currently have no pizza sizes set up! Add a New Pizza Size now, using the button above!</div>
					</td></tr>				
				<?php } ?>
				<?php echo $this->element('table_accent_row', array('cols' => '7'))?>
			</table>
		</div>
	</div>
	<div id="settingCrusts" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<div class="ajax-loading txt-c" style="display:none;"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
		<div id="crustFormFieldset">
		<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Add a New Crust Type</legend>
			<div id="ajaxAddExistingCrustForm">
				<p>Select the new crust type you would like to offer from the drop down menu below. If the crust type you would like to add is not 
				listed, you can <?php echo $html->link('enter a new crust type', '#', array('class'=>'showNewCrustForm')); ?> by 
				<?php echo $html->link('clicking here', '#', array('class'=>'showNewCrustForm'));?>.</p>
				
				<?php echo $form->create('LocationsCrust', array('id'=>'formMenuAddCrust', 'action'=>'add')); ?>
				<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
				<table class="form-table">
					<tr>
						<td class="label-req">Crust Type: <span class="required">*</span></td>
						<td><?php echo $form->select('crust_id', $crust_list, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
					</tr>
					<tr>
						<td class="label-req">Price:</td>
						<td><?php echo $form->input('price', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
						<span class="base orange">(Leave empty for no charge.)</span></td>
					</tr>
					<tr><td></td>
					<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Crust', null, null, 'btnMenuAddCrust', '100'); ?></td></tr>
				</table>
				<?php echo $form->end(); ?>
			</div>
			
			<div id="ajaxAddNewCrustForm">
				<div class="ui-helper-clearfix">
					<div class="left"><p>Enter the new crust you would like to add below.</p></div>
					<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-w', 'Hide New Crust Form', null, null, 'showMyCrustForm', '160'); ?></div>
				</div>
				
				<?php echo $form->create('Crust', array('id'=>'formMenuAddNewCrust', 'action'=>'add')); ?>
				<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
				<table class="form-table">
					<tr>
						<td class="label-req">Crust Title: <span class="required">*</span></td>
						<td><?php echo $form->input('title', array('class'=>'form-text', 'label'=>false, 'div'=>false)); ?>
						<br /><span class="base red b" id="CrustTitleText"></span></td>
					</tr>
					<tr><td></td>
					<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Crust', null, null, 'btnMenuAddNewCrust', '100'); ?></td></tr>
				</table>
				<?php echo $form->end(); ?>
			</div>
		</fieldset>
		</div>
		
		
		<h3 class="orange lg pad-l20 marg-b5">My Offered Crust Types</h3>
		<div class="gray-border">
			<table class="list-table ui-table">
				<tr>
					<th style="width:200px;"><div class="pad-l10">Crust</div></th>
					<th>Price</th>
					<th colspan="2"></th>
				</tr>
				<?php echo $this->element('table_accent_row', array('cols' => '4')); ?>
				<?php if(sizeof($crusts) > 0) { ?>
					<?php foreach ($crusts as $crust): ?>
						<tr>
							<td class="b green"><div class="pad-l5"><?php echo $crust['Crust']['title']; ?></div></td>
							<td><?php echo ($crust['LocationsCrust']['price'] > 0) ? '$'.number_format($crust['LocationsCrust']['price'], 2) : 'No Charge'; ?></td>
							<td style="width:65px;">
							<?php echo $misc->jqueryBtn('/locations_crusts/edit/'.$crust['LocationsCrust']['id'], 'ui-icon-pencil', 'Edit', 
														null, 'btn-menu-edit-crust', 'menu_crust-'.$crust['LocationsCrust']['id'], '60'); ?>
							</td>
							<td style="width:85px;">
								<?php echo $misc->jqueryBtn('/locations_crusts/delete/'.$crust['LocationsCrust']['id'], 'ui-icon-circle-close', 'Delete',
															'Are you sure that you would like to remove this crust type from your menu?', null, null, '75'); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php } else { ?>
					<tr><td colspan="4">
						<div class="txt-c b green">You currently have no pizza crusts set up! Add a New Crust Type now, using the form above!</div>
					</td></tr>				
				<?php } ?>
				<?php echo $this->element('table_accent_row', array('cols' => '4'))?>
			</table>
		</div>
	</div>
	<div id="settingToppings" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<div class="ajax-loading txt-c"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
		<div id="toppingFormFieldset">
			<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Add a New Topping</legend>
				<div id="ajaxAddExistingToppingForm">
					<p>Select the new topping you would like to offer from the drop down menu below. If the topping you would like to add is not 
					listed, you can <?php echo $html->link('enter a new topping', '#', array('class'=>'showNewToppingForm')); ?> by 
					<?php echo $html->link('clicking here', '#', array('class'=>'showNewToppingForm'));?>. <span class="b">NOTE:</span> If you add a new topping to our database, 
					you must still use the form below to add the topping to your menu.</p>
					
					<p>If you would like to make a single topping more/less expensive than the rest of your toppings, use the "Price Alter" field to 
					set a customized price for it. (To set a less expensive topping price, use a <span class="red">negative</span> number in the "Price Alter" field.) 
					To edit the base prices for your toppings, visit the 
					<?php echo $html->link('Sizes and Prices', '/restaurants/edit_menu/'.$location['Location']['slug'].'#settingSizes'); ?> tab of your Menu Manager.</p>
					
					<p>If you have more than just a couple of toppings to add, try adding them all at once, using our 
					<?php echo $html->link('Topping Management Tool', '/locations/mass_add_toppings/'.$location['Location']['id'], array('id'=>'toppingManageToolLink')); ?>.</p>
					
					<?php echo $form->create('LocationsTopping', array('id'=>'formMenuAddTopping', 'action'=>'add')); ?>
					<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
					<table class="form-table">
						<tr>
							<td class="label-req">Topping: <span class="required">*</span></td>
							<td><?php echo $form->select('topping_id', $topping_list, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
						</tr>
						<tr>
							<td class="label-req">Label: <span class="required">*</span></td>
							<td><?php echo $form->select('topping_type_id', $topping_types, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?><br />
							<span class="base orange">(Used to group your toppings.)</span></td>
						</tr>
						<tr>
							<td class="label-req">Price Alter:</td>
							<td><?php echo $form->input('price_alter', array('class'=>'form-text', 'label'=>false, 'div'=>false, 'type'=>'text'))?><br />
							<span class="base orange">(Leave empty for no price change.)</span></td>
						</tr>
						<tr><td></td>
						<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Topping', null, null, 'btnMenuAddTopping', '110'); ?></td></tr>
					</table>
					<?php echo $form->end(); ?>
				</div>
				
				<div id="ajaxAddNewToppingForm">
					<div class="ui-helper-clearfix">
						<div class="left"><p>Enter the topping you would like to add below.</p></div>
						<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-w', 'Hide New Topping Form', null, null, 'showMyToppingForm', '180'); ?></div>
					</div>
					
					<?php echo $form->create('Topping', array('id'=>'formMenuAddNewTopping', 'action'=>'add')); ?>
					<?php echo $form->hidden('location_id', array('value'=>$location['Location']['id'])); ?>
					<table class="form-table">
						<tr>
							<td class="label-req">Topping Title: <span class="required">*</span></td>
							<td><?php echo $form->input('title', array('class'=>'form-text', 'label'=>false, 'div'=>false)); ?>
							<br /><span class="base red b" id="ToppingTitleText"></span></td>
						</tr>
						<tr><td></td>
						<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Topping', null, null, 'btnMenuAddNewTopping', '110'); ?></td></tr>
					</table>
					<?php echo $form->end(); ?>
				</div>
			</fieldset>
		</div>
		<div id="toppingFormFieldset2"></div>
		
		<h3 class="orange lg pad-l20 marg-b5">My Offered Toppings</h3>
		<div class="gray-border">
			<table class="list-table ui-table">
				<tr>
					<th style="width:200px;"><div class="pad-l10">Topping</div></th>
					<th>Topping Type</th>
					<th>Price Alter</th>
					<th colspan="2"></th>
				</tr>
				<?php echo $this->element('table_accent_row', array('cols' => '5')); ?>
				<?php if(sizeof($toppings) > 0) { ?>
					<?php foreach ($toppings as $t): ?>
						<tr>
							<td class="b green"><div class="pad-l5"><?php echo $t['Topping']['title']; ?></div></td>
							<td><?php echo $t['ToppingType']['title']; ?></td>
							<td><?php 
								if($t['LocationsTopping']['price_alter'] > 0) {
									echo "<span class='green'>+$".number_format($t['LocationsTopping']['price_alter'], 2)."</span>";
								} else if($t['LocationsTopping']['price_alter'] < 0) {
									echo "<span class='red'>$".number_format($t['LocationsTopping']['price_alter'], 2)."</span>";
								} else {
									echo "None";
								}
								?>		
							</td>
							<td style="width:65px;">
							<?php echo $misc->jqueryBtn('/locations_toppings/edit/'.$t['LocationsTopping']['id'], 'ui-icon-pencil', 'Edit', 
														null, 'btn-menu-edit-topping', 'menu_topping-'.$t['LocationsTopping']['id'], '60'); ?>
							</td>
							<td style="width:85px;">
								<?php echo $misc->jqueryBtn('/locations_toppings/delete/'.$t['LocationsTopping']['id'], 'ui-icon-circle-close', 'Delete',
															'Are you sure that you would like to remove this topping from your menu?', null, null, '75'); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php } else { ?>
					<tr><td colspan="5">
						<div class="txt-c b green">You currently have no pizza toppings set up! Add a New Topping now, using the form above!</div>
					</td></tr>				
				<?php } ?>
				<?php echo $this->element('table_accent_row', array('cols' => '5'))?>
			</table>
		</div>
	</div>
	<div id="mnuPizzas" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<div id="orderContentTab1">
				<div id="orderSpecialContent">
				
				<div class="ui-helper-clearfix">
					<div class="left"><h3 class="orange lg pad-l20">Specialty Pizzas</h3></div>
					<div class="right"><?php echo $jquery->btn('/specials/add/'.$location['Location']['slug'], 'ui-icon-circle-plus', 'Add a Specialty Pizza'); ?></div>
				</div>
				<div class="gray-border marg-b10 ">
				<table class="list-table ui-table">
					<tr>
						<th colspan="2"></th>
						<th colspan="2"><span style="padding-left:80px;">Available Sizes</span></th>
					</tr>
				<?
				echo $this->element('table_accent_row', array('cols' => 4));
				
				if(sizeof($specials) > 0) {
					foreach($specials as $special):
						?>
						<tr>
							<?php if($location['Location']['show_menu_images'] == '1') { ?>
								<td style="width:100px;"><?php echo $html->image($misc->calcImagePath($special['Special']['image'], 'files/specialtyPizzas/'), 
															  				  array('class'=>'tbl-photo pad2 orange'));?></td>
								<td style="width:190px;">
									<span class="orange"><?=$special['Special']['title']?></span><br /><span class="basefont green"><?=$special['Special']['subtext']?></span>
								</td>
							<?php } else { ?>
								<td colspan="2" style="width:290px;" class="pad-l10">
									<div class="pad-l10">
										<span class="orange"><?=$special['Special']['title']?></span><br />
										<span class="basefont green"><?=$special['Special']['subtext']?></span>
									</div>
								</td>
							<?php } ?>
							
							
							<td>
								<?
								foreach($special['Special']['SpecialsSize'] as $size):
									?>
									<div class="ui-helper-clearfix base">
										<div class="left txt-r" style="width:120px;"><?php echo $size['Size']['title']; ?></div>
										<div class="left pad-l20"><?php echo $number->currency($size['SpecialsSize']['price']); ?></div>
									</div>
									<?php
								endforeach;
								?>
							</td>
							<td style="width:75px;">
								<?php echo $misc->jqueryBtn('/specials/edit/'.$special['Special']['id'], 'ui-icon-pencil', 'Edit', 
															null, 'btn-menu-edit-special', 'menu_special-'.$special['Special']['id']); ?>
								<?php echo $misc->jqueryBtn('/specials/delete/'.$special['Special']['id'], 'ui-icon-circle-close', 'Delete',
															'Are you sure that you would like to remove this specialty pizza from your menu?'); ?>
							</td>
						</tr>
						<?
					endforeach;	
				} else { ?>
					<tr><td colspan="4">
						<div class="txt-c b green">You have no specialty pizzas set up! Add a New Specialty Pizza now, using the button above!</div>
					</td></tr>
				<?php } ?></table>
				</div>
			</div>	
		</div> <!-- end menuContent div -->
	</div>
</div>
<?php } ?>

<div class="ajax-loading2 txt-c"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
<div id="ajaxManageMenuItemContent"></div>

<div id="menuTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
	<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
		<li class="ui-state-default ui-corner-top">
			<a href="#mnuCategories"><?php echo (sizeof($cats) > 0) ? "Categories & Items (".sizeof($cats).")" : "<span class='red'>Categories & Items (0)</span>"; ?></a>
		</li>
		<li class="ui-state-default ui-corner-top">
			<a href="#mnuCombos"><?php echo (sizeof($combos) > 0) ? "Combo Specials (".sizeof($combos).")" : "<span class='red'>Combo Specials (0)</span>"; ?></a>
		</li>
	</ul>
	<div id="mnuCategories" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
		<p style="margin-bottom: 0px;">Click on a category title to expand the category. To change the order of your items within a 
		menu category, use the drag handle to left of each item description, and drag and drop the item row into the display order of your 
		preference.</p>
		<div class="ui-helper-clearfix">
			<div class="right pad-l10"><?php echo $jquery->btn('/items/add/'.$location['Location']['id'], 'ui-icon-circle-plus', 'Add Menu Item', null, null, 'btnMenuAddItem'); ?></div>
			<div class="right"><?php echo $jquery->btn('/locations/manage_categories/'.$location['Location']['id'], 'ui-icon-wrench', 
													   'Manage Categories', null, null, 'btnToggleCatsDiv'); ?></div>
		</div>
		<div id="ajaxItemSortResult"></div>
		<?php if(sizeof($cats) > 0) { ?>
		<div class="ui-widget ui-accordion" id="catsAccordion">
			<?php foreach($cats as $cat): ?>
				<h2 class="ui-widget-header ui-accordion-header" id="cat-header-<?php echo $cat['Cat']['id']; ?>">
	         		<a href="#">
	         			<div class="ui-helper-clearfix">
	         				<div class="left pad-r10 orange">
	         					<?php echo (sizeof($cat['Items']) > 0) ? 
	         								$cat['Cat']['title']." (".sizeof($cat['Items']).")" : 
	         								"<span class='red'>".$cat['Cat']['title']." (".sizeof($cat['Items']).")</span>"; ?>
	         				</div>
	         				<div class="left base"><?php echo $cat['LocationsCat']['subtext']; ?></div>
	         			</div>
	         		</a>
	         	</h2>
	         	<div class="ui-widget-content ui-accordion-content" id="cat-drawer-<?php echo $cat['Cat']['id']; ?>">
					<?
					//make sure there is at least one item to display before we display them
					if(sizeof($cat['Items']) > 0) { ?>
						<div class="gray-border marg-b10 pad3 sort-list" id="itemSortList<?php echo $cat['Cat']['id']; ?>">
						<?php 
						//list all menu items in that category
						foreach($cat['Items'] as $item):
							?>
							<div id="itemSortList<?php echo $cat['Cat']['id']; ?>_<?php echo $item['Item']['id']; ?>" class="ui-helper-clearfix marg-b10" style="border-bottom:1px solid #ddd;">
								<?php echo $this->element('js/jquery/drag-handle'); ?>
								<?php if($location['Location']['show_menu_images'] == '1') { ?>
									<div class="left pad-l5" style="width:100px;"><?
										echo $html->image($misc->calcImagePath($item['Item']['image'], 'files/'), 
														  array('class'=>'tbl-photo orange pad2'));
									?>
									</div>
									<div class="left valign-t b pad-r5" style="width:235px;">
										<span class="orange"><?=$item['Item']['title']?></span>
										<?php if(!empty($item['Item']['description'])) { ?>
											<br /><span class="green base"><?=$item['Item']['description']?></span>
										<?php } ?>
									</div>
								<?php } else { ?>
									<div class="left valign-t b pad-l5 pad-r5" style="width:335px;">
										<span class="orange"><?=$item['Item']['title']?></span>
										<?php if(!empty($item['Item']['description'])) { ?>
											<br /><span class="green base"><?=$item['Item']['description']?></span>
										<?php } ?>
									</div>
								<?php } ?>
								<div class="left b dark-gray" style="width:60px;"><?php 
									if(sizeof($item['ItemsOption']) > 0) {
										//we have multiple pricing options
										?><div class="base orange">Starting at:</div><?php 
									} 
									echo "$".number_format($item['Item']['price'], 2);
								?>
								</div>
								<div class="left" style="width:75px;">
									<?php echo $misc->jqueryBtn('/items/edit/'.$item['Item']['id'], 'ui-icon-pencil', 'Edit',
																	null, 'btn-menu-edit-item', 'menu_item-'.$item['Item']['id']); ?>
									<?php echo $misc->jqueryBtn('/items/delete/'.$item['Item']['id'], 'ui-icon-circle-close', 'Delete',
																'Are you sure that you would like to remove this item from your menu?', 
																'btn-menu-add-item', 'menu_item-'.$item['Item']['id']); ?>
								</div>
							</div>
							<?
						endforeach;
						?></div><?php
					} else {
						?><div class="txt-l red b">You have no items set up in this category! Add a New Menu Item, using the button above!</div><?php	
					}
					?>
				</div>
			<?php endforeach; ?>
		</div> <!-- end catsAccordion -->
		<?php } else { ?>
			<hr /><?php echo $this->element('warning-msg-box', array('msg'=>'You have not set up any menu categories. Click "Manage Categories" above to get started.')); ?>
		<?php } ?>
		</div>
		<div id="mnuCombos" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="ui-helper-clearfix">
				<div class="left"><h3 class="orange lg pad-l20">Combo Specials</h3></div>
				<div class="right"><?php echo $jquery->btn('/combos/add/'.$location['Location']['id'], 'ui-icon-circle-plus', 'Add a New Combo Special'); ?></div>
			</div>
			<div id="ajaxComboSortResult"></div>
			
			<div class="gray-border marg-b10 pad3 combo-sort-list" id="comboSortList">
				<?php if(sizeof($combos) > 0) { 
					foreach($combos as $combo): ?>
						<div id="comboSortList_<?php echo $combo['Combo']['id']; ?>" class="ui-helper-clearfix marg-b10" style="border-bottom:1px solid #ddd;">
							<div style="width:15px;" class="ui-icon-container left ui-state-default ui-corner-all">
								<a href="#" title="Drag & Drop to Change Sort Order"><div class="ui-icon ui-icon-arrowthick-2-n-s"></div></a>
							</div>
				
							<?php if($location['Location']['show_menu_images'] == '1') { ?>
								<div class="left pad-l5" style="width:100px;"><?
									echo $html->image($misc->calcImagePath($combo['Combo']['image'], 'files/combos/'), 
													  array('class'=>'tbl-photo orange pad2'));
								?>
								</div>
								<div class="left valign-t b pad-r5" style="width:290px;">
									<span class="orange"><?=$combo['Combo']['title']?></span>
									<?php if(!empty($combo['Combo']['description'])) { ?>
										<br /><span class="green base"><?=$combo['Combo']['description']?></span>
									<?php } ?>
								</div>
							<?php } else { ?>
								<div class="left valign-t b pad-l5 pad-r5" style="width:390px;">
									<span class="orange"><?=$combo['Combo']['title']?></span>
									<?php if(!empty($combo['Combo']['description'])) { ?>
										<br /><span class="green base"><?=$combo['Combo']['description']?></span>
									<?php } ?>
								</div>
							<?php } ?>
							
							<div class="left b dark-gray" style="width:60px;"><?php 
								if(sizeof($item['ItemsOption']) > 0) {
									//we have multiple pricing options
									?><div class="base orange">Starting at:</div><?php 
								} 
								echo "$".number_format($combo['Combo']['price'], 2);
							?>
							</div>
							<div class="left" style="width:75px;">
								<?php echo $misc->jqueryBtn('/combos/edit/'.$combo['Combo']['id'], 'ui-icon-pencil', 'Edit', 
															null, 'btn-menu-edit-special', 'menu_special-'.$combo['Combo']['id']); ?>
								<?php echo $misc->jqueryBtn('/combos/delete/'.$combo['Combo']['id'], 'ui-icon-circle-close', 'Delete',
															'Are you sure that you would like to delete this Menu Combo?'); ?>
							</div>
						</div><?
					endforeach;	
				} else { ?>
					<div class="txt-l red b">You have no combo specials set up! Add a New Combo Special now, using the button above!</div>
				<?php } ?>
			</div>
		</div>
	</div> <!-- end  mnuCategories tab-->	
</div>
</div>
<script>
$(function() {
	$('#page-loader').hide()
	$('#content-wrapper').slideDown('fast');
});
</script>