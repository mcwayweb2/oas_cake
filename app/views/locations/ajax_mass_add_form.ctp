<script>
$(function() { 
	$('.topping-select').uniform();

	$('.link-new-topping').click(function() {
		var toppingDiv = $(this).closest('.new-topping-div');
		var indexParts = toppingDiv.attr('id').split('_');
		
		toppingDiv.html('<input type="text" class="form-text ui-corner-all ui-state-default" name="data[toppings][' + indexParts[1] + ']" ' + 
					    'id="toppings' + indexParts[1] + '" /><br />' + 
					    '<span class="base b">Enter New Topping Title: <span class="required">*</span></span>');
	});
});
</script>
<?php echo $jquery->scrFormSubmit('#btnAddMassToppings', '#formAddMassToppings'); ?>
<hr />
<p>Select each topping you would like to add from the drop down lists, along with with a topping category. If you need to add a new topping that 
is not on the list, click the corresponding <span class="italic b">"Add New Topping"</span> link, and enter the new topping in the text box that is generated for you.</p>
<div class="ui-widget-content ui-corner-all marg-b10">
	<table class="form-table">
		<tr>
			<th style="width:15px;" class=" pad-b5"></th>
			<th class="b green pad-b5">Topping to Add</th>
			<th class="b green pad-b5">Topping Label</th>
			<th class="b green pad-b5">Price Alter</th>
		</tr>
		<?php echo $this->element('table_accent_row', array('cols'=>'4')); ?>
		<?php for($x = 1; $x <= $amt; $x++) { ?>
		<tr class="valign-t pad-b5">
			<th style="width:15px;"><div style="padding-top:3px;"><?php echo $x; ?></div></th>
			<td><div class="new-topping-div" id="div-index_<?php echo $x; ?>">
				<?php echo $form->select('toppings.'.$x, $topping_list, null, array('class'=>'form-text topping-select', 'label'=>false, 'div'=>false), 'Select your Topping'); ?><br />
				<div class="pad-l5"><?php echo $html->link('Add New Topping', '#', array('class'=>'link-new-topping base')); ?></div>
			</div></td>
			<td><?php echo $form->select('topping_types.'.$x, $topping_types, $default_label, array('class'=>'form-text topping-select', 'label'=>false, 'div'=>false), false); ?></td>
			<td><?php echo $form->text('price_alters.'.$x, array('class'=>'form-text ui-corner-all', 'style'=>'width:80px; margin-top: 2px;'));?></td>
		</tr>
		<?php } ?>
	</table>
</div>

<div class="ui-helper-clearfix">
	<div class="left"></div>
	<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add new Toppings', null, null, 'btnAddMassToppings'); ?></div>
</div>
