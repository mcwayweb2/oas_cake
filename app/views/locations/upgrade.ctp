
<script>
$(function() {
	//any time any of the checkboxes are checked/unchecked, up active ct, and total
	$('.premium-member-check').click(function() {
		var ct = 0;
		var perLocFee = <?php echo PREMIUM_ACCT_MONTHLY_FEE; ?>;

		// loop through checkboxes and get check count for new subscriptions
		$('.premium-member-check').each(function() {
			if($(this).attr('checked') && 
			   $(this).attr('disabled') === false) { ct += 1; }	
		}); 
		
		//change labels for count and total
		$('.spanCurrentSelectionCt').text('(' + ct + ')');
		$('.spanCurrentSelectionTot').text('$' + (ct * perLocFee).toFixed('2') + ' Total');
	});

	$('#btnLocationUpgrade').click(function() {
		//check to make sure all fields are filled out
		var success = true;

		//remove existing error classes and empty msg text
		$('.span-msg-text').empty();
		$('.msg-input').removeClass('ui-state-error ui-state-highlight');

		if($('#LocationAgreement').is(':not(:checked)')) {
			success = false;
			$('#LocationAgreementText').text('Please agree to the terms to proceed.');
			$('#agreementFieldset').addClass('ui-state-highlight');
		}

		
		if($('#LocationFname').val() == '') {
			success = false;
			$('#LocationFnameText').text('Please enter a first name.');
			$('#LocationFname').addClass('ui-state-error');
		}
		if($('#LocationLname').val() == '') {
			success = false;
			$('#LocationLnameText').text('Please enter a last name.');
			$('#LocationLname').addClass('ui-state-error');
		}

		if($('#LocationAddress').val() == '') {
			success = false;
			$('#LocationAddressText').text('Please enter a billing address.');
			$('#LocationAddress').addClass('ui-state-error');
		}

		if($('#LocationCity').val() == '') {
			success = false;
			$('#LocationCityText').text('Please enter a city.');
			$('#LocationCity').addClass('ui-state-error');
		}

		if($('#LocationZip').val() == '') {
			success = false;
			$('#LocationZipText').text('Please enter a zip code.');
			$('#LocationZip').addClass('ui-state-error');
		}

		if($('#LocationCcNum').val() == '') {
			success = false;
			$('#LocationCcNumText').text('Please enter the credit card number.');
			$('#LocationCcNum').addClass('ui-state-error');
		}

		if(success) {
			$('#formLocationUpgrade').submit();
		}
		return false;
	});
});	
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
 		<div class="left pad-l5"><h1>Premium Membership Subscriptions</h1></div>
 		<div class="right pad-r5"><?php echo $jquery->btn(.'/advertisers/dashboard', 'ui-icon-contact', 'Back to Dashboard'); ?></div>
 	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
		<div class="green b marg-b5 marg-t10" style="font-size:13px;">Whats better than a Free Order a Slice Account? ... Our Premium Membership!</div>
		<p>For just <?php echo $number->currency(PREMIUM_ACCT_MONTHLY_FEE); ?> a month, you can upgrade your Restaurant to a <span class="italic b">Premium Membership</span>. If you don't like the extended benefits our <span class="italic b">Premium 
		Membership</span> offers, you can always switch back to a Free Account.</p>
	
		
		<div class="b orange marg-b10">Enjoy all the excellent benefits of our regular account And More:</div>
	
	 	<ul class="list-green-check ui-helper-reset ui-state-default ui-corner-all" >
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10" >Better Online Restaurant Exposure ... Get Listed as a Featured Restaurant on our Home Page!</div>
            	<div class="icon-tooltip left" title='Gain better online exposure by being listed as a Featured Restaurant on our website. Featured Restaurants get a "home page advantage" above restaurants with a standard free account, by being displayed on the home page of the website for customers in their area. They are also ranked above restaurants with a free account in customer searches.'></div>
            </li>
            
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10">Free Menu Setup ... Kiss that $39 menu setup fee goodbye!</div>
            	<div class="icon-tooltip left" title='Free accounts are charged a one time $39 setup fee to create their online menu. If you upgrade your restaurant to a Premium Membership, this $39 menu setup fee is waived.'></div>
            </li>
            
     		<!--  
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left">Better Customized Web Address ... http://your-restaurant-name.orderaslice.com</div>
            </li>-->
            
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10">Generate Instant Delivery Directions for your Drivers ... Increase Employee Productivity!</div>
            	<div class="icon-tooltip left" title='Generate instant delivery directions for your drivers for all incoming orders.'></div>
            </li>
            
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10">Attract More Customers with Promotion Codes ... Setup and Manage Unlimited Promotion Codes!</div>
            </li>      
            
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10">Priority Menu Creation ... We will set up your Online Menu for you in 3 business days or less!</div>
            	<div class="icon-tooltip left" title='Restaurants that upgrade to a Premium Membership before their online menu is set up are given "Priority Service". Priority Service guarantees that we will have your Online Menu set up for you in 3 days or less. (Regular turn around time is 7 days or less.)'></div>
            </li>
            
            <li class="ui-helper-clearfix">
            	<div class="ui-icon ui-icon-check left"></div>
            	<div class="left pad-r10">Free Quarterly Menu Upgrades ... Let us keep your Online Menu Updated, so you don't have to!</div>
            	<div class="icon-tooltip left" title='Restaurant will receive "free quarterly menu updates" by an Order a Slice administrator. A free quarterly menu update is defined as a group of changes to a restaurants online menu, that are submitted to Order a Slice administration at one time. Restaurants with a Premium Membership will be eligible for this service once every 90 days.'></div>
            </li>
        </ul>
	
	<fieldset class="ui-corner-all ui-widget-content marg-b10 marg-t10"><legend class="ui-corner-all">My Restaurants</legend>
		<p>Your restaurants are listed below. To upgrade a restaurant, check the boxes next to the restaurants that you would like a 
		<span class="italic b">Premium Membership</span> for, and press <span class="italic b">"Update Membership"</span>.</p>
		
		<div class="ui-helper-clearfix marg-b5 pad2 b">
			<div class="pad-l5 left">Premium Memberships Active: <span class="red">(<?php echo $locations['active_ct']; ?>)</span> of <span class="red">(<?php echo sizeof($locations) - 2; ?>)</span></div> 
			<div class="right pad-r5">Current Subscription Total: <span class="red"><?php echo $number->currency($locations['active_tot']); ?> / month</span>.</div>
		</div>
		
		<?php echo $form->create('Location', array('action' => 'upgrade', 'id' => 'formLocationUpgrade')); ?>
		<div class="ui-corner-all ui-widget-content marg-b10"> 
			<table class="list-table">
				<tr><th></th>
					<th style="width:280px;">Restaurant Name</th>
					<th>Current Account Type</th>
				</tr>
			<?php 
			echo $this->element('table_accent_row', array('cols'=>'3'));
			foreach($locations as $i => $loc):
				if(is_numeric($i)) {
					$params = array('class' => 'premium-member-check');
					if($loc['Location']['acct_type'] != '1') {
						$params['checked'] = 'checked';
						$params['disabled'] = 'disabled';
					}
				?>
				<tr>
					<td style="width:20px;">
						<?php echo $form->checkbox('locations.'.$loc['Location']['id'], $params); ?>
					</td>
					<td class="orange"><?php echo $loc['Location']['name']; ?></td>
					<td><?php 
						switch($loc['Location']['acct_type']) {
							case '1':
								echo 'Free Account'; break;
							case '2': ?>
								<div class="ui-helper-clearfix">
									<div class="left pad-r10">
										<div class='green'>Premium Account</div>
										<div class="base red">(<?php echo $number->currency($loc['Location']['premium_acct_rate']); ?> / Month)</div>
										
									</div>
									<div class="left"><?php echo $jquery->btn('/locations/unsubscribe/'.$loc['Location']['id'], 'ui-icon-contact', 'Unsubscribe',
																			  'Are you sure that you would like to cancel your Premium Membership Subscription for '.$loc['Location']['name'].'.'); ?></div>
								</div><?php 
								break;
							case '3':
								echo "<span class='green'>Licensed Account</span>"; break;
						}
					?></td>
				</tr>
				<?php 
				}
			endforeach;	
			echo $this->element('table_accent_row', array('cols'=>'3'));
			?>
			</table>
		</div>
		
		<fieldset class="marg-b10 msg-input" id="agreementFieldset">
			<legend>Authorization Agreement</legend>
			<div class="ui-helper-clearfix marg-b5">
				<div class="left pad-r5" style="width:20px;"><?php echo $form->checkbox('agreement'); ?></div>
				<div class="left" style="width:500px;">
					<p>By checking this box, I am agreeing that I am legally authorized 
					to add and conduct business as <span class="b italic"><?php echo $session->read('Advertiser.name'); ?></span> with this credit card.</p>
					
					<p>You may cancel your <span class="italic b">Premium Membership</span> subscription at any time, however prorated refunds cannot be given 
					for time remaining on your subscription.</p>
					<div id="LocationAgreementText" class="b red span-msg-text"></div>
				</div>
			</div>
		</fieldset>
		
		<fieldset class="ui-corner-all ui-widget-content marg-b10"><legend class="ui-corner-all">Update Membership</legend>	
			<div class="pad5 marg-b5 b">
				<span class="spanCurrentSelectionCt red">(0)</span> of <span class="red">(<?php echo sizeof($locations) - 2; ?>)</span> New Restaurants Selected. 
				<span class="spanCurrentSelectionTot red">$0.00 Total</span> to upgrade to Premium Membership(s).
			</div>
			
			<fieldset class='marg-b10'>			
				<div class="ui-helper-clearfix">
					<div class="left">
						<table class="form-table">
							<tr>
								<td class="label-req" style="width:150px;">First Name on card: <span class="required">*</span></td>
								<td><?=$form->input('fname', array('class'=>'form-text msg-input', 'label'=>'', 'div' => false))?><br />
								<span class="base b red span-msg-text" id="LocationFnameText"></span></td>
							</tr>
							<tr>
								<td class="label-req">Last Name on card: <span class="required">*</span></td>
								<td><?=$form->input('lname', array('class'=>'form-text msg-input', 'label'=>'', 'div'=>false))?><br />
								<span class="base b red span-msg-text" id="LocationLnameText"></span></td>
							</tr>
							<tr>
					 			<td class="label-req">Address: <span class="required">*</span></td>
								<td><?=$form->input('address', array('label'=>'','class'=>'form-text msg-input', 'div' => false))?><br />
								<span class="base b red span-msg-text" id="LocationAddressText"></span></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">City: <span class="required">*</span></td>
					 			<td><?=$form->input('city', array('label'=>'','class'=>'form-text msg-input', 'div' => false))?><br />
					 			<span class="base b red span-msg-text" id="LocationCityText"></span></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">State: <span class="required">*</span></td>
					 			<td><?=$form->select('state', $states, null, array('label'=>'','class'=>'form-text'), false)?></td>
					 		</tr>
					 		<tr>
					 			<td class="label-req">Zip: <span class="required">*</span></td>
					 			<td><?=$form->input('zip', array('label'=>'','class'=>'form-text msg-input', 'div' => false))?><br />
					 			<span class="base b red span-msg-text" id="LocationZipText"></span></td>
					 		</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
								<?=$html->image('creditcard_visa.gif')?>&nbsp;<?=$html->image('creditcard_mc.gif')?>&nbsp;<?=$html->image('creditcard_discover.gif')?>&nbsp;<?php //echo $html->image('creditcard_amex.gif')?>   
								</td>
							</tr>
							<tr>
								<td class="label-req">Card Number: <span class="required">*</span></td>
								<td><?=$form->input('cc_num', array('class'=>'form-text msg-input', 'label'=>'', 'value'=>'', 'div' => false))?><br />
								<span class="base b red span-msg-text" id="LocationCcNumText"></span></td>
							</tr>
							<tr>
								<td class="label-req">Expiration Month: <span class="required">*</span></td>
								<td><?=$form->month('month', null, array('class'=>'', 'style'=>'width:100px;'), false)?></td>
							</tr>
							<tr>
								<td class="label-req">Expiration Year: <span class="required">*</span></td>
								<td><?=$form->year('year', date("Y"), date("Y")+10, null, array('class'=>'form-select'), false)?></td>
							</tr>
							<tr>
								<td class="label-req">Reference Code:</td>
								<td><?php echo $form->input('reference_code', array('label'=>'','class'=>'form-text', 'div' => false)); ?></td>
							</tr>
						</table>
					</div>
					<div class="right marg-t20">
						<div class="marg-b10"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
						<div style="padding-left:25px;"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
					</div>
				</div>
			</fieldset>
			
			<div class="ui-helper-clearfix">
				<div class="right"><?php echo $jquery->btn('#', 'ui-icon-check', 'Update Membership', null, null, 'btnLocationUpgrade'); ?></div>
			</div>
		</fieldset>
		<?php echo $form->end(); ?>
	</fieldset>
</div>