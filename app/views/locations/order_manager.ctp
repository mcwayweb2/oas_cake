<?php echo $jquery->scrFormSubmit('#btnLocationOrderManager', '#formLocationOrderManager'); ?>
<script>
$(function() {
	$('#LocationStartDate').datepicker({ 
		dateFormat: "yy-mm-dd"
	});

	$('#LocationEndDate').datepicker({ 
		maxDate: new Date(),
		dateFormat: "yy-mm-dd"
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="pad-l5"><h1>Order Manager</h1></div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Location', array('action' => 'order_manager', 'id' => 'formLocationOrderManager')); ?>
	<?php echo $form->hidden('slug', array('value' => $location['Location']['slug'])); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b5"><legend class="ui-corner-all">Search Orders</legend>
		<p>Enter a date range to search orders with:</p>
		<div class="ui-helper-clearfix">
			<div class="left pad-r20">
				<div class="b base">Start Date:</div>
				<div><?php echo $form->input('start_date', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left pad-r20">
				<div class="b base">End Date:</div>
				<div><?php echo $form->input('end_date', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left" style="padding-top: 8px;"><?php echo $jquery->btn('#', 'ui-icon-search', 'Search Orders', null, null, 'btnLocationOrderManager'); ?></div>
		</div>
	</fieldset>
	<?php echo $form->end(); ?>
	<hr />
	
	<?php if(isset($hdr_txt)) { ?><div class="b marg-b5 pad-l5"><?php echo $hdr_txt; ?></div><?php } ?>
	
	<?php if(sizeof($orders) > 0) { $i = 0; ?>
		<div class="ui-widget-header ui-corner-top">
			<div class="ui-helper-clearfix pad2 b">
				<div class="left pad-l5" style="width:205px;">Customer</div>
				<div class="left">Order Total</div>
			</div>
		</div>
		<div class="ui-widget-content ui-corner-bottom">
		<?php foreach($orders as $o): 
			$class = ($i++ % 2 == 0) ? "alt-row" : "";
			?>
			<div class="ui-helper-clearfix marg-b5 pad5 <?php echo $class; ?>">
				<div class="left">
					<div class="b green"><?php echo $html->link($o['User']['fname'].' '.$o['User']['lname'], '/users/order_history/'.$o['User']['id']); ?></div>
					<div class="base b">(<?=$time->niceshort($o['Order']['timestamp'])?>)</div>
				</div>
				
				<div class="right pad-l10"><?php echo $jquery->btn('/orders/details/'.$o['Order']['id'], 'ui-icon-mail-closed', 'Order Details'); ?></div>
				<div class="right pad-l10" style="width:145px;">
					<?php if($o['Order']['order_method'] == 'Delivery'  && $o['Location']['acct_type'] > 1) { ?>
						<?php echo $jquery->btn('/orders/delivery_directions/'.$o['Order']['id'], 'ui-icon-image', 'Delivery Directions'); ?>
					<?php } else { echo '&nbsp;'; } ?>
				</div>
				
				<div class="right b" style="width:80px"><?php echo $number->currency($o['Order']['total']); ?></div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php } else { ?>
		<a class="b red">There are no orders within this date range.</p>
	<?php } ?>
</div>