<script>
$(function() { 
	$('#numOfAddToppings, #idDefaultLabel').uniform();
	$('#ajaxLoadingMassAddToppings').hide();
	
	$('#numOfAddToppings').change(function() {
		$('#ajaxLoadingMassAddToppings').show(); 
		$('#numOfAddToppingsContent').load('/locations/ajax_mass_add_form', { amt: $(this).val(),
																			  label: $('#idDefaultLabel').val() }, function() { 
			$('#ajaxLoadingMassAddToppings').hide();
		});
		return false;
	});
	
	$('#btnRestoreOgToppingForm').click(function() { 
		$('#toppingFormFieldset2').empty();
		$('#toppingFormFieldset').show();
	});
	
});
</script>

<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Add Multiple Toppings</legend>
	<div class="ui-helper-clearfix">
		<div class="left"></div>
		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-contact', 'Restore Original Topping Form', null, null, 'btnRestoreOgToppingForm'); ?></div>
	</div><hr />
	
	<div class="ui-helper-clearfix">
		<div class="left b green" style="width:260px;"><p>How many toppings would you like to add?</p></div>
		<div class="left">
			<?php for($x = 1; $x <= 20; $x++) { $opts[$x] = $x; } ?>
			<?php echo $form->select('num_toppings', $opts, null, array('class'=>'form-text', 'id'=>'numOfAddToppings'), '0')?>
		</div>
		<div class="left"><?php echo $html->image('ajax-loader-sm-bar.gif', array('id'=>'ajaxLoadingMassAddToppings')); ?></div>
	</div>
	
	
	
	
	<div class="ui-helper-clearfix">
		<div class="left b green"  style="width:260px;"><p>Select Default Topping Label:</p></div>
		<div class="left">
			<?php echo $form->select('default_label', $topping_types, null, array('class'=>'form-text', 'id' => 'idDefaultLabel'), false)?>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	<?php echo $form->create('LocationsTopping', array('action'=>'mass_add', 'id'=>'formAddMassToppings')); ?>
	<?php echo $form->hidden('location_id', array('value'=>$location_id)); ?>
	<div id="numOfAddToppingsContent">
	
	</div>
	</form>
</fieldset>