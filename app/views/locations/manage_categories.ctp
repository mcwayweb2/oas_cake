<?php //echo $html->script(array('jquery/alphanumeric')); ?>
<script type="text/javascript">
$(function() {
	$('#ajaxAddNewCatForm').hide();
	$('html, body').animate( { scrollTop: 0 }, 'fast' );
	$('#LocationsCatCatId').uniform();
	$('#CatTitle').alpha({ allow: " " });
	
	$('.showNewCatForm').click( function () { 
		$('html, body').animate( { scrollTop: 0 }, 'fast' );
		$('.ajax-loading2').show();
		$('#ajaxAddExistingCatForm').hide();
		$('#ajaxAddNewCatForm').show();
		$('#CatTitle').addClass('ui-state-hightlight');
		$('.ajax-loading2').hide();
		return false;
	});

	$('#showMyCatForm').click( function () {
		$('html, body').animate( { scrollTop: 0 }, 'fast' ); 
		$('.ajax-loading2').show();
		$('#ajaxAddExistingCatForm').show();
		$('#ajaxAddNewCatForm, .ajax-loading2').hide();
		return false;
	});

	$('#btnMenuAddNewCat').click( function () {
		if($('#CatTitle').val() == '') {
			$('#CatTitleText').html('Please enter a category title!');
			$('#CatTitle').addClass('ui-state-error');
		} else {
			$('#formMenuAddNewCat').submit();
		}
		return false;
	});

	$('#btnMenuAddCat').click( function () {
		$('#formMenuAddCat').submit();
		return false;
	});

	$('.btn-menu-edit-cat').click( function () {
		$('.ajax-loading2').show();
		var catIdParts = $(this).attr('id').split('-');
		$('#catFormFieldset').load($(this).attr('href'), { id: catIdParts[1] }, function() {
			$('html, body').animate( { scrollTop: 0 }, 'fast' );
			$('.ajax-loading2').hide();
		});
		return false;
	});

	$('#listMenuCats').sortable({ 
		containment: 'parent',
		update: function() {
			$('#ajaxResult').load('/locations_cats/ajax_order', $(this).sortable('serialize'));
		} 
	});
	$('#availTimes').hide();
	$('#LocationsCatAvailAllDay').change(function() {
		if($(this).attr('checked') === false) { $('#availTimes').slideDown('fast'); } 
		else { $('#availTimes').hide("fold"); }
	});

	$('#btnHideCatManager').click(function() {
		$('#ajaxManageMenuItemContent').empty();
		$('html, body').animate( { scrollTop: 0 }, 'fast' );
	});
});
</script>

<?php //echo $html->script('js/jquery/form-styles', false); ?>
<div class="ui-widget-header ui-corner-top pad5 ui-helper-clearfix">
	<div class="left pad-l5"><h1>Manage Menu Categories</h1></div>
	<div class="right pad-r5"><?php echo $jquery->btn('#', 'ui-icon-circle-close', 'Hide Category Manager', null, null, 'btnHideCatManager'); ?></div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b10">
	<div id="catFormFieldset">
	<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">Add a New Menu Category</legend>
		<div id="ajaxAddExistingCatForm">
			<p>Select the new category you would like to offer from the drop down menu below. If the topping you would like to add is not 
			listed, you can <?php echo $html->link('enter a new category', '#', array('class'=>'showNewCatForm')); ?> by 
			<?php echo $html->link('clicking here', '#', array('class'=>'showNewCatForm'));?>.</p>
			<p>To make this Menu Category only available during a specific portion of the day, (i.e. A Lunch Only Category), uncheck the appropriate 
			box, and fill out starting & ending times for when this category will be available to order from. Leave the box checked to make the 
			Menu Category available all day.</p>
			<p>When you are finished, press the <span class="italic b">"Add Menu Category"</span> button to save the Menu Item.<br />
			
			<?php echo $html->link('Need to add more than one category?', '/locations_cats/multiple_add/'.$location_id); ?></p>
			
			<?php echo $form->create('LocationsCat', array('id'=>'formMenuAddCat', 'action'=>'add')); ?>
			<?php echo $form->hidden('location_id', array('value'=>$location_id)); ?>
			<table class="form-table">
				<tr>
					<td class="label-req" style="width: 150px;">Category: <span class="required">*</span></td>
					<td><?php echo $form->select('cat_id', $cat_list, null, array('class'=>'form-text', 'label'=>false, 'div'=>false), false); ?></td>
				</tr>
				<tr>
					<td class="label-req">Subtext:</td>
					<td><?php echo $form->input('subtext', array('class'=>'form-text ui-corner-all ui-state-default', 'label'=>false, 'div'=>false, 'type'=>'textarea'))?><br />
					<span class="base orange">(Enter a description for the category.)</span></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div class="ui-helper-clearfix marg-b5">
							<div class="left pad-r10"><?php echo $form->checkbox('avail_all_day', array('checked' => 'checked')); ?></div>
							<div class="left b">Is Menu Category Served Entire Business Day?</div>
						</div>
					</td>
				</tr>
			</table>
			<div id="availTimes">
				<table class="form-table">
					<tr>
						<td class="label-req" style="width: 150px;">Start Serving Time:</td>
						<td><?php echo $form->input('time_open', array('class'=>'ui-corner-all ui-state-default', 'label'=>false, 'type'=>'time')); ?>
						</td>
					</tr>
					<tr>
						<td class="label-req">End Serving Time:</td>
						<td><?php echo $form->input('time_closed', array('class'=>'ui-corner-all ui-state-default', 'label'=>false, 'type'=>'time')); ?>
						</td>
					</tr>
				</table>
			</div>
			<table class="form-table">
				<tr><td style="width: 150px;"></td>
				<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Menu Category', null, null, 'btnMenuAddCat', '145'); ?></td></tr>
			</table>
			<?php echo $form->end(); ?>
		</div>
		
		<div id="ajaxAddNewCatForm">
			<div class="ui-helper-clearfix">
				<div class="left"><p>Enter the menu category you would like to add below.</p></div>
				<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-arrow-w', 'Show New Category Form', null, null, 'showMyCatForm', '185'); ?></div>
			</div>
			
			<?php echo $form->create('Cat', array('id'=>'formMenuAddNewCat', 'action'=>'add')); ?>
			<?php echo $form->hidden('location_id', array('value'=>$location_id)); ?>
			<table class="form-table">
				<tr>
					<td class="label-req">Category Title: <span class="required">*</span></td>
					<td><?php echo $form->input('title', array('class'=>'form-text ui-corner-all ui-state-default', 'label'=>false, 'div'=>false)); ?>
					<br /><span class="base red b" id="CatTitleText"></span></td>
				</tr>
				<tr><td></td>
				<td><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Menu Category', null, null, 'btnMenuAddNewCat', '145'); ?></td></tr>
			</table>
			<?php echo $form->end(); ?>
		</div>
	</fieldset>
	</div>
			
	<fieldset class="ui-widget-content ui-corner-all marg-b10"><legend class="ui-corner-all">My Menu Categories</legend>
		<p>Drag and drop the menu categories to the order of your preference, using the drag handle icons to the left of each category name. 
		This same sort order will also be used to display your Online Menu to customers that visit your website.</p>
		<p>To add a Selection Series to a category, click on the Categories <span class="italic b">"Edit"</span> button. (i.e. A Selection 
		of side dishes for the customer	to choose from, that comes with each meal in the category.) </p>
		<div id="ajaxResult"></div>
		<div class="ui-widget-content ui-corner-all" id="listMenuCats">
			<?php if(sizeof($cats) > 0) { ?>
				<?php foreach ($cats as $c): ?>
					<div id="listMenuCats_<?php echo $c['LocationsCat']['id']; ?>" class="ui-helper-clearfix" style="border-bottom: 1px solid #f67d07; padding: 5px 2px;">
						<div style="width:15px;" class="ui-icon-container left ui-state-default ui-corner-all">
							<a href="#" title="Drag & Drop to Change Sort Order"><div class="ui-icon ui-icon-arrowthick-2-n-s"></div></a>
						</div>
						<div class="left b pad-l5" style="width:150px;">
							<div class="green"><?php echo $c['Cat']['title']; ?></div>
							<?php if(!empty($c['LocationsCat']['time_open'])) { ?>
								<div class="base red">
									(Available: <?php echo date("g:ia", strtotime($c['LocationsCat']['time_open']));?> - <?php echo date("g:ia", strtotime($c['LocationsCat']['time_closed']));?>)
								</div>
							<?php } else { ?><div class="base red">(Available All Day)</div><?php } ?>
						</div>
						<div class="left pad-r10 base" style="width:205px;"><?php 
							if(sizeof($c['LocationsCatsOption']) > 0) { 
								$current = false; $addons = 0;
								foreach($c['LocationsCatsOption'] as $opt):
									if($opt['add_group_id'] != 0 && $opt['add_group_id'] != '0') {
										//part of a selection series
										if($opt['add_group_id'] != $current) {
											//still on the same series
											?>
											<div class="b green">Served with choice of (<?php echo $opt['add_group_limit']; ?>):</div>
											<?php 
										} 
										?>
										<ul class="pad-l20"><li><?php 
											echo $opt['label'];
											if(!empty($opt['additional_price'])) echo '<span class="b pad-l5">(+$'.number_format($opt['additional_price'], 2).')</span>';
											
										?></li></ul><?php 
									} else { $addons++; }
									$current = $opt['add_group_id'];
								endforeach;
								
								if($addons > 0) { ?><div class="b orange marg-t5">(<?php echo $addons; ?>) Individual Add-Ons Available.</div><?php }
							} else { echo "&nbsp;"; } ?>
						</div>
						<div class="left" style="width:65px;">
							<?php echo $misc->jqueryBtn('/locations_cats/edit/'.$c['LocationsCat']['id'], 'ui-icon-pencil', 'Edit', 
													null, 'btn-menu-edit-cat', 'menu_cat-'.$c['LocationsCat']['id'], '60'); ?>	
						</div>
						<div class="left" style="width:80px;">
							<?php echo $misc->jqueryBtn('/locations_cats/delete/'.$c['LocationsCat']['id'], 'ui-icon-circle-close', 'Delete',
														'Are you sure that you would like to remove this category from your menu? NOTE: Removing a menu category will also remove all items you have set up in that category.', 
														null, null, '70'); ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php } else { ?>
				<div class="txt-c b green">You currently have no menu categories set up!</div>
			<?php } ?>
		</div>
	</fieldset>
</div>