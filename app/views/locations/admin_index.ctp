<?php echo $jquery->scrFormSubmit('#btnLocationAdminIndex', '#formLocationAdminIndex'); ?>

<script>
$(function() {
	$('#adminDashTabs').tabs();
	$('.progress-image, .progress-success').hide();

	$('.link-leave-comment').click(function() {
		//save admin comment via ajax
		var progressImage = $(this).next('.progress-image');
		progressImage.show();
		
		var commentField = $(this).nextAll('.admin-comments-field');		
		var locIdParts = commentField.attr('id').split('-');

		$(this).nextAll('.progress-success').load('/advertisers/ajax_admin_comment_response', 
												  { id: locIdParts[1], comment: commentField.val() }, function() {
			progressImage.hide();
		});
		
		return false;
	});
});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left pad-l5"><h1>Restaurant Manager</h1></div>
		<div class="right"><?php echo $jquery->btn('/registration', 'ui-icon-circle-plus', ''); ?></div>
	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php echo $form->create('Location', array('action' => 'admin_index', 'id' => 'formLocationAdminIndex')); ?>
	<fieldset class="ui-widget-content ui-corner-all marg-b5"><legend class="ui-corner-all">Search Restaurants</legend>
		<div class="ui-helper-clearfix">
			<div class="left pad-r20">
				<div class="b base">Search By Restaurant Keyword(s):</div>
				<div><?php echo $form->input('keywords', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left pad-r10">
				<div class="b base">Search By City or Zip Code:</div>
				<div><?php echo $form->input('city', array('class' => 'form-text ui-state-default ui-corner-all date-fields', 'label' => false)); ?></div>
			</div>
			
			<div class="left" style="padding-top: 8px;"><?php echo $jquery->btn('#', 'ui-icon-search', 'Search Restaurants', null, null, 'btnLocationAdminIndex'); ?></div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Filter by Restaurant Status:</div>
				<?php $opts = array('1' => 'Active', '2' => 'Inactive'); ?>
				<div><?php echo $form->select('status', $opts, null, array(), ''); ?></div>
			</div>
			<div class="left pad-r20">
				<div class="b base">Filter by Menu Status:</div>
				<?php $opts = array('3' => 'Menu Online', '1' => 'Offline/Pending Approval', 
									'0' => 'Pending Customer Setup', '2' => 'Pending Our Setup')?>
				<div><?php echo $form->select('menu_status', $opts, null, array(), ''); ?></div>
			</div>
		</div>
		
		<div class="ui-helper-clearfix">
			<div class="left pad-r10">
				<div class="b base">Filter by Menu Category:</div>
				<div><?php echo $form->select('cat_id', $food_cats, null, array(), ''); ?></div>
			</div>
		</div>
	</fieldset>
	</form>
	<hr />
	
	<?php if(isset($hdr_txt)) { ?><div class="b marg-b5 pad-l5"><?php echo $hdr_txt; ?></div><?php } ?>
	
	<?php if(sizeof($locations) > 0) { ?>
		<div class="ui-widget-header ui-corner-top pad2">
			<div class="ui-helper-clearfix">
				<div class="left txt-r" style="width:160px;">Restaurant</div>
				<div class="left txt-r" style="width:230px;">Admin Comments</div>
			</div>
		
		</div>
		<div class="ui-widget-content ui-corner-bottom">
		<?php foreach($locations as $menu): ?>
			<div class="ui-helper-clearfix marg-b5 pad-b5" style="border-bottom:1px solid #F67D07;">
				<div class="left ui-corner-all pad-r5" style="width:80px">
					<?php 
					echo $html->image($misc->calcImagePath($menu['Location']['logo'], 'files/logos/'), 
									  array('class'=>'orange-border pad2', 'style'=>'width:75px;'));
					?>									   
				</div>
				<div class="left b" style="width:200px;">
					<div class="orange"><?php echo $menu['Advertiser']['name']; ?></div>
					<div class="green base"><?php echo $menu['Location']['name']; ?></div>
					<div style="width:190px;" class="marg-t5">
						<?php 
						if($menu['Location']['menu_ready'] == '0') {
							echo $this->element('error-msg-box', array('msg' => 'Pending Customer Setup')); 
						} else if($menu['Location']['menu_ready'] == '2') {
							echo $this->element('error-msg-box', array('msg' => 'Pending Our Setup'));
						} else if($menu['Location']['active'] == '1') {
							//assumed: menu_active = 1
							echo $this->element('warning-msg-box', array('msg' => 'Menu Online'));
						} else {
							//assumed: menu_active = 1
							//assumed: active = 0
							echo $this->element('error-msg-box', array('msg' => 'Offline, Pending Approval'));
						} 
						
						if($menu['numPayProfiles'] <= 0) { 
							echo $this->element('error-msg-box', array('msg' => 'No Billing Profile!'));
						} ?>
					</div>
				</div>
				<div class="left b base" style="width:150px;">
					<div class="ui-helper-clearfix">
						<?php echo $html->link('Save Comment', '#', array('class'=>'link-leave-comment left', 'id'=>'')); ?>
						<div class="left progress-image"><?php echo $html->image('ajax-loader-sm-bar.gif', array('style'=>'width:40px;')); ?></div>
						<div class="left progress-success"></div>
						<?php echo $form->textarea('admin_comments.'.$menu['Location']['id'], 
												   array('rows' => '5', 'value'=>$menu['Location']['admin_comments'], 'style'=>'font-size:90%; width:140px;', 
												   		 'class'=>'base ui-corner-all pad2 ui-state-default admin-comments-field',
												   		 'id'=>'comments-'.$menu['Location']['id'])); ?>
					</div>
				</div>
				<div class="right" style="width:125px;">
					<?php echo $jquery->btn('mailto:'.$menu['Location']['contact_email'], 'ui-icon-mail-closed', 'Send Email', null, null, null, '120'); ?>
					<div class="marg-b10"><?php echo $jquery->btn('/admin/advertisers/login/'.$menu['Location']['slug'], 'ui-icon-image', 'Menu Manager', null, null, null, '120'); ?></div>
					
					<?php if($menu['Location']['menu_ready'] == '1') {
						echo $jquery->btn('/admin/locations/toggle_active/'.$menu['Location']['id'], 'ui-icon-circle-check', 
										  (($menu['Location']['active'] == '1') ? 'Disable' : 'Approve'), 
										  'Are you sure you want change restaurant status?', null, null, '120');
					} ?>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php } else { ?>
		<a class="b red">There are no Restaurants that match your search criteria.</p>
	<?php } ?>
</div>