<script>
$(function() {
	$('.add-fav-link').click( function () {
		var favIdParts = $(this).attr('id').split('-');
		$('#ajax-fav-update-' + favIdParts[1]).load($(this).attr('href'), { id: favIdParts[1] });
		return false;
	});
});	
</script>

<?php echo $this->element('browse-by-city', array('search_by_zip' => ((isset($search_zip)) ? $search_zip : null))); ?>
<?php //debug($this->params); ?>
<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left pad-l10"><h1>Browse Restaurants</h1></div>
		<div class="right"><?php echo $misc->jqueryBtn('/request-a-restaurant', 'ui-icon-comment', 'Request a Restaurant?'); ?></div>
	</div>
</div>
<div class="ui-widget-content ui-corner-bottom">
	<?php 
if(sizeof($locations) > 0) {
	$prefx = (isset($search_cat)) ? '<span class="red">'.$search_cat.'</span> Restaurants' : 'Restaurants';
	
	if(isset($search_zip) && isset($search_keywords)) {
		echo $this->element('pagination_info', array('msg'=>$prefx.' near: <span class="red">'.ucwords($search_zip).
															'</span>, with keywords: <span class="red">"'.$search_keywords.'"</span>.<br />'));
	} else if(isset($search_zip)) {
		echo $this->element('pagination_info', array('msg'=>$prefx.' near: <span class="red">'.ucwords($search_zip).'</span>.'));
	} else if(isset($search_keywords)) {
		echo $this->element('pagination_info', array('msg'=>$prefx.' with keywords: <span class="red">"'.$search_keywords.'"</span>.'));
	} else { 
		echo $this->element('pagination_info');
	}
	
		foreach ($locations as $location): ?>
	    	<div class="ui-widget-content ui-corner-all marg-b10">
	    		<div class="ui-helper-clearfix">
		    		<div class="left pad-r10" style="width:160px;">
		    		<?php 
		    			if($location['Location']['use_logo_in_banner'] == '2' && !empty($location['Advertiser']['logo'])) {
		    				//attempt to display advertiser logo
		    				echo $html->link($html->image('../files/logos/'.$location['Advertiser']['logo'], 
		    										      array('style'=>'width:150px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$location['Location']['slug'], 
		    							 	 array('escape'=>false));
		    			} else if(!empty($location['Location']['logo'])) {
		    				//attempt to display location logo
		    				echo $html->link($html->image('../files/logos/'.$location['Location']['logo'], 
		    										  	  array('style'=>'width:150px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$location['Location']['slug'],
		    								 array('escape'=>false));
		    			} else { 
		    				//display order a slice logo, to make sure we have something displayed
		    				echo $html->link($html->image('orderaslice_logo_sm.png', 
		    										      array('style'=>'width:150px;', 'class'=>'ui-corner-all orange-border pad2')), 
							             	 '/menu/'.$location['Location']['slug'], 
		    							 	 array('escape'=>false)); 
		    			}
		    		?></div>
		    		<div class="left">
		    			<h1><?php echo $location['Location']['name']?></h1>
		    			<div class="b green marg-b5"><?php echo $location['Location']['slogan']?></div>
		    			<div class="b marg-b5">Cuisine: <?php echo $location['FoodCat']['title']?></div>
		    			<?php if(isset($search_zip)) { ?>
		    				<div class="b base">
		    					(Approx. <span class="red"><?php echo number_format($location[0]['distance'], 2); ?> miles</span> from your search location.)
		    				</div>
		    			<?php } ?>
		    		</div>
		    		<div class="right" style="width:130px;">
						<?php 
						echo $misc->jqueryBtn('/menu/'.$location['Location']['slug'], 
											  'ui-icon-cart', 'View Menu');
						if($session->check('User')) {	
							echo $misc->jqueryBtn('/users/add_favorite', 'ui-icon-star', 'Mark as Favorite', 
												  null, 'add-fav-link', 'fav-'.$location['Location']['id']);
						}
						echo $misc->jqueryBtn('/restaurant/'.$location['Location']['slug'], 
											  'ui-icon-image', 'View Map');
						?>
						<div id="ajax-fav-update-<?=$location['Location']['id']?>"></div>
					</div>
				</div>
	    		<hr />
	    		
	    		<div class="ui-helper-clearfix">
	    			<div class="left">
	    				<?php if($location['Location']['accept_pickups'] == '1') { ?>
	    				<div class="ui-helper-clearfix marg-b5">
	    					<div class="left pad-r5"><?php echo $html->image('icons/16-green-check.png');?></div>
	    					<div class="left b red">Currently Accepting Pickup Orders</div>
	    				</div>
	    				<?php } ?>
	    				<?php if($location['Location']['accept_deliveries'] == '1') { ?>
	    				<div class="ui-helper-clearfix">
	    					<div class="left pad-r5"><?php echo $html->image('icons/16-green-check.png');?></div>
	    					<div class="left b red">Currently Accepting Delivery Orders</div>
	    				</div>
	    				<?php } ?>
	    			</div>
	    			<div class="right pad-r20 txt-r" style="width:220px;">
	    				<div class="ui-helper-clearfix marg-b5">
	    					<div class="left b" style="width:170px;">Minimum Delivery Order:</div>
	    					<div class="right" style="width:50px;"><?php echo $number->currency($location['Location']['min_delivery_amt']); ?></div>
	    				</div>
	    				<div class="ui-helper-clearfix">
	    					<div class="left b" style="width:170px;">Delivery Fee:</div>
	    					<div class="right" style="width:50px;">
	    						<?php echo $number->currency($location['Location']['delivery_charge'] + OAS_PER_ORDER_FEE); ?>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
		<?php endforeach;
		
		echo $this->element('pagination_links', array('class'=>'marg-t10'));
		?>
		<div class="txt-r base marg-t10">
			<?php echo $html->link('Don\'t See Your Favorite Restaurant Listed Here?', '/request-a-restaurant'); ?>
		</div><?php
		
		//echo debug($locations);
	} else {
		$msg = "I'm sorry. There are currently no open ";
		if(isset($search_cat)) $msg .= '"'.$search_cat.'" ';
		
		//if they are searching with a zip code & name
		if(isset($search_zip) && isset($search_keywords)) {
			$msg .= "restaurants near (".ucwords($search_zip).") that match your search criteria: \"$search_keywords\"";	
		} else if(isset($search_zip)) {
			//if they are only searching with a zip code
			$msg .= "restaurants near: ".ucwords($search_zip).".";	
		} else if(isset($search_keywords)){
			//if they are only searching with a name
			$msg .= "restaurants near you that match your search criteria: \"$search_keywords\".";
		} else {
			$msg .= "restaurants near your location.";
		}
		echo $this->element('warning-msg-box', array('msg' => $msg));	
	} ?>
</div>
<?php //debug($this->params); ?>