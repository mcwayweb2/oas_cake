<script type="text/javascript">
	$(function() {
		$('.ajax-loading').hide();
		
		$('.btn-menu-edit').click( function () {
			$('.ajax-loading').show();
			var itemIdParts = $(this).attr('id').split('-');
			$('#ajaxEditOrderContent').load($(this).attr('href'), { id: itemIdParts[1] }, function() {
				$('html, body').animate( { scrollTop: 0 }, 'fast' );
				$('.ajax-loading').hide();
			});
			return false;
		});
	});
</script>

<div class="ui-widget-header ui-corner-top pad5">
	<div class="ui-helper-clearfix">
		<div class="left"><h1>Order Confirmation</h1></div>
		<div class="right">
			<?php echo $jquery->btn('/menu/'.$location['Location']['slug'], 'ui-icon-circle-arrow-w', 'Back to the Menu'); ?>
		</div>
	</div>
</div>
<div class="ui-widget-content ui-corner-bottom marg-b20">
	<p>Take a moment to review your order. At this time you can modify or delete any of the items in your order. To add additional items to your order, 
	revisit the menu using the button above. When you are satisfied with your order, press the "Complete Order" button to confirm your purchase.</p>
	
	<div class="ajax-loading txt-c"><?php echo $html->image('ajax-loader-lg-bar.gif', array('style'=>'height:25px;')); ?></div>
	<div id="ajaxEditOrderContent"></div>
	
	<div class="orange-border ui-widget-content">
	<table class="list-table ui-table shopping-cart-list">
	    <tr>
		    <th style="width:25px;"></th>
		    <th><span class="pad-l5">Item</span></th>
	        <th style="width:50px;" class="txt-l"><span class="pad-l5">Price</span></th>
	        <th style="width:85px;"></th>
	    </tr>
	    <?
	    echo $this->element('table_accent_row', array('cols'=>4));
	    if(sizeof($order['OrdersItem']) > 0) {
		    foreach($order['OrdersItem'] as $item): ?>
		    	<tr>
		    		<td>(<?=$item['OrdersItem']['qty']?>)</td>
		    		<td>
		    			<div class="green b marg-b5"><?=$item['Item']['title']?></div>
		    			<div class=""><?=$item['Item']['description']?></div>
		    			<?php 
		    				if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
		    					?><ul class='no-list base marg-t5 red'><?php 
		    					foreach($item['ItemsOption'] as $opt):
									echo '<li>'.$opt['label'];
									if($opt['OrdersItemsItemsOption']['price'] > 0) {
										echo '<span class="">(+$'.number_format($opt['OrdersItemsItemsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					
		    					foreach($item['LocationsCatsOption'] as $opt):
									echo '<li>'.$opt['label'];
									if($opt['OrdersItemsLocationsCatsOption']['price'] > 0) {
										echo '<span class="pad-l5">(+$'.number_format($opt['OrdersItemsLocationsCatsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					?></ul><?php
		    				}
		    				
		    				if(!empty($item['OrdersItem']['comments'])) {
		    					?><div class="marg-t5 red">Additional Instructions:<span class="italic pad-l5">"<?php echo $item['OrdersItem']['comments']; ?>"</span></div><?php 
		    				}
		    			?>
		    		</td>
		    		<td><?php echo $number->currency($item['OrdersItem']['total']); ?></td>
		    		<td style="width:90px;">
		    			<?php //echo $jquery->btn('/orders_items/edit/'.$item['OrdersItem']['id'], 'ui-icon-pencil', 'Edit', null, 'btn-menu-edit', 'menu_item-'.$item['OrdersItem']['id'], '85'); ?>
		    			<?php echo $jquery->btn('/orders_items/delete/'.$item['OrdersItem']['id'], 'ui-icon-trash', 'Remove', 'Remove this item from your order?', '', null, '85'); ?>
		    		</td>
		    	</tr><?
		    endforeach;
	    }
	    
		if(sizeof($order['Pizza']) > 0) {
		    foreach($order['Pizza'] as $pizza): ?>
		    	<tr>
		    		<td>(<?=$pizza['qty']?>)</td>
		    		<td>
		    			<div class="green b">
		    				<?php echo $pizza['Size']['size'].'" '.$pizza['Size']['title'].' Pizza'; ?>
		    			</div>
	    				<?php 
	    				$lines = array();
						if($pizza['Crust']['LocationsCrust']['price'] > 0) {
	    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
	    				} else {
	    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
	    				}
	    				
	    				if($pizza['sauce'] == 'Extra Sauce') {
	    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
	    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
	    				
						if($pizza['cheese'] == 'Extra Cheese') {
	    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
	    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
	    				
	    				if(sizeof($pizza['Topping']) > 0) {
							foreach($pizza['Topping'] as $topping):
								$l = 'Add '.$topping['Topping']['title'];
								if($topping['PizzasTopping']['placement'] != 'Whole') {
									$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
								}
								
								if($topping['PizzasTopping']['price'] > 0) {
									$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
								}
								
								$lines[] = $l;
							endforeach;
	    				}
	    				
	    				if(sizeof($lines) > 0) {
	    					echo "<div class='base red marg-t5'><ul class='no-list'>";
							foreach($lines as $line):
								echo "<li>".$line."</li>";
							endforeach;
	    					echo "</ul></div>";
	    				}
		    				 
						if(!empty($pizza['comments'])) {
		    				?><div class="marg-t5 red">Additional Instructions:<span class="italic pad-l5">"<?php echo $pizza['comments']; ?>"</span></div><?php 
		    			} ?>
		    		</td>
		    		<td><?php echo $number->currency($pizza['total']); ?></td>
		    		<td>
		    			<?php //echo $jquery->btn('/pizzas/edit/'.$pizza['id'], 'ui-icon-pencil', 'Edit', null, 'btn-menu-edit', 'menu_pizza-'.$pizza['id'], '80'); ?>
		    			<?php echo $jquery->btn('/pizzas/delete/'.$pizza['id'], 'ui-icon-trash', 'Remove', 'Remove this pizza from your order?', '', null, '85'); ?>
		    		</td>
		    	</tr><?	
		    endforeach;
	    }
	    
	    if(sizeof($order['OrdersSpecialsSize']) > 0) {
	    foreach($order['OrdersSpecialsSize'] as $pizza): ?>
	    	<tr>
	    		<td>(<?=$pizza['qty']?>)</td>
	    		<td>
	    			<div class="green b">
	    				<?php echo $pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Specialty Pizza"; ?>
	    			</div>
	    			<?php 
    				$lines = array();
    				if($pizza['sauce'] == 'Extra Sauce') {
    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
    				
					if($pizza['cheese'] == 'Extra Cheese') {
    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
    				
    				if($pizza['Crust']['LocationsCrust']['price'] > 0) {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
    				} else {
    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
    				}
    				/*
    				if(sizeof($pizza['Topping']) > 0) {
						foreach($pizza['Topping'] as $topping):
							$l = 'Add '.$topping['Topping']['title'];
							if($topping['PizzasTopping']['placement'] != 'Whole') {
								$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
							}
							
							if($topping['PizzasTopping']['price'] > 0) {
								$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
							}
							
							$lines[] = $l;
						endforeach;
    				} */
    				
    				if(sizeof($lines) > 0) {
    					echo "<div class='base red marg-t5'><ul class='no-list'>";
						foreach($lines as $line):
							echo "<li>".$line."</li>";
						endforeach;
    					echo "</ul></div>";
    				}
	    				 
					if(!empty($pizza['comments'])) {
	    				?><div class="marg-t5 red">Additional Instructions:<span class="italic pad-l5">"<?php echo $pizza['comments']; ?>"</span></div><?php 
	    			} ?>
	    		</td>
	    		<td><?php echo $number->currency($pizza['total']); ?></td>
	    		<td>
	    			<?php //echo $jquery->btn('/orders_specials_sizes/edit/'.$pizza['id'], 'ui-icon-pencil', 'Edit', null, 'btn-menu-edit', 'menu_special-'.$pizza['id'], '80'); ?>
	    			<?php echo $jquery->btn('/orders_specials_sizes/delete/'.$pizza['id'], 'ui-icon-trash', 'Remove', 'Remove this specialty pizza from your order?', '', null, '85'); ?>
	    		</td>
	    	</tr><?	
	    endforeach;
	    }
	    
	    if(sizeof($order['OrdersCombo']) > 0) {
	    	foreach($order['OrdersCombo'] as $combo_index => $combo): ?>
	    		<tr>
		    		<td>(<?=$combo['qty']?>)</td>
		    		<td>
		    			<div class="green b marg-b5"><?=$combo['OrdersCombo']['Combo']['title']?> Combo</div>
		    			<div class=""><?=$combo['OrdersCombo']['Combo']['description']?></div>
		    			<?php
							if(sizeof($combo['OrdersCombo']['OrdersCombosPizza']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosPizza'] as $pizza):
									$lines = array();
				    				if($pizza['sauce'] == 'Extra Sauce') {
				    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
				    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
				    				
									if($pizza['cheese'] == 'Extra Cheese') {
				    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
				    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
				    				
				    				if($pizza['crust_price'] > 0) {
				    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
				    				} else {
				    					$lines[] = $pizza['Crust']['title'].' Crust';
				    				}
				    				
				    				if(sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['title'];
											if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
											}
											
											if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
												$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
											}
											
											$lines[] = $l;
										endforeach;
				    				}
				    				
			    					echo "<div class='pad-l20 marg-t10 green'>".$pizza['Size']['Size']['size']."\" ".$pizza['Size']['Size']['title']." Pizza</div>";
			    					echo "<div class='pad-l20 base red'><ul class='no-list'>";
									foreach($lines as $line):
										echo "<li>".$line."</li>";
									endforeach;
			    					echo "</ul></div>";
								endforeach;
							}
		    			
							if(sizeof($combo['OrdersCombo']['OrdersCombosSpecialsSize']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosSpecialsSize'] as $pizza):
									$lines = array();
				    				if($pizza['sauce'] == 'Extra Sauce') {
				    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
				    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
				    				
									if($pizza['cheese'] == 'Extra Cheese') {
				    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
				    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
				    				
				    				if($pizza['crust_price'] > 0) {
				    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
				    				} else {
				    					$lines[] = $pizza['Crust']['title'].' Crust';
				    				}
				    				/*
				    				if(sizeof($pizza['Topping']) > 0) {
										foreach($pizza['Topping'] as $topping):
											$l = 'Add '.$topping['title'];
											if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
												$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
											}
											
											if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
												$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
											}
											
											$lines[] = $l;
										endforeach;
				    				}
				    				*/
				    				
			    					echo "<div class='pad-l20 marg-t10 green'>".$pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Pizza</div>";
			    					if(!empty($pizza['Special']['subtext'])) {
			    						echo "<div class='pad-l20 base orange'>".$pizza['Special']['subtext']."</div>";	
			    					}
			    					echo "<div class='pad-l20 base red'><ul class='no-list'>";
									foreach($lines as $line):
										echo "<li>".$line."</li>";
									endforeach;
			    					echo "</ul></div>";
								endforeach;
							}
							
	    					if(sizeof($combo['OrdersCombo']['OrdersCombosItem']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosItem'] as $item): ?>
									<div class='pad-l20 marg-t10 green'><?php echo $item['Item']['title']; ?></div>
									<div class="pad-l20 base orange"><?php echo $item['Item']['description']; ?></div><?php

			    					if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
				    					?><div class='pad-l20'><ul class='no-list base red'><?php 
				    					foreach($item['ItemsOption'] as $opt):
											echo '<li>'.$opt['label'];
											if($opt['OrdersCombosItemsItemsOption']['price'] > 0) {
												echo '<span class="">(+$'.number_format($opt['OrdersCombosItemsItemsOption']['price'], 2).')</span>';	
											}	
				    					endforeach;
				    					
				    					foreach($item['LocationsCatsOption'] as $opt):
											echo '<li>'.$opt['label'];
											if($opt['OrdersCombosItemsLocationsCatsOption']['price'] > 0) {
												echo '<span class="pad-l5">(+$'.number_format($opt['OrdersCombosItemsLocationsCatsOption']['price'], 2).')</span>';	
											}	
				    					endforeach;
				    					?></ul></div><?php
				    				}
								endforeach;
							}
							
							if(sizeof($combo['OrdersCombo']['OrdersCombosTextItem']) > 0) {
								foreach($combo['OrdersCombo']['OrdersCombosTextItem'] as $item):
									?>
									<div class='pad-l20 marg-t10 green'><?php echo $item['CombosTextItem']['text']; ?></div>
									<?php if(!empty($item['comments'])) { ?>
										<div class="base b red pad-l20"><?php echo $item['comments']; ?></div>
									<?php } 
								endforeach;
							}
						
		    				if(!empty($combo['OrdersCombo']['OrdersCombo']['comments'])) {
		    					?><div class="marg-t10 red">Additional Instructions:<span class="italic pad-l5">"<?php echo $combo['OrdersCombo']['OrdersCombo']['comments']; ?>"</span></div><?php 
		    				}
		    				
		    			?>
		    		</td>
		    		<td><?php echo $number->currency($combo['OrdersCombo']['OrdersCombo']['total']); ?></td>
		    		<td>
		    			<?php //echo $jquery->btn('/orders_combos/edit/'.$combo['OrdersCombo']['OrdersCombo']['id'], 'ui-icon-pencil', 'Edit', null, 'btn-menu-edit', 'menu_item-'.$combo['OrdersCombo']['OrdersCombo']['id'], '80'); ?>
		    			<?php echo $jquery->btn('/orders_combos/delete/'.$combo['OrdersCombo']['OrdersCombo']['id'], 'ui-icon-trash', 'Remove', 'Remove this item from your order?', '', null, '85'); ?>
		    		</td>
		    	</tr><?
	    	endforeach;
	    }
	    
	    if(sizeof($order['OrdersItem']) <= 0 && sizeof($order['Pizza']) <= 0 && sizeof($order['OrdersSpecialsSize']) <= 0 && sizeof($order['Combo']) <= 0) {
	    	?><tr><td colspan="4">Your order is empty.</td></tr><?
	    } 
	    ?>
	    </table>
	    <!-- list-table ui-table shopping-cart-list -->
	    <table class="list-table  marg-t10">
		    <?php echo $this->element('table_accent_row', array('cols'=>2)); ?>
		    <?php $tax = ($order['Location']['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal']; ?>
		    <tr>
		    	<td><strong>Subtotal</strong></td>
		        <td style="width:135px;"><?php echo $number->currency($order['Order']['subtotal']); ?></td>
		    </tr>
		    <tr>
		        <td><strong>Tax (<?php echo $order['Location']['ZipCode']['tax_rate']; ?>%)</strong></td>
		        <td><?php echo $number->currency($tax); ?></td>
		    </tr>
		    <?php echo $this->element('table_accent_row', array('cols'=>2)); ?>
	    </table>
	       
	    <!-- if order is for pickup -->
	    <div id="pickupInfoDiv">
	    	<table class="list-table marg-t10">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr>
				        <td class="red">
				        	<span>Discount Applied (<?php echo $order['DiscountCode']['code']?>)</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td class="red" style="width:135px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr>
			    	<td><span class="b">Order Fee</span></td>
			    	<td style="width:135px;">$<?php echo number_format($order['Order']['oas_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr>
			        <td><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
	   
	    <!-- if order is for delivery -->
	    <div id="deliveryInfoDiv">
	    	<table class="list-table marg-t10">
			    <?php $total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] + $order['Location']['delivery_charge'] - $order['Order']['discount_amt'];  ?>
			    <?php if($order['Order']['discount_amt'] > 0) { ?>
				    <tr>
				        <td class="red">
				        	<span>Discount Applied (<?php echo $order['DiscountCode']['code']?>)</span>
				        	<?php if($order['DiscountCode']['discount_type'] == 'Percent') { ?>
				        		<span><?php echo $order['DiscountCode']['discount_amt'] * 100; ?>% OFF</span>
				        	<?php } ?>
				        </td>
				        <td class="red" style="width:135px;"><?php echo '-'.$number->currency($order['Order']['discount_amt']); ?></td>
				    </tr>
			    <?php } ?>
			    
			    <tr>
			    	<td><span class="b">Delivery Fee</span></td>
			    	<td style="width:135px;">$<?php echo number_format($order['Order']['oas_charge'] + $order['Location']['delivery_charge'], 2); ?></td>
			    </tr>			    
			    
			    <tr>
			        <td><span class="lg green">TOTAL</span></td>
			        <td><?php echo $number->currency($total); ?></td>
			    </tr>
			</table>
	    </div>
	    
	</div>
	<div class="ui-helper-clearfix marg-t10">
		<div class="right">
			<div class="submitProgress"><?php echo $html->image('ajax-loader-med-bar.gif');?></div>
			<div class="submitBtn"><?php echo $jquery->btn('#', 'ui-icon-circle-check', 'Complete Order', null, 'btnOrderFinalize'); ?></div>
		</div>
	</div>
</div>

<div class="ui-helper-clearfix">
	<div class="left pad-r20"><!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="be4b0ac5-9652-492b-b868-b282d5ee8d93";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank"></a> </div></div>
	<div class="left"><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=2WI67NgaadOGusKDICsHoWm3uAEUQYFd0ltCiijqIUZdwtx7vM75"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.godaddy.com/ssl/ssl-certificates.aspx" target="_blank"></a></span></div>
</div>
<?php //echo debug($order); ?>
<?php //echo debug($session->read('User')); ?>