<?php echo $html->script('jquery/timepicker'); ?>
<?php echo $html->script(array('jquery/alphanumeric'), false); ?>
<?php echo $jquery->scrFormSubmit('#btnLocationEdit', '#formLocationEdit'); ?>
<script>
$(function() {
	$('#locationEditTabs, #locationSettingsTabs').tabs();
	$('.no-display').removeAttr('style');
	
	$('.numeric-only').numeric({ allow: "." });
	$('.numeric-time-only').numeric({ allow: ":" });
	$('#LocationLogo').uniform();
	
	$('.opening-time').timepicker({
		hourGrid: 4,
		minuteGrid: 10,
		ampm: true
	});
	
	$('.closing-time').timepicker({
		hourGrid: 4,
		minuteGrid: 10,
		ampm: true
	});

	$('#btnLocationEdit').click(function() {
		if($('#LocationAcceptDeliveries').is(':not(:checked)') && 
		   $('##LocationAcceptDeliveries')) {

		} else {

		}
	});

	$('#LocationAcceptDeliveries').click(function() { 
		if($('#LocationAcceptDeliveries').is(':not(:checked)')) {
			$('#divDeliveryFields').hide("fold");
		} else {
			$('#divDeliveryFields').slideDown('slow');
		}
	});

	$('#LocationOfferPizza1, #LocationOfferPizza0').click(function() { 
		if($(this).val() == '0') {
			$('#divPizzaFields').hide("fold");
		} else {
			$('#divPizzaFields').slideDown('slow');
		}
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<h1>Update Restaurant Information</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
<?php echo $form->create('Location', array('id'=>'formLocationEdit', 'type'=>'file')); ?>
<?php echo $form->input('id'); ?>
<?php echo $form->hidden('slug', array('value' => $location['Location']['slug'])); ?>

	<div id="locationEditTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#restaurant-settings">Settings</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#contact-info">Contact Info</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#location-info">Location Info</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#restaurant-hours">Restaurant Hours</a></li>
			<li class="ui-state-default ui-corner-top"><a href="#banner-image">Banner Image</a></li>
		</ul>
		<div id="restaurant-settings" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="ui-helper-clearfix">
				<div class="right pad-l10">
					<?php echo $jquery->btn('/locations/delete/'.$location['Location']['id'], 'ui-icon-alert', 'Remove Restaurant', 
											'Are you sure that you would like to completely remove this restaurant? WARNING: You will not be able to recover your online menu after this is complete!'); ?>
				</div>
				<?php if($location['Location']['acct_type'] == '1') { ?>
					<div class="right">
						<?php echo $jquery->btn('/locations/upgrade/'.$location['Location']['id'], 'ui-icon-arrowreturnthick-1-n', 'Upgrade to Premium Account'); ?>
					</div>
				<?php } ?>
			</div>
			
			<div id="locationSettingsTabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all marg-b10">
				<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
					<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#order-settings">Order Settings</a></li>
					<li class="ui-state-default ui-corner-top"><a href="#notification-settings">Notification Settings</a></li>
					<li class="ui-state-default ui-corner-top"><a href="#menu-settings">Menu Settings</a></li>
					<?php if($session->check('Admin')) { ?>
						<li class="ui-state-default ui-corner-top"><a href="#fees-settings">Admin Settings</a></li>	
					<?php } ?>
				</ul>
				<div id="order-settings" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
					<div id="divOrderMethodText" style="display:none;"><?php echo $this->element('error-msg-box', array('msg' => 'Please select at least one method of Online Ordering.'))?></div>				
			 		<div>
			 			<div class="ui-helper-clearfix">
		 					<div class="left pad-r5" style="padding-left:155px;"><?php echo $form->checkbox('accept_pickups');?></div>
		 					<div class="left b">Does this restaurant Accept Pickup Orders?</div>
		 				</div>
		 				<div class="ui-helper-clearfix">
		 					<div class="left pad-r5" style="padding-left:155px;"><?php echo $form->checkbox('accept_deliveries');?></div>
		 					<div class="left b">Does this restaurant Accept Delivery Orders?</div>
		 				</div>
			 		</div>
					<div id="divDeliveryFields" style="display:none;">
						<hr />
				 		<table class="form-table">
				 			<tr>
				 				<td style="width:150px;" class="label-req">Minimum Delivery Amount:</td>
				 				<td><?=$form->input('min_delivery_amt', array('label'=>'','class'=>'form-text delivery-fields-toggle', 'div'=>false))?><br />
				 				<span class="orange base">(Leave blank for no minimum order amount.)</span></td>
				 			</tr>
				 			<tr>
				 				<td class="label-req">Delivery Charge:</td>
				 				<td><?=$form->input('delivery_charge', array('label'=>'','class'=>'form-text delivery-fields-toggle', 'div'=>false))?><br />
				 				<span class="orange base">(Leave blank for no delivery charge.)</span></td>
				 			</tr>
				 			<tr>
				 				<?php $radiuses = array('10' => '10', '15' => '15', '20' => '20','25' => '25','30' => '30', '40' => '40'); ?>
				 				<td class="label-req">Delivery Radius: <span class="required">*</span></td>
				 				<td><?=$form->select('delivery_radius', $radiuses, null, array('label'=>'','class'=>'form-select delivery-fields-toggle'), false)?> miles.<br />
				 				<span class="orange base">(Set the maximum radius you will travel for deliveries.)</span></td>
				 			</tr>
				 		</table>
			 		</div>
				</div>
				<div id="notification-settings" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
					<?php if($this->data['Location']['acct_type'] == 1) { ?>
						<!-- not a premium account. Only option available is the email method weith an option to upgrade account -->
						<p>When you receive an online order, an email will be sent to your contact email with the order details. You must confirm the order, by clicking the 
						"<span class="italic b">Confirm Order</span>" link in the email, or the "<span class="italic b">Confirm Order</span>" button in your online account. 
						Orders left unconfirmed, will be refunded to the customer at the end of each day.</p>
						
						<p>NOTE: We also offer the option the confirm the order by phone. This option is offered as an additional perk for Restaurants with a  
						<?php echo $html->link('Premium Membership', '/restaurants/upgrade', array('class'=>'b')); ?> only. Take advantage for this feature and many more by <?php echo $html->link('Upgrading Your Account Today!', '/restaurants/upgrade', array('class'=>'b')); ?></p>						
					<?php } else { ?>
						<p>When you receive an online order, you must confirm it using one of the following methods. Orders left unconfirmed, will be 
						refunded to the customer.</p>
						
						<p>Using the <span class="italic b">Email Confirmation</span> method, an email will be sent to your contact email with the 
						order details. You must confirm the order, by clicking the <span class="italic b">"Confirm Order"</span> 
						link in the email or in your online account.</p> 
						
						<p>If you choose to use the <span class="italic b">Phone/Fax Confirmation</span> method, you will have the order details 
						faxed to you, along with a confirmation code and a toll free number to call. To confirm, simply call the toll free number provided, and 
						enter in the confirmation code at the prompt.</p>
						<hr />
						<?php 
						$opts = array('Email' => 'Use the Email Confirmation Method',
									  'Phone' => 'Use the Phone/Fax Confirmation Method');
						echo $form->select('confirm_method', $opts, $this->data['Location']['confirm_method'], array('class' => 'form-select no-uniform-style'), false); 
						?>
					<?php } ?>
				
					
				</div>
				<div id="menu-settings" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
					<div class="">
						<div class="ui-helper-clearfix">
							<div class="left" style="width:430px;" style="">
								<div class="b marg-b5">Would you like to display item images on your Online Menu?</div>
								<div class="txt-r"><span class="b green">Yes, Display Item Images</span> on my Online Menu:</div>
								<div class="txt-r"><span class="b red">Turn off Menu Item Images</span> (I don't want to upload images for my menu items.):</div>
							</div>
							<div class="left txt-c" style="width:70px; margin-top:18px;">
								<input type="radio" id="LocationPairHalfTopping1" name="data[Location][show_menu_images]" value="1" 
								<?php if($this->data['Location']['show_menu_images'] == '1') echo 'checked="checked"'; ?> /><br />
								<input type="radio" id="LocationPairHalfTopping0" name="data[Location][show_menu_images]" value="0" 
								<?php if($this->data['Location']['show_menu_images'] == '0') echo 'checked="checked"'; ?> />
							</div>
						</div><hr />
					
						<div class="ui-helper-clearfix">
							<div class="left" style="width:430px;" style="">
								<div class="b marg-b5">Does your restaurant sell pizza on their Online Menu?</div>
								<div class="txt-r"><span class="b green">Yes</span>, this restaurant offers pizza, turn on pizza settings in my Menu Manager:</div>
								<div class="txt-r"><span class="b red">No</span>, this restaurant <span class="b red">Does Not</span> have pizza as part of their Online Menu:</div>
							</div>
							<div class="left txt-c" style="width:70px; margin-top:18px;">
								<input type="radio" id="LocationOfferPizza1" name="data[Location][offer_pizza]" value="1" 
								<?php if($this->data['Location']['offer_pizza'] == '1') echo 'checked="checked"'; ?> /><br />
								<input type="radio" id="LocationOfferPizza0" name="data[Location][offer_pizza]" value="0" 
								<?php if($this->data['Location']['offer_pizza'] == '0') echo 'checked="checked"'; ?> />
							</div>
						</div>
						
						<div id="divPizzaFields">
							<hr />
							<div class="ui-helper-clearfix">
								<div class="left" style="width:430px;" style="">
									<div class="b marg-b5">If a single half topping is added to a pizza, how should it be charged?</div>
									<div class="txt-r"><span class="b green">Charge Full Price</span> of the topping:</div>
									<div class="txt-r"><span class="b green">Charge Half</span> of the full topping price:</div>
								</div>
								<div class="left txt-c" style="width:70px; margin-top:18px;">
									<input type="radio" id="LocationSingleHalfTopping1" name="data[Location]['single_half_topping']" value="1" 
									<?php if($this->data['Location']['single_half_topping'] == '1') echo 'checked="checked"'; ?> /><br />
									<input type="radio" id="LocationSingleHalfTopping0" name="data[Location]['single_half_topping']" value="0" 
									<?php if($this->data['Location']['single_half_topping'] == '0') echo 'checked="checked"'; ?> />
								</div>
							</div><hr />
							
							<div class="ui-helper-clearfix">
								<div class="left" style="width:430px;" style="">
									<div class="b marg-b5">If a pair of half toppings are added to a pizza, with different prices, how should it be charged?</div>
									<div class="txt-r">Charge the price of the more <span class="b green">Expensive Topping</span>:</div>
									<div class="txt-r"><span class="b green">Charge the Average</span> of the two topping prices:</div>
								</div>
								<div class="left txt-c" style="width:70px; margin-top:32px;">
									<input type="radio" id="LocationPairHalfTopping0" name="data[Location]['pair_half_toppings']" value="0" 
									<?php if($this->data['Location']['pair_half_toppings'] == '0') echo 'checked="checked"'; ?> /><br />
									<input type="radio" id="LocationPairHalfTopping1" name="data[Location]['pair_half_toppings']" value="1" 
									<?php if($this->data['Location']['pair_half_toppings'] == '1') echo 'checked="checked"'; ?> />
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if($session->check('Admin')) { ?>
				<div id="fees-settings" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			 		<table class="form-table">
			 			<tr>
			 				<td style="width:150px;" class="label-req">Consumer Per Order Fee: <span class="required">*</span></td>
			 				<td><?=$form->input('cust_per_order_fee', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?></td>
			 			</tr>
			 			<tr>
			 				<td class="label-req">Restaurant Per Order Fee: <span class="required">*</span></td>
			 				<td><?=$form->input('loc_per_order_fee', array('label'=>'','class'=>'form-text numeric-only', 'div' => false))?><br />
			 				<span class="base red">(Enter as Percentage. e.g. 10 = "10%")</span></td>
			 			</tr>
			 		</table>
				</div>
				<?php } ?>
			</div>

			<!-- 
			<fieldset class="marg-b10"><legend>Order Confirmation</legend>
				<p>When you receive an online order, an email will be sent to your contact email with the order details. You must confirm the order, by clicking the 
				"<span class="italic b">Confirm Order</span>" link in the email. Orders left unconfirmed, will be refunded to the customer at the end of each day.</p>
				
				<p>We also offer a more in depth Order Confirmation process called "<span class="b italic">Order Tracker</span>". Use of "<span class="b italic">Order Tracker</span>" 
				for order confirmations is optional, however it is highly recommended. It is designed to allow you to, quickly and efficiently, give your customers status updates on their 
				orders in "real-time", thus further streamlining employee productivity, by not having to answer order status calls.</p>
				<hr />
				<div class="ui-helper-clearfix pad-l20">
					<div class="left pad-r5"><?php //echo $form->checkbox('use_order_tracker'); ?></div>
					<div class="left b">Participate in "<span class="italic">Order Tracker</span>" for order confirmations.</div>
				</div>
			</fieldset> -->
		</div>
		<div id="contact-info" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<table class="form-table">
	 			<tr>
	 				<td style="width:150px;" class="label-req">First Name: <span class="required">*</span></td>
	 				<td><?=$form->input('contact_fname', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Last Name: <span class="required">*</span></td>
	 				<td><?=$form->input('contact_lname', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Title: <span class="required">*</span></td>
	 				<td><?=$form->input('contact_title', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Phone: <span class="required">*</span></td>
	 				<td><?=$form->input('contact_phone', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
	 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Email: <span class="required">*</span></td>
	 				<td><?=$form->input('contact_email', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 		</table>
		</div>
		<div id="location-info" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<table class="form-table">
	 			<tr>
	 				<td style="width:150px;" class="label-req">Location Name: <span class="required">*</span></td>
	 				<td><?=$form->input('name', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Slogan:</td>
	 				<td><?=$form->input('slogan', array('label'=>'','class'=>'form-text', 'div'=>false))?><br />
	 				<span class="base orange">(This will appear below the restaurant name in the banner.)</span></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Restaurant Type: <span class="required">*</span></td>
	 				<td><?=$form->select('food_cat_id', $food_cats, null, array('label'=>'','class'=>'form-text'), false)?><br />
	 				<span class="base orange">(Select main cuisine served.)</span></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Logo: </td>
	 				<td><?=$form->input('logo_temp', array("label" => "",'class'=>'form-text', "type"  => "file", 'id'=>'LocationLogo'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Address: <span class="required">*</span></td>
					<td><?=$form->input('address1', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Address2:</td>
	 				<td><?=$form->input('address2', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">City: <span class="required">*</span></td>
	 				<td><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">State: <span class="required">*</span></td>
	 				<td><?=$form->input('state_id', array('label'=>'','class'=>'form-text'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Zip: <span class="required">*</span></td>
	 				<td><?=$form->input('zip_code_id', array('label'=>'','class'=>'form-text numeric-only'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Phone: <span class="required">*</span></td>
	 				<td><?=$form->input('phone', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
	 				<span class="base orange">(Digits only. No hypens or parenthesis.)</span></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Fax:</td>
	 				<td><?=$form->input('fax', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
	 				<span class="base orange">(Digits only. No hypens or parenthesis.)</span></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">SMS Capable Number:</td>
	 				<td><?=$form->input('sms', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
	 				<span class="base orange">(Digits only. No hypens or parenthesis.)</span></td>
	 			</tr>
	 		</table>
		</div>
		<div id="restaurant-hours" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<p>Please enter the operating hours for any day that you would like to accept Online Orders. 
	 		If your restaurant is closed on a certain day, or to manually disable Online Orders for a particular day, such as an upcoming holiday, 
	 		check the box next to the appropriate day(s). <br />(You will be allowed to change these at any time in the future.)</p>
	 		
	 		<div>
	 			<div class="ui-helper-clearfix marg-b5 base">
	 				<div class="left pad-r5" style="width:110px">&nbsp;</div>
	 				<div class="left b" style="width:80px">Opening Time</div>
	 				<div class="left b" style="width:80px">Closing Time</div>
	 				<div class="left b" style="width:100px">Opening Time (2)**</div>
	 				<div class="left b" style="width:100px">Closing Time (2)**</div>
	 				<div class="left b" style="width:70px">Store Closed</div>	
	 			</div>
	 			<?php foreach($this->data['LocationsTime'] as $day): ?>
	 				<?php echo $form->hidden('Location.days.'.$day['LocationsTime']['day'].'.id', 
	 										 array('value' => $day['LocationsTime']['id'])); ?>
	 				<div class="ui-helper-clearfix marg-b5">
	 					
	 					<div class="left label-req pad-r5" style="width:110px"><?php echo $day['LocationsTime']['day']; ?>:</div>
	 					<div class="left" style="width:80px">
	 						<?php echo $form->input('Location.days.'.$day['LocationsTime']['day'].'.time_open', 
	 											 array('label'=>'','class'=>'form-text opening-time numeric-time-only', 'type'=>'text', 
	 							  					   'style'=>'width:60px', 'div'=>false, 'value'=>$misc->timeFromMilitary($day['LocationsTime']['time_open']))); ?>
	 					</div>
	 					<div class="left" style="width:80px">
	 						<?php echo $form->input('Location.days.'.$day['LocationsTime']['day'].'.time_closed', 
	 										     array('label'=>'','class'=>'form-text closing-time numeric-time-only', 
													   'type'=>'text', 'style'=>'width:60px', 'div'=>false,
	 												   'value'=>$misc->timeFromMilitary($day['LocationsTime']['time_closed']))); ?>
	 					</div>
	 					<div class="left" style="width:100px">
	 						<?php echo $form->input('Location.days.'.$day['LocationsTime']['day'].'.time_open2', 
	 											 array('label'=>'','class'=>'form-text opening-time numeric-time-only', 
													   'type'=>'text', 'style'=>'width:80px', 'div'=>false,
	 											 	   'value'=>$misc->timeFromMilitary($day['LocationsTime']['time_open2']))); ?>
						</div>
	 					<div class="left" style="width:100px">
	 						<?php echo $form->input('Location.days.'.$day['LocationsTime']['day'].'.time_closed2', 
	 											 array('label'=>'','class'=>'form-text closing-time numeric-time-only', 
													   'type'=>'text', 'style'=>'width:80px', 'div'=>false,
	 											 	   'value'=>$misc->timeFromMilitary($day['LocationsTime']['time_closed2']))); ?>
	 					</div>
	 					<div class="left" style="width:70px">
	 						<?php $opts = ($day['LocationsTime']['closed'] == '1') ? array('checked'=>'checked', 'div' => false) : array('div' => false);?>
	 						<?php echo $form->checkbox('Location.days.'.$day['LocationsTime']['day'].'.closed', $opts); ?>
	 					</div>
	 				</div>
	 			<?php endforeach; ?>
	 		</div>
	 		<a class="">** To provide maximum flexibility for your restaurants' schedule, we allow you to set an optional second opening and closing time, 
	 		for any day(s) you would like. Use these additional time slots if this restaurant closes for a portion of the day, and then reopens.  
	 		(e.g. You are open past midnight on given days, or your	location closes for a portion of the day for lunch.)</p>
		</div>
		<div id="banner-image" class="ui-tabs-panel ui-widget-content ui-corner-bottom no-display" style="display:none;">
			<input name="data[Location][banner_id]" id="LocationBannerId_" value="" type="hidden">
	 		<div class="ui-helper-clearfix">
	 			<?php foreach($banners as $id => $filename): ?>
	 			<?php $checked = ($this->data['Location']['banner_id'] == $id) ? 'checked="checked"' : ''; ?>
	 			<div class="left marg-b10" style="width:270px;">
	 				<div class="ui-helper-clearfix">
	 					<div class="left"><input name="data[Location][banner_id]" <?php echo $checked; ?> id="LocationBannerId<?php echo $id; ?>" value="<?php echo $id; ?>" type="radio"></div>
	 					<div class="left"><?php echo $html->image('banners/'.$filename, array('style'=>'width:240px;', 'class'=>'orange-border pad2'))?></div>
	 				</div>
	 			</div>
	 			<?php endforeach; ?>
	 		</div>
	 		
	 		<hr />
			<div class="ui-helper-clearfix">
				<div class="left" style="width:450px;" style="">
					<div class="b marg-b5">Would you like to use the restaurants logo in the banner?</div>
					<div class="txt-r"><span class="b green">No</span>, I prefer text:</div>
					<div class="txt-r"><span class="b green">Yes</span>, try using my restaurant logo in the banner:</div>
					<div class="txt-r"><span class="b green">Use the Main Account Logo</span>, instead of my restaurant logo:</div>
				</div>
				<div class="left txt-c" style="width:70px; margin-top:18px;">
					<input type="radio" id="LocationUseLogoInBanner0" name="data[Location][use_logo_in_banner]" value="0" 
					<?php if($this->data['Location']['use_logo_in_banner'] == '0') echo 'checked="checked"'; ?> /><br />
					<input type="radio" id="LocationUseLogoInBanner1" name="data[Location][use_logo_in_banner]" value="1" 
					<?php if($this->data['Location']['use_logo_in_banner'] == '1') echo 'checked="checked"'; ?> /><br />
					<input type="radio" id="LocationUseLogoInBanner2" name="data[Location][use_logo_in_banner]" value="2" 
					<?php if($this->data['Location']['use_logo_in_banner'] == '2') echo 'checked="checked"'; ?> />
				</div>
			</div>
		</div>
	</div>
	
	<div class="ui-helper-clearfix">
 		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-pencil', 'Update Restaurant', null, null, 'btnLocationEdit', 145); ?></div>
 	</div>
 	<?php echo $form->end(); ?>
</div>
<?php //echo debug($this->data); ?>
<script>
$(function() { 
	if($('#LocationAcceptDeliveries').is(':checked')) {
		$('#divDeliveryFields').slideDown("slow");
	}
});
</script>