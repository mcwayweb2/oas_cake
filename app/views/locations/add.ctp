<?php echo $html->script('jquery/timepicker'); ?>
<?php echo $html->script(array('jquery/alphanumeric'), false); ?>

<script>
$(function() {
	$('#divOrderMethodText').hide();
	$('.numeric-only').numeric({ allow: "." });
	$('.numeric-time-only').numeric({ allow: ":" });
	$('#LocationLogo').uniform();
	
	$('.opening-time, .opening-time2').timepicker({
		hourGrid: 4,
		minuteGrid: 10,
		ampm: true
	});
	
	$('.closing-time, .closing-time2').timepicker({
		hourGrid: 4,
		minuteGrid: 10,
		ampm: true
	});

	$('#LocationTimeOpenAll, #LocationTimeClosedAll, #LocationTimeOpen2All, #LocationTimeClosed2All').change(function() {
		var copyValue = $(this).val(); 
		$('.' + $(this).attr('associationClass')).each(function(index) {
			$(this).val(copyValue);
		})
	});
	
	$('.btnLocationAdd').click(function() { 
		if($('#LocationAcceptDeliveries').is(':not(:checked)') && $('#LocationAcceptPickups').is(':not(:checked)')) {
			$('#divOrderMethodText').show();
			$('html, body').animate( { scrollTop: 800 }, 'fast' );
		} else {
			$('#formLocationAdd').submit();
		}
		return false;
	});

	$('#LocationAcceptDeliveries').click(function() { 
		if($(this).attr('checked') === true) { $('#deliveryFields').slideDown('fast'); } 
		else { $('#deliveryFields').hide("fold"); }
	});

	$('#LocationCopyContactInfo').click(function() { 
		if($('#LocationCopyContactInfo').is(':checked')) {
			$('#LocationContactFname').val('<?php echo $session->read('Advertiser.contact_fname'); ?>');
			$('#LocationContactLname').val('<?php echo $session->read('Advertiser.contact_lname'); ?>');
			$('#LocationContactTitle').val('<?php echo $session->read('Advertiser.contact_title'); ?>');
			$('#LocationContactPhone').val('<?php echo $session->read('Advertiser.contact_phone'); ?>');
			$('#LocationContactEmail').val('<?php echo $session->read('Advertiser.email'); ?>');
		}
	});
});
</script>
<div class="ui-widget-header ui-corner-top pad5">
	<h1>Add New Restaurant Location</h1>
</div>
<div class="ui-widget-content ui-corner-bottom">
<?php echo $form->create('Location', array('id'=>'formLocationAdd', 'type'=>'file')); ?>
	<fieldset class="marg-b10">
 		<legend><?php __('Contact Information');?></legend>
 		<table class="form-table">
 			<tr><td colspan="2">
 				<div class="ui-helper-clearfix" style="padding-left: 30px;">
 					<div class="left pad-r5"><?php echo $form->checkbox('copy_contact_info'); ?></div>
 					<div class="left b">This contact info is the same as my primary contact.</div>
 				</div>
 				</td>
 			</tr>
 			<tr>
 				<td style="width:150px;" class="label-req">First Name: <span class="required">*</span></td>
 				<td><?=$form->input('contact_fname', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Last Name: <span class="required">*</span></td>
 				<td><?=$form->input('contact_lname', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Title: <span class="required">*</span></td>
 				<td><?=$form->input('contact_title', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Phone: <span class="required">*</span></td>
 				<td><?=$form->input('contact_phone', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Email: <span class="required">*</span></td>
 				<td><?=$form->input('contact_email', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 		</table>
	</fieldset>
	
	<?php if($session->check('Admin')) { ?>
		<fieldset class="marg-b10">
	 		<legend><?php __('Admin Fee Options');?></legend>
	 		<table class="form-table">
	 			<tr>
	 				<td style="width:150px;" class="label-req">Consumer Per Order Fee: <span class="required">*</span></td>
	 				<td><?=$form->input('cust_per_order_fee', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false, 'value' => '0.00'))?></td>
	 			</tr>
	 			<tr>
	 				<td class="label-req">Restaurant Per Order Fee: <span class="required">*</span></td>
	 				<td><?=$form->input('loc_per_order_fee', array('label'=>'','class'=>'form-text numeric-only', 'div' => false, 'value' => '10'))?><br />
	 				<span class="base red">(Enter as Percentage. e.g. 10 = "10%")</span></td>
	 			</tr>
	 		</table>
		</fieldset>
	<?php } ?>
	
	<fieldset class="marg-b10">
 		<legend><?php __('Location Information');?></legend>
 		<table class="form-table">
 			<tr>
 				<td style="width:150px;" class="label-req">Location Name: <span class="required">*</span></td>
 				<td><?=$form->input('name', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Slogan:</td>
 				<td><?=$form->input('slogan', array('label'=>'','class'=>'form-text', 'div'=>false))?><br />
 				<span class="base orange">(This will appear below the restaurant name in the banner.)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Restaurant Type: <span class="required">*</span></td>
 				<td><?=$form->select('food_cat_id', $food_cats, null, array('label'=>'','class'=>'form-text'), false)?><br />
 				<span class="base orange">(Select main cuisine served.)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Logo: </td>
 				<td><?=$form->input('logo', array("label" => "",'class'=>'form-text', "type"  => "file", 'id'=>'LocationLogo'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Address: <span class="required">*</span></td>
				<td><?=$form->input('address1', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Address2:</td>
 				<td><?=$form->input('address2', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">City: <span class="required">*</span></td>
 				<td><?=$form->input('city', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">State: <span class="required">*</span></td>
 				<td><?=$form->input('state_id', array('label'=>'','class'=>'form-text'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Zip: <span class="required">*</span></td>
 				<td><?=$form->input('zip_code_id', array('label'=>'','class'=>'form-text numeric-only'))?></td>
 			</tr>
 			<tr>
 				<td class="label-req">Phone: <span class="required">*</span></td>
 				<td><?=$form->input('phone', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Fax:</td>
 				<td><?=$form->input('fax', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">SMS Capable Number:</td>
 				<td><?=$form->input('sms', array('label'=>'','class'=>'form-text numeric-only', 'div'=>false))?><br />
 				<span class="base orange">(Digits only. No hypens or parenthesis)</span></td>
 			</tr>
 		</table>
 	</fieldset>
 	
 	<fieldset class="marg-b10">
 		<legend><?php __('Restaurant Hours');?></legend>
 		<p>Please enter the operating hours for any day that you would like to accept Online Orders. 
 		If your restaurant is closed on a certain day, or to manually disable Online Orders for a particular day, such as an upcoming holiday, 
 		check the box next to the appropriate day(s). If an opening/closing time is the same for all days of the week, you can enter the time in 
 		the corresponding input box titled <span class="b italic">"Copy To All Days"</span>. (NOTE: You will be allowed to change these at any time in the future.)</p>
 		
 		<div>
 			<div class="ui-helper-clearfix marg-b5 base">
 				<div class="left pad-r5" style="width:110px">&nbsp;</div>
 				<div class="left b" style="width:80px">Opening Time</div>
 				<div class="left b" style="width:80px">Closing Time</div>
 				<div class="left b" style="width:100px">Opening Time (2)**</div>
 				<div class="left b" style="width:100px">Closing Time (2)**</div>
 				<div class="left b" style="width:70px">Store Closed</div>	
 			</div>
 			<?php $days = array('Mon' => 'Monday', 'Tues' => 'Tuesday', 'Wed' => 'Wednesday', 'Thur' => 'Thursday',
 								'Fri' => 'Friday', 'Sat'  => 'Saturday','Sun' => 'Sunday'); ?>
 								
 			<div class="ui-helper-clearfix marg-b5">
 				<div class="left label-req pad-r5 red" style="width:110px">(Copy To All Days):</div>
 				<div class="left" style="width:80px">
 					<?php echo $form->input('time_open_all', array('label'=>'','class'=>'form-text opening-time numeric-time-only', 
										 						'type'=>'text', 'style'=>'width:60px', 'div'=>false, 
										 						'associationClass' => 'opening-time')); ?>
				</div>
 				<div class="left" style="width:80px">
 					<?php echo $form->input('time_closed_all', array('label'=>'','class'=>'form-text closing-time numeric-time-only', 
																  'type'=>'text', 'style'=>'width:60px', 'div'=>false,
 																  'associationClass' => 'closing-time')); ?>
 				</div>
 				<div class="left" style="width:100px">
 					<?php echo $form->input('time_open2_all', array('label'=>'','class'=>'form-text opening-time2 numeric-time-only', 
																 'type'=>'text', 'style'=>'width:80px', 'div'=>false,
 																 'associationClass' => 'opening-time2')); ?>
				</div>
 				<div class="left" style="width:100px">
 					<?php echo $form->input('time_closed2_all', array('label'=>'','class'=>'form-text closing-time2 numeric-time-only', 
																	'type'=>'text', 'style'=>'width:80px', 'div'=>false,
 																    'associationClass' => 'closing-time2')); ?>
 				</div>
 			</div><hr />
 			
 			
 			<?php foreach($days as $abbr =>  $day): ?>
 				<div class="ui-helper-clearfix marg-b5">
 					<div class="left label-req pad-r5" style="width:110px"><?php echo $day; ?>:</div>
 					<div class="left" style="width:80px">
 						<?php echo $form->input('Location.days.'.$abbr.'.time_open', array('label'=>'','class'=>'form-text opening-time numeric-time-only', 
																		 		   'type'=>'text', 'style'=>'width:60px', 'div'=>false)); ?>
					</div>
 					<div class="left" style="width:80px">
 						<?php echo $form->input('Location.days.'.$abbr.'.time_closed', array('label'=>'','class'=>'form-text closing-time numeric-time-only', 
																		 		   'type'=>'text', 'style'=>'width:60px', 'div'=>false)); ?>
 					</div>
 					<div class="left" style="width:100px">
 						<?php echo $form->input('Location.days.'.$abbr.'.time_open2', array('label'=>'','class'=>'form-text opening-time2 numeric-time-only', 
																		 		   'type'=>'text', 'style'=>'width:80px', 'div'=>false)); ?>
					</div>
 					<div class="left" style="width:100px">
 						<?php echo $form->input('Location.days.'.$abbr.'.time_closed2', array('label'=>'','class'=>'form-text closing-time2 numeric-time-only', 
																		 		   'type'=>'text', 'style'=>'width:80px', 'div'=>false)); ?>
 					</div>
 					<div class="left" style="width:70px">
 						<?php echo $form->checkbox('Location.days.'.$abbr.'.closed', array('div'=>false)); ?>
 					</div>
 				</div>
 			<?php endforeach; ?>
 		</div>
 		<a class="">** To provide maximum flexibility for your restaurants' schedule, we allow you to set an optional second opening and closing time, 
 		for any day(s) you would like.Use these additional time slots if this restaurant closes for a portion of the day, and then reopens.  
 		(e.g. You are open past midnight on given days, or your	location closes for a portion of the day for lunch.)</p>
 	</fieldset>
 	
 	<fieldset class="marg-b10">
 		<legend><?php __('Order Settings');?></legend>
 		<div id="divOrderMethodText"><?php echo $this->element('error-msg-box', array('msg' => 'Please select at least one method of Online Ordering.'))?></div>
 		<table class="form-table">
 			<tr><td style="width:150px;"></td>
 				<td><div class="ui-helper-clearfix">
 						<div class="left pad-r5"><?php echo $form->checkbox('accept_pickups', array('checked'=>'checked'));?></div>
 						<div class="left b">Does this restaurant Accept Pickup Orders?</div>
 					</div>
 				</td>
 			</tr>
 			<tr><td></td>
 				<td><div class="ui-helper-clearfix">
 						<div class="left pad-r5"><?php echo $form->checkbox('accept_deliveries', array('checked'=>'checked'));?></div>
 						<div class="left b">Does this restaurant Accept Delivery Orders?</div>
 					</div>
 				</td>
 			</tr>
 		</table>
 		<div id="deliveryFields">
 		<table class="form-table">
 			<tr>
 				<td style="width:150px;" class="label-req">Minimum Delivery Amount:</td>
 				<td><?=$form->input('min_delivery_amt', array('label'=>'','class'=>'form-text', 'div'=>false))?><br />
 				<span class="orange base">(Leave blank for no minimum order amount.)</span></td>
 			</tr>
 			<tr>
 				<td class="label-req">Delivery Charge:</td>
 				<td><?=$form->input('delivery_charge', array('label'=>'','class'=>'form-text', 'div'=>false))?><br />
 				<span class="orange base">(Leave blank for no delivery charge.)</span></td>
 			</tr>
 			<tr>
 				<?php $radiuses = array('2' => '2', '5' => '5', '10' => '10', '15' => '15', '20' => '20','25' => '25','30' => '30', '40' => '40'); ?>
 				<td class="label-req">Delivery Radius: <span class="required">*</span></td>
 				<td><?=$form->select('delivery_radius', $radiuses, null, array('label'=>'','class'=>'form-select'), false)?> miles.<br />
 				<span class="orange base">(Set the maximum radius you will travel for deliveries.)</span></td>
 			</tr>
 		</table>
 		</div>
 	</fieldset>
 	
 	<div class="ui-helper-clearfix">
 		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Restaurant', null, 'btnLocationAdd', null, 125); ?></div>
 	</div>
 	
 	<fieldset class="marg-b10" style="padding-top:10px;">
 		<legend><?php __('Select your Banner');?></legend>
 		<input name="data[Location][banner_id]" id="LocationBannerId_" value="" type="hidden">
 		<div class="ui-helper-clearfix">
 			<?php foreach($banners as $id => $filename): ?>
 			<?php $checked = ($id == '1') ? 'checked="checked"' : ''; ?>
 			<div class="left marg-b10" style="width:280px;">
 				<div class="ui-helper-clearfix">
 					<div class="left"><input name="data[Location][banner_id]" <?php echo $checked; ?> id="LocationBannerId<?php echo $id; ?>" value="<?php echo $id; ?>" type="radio"></div>
 					<div class="left"><?php echo $html->image('banners/'.$filename, array('style'=>'width:250px;', 'class'=>'orange-border pad2'))?></div>
 				</div>
 			</div>
 			<?php endforeach; ?>
 		</div>
 	</fieldset>
 	
 	<div class="ui-helper-clearfix">
 		<div class="right"><?php echo $jquery->btn('#', 'ui-icon-circle-plus', 'Add Restaurant', null, 'btnLocationAdd', null, 125); ?></div>
 	</div>
 	<?php echo $form->end(); ?>
</div>