<?php
class AppModel extends Model {
    var $actsAs = array('Containable');
	
    function getDayAbbr() {
   		switch(date('l')) {
			case 'Monday': return 'Mon';
			case 'Tuesday': return 'Tues';
			case 'Wednesday': return 'Wed';
			case 'Thursday': return 'Thur';
			case 'Friday': return 'Fri';
			case 'Saturday': return 'Sat';
			case 'Sunday': return 'Sun';
		}
    }
}
?>