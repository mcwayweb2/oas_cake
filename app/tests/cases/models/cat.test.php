<?php 
/* SVN FILE: $Id$ */
/* Cat Test cases generated on: 2009-02-01 12:02:48 : 1233518448*/
App::import('Model', 'Cat');

class CatTestCase extends CakeTestCase {
	var $Cat = null;
	var $fixtures = array('app.cat');

	function startTest() {
		$this->Cat =& ClassRegistry::init('Cat');
	}

	function testCatInstance() {
		$this->assertTrue(is_a($this->Cat, 'Cat'));
	}

	function testCatFind() {
		$this->Cat->recursive = -1;
		$results = $this->Cat->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Cat' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>