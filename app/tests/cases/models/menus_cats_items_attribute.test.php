<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItemsAttribute Test cases generated on: 2009-02-01 12:02:56 : 1233520016*/
App::import('Model', 'MenusCatsItemsAttribute');

class MenusCatsItemsAttributeTestCase extends CakeTestCase {
	var $MenusCatsItemsAttribute = null;
	var $fixtures = array('app.menus_cats_items_attribute', 'app.menus_cats_item', 'app.attribute');

	function startTest() {
		$this->MenusCatsItemsAttribute =& ClassRegistry::init('MenusCatsItemsAttribute');
	}

	function testMenusCatsItemsAttributeInstance() {
		$this->assertTrue(is_a($this->MenusCatsItemsAttribute, 'MenusCatsItemsAttribute'));
	}

	function testMenusCatsItemsAttributeFind() {
		$this->MenusCatsItemsAttribute->recursive = -1;
		$results = $this->MenusCatsItemsAttribute->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('MenusCatsItemsAttribute' => array(
			'id'  => 1,
			'menus_cats_item_id'  => 1,
			'attribute_id'  => 1,
			'price'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>