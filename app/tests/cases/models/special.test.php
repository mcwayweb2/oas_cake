<?php 
/* SVN FILE: $Id$ */
/* Special Test cases generated on: 2009-05-13 18:05:22 : 1242265342*/
App::import('Model', 'Special');

class SpecialTestCase extends CakeTestCase {
	var $Special = null;
	var $fixtures = array('app.special', 'app.location');

	function startTest() {
		$this->Special =& ClassRegistry::init('Special');
	}

	function testSpecialInstance() {
		$this->assertTrue(is_a($this->Special, 'Special'));
	}

	function testSpecialFind() {
		$this->Special->recursive = -1;
		$results = $this->Special->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Special' => array(
			'id'  => 1,
			'location_id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'subtext'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>