<?php 
/* SVN FILE: $Id$ */
/* User Test cases generated on: 2009-04-25 09:04:04 : 1240678624*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $User = null;
	var $fixtures = array('app.user', 'app.state', 'app.favorite', 'app.order');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function testUserInstance() {
		$this->assertTrue(is_a($this->User, 'User'));
	}

	function testUserFind() {
		$this->User->recursive = -1;
		$results = $this->User->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('User' => array(
			'id'  => 1,
			'fname'  => 'Lorem ipsum dolor sit amet',
			'lname'  => 'Lorem ipsum dolor sit amet',
			'username'  => 'Lorem ipsum dolor sit amet',
			'password'  => 'Lorem ipsum dolor sit amet',
			'address'  => 'Lorem ipsum dolor sit amet',
			'city'  => 'Lorem ipsum dolor sit amet',
			'state_id'  => 1,
			'zip'  => 'Lorem ip'
			));
		$this->assertEqual($results, $expected);
	}
}
?>