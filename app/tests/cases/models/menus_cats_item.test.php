<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItem Test cases generated on: 2009-02-01 12:02:36 : 1233519936*/
App::import('Model', 'MenusCatsItem');

class MenusCatsItemTestCase extends CakeTestCase {
	var $MenusCatsItem = null;
	var $fixtures = array('app.menus_cats_item', 'app.menus_cat');

	function startTest() {
		$this->MenusCatsItem =& ClassRegistry::init('MenusCatsItem');
	}

	function testMenusCatsItemInstance() {
		$this->assertTrue(is_a($this->MenusCatsItem, 'MenusCatsItem'));
	}

	function testMenusCatsItemFind() {
		$this->MenusCatsItem->recursive = -1;
		$results = $this->MenusCatsItem->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('MenusCatsItem' => array(
			'id'  => 1,
			'menus_cat_id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'description'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'price'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'image'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>