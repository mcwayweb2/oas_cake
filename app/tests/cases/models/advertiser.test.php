<?php 
/* SVN FILE: $Id$ */
/* Advertiser Test cases generated on: 2009-02-01 11:02:17 : 1233517817*/
App::import('Model', 'Advertiser');

class AdvertiserTestCase extends CakeTestCase {
	var $Advertiser = null;
	var $fixtures = array('app.advertiser', 'app.location');

	function startTest() {
		$this->Advertiser =& ClassRegistry::init('Advertiser');
	}

	function testAdvertiserInstance() {
		$this->assertTrue(is_a($this->Advertiser, 'Advertiser'));
	}

	function testAdvertiserFind() {
		$this->Advertiser->recursive = -1;
		$results = $this->Advertiser->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Advertiser' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'contact_fname'  => 'Lorem ipsum dolor sit amet',
			'contact_lname'  => 'Lorem ipsum dolor sit amet',
			'contact_title'  => 'Lorem ipsum dolor sit amet',
			'contact_phone'  => 'Lorem ipsum ',
			'logo'  => 'Lorem ipsum dolor ',
			'date_registered'  => '2009-02-01 11:50:17',
			'active'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>