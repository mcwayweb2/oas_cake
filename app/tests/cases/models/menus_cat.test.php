<?php 
/* SVN FILE: $Id$ */
/* MenusCat Test cases generated on: 2009-02-01 12:02:23 : 1233519083*/
App::import('Model', 'MenusCat');

class MenusCatTestCase extends CakeTestCase {
	var $MenusCat = null;
	var $fixtures = array('app.menus_cat', 'app.menu', 'app.cat');

	function startTest() {
		$this->MenusCat =& ClassRegistry::init('MenusCat');
	}

	function testMenusCatInstance() {
		$this->assertTrue(is_a($this->MenusCat, 'MenusCat'));
	}

	function testMenusCatFind() {
		$this->MenusCat->recursive = -1;
		$results = $this->MenusCat->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('MenusCat' => array(
			'id'  => 1,
			'menu_id'  => 1,
			'cat_id'  => 1,
			'list_order'  => 1,
			'subtext'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>