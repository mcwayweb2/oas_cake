<?php 
/* SVN FILE: $Id$ */
/* Order Test cases generated on: 2009-04-25 09:04:58 : 1240678318*/
App::import('Model', 'Order');

class OrderTestCase extends CakeTestCase {
	var $Order = null;
	var $fixtures = array('app.order', 'app.location', 'app.user');

	function startTest() {
		$this->Order =& ClassRegistry::init('Order');
	}

	function testOrderInstance() {
		$this->assertTrue(is_a($this->Order, 'Order'));
	}

	function testOrderFind() {
		$this->Order->recursive = -1;
		$results = $this->Order->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Order' => array(
			'id'  => 1,
			'location_id'  => 1,
			'user_id'  => 1,
			'timestamp'  => '2009-04-25 09:51:58',
			'filename'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>