<?php 
/* SVN FILE: $Id$ */
/* Size Test cases generated on: 2009-05-13 18:05:44 : 1242264584*/
App::import('Model', 'Size');

class SizeTestCase extends CakeTestCase {
	var $Size = null;
	var $fixtures = array('app.size');

	function startTest() {
		$this->Size =& ClassRegistry::init('Size');
	}

	function testSizeInstance() {
		$this->assertTrue(is_a($this->Size, 'Size'));
	}

	function testSizeFind() {
		$this->Size->recursive = -1;
		$results = $this->Size->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Size' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'size'  => 1,
			'base_price'  => 1,
			'extra_toppings'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>