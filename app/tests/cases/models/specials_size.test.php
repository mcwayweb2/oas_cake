<?php 
/* SVN FILE: $Id$ */
/* SpecialsSize Test cases generated on: 2009-05-13 18:05:00 : 1242265380*/
App::import('Model', 'SpecialsSize');

class SpecialsSizeTestCase extends CakeTestCase {
	var $SpecialsSize = null;
	var $fixtures = array('app.specials_size', 'app.special', 'app.sizes');

	function startTest() {
		$this->SpecialsSize =& ClassRegistry::init('SpecialsSize');
	}

	function testSpecialsSizeInstance() {
		$this->assertTrue(is_a($this->SpecialsSize, 'SpecialsSize'));
	}

	function testSpecialsSizeFind() {
		$this->SpecialsSize->recursive = -1;
		$results = $this->SpecialsSize->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('SpecialsSize' => array(
			'id'  => 1,
			'special_id'  => 1,
			'sizes_id'  => 1,
			'price'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>