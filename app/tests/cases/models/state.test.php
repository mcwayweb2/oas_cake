<?php 
/* SVN FILE: $Id$ */
/* State Test cases generated on: 2009-02-01 11:02:00 : 1233517680*/
App::import('Model', 'State');

class StateTestCase extends CakeTestCase {
	var $State = null;
	var $fixtures = array('app.state', 'app.location');

	function startTest() {
		$this->State =& ClassRegistry::init('State');
	}

	function testStateInstance() {
		$this->assertTrue(is_a($this->State, 'State'));
	}

	function testStateFind() {
		$this->State->recursive = -1;
		$results = $this->State->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('State' => array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'abbr'  => ''
			));
		$this->assertEqual($results, $expected);
	}
}
?>