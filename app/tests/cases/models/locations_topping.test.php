<?php 
/* SVN FILE: $Id$ */
/* LocationsTopping Test cases generated on: 2009-05-13 18:05:13 : 1242265213*/
App::import('Model', 'LocationsTopping');

class LocationsToppingTestCase extends CakeTestCase {
	var $LocationsTopping = null;
	var $fixtures = array('app.locations_topping', 'app.location', 'app.topping');

	function startTest() {
		$this->LocationsTopping =& ClassRegistry::init('LocationsTopping');
	}

	function testLocationsToppingInstance() {
		$this->assertTrue(is_a($this->LocationsTopping, 'LocationsTopping'));
	}

	function testLocationsToppingFind() {
		$this->LocationsTopping->recursive = -1;
		$results = $this->LocationsTopping->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('LocationsTopping' => array(
			'id'  => 1,
			'location_id'  => 1,
			'topping_id'  => 1,
			'price_alter'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>