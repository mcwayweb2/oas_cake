<?php 
/* SVN FILE: $Id$ */
/* Topping Test cases generated on: 2009-05-13 18:05:04 : 1242265144*/
App::import('Model', 'Topping');

class ToppingTestCase extends CakeTestCase {
	var $Topping = null;
	var $fixtures = array('app.topping');

	function startTest() {
		$this->Topping =& ClassRegistry::init('Topping');
	}

	function testToppingInstance() {
		$this->assertTrue(is_a($this->Topping, 'Topping'));
	}

	function testToppingFind() {
		$this->Topping->recursive = -1;
		$results = $this->Topping->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Topping' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>