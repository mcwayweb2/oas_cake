<?php 
/* SVN FILE: $Id$ */
/* Favorite Test cases generated on: 2009-04-25 10:04:14 : 1240678814*/
App::import('Model', 'Favorite');

class FavoriteTestCase extends CakeTestCase {
	var $Favorite = null;
	var $fixtures = array('app.favorite', 'app.location', 'app.user');

	function startTest() {
		$this->Favorite =& ClassRegistry::init('Favorite');
	}

	function testFavoriteInstance() {
		$this->assertTrue(is_a($this->Favorite, 'Favorite'));
	}

	function testFavoriteFind() {
		$this->Favorite->recursive = -1;
		$results = $this->Favorite->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Favorite' => array(
			'id'  => 1,
			'location_id'  => 1,
			'user_id'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>