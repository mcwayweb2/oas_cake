<?php 
/* SVN FILE: $Id$ */
/* Menu Test cases generated on: 2009-02-01 12:02:53 : 1233518633*/
App::import('Model', 'Menu');

class MenuTestCase extends CakeTestCase {
	var $Menu = null;
	var $fixtures = array('app.menu', 'app.location');

	function startTest() {
		$this->Menu =& ClassRegistry::init('Menu');
	}

	function testMenuInstance() {
		$this->assertTrue(is_a($this->Menu, 'Menu'));
	}

	function testMenuFind() {
		$this->Menu->recursive = -1;
		$results = $this->Menu->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Menu' => array(
			'id'  => 1,
			'location_id'  => 1,
			'active'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>