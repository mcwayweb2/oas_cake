<?php 
/* SVN FILE: $Id$ */
/* Attribute Test cases generated on: 2009-02-01 12:02:57 : 1233519837*/
App::import('Model', 'Attribute');

class AttributeTestCase extends CakeTestCase {
	var $Attribute = null;
	var $fixtures = array('app.attribute');

	function startTest() {
		$this->Attribute =& ClassRegistry::init('Attribute');
	}

	function testAttributeInstance() {
		$this->assertTrue(is_a($this->Attribute, 'Attribute'));
	}

	function testAttributeFind() {
		$this->Attribute->recursive = -1;
		$results = $this->Attribute->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Attribute' => array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
		$this->assertEqual($results, $expected);
	}
}
?>