<?php 
/* SVN FILE: $Id$ */
/* Location Test cases generated on: 2009-02-01 11:02:33 : 1233518013*/
App::import('Model', 'Location');

class LocationTestCase extends CakeTestCase {
	var $Location = null;
	var $fixtures = array('app.location', 'app.advertiser', 'app.state', 'app.menu');

	function startTest() {
		$this->Location =& ClassRegistry::init('Location');
	}

	function testLocationInstance() {
		$this->assertTrue(is_a($this->Location, 'Location'));
	}

	function testLocationFind() {
		$this->Location->recursive = -1;
		$results = $this->Location->find('first');
		$this->assertTrue(!empty($results));

		$expected = array('Location' => array(
			'id'  => 1,
			'advertiser_id'  => 1,
			'address1'  => 'Lorem ipsum dolor sit amet',
			'address2'  => 'Lorem ipsum dolor sit amet',
			'city'  => 'Lorem ipsum dolor sit amet',
			'state_id'  => 1,
			'zip'  => 'Lorem ip',
			'phone'  => 'Lorem ipsum ',
			'fax'  => 'Lorem ipsum ',
			'sms'  => 'Lorem ipsum ',
			'contact_fname'  => 'Lorem ipsum dolor sit amet',
			'contact_lname'  => 'Lorem ipsum dolor sit amet',
			'contact_title'  => 'Lorem ipsum dolor sit amet',
			'contact_phone'  => 'Lorem ipsum ',
			'time_open'  => '11:53:33',
			'time_closed'  => '11:53:33',
			'active'  => 1
			));
		$this->assertEqual($results, $expected);
	}
}
?>