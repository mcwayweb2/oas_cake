<?php 
/* SVN FILE: $Id$ */
/* ToppingsController Test cases generated on: 2009-05-13 19:05:39 : 1242267159*/
App::import('Controller', 'Toppings');

class TestToppings extends ToppingsController {
	var $autoRender = false;
}

class ToppingsControllerTest extends CakeTestCase {
	var $Toppings = null;

	function setUp() {
		$this->Toppings = new TestToppings();
		$this->Toppings->constructClasses();
	}

	function testToppingsControllerInstance() {
		$this->assertTrue(is_a($this->Toppings, 'ToppingsController'));
	}

	function tearDown() {
		unset($this->Toppings);
	}
}
?>