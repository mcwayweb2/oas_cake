<?php 
/* SVN FILE: $Id$ */
/* LocationsController Test cases generated on: 2009-02-01 12:02:36 : 1233520356*/
App::import('Controller', 'Locations');

class TestLocations extends LocationsController {
	var $autoRender = false;
}

class LocationsControllerTest extends CakeTestCase {
	var $Locations = null;

	function setUp() {
		$this->Locations = new TestLocations();
		$this->Locations->constructClasses();
	}

	function testLocationsControllerInstance() {
		$this->assertTrue(is_a($this->Locations, 'LocationsController'));
	}

	function tearDown() {
		unset($this->Locations);
	}
}
?>