<?php 
/* SVN FILE: $Id$ */
/* MenusCatsController Test cases generated on: 2009-02-01 12:02:19 : 1233520399*/
App::import('Controller', 'MenusCats');

class TestMenusCats extends MenusCatsController {
	var $autoRender = false;
}

class MenusCatsControllerTest extends CakeTestCase {
	var $MenusCats = null;

	function setUp() {
		$this->MenusCats = new TestMenusCats();
		$this->MenusCats->constructClasses();
	}

	function testMenusCatsControllerInstance() {
		$this->assertTrue(is_a($this->MenusCats, 'MenusCatsController'));
	}

	function tearDown() {
		unset($this->MenusCats);
	}
}
?>