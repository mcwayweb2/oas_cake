<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItemsController Test cases generated on: 2009-02-01 12:02:37 : 1233520537*/
App::import('Controller', 'MenusCatsItems');

class TestMenusCatsItems extends MenusCatsItemsController {
	var $autoRender = false;
}

class MenusCatsItemsControllerTest extends CakeTestCase {
	var $MenusCatsItems = null;

	function setUp() {
		$this->MenusCatsItems = new TestMenusCatsItems();
		$this->MenusCatsItems->constructClasses();
	}

	function testMenusCatsItemsControllerInstance() {
		$this->assertTrue(is_a($this->MenusCatsItems, 'MenusCatsItemsController'));
	}

	function tearDown() {
		unset($this->MenusCatsItems);
	}
}
?>