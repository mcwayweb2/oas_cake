<?php 
/* SVN FILE: $Id$ */
/* SizesController Test cases generated on: 2009-05-13 19:05:38 : 1242267098*/
App::import('Controller', 'Sizes');

class TestSizes extends SizesController {
	var $autoRender = false;
}

class SizesControllerTest extends CakeTestCase {
	var $Sizes = null;

	function setUp() {
		$this->Sizes = new TestSizes();
		$this->Sizes->constructClasses();
	}

	function testSizesControllerInstance() {
		$this->assertTrue(is_a($this->Sizes, 'SizesController'));
	}

	function tearDown() {
		unset($this->Sizes);
	}
}
?>