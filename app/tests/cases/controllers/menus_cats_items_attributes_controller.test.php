<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItemsAttributesController Test cases generated on: 2009-02-01 12:02:05 : 1233520565*/
App::import('Controller', 'MenusCatsItemsAttributes');

class TestMenusCatsItemsAttributes extends MenusCatsItemsAttributesController {
	var $autoRender = false;
}

class MenusCatsItemsAttributesControllerTest extends CakeTestCase {
	var $MenusCatsItemsAttributes = null;

	function setUp() {
		$this->MenusCatsItemsAttributes = new TestMenusCatsItemsAttributes();
		$this->MenusCatsItemsAttributes->constructClasses();
	}

	function testMenusCatsItemsAttributesControllerInstance() {
		$this->assertTrue(is_a($this->MenusCatsItemsAttributes, 'MenusCatsItemsAttributesController'));
	}

	function tearDown() {
		unset($this->MenusCatsItemsAttributes);
	}
}
?>