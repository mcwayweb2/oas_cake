<?php 
/* SVN FILE: $Id$ */
/* LocationsToppingsController Test cases generated on: 2009-05-13 19:05:22 : 1242267202*/
App::import('Controller', 'LocationsToppings');

class TestLocationsToppings extends LocationsToppingsController {
	var $autoRender = false;
}

class LocationsToppingsControllerTest extends CakeTestCase {
	var $LocationsToppings = null;

	function setUp() {
		$this->LocationsToppings = new TestLocationsToppings();
		$this->LocationsToppings->constructClasses();
	}

	function testLocationsToppingsControllerInstance() {
		$this->assertTrue(is_a($this->LocationsToppings, 'LocationsToppingsController'));
	}

	function tearDown() {
		unset($this->LocationsToppings);
	}
}
?>