<?php 
/* SVN FILE: $Id$ */
/* SpecialsController Test cases generated on: 2009-05-13 19:05:45 : 1242267285*/
App::import('Controller', 'Specials');

class TestSpecials extends SpecialsController {
	var $autoRender = false;
}

class SpecialsControllerTest extends CakeTestCase {
	var $Specials = null;

	function setUp() {
		$this->Specials = new TestSpecials();
		$this->Specials->constructClasses();
	}

	function testSpecialsControllerInstance() {
		$this->assertTrue(is_a($this->Specials, 'SpecialsController'));
	}

	function tearDown() {
		unset($this->Specials);
	}
}
?>