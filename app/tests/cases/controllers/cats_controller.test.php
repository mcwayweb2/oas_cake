<?php 
/* SVN FILE: $Id$ */
/* CatsController Test cases generated on: 2009-02-01 12:02:06 : 1233520326*/
App::import('Controller', 'Cats');

class TestCats extends CatsController {
	var $autoRender = false;
}

class CatsControllerTest extends CakeTestCase {
	var $Cats = null;

	function setUp() {
		$this->Cats = new TestCats();
		$this->Cats->constructClasses();
	}

	function testCatsControllerInstance() {
		$this->assertTrue(is_a($this->Cats, 'CatsController'));
	}

	function tearDown() {
		unset($this->Cats);
	}
}
?>