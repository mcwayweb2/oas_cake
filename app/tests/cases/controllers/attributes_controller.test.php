<?php 
/* SVN FILE: $Id$ */
/* AttributesController Test cases generated on: 2009-02-01 12:02:47 : 1233520187*/
App::import('Controller', 'Attributes');

class TestAttributes extends AttributesController {
	var $autoRender = false;
}

class AttributesControllerTest extends CakeTestCase {
	var $Attributes = null;

	function setUp() {
		$this->Attributes = new TestAttributes();
		$this->Attributes->constructClasses();
	}

	function testAttributesControllerInstance() {
		$this->assertTrue(is_a($this->Attributes, 'AttributesController'));
	}

	function tearDown() {
		unset($this->Attributes);
	}
}
?>