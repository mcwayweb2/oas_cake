<?php 
/* SVN FILE: $Id$ */
/* OrdersController Test cases generated on: 2009-04-25 10:04:39 : 1240679259*/
App::import('Controller', 'Orders');

class TestOrders extends OrdersController {
	var $autoRender = false;
}

class OrdersControllerTest extends CakeTestCase {
	var $Orders = null;

	function setUp() {
		$this->Orders = new TestOrders();
		$this->Orders->constructClasses();
	}

	function testOrdersControllerInstance() {
		$this->assertTrue(is_a($this->Orders, 'OrdersController'));
	}

	function tearDown() {
		unset($this->Orders);
	}
}
?>