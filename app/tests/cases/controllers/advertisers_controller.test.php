<?php 
/* SVN FILE: $Id$ */
/* AdvertisersController Test cases generated on: 2009-02-01 12:02:42 : 1233520122*/
App::import('Controller', 'Advertisers');

class TestAdvertisers extends AdvertisersController {
	var $autoRender = false;
}

class AdvertisersControllerTest extends CakeTestCase {
	var $Advertisers = null;

	function setUp() {
		$this->Advertisers = new TestAdvertisers();
		$this->Advertisers->constructClasses();
	}

	function testAdvertisersControllerInstance() {
		$this->assertTrue(is_a($this->Advertisers, 'AdvertisersController'));
	}

	function tearDown() {
		unset($this->Advertisers);
	}
}
?>