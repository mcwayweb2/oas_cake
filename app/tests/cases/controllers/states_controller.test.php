<?php 
/* SVN FILE: $Id$ */
/* StatesController Test cases generated on: 2009-02-01 12:02:32 : 1233520592*/
App::import('Controller', 'States');

class TestStates extends StatesController {
	var $autoRender = false;
}

class StatesControllerTest extends CakeTestCase {
	var $States = null;

	function setUp() {
		$this->States = new TestStates();
		$this->States->constructClasses();
	}

	function testStatesControllerInstance() {
		$this->assertTrue(is_a($this->States, 'StatesController'));
	}

	function tearDown() {
		unset($this->States);
	}
}
?>