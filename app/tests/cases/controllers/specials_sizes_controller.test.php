<?php 
/* SVN FILE: $Id$ */
/* SpecialsSizesController Test cases generated on: 2009-05-13 19:05:16 : 1242267376*/
App::import('Controller', 'SpecialsSizes');

class TestSpecialsSizes extends SpecialsSizesController {
	var $autoRender = false;
}

class SpecialsSizesControllerTest extends CakeTestCase {
	var $SpecialsSizes = null;

	function setUp() {
		$this->SpecialsSizes = new TestSpecialsSizes();
		$this->SpecialsSizes->constructClasses();
	}

	function testSpecialsSizesControllerInstance() {
		$this->assertTrue(is_a($this->SpecialsSizes, 'SpecialsSizesController'));
	}

	function tearDown() {
		unset($this->SpecialsSizes);
	}
}
?>