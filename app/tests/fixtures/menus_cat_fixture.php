<?php 
/* SVN FILE: $Id$ */
/* MenusCat Fixture generated on: 2009-02-01 12:02:23 : 1233519083*/

class MenusCatFixture extends CakeTestFixture {
	var $name = 'MenusCat';
	var $table = 'menus_cats';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'menu_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'cat_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'list_order' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 3),
			'subtext' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'menu_id'  => 1,
			'cat_id'  => 1,
			'list_order'  => 1,
			'subtext'  => 'Lorem ipsum dolor sit amet'
			));
}
?>