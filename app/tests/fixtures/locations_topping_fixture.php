<?php 
/* SVN FILE: $Id$ */
/* LocationsTopping Fixture generated on: 2009-05-13 18:05:13 : 1242265213*/

class LocationsToppingFixture extends CakeTestFixture {
	var $name = 'LocationsTopping';
	var $table = 'locations_toppings';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'location_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'topping_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'price_alter' => array('type'=>'float', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'location_id'  => 1,
			'topping_id'  => 1,
			'price_alter'  => 1
			));
}
?>