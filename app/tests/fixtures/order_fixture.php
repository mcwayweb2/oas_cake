<?php 
/* SVN FILE: $Id$ */
/* Order Fixture generated on: 2009-04-25 09:04:58 : 1240678318*/

class OrderFixture extends CakeTestFixture {
	var $name = 'Order';
	var $table = 'orders';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'location_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'user_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'timestamp' => array('type'=>'datetime', 'null' => false, 'default' => NULL),
			'filename' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'location_id'  => 1,
			'user_id'  => 1,
			'timestamp'  => '2009-04-25 09:51:58',
			'filename'  => 'Lorem ipsum dolor sit amet'
			));
}
?>