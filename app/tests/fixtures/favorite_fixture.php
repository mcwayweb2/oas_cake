<?php 
/* SVN FILE: $Id$ */
/* Favorite Fixture generated on: 2009-04-25 10:04:14 : 1240678814*/

class FavoriteFixture extends CakeTestFixture {
	var $name = 'Favorite';
	var $table = 'favorites';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'location_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'user_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'location_id'  => 1,
			'user_id'  => 1
			));
}
?>