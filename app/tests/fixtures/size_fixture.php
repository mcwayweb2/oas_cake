<?php 
/* SVN FILE: $Id$ */
/* Size Fixture generated on: 2009-05-13 18:05:44 : 1242264584*/

class SizeFixture extends CakeTestFixture {
	var $name = 'Size';
	var $table = 'sizes';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'size' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 2),
			'base_price' => array('type'=>'float', 'null' => false, 'default' => NULL),
			'extra_toppings' => array('type'=>'float', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'size'  => 1,
			'base_price'  => 1,
			'extra_toppings'  => 1
			));
}
?>