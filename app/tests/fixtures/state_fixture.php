<?php 
/* SVN FILE: $Id$ */
/* State Fixture generated on: 2009-02-01 11:02:00 : 1233517680*/

class StateFixture extends CakeTestFixture {
	var $name = 'State';
	var $table = 'states';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 3, 'key' => 'primary'),
			'name' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'abbr' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 2),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'abbr'  => ''
			));
}
?>