<?php 
/* SVN FILE: $Id$ */
/* Attribute Fixture generated on: 2009-02-01 12:02:57 : 1233519837*/

class AttributeFixture extends CakeTestFixture {
	var $name = 'Attribute';
	var $table = 'attributes';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
}
?>