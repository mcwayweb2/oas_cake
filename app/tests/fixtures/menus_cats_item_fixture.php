<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItem Fixture generated on: 2009-02-01 12:02:36 : 1233519936*/

class MenusCatsItemFixture extends CakeTestFixture {
	var $name = 'MenusCatsItem';
	var $table = 'menus_cats_items';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'menus_cat_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'description' => array('type'=>'text', 'null' => false, 'default' => NULL),
			'price' => array('type'=>'float', 'null' => false, 'default' => NULL),
			'image' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'menus_cat_id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'description'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'price'  => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida,phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam,vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit,feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'image'  => 'Lorem ipsum dolor sit amet'
			));
}
?>