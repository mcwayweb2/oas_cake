<?php 
/* SVN FILE: $Id$ */
/* Topping Fixture generated on: 2009-05-13 18:05:04 : 1242265144*/

class ToppingFixture extends CakeTestFixture {
	var $name = 'Topping';
	var $table = 'toppings';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
}
?>