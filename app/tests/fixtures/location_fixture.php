<?php 
/* SVN FILE: $Id$ */
/* Location Fixture generated on: 2009-02-01 11:02:33 : 1233518013*/

class LocationFixture extends CakeTestFixture {
	var $name = 'Location';
	var $table = 'locations';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'advertiser_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'address1' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'address2' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'city' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'state_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 3),
			'zip' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 10),
			'phone' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 14),
			'fax' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 14),
			'sms' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 14),
			'contact_fname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_lname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_title' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_phone' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 14),
			'time_open' => array('type'=>'time', 'null' => false, 'default' => NULL),
			'time_closed' => array('type'=>'time', 'null' => false, 'default' => NULL),
			'active' => array('type'=>'boolean', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'advertiser_id'  => 1,
			'address1'  => 'Lorem ipsum dolor sit amet',
			'address2'  => 'Lorem ipsum dolor sit amet',
			'city'  => 'Lorem ipsum dolor sit amet',
			'state_id'  => 1,
			'zip'  => 'Lorem ip',
			'phone'  => 'Lorem ipsum ',
			'fax'  => 'Lorem ipsum ',
			'sms'  => 'Lorem ipsum ',
			'contact_fname'  => 'Lorem ipsum dolor sit amet',
			'contact_lname'  => 'Lorem ipsum dolor sit amet',
			'contact_title'  => 'Lorem ipsum dolor sit amet',
			'contact_phone'  => 'Lorem ipsum ',
			'time_open'  => '11:53:33',
			'time_closed'  => '11:53:33',
			'active'  => 1
			));
}
?>