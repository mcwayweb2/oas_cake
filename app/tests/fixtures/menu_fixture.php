<?php 
/* SVN FILE: $Id$ */
/* Menu Fixture generated on: 2009-02-01 12:02:53 : 1233518633*/

class MenuFixture extends CakeTestFixture {
	var $name = 'Menu';
	var $table = 'menus';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'location_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'active' => array('type'=>'boolean', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'location_id'  => 1,
			'active'  => 1
			));
}
?>