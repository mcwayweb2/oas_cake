<?php 
/* SVN FILE: $Id$ */
/* Advertiser Fixture generated on: 2009-02-01 11:02:17 : 1233517817*/

class AdvertiserFixture extends CakeTestFixture {
	var $name = 'Advertiser';
	var $table = 'advertisers';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'name' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'contact_fname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_lname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_title' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'contact_phone' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 14),
			'logo' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 20),
			'date_registered' => array('type'=>'datetime', 'null' => false, 'default' => NULL),
			'active' => array('type'=>'boolean', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'name'  => 'Lorem ipsum dolor sit amet',
			'contact_fname'  => 'Lorem ipsum dolor sit amet',
			'contact_lname'  => 'Lorem ipsum dolor sit amet',
			'contact_title'  => 'Lorem ipsum dolor sit amet',
			'contact_phone'  => 'Lorem ipsum ',
			'logo'  => 'Lorem ipsum dolor ',
			'date_registered'  => '2009-02-01 11:50:17',
			'active'  => 1
			));
}
?>