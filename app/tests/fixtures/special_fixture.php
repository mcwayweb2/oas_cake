<?php 
/* SVN FILE: $Id$ */
/* Special Fixture generated on: 2009-05-13 18:05:22 : 1242265342*/

class SpecialFixture extends CakeTestFixture {
	var $name = 'Special';
	var $table = 'specials';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'location_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'subtext' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'location_id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet',
			'subtext'  => 'Lorem ipsum dolor sit amet'
			));
}
?>