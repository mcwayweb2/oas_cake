<?php 
/* SVN FILE: $Id$ */
/* MenusCatsItemsAttribute Fixture generated on: 2009-02-01 12:02:56 : 1233520016*/

class MenusCatsItemsAttributeFixture extends CakeTestFixture {
	var $name = 'MenusCatsItemsAttribute';
	var $table = 'menus_cats_items_attributes';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'menus_cats_item_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'attribute_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'price' => array('type'=>'float', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'menus_cats_item_id'  => 1,
			'attribute_id'  => 1,
			'price'  => 1
			));
}
?>