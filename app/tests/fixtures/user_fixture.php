<?php 
/* SVN FILE: $Id$ */
/* User Fixture generated on: 2009-04-25 09:04:04 : 1240678624*/

class UserFixture extends CakeTestFixture {
	var $name = 'User';
	var $table = 'users';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'fname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'lname' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'username' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'password' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'address' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'city' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'state_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 4),
			'zip' => array('type'=>'string', 'null' => false, 'default' => NULL, 'length' => 10),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'fname'  => 'Lorem ipsum dolor sit amet',
			'lname'  => 'Lorem ipsum dolor sit amet',
			'username'  => 'Lorem ipsum dolor sit amet',
			'password'  => 'Lorem ipsum dolor sit amet',
			'address'  => 'Lorem ipsum dolor sit amet',
			'city'  => 'Lorem ipsum dolor sit amet',
			'state_id'  => 1,
			'zip'  => 'Lorem ip'
			));
}
?>