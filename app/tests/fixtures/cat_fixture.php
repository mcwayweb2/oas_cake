<?php 
/* SVN FILE: $Id$ */
/* Cat Fixture generated on: 2009-02-01 12:02:48 : 1233518448*/

class CatFixture extends CakeTestFixture {
	var $name = 'Cat';
	var $table = 'cats';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'title' => array('type'=>'string', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'title'  => 'Lorem ipsum dolor sit amet'
			));
}
?>