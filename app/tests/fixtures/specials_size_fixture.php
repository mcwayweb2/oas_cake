<?php 
/* SVN FILE: $Id$ */
/* SpecialsSize Fixture generated on: 2009-05-13 18:05:00 : 1242265380*/

class SpecialsSizeFixture extends CakeTestFixture {
	var $name = 'SpecialsSize';
	var $table = 'specials_sizes';
	var $fields = array(
			'id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
			'special_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'sizes_id' => array('type'=>'integer', 'null' => false, 'default' => NULL, 'length' => 10),
			'price' => array('type'=>'float', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
			);
	var $records = array(array(
			'id'  => 1,
			'special_id'  => 1,
			'sizes_id'  => 1,
			'price'  => 1
			));
}
?>