<?php
class LocationsDiscountCode extends AppModel {

	var $name = 'LocationsDiscountCode';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'DiscountCode' => array('className' => 'DiscountCode',
								'foreignKey' => 'discount_code_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);

}
?>