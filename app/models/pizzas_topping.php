<?php
class PizzasTopping extends AppModel {

	var $name = 'PizzasTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Pizza' => array('className' => 'Pizza',
								'foreignKey' => 'pizza_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	function pizzaToppings($pizza_id) {
		return $this->find('all', array('conditions' => array('PizzasTopping.pizza_id'=>$pizza_id),
										'recursive' => 0,
										'fields' => array('PizzasTopping.placement', 'PizzasTopping.price', 'Topping.title')));
	}
}
?>