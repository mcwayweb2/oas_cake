<?php
class UsersTransaction extends AppModel {

	var $name = 'UsersTransaction';
	
	var $belongsTo = array(
			'User' => array('className' => 'User',
							'foreignKey' => 'users_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
			),
			'Order' => array('className' => 'Order',
							'foreignKey' => 'order_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
			)
	);
}
?>