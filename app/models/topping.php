<?php
class Topping extends AppModel {

	var $name = 'Topping';
	var $validate = array(
		'title' => array('notempty')
	);
	
	var $hasAndBelongsToMany = array('Location' 		  => array('with'=>'LocationsTopping'),
									 'Pizza'			  => array('with'=>'PizzasTopping'),
									 'CombosPizza'		  => array('with'=>'CombosPizzasTopping'),
									 'OrdersSpecialsSize' => array('with'=>'OrdersSpecialsSizesTopping'),
									 'OrdersCombosPizza'  => array('with'=>'OrdersCombosPizzasTopping'),
									 'OrdersCombosSpecialsSize' => array('with'=>'OrdersCombosSpecialsSizesTopping'));
	
	//return a list array of all current toppings to choose from
    function getToppingList() {
    	return $this->find('list', array('fields'    => array('Topping.id', 'Topping.title'), 
    									 'order'     => array('Topping.title')));
    }
}
?>