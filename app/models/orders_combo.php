<?php
class OrdersCombo extends AppModel {

	var $name = 'OrdersCombo';
	
	/*
	var $validate = array(
		'title' => array('notempty'),
		'price' => array('rule' => 'money', 'message' => 'Please enter a valid price.')
	);
	*/
	
	var $belongsTo = array(
		'Order' => array('className' => 'Order',
							'foreignKey' => 'order_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'Combo' => array('className' => 'Combo',
							'foreignKey' => 'combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasMany = array(
			'OrdersCombosPizza' => array('className' => 'OrdersCombosPizza',
								'foreignKey' => 'orders_combo_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'OrdersCombosTextItem' => array('className' => 'OrdersCombosTextItem',
								'foreignKey' => 'orders_combo_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'OrdersCombosSpecialsSize' => array('className' => 'OrdersCombosSpecialsSize',
								'foreignKey' => 'orders_combo_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'OrdersCombosItem' => array('className' => 'OrdersCombosItem',
								'foreignKey' => 'orders_combo_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	var $hasAndBelongsToMany = array(
		'Item' => array('className' => 'Item',
					'joinTable' => 'orders_combos_items',
					'foreignKey' => 'orders_combo_id',
					'associationForeignKey' => 'item_id',
					'unique' => false,
					'conditions' => '',
					'fields' => array('Item.id'),
					'order' => '',
					'limit' => '',
					'offset' => '',
					'finderQuery' => '',
					'deleteQuery' => '',
					'insertQuery' => ''
		),
		'SpecialsSize' => array('className' => 'SpecialsSize',
					'joinTable' => 'orders_combos_specials_sizes',
					'foreignKey' => 'orders_combo_id',
					'associationForeignKey' => 'specials_size_id',
					'unique' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'finderQuery' => '',
					'deleteQuery' => '',
					'insertQuery' => ''
		)
	);
}
?>