<?php
class ContentPage extends AppModel {

	var $name = 'ContentPage';
	var $validate = array(
		'title' => array('notempty'),
		'content' => array('notempty')
	);
	var $actsAs = array(
		'Sluggable'=> array('label' => 'name', 'overwrite' => true)	
    );

}
?>