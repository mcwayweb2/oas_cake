<?php
class UsersPaymentProfile extends AppModel {

	var $name = 'UsersPaymentProfile';
	var $validate = array(
		'name' => array('notempty'),
		'lname' => array('notempty'),
		'fname' => array('notempty'),
		'address' => array('notempty'),
		'city' => array('notempty'),
		'zip' => array('numeric'),
		'cc_num' => array(
			'rule' => array('cc', 'all', false, null),
			'message' => 'The credit card number you supplied was invalid.')
	);

	var $belongsTo = array(
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'users_payment_profile_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	function haveProfileCheck($user_id) {
		$profile = $this->find('first', array('conditions' => array('UsersPaymentProfile.user_id'=>$user_id),
											  'recursive'  => -1));
		return ($profile) ? true : false;
	}
	
	// record an auth.net user transaction
	function saveTransaction($user_id, $pay_profile_id = null, $transaction_id, $amt, $order_id = null, $desc = null, $type = 'Once') {
		$this->User->UsersTransaction->create();
		
		return $this->User->UsersTransaction->save(array('UsersTransaction' => array('user_id'                  => $user_id,
																				 	 'users_payment_profile_id' => $pay_profile_id,
																				 	 'transaction_id'           => $transaction_id,
																					 'order_id'                 => $order_id,
																				 	 'type' 					=> $type,
																				 	 'amount' 					=> $amt,
																				 	 'description' 				=> $desc,
																				 	 'timestamp' 				=> date('Y-m-d H:i:s'))));
	}
}
?>