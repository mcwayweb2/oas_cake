<?php
class CombosSpecialsSizesTopping extends AppModel {

	var $name = 'CombosSpecialsSizesTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'CombosSpecialsSize' => array('className' => 'CombosSpecialsSize',
								'foreignKey' => 'combos_specials_size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	/*
	function specialsTopping($size_id) {
		$fields = array('OrdersSpecialsSizesTopping.id', 'OrdersSpecialsSizesTopping.price', 'OrdersSpecialsSizesTopping.placement',
						'Topping.id', 'Topping.title');
		return $this->find('all', array('conditions' => array('OrdersSpecialsSizesTopping.orders_specials_size_id'=>$size_id),
										'recursive'  => 1, 'fields'=>$fields));
	}
	*/
}
?>