<?php
class OrdersCombosItemsLocationsCatsOption extends AppModel {

	var $name = 'OrdersCombosItemsLocationsCatsOption';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersCombosItem' => array('className' => 'OrdersCombosItem',
								'foreignKey' => 'orders_combos_item_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'LocationsCatsOption' => array('className' => 'LocationsCatsOption',
								'foreignKey' => 'locations_cats_option_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
}
?>