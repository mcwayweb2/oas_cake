<?php
class LocationsTime extends AppModel {

	var $name = 'LocationsTime';
	
	var $validate = array(
		'day' => array('notempty')
	);

	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);

}
?>