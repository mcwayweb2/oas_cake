<?php
class Order extends AppModel {

	var $name = 'Order';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'UsersAddr' => array('className' => 'UsersAddr',
								'foreignKey' => 'users_addr_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'DiscountCode' => array('className' => 'DiscountCode',
								'foreignKey' => 'discount_code_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'UsersPaymentProfile' => array('className' => 'UsersPaymentProfile',
								'foreignKey' => 'users_payment_profile_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Pizza' => array('className' => 'Pizza',
								'foreignKey' => 'order_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'OrdersSpecialsSize' => array('className' => 'OrdersSpecialsSize',
								'foreignKey' => 'order_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'OrdersItem' => array('className' => 'OrdersItem',
								'foreignKey' => 'order_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => 'SELECT * FROM `orders_items` AS `OrdersItem` INNER JOIN `items` AS `Item` 
												  ON `OrdersItem`.`item_id` = `Item`.`id`  
												  WHERE `OrdersItem`.`order_id` IN ({$__cakeID__$})',
								'counterQuery' => ''
			),
			'OrdersCombo' => array('className' => 'OrdersCombo',
								'foreignKey' => 'order_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'UsersTransaction' => array('className' => 'UsersTransaction',
								'foreignKey' => 'order_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	var $hasAndBelongsToMany = array(
			'Item' => array('className' => 'Item',
						'joinTable' => 'orders_items',
						'foreignKey' => 'order_id',
						'associationForeignKey' => 'item_id',
						'unique' => false,
						'conditions' => '',
						'fields' => array('Item.id'),
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'Combo' => array('className' => 'Combo',
						'joinTable' => 'orders_combos',
						'foreignKey' => 'order_id',
						'associationForeignKey' => 'combo_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'SpecialsSize' => array('className' => 'SpecialsSize',
						'joinTable' => 'orders_specials_sizes',
						'foreignKey' => 'order_id',
						'associationForeignKey' => 'specials_size_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);

	//updates the subtotal of the specified order record
	function updateOrderTotals($order_id, $price) {
		$o = $this->read(array('Order.subtotal', 'DiscountCode.min_order_amt', 'Order.discount_amt', 'DiscountCode.discount_amt', 'DiscountCode.discount_type'), 
						 $order_id);
		$amt = $o['Order']['subtotal'] + $price;
		
		//make sure new order total is still eligible for the discount amt, if a discount code is being used.
		if($o['DiscountCode']['min_order_amt'] > $amt) {
			$this->saveField('discount_amt', '0');
			$this->saveField('discount_code_id', null);
		} else if($o['Order']['discount_amt'] > 0 && $o['DiscountCode']['discount_type'] == 'Percent') {
			//if a code is being used, and is a percentage type discount, recalculate discount_amt
			$discount = $amt * $o['DiscountCode']['discount_amt'];
			$this->saveField('discount_amt', $discount);
		}
		return $this->saveField('subtotal', $amt);
	}
	
	function orderInfo($order_id, $no_toppings = false) {
		$this->recursive = 1;
		$order = $this->read(null, $order_id);
		if(sizeof($order['OrdersSpecialsSize']) > 0) {
			foreach($order['OrdersSpecialsSize'] as $i => $size):
				$s = $this->SpecialsSize->find('first', array('conditions' => array('SpecialsSize.id'=>$size['specials_size_id']),
															  'recursive'  => 0));
				$order['OrdersSpecialsSize'][$i]['Special'] = $s['Special'];
				$order['OrdersSpecialsSize'][$i]['Size'] = $s['Size'];
				$order['OrdersSpecialsSize'][$i]['Crust'] = $this->Pizza->LocationsCrust->getCrustInfo($size['locations_crust_id']);
				
				if(!$no_toppings) {
					//$order['OrdersSpecialsSize'][$i]['Topping'] = $this->OrdersSpecialsSize->SpecialsSize->Topping->OrdersSpecialsSizesTopping->specialsToppings($size['id']);	
					/*$order['OrdersSpecialsSize'][$i]['Topping'] = $this->OrdersSpecialsSize->find('first', array('conditions' => array('OrdersSpecialsSize.id' => $size['id']),
																												 'recursive'  => 2)); */
					//$order['OrdersSpecialsSize'][$i]['Topping'] = $this->OrdersSpecialsSize->getToppings($size['id']);
				}
			endforeach;
		}
		if(sizeof($order['Pizza']) > 0) {
			foreach($order['Pizza'] as $i => $pizza):
				$size = $this->Pizza->Size->find('first', array('conditions' => array('Size.id'=>$pizza['size_id']),
															    'recursive'  => '0'));
				$order['Pizza'][$i]['Size'] = $size['Size'];
				$order['Pizza'][$i]['Topping'] = $this->Pizza->PizzasTopping->pizzaToppings($pizza['id']);
				$order['Pizza'][$i]['Crust'] = $this->Pizza->LocationsCrust->getCrustInfo($pizza['locations_crust_id']); 
			endforeach;
		}
		
		//add opt records and cat opt records for any order items
		if(sizeof($order['OrdersItem']) > 0) {
			foreach($order['OrdersItem'] as $index => $i):
				$order['OrdersItem'][$index] = $this->OrdersItem->find('first', array('conditions' => array('OrdersItem.id' => $i['id']),
															   						  'contain'    => array('ItemsOption', 'LocationsCatsOption', 'Item')));
			endforeach;
		}
		
		
		
		
		if(sizeof($order['OrdersCombo']) > 0) {
			foreach($order['OrdersCombo'] as $index => $c):
				$order['OrdersCombo'][$index]['OrdersCombo'] = $this->OrdersCombo->find('first', array('conditions' => array('OrdersCombo.id' => $c['id']),
																						  		 		'recursive'  => 2,
																						  		 		'contain'    => array('Combo', 'OrdersCombosPizza.LocationsCrust', 'OrdersCombosPizza.Topping', 'OrdersCombosItem.Item', 'OrdersCombosItem.ItemsOption', 'OrdersCombosItem.LocationsCatsOption', 'OrdersCombosSpecialsSize', 'OrdersCombosTextItem')));
				if(sizeof($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosTextItem']) > 0) {
					foreach($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosTextItem'] as $index2 => $i):
						$text_item = $this->OrdersCombo->OrdersCombosTextItem->CombosTextItem->find('first', array('conditions' => array('CombosTextItem.id' => $i['combos_text_item_id']),
																												   'recursive'  => -1));
						$order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosTextItem'][$index2]['CombosTextItem'] = $text_item['CombosTextItem'];
					endforeach;
				}
				
				if(sizeof($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosSpecialsSize']) > 0) {
					foreach($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosSpecialsSize'] as $index2 => $i):
						$temp_crust = $this->Pizza->LocationsCrust->getCrustInfo($i['locations_crust_id']);
						$order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosSpecialsSize'][$index2]['Crust'] = $temp_crust['Crust'];
						
						$order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosSpecialsSize'][$index2] = array_merge($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosSpecialsSize'][$index2], 
																														$this->SpecialsSize->read(array('Size.size', 'Size.title', 'Special.title', 'Special.subtext'), $i['specials_size_id']));
					endforeach;
				}
				
				if(sizeof($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosPizza']) > 0) {
					foreach($order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosPizza'] as $index2 => $p):
						$temp_crust = $this->Pizza->LocationsCrust->getCrustInfo($p['locations_crust_id']);
						$order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosPizza'][$index2]['Crust'] = $temp_crust['Crust'];
						
						$combos_pizza = $this->Combo->CombosPizza->read('size_id', $p['combos_pizza_id']);
						$this->Pizza->Size->recursive = -1;
						$order['OrdersCombo'][$index]['OrdersCombo']['OrdersCombosPizza'][$index2]['Size'] = $this->Pizza->Size->read(array('title', 'size'), $combos_pizza['CombosPizza']['size_id']); 
					endforeach;
				}
				
			endforeach;
		}
		
		//add location tax info
		$zip = $this->Location->ZipCode->find('first', array('conditions' => array('ZipCode.id' => $order['Location']['zip_code_id']),
														     'recursive'  => '-1',
															 'fields'	  => array('ZipCode.tax_rate')));
		$order['Location']['ZipCode'] = $zip['ZipCode'];
		
		return $order;
	}

	function killOrder($id) {
		if(!$id) { return false; }
		$this->delete($id);
		$this->OrdersItem->deleteAll(array('OrdersItem.order_id' => $id));
		$this->OrdersSpecialsSize->deleteAll(array('OrdersSpecialsSize.order_id' => $id));
	}

	function getConfirmHash($type = 'Email') {
		while(1 == 1) {
			$i = 0;
			$hash = "";
			
			if($type == 'Email') {
				//make random hash
				$charset = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
				
				while ($i++ < 50) { 
					$hash .= substr($charset, mt_rand(0, strlen($charset)-1), 1);
				}
				
				//make sure hash is not already being used in db
				$order = $this->find('first', array('conditions' => array('Order.confirm_hash' => $hash)));
			} else {
				// $type = "Phone"
				// confirm hash number for $type = "Phone" is 3 digit number
				while ($i++ < 3) { 
					$hash .= substr("0123456789", mt_rand(0, strlen("0123456789")-1), 1);
				} 	
				
				//make sure hash is not already being used in db
				$order = $this->find('first', array('conditions' => array('Order.confirm_hash' 	=> $hash,
																	      'Order.order_started'	=> '0')));
			}
			//no record with this hash found, return hash
			if(!$order) { return $hash; }	
		}
	}

	function ordersByDateRange($start_date = null, $end_date = null, $conditions = null) {
		$conditions = (isset($conditions)) ? $conditions : array();
		$conditions['Order.complete'] = '1';
		
		//is there start/end date search criteria?
		if($start_date && $end_date) {
			$conditions['Order.timestamp > '] = $start_date;
			$conditions['Order.timestamp < '] = $end_date;
		}
		
		return $this->find('all', array('conditions' => $conditions,
										'contain'   => array('User', 'UsersAddr', 'Location.Advertiser'),
										'order'		=> array('Order.timestamp' => 'DESC')));
	}
	
	// show all orders that have been paid for, but not confirmed.
	function getUnsettledOrders() {
		return $this->find('all', array('conditions' => array('Order.complete'      => '1',
															  'Order.order_started' => '0',
															  'Order.refunded'      => '0'),
										'recursive'  => 0,
										'contain'    => array('Location', 'User'),
										'order'      => array('Order.timestamp')));
	}
}
?>