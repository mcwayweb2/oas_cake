<?php
class ItemsOption extends AppModel {

	var $name = 'ItemsOption';
	
	var $validate = array(
		'item_id' => array('numeric'),
		'label' => array('notempty')
	);
	
	var $belongsTo = array(
		'Item' => array('className' => 'Item',
						'foreignKey' => 'item_id',
						'conditions' => '',
						'fields' => '',
						'order' => array('ItemsOption.price' => 'ASC')
		)
	);
	
	var $hasAndBelongsToMany = array(
			'OrdersItem' => array('className' => 'OrdersItem',
						'joinTable' => 'orders_items_items_options',
						'foreignKey' => 'items_option_id',
						'associationForeignKey' => 'orders_item_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'OrdersCombosItem' => array('className' => 'OrdersCombosItem',
						'joinTable' => 'orders_combos_items_items_options',
						'foreignKey' => 'items_option_id',
						'associationForeignKey' => 'orders_combos_item_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);
}
?>