<?php
class OrdersItem extends AppModel {

	var $name = 'OrdersItem';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'order_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Item' => array('className' => 'Item',
								'foreignKey' => 'item_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasAndBelongsToMany = array(
			'ItemsOption' => array('className' => 'ItemsOption',
						'joinTable' => 'orders_items_items_options',
						'foreignKey' => 'orders_item_id',
						'associationForeignKey' => 'items_option_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'LocationsCatsOption' => array('className' => 'LocationsCatsOption',
						'joinTable' => 'orders_items_locations_cats_options',
						'foreignKey' => 'orders_item_id',
						'associationForeignKey' => 'locations_cats_option_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);
}
?>