<?php
class AdvertisersPaymentProfilesTransaction extends AppModel {

	var $name = 'AdvertisersPaymentProfilesTransaction';
	
	var $belongsTo = array(
			'AdvertisersPaymentProfile' => array('className' => 'AdvertisersPaymentProfile',
								'foreignKey' => 'advertisers_payment_profile_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	function nextId() {
		$rec = $this->find('first', array('fields' => array('AdvertisersPaymentProfilesTransaction.id'),
										  'order'  => array('AdvertisersPaymentProfilesTransaction.id' => 'DESC')));
		return $rec['AdvertisersPaymentProfilesTransaction']['id'] + 1;
	}
}
?>