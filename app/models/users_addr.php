<?php
class UsersAddr extends AppModel {

	var $name = 'UsersAddr';
	var $validate = array(
		'name' => array('notempty'),
		'address' => array('notempty'),
		'city' => array('notempty'),
		'state_id' => array('numeric'),
		'zip' => array('notempty')
	);
	
	/* Map Key For Orderaslice.com
	 * 
	var $actsAs = array('Geocoded' => array(
        'key' => 'ABQIAAAAt8g3H2s5nd-sDiPWRvogYhQBHCZsqLbzyg2evaw2X_ao5gVF2hS2cK-7mlwrGGBOsZDrYXY6LjtqBQ'
    ));
    *
    * Key for mcwaywebdesign.com
    */
	var $actsAs = array('Geocoded' => array(
        'key' => 'ABQIAAAAt8g3H2s5nd-sDiPWRvogYhQ8tQ_4FvAyTVJ_LoiXN6g-HkKL-BQnDWANq6AnzHF55cDqu7-MXmEj6A'
    ));

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'State' => array('className' => 'State',
								'foreignKey' => 'state_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'User' => array('className' => 'User',
								'foreignKey' => 'user_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'users_addr_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	function getDefault($id) {
		return $this->find('first', array('conditions' => array('UsersAddr.user_id'=>$id,
															    'UsersAddr.default'=>1)));	
	}
	
	function userAddresses($user_id, $fields = null) {
		$params = array('conditions' => array('UsersAddr.user_id'=>$user_id),
						'recursive'  => 0,
						'order' => array('UsersAddr.default' => 'DESC', 'UsersAddr.name' => 'ASC'));
		if($fields) $params['fields'] = $fields;
		
		return $this->find('all', $params);
	}
}
?>