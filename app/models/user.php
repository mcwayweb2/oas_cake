<?php
class User extends AppModel {

	var $name = 'User';
	var $validate = array(
		'fname' => array('notempty'),
		'lname' => array('notempty'),
		'username' => array('notempty'),
		'pass1' => array('rule1' => array('rule' => array('between', 6, 20), 
		                                  'message' => 'Password length must be 6-20 characters.')),
		'pass2' => array('rule1' => array('rule' => array('between', 6, 20), 
		                                  'message' => 'Password length must be 6-20 characters.')),
		'phone' => array('rule1' => array('rule' => array('between', 10, 10), 'message' => 'Your phone number must be 10 numeric digits in length.'),
				         'rule2' => array('rule' => 'numeric',                'message' => 'Please enter only numeric digits.')),
		'email' => array('rule'    => 'email',
						 'message' => 'Please enter a valid email address.')
	);

	var $hasMany = array(
			'UsersPaymentProfile' => array('className' => 'UsersPaymentProfile',
								'foreignKey' => 'user_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'UsersTransaction' => array('className' => 'UsersTransaction',
								'foreignKey' => 'user_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Favorite' => array('className' => 'Favorite',
								'foreignKey' => 'user_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'UsersAddr' => array('className' => 'UsersAddr',
								'foreignKey' => 'user_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Order' => array('className' => 'Order',
								'foreignKey' => 'user_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>