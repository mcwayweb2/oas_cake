<?php
class LocationsTopping extends AppModel {

	var $name = 'LocationsTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'ToppingType' => array('className' => 'ToppingType',
								'foreignKey' => 'topping_type_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);

}
?>