<?php
class SpecialsSize extends AppModel {

	var $name = 'SpecialsSize';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Special' => array('className' => 'Special',
								'foreignKey' => 'special_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Size' => array('className' => 'Size',
								'foreignKey' => 'size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasAndBelongsToMany = array(
    	'Order' => array('className'             => 'Order',
                		'joinTable'              => 'orders_specials_sizes',
                		'foreignKey'             => 'specials_size_id',
		                'associationForeignKey'  => 'order_id',
		                'unique'                 => true,
		                'conditions'             => '',
		                'fields'                 => '',
		                'order'                  => '',
		                'limit'                  => '',
		                'offset'                 => '',
		                'finderQuery'            => '',
		                'deleteQuery'            => '',
		                'insertQuery'            => ''
		),
		'Combo' => array('className'              => 'Combo',
                		'joinTable'              => 'combos_specials_sizes',
                		'foreignKey'             => 'specials_size_id',
		                'associationForeignKey'  => 'combo_id',
		                'unique'                 => true,
		                'conditions'             => '',
		                'fields'                 => '',
		                'order'                  => '',
		                'limit'                  => '',
		                'offset'                 => '',
		                'finderQuery'            => '',
		                'deleteQuery'            => '',
		                'insertQuery'            => ''
		),
		'OrdersCombo' => array('className'       => 'OrdersCombo',
                		'joinTable'              => 'orders_combos_specials_sizes',
                		'foreignKey'             => 'specials_size_id',
		                'associationForeignKey'  => 'orders_combo_id',
		                'unique'                 => true,
		                'conditions'             => '',
		                'fields'                 => '',
		                'order'                  => '',
		                'limit'                  => '',
		                'offset'                 => '',
		                'finderQuery'            => '',
		                'deleteQuery'            => '',
		                'insertQuery'            => ''
		)
    );
	
    function getSizes($special_id) {
    	return $this->find('all', array('conditions' => array('SpecialsSize.special_id' => $special_id),
    									'recursive'  => -1));
    }
}
?>