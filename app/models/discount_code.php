<?php
class DiscountCode extends AppModel {

	var $name = 'DiscountCode';
	var $validate = array(
		'code' => array('rule1' => array('rule' => array('between', 6, 12), 'message' => 'Discount code must be 6-12 characters in length.'),
						'rule2' => array('rule' => 'alphaNumeric', 'message' => 'Please use only alphanumeric characters.')),
		'use_limit' => array('rule1' => array('rule' => 'numeric', 'allowEmpty' => true, 'message' => 'Please enter numeric digits only.')),
		'min_order_amt' => array('rule1' => array('rule' => 'money', 'allowEmpty' => true, 'message' => 'Please enter a valid monetary amount.')),
		'discount_amt' => array('numeric')
	);
	
	var $belongsTo = array(
			'Advertiser' => array('className' => 'Advertiser',
								'foreignKey' => 'advertiser_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'discount_code_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'LocationsDiscountCode' => array('className' => 'LocationsDiscountCode',
								'foreignKey' => 'discount_code_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	var $hasAndBelongsToMany = array('Location' => array('with'=>'LocationsDiscountCode'));
	
	//return all promotion codes for a single advertiser
	function codesByAdvertiser($advertiser_id) {
		return $this->find('all', array('conditions' => array('DiscountCode.advertiser_id' => $advertiser_id),
										'recursive'  => -1));
	}
	
	//return all location ids that are using the passed discount code
	function getLocationsUsingCode($id) {
		return $this->LocationsDiscountCode->find('list', array('conditions' => array('LocationsDiscountCode.discount_code_id' => $id),
																'fields'     => array('location_id')));
	}
	
	function timesUsed($code_id) {
		return $this->Order->find('count', array('conditions' => array('Order.discount_code_id' => $code_id)));
	}
}
?>