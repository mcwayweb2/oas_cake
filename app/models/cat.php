<?php
class Cat extends AppModel {

	var $name = 'Cat';
	var $validate = array(
		'title' => array('notempty')
	);
	
	var $hasAndBelongsToMany = array('Location' => array('with'=>'LocationsCat'));

	var $hasMany = array(
		'Item' => array('className' => 'Item',
							'foreignKey' => 'cat_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => array('Item.list_order', 'Item.id'),
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		)
	);
	
	function getCatList() {
		return $this->find('list', array('fields' => array('Cat.title'), 'order' => array('Cat.title')));
	}
}
?>