<?php
class Advertiser extends AppModel {

	var $name = 'Advertiser';
	
	var $actsAs = array('Sluggable' => array('separator' => '-',
											 'label'     => 'name',
											 'overwrite' => true));
											 
	var $validate = array(
		'name' => array('notempty'),
		'contact_fname' => array('notempty'),
		'contact_lname' => array('notempty'),
		'contact_title' => array('notempty'),
		'billing_fname' => array('notempty'),
		'billing_lname' => array('notempty'),
		'billing_address' => array('notempty'),
		'billing_city' => array('notempty'),
		'billing_zip' => array('notempty'),
		'cc_num' => array('notempty'),
		'username' => array('notempty'),
		'contact_phone' => array('rule1' => array('rule' => array('between', 10, 10), 'message' => 'Your phone number must be 10 numeric digits in length.'),
				         		'rule2' => array('rule' => 'numeric',                'message' => 'Please enter only numeric digits')),
		'email' => array('rule'       => 'email',
						 'message' => 'Please enter a valid email address.')
	);
	
	var $belongsTo = array(
			'Banner' => array('className' => 'Banner',
								'foreignKey' => 'banner_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
			

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'advertiser_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'DiscountCode' => array('className' => 'DiscountCode',
								'foreignKey' => 'advertiser_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'AdvertisersPaymentProfile' => array('className' => 'AdvertisersPaymentProfile',
								'foreignKey' => 'advertiser_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);

	//check if a recurring transaction is already in place for an advertiser. If so, return transaction
	function checkRecurringTrans($id) {
		//get list of all ads pay profiles
		$profiles = $this->AdvertisersPaymentProfile->find('list', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id' => $id),
																		 'fields'     => array('AdvertisersPaymentProfile.payment_profile_id')));
		
		//now check for recurring transaction
		return $this->AdvertisersPaymentProfile->AdvertisersPaymentProfilesTransaction->find('first', array('conditions' => array('AdvertisersPaymentProfilesTransaction.payment_profile_id' => $profiles),
																											'recursive'  => -1));
	}
}
?>