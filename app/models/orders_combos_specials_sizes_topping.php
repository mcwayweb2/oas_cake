<?php
class OrdersCombosSpecialsSizesTopping extends AppModel {

	var $name = 'OrdersCombosSpecialsSizesTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersCombosSpecialsSize' => array('className' => 'OrdersCombosSpecialsSize',
								'foreignKey' => 'orders_combos_specials_size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
}
?>