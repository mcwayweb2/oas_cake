<?php
class LocationsCatsOption extends AppModel {

	var $name = 'LocationsCatsOption';

	var $belongsTo = array(
			'LocationsCat' => array('className' => 'LocationsCat',
								'foreignKey' => 'locations_cat_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);

	
	var $hasAndBelongsToMany = array(
			'OrdersItem' => array('className' => 'OrdersItem',
						'joinTable' => 'orders_items_locations_cats_options',
						'foreignKey' => 'locations_cat_id',
						'associationForeignKey' => 'orders_item_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'OrdersCombosItem' => array('className' => 'OrdersCombosItem',
						'joinTable' => 'orders_combos_items_locations_cats_options',
						'foreignKey' => 'locations_cat_id',
						'associationForeignKey' => 'orders_combos_item_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);
}
?>