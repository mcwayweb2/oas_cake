<?php
class State extends AppModel {

	var $name = 'State';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'state_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Prospect' => array('className' => 'Prospect',
								'foreignKey' => 'state_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'UsersAddr' => array('className' => 'UsersAddr',
								'foreignKey' => 'state_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Contact' => array('className' => 'Contact',
								'foreignKey' => 'state_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	function activeStateList() {
		return $this->find('list', array('conditions' => array('State.active' => '1')));
	}
}
?>