<?php
class Banner extends AppModel {

	var $name = 'Banner';
	
	var $hasMany = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'banner_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Advertiser' => array('className' => 'Advertiser',
								'foreignKey' => 'banner_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
}
?>