<?php
class CombosPizza extends AppModel {

	var $name = 'CombosPizza';
	
	var $hasMany = array(
		'OrdersCombosPizza' => array('className' => 'OrdersCombosPizza',
							'foreignKey' => 'combos_pizza_id',
							'dependent' => true,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => true,
							'finderQuery' => '',
							'counterQuery' => ''
		)
	);
	
	var $belongsTo = array(
		'Combo' => array('className' => 'Combo',
							'foreignKey' => 'combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'Size' => array('className' => 'Size',
							'foreignKey' => 'size_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'CombosPizzasTopping'));
	
	function getToppings($id) {
		return $this->CombosPizzasTopping->find('all', array('conditions' => array('CombosPizzasTopping.combos_pizza_id' => $id),
											  			     'recursive'  => 0, 
														  	 'fields'	  => array('CombosPizzasTopping.id', 'CombosPizzasTopping.topping_id', 
														  	 					   'CombosPizzasTopping.placement', 'Topping.title')));
	}
}
?>