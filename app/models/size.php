<?php
class Size extends AppModel {

	var $name = 'Size';
	var $validate = array(
		'title' => array('notempty'),
		'size' => array('numeric'),
		'base_price' => array('numeric'),
		'topping_price' => array('numeric'),
		'extra_cheese' => array('rule' => 'numeric', 'message' => 'Please enter extra cheese price.', 'allowEmpty' => true),
		'extra_sauce' => array('rule' => 'numeric', 'message' => 'Please enter extra sauce price.', 'allowEmpty' => true)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasAndBelongsToMany = array(
			'Special' => array('className' => 'Special',
						'joinTable' => 'specials_sizes',
						'foreignKey' => 'size_id',
						'associationForeignKey' => 'special_id',
						'unique' => true,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);

	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Pizza' => array('className' => 'Pizza',
								'foreignKey' => 'size_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'CombosPizza' => array('className' => 'CombosPizza',
								'foreignKey' => 'size_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>