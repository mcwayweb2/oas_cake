<?php
class Location extends AppModel {

	var $name = 'Location';
	var $validate = array(
		'name' => array('notempty'),
		'advertiser_id' => array('numeric'),
		'address1' => array('notempty'),
		'city' => array('notempty'),
		'state_id' => array('numeric'),
		'zip_code_id' => array('postal'),
		'phone' => array('rule1' => array('rule' => array('between', 10, 10), 'message' => 'Your phone number must be 10 numeric digits in length.'),
				         'rule2' => array('rule' => 'numeric',                'message' => 'Please enter only numeric digits')),
		'fax' => array('rule1' => array('allowEmpty'=>true, 'rule' => array('between', 10, 10), 'message' => 'Your fax number must be 10 numeric digits in length.'),
				       'rule2' => array('allowEmpty'=>true, 'rule' => 'numeric',                'message' => 'Please enter only numeric digits')),
		'sms' => array('rule1' => array('allowEmpty'=>true, 'rule' => array('between', 10, 10), 'message' => 'Your sms enabled number must be 10 numeric digits in length.'),
				       'rule2' => array('allowEmpty'=>true, 'rule' => 'numeric',                'message' => 'Please enter only numeric digits')),
		'contact_fname' => array('notempty'),
		'contact_lname' => array('notempty'),
		'contact_title' => array('notempty'),
		'contact_phone' => array('rule1' => array('rule' => array('between', 10, 10), 'message' => 'Your phone number must be 10 numeric digits in length.'),
				         		 'rule2' => array('rule' => 'numeric',                'message' => 'Please enter only numeric digits')),
		'contact_email' => array('rule'       => 'email', 'message' => 'Please enter a valid email address.')
	);
	
	var $actsAs = array('Geocoded'  => array('key' => 'ABQIAAAAV7Wn_Tt2hDW924MR8sZnERSPJ9-aYJHKYo5MBtfMlkzoO6a8YBTL4GxXOzxk0u3Yy-vENhzXY22MZg'),
					    'Sluggable' => array('separator'	=> '-',
											 'label'  		=> array('name', 'city'),
											 'overwrite' 	=> false));
    
    var $hasAndBelongsToMany = array(
        'Topping' => array(
                'className'              => 'Topping',
                'joinTable'              => 'locations_toppings',
                'foreignKey'             => 'location_id',
                'associationForeignKey'  => 'topping_id',
                'unique'                 => false,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
    	),
    	'Cat' => array(
                'className'              => 'Cat',
                'joinTable'              => 'locations_cats',
                'foreignKey'             => 'location_id',
                'associationForeignKey'  => 'cat_id',
                'unique'                 => false,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => array('LocationsCat.list_order'),
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
    	),
    	'Crust' => array(
                'className'              => 'Crust',
                'joinTable'              => 'locations_crusts',
                'foreignKey'             => 'location_id',
                'associationForeignKey'  => 'crust_id',
                'unique'                 => false,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
    	),
    	'DiscountCode' => array(
                'className'              => 'DiscountCode',
                'joinTable'              => 'locations_discount_codes',
                'foreignKey'             => 'location_id',
                'associationForeignKey'  => 'discount_code_id',
                'unique'                 => false,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
    	)
    );
    
 	/*
    var $hasAndBelongsToMany = array('Topping' 		=> array('with'=>'LocationsTopping'),
    								 'Cat'	   		=> array('with'=>'LocationsCat'),
    								 'Crust'   		=> array('with'=>'LocationsCrust'),
    								 'DiscountCode' => array('with'=>'LocationsDiscountCode'));
    */
    
	var $belongsTo = array(
			'Advertiser' => array('className' => 'Advertiser',
								'foreignKey' => 'advertiser_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'State' => array('className' => 'State',
								'foreignKey' => 'state_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Banner' => array('className' => 'Banner',
								'foreignKey' => 'banner_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'ZipCode' => array('className' => 'ZipCode',
								'foreignKey' => 'zip_code_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'AdvertisersPaymentProfile' => array('className' => 'AdvertisersPaymentProfile',
								'foreignKey' => 'advertisers_payment_profile_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'FoodCat' => array('className' => 'FoodCat',
								'foreignKey' => 'food_cat_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'LocationsAd' => array('className' => 'LocationsAd',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Item' => array('className' => 'Item',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => array('Item.list_order', 'Item.id'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Combo' => array('className' => 'Combo',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => array('order', 'title'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Favorite' => array('className' => 'Favorite',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Size' => array('className' => 'Size',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => array('size' => 'ASC', 'base_price' => 'ASC'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'Special' => array('className' => 'Special',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'LocationsTime' => array('className' => 'LocationsTime',
								'foreignKey' => 'location_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => array('order'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'LocationsMonthTotal' => array('className' => 'LocationsMonthTotal',
								'foreignKey' => 'location_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	//this function pulls all locations for a passed advertiser id
	function allLocationsByAdvertiser($advertiserid, $list_syntax = null, $recursion = 0, $fields = null) {
		$params = array('conditions' => array('Location.advertiser_id' => $advertiserid),
						'recursive'  => $recursion);
		if($fields) $params['fields'] = $fields;
		return $this->find(($list_syntax) ? 'list':'all', $params);
	}
	
	function getLocationInfo($id) {
		$loc = $this->find('first', array('conditions' => array('Location.id'=>$id),
										  'recursive'  => 0));
		
		$loc['LocationsTime'] = $this->LocationsTime->find('all', array('conditions' => array('LocationsTime.location_id' => $id),
																		'recursive'  => -1));
		return $loc;
	}
	
	//this function checks a locations close time to determine if they are currently open, compared to the current time.
	//returns true for open, false for store closed
	function isLocationOpen($id) {
		//find location close time
		$loc = $this->LocationsTime->find('first', array('conditions' => array('LocationsTime.location_id' => $id,
																			   'LocationsTime.day'         => $this->getDayAbbr()),
														 'fields'     => array('time_open', 'time_closed', 'time_open2', 'time_closed2', 'closed')));
		$current_time = (date('H:i:s', time() + ($timezone_offset = -2) * 60 * 60));
		
		if($current_time < $loc['LocationsTime']['time_open'] || 
		   $current_time > $loc['LocationsTime']['time_closed']) return false;
		   
		if(!empty($loc['LocationsTime']['time_open2']) && !empty($loc['LocationsTime']['time_closed2'])) {
			if($current_time < $loc['LocationsTime']['time_open2'] || 
		   	   $current_time > $loc['LocationsTime']['time_closed2']) return false;
		}
		return ($loc['LocationsTime']['closed'] == 1) ? false : true;
	}
	
	//this function checks if a menu is active to display
	function isMenuReady($loc_id) {
		$l = $this->read(array('active','menu_ready'), $loc_id);
		return ($l['Location']['active'] != 1 || $l['Location']['menu_ready'] != 1) ? false : true;
	}
	
	//check if a logo is set for the restaurant first, if not then check if advertisers logo set
	function getLocationLogo($loc_id) {
		$l = $this->read(array('advertiser_id', 'logo'), $loc_id);
		
		if(!empty($l['Location']['logo']) && $l['Location']['logo'] != "") { 
			return $l['Location']['logo'];
		} else {
			$ad = $this->Advertiser->read('logo', $l['Location']['advertiser_id']);
			return $ad['Advertiser']['logo'];	
		}
	}
	
	function getSizes($loc_id) {
		return $this->Size->find('all', array('conditions' => array('Size.location_id' => $loc_id),
											  'recursive'  => -1,
											  'order'      => array('size', 'base_price')));
	}
	
	function getSizeList($loc_id) {
		return $this->Size->find('list', array('conditions' => array('Size.location_id' => $loc_id),
											   'fields'     => array('Size.title')));
	}
	
	function getSpecials($loc_id) {
		$specials = $this->Special->find('all', array('conditions' => array('Special.location_id' => $loc_id),
											          'recursive'  => -1));
		foreach($specials as $i => $s):
			$specials[$i]['Special']['SpecialsSize'] = $this->Special->getSizes($s['Special']['id']);
		endforeach;
		return $specials;
	}
	
	function getCombos($loc_id, $fields = null) {
		$opts = array('conditions' => array('Combo.location_id' => $loc_id),
					  'recursive'  => '-1',
					  'order'	   => array('order', 'title'));
		if($fields) $opts['fields'] = $fields;
		return $this->Combo->find('all', $opts);
	}
	
	function getCats($loc_id) {
		return $this->LocationsCat->find('all', array('conditions' => array('LocationsCat.location_id' => $loc_id),
													  'contain'    => array('Cat', 'LocationsCatsOption'),					
													  'order'      => array('LocationsCat.list_order')));
	}
	
	function getAllItemsByCat($loc_id) {
		$cats = $this->LocationsCat->find('all', array('conditions' => array('LocationsCat.location_id' => $loc_id),
													   'order'      => array('LocationsCat.list_order'),
													   'recursive'  => 0,
													   'fields'     => array('Cat.title', 'Cat.id', 'LocationsCat.subtext', 'LocationsCat.id')));
		foreach ($cats as $i => $c):
			$cats[$i]['Items'] = $this->Item->getItemsByCat($c['Cat']['id'], $loc_id);
		endforeach;
		return $cats;
	}
	
	function getToppings($loc_id) {
		return $this->LocationsTopping->find('all', array('conditions' => array('LocationsTopping.location_id' => $loc_id),
											  			  'recursive'  => 0, 
														  'order'	   => array('LocationsTopping.order', 'ToppingType.title', 'Topping.title'),
														  'fields'	   => array('LocationsTopping.id', 'LocationsTopping.topping_id', 'LocationsTopping.order', 
														  						'LocationsTopping.price_alter', 'ToppingType.title', 'Topping.title')));
	}
	
	function getToppingList($id) {
		$toppings = $this->LocationsTopping->find('all', array('conditions' => array('LocationsTopping.location_id' => $id),
														  		'order'	   => array('LocationsTopping.order', 'Topping.title'),
														  		'fields'	   => array('LocationsTopping.id', 'Topping.title')));
		$list_array = array();
		foreach($toppings as $t):
			$list_array[$t['LocationsTopping']['id']] = $t['Topping']['title'];
		endforeach;
		return $list_array;
	}
	
	function getCrusts($loc_id) {
		return $this->LocationsCrust->find('all', array('conditions' => array('LocationsCrust.location_id'=>$loc_id),
														'order'		 => array('order' => 'ASC'),
														'fields'	 => array('LocationsCrust.id', 'LocationsCrust.price', 'LocationsCrust.order',
																			  'Crust.title', 'LocationsCrust.crust_id'),
														'recursive'  => 0));
	}
	
	//return a list array of the crust types that the selected location offers
	function getCrustList($loc_id) {
		$crusts = $this->LocationsCrust->find('all', array('conditions' => array('LocationsCrust.location_id'=>$loc_id),
														   'fields'		=> array('LocationsCrust.id', 'LocationsCrust.price', 'LocationsCrust.order',
																				 'Crust.title', 'LocationsCrust.crust_id'),
														   'order'		=> array('order' => 'ASC')));
		$opts = array();
		foreach($crusts as $c):
			$opts[$c['LocationsCrust']['id']] = $c['Crust']['title'];
			if($c['LocationsCrust']['price'] > 0) {
				$opts[$c['LocationsCrust']['id']] .= ' (+$'.number_format($c['LocationsCrust']['price'], 2).')';
			}
		endforeach;
		return $opts;		
	}
	
	function getCatList($loc_id) {
		$cats = $this->LocationsCat->find('list', array('conditions' => array('LocationsCat.location_id' => $loc_id),
													   'order'      => array('LocationsCat.list_order'),
													   'fields'     => array('LocationsCat.cat_id')));
		$list = array();
		foreach($cats as $i => $c ):
			$temp = $this->Cat->read('title', $c);
			$list[$c] = $temp['Cat']['title'];
		endforeach;
		return $list;
	}

	function getFieldsForBanner($id, $additional_fields = null) {
		$fields = array('Location.name', 'Location.address1', 'Location.address2', 'Banner.filename', 
						'Location.city', 'Location.phone', 'Location.id', 'ZipCode.id', 'Location.slug', 'Advertiser.slug', 
						'Advertiser.logo', 'Advertiser.name', 'Location.offer_pizza', 'Location.show_menu_images', 
					    'Location.slogan', 'Location.use_logo_in_banner', 'Location.logo', 'Location.acct_type');
		
		if(!empty($additional_fields)) {
			$more_fields = array_merge($fields, $additional_fields);
			$fields = $more_fields;
		}
		
		return $this->find('first', array('conditions' => array('Location.id'=>$id),
										  'recursive'  => 0,
										  'fields'     => $fields));
	}

	function idBySlug($slug) {
		$l = $this->find('first', array('conditions' => array('Location.slug' => $slug),
										'fields'     => 'Location.id'));
		return $l['Location']['id'];
	}
	
	function getSlugFromId($id) {
		$l = $this->find('first', array('conditions' => array('Location.id' => $id),
										'fields'     => 'Location.slug'));
		return $l['Location']['slug'];
	}

	function getUrlSlugs($id) {
		$l = $this->read(array('Advertiser.slug', 'Location.slug'), $id);
		//return $l['Advertiser']['slug'].'/'.$l['Location']['slug'];
		
		return $l['Location']['slug'];
	}
	
	//find and return appropriate index number to be used with locations accordion in advertiser dashboard
	function getArrayIndex($loc_id) {
		$ad = $this->read('advertiser_id', $loc_id);
		
		$locs = $this->find('all', array('conditions' => array('Location.advertiser_id' => $ad['Location']['advertiser_id']),
										 'order'      => array('Location.name'),
										 'fields'     => array('Location.id'),
										 'recursive'  => -1));
		foreach($locs as $i => $loc):
			if($loc['Location']['id'] == $loc_id) return $i;
		endforeach;
		return false;
	}

	function getOrders($id, $start_date = null, $end_date = null) {
		$conditions = array('Order.location_id' => $id,
							'Order.complete'    => '1');
		
		//is there start/end date search criteria?
		if($start_date && $end_date) {
			$conditions['Order.timestamp > '] = $start_date;
			$conditions['Order.timestamp < '] = $end_date;
		}
		
		return $this->Order->find('all', array('conditions' => $conditions,
												'contain'   => array('User', 'UsersAddr', 'Location'),
												'order'		=> array('Order.timestamp' => 'DESC')));
	}
	
	function locationListByAdvertiser() {
		//first get all advertisers
		$ads = $this->Advertiser->find('all', array('fields' => array('Advertiser.id', 'Advertiser.name'),
													'order'  => array('Advertiser.name')));
		$return_array = array();
		foreach($ads as $a):
			$locs = $this->find('list', array('conditions' => array('Location.advertiser_id' => $a['Advertiser']['id']),
												'fields'     => array('Location.id', 'Location.name'),
												'order'		 => array('Location.name')));
			
			if(sizeof($locs) > 0) {
				$return_array[$a['Advertiser']['name']] = $locs;	
			}
		endforeach;
		return $return_array;
	}

	// right now we are currently manually omitting Doorstep Dining records.
	function getPendingMenus() {
		$menus = $this->find('all', array('conditions' => array('Location.active' => '0',
															  	'NOT' => array('Advertiser.id' 			=> '37',
															  				   'Location.menu_ready' 	=> '0')),
										  'recursive'  => 0,
										  'contain'    => array('Advertiser')));
		foreach($menus as $i => $m):
			$menus[$i]['numPayProfiles'] = $this->Advertiser->AdvertisersPaymentProfile->find('count', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id' => $m['Location']['advertiser_id'])));
		endforeach;
		return $menus;
	}
	
	function getLocsBySearch($conditions = array()) {
		$menus = $this->find('all', array('conditions' => $conditions,
										'recursive'  => 0,
										'contain'    => array('Advertiser')));
		foreach($menus as $i => $m):
			$menus[$i]['numPayProfiles'] = $this->Advertiser->AdvertisersPaymentProfile->find('count', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id' => $m['Location']['advertiser_id'])));
		endforeach;
		return $menus;
	}

	function getDistanceFormula($searchCoords) {
		return "( 3959 * acos( cos( radians(" . $searchCoords['latitude'] . 
			   ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $searchCoords['longitude'] . 
			   ") ) + sin( radians(" . $searchCoords['latitude'] . ") ) * sin( radians( latitude ) ) ) )";
	}
	
	//function is passed the lat/long coords of visiting user. returns up to 3 featured restaurants, within a max radius of 20 miles. 
	function getFeatured($lat = DEFAULT_LATITUDE, $long = DEFAULT_LONGITUDE) {
		$fields = array('Advertiser.logo', 'Location.name', 'Location.id', 'Location.city',
					   'Location.slug', 'State.abbr', 'Location.logo', 'ZipCode.slug', 
					   'Location.use_logo_in_banner', 'Advertiser.slug', 'ZipCode.city',
					   'FoodCat.title');
		
		$conditions = array('Location.active'     => '1', 
							'Location.featured'   => '1', 
							'Location.menu_ready' => '1');
		
		$conditionString = $this->getDistanceFormula(array('latitude' => $lat, 'longitude' => $long));
		$condition = $conditionString . " <= ";
		$conditions[$condition] = " 20";
		$fields[''] = $conditionString . " as distance";
		
		$locs = $this->find('all', array('conditions' => $conditions,
										'limit' 	 => 3,
										'contain' 	 => array('Advertiser', 'State', 'ZipCode', 'FoodCat'),
										'fields'	 => $fields,
										'order' 	 => array('RAND()')));
										
		return $locs;
	}
}
?>