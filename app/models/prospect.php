<?php
class Prospect extends AppModel {
    var $name = 'Prospect';
	var $validate = array(
		'name' => array('notempty'),
		'city' => array('notempty'),
		'state_id' => array('notempty'),
		'phone' => array('rule1' => array('rule' => array('between', 10, 10), 'allowEmpty' => true,
	                                      'message' => 'Your phone number must be 10 numeric digits in length.'),
			             'rule2' => array('rule' => 'numeric',                'allowEmpty' => true,
			                              'message' => 'Please enter only numeric digits'))
	);
	
	var $actsAs = array('Geocoded'  => array('key' => 'ABQIAAAAV7Wn_Tt2hDW924MR8sZnERSPJ9-aYJHKYo5MBtfMlkzoO6a8YBTL4GxXOzxk0u3Yy-vENhzXY22MZg'));
	
	var $belongsTo = array(
			'State' => array('className' => 'State',
							 'foreignKey' => 'state_id',
							 'conditions' => '',
							 'fields' => '',
							 'order' => ''
			),
			'ZipCode' => array('className' => 'ZipCode',
							 'foreignKey' => 'zip_code_id',
							 'conditions' => '',
							 'fields' => '',
							 'order' => ''
			),
			'Region' => array('className' => 'Region',
							 'foreignKey' => 'region_id',
							 'conditions' => '',
							 'fields' => '',
							 'order' => ''
			)
	);
}
?>