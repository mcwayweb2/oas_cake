<?php
class ToppingType extends AppModel {

	var $name = 'ToppingType';

	var $hasMany = array(
		'LocationsTopping' => array('className' => 'LocationsTopping',
							'foreignKey' => 'topping_type_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		)
	);

}
?>