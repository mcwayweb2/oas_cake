<?php
class AdvertisersPaymentProfile extends AppModel {

	var $name = 'AdvertisersPaymentProfile';
	var $validate = array(
		'name' 		=> array('notempty'),
		'lname' 	=> array('notempty'),
		'fname' 	=> array('notempty'),
		'address' 	=> array('notempty'),
		'city' 		=> array('notempty'),
		'zip' 		=> array('numeric'),
		'cc_num' 	=> array(
			'rule' => array('cc', 'all', false, null),
			'message' => 'This credit card number is invalid.'),
		'routing' 	=> array('rule1' => array('rule' => array('between', 9, 9), 'message' => 'Routing Number must be 9 digits.'),
				         	 'rule2' => array('rule' => 'numeric',              'message' => 'Please enter only numeric digits.')),
		'account' 	=> array('rule1' => array('rule' => array('between', 5, 17), 'message' => 'Account Number must be between 5-17 digits.'),
				         	 'rule2' => array('rule' => 'numeric',               'message' => 'Please enter only numeric digits.'))
	);

	var $belongsTo = array(
			'Advertiser' => array('className' => 'Advertiser',
								'foreignKey' => 'advertiser_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'advertisers_payment_profile_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			),
			'AdvertisersPaymentProfilesTransaction' => array('className' => 'AdvertisersPaymentProfilesTransaction',
								'foreignKey' => 'advertisers_payment_profile_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	//return all payment profiles for an advertiser, in an a select dropdown array format
	function payProfileList($advertiser_id) {
		$profiles = $this->find('all', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id'=>$advertiser_id),
											 'fields'     => array('AdvertisersPaymentProfile.id', 'AdvertisersPaymentProfile.name', 
																   'AdvertisersPaymentProfile.card_suffix')));
		$pay_array = array();
		foreach($profiles as $profile):
			$pay_array[$profile['AdvertisersPaymentProfile']['id']] = $profile['AdvertisersPaymentProfile']['name']." (".
																	  $profile['AdvertisersPaymentProfile']['card_suffix'].")";
		endforeach;
		return $pay_array;
	}
	
	//return an advertisers current default payment profile
	function getDefaultProfile($advertiser_id) {
		return $this->find('first', array('conditions' => array('AdvertisersPaymentProfile.default' 	  => '1', 
																'AdvertisersPaymentProfile.advertiser_id' => $advertiser_id),
										  'contain'    => array('Advertiser.profile_id')));
	}
	
	// record an auth.net user transaction
	function saveTransaction($pay_profile_id = null, $transaction_id, $amt, $loc_id = null, $desc = null, $type = 'Once') {
		$this->AdvertisersPaymentProfilesTransaction->create();
		
		return $this->AdvertisersPaymentProfilesTransaction->save(array('AdvertisersPaymentProfilesTransaction' => 
																		array('advertisers_payment_profile_id' 	=> $pay_profile_id,
																		 	  'transaction_id'           	   	=> $transaction_id,
																			  'location_id'              		=> $loc_id,
																		 	  'type' 							=> $type,
																		 	  'amount' 							=> $amt,
																		 	  'description' 					=> $desc,
																		 	  'timestamp' 						=> date('Y-m-d H:i:s'))));
	}
}
?>