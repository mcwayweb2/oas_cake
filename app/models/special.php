<?php
class Special extends AppModel {

	var $name = 'Special';
	var $validate = array(
		'title' => array('notempty')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);

	var $hasAndBelongsToMany = array(
    	'Size' => array('className'              => 'Size',
                		'joinTable'              => 'specials_sizes',
                		'foreignKey'             => 'special_id',
		                'associationForeignKey'  => 'size_id',
		                'unique'                 => true,
		                'conditions'             => '',
		                'fields'                 => '',
		                'order'                  => array('size' => 'ASC'),
		                'limit'                  => '',
		                'offset'                 => '',
		                'finderQuery'            => '',
		                'deleteQuery'            => '',
		                'insertQuery'            => '')	
    );
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'SpecialsSize' => array('className' => 'SpecialsSize',
								'foreignKey' => 'special_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	function getSizes($special_id) {
		return $this->SpecialsSize->find('all', array('conditions' => array('SpecialsSize.special_id' => $special_id),
													  'fields'	   => array('Size.id', 'Size.size', 'Size.title', 'SpecialsSize.price', 'SpecialsSize.id'),
													  'order'	   => array('Size.size' => 'ASC')));
	}
	/*
	function getSizeList($special_id) {
		return $this->SpecialsSize->find('all', array('conditions' => array('SpecialsSize.special_id' => $special_id),
													   'fields'	   => array('SpecialsSize.id', 'Size.title'),
													  'order'      => array('Size.size')));
	}
	*/
	
	function getSpecialInfo($special_id, $recursion = -1) {
		return $this->find('first', array('conditions' => array('Special.id' => $special_id),
										  'recursive'  => $recursion));
	}
}
?>