<?php
class Crust extends AppModel {

	var $name = 'Crust';
	var $validate = array('title' => array('notempty'));
	
    var $hasAndBelongsToMany = array('Location' => array('with'=>'LocationsCrust'));

    //return a list array of all current crust types to choose from
    function getCrustList() {
    	return $this->find('list', array('fields' => array('Crust.title'), 'order' => array('Crust.title')));
    }
}
?>