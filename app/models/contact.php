<?php
class Contact extends AppModel {
    var $name = 'Contact';
	var $validate = array(
		'fname' => array('notempty'),
		'lname' => array('notempty'),
		'type' => array('notempty'),
		'email' => array('email'),
		'phone' => array('rule1' => array('rule' => array('between', 10, 10), 'allowEmpty' => true,
	                                      'message' => 'Your phone number must be 10 numeric digits in length.'),
			             'rule2' => array('rule' => 'numeric',                'allowEmpty' => true,
			                              'message' => 'Please enter only numeric digits'))
	);
	
	var $belongsTo = array(
			'State' => array('className' => 'State',
							 'foreignKey' => 'state_id',
							 'conditions' => '',
							 'fields' => '',
							 'order' => ''
			)
	);
}
?>