<?php
class Pizza extends AppModel {

	var $name = 'Pizza';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'order_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'LocationsCrust' => array('className' => 'LocationsCrust',
								'foreignKey' => 'locations_crust_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Size' => array('className' => 'Size',
								'foreignKey' => 'size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			) 
	);
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'PizzasTopping'));

}
?>