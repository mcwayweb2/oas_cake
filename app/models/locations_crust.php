<?php
class LocationsCrust extends AppModel {

	var $name = 'LocationsCrust';
	var $validate = array('price' => array('rule1' => array('rule' => 'money', 'allowEmpty' => true, 'message' => 'Your phone number must be 10 numeric digits in length.')));
	
	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Crust' => array('className' => 'Crust',
								'foreignKey' => 'crust_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
		'Pizza' => array('className' => 'Pizza',
							'foreignKey' => 'locations_crust_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		),
		'OrdersCombosPizza' => array('className' => 'OrdersCombosPizza',
							'foreignKey' => 'locations_crust_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		),
		'OrdersSpecialsSize' => array('className' => 'OrdersSpecialsSize',
							'foreignKey' => 'locations_crust_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		),
		'OrdersCombosSpecialsSize' => array('className' => 'OrdersCombosSpecialsSize',
							'foreignKey' => 'locations_crust_id',
							'dependent' => false,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => '',
							'finderQuery' => '',
							'counterQuery' => ''
		)
	);	

	function getCrustInfo($id = null) {
		if(!$id) return false;
		return $this->find('first', array('conditions' => array('LocationsCrust.id' => $id),
										  'contain'    => array('Crust')));
	}
}
?>