<?php
class LocationsAd extends AppModel {

	var $name = 'LocationsAd';
	var $validate = array(
		'title' => array('notempty'),
		'size' => array('notempty'),
		'image' => array('notempty')
	);

	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	function getAds($location_id) {
		$ads = array();
		$ads['Home'] = $this->find('all', array('conditions' => array('LocationsAd.location_id' => $location_id,
																	  'LocationsAd.type'		=> 'Home'),
												'order'		 => array('LocationsAd.rotation' => 'DESC', 'LocationsAd.id' => 'ASC'),
												'recursive'  => -1));
		$ads['Sub'] = $this->find('all', array('conditions' => array('LocationsAd.location_id' => $location_id,
																	  'LocationsAd.type'		=> 'Sub'),
												'order'		 => array('LocationsAd.rotation' => 'DESC', 'LocationsAd.id' => 'ASC'),
												'recursive'  => -1));
		$ads['Btn'] = $this->find('all', array('conditions' => array('LocationsAd.location_id' => $location_id,
																	  'LocationsAd.type'		=> 'Btn'),
												'order'		 => array('LocationsAd.rotation' => 'DESC', 'LocationsAd.id' => 'ASC'),
												'recursive'  => -1));
		
		return $ads;
	}
}
?>