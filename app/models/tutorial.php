<?php
class Tutorial extends AppModel {

	var $name = 'Tutorial';
	
	var $validate = array(
		'title' => array('notempty'),
		'url' => array('notempty')
	);
	
	var $actsAs = array('Sluggable' => array('separator'	=> '-',
											 'label'  		=> 'title',
											 'overwrite' 	=> true));
	
	var $belongsTo = array('TutorialsType' => array('className' => 'TutorialsType',
													'foreignKey' => 'type_id',
													'conditions' => '',
													'fields' => '',
													'order' => ''));
	var $order = array('Tutorial.type_id', 'Tutorial.sort');
	
	function getTypeList() {
		return $this->TutorialsType->find('list');
	}
	
	//return all tutorials grouped by cat type
	function allTutorialsByType() {
		$types = $this->TutorialsType->find('list');
		
		$tuts = array();
		foreach($types as $type_id => $title):
			$tuts[$title] = $this->find('all', array('conditions' => array('Tutorial.type_id' => $type_id)));
		endforeach;
		
		
		return $tuts;
	}
}
?>