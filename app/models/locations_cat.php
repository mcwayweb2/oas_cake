<?php
class LocationsCat extends AppModel {

	var $name = 'LocationsCat';

	var $belongsTo = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'location_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Cat' => array('className' => 'Cat',
								'foreignKey' => 'cat_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'LocationsCatsOption' => array('className' => 'LocationsCatsOption',
								'foreignKey' => 'locations_cat_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => array('LocationsCatsOption.add_group_id'     => 'DESC',
												 'LocationsCatsOption.id' => 'ASC'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>