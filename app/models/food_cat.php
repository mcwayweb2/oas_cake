<?php
class FoodCat extends AppModel {

	var $name = 'FoodCat';
	
	var $hasMany = array(
			'Location' => array('className' => 'Location',
								'foreignKey' => 'food_cat_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>