<?php
class Region extends AppModel {

	var $name = 'Region';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
			'Prospect' => array('className' => 'Prospect',
								'foreignKey' => 'region_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>