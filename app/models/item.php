<?php
class Item extends AppModel {

	var $name = 'Item';
	
	var $validate = array(
		'cat_id' => array('numeric'),
		'location_id' => array('numeric'),
		'title' => array('notempty'),
		'price' => array('numeric')
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Cat' => array('className' => 'Cat',
							'foreignKey' => 'cat_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
			),
			'Location' => array('className' => 'Location',
							'foreignKey' => 'location_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
			)
	);
	
	var $hasMany = array(
			'ItemsOption' => array('className' => 'ItemsOption',
								'foreignKey' => 'item_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => array('ItemsOption.add_group_id' => 'DESC', 'ItemsOption.id'),
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	var $hasAndBelongsToMany = array('Order' => array('with'=>'OrdersItem'),
									 'OrdersCombo' => array('with'=>'OrdersCombosItem'));
	
	function getItemsByCat($cat_id, $loc_id) {
		
		$items = $this->find('all', array('conditions' => array('Item.location_id' => $loc_id, 'Item.cat_id' => $cat_id),
										  'order'	   => array('Item.list_order', 'Item.id'),
									      'recursive'  => '-1'));
		foreach($items as $index => $i):
			$items[$index]['ItemsOption'] = $this->ItemsOption->find('all', array('conditions' => array('ItemsOption.item_id' => $i['Item']['id']),
														                          'recursive'  => -1,
																				  'order'      => array('ItemsOption.price')));
		endforeach;
		return $items;
	}
	
	function getItemOpts($item_id) {
		return $this->ItemsOption->find('list', array('conditions' => array('ItemsOption.item_id' => $item_id),
													  'fields'     => array('ItemsOption.id', 'ItemsOption.label')));
	}
	
	function getItemInfo($item_id, $include_item_options = true) {
		$i = $this->find('first', array('conditions' => array('Item.id' => $item_id),
										'contain'    => array('Cat', 'Location.show_menu_images')));
		if($include_item_options) {
			$add_groups = $this->ItemsOption->find('list', array('conditions' => array('ItemsOption.item_id' => $item_id),
																 'fields'     => array('ItemsOption.add_group_id'),
																 'group'      => 'ItemsOption.add_group_id',
																 'order'      => array('ItemsOption.add_group_id' => 'DESC')));
			
			//if there is at least 1 add group of options to deal with
			if(sizeof($add_groups) > 0) {
				$add_array = array();
				foreach($add_groups as $add_group_id):
					$opts = $this->ItemsOption->find('all', array('conditions' => array('ItemsOption.item_id' => $item_id,
																						'ItemsOption.add_group_id' => $add_group_id),
														  		  'recursive'  => '-1'));
					foreach($opts as $index => $o):
						$add_array[$add_group_id]['OptList'][$o['ItemsOption']['id']] = $o['ItemsOption']['label'];
						if($o['ItemsOption']['price'] > 0) {
							$add_array[$add_group_id]['OptList'][$o['ItemsOption']['id']] .= '  (+$'.number_format($o['ItemsOption']['price'], 2).')';
						}  
					endforeach;
					
					$add_array[$add_group_id]['add_group_limit'] = $o['ItemsOption']['add_group_limit'];
				endforeach;
				$i['ItemsOption'] = $add_array;
			}
			
			//now check to see if there are any additional options that come with the category
			
			//first we need the location cats id
			$loc_cat = $this->Location->LocationsCat->find('first', array('conditions' => array('LocationsCat.location_id' => $i['Item']['location_id'],
																								'LocationsCat.cat_id'      => $i['Item']['cat_id']),
																		  'fields'     => array('LocationsCat.id')));
			
			
			$add_groups = $this->Location->LocationsCat->LocationsCatsOption->find('list', array('conditions' => array('LocationsCatsOption.locations_cat_id' => $loc_cat['LocationsCat']['id']),
																								 'fields'     => array('LocationsCatsOption.add_group_id'),
																								 'group'      => 'LocationsCatsOption.add_group_id',
																								 'order'      => array('LocationsCatsOption.add_group_id' => 'DESC')));
			if(sizeof($add_groups) > 0) {
				$add_array = array();
				foreach($add_groups as $add_group_id):	
					$opts = $this->Location->LocationsCat->LocationsCatsOption->find('all', array('conditions' => array('LocationsCatsOption.locations_cat_id' => $loc_cat['LocationsCat']['id'],
																														'LocationsCatsOption.add_group_id'     => $add_group_id),
																								  'fields'     => array('LocationsCatsOption.id', 'LocationsCatsOption.add_group_limit',
																														'LocationsCatsOption.label', 'LocationsCatsOption.additional_price'),
														  		  								  'recursive'  => '-1'));
					foreach($opts as $index => $o):
						$add_array[$add_group_id]['OptList'][$o['LocationsCatsOption']['id']] = $o['LocationsCatsOption']['label'];
						if($o['LocationsCatsOption']['additional_price'] > 0) {
							$add_array[$add_group_id]['OptList'][$o['LocationsCatsOption']['id']] .= '  (+$'.number_format($o['LocationsCatsOption']['additional_price'], 2).')';
						}  
					endforeach;
					
					$add_array[$add_group_id]['add_group_limit'] = $o['LocationsCatsOption']['add_group_limit'];
				endforeach;
				$i['CatsOption'] = $add_array;
			}
		}
		return  $i;
	}
}
?>