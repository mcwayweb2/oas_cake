<?php
class TutorialsType extends AppModel {

	var $name = 'TutorialsType';
	
	var $validate = array(
		'title' => array('notempty')
	);
	
	var $hasMany = array(
			'Tutorial' => array('className' => 'Tutorial',
								'foreignKey' => 'type_id',
								'dependent' => false,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
		);
}
?>