<?php
class OrdersCombosPizzasTopping extends AppModel {

	var $name = 'OrdersCombosPizzasTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersCombosPizza' => array('className' => 'OrdersCombosPizza',
								'foreignKey' => 'orders_combos_pizza_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
}
?>