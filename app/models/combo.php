<?php
class Combo extends AppModel {

	var $name = 'Combo';
	
	var $validate = array(
		'title' => array('notempty'),
		'price' => array('rule' => 'money', 'message' => 'Please enter a valid price.')
	);
	
	var $belongsTo = array(
		'Location' => array('className' => 'Location',
							'foreignKey' => 'location_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasAndBelongsToMany = array('Item' => array('with'=>'CombosItem'),
									 'Order'=> array('with'=>'OrdersCombo'));
	
	var $hasMany = array(
		'CombosPizza' => array('className' => 'CombosPizza',
							'foreignKey' => 'combo_id',
							'dependent' => true,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => true,
							'finderQuery' => '',
							'counterQuery' => ''
		),
		'CombosTextItem' => array('className' => 'CombosTextItem',
							'foreignKey' => 'combo_id',
							'dependent' => true,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => true,
							'finderQuery' => '',
							'counterQuery' => ''
		),
		'CombosSpecialsSize' => array('className' => 'CombosSpecialsSize',
							'foreignKey' => 'combo_id',
							'dependent' => true,
							'conditions' => '',
							'fields' => '',
							'order' => '',
							'limit' => '',
							'offset' => '',
							'exclusive' => true,
							'finderQuery' => '',
							'counterQuery' => ''
		)
	);
	
	function getComboInfo($id) {
		$c = $this->find('first', array('conditions' => array('Combo.id' => $id),
										'fields'     => array('Combo.id', 'Combo.location_id', 'Combo.title', 'Combo.description', 'Combo.price',
															  'Combo.image', 'Location.slug', 'Location.show_menu_images'),
										'recursive'  => 0));
		
		$c['CombosTextItem'] = $this->CombosTextItem->find('all', array('conditions' => array('CombosTextItem.combo_id' => $id),
																  	  	'recursive'  => -1));
		
		$c['CombosPizza'] = $this->CombosPizza->find('all', array('conditions' => array('CombosPizza.combo_id' => $id),
																  'recursive'  => 0));
		
		foreach($c['CombosPizza'] as $i => $pizza):
			$c['CombosPizza'][$i]['Topping'] = $this->CombosPizza->getToppings($pizza['CombosPizza']['id']);
		endforeach;
		
		$c['CombosSpecialsSize'] = $this->CombosSpecialsSize->find('all', array('conditions' => array('CombosSpecialsSize.combo_id' => $id),
																  				'recursive'  => -1));
		foreach($c['CombosSpecialsSize'] as $i => $special):
			if(!empty($special['CombosSpecialsSize']['size_id_temp'])) {
				//can choose any specialty pizza of a certain size
				$size = $this->CombosPizza->Size->read(array('title', 'size', 'extra_cheese', 'extra_sauce'), $special['CombosSpecialsSize']['size_id_temp']);
				$c['CombosSpecialsSize'][$i]['Size'] = $size['Size'];

				//create list of specialty pizzas available in the selected size, for dropdown menus
				$params = array('conditions' => array('SpecialsSize.size_id' => $special['CombosSpecialsSize']['size_id_temp']),
								'fields'     => array('SpecialsSize.id', 'Special.title'),
								'order'		 => array('Special.title'),
								'recursive'  => 1);
				$c['CombosSpecialsSize'][$i]['SpecialsSize'] = $this->Location->Special->SpecialsSize->find('list', $params);
			} else {
				//we are adding a specific specialty pizza
				$specials_size = $this->CombosPizza->Size->SpecialsSize->read(array('size_id', 'special_id'), $special['CombosSpecialsSize']['specials_size_id']);
				$size = $this->CombosPizza->Size->read(array('title', 'size', 'extra_cheese', 'extra_sauce'), $specials_size['SpecialsSize']['size_id']);
				$special = $this->CombosPizza->Size->SpecialsSize->Special->read(array('title', 'subtext'), $specials_size['SpecialsSize']['special_id']);
				$c['CombosSpecialsSize'][$i]['Size'] = $size['Size'];
				$c['CombosSpecialsSize'][$i]['Special'] = $special['Special'];
			}
		endforeach;
		
		$c['Item'] = $this->CombosItem->find('all', array('conditions' => array('CombosItem.combo_id' => $id),
														  'order'      => array('CombosItem.add_group_id'),
														  'fields'     => array('CombosItem.id', 'CombosItem.combo_id', 'CombosItem.item_id',
																				'CombosItem.add_type', 'CombosItem.add_group_id', 'Item.cat_id', 
																				'CombosItem.add_group_limit', 'Item.title', 'Item.description',
																				'CombosItem.qty', 'Item.image', 'CombosItem.temp_cat_id')));
		$series_list_group_id = null;
		foreach($c['Item'] as $i => $item):
			$cat_id = ($item['CombosItem']['add_type'] == 'Cat') ? $item['CombosItem']['temp_cat_id'] : $item['Item']['cat_id']; 
			$cat = $this->CombosItem->Item->Cat->read('title', $cat_id);
			$c['Item'][$i]['Cat'] = $cat['Cat'];
			
			if($item['CombosItem']['add_type'] == 'Series') {
				//make sure this is a new add group
				$c['Item'][$i]['SeriesList'] = $this->CombosItem->find('list', array('conditions' => array('CombosItem.combo_id'     => $item['CombosItem']['combo_id'],
																										   'CombosItem.add_group_id' => $item['CombosItem']['add_group_id']),
																		   			 'fields'     => array('Item.id', 'Item.title'),
																					 'recursive'  => '0'));
				$series_list_group_id = $item['CombosItem']['add_group_id'];	
			} else if($item['CombosItem']['add_type'] == 'Cat') {
				//get all items in this category to select from
				$items_list = $this->Item->find('list', array('conditions' => array('Item.cat_id' 		=> $item['CombosItem']['temp_cat_id'],
																				    'Item.location_id' 	=> $c['Combo']['location_id']),
															  'recursive'  => -1,
															  'fields'     => array('Item.title')));
				$c['Item'][$i]['SeriesList'] = $items_list;
			} else {
				//single item. Need to include its opts
				$c['Item'][$i]['Item'] = $this->Item->getItemInfo($item['CombosItem']['item_id']);
			}
		endforeach;
		
		return $c;
	}
}
?>