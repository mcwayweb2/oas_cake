<?php
class OrdersSpecialsSize extends AppModel {

	var $name = 'OrdersSpecialsSize';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Order' => array('className' => 'Order',
								'foreignKey' => 'order_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'SpecialsSize' => array('className' => 'SpecialsSize',
								'foreignKey' => 'specials_size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'LocationsCrust' => array('className' => 'LocationsCrust',
								'foreignKey' => 'locations_crust_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'OrdersSpecialsSizesTopping' => array('className' => 'OrdersSpecialsSizesTopping',
								'foreignKey' => 'orders_specials_size_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'OrdersSpecialsSizesTopping'));
}
?>