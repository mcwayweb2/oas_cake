<?php
class OrdersItemsItemsOption extends AppModel {

	var $name = 'OrdersItemsItemsOption';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersItem' => array('className' => 'OrdersItem',
								'foreignKey' => 'orders_item_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'ItemsOption' => array('className' => 'ItemsOption',
								'foreignKey' => 'items_option_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
}
?>