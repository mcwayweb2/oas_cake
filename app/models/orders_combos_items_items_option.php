<?php
class OrdersCombosItemsItemsOption extends AppModel {

	var $name = 'OrdersCombosItemsItemsOption';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersCombosItem' => array('className' => 'OrdersCombosItem',
								'foreignKey' => 'orders_combos_item_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'ItemsOption' => array('className' => 'ItemsOption',
								'foreignKey' => 'items_option_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
}
?>