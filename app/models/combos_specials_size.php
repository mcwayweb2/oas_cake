<?php
class CombosSpecialsSize extends AppModel {

	var $name = 'CombosSpecialsSize';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Combo' => array('className' => 'Combo',
								'foreignKey' => 'combo_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'SpecialsSize' => array('className' => 'SpecialsSize',
								'foreignKey' => 'specials_size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'CombosSpecialsSizesTopping' => array('className' => 'CombosSpecialsSizesTopping',
								'foreignKey' => 'combos_specials_size_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'CombosSpecialsSizesTopping'));
	/*
	function getToppings($special_size_id) {
		return $this->OrdersSpecialsSizesTopping->find('all', 
													   array('conditions' => array('OrdersSpecialsSizesTopping.orders_specials_size_id' => $special_size_id)));	
	}
	*/
}
?>