<?php
class OrdersCombosSpecialsSize extends AppModel {

	var $name = 'OrdersCombosSpecialsSize';
	
	var $belongsTo = array(
		'OrdersCombo' => array('className' => 'OrdersCombo',
							'foreignKey' => 'orders_combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'LocationsCrust' => array('className' => 'LocationsCrust',
							'foreignKey' => 'locations_crust_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'SpecialsSize' => array('className' => 'SpecialsSize',
							'foreignKey' => 'specials_size_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'OrdersCombosSpecialsSizesTopping'));
	
	/*
	function getToppings($id) {
		return $this->CombosPizzasTopping->find('all', array('conditions' => array('CombosPizzasTopping.combos_pizza_id' => $id),
											  			     'recursive'  => 0, 
														  	 'fields'	  => array('CombosPizzasTopping.id', 'CombosPizzasTopping.topping_id', 
														  	 					   'CombosPizzasTopping.placement', 'Topping.title')));
	}
	*/
}
?>