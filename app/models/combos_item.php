<?php
class CombosItem extends AppModel {

	var $name = 'CombosItem';
	
	var $belongsTo = array(
			'Combo' => array('className' => 'Combo',
								'foreignKey' => 'combo_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Item' => array('className' => 'Item',
								'foreignKey' => 'item_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	
}
?>