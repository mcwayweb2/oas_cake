<?php
class OrdersCombosPizza extends AppModel {

	var $name = 'OrdersCombosPizza';
	
	var $belongsTo = array(
		'OrdersCombo' => array('className' => 'OrdersCombo',
							'foreignKey' => 'orders_combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'LocationsCrust' => array('className' => 'LocationsCrust',
							'foreignKey' => 'locations_crust_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'CombosPizza' => array('className' => 'CombosPizza',
							'foreignKey' => 'combos_pizza_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasAndBelongsToMany = array('Topping' => array('with'=>'OrdersCombosPizzasTopping'));
	
	/*
	function getToppings($id) {
		return $this->CombosPizzasTopping->find('all', array('conditions' => array('CombosPizzasTopping.combos_pizza_id' => $id),
											  			     'recursive'  => 0, 
														  	 'fields'	  => array('CombosPizzasTopping.id', 'CombosPizzasTopping.topping_id', 
														  	 					   'CombosPizzasTopping.placement', 'Topping.title')));
	}
	*/
}
?>