<?php
class OrdersSpecialsSizesTopping extends AppModel {

	var $name = 'OrdersSpecialsSizesTopping';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'OrdersSpecialsSize' => array('className' => 'OrdersSpecialsSize',
								'foreignKey' => 'orders_specials_size_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			),
			'Topping' => array('className' => 'Topping',
								'foreignKey' => 'topping_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	function specialsTopping($orders_specials_size_id) {
		$fields = array('OrdersSpecialsSizesTopping.id', 'OrdersSpecialsSizesTopping.price', 'OrdersSpecialsSizesTopping.placement',
						'Topping.id', 'Topping.title');
		
		return $this->find('all', array('conditions' => array('OrdersSpecialsSizesTopping.orders_specials_size_id' => $orders_specials_size_id),
										'recursive'  => 1, 
										'fields'     => $fields));
	}
}
?>