<?php
class OrdersCombosItem extends AppModel {

	var $name = 'OrdersCombosItem';
	
	var $belongsTo = array(
		'OrdersCombo' => array('className' => 'OrdersCombo',
							'foreignKey' => 'orders_combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'Item' => array('className' => 'Item',
							'foreignKey' => 'item_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
	
	var $hasAndBelongsToMany = array(
			'ItemsOption' => array('className' => 'ItemsOption',
						'joinTable' => 'orders_combos_items_items_options',
						'foreignKey' => 'orders_combos_item_id',
						'associationForeignKey' => 'items_option_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			),
			'LocationsCatsOption' => array('className' => 'LocationsCatsOption',
						'joinTable' => 'orders_combos_items_locations_cats_options',
						'foreignKey' => 'orders_combos_item_id',
						'associationForeignKey' => 'locations_cats_option_id',
						'unique' => false,
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'limit' => '',
						'offset' => '',
						'finderQuery' => '',
						'deleteQuery' => '',
						'insertQuery' => ''
			)
	);
}
?>