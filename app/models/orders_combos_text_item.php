<?php
class OrdersCombosTextItem extends AppModel {

	var $name = 'OrdersCombosTextItem';
	
	var $belongsTo = array(
		'OrdersCombo' => array('className' => 'OrdersCombo',
							'foreignKey' => 'orders_combo_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		),
		'CombosTextItem' => array('className' => 'CombosTextItem',
							'foreignKey' => 'combos_text_item_id',
							'conditions' => '',
							'fields' => '',
							'order' => ''
		)
	);
}
?>
