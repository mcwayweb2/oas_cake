<?php
class CombosTextItem extends AppModel {

	var $name = 'CombosTextItem';
	
	var $belongsTo = array(
			'Combo' => array('className' => 'Combo',
								'foreignKey' => 'combo_id',
								'conditions' => '',
								'fields' => '',
								'order' => ''
			)
	);
	
	var $hasMany = array(
			'OrdersCombosTextItem' => array('className' => 'OrdersCombosTextItem',
								'foreignKey' => 'combos_text_item_id',
								'dependent' => true,
								'conditions' => '',
								'fields' => '',
								'order' => '',
								'limit' => '',
								'offset' => '',
								'exclusive' => '',
								'finderQuery' => '',
								'counterQuery' => ''
			)
	);
}
?>