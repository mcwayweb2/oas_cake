<?php
/* SVN FILE: $Id: routes.php 7945 2008-12-19 02:16:01Z gwoo $ */
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7945 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2008-12-18 18:16:01 -0800 (Thu, 18 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/restaurants/browse-menus', array('controller'=> 'locations', 'action'	=> 'index'));
	Router::connect('/restaurants/browse-menus/:search', 
					array(
						'controller'=> 'locations', 
						'action' 	=> 'index'
					),
					array('pass' => array('search')));
	
	Router::connect('/restaurants/:action/*', array('controller' => 'locations', 'action' => 'index'));
	Router::connect('/restaurant/:locslug', 
					array('controller' => 'locations', 'action' => 'view'),
					array('pass' => array('locslug')));
					
	Router::connect('/menu/:locslug', 
					array('controller' => 'locations', 'action' => 'menu'),
					array('pass' => array('locslug')));

	
	
	
	
	
	
	
	Router::connect('/orders', array('controller' => 'advertiser', 'action' => 'dashboard'));
	Router::connect('/order-tracker', array('controller' => 'orders', 'action' => 'success'));
					
	Router::connect('/user-dash/orders', array('controller' => 'users', 'action' => 'dashboard'));
	Router::connect('/user-dash/profile', array('controller' => 'users', 'action' => 'edit'));
	Router::connect('/user-dash/addresses', array('controller' => 'users_addrs', 'action' => 'manage_addresses'));
	Router::connect('/user-dash/payment_profiles', array('controller' => 'users_payment_profiles', 'action' => 'manage'));
	
	Router::connect('/registration', array('controller' => 'advertisers', 'action' => 'register'));
	Router::connect('/signup', array('controller' => 'users', 'action' => 'register'));
	
	Router::connect('/contact-us', array('controller' => 'contacts', 'action' => 'contactus'));
	Router::connect('/request-a-restaurant', array('controller' => 'prospects', 'action' => 'add'));
	
	Router::connect('/advertisers/payment_profiles', array('controller' => 'advertisers_payment_profiles', 'action' => 'manage'));

	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/learn-more', array('controller' => 'pages', 'action' => 'display', 'learn_more'));
	Router::connect('/about-us', array('controller' => 'pages', 'action' => 'display', 'about_us'));
?>