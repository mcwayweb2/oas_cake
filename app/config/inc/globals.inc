<?php 
//OrderaSlice Global Config Variables

define('OAS_PER_ORDER_FEE', 1.99);
define('PREMIUM_ACCT_MONTHLY_FEE', 19.99);

define('OAS_DOMAIN', 'http://orderaslice');
define('OAS_DMN', 'http://orderaslice');

//set up for a 6 month free trial
define('FREE_TRIAL_CODE', 'GIMME180FREE');

define('DEFAULT_LATITUDE', 32.7952);
define('DEFAULT_LONGITUDE', -116.9671);