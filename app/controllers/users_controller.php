<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html', 'Form', 'GoogleMap');
	var $components = array('AuthorizeNet', 'Email', 'FileUpload', 'MaxMind');
	
	function ajax_check_username() {
		Configure::write('debug', 0);
		
		$username = $this->params['form']['username'];
		$user = $this->User->findByUsername($username);
		$color = 'red';
		
		//check against regular expression
		if(strlen($username) < 5 || strlen($username) > 20) {
			$msg = 'Username must be 5-20 characters in length.';
		} else if(!ctype_alnum($username)) {
			$msg = 'Invalid. Username must be alphanumeric.';
		} else if($user) {
			$msg = 'This username is already taken. Please choose another one.';
		} else {
			$msg = 'This username is available';
			$color = 'green';
		}
		
		$this->set('msg', $msg);
		$this->set('color', $color);
	}
	
	function order_history($id = null) {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		if(!$id) {
			$this->Session->setFlash(__('Invalid User.', true));
			$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
		}
		$this->User->recursive = -1;
		$this->set('user', $this->User->read(array('User.fname', 'User.lname'), $id));
		
		//get all location ids for
		$locations = $this->requestAction('/locations/getAdvertiserLocations/list');
		$loc_ids = array();
		foreach($locations as $loop_id => $title):
			$loc_ids[] = $loop_id;
		endforeach;
		$this->set('orders', $this->User->Order->find('all', array('conditions' => array('Order.user_id' => $id,
																						 'Order.location_id' => $loc_ids),
																   'contain'    => array('Location'),
																   'order'      => array('Order.timestamp' => 'DESC'),
																   'fields'     => array('Order.id', 'Location.name', 'Order.timestamp'))));
	}
	
	function setUserDashVars() {
		$default = $this->User->UsersAddr->getDefault($this->Session->read('User.id'));
		if($default) { $this->set('default_addr',  $default); }
		
		$this->set('pay_profile_check', $this->User->UsersPaymentProfile->haveProfileCheck($this->Session->read('User.id')));
	}
	
	function dashboard() {
		$this->checkUserSession();
		$this->layout = "user";
		
		//does user have a payment method set?
		$this->paginate = array('Order' => array('limit' 		=> 10,
												 'recursive'	=> 2,
												 'contain'		=> array('Location.id', 'Location.advertiser_id', 'Location.name', 'Location.slug', 'Location.Advertiser.slug'),
												 'fields'		=> array('Order.timestamp', 'Order.order_started', 'Order.order_started_timestamp', 'Location.use_logo_in_banner',  
												 				  		 'Order.refunded', 'Order.refunded_timestamp', 'Order.id'),
											     'order' => array('timestamp' => 'desc')));
		$orders = $this->paginate('Order', array('Order.user_id' => $this->Session->read('User.id'),
												 'Order.complete'=>'1'));
		foreach($orders as $index => $o):
			$orders[$index]['Location']['logo'] = $this->User->Order->Location->getLocationLogo($o['Location']['id']);
			$ad = $this->User->Order->Location->Advertiser->read('slug', $o['Location']['advertiser_id']);
			$orders[$index]['Advertiser']['slug'] = $ad['Advertiser']['slug'];
		endforeach;
		$this->set('orders', $orders);
		
		$this->setUserDashVars();
	}
	
	function getMyFavs() {
		$favs = $this->User->Favorite->find('all', array('conditions'=>array('Favorite.user_id'=>$this->Session->read('User.id')),
														'recursive'=>0)); 
		//loop through the favs and add add an order total for each location
		$count = 0;
		foreach($favs as $fav):
			$conditions = array('Order.user_id'=>$this->Session->read('User.id'),
								'Order.location_id'=>$fav['Favorite']['location_id'],
								'Order.complete' =>'1');
								
			$favs[$count]['num_orders'] = $this->User->UsersAddr->Order->find('count', array('conditions' => $conditions));
			$advertiser = $this->User->UsersAddr->Order->Location->Advertiser->read('slug', $fav['Location']['advertiser_id']);
			$favs[$count]['Location']['logo'] = $this->User->Order->Location->getLocationLogo($fav['Location']['id']);
			$favs[$count]['Advertiser']['slug'] = $advertiser['Advertiser']['slug'];
			
			$o = $this->User->UsersAddr->Order->find('first', array('conditions' => $conditions,
														 'fields'     => 'timestamp',
														 'order'      => array('timestamp' => 'DESC')));
			$favs[$count]['last_ordered'] = $o['Order']['timestamp'];
			$favs[$count++]['currently_open'] = ($this->User->Favorite->Location->isLocationOpen($fav['Favorite']['location_id'])) ? "open" : "closed";
		endforeach;
		return $favs;
	}
	
	function add_favorite() {
		Configure::write('debug', 0);
		if($this->RequestHandler->isAjax()) {
			//determine if there is already a record for this
			$user = $this->User->Favorite->find('all', array('conditions'=>array('Favorite.location_id'=>$this->params['form']['id'],
																				 'Favorite.user_id'=>$this->Session->read('User.id'))));
			if(sizeof($user) > 0) {
				//already a record with this criteria
				$this->set('text', 'Already Saved!');
			} else {
				$this->User->Favorite->create();
				$this->data['Favorite']['location_id'] = $this->params['form']['id'];
				$this->data['Favorite']['user_id'] = $this->Session->read('User.id');
				if($this->User->Favorite->save($this->data)) {
					$this->set('text', 'Favorite added!');
				} else {
					$this->set('text', 'Error adding fav!');
				}	
			}	
		}
	}
	
	function remove_favorite() {
		$this->checkUserSession();
		Configure::write('debug', 0);
		if($this->RequestHandler->isAjax()) {
			if($this->User->Favorite->deleteAll(array('Favorite.user_id'=>$this->Session->read('User.id'),
												      'Favorite.location_id'=>$this->params['form']['id']))) {
				$this->set('text', 'Favorite successfully removed!');								   	
			} else { $this->set('text', 'Error removing favorite!'); }	
		}
	}
	
	
	function login() {
        if (!empty($this->data)) {
            $someone = $this->User->findByUsername($this->data['User']['loginUsername']);

            if(!empty($someone['User']['password']) && $someone['User']['password'] == md5($this->data['User']['loginPassword'])) {	 
            	$this->Session->write('show_warning', '1');
            	$this->Session->setFlash(__('Welcome back '.$someone['User']['fname'].'.', true));
                $this->Session->write('User', $someone['User']);
                
                $this->redirect((strpos($this->referer(), '/menu') !== false) ? $this->referer() : 'dashboard');
            } else {
            	$this->Session->setFlash(__('Invalid Username/Password. Please try again.', true));
            }
        }
        
        //featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->User->Favorite->Location->getFeatured($lat, $long));
    }

    function logout() {
        $this->Session->delete('User');
        $this->Session->write('show_warning', '1');
        $this->Session->setFlash(__('You have successfully logged out.', true));
        $this->redirect('/');
    }
    
    function updatepw() {
    	$this->checkUserSession();
    	$this->User->set($this->data);
    	if(!$this->User->validates()) {
    		$this->Session->setFlash(__('Your new password needs to be 6-14 characters in length. Please try again.', true));
    	} else if($this->data['User']['pass1'] != $this->data['User']['pass2']) {
    		$this->Session->setFlash(__('Your new passwords do not match, please try again.', true));
    	} else {
    		$this->User->id = $this->Session->read('User.id');
    		if ($this->User->saveField('password', md5($this->data['User']['pass1']))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your account password has been updated successfully.', true));
				$this->redirect(array('action'=>'dashboard'));
			} else {
				$this->Session->setFlash(__('Problem updating account password. Please, try again.', true));
			}
    	}
    	$this->redirect($this->referer());
    }
    
    function ajax_geocode_problem() {
    
    }
	
	function password_reset() {
    	if(!empty($this->data)) {
    		$this->User->set($this->data);
    		if($this->User->validates()) {
	    		//verify email
	    		$someone = $this->User->find('first', array('conditions'=>array('User.username'=>$this->data['User']['username'],
	    																		'User.email'   =>$this->data['User']['email'])));
	    		if(!empty($someone['User']['username'])) {
	    			//create new password 
	    			$password = $this->Strings->randomString();	
	    			
	    			//update db with new password
	    			$this->User->id = $someone['User']['id'];
	    			$this->User->saveField('password', md5($password));
	    			
	    			//send email to user notifying new password
	    			if($this->email_password_reset($password, $someone)) {
	    				$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your account password has been reset successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem resetting your password.', true));
					}
	    		} else {
	    			$this->Session->setFlash(__('Sorry, there is no account registered with those credentials.', true));
	    		}
	    	} else {
	    		$this->Session->setFlash(__('Please fill out all the required form fields.', true));
	    	}
    	}
    	
    	//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->User->Favorite->Location->getFeatured($lat, $long));
    }
    
    function email_password_reset($password = null, $user = null) {
		if(!$password) { return false; }
		
		$this->Email->to = $this->data['User']['email'];
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->subject = 'OrderASlice.com Password Reset Email';
		$this->Email->sendAs = 'html';
		
		$name = $user['User']['fname']." ".$user['User']['lname'];
		$this->set('name',$name);
		$this->set('password',$password);
		
		$this->Email->template = 'pw_reset_user';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
    
	function recover_username() {
		if(!empty($this->data)) {
    		if($this->User->validates()) {
	    		//verify email
	    		$someone = $this->User->findByEmail($this->data['User']['email']);
	    		if(!empty($someone['User']['email'])) {
	    			//send email to User notifying new password
	    			if($this->email_recover_username($someone)) {
	    				$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Email successfully sent to: '.$someone['User']['email'].'.', true));
					} else {
						$this->Session->setFlash(__('There was a problem recovering your account. Please try again.', true));
					}
	    		} else {
	    			$this->Session->setFlash(__('Sorry, there is no registered account under this email address.', true));
	    		}
	    	} else {
	    		$this->Session->setFlash(__('There was an error validating the form.', true));
	    	}
    	}
    	
    	//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->User->Favorite->Location->getFeatured($lat, $long));
	}
	
	function email_recover_username($user = null) {
		if(!$user) { return false; }
		
		$this->Email->to = $user['User']['email'];
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->subject = 'OrderASlice.com Username Recovery Email';
		$this->Email->sendAs = 'html';
		
		$this->set('name',$user['User']['fname']." ".$user['User']['lname']);
		$this->set('username', $user['User']['username']);
		
		$this->Email->template = 'recover_username_user';
		return ($this->Email->send()) ? true : false;
	}
	
	function register() {
		if (!empty($this->data)) {
			$this->User->set($this->data);
			if($this->User->validates()) {
				$this->User->create();
				
				//make sure username is not already taken 
				$user = $this->User->findByUsername($this->data['User']['username']);
				$find = $this->User->findByEmail($this->data['User']['email']);
				if (!empty($user['User']['username'])) {
					$this->Session->setFlash(__('This username is taken, please choose a new one.', true));
				} else if(!empty($find['User']['email'])) {
					$this->Session->setFlash(__('There is already an account registered to this email. <a href="/users/login">Login</a> or <a href="/users/password_reset">Trouble Logging In?</a>.', true));
				} else if(empty($this->data['User']['pass1']) || empty($this->data['User']['pass2'])) {
					$this->Session->setFlash(__('Please fill out both password fields.', true));
					$this->set('highlight_fields', array('UserPass1', 'UserPass2'));
				} else if($this->data['User']['pass1'] != $this->data['User']['pass2']) {
					$this->Session->setFlash(__('Your new passwords must match!', true));
					$this->set('highlight_fields', array('UserPass1', 'UserPass2'));
				} else {
					$this->data['User']['password'] = md5($this->data['User']['pass1']);
					$this->data['User']['timestamp'] = date("Y-m-d H:i:s");
					
					if ($this->User->save($this->data)) {
						//create the auth.net profile id
						$response = $this->AuthorizeNet->cim_create_profile($this->User->getLastInsertId(), $this->data['User']['email'], 'User Profile');
						if($response['resultcode'] != 'Ok') {
							$this->Session->setFlash(__('There was an error creating payment profile because: '.$response['text'].'. Please try again.', true));
						} else {
							$this->User->id = $this->User->getLastInsertId();
							$this->User->saveField('profile_id', $response['profileId']);
							$usr = $this->User->read(null, $this->User->getLastInsertId());
							$this->Session->write('User', $usr['User']);
							$this->email_welcome_user();
							$this->Session->write('show_warning', '1');
							$this->Session->setFlash(__('You\'re almost ready to Order A Slice. Please add a default delivery address with the link to the left.' , true));
							$this->redirect(array('action'=>'dashboard'));	
						}
					} else {
						$this->Session->setFlash(__('There was a problem signing up. Please, try again.', true));
					}	
				}	
			} else {
				$this->Session->setFlash(__('There was a problem with the form. Please, fill out all the required fields.', true));
			}
		}
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->User->Favorite->Location->getFeatured($lat, $long));
	} 
	
    function email_welcome_user() {		
		$this->Email->to = $this->data['User']['email'];
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->subject = 'Welcome to OrderaSlice.com!';
		$this->Email->sendAs = 'html';
		
		$this->set('name', $this->data['User']['fname']);
		$this->Email->template = 'registration_success_user';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
	
	function sendGeocodeErrorEmail($id) {
		$user = $this->User->read(null, $id);
		$this->Email->to = 'robert@mcwaywebdevelopment.com, admin@orderaslice.com';
		$this->Email->from = 'admin@orderaslice.com';
		$this->Email->subject = 'OrderASlice.com - USER ADDRESS GEOCODE ERROR';
		$this->Email->sendAs = 'html';
		
		$this->set('user',$user);
		
		$this->Email->template = 'geocoded_error_user';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}

	function edit() {
		$this->checkUserSession();
		$this->layout = "user";
		if (!empty($this->data)) {			
			if ($this->User->save($this->data)) {
				$this->Session->write('User.fname', $this->data['User']['fname']);
				$this->Session->write('User.lname', $this->data['User']['lname']);
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your account has been updated successfully.', true));
				$this->redirect(array('action'=>'dashboard'));
			} else {
				$this->Session->setFlash(__('Your account could not be updated. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $this->Session->read('User.id'));
		}
		//even though we are only pulling one record we must use 'find all' to keep the array dimensions correct for google map data input
		$this->set('users',  $this->paginate('User', array('User.id'=>$this->Session->read('User.id'))));
		$default = $this->User->UsersAddr->getDefault($this->Session->read('User.id'));
		if($default) { $this->set('default_addr',  $default); } 
	}

	function admin_login() {
		$this->layout = 'admin';
		if (!empty($this->data)) {
            $someone = $this->User->findByUsername($this->data['User']['username']);

            if(!empty($someone['User']['password']) && $someone['User']['password'] == md5($this->data['User']['password']) && $someone['User']['admin'] == '1') {	 
            	$this->Session->write('show_warning', '1');
            	$this->Session->setFlash(__('Welcome '.$someone['User']['fname'].'.', true));
                $this->Session->write('Admin', $someone['User']);
                $this->redirect('/admin/advertisers/dashboard');
            } else {
            	$this->Session->setFlash(__('Invalid Login Credentials. Please try again.', true));
            }
        }
	}
	
	function admin_logout() {
		$this->Session->delete('Admin');
        $this->Session->write('show_warning', '1');
        $this->Session->setFlash(__('Admin successfully logged out.', true));
        $this->redirect('/admin/users/login');
	}


	function admin_edit() {
		$this->checkAdminSession();
		if (!empty($this->data)) {
			$this->data['User']['id'] = $this->Session->read('Admin.id');
			if ($this->User->save($this->data)) {
				$this->Session->write('Admin.fname', $this->data['User']['fname']);
				$this->Session->write('Admin.lname', $this->data['User']['lname']);
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your admin profile has been updated successfully.', true));
				$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
			} else {
				$this->Session->setFlash(__('Your profile could not be updated. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $this->Session->read('Admin.id'));
		} 
	}
	
 	function admin_updatepw() {
    	$this->checkAdminSession();
    	$this->User->set($this->data);
    	if(!$this->User->validates()) {
    		$this->Session->setFlash(__('Please fill out both password fields and try again.', true));
    	} else if($this->data['User']['pass1'] != $this->data['User']['pass2']) {
    		$this->Session->setFlash(__('Your new passwords do not match, please try again.', true));
    	} else {
    		$this->User->id = $this->Session->read('Admin.id');
    		if ($this->User->saveField('password', md5($this->data['User']['pass1']))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your account password has been updated successfully.', true));
				$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
			} else {
				$this->Session->setFlash(__('Problem updating account password. Please, try again.', true));
			}
    	}
    	$this->redirect($this->referer());
    }
}
?>