<?php
class UsersPaymentProfilesController extends AppController {

	var $name = 'UsersPaymentProfiles';
	var $helpers = array('Html', 'Form');
	var $components = array('AuthorizeNet', 'Email', 'Security');
	
	function beforeFilter() {
		$this->Security->blackHoleCallback = 'forceSSL';
        $this->Security->requireSecure('manage');
	}
	
	function manage() {
		//Configure::write('debug', '1');
		$this->checkUserSession();
		$this->layout = 'user';
		
		if (!empty($this->data)) {
			//make sure user has a valid auth.net profile id, if not give them one.
			$profile_id = $this->Session->read('User.profile_id');
			if($profile_id == '0' || empty($profile_id)) {
				//create the auth.net profile id
				$response = $this->AuthorizeNet->cim_create_profile($this->Session->read('User.id'), $this->Session->read('User.email'), 'User Profile');
				if($response['resultcode'] != 'Ok') {
					$this->Session->setFlash(__('There was an error creating payment profile because: '.$response['text'].'. Please try again.', true));
				} else {
					$this->UsersPaymentProfile->User->id = $this->Session->read('User.id');
					$this->UsersPaymentProfile->User->saveField('profile_id', $response['profileId']);
					
					//save the new profile id to the users session
					$this->Session->write('User.profile_id', $response['profileId']);
				}
			}
			
			$this->UsersPaymentProfile->set($this->data);
			if($this->UsersPaymentProfile->validates()) {
				$this->UsersPaymentProfile->create();
				$this->data['UsersPaymentProfile']['timestamp'] = date("Y-m-d H:i:s");
				$this->data['UsersPaymentProfile']['card_suffix'] = 'xxxx'.substr($this->data['UsersPaymentProfile']['cc_num'], 12);
				if($this->UsersPaymentProfile->save($this->data)) {
					$user_info = $this->UsersPaymentProfile->User->read('profile_id', $this->Session->read('User.id'));
					$exp = $this->data['UsersPaymentProfile']['year']['year']."-".$this->data['UsersPaymentProfile']['month']['month'];
					
					$billinginfo = array("fname" => $this->data['UsersPaymentProfile']['fname'],
										 "lname" => $this->data['UsersPaymentProfile']['lname'],
										 "address" => $this->data['UsersPaymentProfile']['address'],
										 "city" => $this->data['UsersPaymentProfile']['city'],
										 "state" => $this->data['UsersPaymentProfile']['state'],
										 "zip" => $this->data['UsersPaymentProfile']['zip'],
										 "cc"   => $this->data['UsersPaymentProfile']['cc_num'],
										 "exp"  => $exp);
					$response = $this->AuthorizeNet->cim_create_payment_profile($user_info['User']['profile_id'], $billinginfo);
					if($response['resultcode'] == 'Ok') {
						$this->UsersPaymentProfile->saveField('payment_profile_id', $response['paymentProfileId']);
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your payment profile has been added successfully.', true));
					} else {
						$this->set('resp', $response);
						$this->Session->setFlash(__('Your credit card cannot be authorized.:'.$response['text'], true));
						//$this->Session->setFlash(__('Your credit card cannot be authorized. Please try again.', true));
						$this->UsersPaymentProfile->delete($this->UsersPaymentProfile->getLastInsertId());
					}
				} else {
					$this->Session->setFlash(__('There was a problem creating your payment profile.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required form fields.', true));
			}
		}
		$this->set('profiles', $this->UsersPaymentProfile->find('all', array('conditions' => array('UsersPaymentProfile.user_id' => $this->Session->read('User.id')),
																			 'recursive'  => -1)));
		$this->set('states', $this->UsersPaymentProfile->User->UsersAddr->State->find('list', array('fields' => array('abbr', 'name'))));
		$default = $this->UsersPaymentProfile->User->UsersAddr->getDefault($this->Session->read('User.id'));
		if($default) { $this->set('default_addr',  $default); }	
		$this->set('pay_profile_check', $this->UsersPaymentProfile->haveProfileCheck($this->Session->read('User.id')));																										   
	}
	
	function remove($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid payment profile. Please try again.', true));
			$this->redirect($this->referer());
		}
		$this->checkUserSession(array('pay_profile_id' => $id));
		
		$profile = $this->UsersPaymentProfile->read('payment_profile_id', $id);
		
		//remove payment profile from authorize.net CIM
		$response = $this->AuthorizeNet->cim_delete_payment_profile($this->Session->read('User.profile_id'), $profile['UsersPaymentProfile']['payment_profile_id']);
		if($response['resultcode'] == 'Ok') {
			//now remove local db record
			$this->UsersPaymentProfile->delete($id);
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your payment profile has been removed successfully.', true));				
		} else {
			$this->Session->setFlash(__('There was a problem deleting your payment profile. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function profileCount() {
		return sizeof($this->UsersPaymentProfile->find('all', array('conditions'=>array('UsersPaymentProfile.advertiser_id'=>$this->Session->read('Advertiser.id')))));
	}
	
	function verifyIdentity($id) {
		$check = $this->UsersPaymentProfile->read('user_id', $id);
		return ($check['UsersPaymentProfile']['user_id'] == $this->Session->read('User.id')) ? true : false; 
	}
}
?>