<?php
class OrdersController extends AppController {
	
	var $name = 'Orders';
	var $helpers = array('Html', 'Form', 'GoogleMap');
	var $components = array('Clickatell', 'Strings', 'Email', 'AuthorizeNet', 'Interfax');
	
	//verify that the passed order id belongs to logged in restaurant
	function verifyAdvertiserIdentity($id) {
		$o = $this->Order->read('location_id', $id);
		$loc = $this->Order->Location->read('advertiser_id', $o['Order']['location_id']);
		return ($loc['Location']['advertiser_id'] == $this->Session->read('Advertiser.id')) ? true : false;
	}
	
	//verify that the passed order id belongs to logged in user
	function verifyUserIdentity($id) {
		$o = $this->Order->read('user_id', $id);
		return ($o['Order']['user_id'] == $this->Session->read('User.id')) ? true : false;
	}
	
	//starts the order session
	function start($loc_id) {
		$this->checkUserSession();
		
		//make sure this location is active before we start the session
		if(!$this->Order->Location->isLocationOpen($loc_id)) {
			 $this->Session->setFlash(__('This restaurant location is currently not accepting orders.', true));
			$this->redirect(array('action'=>'index', 'controller'=>'locations'));
		} 
		
		//get the customer order fee
		$loc = $this->Order->Location->read(array('cust_per_order_fee'), $loc_id);
		
		$this->Order->create();
		if($this->Order->save(array('Order' => array('user_id'     => $this->Session->read('User.id'),
													 'location_id' => $loc_id,
													 'oas_charge'  => $loc['Location']['cust_per_order_fee'],
													 'timestamp'   => date("Y-m-d H:i:s", $this->getLocalTimeOffset()))))) {
			$this->Session->write('User.order_id', $this->Order->getLastInsertId());
			$this->Session->write('User.location_id', $loc_id);
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Go ahead and start ordering...', true));
		} else {
			$this->Session->setFlash(__('There was a problem starting your order. Please try again.', true));
		}
		
		//redirect back to whatever menu they were on
		$this->redirect($this->referer());
	}

	
	//kills the current order
	function kill() {
		$this->checkUserSession();
		if($this->Session->check('User.order_id')) {
			$this->Order->killOrder($this->Session->read('User.order_id'));
			$this->Session->delete('User.order_id');
			$this->Session->delete('User.location_id');
		}
		$this->redirect($this->referer());
	}
	
	function finalize() {
		$this->checkUserSession();
		$this->layout = "user";
		
		if(!$this->Session->check('User.order_id')) {
			$this->Session->setFlash(__('Your order session has expired, due to inactivity.', true));
			$this->redirect(array('action'=>'index', 'controller'=>'locations'));
		}
		
		if(!empty($this->data)) {
			$success = true;
			
			//make sure user has a valid auth.net profile id, if not give them one.
			$profile_id = $this->Session->read('User.profile_id');
			if($profile_id == '0' || empty($profile_id)) {
				//create the auth.net profile id
				$response = $this->AuthorizeNet->cim_create_profile($this->Session->read('User.id'), $this->Session->read('User.email'), 'User Profile');
				if($response['resultcode'] != 'Ok') {
					$this->Session->setFlash(__('There was an error processing your order because: '.$response['text'].'. Please try again.', true));
					$success = false;
				} else {
					$this->UsersPaymentProfile->User->id = $this->Session->read('User.id');
					$this->UsersPaymentProfile->User->saveField('profile_id', $response['profileId']);
					//save the new profile id to the users session
					$this->Session->write('User.profile_id', $response['profileId']);
					$profile_id = $response['profileId'];
				}
			}
			
			//prepare fields for order record
			$order = $this->Order->read(array('Order.subtotal', 'Location.zip_code_id', 'Location.accept_deliveries', 'Location.delivery_charge', 
											  'Order.discount_amt', 'Order.oas_charge', 'Location.notify_fax', 'Location.notify_sms', 'Location.name', 
											  'Location.fax', 'Location.acct_type', 'Location.confirm_method', 'Location.loc_per_order_fee'), 
										$this->Session->read('User.order_id'));
										
			$zip = $this->Order->Location->ZipCode->read('tax_rate', $order['Location']['zip_code_id']);
		
			$tax_amt = ($zip['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal'];
			$delivery_charge = ($this->data['Order']['order_method'] == 'Pickup') ? 0 : $order['Location']['delivery_charge'];
			$rec_total = $order['Order']['subtotal'] + $tax_amt + $order['Order']['oas_charge'] + $delivery_charge - $order['Order']['discount_amt'];
			
			//calculate the fee for the restaurant
			$restaurant_fee = ($order['Order']['subtotal'] + $tax_amt - $order['Order']['discount_amt']) * ($order['Location']['loc_per_order_fee'] / 100);
		
			//total is subtotal, plus tax, plus OAS Fee, plus delivery fee, minus any discount amount!
			$save = array('Order' => array('order_method'	 		=> $this->data['Order']['order_method'],
										   'tax'		     		=> $tax_amt,
										   'tax_rate'        		=> $zip['ZipCode']['tax_rate'],
										   'delivery_charge' 		=> $delivery_charge,
										   'oas_restaurant_charge' 	=> $restaurant_fee,
										   'complete'		 		=> '1',										   
										   'total'           		=> $rec_total));
			
			if($this->data['Order']['order_method'] == 'Delivery') {
				$save['Order']['users_addr_id'] = $this->data['Order']['users_addr_id'];
			}
			
			//is this NOT a CIM transaction?
			if($this->data['Order']['users_payment_profile_id'] == 'new' && $this->data['Order']['save_profile'] == '0') {
				$save['Order']['is_cim_transaction'] = 0;
				$save['Order']['masked_cc'] = 'XXXX'.substr($this->data['Order']['cc_num'], 12);
			}
		
			//only try and process payment if we have a valid profile id 
			if($success) {
				//validate the payment method
				if($this->data['Order']['users_payment_profile_id'] == 'new') {
					//we are paying with a new card that has no payment profile yet
					if($this->data['Order']['save_profile'] == '1') {
						//we are saving this card to a payment profile for future use
						$exp = $this->data['Order']['year']['year']."-".$this->data['Order']['month']['month'];
						$name = (empty($this->data['Order']['name'])) ? $this->data['Order']['fname']." ".$this->data['Order']['lname'] : $this->data['Order']['name'];
						$response = $this->AuthorizeNet->cim_create_payment_profile($profile_id, array("fname" => $this->data['Order']['fname'],
																									   "lname" => $this->data['Order']['lname'],
																									   "address" => $this->data['Order']['address'],
																									   "city" => $this->data['Order']['city'],
																									   "state" => $this->data['Order']['state'],
																									   "zip" => $this->data['Order']['zip'],
																									   "cc"   => $this->data['Order']['cc_num'],
																									   "exp"  => $exp));
						if($response['resultcode'] == 'Ok') {
							//payment profile successfully created... save a db record for it and charge for order
							$this->Order->UsersPaymentProfile->create();
							$save_profile = array('UsersPaymentProfile' => array('user_id'    => $this->Session->read('User.id'),
																		 'payment_profile_id' => $response['paymentProfileId'],
																		 'name' 			  => $name,
																		 'fname'              => $this->data['Order']['fname'],
																		 'lname' 			  => $this->data['Order']['lname'],
																		 'address' 			  => $this->data['Order']['address'],
																	   	 'city' 			  => $this->data['Order']['city'],
																	     'state' 			  => $this->data['Order']['state'],
																	     'zip' 				  => $this->data['Order']['zip'],
																		 'card_suffix' 		  => 'XXXX'.substr($this->data['Order']['cc_num'], 12),
																		 'timestamp' 		  => date("Y-m-d H:i:s", $this->getLocalTimeOffset()),
																		 'default' 			  => '1'));
							if($this->Order->UsersPaymentProfile->save($save_profile)) {
								//since we are making this new pay profile the new default, we need to change the old default
								$this->Order->updateAll(array('UsersPaymentProfile.default' => '0'),
														array('NOT'   => array('UsersPaymentProfile.id' => $this->Order->UsersPaymentProfile->getLastInsertId()),
															  'UsersPaymentProfile.default' => '1'));
														
								$resp = $this->AuthorizeNet->cim_create_transaction($profile_id, $response['paymentProfileId'], $save['Order']['total'],
																					$recurring = false,
																					array('invoiceNumber' => 'Order: #'.$this->Session->read('User.order_id'),
																						  'description'   => 'Restaurant Order: '.$order['Location']['name']));
								
								if($resp['resultcode'] != 'Ok') {
									$success = false; //card was declined
									$this->Session->setFlash(__('There was a problem with your payment: '.$resp['text'].'. Please try again.', true));
								} else {
									//record the transaction
									$this->Order->UsersPaymentProfile->saveTransaction($this->Session->read('User.id'), $response['paymentProfileId'],
																							 $resp['transactionId'], $rec_total, $this->Session->read('User.order_id'), 
																							 'CIM Payment - Order #: '.$this->Session->read('User.order_id'));
									//record the transaction ID for the completed order
									$this->Order->id = $this->Session->read('User.order_id');
									$this->Order->saveField('users_payment_profile_id', $this->Order->UsersPaymentProfile->getLastInsertId());
									$this->Order->saveField('transaction_id', $resp['transactionId']);
								}
							} else {
								$success = false; 
								$this->Session->setFlash(__('There was a problem creating your payment profile. Please try again.', true));
							}
						} else {
							$success = false;
							$this->Session->setFlash(__('There was a problem creating your payment profile: '.$response['text'].'. Please try again.', true));
						}
					} else {
						//we are only charging the card this one time
						$resp = $this->AuthorizeNet->chargeCard($this->data['Order']['cc_num'], $this->data['Order']['year']['year'], $this->data['Order']['month']['month'], 
																null, $save['Order']['total'], $tax_amt, '0', 
																'Restaurant Order: '.$order['Location']['name'], array('fname'=>$this->data['Order']['fname'], 'lname'=>$this->data['Order']['lname']),
																'Order: #'.$this->Session->read('User.order_id'));
						if($resp[1] != 1) {
							$success = false; //card was declined
							$this->Session->setFlash(__('There was a problem with your payment: '.$resp[4].'. Please try again.', true));
						} else {
							$this->Order->UsersPaymentProfile->saveTransaction($this->Session->read('User.id'), null,
																					 $resp[7], $rec_total, $this->Session->read('User.order_id'), 
																					 'One Time Payment - Order #: '.$this->Session->read('User.order_id'));
							// record the transaction ID for the completed order
							$this->Order->id = $this->Session->read('User.order_id');
							$this->Order->saveField('transaction_id', $resp[7]);
						}
					}
				} else {
					//paying with a card already saved
					$pay_profile = $this->Order->UsersPaymentProfile->read(array('payment_profile_id'), $this->data['Order']['users_payment_profile_id']);
					$response = $this->AuthorizeNet->cim_create_transaction($profile_id, $pay_profile['UsersPaymentProfile']['payment_profile_id'], $save['Order']['total'],
																			$recurring = false, 
																			array('invoiceNumber' => 'Order: #'.$this->Session->read('User.order_id'),
																				  'description'   => 'Restaurant Order: '.$order['Location']['name']));
					if($response['resultcode'] != 'Ok') {
						$success = false; //card was declined
						$this->Session->setFlash(__('There was a problem with your payment: '.$response['text'].'. Please try again.', true));
					} else {
						$this->Order->UsersPaymentProfile->saveTransaction($this->Session->read('User.id'), $this->data['Order']['users_payment_profile_id'],
																				 $response['transactionId'], $rec_total, $this->Session->read('User.order_id'), 
																				 'CIM Payment - Order #: '.$this->Session->read('User.order_id'));
						//record the transaction ID for the completed order
						$this->Order->id = $this->Session->read('User.order_id');
						$this->Order->saveField('users_payment_profile_id', $this->data['Order']['users_payment_profile_id']);
						$this->Order->saveField('transaction_id', $response['transactionId']);
					}
				}
			} //end payment processing conditionals
			
			//only continue, if the payment was successful			
			if($success) {
				$this->Order->id = $this->Session->read('User.order_id');

				$save['Order']['confirm_hash'] = $this->Order->getConfirmHash($order['Location']['confirm_method']);	
				
				
				if($this->Order->save($save)) {
					$total_success = true;
					//send email to advertiser about order
					//at least this test needs to succeed, or we assume the restaurant did not get the order
					if($this->sendOrderEmail($this->Session->read('User.order_id'))) {
						$this->Order->saveField('success_email', '1');
						$this->Session->write('show_warning', '1'); 
						//$this->Session->setFlash(__('Thank You. Your order has been received.', true));
					} else { 
						$total_success = false; 
						$this->Session->setFlash(__('There was a problem processing your order. Please try again.', true));
						$this->orderNotificationErrorEmail($this->Session->read('User.order_id'), $refund = true);
						$this->redirect($this->referer());
					} 
					
					
					//notify by fax if signed up for faxes
					if($order['Location']['notify_fax'] == '1' && $order['Location']['acct_type'] > 1 && $this->checkPhoneFormat($order['Location']['fax'])) {
						$resp = $this->Interfax->sendFax($this->build_fax_body($this->Session->read('User.order_id')), $order['Location']['fax']);
		
						if($resp->SendCharFaxResult > 0) {
							//successful fax
							$this->Order->saveField('success_fax', '1');
						} 
					}
					/*
					//notify by sms if signed up for sms
					if($order['Location']['notify_sms'] == '1') {
						
					}
					*/
					
					//there was a problem during the notification process. Send admin email to take care of issue
					if(!$total_success) {
						$this->orderNotificationErrorEmail($this->Session->read('User.order_id'));	
					}
				} else {
					//at this point if saving the order record fails we need to refund the purchase since it has already been charged.
					$this->orderNotificationErrorEmail($this->Session->read('User.order_id'), $refund = true);
					$this->Session->setFlash(__('There was a problem processing your order. Please try again.', true));
					$this->redirect($this->referer());
				}
				$this->redirect('/order-tracker');
			}
		} //end if(!empty($this->data) conditional
		$this->redirect($this->referer());
	}
	
	function orderNotificationErrorEmail($id) {
		$order = $this->Order->read(null, $id);
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->to = "admin@orderaslice.com";
		$this->Email->subject = 'OrderASlice.com -> ORDER ERROR NOTIFICATION';
		$this->Email->sendAs = 'html';
	}
	
	//this sends the email to the restaurant that contains the order info details
	function sendOrderEmail($id) {
		$order = $this->Order->orderInfo($id);
		
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->to = $order['Location']['contact_email'];
		$this->Email->replyTo = 'customer-service@orderaslice.com';
		$this->Email->subject = 'You have received a new online order';
		$this->Email->sendAs = 'html';

		$this->Email->template = 'order_success';
		$this->set('order', $order);
		
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
	
	//url alias: "/order-tracker"
	function success() {
		$this->checkUserSession();
		$this->layout = "user";
		
		//make sure there is a complete order in the session
		if($this->Session->check('User.order_id')) {
			//is the order complete?
			$order = $this->Order->orderInfo($this->Session->read('User.order_id'));	
			
			if($order['Order']['complete'] == '1') {
				$this->set('order', $order);

				$default = $this->Order->User->UsersAddr->getDefault($this->Session->read('User.id'));
				if($default) { $this->set('default_addr',  $default); }
				
				$this->set('pay_profile_check', $this->Order->User->UsersPaymentProfile->haveProfileCheck($this->Session->read('User.id')));
				
				//take the completed order out of the session
				$this->Session->delete('User.order_id');
			} else {
				$this->Session->setFlash(__('You are trying to reach a page that does not exist.', true));
				$this->redirect($this->referer());
			}
		} else {
			//no order in session
			$this->Session->setFlash(__('You are trying to reach a page that does not exist.', true));
			$this->redirect($this->referer());
		}
	}
	
	function credit_for_order($order) {
		//get restaurants primary card
		$l = $this->Order->Location->find('first', array('conditions' => array('Location.id' => $order['Order']['location_id']),
														 'fields'	  => array('Location.advertiser_id')));
		$ad_pay_profile = $this->Order->Location->Advertiser->AdvertisersPaymentProfile->getDefaultProfile($l['Location']['advertiser_id']);
		
		$amt_to_refund = $order['Order']['total'] - $order['Order']['oas_charge'] - $order['Order']['oas_restaurant_charge'];
		
		//issue a refund for order, MINUS the OAS customer fee, & MINUS the OAS Restaurant Fee
		$resp = $this->AuthorizeNet->cim_unlinked_refund($ad_pay_profile['Advertiser']['profile_id'],
														$ad_pay_profile['AdvertisersPaymentProfile']['payment_profile_id'], 
														$amt_to_refund, 
														'Credit: #'.$order['Order']['id']);
		if($resp['resultcode'] != 'Ok') {
			//there was a problem crediting the restaurant... notify admin
			$this->orderConfirmationErrorEmail($order['Order']['id']);
		} else {
			$this->Order->Location->Advertiser->AdvertisersPaymentProfile->saveTransaction($ad_pay_profile['AdvertisersPaymentProfile']['payment_profile_id'], 
																						   $resp['transactionId'], $amt_to_refund, 
																						   $order['Order']['location_id'], 
																						   'Order Credit (#'.$order['Order']['id'].')', 'Credit');
		}
	} 
	
	//url from email link to confirm that restaurant has received the order.
	function confirm($hash = null, $redirect_opt = null) {
		if(!$hash) {
			$this->Session->setFlash(__('This order does not exist.', true));
		} else {
			$this->Order->recursive = -1;
			$order = $this->Order->findByConfirmHash($hash);
			if($order) {
				//hash is a valid order, make sure order has not already been confirmed
				if($order['Order']['order_started'] == '1') {
					$this->Session->setFlash(__('This order has already been confirmed.', true));
				} else {
					//mark the order as confirmed and display order details for restaurant
					$save['Order'] = array('id'                      => $order['Order']['id'],
										   'order_started'           => '1',
										   'order_started_timestamp' => date('Y-m-d H:i:s', $this->getLocalTimeOffset()));
					if($this->Order->save($save)) {
						//order has been confirmed. Credit the restaurant with the funds for the order.
						$this->credit_for_order($order);
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your order has been successfully confirmed.', true));
						
						if($redirect_opt == '1') {
							$this->redirect($this->referer());
						} else {
							$this->set('order', $this->Order->orderInfo($order['Order']['id']));
							$this->set('location', $this->Order->Location->getFieldsForBanner($order['Order']['location_id'], array('Location.active', 'Location.menu_ready', 'Location.advertiser_id', 'Location.id')));
							$this->set('advertiser_login_token', true);
	
							$this->layout = 'restaurant';
						}
					} else {
						$this->Session->setFlash(__('There was a problem confirming your order. Please try again.', true));
					}
				}
			} else {
				$this->Session->setFlash(__('This order does not exist.', true));
			}	
		}
	}
	
	function orderConfirmationErrorEmail($id) {
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->to = "admin@orderaslice.com";
		$this->Email->subject = 'ORDER CONFIRMATION ERROR - Needs Manual Credit!';
		$this->Email->sendAs = 'html';

		$this->Email->template = 'order_confirmation_error';
		$this->set('order', $this->Order->orderInfo($id));
		
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
	
	// function called by 'IfByPhone' API when restaurant confirms order by calling in confirmation number
	function phone_confirmation() {
		$this->layout = null;
		$success = true;
		
		if(isset($this->params['form']['confirmId'])) {
			list($location_id, $order_confirm) = explode(".", $this->params['form']['confirmId']);
			
			//check to see if these are valid confirmation creds
			$order = $this->Order->find('first', array('conditions' => array('Order.location_id'  	=> $location_id,
																			 'Order.confirm_hash' 	=> $order_confirm,
																			 'Order.order_started' 	=> '0')));
			
			if($order) {
				//make sure order has not been refunded
				if($order['Order']['refunded'] == '1') {
					$text = "Do not prepare this order. This order has already been refunded.";
				} else {
					//valid confirmation
					$this->Order->id = $order['Order']['id'];
					$this->Order->saveField('order_started', '1');
					$this->Order->saveField('order_started_timestamp', $this->params['form']['timestamp']);
					$this->credit_for_order($order);
					$text = "Thank You ".$order['Location']['name'].". Your order has been confirmed.";	
				}
			} else { $text = "There was a problem confirming this order. Please try again."; }
		} else { $text = "There was a problem confirming this order. Please try again."; }
		
		$this->set("text", $text);
	}
	
	function send_sample_fax($phone = null) {
		//$this->checkAdminSession();
		$id = '2';
		if(!$phone) {
			$this->Session->setFlash(__('Invalid Phone Number.', true));
			$this->redirect($this->referer());
		}
		$html_body = $this->build_fax_body($id);
		$resp = $this->Interfax->sendFax($html_body, $phone);
		
		if($resp->SendCharFaxResult > 0) {
			//successful fax
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Test Fax Successfully sent to: '.$this->formatPhone($phone).'.', true));
		} else {
			$this->Session->setFlash(__('Problem Sending test fax to: '.$this->formatPhone($phone).'.', true));
		}
	}
	
	function build_fax_body($id) {
		$order = $this->Order->orderInfo($id);
		
		$html = '
			<html>
			<body>
			<p>You have a new online order placed from Orderaslice.com ...</p>
			<p><span style="font-weight: bold; padding-right: 5px; ">Order Placed:</span> '.$order['Order']['timestamp'].'</p>
			<hr />
			<?php style="font-weight: bold;">Order For: <span style="color: #BB080D; padding-left: 5px; font-size:120%;">'.$order['Order']['order_method'].'</span></p>';

		if($order['Order']['order_method'] == 'Delivery') { 
			$html .= '
				<p><div style="font-weight: bold; margin-bottom:5px; ">Deliver To:</div>
				<span style="font-size:120%;">
					<span style=" font-weight: bold; ">'.$order['User']['fname'].' '.$order['User']['lname'].'</span><br />
					"'.$order['UsersAddr']['name'].'"<br />'.
					$order['UsersAddr']['address'].'<br />'.
					$order['UsersAddr']['city'].', '.$order['UsersAddr']['zip'].'<br />'.
					$this->formatPhone($order['User']['phone']);
					
					if(!empty($order['UsersAddr']['special_instructions'])) {
						$html .= '
							<div class="ui-helper-clearfix">
								<div class="left pad-r5 b">Special Instructions:</div>
								<div class="left">'.$order['UsersAddr']['special_instructions'].'</div>
							</div>';
					}
			$html .= '
				</span>
				</p>
			';
		} else {
			$html .= '
				<p><span style="font-size:120%;">
					<span style=" font-weight: bold; ">'.$order['User']['fname'].' '.$order['User']['lname'].'</span><br />'.
					$this->formatPhone($order['User']['phone']).'
				</span></p>';
		}
	
		$html .= '
			<hr />
			<div class="start-main-div">
				<table style="width: 100%;">
				    <tr>
					    <th style="text-align: left; width:60px;">Qty</th>
					    <th style="text-align: left;"><span>Item</span></th>
				        <th style="width:100px; text-align: left;"><span>Price</span></th>
				    </tr>
					<tr><td colspan="3" style="padding: 0px;"></td></tr>';

	    if(sizeof($order['OrdersItem']) > 0) {
		    foreach($order['OrdersItem'] as $item): 
		    	$html .= '
		    		<tr>
			    		<td style="font-weight: bold; vertical-align: top;">('.$item['OrdersItem']['qty'].')</td>
			    		<td>
			    			<div style=" font-weight:bold; margin-bottom:5px;">'.$item['Item']['title'].'</div>
			    			<div style="">'.$item['Item']['description'].'</div>';

		    				if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
		    					$html .= '<ul style="text-decoration:none;  margin-top: 5px; color: #BB080D; font-weight: bold;" >'; 
		    					foreach($item['ItemsOption'] as $opt):
									$html .=  '<li>'.$opt['label'];
									if($opt['OrdersItemsItemsOption']['price'] > 0) {
										$html .=  '<span class=""> (+$'.number_format($opt['OrdersItemsItemsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					
		    					foreach($item['LocationsCatsOption'] as $opt):
									$html .= '<li>'.$opt['label'];
									if($opt['OrdersItemsLocationsCatsOption']['price'] > 0) {
										$html .= '<span style="padding-left: 5px;"> (+$'.number_format($opt['OrdersItemsLocationsCatsOption']['price'], 2).')</span>';	
									}	
		    					endforeach;
		    					$html .= '</ul>'; 
		    				}
		    				
		    				if(!empty($item['OrdersItem']['comments'])) {
		    					$html .= '<div style="margin-top: 5px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;">'.$item['OrdersItem']['comments'].'</span></div>'; 
		    				}
			    $html .= '
			    		</td>
			    		<td style="font-weight: bold; vertical-align: top;">$'.number_format($item['OrdersItem']['total'], 2).'</td>
			    	</tr>';
		    endforeach;
	    }
	    
		if(sizeof($order['Pizza']) > 0) {
		    foreach($order['Pizza'] as $pizza):
		    	$html .= '
		    		<tr>
			    		<td style="font-weight: bold; vertical-align: top;">'.$pizza['qty'].')</td>
			    		<td>
			    			<div style=" font-weight:bold;">'.
			    				$pizza['Size']['size'].'" '.$pizza['Size']['title'].' Pizza
			    			</div>';
			    			
		    				$lines = array();
							if($pizza['Crust']['LocationsCrust']['price'] > 0) {
		    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
		    				} else {
		    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
		    				}
		    				
		    				if($pizza['sauce'] == 'Extra Sauce') {
		    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
		    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
		    				
							if($pizza['cheese'] == 'Extra Cheese') {
		    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
		    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
		    				
		    				if(sizeof($pizza['Topping']) > 0) {
								foreach($pizza['Topping'] as $topping):
									$l = 'Add '.$topping['Topping']['title'];
									if($topping['PizzasTopping']['placement'] != 'Whole') {
										$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
									}
									
									if($topping['PizzasTopping']['price'] > 0) {
										$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
									}
									
									$lines[] = $l;
								endforeach;
		    				}
		    				
		    				if(sizeof($lines) > 0) {
		    					$html .= "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
								foreach($lines as $line):
									$html .= "<li>".$line."</li>";
								endforeach;
		    					$html .= "</ul></div>";
		    				}
			    				 
							if(!empty($pizza['comments'])) {
			    				$html .= '<div style="margin-top: 5px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;">'.$pizza['comments'].'</span></div>'; 
			    			}
				$html .= '
			    		</td>
			    		<td style="font-weight: bold; vertical-align: top;">'.number_format($pizza['total'], 2).'</td>
			    	</tr>';	
		    endforeach;
	    }
	    
	    if(sizeof($order['OrdersSpecialsSize']) > 0) {
		    foreach($order['OrdersSpecialsSize'] as $pizza):
		    	$html .= '
	    		    	<tr>
				    		<td style="font-weight: bold; vertical-align: top;">('.$pizza['qty'].')</td>
				    		<td>
				    			<div style=" font-weight:bold;">'.
				    				$pizza['Size']['size'].'" '.$pizza['Size']['title'].' '.$pizza['Special']['title'].' Specialty Pizza'.
				    			'</div>';
				    			
			    				$lines = array();
			    				if($pizza['sauce'] == 'Extra Sauce') {
			    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['Size']['extra_sauce'], 2).')';
			    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
			    				
								if($pizza['cheese'] == 'Extra Cheese') {
			    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['Size']['extra_cheese'], 2).')';
			    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
			    				
			    				if($pizza['Crust']['LocationsCrust']['price'] > 0) {
			    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust (+$'.number_format($pizza['Crust']['LocationsCrust']['price'], 2).')';
			    				} else {
			    					$lines[] = $pizza['Crust']['Crust']['title'].' Crust';
			    				}
			    				/*
			    				if(sizeof($pizza['Topping']) > 0) {
									foreach($pizza['Topping'] as $topping):
										$l = 'Add '.$topping['Topping']['title'];
										if($topping['PizzasTopping']['placement'] != 'Whole') {
											$l .= ' ('.$topping['PizzasTopping']['placement'].' Side)';
										}
										
										if($topping['PizzasTopping']['price'] > 0) {
											$l .= ' (+$'.number_format($topping['PizzasTopping']['price'], 2).')';	
										}
										
										$lines[] = $l;
									endforeach;
			    				} */
			    				
			    				if(sizeof($lines) > 0) {
			    					$html .= "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
									foreach($lines as $line):
										$html .= "<li>".$line."</li>";
									endforeach;
			    					$html .= "</ul></div>";
			    				}
				    				 
								if(!empty($pizza['comments'])) {
				    				$html .= '<div style="margin-top: 5px;">Additional Instructions:<span style="padding-left: 5px; font-style: italic;">'.$pizza['comments'].'</span></div>'; 
				    			}
				$html .= '
				    		</td>
				    		<td style="font-weight: bold; vertical-align: top;">$'.number_format($pizza['total'], 2).'</td>
				    	</tr>';	
		    endforeach;
	    }
	    
	    if(sizeof($order['OrdersCombo']) > 0) {
	    	foreach($order['OrdersCombo'] as $combo_index => $combo):
	    		$html .= '
		    		<tr>
			    		<td style="font-weight: bold; vertical-align: top;">('.$combo['qty'].')</td>
			    		<td>
			    			<div style=" font-weight:bold; margin-bottom: 5px; ">'.$combo['OrdersCombo']['Combo']['title'].' Combo</div>
			    			<div class="">'.$combo['OrdersCombo']['Combo']['description'].'</div>';
			    			
				if(sizeof($combo['OrdersCombo']['OrdersCombosPizza']) > 0) {
					foreach($combo['OrdersCombo']['OrdersCombosPizza'] as $pizza):
						$lines = array();
	    				if($pizza['sauce'] == 'Extra Sauce') {
	    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
	    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
	    				
						if($pizza['cheese'] == 'Extra Cheese') {
	    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
	    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
	    				
	    				if($pizza['crust_price'] > 0) {
	    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
	    				} else {
	    					$lines[] = $pizza['Crust']['title'].' Crust';
	    				}
	    				
	    				if(sizeof($pizza['Topping']) > 0) {
							foreach($pizza['Topping'] as $topping):
								$l = 'Add '.$topping['title'];
								if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
									$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
								}
								
								if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
									$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
								}
								
								$lines[] = $l;
							endforeach;
	    				}
	    				
    					$html .= "<div style='padding-left: 20px; margin-top: 10px;  font-weight: bold;'>".$pizza['Size']['Size']['size']."\" ".$pizza['Size']['Size']['title']." Pizza</div>";
    					$html .= "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
						foreach($lines as $line):
							$html .= "<li>".$line."</li>";
						endforeach;
    					$html .= "</ul></div>";
					endforeach;
				}
			    			
				if(sizeof($combo['OrdersCombo']['OrdersCombosSpecialsSize']) > 0) {
					foreach($combo['OrdersCombo']['OrdersCombosSpecialsSize'] as $pizza):
						$lines = array();
	    				if($pizza['sauce'] == 'Extra Sauce') {
	    					$lines[] = $pizza['sauce'].' (+$'.number_format($pizza['sauce_price'], 2).')';
	    				} else if($pizza['sauce'] != 'Regular Sauce') { $lines[] = $pizza['sauce']; }
	    				
						if($pizza['cheese'] == 'Extra Cheese') {
	    					$lines[] = $pizza['cheese'].' (+$'.number_format($pizza['cheese_price'], 2).')';
	    				} else if($pizza['cheese'] != 'Regular Cheese') { $lines[] = $pizza['cheese']; }
	    				
	    				if($pizza['crust_price'] > 0) {
	    					$lines[] = $pizza['Crust']['title'].' Crust (+$'.number_format($pizza['crust_price'], 2).')';
	    				} else {
	    					$lines[] = $pizza['Crust']['title'].' Crust';
	    				}
	    				/*
	    				if(sizeof($pizza['Topping']) > 0) {
							foreach($pizza['Topping'] as $topping):
								$l = 'Add '.$topping['title'];
								if($topping['OrdersCombosPizzasTopping']['placement'] != 'Whole') {
									$l .= ' ('.$topping['OrdersCombosPizzasTopping']['placement'].' Side)';
								}
								
								if($topping['OrdersCombosPizzasTopping']['price'] > 0) {
									$l .= ' (+$'.number_format($topping['OrdersCombosPizzasTopping']['price'], 2).')';	
								}
								
								$lines[] = $l;
							endforeach;
	    				}
	    				*/
	    				
    					$html .= "<div style='padding-left: 20px; margin-top: 10px;  font-weight: bold;'>".$pizza['Size']['size']."\" ".$pizza['Size']['title']." ".$pizza['Special']['title']." Pizza</div>";
    					if(!empty($pizza['Special']['subtext'])) {
    						$html .= "<div style='padding-left: 20px;'>".$pizza['Special']['subtext']."</div>";	
    					}
    					$html .= "<div style=' margin-top: 5px; color: #BB080D; '><ul style='text-decoration:none; font-weight: bold;'>";
						foreach($lines as $line):
							$html .= "<li>".$line."</li>";
						endforeach;
    					$html .= "</ul></div>";
					endforeach;
				}
								
    					if(sizeof($combo['OrdersCombo']['OrdersCombosItem']) > 0) {
							foreach($combo['OrdersCombo']['OrdersCombosItem'] as $item):
								$html .= '
									<div style="padding-left: 20px; margin-top: 10px;  font-weight: bold;">'.$item['Item']['title'].'</div>
									<div style="padding-left: 20px;">'.$item['Item']['description'].'</div>';
							
		    					if(sizeof($item['ItemsOption']) > 0 || sizeof($item['LocationsCatsOption']) > 0) {
			    					$html .= '<div style="padding-left:20px;"><ul style="text-decoration: none; color: #BB080D; font-weight: bold;">'; 
			    					foreach($item['ItemsOption'] as $opt):
										$html .= '<li>'.$opt['label'];
										if($opt['OrdersCombosItemsItemsOption']['price'] > 0) {
											$html .= '<span class=""> (+$'.number_format($opt['OrdersCombosItemsItemsOption']['price'], 2).')</span>';	
										}	
			    					endforeach;
			    					
			    					foreach($item['LocationsCatsOption'] as $opt):
										$html .= '<li>'.$opt['label'];
										if($opt['OrdersCombosItemsLocationsCatsOption']['price'] > 0) {
											$html .= '<span style="padding-left: 5px;"> (+$'.number_format($opt['OrdersCombosItemsLocationsCatsOption']['price'], 2).')</span>';	
										}	
			    					endforeach;
			    					$html .= '</ul></div>';
			    				}
							endforeach;
						}
						
	    			if(sizeof($combo['OrdersCombo']['OrdersCombosTextItem']) > 0) {
						foreach($combo['OrdersCombo']['OrdersCombosTextItem'] as $item):
							echo '<div class="pad-l20 marg-t10 green">'.$item['CombosTextItem']['text'].'</div>';
							if(!empty($item['comments'])) { 
								echo '<div class="base b red pad-l20">'.$item['comments'].'</div>';
							} 
						endforeach;
					}
						
    				if(!empty($combo['OrdersCombo']['OrdersCombo']['comments'])) {
    					echo '<div style="margin-top: 10px; ">Additional Instructions:<span style="padding-left: 5px; font-style: italic;">'.$combo['OrdersCombo']['OrdersCombo']['comments'].'</span></div>'; 
    				}
			    				
			    $html .= '
			    		</td>
			    		<td style="font-weight: bold; vertical-align: top;">'.number_format($combo['OrdersCombo']['OrdersCombo']['total'], 2).'</td>
			    	</tr>';
	    	endforeach;
	    }
	    
	    $tax = ($order['Location']['ZipCode']['tax_rate'] / 100) * $order['Order']['subtotal'];
	    $html .= '
	    	</table>
		    <hr />
		    
		    <table style="width: 100%; margin-top: 10px; ">
			    <tr><td colspan="3" style="padding: 0px;"></td></tr>
			    <tr style="font-weight:bold;">
			    	<td colspan="2"><strong>Subtotal</strong></td>
			        <td style="width:100px;">$'.number_format($order['Order']['subtotal'], 2).'</td>
			    </tr>
			    <tr style="font-weight:bold;">
			        <td colspan="2"><strong>Tax ('.$order['Location']['ZipCode']['tax_rate'].'%)</strong></td>
			        <td>$'.number_format($tax, 2).'</td>
			    </tr>
			    <tr><td colspan="3" style="padding: 0px;"></td></tr>
		    </table>
		    <hr />';
	
    
    	if($order['Order']['order_method'] == 'Pickup') {    
	    	//if order is for pickup -->
	    	$total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] - $order['Order']['discount_amt'];
    		$html .= '
			    <div id="pickupInfoDiv">
			    	<table style="width: 100%; margin-top: 10px;">';
    		
					    if($order['Order']['discount_amt'] > 0) {
						    $html .= '
						    	<tr style="font-weight: bold;">
							        <td style="color: #BB080D;" colspan="2">
							        	<span>Discount Applied ('.$order['DiscountCode']['code'].')</span>';
						    
				        	if($order['DiscountCode']['discount_type'] == 'Percent') {
				        		$html .= '<span>'.($order['DiscountCode']['discount_amt'] * 100).'% OFF</span>';
				        	}
				        	
							$html .= '  
							        </td>
							        <td style="color: #BB080D;" style="width:100px;">-$'.number_format($order['Order']['discount_amt'], 2).'</td>
							    </tr>
						    ';
					    }
					    
					    $html .= '
					    	<tr style="font-weight: bold;">
						    	<td colspan="2">Order Fee</td>
						    	<td style="width:100px;">$'.number_format($order['Order']['oas_charge'], 2).'</td>
						    </tr>			    
						    
						    <tr style="font-weight: bold; ">
						        <td colspan="2"><span class="lg green">TOTAL</span></td>
						        <td>$'.number_format($total, 2).'</td>
						    </tr>
						</table>
				    </div>';
    	} else { 
	    	//if order is for delivery
	    	$total = $order['Order']['subtotal'] + $tax + $order['Order']['oas_charge'] + $order['Location']['delivery_charge'] - $order['Order']['discount_amt'];
	    	$html .= '
	    		<div id="deliveryInfoDiv">
			    	<table style="width: 100%; margin-top: 10px; ">';
					    if($order['Order']['discount_amt'] > 0) {
					    	$html .= '
							    <tr style="font-weight: bold;">
							        <td style="color: #BB080D;" colspan="2">
							        	<span>Discount Applied</span>';
					    	
							        	if($order['DiscountCode']['discount_type'] == 'Percent') {
							        		$html .= '<span>'.($order['DiscountCode']['discount_amt'] * 100).'% OFF</span>';
							        	}
							$html .= '
							        </td>
							        <td style="color: #BB080D;" style="width:100px;">-$'.number_format($order['Order']['discount_amt'], 2).'</td>
							    </tr>';
					    }
			$html .= '		    
					    <tr style="font-weight: bold;">
					    	<td colspan="2"><span>Delivery Fee</span></td>
					    	<td style="width:100px;">$'.number_format($order['Order']['oas_charge'] + $order['Location']['delivery_charge'], 2).'</td>
					    </tr>			    
					    
					    <tr style="font-weight: bold; ">
					        <td colspan="2"><span class="lg green">TOTAL</span></td>
					        <td>$'.number_format($total, 2).'</td>
					    </tr>
					</table>
			    </div>';
    	}
    	
    	$html .= '<hr /><hr />
    		<div style="font-weight:bold; font-size:120%; margin-top:20px; text-align:center;">CALL 1-855-MY-SLICE TO CONFIRM ORDER! ( Enter Confirmation: '.
    		$order['Location']['id'].' * '.$order['Order']['confirm_hash'].' # )</div>';
		
    	$html .= '
    		</div>
			</body>
			</html>';
    	return $html;
	}
	
	function admin_confirm($id = null) {
		$this->checkAdminSession();
		if(!$id) {
			$this->Session->setFlash(__('Invalid Order', true));
			$this->redirect($this->referer());
		}
		$order = $this->Order->orderInfo($id);
		
		//get restaurants primary card
		$l = $this->Order->Location->find('first', array('conditions' => array('Location.id' => $order['Order']['location_id']),
														 'fields'	  => array('Location.advertiser_id')));
		$ad_pay_profile = $this->Order->Location->Advertiser->AdvertisersPaymentProfile->getDefaultProfile($l['Location']['advertiser_id']);
		
		$amt_to_refund = $order['Order']['total'] - $order['Order']['oas_charge'] - $order['Order']['oas_restaurant_charge'];
		
		//issue a refund for order, minus the OAS fee, minus the restaurant fee
		$resp = $this->AuthorizeNet->cim_unlinked_refund($ad_pay_profile['Advertiser']['profile_id'],
														$ad_pay_profile['AdvertisersPaymentProfile']['payment_profile_id'], 
														$amt_to_refund, 'Credit: #'.$order['Order']['id']);
		
		if($resp['resultcode'] == 'Ok') {
			$save['Order'] = array('id'                  		=> $order['Order']['id'],
									'order_started'           	=> '1',
									'order_started_timestamp' 	=> date('Y-m-d H:i:s', $this->getLocalTimeOffset()),
									'admin_manual_confirm'		=> '1');
			
			if($this->Order->save($save)) {
				//add transaction record
				$this->Order->Location->Advertiser->AdvertisersPaymentProfile->saveTransaction($ad_pay_profile['AdvertisersPaymentProfile']['payment_profile_id'], 
																							   $resp['transactionId'], $amt_to_refund, 
																							   $order['Order']['location_id'], 
																							   'Order Credit (#'.$order['Order']['id'].')', 'Credit');
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Order has been confirmed successfully.', true));
			} else {
				$this->Session->setFlash(__('There was problem confirming the order. Please try again.', true));
			}
		} else {
			$this->Session->setFlash(__('There was problem crediting the order. Please try again.', true));
		}
		
		
		$this->redirect($this->referer());
	}
	
	function delivery_directions($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid Order.', true));
			$this->redirect($this->referer());
		}
		//make sure this order belongs to this restaurant
		$this->checkAdvertiserSession(array('type' => 'order_id', 'value' => $id));
		
		$this->Order->recursive = 2;
		$o = $this->Order->read(null, $id);
		
		//make sure this location has a Premium Membership before allowing use of the page. 
		if($o['Location']['acct_type'] == '1') {
			$this->Session->setFlash(__('This restaurant must have a Premium Membership to use this feature.', true));
			$this->redirect('/restaurants/upgrade');
		}
		
		$this->set('order', $o);
		$this->layout = 'restaurant';
	}

	function index() {
		$this->Order->recursive = 0;
		$this->set('orders', $this->paginate());
	}

	function details($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Order.', true));
			$this->redirect($this->referer());
		}
		
		//make sure either the restaurant or the user that the order belongs to, is logged in.
		if($this->Session->check('User')) {
			$this->checkUserSession(array('order_id' => $id));
			
			$this->layout = "user";
			$default = $this->Order->User->UsersAddr->getDefault($this->Session->read('User.id'));
			if($default) { $this->set('default_addr',  $default); }
			
			$this->set('pay_profile_check', $this->Order->User->UsersPaymentProfile->haveProfileCheck($this->Session->read('User.id')));
		} else {
			$this->checkAdvertiserSession(array('type' => 'order_id', 'value' => $id));
			$this->layout = "restaurant";
			
		}
		
		$this->set('order', $this->Order->orderInfo($id));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Order', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Order->save($this->data)) {
				$this->Session->setFlash(__('The Order has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Order could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Order->read(null, $id);
		}
		$locations = $this->Order->Location->find('list');
		$users = $this->Order->User->find('list');
		$this->set(compact('locations','users'));
	}

	function admin_index() { 
		$this->checkAdminSession();
		
		if(!empty($this->data)) {
			//search criteria has been submitted
			$conditions = array();
			if($this->data['Order']['status'] != '') {
				if($this->data['Order']['status'] == 'Refunded') {
					$conditions['Order.refunded'] = '1';
				} else if($this->data['Order']['status'] == 'Confirmed') {
					$conditions['Order.order_started'] = '1';
				} else {
					$conditions['Order.order_started'] = '0';
				}
			}
			if($this->data['Order']['location_id'] != '') {
				$conditions['Order.location_id'] = $this->data['Order']['location_id'];
			}
			
			if(empty($this->data['Order']['start_date']) || empty($this->data['Order']['end_date'])) {
				//start and end dates not specified
				$orders = $this->Order->ordersByDateRange(null, null, $conditions);
			} else {
				$orders = $this->Order->ordersByDateRange($this->data['Order']['start_date'], $this->data['Order']['end_date'], $conditions);
				$hdr_txt = 'Orders From: <span class="italic red">'.date('D, M jS, Y', strtotime($this->data['Order']['start_date'])).'</span> To <span class="italic red">'.
					   date('D, M jS, Y', strtotime($this->data['Order']['end_date'])).'</span>';
			}
		} else {
			//other wise display all previous orders for past 7 days
			$orders = $this->Order->ordersByDateRange($start_date = date('Y-m-d', strtotime("-7 days")), $end_date = date('Y-m-d'));
			$hdr_txt = 'Orders From: <span class="italic red">'.date('D, M jS, Y', strtotime($start_date)).'</span> To <span class="italic red">'.date('D, M jS, Y', strtotime($end_date)).'</span>';
		}
		$this->set('hdr_txt', $hdr_txt);
		$this->set('orders', $orders);
		$this->set('restaurant_list', $this->Order->Location->locationListByAdvertiser());
	}

	
	//refund order amount to customer for an unconfirmed order.
	function admin_refund($id = null) {
		$this->checkAdminSession();
		if(!$id) {
			$this->Session->setFlash(__('Invalid Order', true));
			$this->redirect($this->referer());
		}
		$order = $this->Order->orderInfo($id);
		
		// was the original charge a CIM transaction?
		if($order['Order']['is_cim_transaction'] == '1') {
			// refund/void the original cim transaction
			$response = $this->AuthorizeNet->cim_void_transaction($order['User']['profile_id'], 
																  $order['UsersPaymentProfile']['payment_profile_id'], 
																  $order['Order']['transaction_id'],
																  $order['Order']['total']);
			if($response['resultcode'] == 'Ok') {
				//cim transaction successfully refunded.
				$this->Order->UsersPaymentProfile->saveTransaction($order['Order']['user_id'], $order['UsersPaymentProfile']['payment_profile_id'],
																	 $response['transactionId'], $order['Order']['total'], $order['Order']['id'], 
																	 'CIM Refund - Order #: '.$order['Order']['id'], 'Refund');
				
				$this->Order->id = $id;
				$this->Order->saveField('refunded', '1');
				$this->Order->saveField('refunded_timestamp', date('Y-m-d H:i:s', $this->getLocalTimeOffset()));
				$this->Order->saveField('refund_transaction_id', $response['transactionId']);
				
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Order has been refunded successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem refunding the order. Please try again.', true));
			}
		} else { 
			//we are refunding a regular transaction type
			$response = $this->AuthorizeNet->voidCharge($order['Order']['transaction_id'], 
														$order['Order']['masked_cc'],
														$order['Order']['total']);
														
			if($response[1] == '1' || $response['resultcode'] == 'Ok') {
				// void/refund successful
				if($response['resultcode'] == 'Ok') {
					//this was a refund
					$this->Order->UsersPaymentProfile->saveTransaction($order['Order']['user_id'], null,
																   	   $response['transactionId'], $order['Order']['total'], $order['Order']['id'], 
																   	   'Non-CIM Refund - Order #: '.$order['Order']['id'], 'Refund');	
				} else {
					//this was a void
					$this->Order->UsersPaymentProfile->saveTransaction($order['Order']['user_id'], null,
																   	   $order['Order']['transaction_id'], $order['Order']['total'], $order['Order']['id'], 
																   	   'Non-CIM Void - Order #: '.$order['Order']['id'], 'Refund');
				}
				
				$this->Order->id = $id;
				$this->Order->saveField('refunded', '1');
				$this->Order->saveField('refunded_timestamp', date('Y-m-d H:i:s', $this->getLocalTimeOffset()));
				if(isset($response['transactionId'])) {
					//if this was a refund, and not a void....
					$this->Order->saveField('refund_transaction_id', $response['transactionId']);
				}
				
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Order has been refunded successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem refunding the order. Please try again.', true));
			}										
		}
		$this->redirect($this->referer());
		//$this->set('response', $response);
	}
	
	function admin_details($id = null) {
		$this->checkAdminSession();
		if(!$id) {
			$this->Session->setFlash(__('Invalid Order', true));
			$this->redirect($this->referer());
		}
		$this->set('order', $this->Order->orderInfo($id));
	}
	
}
?>