<?php
class CombosItemsController extends AppController {

	var $name = 'CombosItems';
	var $helpers = array('Text');
	
	function add($combo_id = null) {
		if (!$combo_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Combo Special.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $combo_id = $this->data['CombosItem']['combo_id'];
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$combo_id));
		
		if(!empty($this->data)) {
			//are we adding a single record or a series?
			if($this->data['CombosItem']['add_type'] == 'Series') {
				//get value for add_group_id
				$add_group = $this->CombosItem->find('first', array('conditions' => array('CombosItem.combo_id' => $combo_id),
																	'fields'     => array('MAX(CombosItem.add_group_id) as add_group_id')));				
				$add_group_id = (!empty($add_group[0]['add_group_id'])) ? $add_group[0]['add_group_id'] + 1 : '1';
				$success = true;
				
				//creating a choice series
				foreach($this->data['items'] as $id => $value):
					if($value == '1') {
						$save = array('CombosItem' => array('combo_id'        => $this->data['CombosItem']['combo_id'],
															'add_type'        => $this->data['CombosItem']['add_type'],
															'add_group_id'    => $add_group_id,
															'item_id'		  => $id,
															'add_group_limit' => $this->data['CombosItem']['add_group_limit']));
						$this->CombosItem->create();
						if(!$this->CombosItem->save($save)) { $success = false; }
					}
				endforeach;
				
				if($success) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('The Choice Series has been successfully added to your combo.', true));
				} else {
					$this->Session->setFlash(__('There was a problem adding the Choice Series to your combo. Please try again.', true));
				}
			} else {
				$this->CombosItem->create();
				if($this->CombosItem->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('The Menu Item has been successfully added to your combo.', true));
				} else {
					$this->Session->setFlash(__('There was a problem adding the menu item to your combo. Please try again.', true));
				}	
			}
			$this->redirect('/combos/edit/'.$this->data['CombosItem']['combo_id']);
		}
		$this->set('combo_id', $combo_id);
	}
	
	function ajax_single_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$c = $this->CombosItem->Combo->read('location_id', $combo_id);
				$this->set('cats', $this->CombosItem->Combo->Location->getCatList($c['Combo']['location_id']));	
				$this->set('combo_id', $combo_id);	
			}
		}
	}
	
	function ajax_populate_items_add() {
		if($this->RequestHandler->isAjax()) {
			$c = $this->CombosItem->Combo->read('location_id', $this->params['form']['comboId']);
			
			if($this->params['form']['showCheckboxes']) {
				$this->set('show_checkboxes', true);
			}
			$this->set('items', $this->CombosItem->Item->getItemsByCat($this->params['form']['catId'], $c['Combo']['location_id']));
		}
	}
	
	function ajax_populate_qtys_add() {
		if($this->RequestHandler->isAjax()) {
			$c = $this->CombosItem->Combo->read('location_id', $this->params['form']['comboId']);
			$this->set('max_qty', $this->CombosItem->Item->find('count', array('conditions' => array('Item.location_id' => $c['Combo']['location_id'],
																									 'Item.cat_id'      => $this->params['form']['catId']))));
		} 
	}
	
	function ajax_series_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$c = $this->CombosItem->Combo->read('location_id', $combo_id);
				$this->set('cats', $this->CombosItem->Combo->Location->getCatList($c['Combo']['location_id']));	
				$this->set('combo_id', $combo_id);	
			}
		}
	}
	
	function ajax_cat_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$c = $this->CombosItem->Combo->read('location_id', $combo_id);
				$this->set('cats', $this->CombosItem->Combo->Location->getCatList($c['Combo']['location_id']));	
				$this->set('combo_id', $combo_id);	
			}
		}
	}
	
	function ajax_single_sizes_add() {
		if($this->RequestHandler->isAjax()) {
			$this->set('sizes', $this->CombosItem->SpecialsSize->Special->getSizes($this->params['form']['specialId']));
		}
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('The page you are trying to reach does not exist.', true));
		} else {
			$c = $this->CombosItem->read(array('combo_id', 'add_group_id'), $id);
			$this->checkAdvertiserSession(array('type'=>'combo','value'=>$c['CombosItem']['combo_id']));

			if(!empty($c['CombosItem']['add_group_id'])) {
				//we have an entire add group to delete
				if($this->CombosItem->deleteAll(array('CombosItem.combo_id'     => $c['CombosItem']['combo_id'],
													  'CombosItem.add_group_id' => $c['CombosItem']['add_group_id']), false)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Choice Series successfully removed from your combo.', true));
				} else {
					$this->Session->setFlash(__('There was a problem removing the Choice Series from your combo. Please try again.', true));
				}
			} else {
				//just a single record to delete
				if($this->CombosItem->delete($id)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Item successfully removed from your combo.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem removing the item from your combo. Please try again.', true));	
				}	
			}	
		}
		$this->redirect($this->referer());
	}
}