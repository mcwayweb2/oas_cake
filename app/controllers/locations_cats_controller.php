<?php
class LocationsCatsController extends AppController {

	var $name = 'LocationsCats';
	
	function add() {
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['LocationsCat']['location_id']));
		
		if(!empty($this->data)) {
			//check to make sure there is not already a record for this categorys
			$category = $this->LocationsCat->find('first', array('conditions' => array('LocationsCat.location_id' => $this->data['LocationsCat']['location_id'],
																					   'LocationsCat.cat_id'      => $this->data['LocationsCat']['cat_id']),
																 'recursive'  => '-1'));
			if($category) {
				$this->Session->setFlash(__('You are already using this category.', true));
			} else {
				$this->LocationsCat->create();
				
				//is avail all day checkbox checked??
				if($this->data['LocationsCat']['avail_all_day'] == '1') {
					//avail all day
					unset($this->data['LocationsCat']['time_open']);
					unset($this->data['LocationsCat']['time_closed']);		
				} else {
					//make sure that start time is before end time
					if($this->data['LocationsCat']['time_open'] >= $this->data['LocationsCat']['time_closed']) {
						$this->Session->setFlash(__('Availability start time must be before the ending time.', true));
						$this->redirect($this->referer());
					}
				}
			
				if($this->LocationsCat->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully added your new category.', true));
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a category.', true));
				}
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a category.', true));
		}
		$this->redirect($this->referer());
	}
	
	function multiple_add($location_id = null) {
		if (!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $location_id = $this->data['LocationsCat']['location_id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			$success_ct = 0;
			$failure_ct = 0;
			$error_ct = 0;
			
			//loop through each item to add
			foreach($this->data['cats'] as $cat):
				if(!empty($cat['cat_id'])) {
					$success = true;
					//is this a new category or an existing one?
					if(!is_numeric($cat['cat_id'])) {
						//new cat to add, make sure this cat is not already in system
						$c = $this->LocationsCat->Cat->findByTitle($cat['cat_id']);
						if(!$c) {
							$this->LocationsCat->Cat->create();
							if(!$this->LocationsCat->Cat->save(array('Cat' => array('title' => ucwords($cat['cat_id']))))) $success = false;
							$cat_id = $this->LocationsCat->Cat->getLastInsertId();	
						} else { $cat_id = $c['Cat']['id']; }
					} else { $cat_id = $cat['cat_id']; } 
					
					if($success) {
						$this->LocationsCat->create();
						if($this->LocationsCat->save(array('LocationsCat' => array('cat_id'      => $cat_id,
																				   'location_id' => $location_id,
												  	  					   		   'subtext'     => $cat['subtext'])))) $success_ct++;				
					}			
				}
			endforeach;
			
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('('.$success_ct.') Menu Categories successfully added.', true));
			
			$l = $this->LocationsCat->Location->read('slug', $location_id);
			$this->redirect('/restaurants/edit_menu/'.$l['Location']['slug'].'#mnuCategories');
		}
		//need to create an array of locations, with index numbers that correspond with indexes in ad_locations array
		$this->set('current_loc_index', $this->LocationsCat->Location->getArrayIndex($location_id));
		$this->set('location', $this->LocationsCat->Location->getFieldsForBanner($location_id));
	}
	
	function edit() {
		if(empty($this->data)) {
			$category = $this->LocationsCat->read(array('id', 'location_id', 'subtext', 'cat_id', 'time_open', 'time_closed'), 
												  $this->params['form']['id']);
			$cat_id = $category['LocationsCat']['location_id'];
		} else { $cat_id = $this->data['LocationsCat']['location_id']; }
		
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$cat_id));
		
		if(!empty($this->data)) {
			//is avail all day checkbox checked??
			if($this->data['LocationsCat']['avail_all_day'] == '1') {
				//avail all day
				$this->data['LocationsCat']['time_open'] = null;
				$this->data['LocationsCat']['time_closed'] = null;		
			} else {
				//make sure that start time is before end time
				if($this->data['LocationsCat']['time_open'] >= $this->data['LocationsCat']['time_closed']) {
					$this->Session->setFlash(__('Availability start time must be before the ending time.', true));
					$this->redirect($this->referer());
				}
			}
			
			if($this->LocationsCat->save($this->data)) {
				$success = true;
				//do we have a new selection series to add?
				if(isset($this->data['labels'])) {
					if($this->data['LocationsCat']['new_series_limit'] == $this->data['LocationsCat']['new_series_qty']) {
						//add options as individual options...set add group to zero
						$add_group_id = 0;
						$add_group_limit = 0;
					} else {
						//get value for add_group_id
						$add_group = $this->LocationsCat->LocationsCatsOption->find('first', array('conditions' => array('LocationsCatsOption.locations_cat_id' => $this->data['LocationsCat']['id']),
																			  					   'fields'     => array('MAX(LocationsCatsOption.add_group_id) as add_group_id')));				
						$add_group_id = (!empty($add_group[0]['add_group_id'])) ? $add_group[0]['add_group_id'] + 1 : '1';
						$add_group_limit = $this->data['LocationsCat']['new_series_limit'];
					}
					
					foreach($this->data['labels'] as $i => $lab):
						if(!empty($lab)) {
							$save = array('LocationsCatsOption' => array('locations_cat_id' => $this->data['LocationsCat']['id'],
																		 'add_group_id'     => $add_group_id,
																		 'add_group_limit'  => $add_group_limit,
																		 'label'            => $lab,
																		 'additional_price' => $this->data['prices'][$i]));
							$this->LocationsCat->LocationsCatsOption->create();
							if(!$this->LocationsCat->LocationsCatsOption->save($save)) $success = false;
						}
					endforeach;
				}
				
				if($success) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully updated your category.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem updating your menu category. Please try again.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields for the new category.', true));
			}
			$this->redirect($this->referer());	
		}
		if(empty($this->data)) { $this->data = $category; }
		$this->set('cat_list', $this->LocationsCat->Cat->getCatList());
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid category.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$category = $this->LocationsCat->read(array('location_id', 'cat_id'), $id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$category['LocationsCat']['location_id']));
		
		if($this->LocationsCat->delete($id)) {
			$this->LocationsCat->Cat->Item->deleteAll(array('Item.cat_id' => $category['LocationsCat']['cat_id']));
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your category has been deleted successfully.', true));	
		} else {
			$this->Session->setFlash(__('There was a problem deleting your category. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function ajax_order() {
		$success = true;
		if($this->RequestHandler->isAjax()) {
			//need to validate that all locationsCat ids belong to logged in advertiser
	
			foreach($this->params['url']['listMenuCats'] as $order => $id):
				$this->LocationsCat->id = $id;
				if(!$this->LocationsCat->saveField('list_order', $order)) $success = false;
			endforeach;	
		}
		$this->set('result', (($success) ? "New Menu Category Order Saved!" : "Trouble Saving New Menu Category Order. Please try again!"));
	}
	
	function ajax_update_series_limit() {
		Configure::write('debug', '0');
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['qty']);
		}
	}
	
	function ajax_update_series_labels() {
		Configure::write('debug', '0');
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['qty']);
		}
	}

	function ajax_multiple_cats() {
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['numCats']);
			$this->set('cat_list', $this->LocationsCat->Cat->find('list', array('order' => array('Cat.title'))));
		}
	}
	
	function delete_series($option_id = null) {
		if (!$option_id) {
			$this->Session->setFlash(__('Invalid Selection Series.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$opt = $this->LocationsCat->LocationsCatsOption->read(null, $option_id);
		$category = $this->LocationsCat->read(array('location_id'), $opt['LocationsCatsOption']['locations_cat_id']);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$category['LocationsCat']['location_id']));
		
		//is this a single option to delete, or an entire series to delete?
		if($opt['LocationsCatsOption']['add_group_id'] > 0) {
			//...an entire series
			if($this->LocationsCat->LocationsCatsOption->deleteAll(array('LocationsCatsOption.locations_cat_id' => $opt['LocationsCatsOption']['locations_cat_id'],
																		 'LocationsCatsOption.add_group_id'     => $opt['LocationsCatsOption']['add_group_id']))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Selection series removed successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem removing the selection series. Please try again.', true));
			}	 
		} else {
			//...a single option
			if($this->LocationsCat->LocationsCatsOption->delete($option_id)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Add-On Option removed successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem removing the Add-On Option. Please try again.', true));
			}
		}
		$this->redirect($this->referer());
	}
}
?>