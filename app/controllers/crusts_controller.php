<?php
class CrustsController extends AppController {

	var $name = 'Crusts';
	
	//add a new crust type to the selectable list of crusts
	function add() {
		$this->checkAdvertiserSession();
		
		if(!empty($this->data)) {
			$this->data['Crust']['title'] = ucwords($this->data['Crust']['title']);
			$crust = $this->Crust->findByTitle($this->data['Crust']['title']);
			if($crust) {
				$this->Session->setFlash(__('The crust type you are trying to add is already available.', true));
			} else {
				$this->Crust->create();
				if($this->Crust->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully added your crust type.', true));
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a crust type.', true));
				}	
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a crust type.', true));
		}
		$this->redirect($this->referer().'#settingCrusts');
	}
	
}
?>