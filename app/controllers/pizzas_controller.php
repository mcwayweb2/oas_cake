<?php
class PizzasController extends AppController {

	var $name = 'Pizzas';
	var $components = array('Email');
	
	function verifyIdentity($pizza_id) {
		$pizza = $this->Pizza->read('order_id', $pizza_id);
		return ($pizza['Pizza']['order_id'] == $this->Session->read('User.order_id')) ? true : false;
	}
	
	function add() {
		if(!empty($this->data)) {
			$this->Pizza->create();
			$size = $this->Pizza->Size->read(array('base_price', 'topping_price', 'extra_cheese', 'extra_sauce'), 
											 $this->data['Pizza']['size_id']);
			$this->data['Pizza']['base_price'] = $size['Size']['base_price'];
			
			$crust = $this->Pizza->LocationsCrust->read('price', $this->data['Pizza']['locations_crust_id']);
			$this->data['Pizza']['crust_price'] = $crust['LocationsCrust']['price'];
			
			$total = $this->data['Pizza']['crust_price'] + $this->data['Pizza']['base_price'];
			if($this->data['Pizza']['sauce'] == 'Extra Sauce') { 
				$total += $size['Size']['extra_sauce'];
				$this->data['Pizza']['sauce_price'] = $size['Size']['extra_sauce']; 
			}
			if($this->data['Pizza']['cheese'] == 'Extra Cheese') { 
				$total += $size['Size']['extra_cheese'];
				$this->data['Pizza']['cheese_price'] = $size['Size']['extra_cheese']; 
			}

			if($this->Pizza->save($this->data)) {
				//add records for the toppings
				$success = true;
				$topping_total = 0;
				foreach($this->data['topping'] as $topping_id => $value):
					if($value != 'None') {
						$price = $size['Size']['topping_price'];
						//check if this topping has a price alter
						$loc_topping = $this->Pizza->Topping->LocationsTopping->find('first', array('conditions' => array('LocationsTopping.topping_id'=>$topping_id,
																														  'LocationsTopping.location_id'=>$this->Session->read('User.location_id')),
																									'fields' => array('LocationsTopping.price_alter')));
						if($loc_topping['LocationsTopping']['price_alter']) { $price += $loc_topping['LocationsTopping']['price_alter']; }

						$this->Pizza->PizzasTopping->create();
						if(!$this->Pizza->PizzasTopping->save(array('PizzasTopping'=>array('pizza_id' => $this->Pizza->getLastInsertId(),
																						  'topping_id' => $topping_id,
																						  'placement' => $value,
																						  'price' => $price)))) { $success = false; } 
						else { $topping_total += $price; }
					}
				endforeach;
				
				//update total in pizza record
				if($success) {
					$total = ($topping_total + $total) * $this->data['Pizza']['qty'];
					$this->Pizza->id = $this->Pizza->getLastInsertId();
					$this->Pizza->saveField('total', $total);
					$this->Pizza->saveField('topping_price', $topping_total);
					
					if($this->Pizza->Order->updateOrderTotals($this->Session->read('User.order_id'), $total)) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your pizza has been added to your order.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your pizza. Please try again.', true));
					}	
				} else {
					$this->Session->setFlash(__('There was a problem adding your pizza. Please try again.', true));
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding your pizza. Please try again.', true));
			}
			
			$this->redirect('/menu/'.$this->Pizza->Order->Location->getUrlSlugs($this->Session->read('User.location_id')));
		}
		$this->set('crusts', $this->Pizza->LocationsCrust->Location->getCrustList($this->Session->read('User.location_id')));
		$this->set('sizes', $this->Pizza->LocationsCrust->Location->getSizes($this->Session->read('User.location_id')));
		$this->set('toppings', $this->Pizza->Topping->Location->getToppings($this->Session->read('User.location_id')));
	}
	
	//delete a pizza from the current order
	function delete($pizza_id = null) {
		if(!$pizza_id) {
			$this->Session->setFlash(__('Invalid pizza id.', true));
		} else {
			$this->checkUserSession(array('order_pizza' => $pizza_id));

			//get line total before deleting record to decrement order subtotal
			$pizza = $this->Pizza->read('total', $pizza_id);
			
			if($this->Pizza->delete($pizza_id)) {
				if($this->Pizza->PizzasTopping->deleteAll(array('PizzasTopping.pizza_id' => $pizza_id), false)) {
					if($this->Pizza->Order->updateOrderTotals($this->Session->read('User.order_id'), $pizza['Pizza']['total'] * -1)) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Pizza removed from your order.', true));	
					} else {
						$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));
					}	
				} else {
					$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));	
			}	
		}
		$this->redirect($this->referer());
	}
}
?>