<?php
class ContentPagesController extends AppController {

	var $name = 'ContentPages';
	var $helpers = array('Html', 'Form');
	
	function get_content($id) {
		return $this->ContentPage->read(null, $id);
	}
	
	function view($slug = null) {
		$this->layout = 'nomap';
		$content = $this->ContentPage->findBySlug($slug);
		if (!$content) {
			$this->Session->setFlash(__('Your are attempting to connect to a page that does not exist.', true));
			$this->redirect('/');
		}
		$this->set('content', $content);
	}

	function admin_index() {
		$this->checkAdminSession();
		$this->ContentPage->recursive = 0;
		$this->set('contentPages', $this->paginate());
	}

	function admin_view($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid Content Page.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('contentPage', $this->ContentPage->read(null, $id));
	}
	
	function admin_add() {
		$this->checkAdminSession();
		if (!empty($this->data)) {
			$this->ContentPage->create();
			if ($this->ContentPage->save($this->data)) {
				$this->Session->setFlash(__('The Content Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Content Page could not be saved. Please, try again.', true));
			}
		}
	}
	
	function admin_edit($id = null) {
		$this->checkAdminSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Content Page', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->ContentPage->save($this->data)) {
				$this->Session->setFlash(__('The Content Page has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Content Page could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ContentPage->read(null, $id);
		}
	}
	
	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Content Page.', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ContentPage->delete($id)) {
			$this->Session->setFlash(__('Content Page successfully deleted.', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>