<?php
class OrdersSpecialsSizesController extends AppController {

	var $name = 'OrdersSpecialsSizes';
	var $helpers = array('Html', 'Form', 'GoogleMap');
	var $components = array('Clickatell', 'Strings', 'Email');
	
	function verifyIdentity($special_id) {
		$special = $this->OrdersSpecialsSize->read('order_id', $special_id);
		return ($special['OrdersSpecialsSize']['order_id'] == $this->Session->read('User.order_id')) ? true : false;
	}
	
	//add a specialty pizza to the current order session
	function add($special_id = null) {
		$this->checkUserSession();
		if(!$special_id && empty($this->data)) {
			$this->Session->setFlash(__('Error adding special. Invalid Special.', true));
			
			$this->redirect('/menu/'.$this->OrdersSpecialsSize->Order->Location->getUrlSlugs($this->Session->read('User.location_id')));
		}
		if(!empty($this->data)) {
			$this->OrdersSpecialsSize->create();
			$size = $this->OrdersSpecialsSize->SpecialsSize->read(array('Size.extra_cheese', 'Size.extra_sauce', 'SpecialsSize.price', 'Size.topping_price'), 
											 					  $this->data['OrdersSpecialsSize']['specials_size_id']);
			$this->data['OrdersSpecialsSize']['base_price'] = $size['SpecialsSize']['price'];
											 					  
			//$s = $this->OrdersSpecialsSize->SpecialsSize->read('price', $this->data['OrdersSpecialsSize']['specials_size_id']);
			
											 					  
			
			$crust = $this->OrdersSpecialsSize->LocationsCrust->read('price', $this->data['OrdersSpecialsSize']['locations_crust_id']);
			$this->data['OrdersSpecialsSize']['crust_price'] = $crust['LocationsCrust']['price'];
			
			$total = $this->data['OrdersSpecialsSize']['crust_price'] + $this->data['OrdersSpecialsSize']['base_price'];
			if($this->data['OrdersSpecialsSize']['sauce'] == 'Extra Sauce') { 
				$total += $size['Size']['extra_sauce'];
				$this->data['OrdersSpecialsSize']['sauce_price'] = $size['Size']['extra_sauce']; 
			}
			if($this->data['OrdersSpecialsSize']['cheese'] == 'Extra Cheese') { 
				$total += $size['Size']['extra_cheese'];
				$this->data['OrdersSpecialsSize']['cheese_price'] = $size['Size']['extra_cheese']; 
			}
			$this->data['OrdersSpecialsSize']['total'] = $total * $this->data['OrdersSpecialsSize']['qty']; 
			
			if($this->OrdersSpecialsSize->save($this->data)) {
				//check to see if any extra toppings need to be added to the price
				if(isset($this->data['topping'])) {
					$success = true;
					$topping_total = 0;

					foreach($this->data['topping'] as $topping_id => $value):
						if($value != 'None') {
							$price = $size['Size']['topping_price'];
							
							//check if this topping has a price alter
							$loc_topping = $this->OrdersSpecialsSize->Topping->LocationsTopping->find('first', array('conditions' => array('LocationsTopping.topping_id'=>$topping_id,
																															  			   'LocationsTopping.location_id'=>$this->Session->read('User.location_id')),
																									  'fields' => array('LocationsTopping.price_alter')));
							if($loc_topping['LocationsTopping']['price_alter']) { $price += $loc_topping['LocationsTopping']['price_alter']; }
		
							$this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->create();
							if(!$this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->save(array('OrdersSpecialsSizesTopping'=>array('orders_specials_size_id' => $this->OrdersSpecialsSize->getLastInsertId(),
																							  							'topping_id' => $topping_id,
																							  							'placement' => $value,
																							  							'price' => $price)))) { $success = false; } 
							else { $topping_total += $price; }
						}
					endforeach;
					
					//update order special size total & topping price if necessary
					if($success) {
						$this->data['OrdersSpecialsSize']['total'] += $topping_total;
						$this->OrdersSpecialsSize->id = $this->OrdersSpecialsSize->getLastInsertId();
						$this->OrdersSpecialsSize->saveField('total', $this->data['OrdersSpecialsSize']['total']);
						$this->OrdersSpecialsSize->saveField('topping_price', $topping_total);
						
						if($this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersSpecialsSize']['total'])) {
							$this->Session->write('show_warning', '1');
							$this->Session->setFlash(__('Your special has been added to your order.', true));
						} else {
							$this->Session->setFlash(__('There was a problem adding your special. Please try again.', true));
						}
					} else {
						$this->Session->setFlash(__('There was a problem adding your special, please try again.', true));
					}
				} else {
					if($this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersSpecialsSize']['total'])) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your special has been added to your order.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your special. Please try again.', true));
					}
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding your special, please try again.', true));
			}
			$this->redirect('/menu/'.$this->OrdersSpecialsSize->Order->Location->getUrlSlugs($this->Session->read('User.location_id')));
		} else {
			$this->set('special', $this->OrdersSpecialsSize->SpecialsSize->Special->getSpecialInfo($special_id, 1));
			$this->set('crusts', $this->OrdersSpecialsSize->LocationsCrust->Location->getCrustList($this->Session->read('User.location_id')));
		}
	}
	
	function edit($orders_specials_size_id = null) {
		Configure::write('debug', 0);
		$this->checkUserSession();
		if(!$orders_specials_size_id && empty($this->data)) {
			$this->Session->setFlash(__('Error updating special. Invalid Special.', true));
			$this->redirect('/locations/order_review');
		}
		if(!empty($this->data)) {
			$size = $this->OrdersSpecialsSize->SpecialsSize->read(array('Size.standard_topping', 'Size.premium_topping', 'Size.extra_cheese', 
																		'Size.extra_sauce', 'SpecialSize.price'), 
											 							$this->data['OrdersSpecialsSize']['specials_size_id']);
			
			$this->data['OrdersSpecialsSize']['base_price'] = $size['SpecialsSize']['price'];
			$crust = $this->OrdersSpecialsSize->LocationsCrust->read('price', $this->data['OrdersSpecialsSize']['locations_crust_id']);
			$this->data['OrdersSpecialsSize']['crust_price'] = $crust['LocationsCrust']['price'];
			
			$total = $this->data['OrdersSpecialsSize']['crust_price'] + $this->data['OrdersSpecialsSize']['base_price'];
			if($this->data['OrdersSpecialsSize']['sauce'] == 'Extra Sauce') { 
				$total += $size['Size']['extra_sauce'];
				$this->data['OrdersSpecialsSize']['sauce_price'] = $size['Size']['extra_sauce']; 
			}
			if($this->data['OrdersSpecialsSize']['cheese'] == 'Extra Cheese') { 
				$total += $size['Size']['extra_cheese'];
				$this->data['OrdersSpecialsSize']['cheese_price'] = $size['Size']['extra_cheese']; 
			}
			$old_total = $this->data['OrdersSpecialsSize']['total'] * -1;
			$this->data['OrdersSpecialsSize']['total'] = $total * $this->data['OrdersSpecialsSize']['qty']; 
			
			if($this->OrdersSpecialsSize->save($this->data)) {
				//remove all old topping records before starting to insert new ones
				$this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->deleteAll(array('OrdersSpecialsSizesTopping.orders_specials_size_id' => 
																					   $this->data['OrdersSpecialsSize']['id']));
				
				//check to see if any extra toppings need to be added to the price
				if(isset($this->data['topping'])) {
					$success = true;
					$topping_total = 0;
					foreach($this->data['topping'] as $topping_id => $value):
						if($value != 'None') {
							$topping = $this->OrdersSpecialsSize->Topping->read('type', $topping_id);
							$price = ($topping['Topping']['type'] == 'Premium') ? $size['Size']['premium_topping'] : $size['Size']['standard_topping'];
							//check if this topping has a price alter
							$loc_topping = $this->OrdersSpecialsSize->Topping->LocationsTopping->find('first', array('conditions' => array('LocationsTopping.topping_id'=>$topping_id,
																															  			   'LocationsTopping.location_id'=>$this->Session->read('User.location_id')),
																									  'fields' => array('LocationsTopping.price_alter')));
							if($loc_topping['LocationsTopping']['price_alter']) { $price += $loc_topping['LocationsTopping']['price_alter']; }
		
							$this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->create();
							if(!$this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->save(array('OrdersSpecialsSizesTopping'=>array('orders_specials_size_id' => $this->OrdersSpecialsSize->getLastInsertId(),
																							  							'topping_id' => $topping_id,
																							  							'placement' => $value,
																							  							'price' => $price)))) { $success = false; } 
							else { $topping_total += $price; }
						}
					endforeach;
					
					//update order special size total & topping price if necessary
					if($success) {
						$this->data['OrdersSpecialsSize']['total'] += $topping_total;
						$this->OrdersSpecialsSize->id = $this->data['OrdersSpecialsSize']['id'];
						$this->OrdersSpecialsSize->saveField('total', $this->data['OrdersSpecialsSize']['total']);
						$this->OrdersSpecialsSize->saveField('topping_price', $topping_total);
						
						//make sure to decrement the old line total, before adding the new one
						$this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $old_total);
						
						if($this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersSpecialsSize']['total'])) {
							$this->Session->write('show_warning', '1');
							$this->Session->setFlash(__('Your special has been added to your order.', true));
						} else {
							$this->Session->setFlash(__('There was a problem adding your special. Please try again.', true));
						}
					} else {
						$this->Session->setFlash(__('There was a problem adding your special, please try again.', true));
					}
				} else {
					//make sure to decrement the old line total, before adding the new one
					$this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $old_total);
					
					if($this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersSpecialsSize']['total'])) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your special has been added to your order.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your special. Please try again.', true));
					}
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding your special, please try again.', true));
			}
			$this->redirect('/locations/order_review');
		} else {
			$this->data = $this->OrdersSpecialsSize->read(null, $orders_specials_size_id);
		}
		$special_size = $this->OrdersSpecialsSize->SpecialsSize->read('special_id', $this->data['OrdersSpecialsSize']['specials_size_id']);
		$this->set('special', $this->OrdersSpecialsSize->SpecialsSize->Special->getSpecialInfo($special_size['SpecialsSize']['special_id'], 1));
		$this->set('crusts', $this->OrdersSpecialsSize->LocationsCrust->Location->getCrustList($this->Session->read('User.location_id')));
	}
	
	//delete a specialty pizza from the current order
	function delete($special_id) {
		$this->checkUserSession(array('order_special' => $special_id));

		//get line total before deleting record to decrement order subtotal
		$special = $this->OrdersSpecialsSize->read('total', $special_id);
		
		if($this->OrdersSpecialsSize->delete($special_id)) {
			if($this->OrdersSpecialsSize->OrdersSpecialsSizesTopping->deleteAll(array('OrdersSpecialsSizesTopping.orders_specials_size_id' => $special_id), false)) {
				if($this->OrdersSpecialsSize->Order->updateOrderTotals($this->Session->read('User.order_id'), $special['OrdersSpecialsSize']['total'] * -1)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Item removed from your order.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));
			}	
		} else {
			$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));	
		}
		$this->redirect($this->referer());
	}
	
	function ajax_extra_toppings() {
		$this->set('toppings', $this->OrdersSpecialsSize->Order->Location->getToppings($this->Session->read('User.location_id')));
	}
}
?>