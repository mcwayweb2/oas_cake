<?php
class TutorialsController extends AppController {

	var $name = 'Tutorials';
	
	function index() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		$this->set('tutorials', $this->Tutorial->allTutorialsByType());
	}
	
	function admin_index() {
		$this->checkAdminSession();
		
		//are we trying to add a new record
		if(!empty($this->data)) {
			$this->Tutorial->set($this->data);
			if($this->Tutorial->validates()) {
				//make sure a tutorial with this title does not exist
				$t = $this->Tutorial->findByTitle($this->data['Tutorial']['title']);
				if($t) {
					$this->Session->setFlash(__('There is already a tutorial with this title. Please change the title and try again.', true));
				} else {
					$this->Tutorial->create();
					if($this->Tutorial->save($this->data)) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Tutorial added successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding the tutorial. Please try again.', true));
					}	
				}
			}
		}
		$this->set('tutorials', $this->Tutorial->find('all'));
		$this->set('types', $this->Tutorial->getTypeList());
	}
	
	function view($slug = null) {
		$this->checkAdvertiserSession();
		if(!$slug) {
			$this->Session->setFlash(__('Invalid tutorial.', true));
			$this->redirect($this->referer());
		}
		$t = $this->Tutorial->findBySlug($slug);
		if(!$t) {
			$this->Session->setFlash(__('Invalid tutorial.', true));
			$this->redirect($this->referer());
		}
		
		$this->layout = 'doc-view';
		$this->set('t', $t);
	}
	
	function admin_view($slug = null) {
		$this->checkAdminSession();
		if(!$slug) {
			$this->Session->setFlash(__('Invalid tutorial.', true));
			$this->redirect($this->referer());
		}
		$t = $this->Tutorial->findBySlug($slug);
		if(!$t) {
			$this->Session->setFlash(__('Invalid tutorial.', true));
			$this->redirect($this->referer());
		}
		
		$this->layout = 'admin-doc-view';
		$this->set('t', $t);
	}
	
	function admin_edit() {
		$this->checkAdminSession();
	}
	
	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Tutorial', true));
			$this->redirect($this->referer());
		}
		if ($this->Tutorial->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Tutorial successfully removed.', true));
		} else {
			$this->Session->setFlash(__('There was a problem removing the Tutorial. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
}
?>