<?php
class AdvertisersPaymentProfilesController extends AppController {

	var $name = 'AdvertisersPaymentProfiles';
	var $helpers = array('Html', 'Form');
	var $components = array('AuthorizeNet', 'Email', 'Security');
	
	function beforeFilter() {
		if($this->params['action'] == 'manage') {
			$this->Security->validatePost = false;
		}
		
		$this->Security->blackHoleCallback = 'forceSSL';
        $this->Security->requireSecure('manage');
	}
	
	// NOTE: code is already done to have bank account use integrated as well. use: /views/advertisers_payment_profiles/manage_WITH_BANK_ACCT_CODE.ctp
	function manage() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';

		if (!empty($this->data)) {
			$this->AdvertisersPaymentProfile->set($this->data);
			if($this->AdvertisersPaymentProfile->validates()) {
				
				$this->AdvertisersPaymentProfile->create();
				$this->data['AdvertisersPaymentProfile']['timestamp'] = date("Y-m-d H:i:s");
				
				if($this->data['AdvertisersPaymentProfile']['profile_type'] == 'Credit Card') {
					$this->data['AdvertisersPaymentProfile']['card_suffix'] = 'XXXX'.substr($this->data['AdvertisersPaymentProfile']['cc_num'], 12);
				} else {
					$this->data['AdvertisersPaymentProfile']['card_suffix'] = 'XXXX'.substr($this->data['AdvertisersPaymentProfile']['account'], -4);
				}
				if($this->AdvertisersPaymentProfile->save($this->data)) {
					$ad_info = $this->AdvertisersPaymentProfile->Advertiser->read('profile_id', $this->Session->read('Advertiser.id'));
					
					if($this->data['AdvertisersPaymentProfile']['profile_type'] == 'Credit Card') {
						$exp = $this->data['AdvertisersPaymentProfile']['year']['year']."-".$this->data['AdvertisersPaymentProfile']['month']['month'];
					
						$billinginfo = array("fname"   => $this->data['AdvertisersPaymentProfile']['fname'],
											 "lname"   => $this->data['AdvertisersPaymentProfile']['lname'],
											 "address" => $this->data['AdvertisersPaymentProfile']['address'],
											 "city"    => $this->data['AdvertisersPaymentProfile']['city'],
											 "state"   => $this->data['AdvertisersPaymentProfile']['state'],
											 "zip"     => $this->data['AdvertisersPaymentProfile']['zip'],
											 "cc"      => $this->data['AdvertisersPaymentProfile']['cc_num'],
											 "exp"     => $exp);
						
						$response = $this->AuthorizeNet->cim_create_payment_profile($ad_info['Advertiser']['profile_id'], $billinginfo, 'Credit Card');
					} else {
						//type is bank account
						$billinginfo = array("fname"   => $this->data['AdvertisersPaymentProfile']['fname'],
											 "lname"   => $this->data['AdvertisersPaymentProfile']['lname'],
											 "routing" => $this->data['AdvertisersPaymentProfile']['routing'],
											 "account"    => $this->data['AdvertisersPaymentProfile']['account']);
						
						$response = $this->AuthorizeNet->cim_create_payment_profile($ad_info['Advertiser']['profile_id'], $billinginfo, 'Bank Account');
					}
					
					if($response['resultcode'] == 'Ok') {
						$this->AdvertisersPaymentProfile->saveField('payment_profile_id', $response['paymentProfileId']);
						
						if($this->Session->read('Advertiser.is_billing_profile_set') == '0') {
							$this->AdvertisersPaymentProfile->Advertiser->id = $this->Session->read('Advertiser.id');
							$this->AdvertisersPaymentProfile->Advertiser->saveField('is_billing_profile_set', '1');
							$this->Session->write('Advertiser.is_billing_profile_set', '1');  
						}
						
						//check to see if the new profile is default, if so, remove old default indicator
						if($this->data['AdvertisersPaymentProfile']['default'] == '1') {
							$old_default = $this->AdvertisersPaymentProfile->find('first', array('conditions' => array('AdvertisersPaymentProfile.default' => '1',
																													   'NOT' => array('AdvertisersPaymentProfile.id' => $this->AdvertisersPaymentProfile->getLastInsertId())),
																							     'fields'     => array('AdvertisersPaymentProfile.id')));
							$this->AdvertisersPaymentProfile->id = $old_default['AdvertisersPaymentProfile']['id'];
							$this->AdvertisersPaymentProfile->saveField('default', '0');
						}
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your billing profile has been added successfully.', true));
						$this->redirect('/advertisers/dashboard');				
					} else {
						$this->set('resp', $response);
						$this->Session->setFlash(__('Your billing profile cannot be authorized: '.$response['text'], true));
						//$this->Session->setFlash(__('Your credit card cannot be authorized. Please try again.', true));
						$this->AdvertisersPaymentProfile->delete($this->AdvertisersPaymentProfile->getLastInsertId());
					}
				} else {
					$this->Session->setFlash(__('There was a problem creating your payment profile.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required form fields.', true));
			}
		}

		$profiles = $this->AdvertisersPaymentProfile->find('all', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id' => $this->Session->read('Advertiser.id')),
																		'recursive'  => -1,
																		'order'      => array('AdvertisersPaymentProfile.default' => 'DESC',
																							  'AdvertisersPaymentProfile.name'    => 'ASC')));
		$this->set('profiles', $profiles);
		$this->set('states', $this->AdvertisersPaymentProfile->Advertiser->Location->State->find('list', array('fields' => array('abbr', 'name'))));																											   
	}
	
	function ajax_cc_fields() {
		if(!$this->RequestHandler->isAjax()) {
			$this->Session->setFlash(__('Invalid page request.', true));
			$this->redirect($this->referer());
		}
		$this->set('states', $this->AdvertisersPaymentProfile->Advertiser->Location->State->find('list', array('fields' => array('abbr', 'name'))));
	}
	
	function ajax_bank_fields() {
		if(!$this->RequestHandler->isAjax()) {
			$this->Session->setFlash(__('Invalid page request.', true));
			$this->redirect($this->referer());
		}	
	}

	function remove($id) {
		$this->checkAdvertiserSession(array('type'  => 'pay_profile',
										    'value' => $id));
		
		$ad_info = $this->AdvertisersPaymentProfile->Advertiser->read('profile_id', $this->Session->read('Advertiser.id'));
		$profile_info = $this->AdvertisersPaymentProfile->read(array('payment_profile_id', 'default'), $id);
		
		//make sure we are not trying to delete the default
		if($profile_info['AdvertisersPaymentProfile']['default'] == '1') {
			$this->Session->setFlash(__('You can\'t delete your default payment profile.', true));
		} else {
			//remove payment profile from authorize.net CIM
			$response = $this->AuthorizeNet->cim_delete_payment_profile($ad_info['Advertiser']['profile_id'], 
																		$profile_info['AdvertisersPaymentProfile']['payment_profile_id']);
			if($response['resultcode'] == 'Ok') {
				//now remove local db record
				$this->AdvertisersPaymentProfile->delete($id);
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your payment profile has been removed successfully.', true));				
			} else {
				$this->Session->setFlash(__('There was a problem deleting your pay profile. Please try again.', true));
			}
		}
		$this->redirect($this->referer());
	}
	
	function set_default($id) {
		$this->checkAdvertiserSession(array('type'  => 'pay_profile',
										    'value' => $id));
		
		$profile = $this->AdvertisersPaymentProfile->read('advertiser_id', $id);
		//remove old default profile setting
		$p = $this->AdvertisersPaymentProfile->find('first', array('conditions' => array('AdvertisersPaymentProfile.advertiser_id' => $profile['AdvertisersPaymentProfile']['advertiser_id'],
																						 'AdvertisersPaymentProfile.default'       => '1'),
																   'fields'     => array('AdvertisersPaymentProfile.id')));
		$this->AdvertisersPaymentProfile->id = $p['AdvertisersPaymentProfile']['id'];
		if($this->AdvertisersPaymentProfile->saveField('default', '0')) {
			//set new default record
			$this->AdvertisersPaymentProfile->id = $id;
			if($this->AdvertisersPaymentProfile->saveField('default', '1')) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('New default payment profile has been successfully set.', true));
			} else {
				$this->Session->setFlash(__('There was a problem setting your new default payment profile. Please try again.', true));
			}
		} else {
			$this->Session->setFlash(__('There was a problem setting your new default payment profile. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function profileCount() {
		return sizeof($this->AdvertisersPaymentProfile->find('all', array('conditions'=>array('AdvertisersPaymentProfile.advertiser_id'=>$this->Session->read('Advertiser.id')))));
	}
	
	function verifyIdentity($id) {
		$check = $this->AdvertisersPaymentProfile->read('advertiser_id', $id);
		return ($check['AdvertisersPaymentProfile']['advertiser_id'] == $this->Session->read('Advertiser.id')) ? true : false; 
	}
	
	function getProfileInfo($advertiser_id) {
		$ad = $this->AdvertisersPaymentProfile->Advertiser->read('profile_id', $advertiser_id);
		return $this->AuthorizeNet->cim_get_profile($ad['Advertiser']['profile_id']);
	}
}
?>