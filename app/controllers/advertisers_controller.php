<?php
class AdvertisersController extends AppController {

	var $name = 'Advertisers';
	var $components = array('FileUpload', 'AuthorizeNet', 'Email', 'MaxMind');
	
	//function to ajax load customers tab of the home page
	function home_ajax_div_advertisers() {}
	
	function ajax_check_username() {
		Configure::write('debug', 0);
		
		$username = $this->params['form']['username'];
		$user = $this->Advertiser->findByUsername($username);
		$color = 'red';
		
		//check against regular expression
		if(strlen($username) < 5 || strlen($username) > 20) {
			$msg = 'Username must be 5-20 characters in length.';
		} else if(!ctype_alnum($username)) {
			$msg = 'Invalid. Username must be alphanumeric.';
		} else if($user) {
			$msg = 'This username is already taken. Please choose another one.';
		} else {
			$msg = 'This username is available';
			$color = 'green';
		}
		
		$this->set('msg', $msg);
		$this->set('color', $color);
	}
	
	function getAdvertiserInfo($advertiserid) {
		$params = array('conditions' => array('Advertiser.id' => $advertiserid));
		return $this->Advertiser->find('first', $params);
	}
	/*
	function manage_ads() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		if(!empty($this->data)) {
			$this->Advertiser->AdvertisersAd->create();
			$this->Advertiser->AdvertisersAd->set($this->data);	
			if($this->Advertiser->AdvertisersAd->validates()) {
				if (!empty($this->data['AdvertisersAd']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['AdvertisersAd']['image']['name'], "/files/ads/");
				    $error = $this->FileUpload->handleFileUpload($this->data['AdvertisersAd']['image'], $fileName, "ads/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['AdvertisersAd']['image'] = $fileName;
				    	
				    } else { 
				    	$this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); 
				    	$this->Session->setFlash(__('There was a problem uploading your ad.', true));
				   	}	
		    	} else { 
		    		$this->Session->setFlash(__('Please select an image to upload.', true));
		    	}
			} else {
				$this->Session->setFlash(__('Please fill out all the form fields.', true));
			}
		}
		
		$this->set('home_ads', $this->Advertiser->AdvertisersAd->find('all', array('conditions'=>array('AdvertisersAd.advertiser_id'=>$this->Session->read('Advertiser.id'),
																				                       'AdvertisersAd.type'=>'Home'))));
		$this->set('sub_ads', $this->Advertiser->AdvertisersAd->find('all', array('conditions'=>array('AdvertisersAd.advertiser_id'=>$this->Session->read('Advertiser.id'),
																				                      'NOT' => array('AdvertisersAd.type'=>'Home')))));
	}
	*/
	
	function password_reset() {
    	if(!empty($this->data)) {
    		if($this->Advertiser->validates()) {
	    		//verify email
	    		$someone = $this->Advertiser->find('first', array('conditions'=>array('Advertiser.username'=>$this->data['Advertiser']['username'],
	    																		'Advertiser.email'   =>$this->data['Advertiser']['email'])));
	    		if(!empty($someone['Advertiser']['username'])) {
	    			//create new password 
	    			$password = $this->Strings->randomString();	
	    			
	    			//update db with new password
	    			$this->Advertiser->id = $someone['Advertiser']['id'];
	    			$this->Advertiser->saveField('password', md5($password));
	    			
	    			//send email to Advertiser notifying new password
	    			if($this->email_password_reset($password, $someone)) {
						$this->Session->setFlash(__('Your password has been reset successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem resetting your password.', true));
					}
	    		} else {
	    			$this->Session->setFlash(__('Sorry, there is no registered account with those credentials.', true));
	    		}
	    	} else {
	    		$this->Session->setFlash(__('There was an error validating the form.', true));
	    	}
    	}
    	
    	//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Advertiser->Location->getFeatured($lat, $long));
    }
    
	function email_password_reset($password = null, $advertiser = null) {
		if(!$password) { return false; }
		
		$this->Email->to = $this->data['Advertiser']['email'];
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->subject = 'OrderASlice.com Advertiser Password Reset Email';
		$this->Email->sendAs = 'html';
		
		$this->set('name',$advertiser['Advertiser']['contact_fname']." ".$advertiser['Advertiser']['contact_lname']);
		$this->set('password',$password);
		
		$this->Email->template = 'pw_reset_advertiser';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
	
	function recover_username() {
		if(!empty($this->data)) {
    		if($this->Advertiser->validates()) {
	    		//verify email
	    		$someone = $this->Advertiser->findByEmail($this->data['Advertiser']['email']);
	    		if(!empty($someone['Advertiser']['email'])) {
	    			//send email to Advertiser notifying new password
	    			if($this->email_recover_username($someone)) {
	    				$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Email successfully sent to: '.$someone['Advertiser']['email'].'.', true));
					} else {
						$this->Session->setFlash(__('There was a problem recovering your account. Please try again.', true));
					}
	    		} else {
	    			$this->Session->setFlash(__('Sorry, there is no registered account under this email address.', true));
	    		}
	    	} else {
	    		$this->Session->setFlash(__('There was an error validating the form.', true));
	    	}
    	}
    	
    	//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Advertiser->Location->getFeatured($lat, $long));
	}
	
	function email_recover_username($advertiser = null) {
		if(!$advertiser) { return false; }
		
		$this->Email->to = $advertiser['Advertiser']['email'];
		$this->Email->from = "admin@orderaslice.com";
		$this->Email->subject = 'OrderASlice.com Username Recovery Email';
		$this->Email->sendAs = 'html';
		
		$this->set('name',$advertiser['Advertiser']['contact_fname']." ".$advertiser['Advertiser']['contact_lname']);
		$this->set('username', $advertiser['Advertiser']['username']);
		
		$this->Email->template = 'recover_username_advertiser';
		return ($this->Email->send()) ? true : false;
	}

	function register() {
		if (!empty($this->data)) {
			$this->Advertiser->create();
			$this->data['Advertiser']['date_registered'] = date("Y-m-d H:i:s");
			$this->data['Advertiser']['active'] = '1';
			
			//make sure username is not already taken 
			$ad = $this->Advertiser->findByUsername($this->data['Advertiser']['username']);
			$find = $this->Advertiser->findByEmail($this->data['Advertiser']['email']);
			$this->Advertiser->set($this->data);
			if(!$this->Advertiser->validates()) {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} else if(!empty($ad['Advertiser']['username'])) {
				$this->Session->setFlash(__('This username is taken, please choose a new one.', true));
			} else if(!empty($find['Advertiser']['email'])) {
				$this->Session->setFlash(__('There is already an account registered to this email. <a href="/advertisers/login">Login</a> or <a href="/advertisers/password_reset">Trouble Logging In?</a>.', true));
			} else if(empty($this->data['Pass']['pass1']) || empty($this->data['Pass']['pass2'])) {
				$this->Session->setFlash(__('Please fill out both password fields.', true));
			} else if($this->data['Pass']['pass1'] != $this->data['Pass']['pass2']) {
				$this->Session->setFlash(__('Your new passwords must match!', true));
			} else {
				$this->data['Advertiser']['password'] = md5($this->data['Pass']['pass1']);
			
		    	if (!empty($this->data['Advertiser']['logo']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Advertiser']['logo']['name'], "/files/logos/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Advertiser']['logo'], $fileName, "files/logos/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Advertiser']['logo'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }	
		    	} else { $this->data['Advertiser']['logo'] = ""; }
		    	
				if ($this->Advertiser->save($this->data)) {
					//create the auth.net profile id
					$response = $this->AuthorizeNet->cim_create_profile($this->Advertiser->getLastInsertId(), $this->data['Advertiser']['email'], 'Advertiser Profile');
					if($response['resultcode'] != 'Ok') {
						$this->Session->setFlash(__('There was an error creating payment profile because: '.$response['text'].'. Please try again.', true));
					} else {
						$this->Advertiser->id = $this->Advertiser->getLastInsertId();
						$this->Advertiser->saveField('profile_id', $response['profileId']);
						
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Registration successful. Please proceed to setup your first restaurant location.', true));
						$this->email_successful_registration($this->Advertiser->getLastInsertId());
						
						$ad = $this->Advertiser->read(null, $this->Advertiser->getLastInsertId());
						$ad['Advertiser']['Banner']['filename'] = $ad['Banner']['filename'];
						$this->Session->write('Advertiser', $ad['Advertiser']);
						$this->redirect('/locations/add');	
					}
				} else {
					$this->Session->setFlash(__('There was a problem with registration. Please, try again.', true));
				}
			}
		}
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Advertiser->Location->getFeatured($lat, $long));
	}
	
	/*
	function payment ($id = null) {
		if(!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid advertiser. Please, try again.', true));
			$this->redirect(array('action'=>'register'));
		}
		if(!empty($this->data)) {
			$this->Advertiser->set($this->data);
			if($this->Advertiser->validates()) {
				//form fields validated, now try and create the payment profile
				$ad = $this->Advertiser->read('profile_id', $this->data['Advertiser']['id']);
				$this->data['Advertiser']['exp'] = $this->data['Advertiser']['year']['year']."-".$this->data['Advertiser']['month']['month'];	
				
				$response = $this->AuthorizeNet->cim_create_payment_profile($ad['Advertiser']['profile_id'], $this->data['Advertiser']);
				if($response['resultcode'] == 'Ok') {
					//payment profile successfully created, now insert the record for payment profile info 
					$card_suffix = 'xxxx'.substr($this->data['Advertiser']['cc_num'], 12);
					$save = array('advertiser_id'=>$this->data['Advertiser']['id'],
								  'payment_profile_id'=>$response['paymentProfileId'],
								  "card_suffix"=>$card_suffix,
								  "default" => "1",
								  "refund"  => '1',
								  'timestamp'=>date("Y-m-d h:i:s"));
					$save_data = array_merge($this->data['Advertiser'], $save);
					
					$this->Advertiser->AdvertisersPaymentProfile->create();
					if($this->Advertiser->AdvertisersPaymentProfile->save($save_data)) {
						//record added successfully, now set advertiser to active, send welcome email, redirect to dashboard
						$this->Advertiser->id = $this->data['Advertiser']['id'];
						$this->Advertiser->saveField('active', '1');
						
						$this->email_successful_registration($this->data['Advertiser']['id']);
						$ad = $this->Advertiser->read(null, $this->data['Advertiser']['id']);
						$this->Session->write('Advertiser', $ad['Advertiser']);
						
						$this->Session->setFlash(__('You have successfully completed registration. Please proceed to setting up locations.', true));
						$this->redirect(array('action'=>'dashboard'));
					} else {
						$this->Session->setFlash(__('There was a problem creating your payment profile.', true));
					}				
				} else {
					$this->Session->setFlash(__('Your credit card cannot be authorized.:'.$response2['text'], true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required form fields.', true));
			}
			$id = $this->data['Advertiser']['id'];
		}
		if(empty($this->data)) {
			$this->data = $this->Advertiser->read(null, $id);
		}
		$this->set('states', $this->Advertiser->Location->State->find('list', array('fields' => array('abbr', 'name'))));
	}
	*/
	function email_successful_registration($id) {
		$ad = $this->Advertiser->read(null, $id);
		
		$this->Email->to = $ad['Advertiser']['email'];
		$this->Email->replyTo = 'customer-service@orderaslice.com';
		$this->Email->from = 'admin@orderaslice.com';
		$this->Email->subject = 'Welcome to OrderASlice.com!';
		$this->Email->sendAs = 'html';
		
		//set email vars
		$this->set('ad',$ad);
		
		$this->Email->template = 'registration_success_advertiser';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
		
	}
	
	function dashboard() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		$locations = $this->Advertiser->Location->find('all', array('conditions' => array('Location.advertiser_id' => $this->Session->read('Advertiser.id')),
																	'fields'	=> array('Location.id', 'Location.active', 'Location.menu_ready', 
																						 'Location.acct_type')));
		//records for recent orders for each location
		$orders = array();
		foreach ($locations as $loc):
			if($loc['Location']['active'] == '1' || $loc['Location']['menu_ready'] == '1') {
				$orders[$loc['Location']['id']] = $this->Advertiser->Location->Order->find('all', array('conditions'=>array('Order.location_id'=>$loc['Location']['id'],
																															'Order.timestamp LIKE'=>'%'.date('Y-m-d').'%',
																															'Order.complete' => '1'),
																										'contain'   =>array('Location', 'User', 'UsersAddr'),
																										'order'		=>array('Order.timestamp' => 'DESC')));
				//get count of any currently unconfirmed orders
				$orders[$loc['Location']['id']]['unsettled_ct'] = $this->Advertiser->Location->Order->find('count', array('conditions' => array('Order.location_id' => $loc['Location']['id'],
																																				'Order.complete'	=> '1',
																																				'Order.order_started' => '0')));
			}
		endforeach;
		$this->set('orders', $orders);
	}

	function login() {
        if (!empty($this->data)) {
            $someone = $this->Advertiser->findByUsername($this->data['Advertiser']['loginUsername']);

            if(!empty($someone['Advertiser']['password']) && $someone['Advertiser']['password'] == md5($this->data['Advertiser']['loginPassword'])) {	              
                $someone['Advertiser']['Banner']['filename'] = $someone['Banner']['filename'];
            	$this->Session->write('Advertiser', $someone['Advertiser']);
            	$this->Session->delete('Advertiser.password');
            	
            	$this->Session->write('show_warning', '1');
            	
            	//check to make sure they have changed password from default
            	if($someone['Advertiser']['is_password_changed'] == '0') {
            		$this->Session->setFlash(__('Welcome back '.$someone['Advertiser']['name'].'. To get started, please update your password.', true));
                	$this->redirect('edit');
            	} else if($someone['Advertiser']['is_billing_profile_set'] == '0') {
            		//check to make sure they have a billing profile set up
            		$this->Session->setFlash(__('Welcome back '.$someone['Advertiser']['name'].'. Please set up a Billing Profile to start accepting orders.', true));
                	$this->redirect('payment_profiles');
            	} else {
            		//prereqs met, forward to dashboard
            		$this->Session->setFlash(__('Welcome back '.$someone['Advertiser']['name'].'.', true));
                	$this->redirect('dashboard');
            	}
            } else {
            	$this->Session->setFlash(__('Invalid Username/Password. Please try again.', true));
            }
        }
        
        //featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Advertiser->Location->getFeatured($lat, $long));
    }

    function admin_login($loc_slug = null) {
    	$this->checkAdminSession();
    	if(!$loc_slug) { $this->redirect($this->referer());	}
    	
    	$loc = $this->Advertiser->Location->findBySlug($loc_slug);
    	if($loc) {
    		$loc['Advertiser']['Banner']['filename'] = $loc['Banner']['filename'];
    		$this->Session->write('Advertiser', $loc['Advertiser']);
    		$this->Session->delete('Advertiser.password');
    		
    		$this->Session->write('show_warning', '1');
    		$this->Session->setFlash(__('Successfully logged in as: '.$loc['Advertiser']['name'].'.', true));
    		$this->redirect('/restaurants/edit_menu/'.$loc['Location']['slug']);
    	} else { $this->redirect($this->referer()); }
    }
    
    function logout() {
        $this->Session->delete('Advertiser');
        $this->Session->write('show_warning', '1');
        $this->Session->setFlash(__('You have successfully logged out.', true));
        $this->redirect('/advertisers/login');
    }  

    function admin_logout() {
    	$this->Session->delete('Advertiser');
    	$this->Session->write('show_warning', '1');
        $this->Session->setFlash(__('You have successfully logged out of the advertiser account.', true));
        $this->redirect($this->referer());
    }
    
	function edit() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		if (!empty($this->data)) {
			//check if logo is being uploaded and handle it if necessary
			if (!empty($this->data['Advertiser']['logo']['tmp_name'])) {
	      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Advertiser']['logo']['name'], "/files/logos/");
	      		$error = $this->FileUpload->handleFileUpload($this->data['Advertiser']['logo'], $fileName, "files/logos/");
			    if(!$error) {
			    	//file upload was ok
			    	$this->data['Advertiser']['logo'] = $fileName;
			    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }	
	    	} else { unset($this->data['Advertiser']['logo']); }
	    	
	    	//only update the password if necessary
	    	$ok_to_update = true;
			if(!empty($this->data['Pass']['pass1']) || !empty($this->data['Pass']['pass2'])) {
				if(empty($this->data['Pass']['pass1']) || empty($this->data['Pass']['pass2'])) {
					$this->Session->setFlash(__('Please fill out both password fields.', true));
					$ok_to_update = false;	
				} else if($this->data['Pass']['pass1'] != $this->data['Pass']['pass2']) {
	    			$this->Session->setFlash(__('New passwords do not match, please try again.', true));
	    			$ok_to_update = false;
	    		} else {
	    			$this->data['Advertiser']['is_password_changed'] = '1';
	    			$this->data['Advertiser']['password'] = md5($this->data['Pass']['pass2']);
	    		}
	    	}
	    	
			if($ok_to_update) {
				$this->Advertiser->id = $this->Session->read('Advertiser.id');
				if ($this->Advertiser->save($this->data)) {
					//gotta update the session vars in case they have changed
					$new_info = $this->Advertiser->read(null, $this->Session->read('Advertiser.id'));
					$new_info['Advertiser']['Banner']['filename'] = $new_info['Banner']['filename'];
					$this->Session->write('Advertiser', $new_info['Advertiser']);
					$this->Session->write('show_warning', '1');
					
					$this->Session->setFlash(__('Account settings successfully updated.', true));
					if($new_info['Advertiser']['is_password_changed'] == '1') {
						if($new_info['Advertiser']['is_billing_profile_set'] == '0') {
							$this->Session->setFlash(__('Account settings successfully updated. Please set up a billing profile.', true));
							$this->redirect(array('action'=>'payment_profiles'));
						} else {
							$this->redirect(array('action'=>'dashboard'));	
						}
					}
				} else {
					$this->Session->setFlash(__('Problem updating account settings. Please, try again.', true));
				}	
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Advertiser->read(null, $this->Session->read('Advertiser.id'));
		}
		$this->set('banners', $this->Advertiser->Banner->find('list', array('fields'=>'filename')));
	}
	
	function admin_dashboard() {
		$this->checkAdminSession();
		
		//right now we are currently manually omitting Doorstep Dining records.
		$this->set('pending_menus', $this->Advertiser->Location->getPendingMenus());
		$this->set('cust_support', true);
		$this->set('unsettled_orders', $this->Advertiser->Location->Order->getUnsettledOrders());
	}
	
	function ajax_admin_comment_response() {
		$this->checkAdminSession();
		if($this->RequestHandler->isAjax()) {
			$success = true;
			if(isset($this->params['form']['id']) && isset($this->params['form']['comment'])) {
				$this->Advertiser->Location->id = $this->params['form']['id'];
				if(!$this->Advertiser->Location->saveField('admin_comments', $this->params['form']['comment'])) $success = false;
			} else { $success = false; }
		} else { $success = false; }
		$this->set('response', true);
	}
	
	function admin_disable($id = null) {
		$this->checkAdminSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Advertiser', true));
			$this->redirect(array('action'=>'index'));
		}
		if(empty($this->data)) {
			$this->data = $this->Advertiser->read(null, $id);
		} 
		
		if($this->data['Advertiser']['active'] == 1) {
			$this->data['Advertiser']['active'] = 0;
			$active_text = "disabled!";
		} else {
			$this->data['Advertiser']['active'] = 1;
			$active_text = "enabled!";
		}
		if ($this->Advertiser->saveField('active', $this->data['Advertiser']['active'])) {
			$this->Session->setFlash(__('The Advertiser has been successfully '.$active_text, true));
		} else {
			$this->Session->setFlash(__('The Advertiser could not be '.$active_text.'. Please, try again.', true));
		}
		$this->redirect(array('action'=>'index'));
	}


	function admin_index() {
		$this->checkAdminSession();
		$this->Advertiser->recursive = 1;
		$this->set('advertisers', $this->paginate());
	}

	function admin_view($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid Advertiser.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('advertiser', $this->Advertiser->read(null, $id));
	}

	function admin_add() {
		$this->checkAdminSession();
		if (!empty($this->data)) {
			$this->Advertiser->create();
			$this->data['Advertiser']['date_registered'] = date("Y-m-d H:i:s");
			
			$err = false;
	    	if (!empty($this->data['Advertiser']['logo']['tmp_name'])) {
	      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Advertiser']['logo']['name'], "/files/logos/");
			    $error = $this->FileUpload->handleFileUpload($this->data['Advertiser']['logo'], $fileName, "logos/");
			    if(!$error) {
			    	//file upload was ok
			    	$this->data['Advertiser']['logo'] = $fileName;
			    } else {
			    	//Something failed. Remove the image uploaded if any.
			        $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName);
			        $this->set('error', $error);
			        $this->set('data', $this->data);
			        $this->validateErrors($this->Advertiser);
			        $this->render();
			    }	
	    	} else {
	    		$this->data['Advertiser']['logo'] = "";
	    	}
			    
	    	if ($this->Advertiser->save($this->data)) {
        		$this->Session->setFlash(__('The Business has been saved', true));
        		$this->redirect(array('action'=>'index'));
      		} else {
        		$this->Session->setFlash(__('The Business could not be saved', true));
      		}
		}
	}

	function admin_edit($id = null) {
		$this->checkAdminSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Advertiser', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$err = false;
	    	if (!empty($this->data['Advertiser']['logo']['tmp_name'])) {
	      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Advertiser']['logo']['name'], "/files/logos/");
			    $error = $this->FileUpload->handleFileUpload($this->data['Advertiser']['logo'], $fileName, "logos/");
			    if(!$error) {
			    	//file upload was ok
			    	$this->data['Advertiser']['logo'] = $fileName;
			    	$this->Advertiser->saveField('logo',$this->data['Advertiser']['logo']);
			    } else {
			    	//Something failed. Remove the image uploaded if any.
			        $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName);
			        $this->set('error', $error);
			        $this->set('data', $this->data);
			        $this->validateErrors($this->Advertiser);
			        $this->render();
			    }	
	    	}
			    
	    	$this->Advertiser->id = $id;
	    	if ($this->Advertiser->save($this->data, true, "name, contact_fname, contact_lname, contact_title, contact_phone, active")) {
        		$this->Session->setFlash(__('The Business has been saved', true));
        		$this->redirect(array('action'=>'index'));
      		} else {
        		$this->Session->setFlash(__('The Business could not be saved', true));
      		}
		}
		if (empty($this->data)) {
			$this->data = $this->Advertiser->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Advertiser', true));
			$this->redirect(array('action'=>'index'));
		} else {
			//unlink the logo if necessary
			$this->set('advertiser', $this->Advertiser->read(null, $id));
			if($this->data['Advertiser']['logo'] != "") {
				$this->FileUpload->deleteMovedFile($this->data['Advertiser']['logo']);
			}
			
			if ($this->Advertiser->delete($id)) {
				$this->Session->setFlash(__('Advertiser deleted', true));
				$this->redirect(array('action'=>'index'));
			}
		}
	}

}
?>