<?php
class CombosTextItemsController extends AppController {

	var $name = 'CombosTextItems';
	//var $helpers = array('Text');
	
	function add() {
		if(empty($this->data['CombosTextItem']['combo_id'])) {
			$this->redirect($this->referer());
		}
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$this->data['CombosTextItem']['combo_id']));
		
		$this->CombosTextItem->create();
		if($this->CombosTextItem->save($this->data)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('The Menu Item has been successfully added to your combo.', true));
		} else {
			$this->Session->setFlash(__('There was a problem adding the menu item to your combo. Please try again.', true));
		}
		$this->redirect('/combos/edit/'.$this->data['CombosTextItem']['combo_id']);
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid item id.', true));
			$this->redirect($this->referer());
		}
		//get combo id
		$item = $this->CombosTextItem->read(array('combo_id'), $id);
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$item['CombosTextItem']['combo_id']));
		
		if($this->CombosTextItem->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('The Menu Item has been successfully removed from your combo.', true));
		} else {
			$this->Session->setFlash(__('There was a problem removing the menu item from your combo. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function ajax_text_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$this->set('combo_id', $combo_id);	
			}
		}
	}
}
?>