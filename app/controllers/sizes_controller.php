<?php
class SizesController extends AppController {

	var $name = 'Sizes';
	var $helpers = array('Html', 'Form');
	
	function add($location_id = null) {
		if(!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $location_id = $this->data['Size']['location_id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->set('location_id', $location_id);
		
		if(!empty($this->data)) {
			$this->Size->set($this->data);
			if($this->Size->validates()) {
				$this->Size->create();
				//populate extra cheese & sauce fields if they have been left blank
				if(empty($this->data['Size']['extra_cheese'])) $this->data['Size']['extra_cheese'] = $this->data['Size']['topping_price'];
				if(empty($this->data['Size']['extra_sauce'])) $this->data['Size']['extra_sauce'] = $this->data['Size']['topping_price'];
				
				if($this->Size->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Your new pizza size has been added successfully.', true));
				} else {
					$this->Session->setFlash(__('There was a problem adding your pizza size. Please try again.', true));	
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
			
			$this->redirect('/locations/edit_menu/'.$this->Size->Location->getSlugFromId($location_id));
		} 
	}
	
	function edit() {
		if(empty($this->params['form']['id']) && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(empty($this->data)) {
			$size = $this->Size->read(null, $this->params['form']['id']);
			$this->data['Size']['location_id'] = $size['Size']['location_id'];
		}
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['Size']['location_id']));
		
		if(!empty($this->data['Size']['id'])) {
			$this->Size->set($this->data);
			if($this->Size->validates()) {
				if($this->Size->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Your pizza size has been updated successfully.', true));
				} else {
					$this->Session->setFlash(__('There was a problem updating your pizza size. Please try again.', true));	
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} 
			$this->redirect('/restaurants/edit_menu/'.$this->Size->Location->getSlugFromId($this->data['Size']['location_id']));
		}
		if(empty($this->data['Size']['id'])) $this->data = $size;
	
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid pizza size record.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$size = $this->Size->read('location_id', $id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$size['Size']['location_id']));
		
		if($this->Size->delete($id)) {
			//now delete any associated specials sizes records.
			if($this->Size->SpecialsSize->deleteAll(array('SpecialsSize.size_id' => $id))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your pizza size has been deleted successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem deleting your pizza size. Please try again.', true));
			}
		} else {
			$this->Session->setFlash(__('There was a problem deleting your pizza size. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	
	/*
	function manage($menuid = null) {
		$params = $menuid ? array('type'=>'menu','value'=>$menuid) : array();
		$this->checkAdvertiserSession($params);
		$this->layout = "restaurant";
		
		if(!empty($this->data)) {
			$this->Size->set($this->data);
			if($this->Size->validates()) {
				//create an array of all location ids we are going to update toppings for
				$locations_ids = $this->getLocationIds();
				
				foreach($locations_ids as $locations_id):
					//add the size for each location id in the array
					$this->data['Size']['location_id'] = $locations_id;
					$this->Size->save($this->data);
					//since we are saving mulitple records in succession we need to reset the size model, or it will update the last record inserted
					//instead of inserting a new one
					$this->Size->id = false;
				endforeach;
				
				$this->Session->setFlash(__('Your new size has been added.', true));
			} 
		}
		
		if($this->Session->read('Advertiser.universal_sizes') == 1) {
			$loc = $this->Size->Location->find('first', array('conditions'=>array('Location.advertiser_id'=>$this->Session->read('Advertiser.id'))));
			$location_id = $loc['Location']['id'];
		} else { $location_id = $menuid; }
		$this->set('sizes', $this->Size->find('all', array('conditions' => array('Size.location_id'=>$location_id),
														   'order'      => 'Size.size')));
	}
	
	function getLocationIds() {
		$locations_ids = array();
		
		//find out if we are managing all menus at once...
		if($this->Session->read('Advertiser.universal_sizes') == 1) {
			//we need to manage records for all locations	
			$locations = $this->Size->Location->find('all', array('conditions'=>array('Location.advertiser_id'=>$this->Session->read('Advertiser.id'))));
			foreach ($locations as $location):
				$locations_ids[] = $location['Location']['id'];
			endforeach;
		} else {
			//managing an individual location
			$locations_ids[] = $this->data['Location']['id'];
		}
		return $locations_ids;
	}
	
	function delete($size) {
		$this->checkAdvertiserSession();
		
		//create array of location ids we are deleting the size for
		$locations_ids = $this->getLocationIds();
		
		//delete appropriate records
		foreach($locations_ids as $location_id):
			$this->Size->deleteAll(array('Size.location_id' => $location_id,
										 'Size.size'        => $size));
		endforeach;
		//query for sizes to pass to ajax render
		$this->set('sizes', $this->Size->find('all', array('conditions' => array('Size.location_id'=>$locations_ids[0]),
														   'order'      => 'Size.size')));
		$this->render('ajax_delete','ajax');
	}
	*/

	function admin_index() {
		$this->Size->recursive = 0;
		$this->set('sizes', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Size.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('size', $this->Size->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Size->create();
			if ($this->Size->save($this->data)) {
				$this->Session->setFlash(__('The Size has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Size could not be saved. Please, try again.', true));
			}
		}
		$specials = $this->Size->Special->find('list');
		$locations = $this->Size->Location->find('list');
		$this->set(compact('specials', 'locations'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Size', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Size->save($this->data)) {
				$this->Session->setFlash(__('The Size has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Size could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Size->read(null, $id);
		}
		$specials = $this->Size->Special->find('list');
		$locations = $this->Size->Location->find('list');
		$this->set(compact('specials','locations'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Size', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Size->delete($id)) {
			$this->Session->setFlash(__('Size deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>