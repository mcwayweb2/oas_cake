<?php
class OrdersCombosController extends AppController {

	var $name = 'OrdersCombos';
	
	function verifyIdentity($combo_id) {
		$combo = $this->OrdersCombo->read('order_id', $combo_id);
		return ($combo['OrdersCombo']['order_id'] == $this->Session->read('User.order_id')) ? true : false;
	}

	function add($combo_id = null) {
		$this->checkUserSession();
		if(!$combo_id && empty($this->data)) {
			$this->Session->setFlash(__('Error adding combo. Invalid combo.', true));
			$this->redirect('/locations/menu/'.$this->Session->read('User.location_id'));
		}
		
		if(!empty($this->data)) {  
			//start by creating the orders combo record
			$this->OrdersCombo->create();
			$combo = $this->OrdersCombo->Combo->read('price', $this->data['OrdersCombo']['combo_id']);
			$this->data['OrdersCombo']['price'] = $combo['Combo']['price'];
			$this->data['OrdersCombo']['total'] = $this->data['OrdersCombo']['price'] * $this->data['OrdersCombo']['qty']; 
			
			if($this->OrdersCombo->save($this->data)) { 
				//combo added to order, now update all combo records with user feedback...
				$opt_total = 0;
				$success = true;
				//orders items??
				if(isset($this->data['CombosItem'])) {
					foreach($this->data['CombosItem'] as $combos_item_id => $items):
						//add record for each combo item
						foreach($items as $array_index => $i):
							//make sure that an item was chosen
							if(empty($i['item_id'])) {
								$success = false;
								$msg = 'Please make choices for all the combo options and try again.';
								break 2;	
							}
						
							$this->OrdersCombo->OrdersCombosItem->create();
							if($this->OrdersCombo->OrdersCombosItem->save(array('OrdersCombosItem' => array('orders_combo_id' => $this->OrdersCombo->getLastInsertId(),
																											'item_id'		  => $i['item_id'],
																											'comments'		  => $i['comments'])))) {
								//are there any item options to take care of?
								if(isset($i['ItemsOption'])) {
									foreach($i['ItemsOption'] as $add_group_id => $opts):
										if($add_group_id > 0) {
											// selection series
											foreach($opts as $add_group_index => $option_id):
												//check to see if this option has a price adjustment
												$item_opt = $this->OrdersCombo->OrdersCombosItem->ItemsOption->read('price', $option_id);
												$opt_total += $item_opt['ItemsOption']['price'];
											
												$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsItemsOption->create();
												$save_array = array('OrdersCombosItemsItemsOption' => array('orders_combos_item_id' => $this->OrdersCombo->OrdersCombosItem->getLastInsertId(),
																											'items_option_id' 		=> $option_id,
																											'price'           		=> $item_opt['ItemsOption']['price']));
												if(!$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsItemsOption->save($save_array)) { 
													$success = false;
												}
											endforeach;
										} else {
											// additional add-on options
											foreach($opts as $option_id => $val):
												if($val == '1') {
													//check to see if this option has a price adjustment
													$item_opt = $this->OrdersCombo->OrdersCombosItem->ItemsOption->read('price', $option_id);
													$opt_total += $item_opt['ItemsOption']['price'];

													$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsItemsOption->create();
													$save_array = array('OrdersCombosItemsItemsOption' => array('orders_combos_item_id' => $this->OrdersCombo->OrdersCombosItem->getLastInsertId(),
																												'items_option_id' 		=> $option_id,
																												'price'           		=> $item_opt['ItemsOption']['price']));
													if(!$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsItemsOption->save($save_array)) { 
														$success = false;
													}
												}
											endforeach;
										}
									endforeach;
								}
								
								//are there any cat options to take care of?
								if(isset($i['CatsOption'])) {
									foreach($i['CatsOption'] as $add_group_id => $opts):
										if($add_group_id > 0) {
											// selection series
											foreach($opts as $add_group_index => $option_id):
												//check to see if this cat option has a price adjustment
												$cat_opt = $this->OrdersCombo->OrdersCombosItem->LocationsCatsOption->read('additional_price', $option_id);
												$opt_total += $cat_opt['LocationsCatsOption']['additional_price'];
											
												$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsLocationsCatsOption->create();
												
												$save_array = array('OrdersCombosItemsLocationsCatsOption' => array('orders_combos_item_id'    => $this->OrdersCombo->OrdersCombosItem->getLastInsertId(),
																											  		'locations_cats_option_id' => $option_id,
																											  		'price'					   => $cat_opt['LocationsCatsOption']['additional_price']));
												if(!$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsLocationsCatsOption->save($save_array)) { 
													$success = false;
												}
											endforeach;
										} else {
											// additional add-on options
											foreach($opts as $option_id => $val):
												if($val == '1') {
													//check to see if this option has a price adjustment
													$cat_opt = $this->OrdersCombo->OrdersCombosItem->LocationsCatsOption->read('additional_price', $option_id);	
													$opt_total += $cat_opt['LocationsCatsOption']['additional_price'];
													$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsLocationsCatsOption->create();
													$save_array = array('OrdersCombosItemsLocationsCatsOption' => array('orders_combos_item_id'	   => $this->OrdersCombo->OrdersCombosItem->getLastInsertId(),
																												  		'locations_cats_option_id' => $option_id,
																												  		'price'					   => $cat_opt['LocationsCatsOption']['additional_price']));
													if(!$this->OrdersCombo->OrdersCombosItem->OrdersCombosItemsLocationsCatsOption->save($save_array)) { 
														$success = false;
													}
												}
											endforeach;
										}
									endforeach;
								}
							} else { $success = false; }
						endforeach;
					endforeach;
				} //end if Combo Items...
				
				if(isset($this->data['CombosPizza']) && $success) {
					foreach($this->data['CombosPizza'] as $combos_pizza_id => $pizzas):
						foreach($pizzas as $array_index => $p):
							$this->OrdersCombo->OrdersCombosPizza->create();
							$xtras_total = 0;
							
							$save_array = array('OrdersCombosPizza' => array('orders_combo_id'    => $this->OrdersCombo->getLastInsertId(),
																			 'combos_pizza_id'    => $combos_pizza_id,
																			 'locations_crust_id' => $p['locations_crust_id'],
																			 'comments'           => $p['comments'],
																			 'sauce'              => $p['sauce'],
																			 'cheese'			  => $p['cheese']));
							
							$combos_pizza = $this->OrdersCombo->OrdersCombosPizza->CombosPizza->read('size_id', $combos_pizza_id);
							$size = $this->OrdersCombo->OrdersCombosPizza->CombosPizza->Size->read(array('base_price', 'topping_price', 'extra_cheese', 'extra_sauce'), 
															 						  			   $combos_pizza['CombosPizza']['size_id']);
							
							$crust = $this->OrdersCombo->OrdersCombosPizza->LocationsCrust->read('price', $p['locations_crust_id']);
							$save_array['OrdersCombosPizza']['crust_price'] = $crust['LocationsCrust']['price'];
							$xtras_total += $save_array['OrdersCombosPizza']['crust_price'];
							
							if($p['sauce'] == 'Extra Sauce') { 
								$xtras_total += $size['Size']['extra_sauce'];
								$save_array['OrdersCombosPizza']['sauce_price'] = $size['Size']['extra_sauce']; 
							}
							if($p['cheese'] == 'Extra Cheese') { 
								$xtras_total += $size['Size']['extra_cheese'];
								$save_array['OrdersCombosPizza']['cheese_price'] = $size['Size']['extra_cheese']; 
							}
							
							if($this->OrdersCombo->OrdersCombosPizza->save($save_array)) {
								//now add any topping records
								$topping_price_adj = 0;
								
								//add any records for toppings that are already included
								$included_toppings = $this->OrdersCombo->OrdersCombosPizza->CombosPizza->CombosPizzasTopping->find('all', array('conditions' => array('CombosPizzasTopping.combos_pizza_id' => $combos_pizza_id),
																																				'recursive'  => -1,
																																				'fields'	 => array('topping_id', 'placement')));
								if(sizeof($included_toppings) > 0) {
									foreach($included_toppings as $t):
										$this->OrdersCombo->OrdersCombosPizza->OrdersCombosPizzasTopping->create();
									
										$save_array = array('OrdersCombosPizzasTopping' => array('orders_combos_pizza_id' => $this->OrdersCombo->OrdersCombosPizza->getLastInsertId(),
																								 'topping_id'             => $t['CombosPizzasTopping']['topping_id'],
																								 'placement'			  => $t['CombosPizzasTopping']['placement']));
										if(!$this->OrdersCombo->OrdersCombosPizza->OrdersCombosPizzasTopping->save($save_array)) $success = false;
									endforeach;
								}
								
								if(isset($p['toppings'])) {
									foreach($p['toppings'] as $index => $loc_topping_id):
										//make sure a topping was chosen
										if($loc_topping_id) {
											//check to see if this topping has a price alteration
											$t = $this->OrdersCombo->Combo->Location->LocationsTopping->read(array('price_alter', 'topping_id'), $loc_topping_id);
											
											$save_array = array('OrdersCombosPizzasTopping' => array('orders_combos_pizza_id' => $this->OrdersCombo->OrdersCombosPizza->getLastInsertId(),
																									 'topping_id'             => $t['LocationsTopping']['topping_id']));
											if($t['LocationsTopping']['price_alter'] > 0) {
												$topping_price_adj += $t['LocationsTopping']['price_alter'];
												$save_array['OrdersCombosPizzasTopping']['price'] = $t['LocationsTopping']['price_alter'];
											} else { $save_array['OrdersCombosPizzasTopping']['price'] = 0; }
										
											$this->OrdersCombo->OrdersCombosPizza->OrdersCombosPizzasTopping->create();
											if(!$this->OrdersCombo->OrdersCombosPizza->OrdersCombosPizzasTopping->save($save_array)) $success = false;	
										}
									endforeach;
								}
								//calc and store total topping price, and total price adjustment
								$this->OrdersCombo->OrdersCombosPizza->id = $this->OrdersCombo->OrdersCombosPizza->getLastInsertId();
								if(!$this->OrdersCombo->OrdersCombosPizza->saveField('topping_price', $topping_price_adj)) $success = false;
								$tot_adj = $xtras_total + $topping_price_adj;
								if(!$this->OrdersCombo->OrdersCombosPizza->saveField('total_price_adj', $tot_adj)) $success = false;
								$opt_total += $tot_adj;
							} else { 
								$success = false; 
								break 2;
							}
						endforeach;
					endforeach;
				} //end if combo pizzas
				
				if(isset($this->data['CombosSpecialsSize']) && $success) {
					foreach($this->data['CombosSpecialsSize'] as $combos_specials_size_id => $pizzas):
						foreach($pizzas as $array_index => $p):
							$this->OrdersCombo->OrdersCombosSpecialsSize->create();
							$xtras_total = 0;
							
							$save_array = array('OrdersCombosSpecialsSize' => array('orders_combo_id'    => $this->OrdersCombo->getLastInsertId(),
																			 		'specials_size_id'   => $p['specials_size_id'],
																			 		'locations_crust_id' => $p['locations_crust_id'],
																			 		'comments'           => $p['comments'],
																			 		'sauce'              => $p['sauce'],
																			 		'cheese'			 => $p['cheese']));
							
							$specials_size = $this->OrdersCombo->SpecialsSize->read('size_id', $p['specials_size_id']);
							$size = $this->OrdersCombo->SpecialsSize->Size->read(array('base_price', 'topping_price', 'extra_cheese', 'extra_sauce'), 
															 					 $specials_size['SpecialsSize']['size_id']);
							$crust = $this->OrdersCombo->OrdersCombosPizza->LocationsCrust->read('price', $p['locations_crust_id']);

							$save_array['OrdersCombosSpecialsSize']['crust_price'] = $crust['LocationsCrust']['price'];
							$xtras_total += $save_array['OrdersCombosSpecialsSize']['crust_price'];
							
							if($p['sauce'] == 'Extra Sauce') { 
								$xtras_total += $size['Size']['extra_sauce'];
								$save_array['OrdersCombosSpecialsSize']['sauce_price'] = $size['Size']['extra_sauce']; 
							}
							if($p['cheese'] == 'Extra Cheese') { 
								$xtras_total += $size['Size']['extra_cheese'];
								$save_array['OrdersCombosSpecialsSize']['cheese_price'] = $size['Size']['extra_cheese']; 
							}
							$save_array['OrdersCombosSpecialsSize']['total_price_adj'] = $xtras_total;
							
							if(!$this->OrdersCombo->OrdersCombosSpecialsSize->save($save_array)) {
								$success = false; 
								break 2;
							}
							$opt_total += $xtras_total;
						endforeach;
					endforeach;
				} //end if combo specialty pizzas
				
				if(isset($this->data['CombosTextItem']) && $success) {
					foreach($this->data['CombosTextItem'] as $combos_text_item_id => $item):
						$this->OrdersCombo->OrdersCombosTextItem->create();
						$save_array = array('OrdersCombosTextItem' => array('orders_combo_id'    	=> $this->OrdersCombo->getLastInsertId(),
																		 	'combos_text_item_id'   => $combos_text_item_id,
																		 	'comments'           	=> $item['comments']));
						if(!$this->OrdersCombo->OrdersCombosTextItem->save($save_array)) {
							$success = false; 
							break;
						}
					endforeach;
				} // end if text items
				
				if($success) { //if everything is still successful
					//update the combo price with any additional price costs if necessary
					if($opt_total > 0) {
						$this->OrdersCombo->id = $this->OrdersCombo->getLastInsertId();
						$this->OrdersCombo->saveField('additional_opts_price', $opt_total);
						$this->data['OrdersCombo']['total'] += ($opt_total * $this->data['OrdersCombo']['qty']);
						$this->OrdersCombo->saveField('total', $this->data['OrdersCombo']['total']);
					}
					
					if($this->OrdersCombo->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersCombo']['total'])) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Combo added to your order.', true));	
					} else {
						$this->Session->setFlash(__('There was a problem adding the combo to your order. Please try again', true));
					}
				} else {
					//otherwise remove the newly added record, and try again
					$this->OrdersCombo->delete($this->OrdersCombo->getLastInsertId());
					$this->Session->setFlash(__((isset($msg) ? $msg : 'There was a problem adding the combo to your order. Please try again'), true));
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding the combo to your order. Please try again', true));
			}
			$this->redirect('/menu/'.$this->OrdersCombo->Order->Location->getUrlSlugs($this->Session->read('User.location_id')));
			
		}
		
		$this->set('combo', $this->OrdersCombo->Combo->getComboInfo($combo_id));
		$this->set('crusts', $this->OrdersCombo->Order->Location->getCrustList($this->Session->read('User.location_id')));
		$this->set('toppings_list', $this->OrdersCombo->Order->Location->getToppingList($this->Session->read('User.location_id')));
	}
	
	/*
	function edit($orders_item_id = null) {
		//Configure::write('debug', 0);
		$this->checkUserSession();
		if(!$orders_item_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid menu item.', true));
			$this->redirect('/locations/menu/'.$this->Session->read('User.location_id'));
		}
		
		if(!empty($this->data)) {
			$item = $this->OrdersItem->Item->read('price', $this->data['OrdersItem']['item_id']);
			$this->data['OrdersItem']['price'] = $item['Item']['price'];
			$old_total = $this->data['OrdersItem']['total'] * -1;
			$this->data['OrdersItem']['total'] = $this->data['OrdersItem']['qty'] * $this->data['OrdersItem']['price'];
			
			if($this->OrdersItem->save($this->data)) {
				//subtract the old total before adding the new total
				$this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $old_total);
				
				if($this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersItem']['total'])) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Order item has been updated.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem editing your order item. Please try again', true));
				}
			} else {
				$this->Session->setFlash(__('There was a problem editing your order item. Please try again', true));
			}
			$this->redirect('/locations/order_review');
		} else {
			$this->data = $this->OrdersItem->read(null, $orders_item_id);
		}
		$this->set('item', $this->OrdersItem->Item->read(null, $this->data['OrdersItem']['item_id']));
	}
	*/
	function delete($id = null) {
		$this->checkUserSession();
		if(!$id) {
			$this->Session->setFlash(__('Invalid order combo. Please try again.', true));
			$this->redirect($this->referer());
		}
		//get order id for validation
		$o = $this->OrdersCombo->read(array('order_id', 'total'), $id);
		
		//make sure this is part of the current user order combo
		if($this->Session->read('User.order_id') == $o['OrdersCombo']['order_id']) {
			if($this->OrdersCombo->delete($id)) {
				if($this->OrdersCombo->Order->updateOrderTotals($this->Session->read('User.order_id'), $o['OrdersCombo']['total'] * -1)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('The combo has been removed from your order.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem removing your order combo. Please try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('There was a problem removing your order combo. Please try again.', true));	
			}	
		} else {
			$this->Session->setFlash(__('This combo is not part of your current order.', true));
		}
		$this->redirect($this->referer());
	}
}
?>