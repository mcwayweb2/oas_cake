<?php
class LocationsToppingsController extends AppController {

	var $name = 'LocationsToppings';
	
	//add a new crust type to the selectable list of crusts
	function add() {
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['LocationsTopping']['location_id']));
		
		if(!empty($this->data)) {
			//check to make sure there is not already a record for this toppings
			$topping = $this->LocationsTopping->find('first', array('conditions' => array('LocationsTopping.location_id' => $this->data['LocationsTopping']['location_id'],
																					      'LocationsTopping.topping_id'  => $this->data['LocationsTopping']['topping_id']),
																    'recursive'  => '-1'));
			if($topping) {
				$this->Session->setFlash(__('You are already using this topping.', true));
			} else {
				$this->LocationsTopping->create();
				if(empty($this->data['LocationsTopping']['price_alter'])) $this->data['LocationsTopping']['price_alter'] = 0;
				
				if($this->LocationsTopping->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully added your new topping.', true));
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a topping.', true));
				}	
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a topping.', true));
		}
		$this->redirect($this->referer().'#settingToppings');
	}
	/*
	function multiple_add($location_id = null) {
		if(!empty($this->data)) {
			$success_ct = 0;
			$failure_ct = 0;
			$error_ct = 0;
			
			//loop through each item to add
			foreach($this->data['cats'] as $cat):
				if(!empty($cat['cat_id'])) {
					$success = true;
					//is this a new category or an existing one?
					if(!is_numeric($cat['cat_id'])) {
						//new cat to add, make sure this cat is not already in system
						$c = $this->LocationsCat->Cat->findByTitle($cat['cat_id']);
						if(!$c) {
							$this->LocationsCat->Cat->create();
							if(!$this->LocationsCat->Cat->save(array('Cat' => array('title' => ucwords($cat['cat_id']))))) $success = false;
							$cat_id = $this->LocationsCat->Cat->getLastInsertId();	
						} else { $cat_id = $c['Cat']['id']; }
					} else { $cat_id = $cat['cat_id']; } 
					
					if($success) {
						$this->LocationsCat->create();
						if($this->LocationsCat->save(array('LocationsCat' => array('cat_id'      => $cat_id,
																				   'location_id' => $location_id,
												  	  					   		   'subtext'     => $cat['subtext'])))) $success_ct++;				
					}			
				}
			endforeach;
			
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('('.$success_ct.') Menu Categories successfully added.', true));
			
			$l = $this->LocationsCat->Location->read('slug', $location_id);
			$this->redirect('/restaurants/edit_menu/'.$l['Location']['slug'].'#mnuCategories');
		}
		//need to create an array of locations, with index numbers that correspond with indexes in ad_locations array
		$this->set('current_loc_index', $this->LocationsCat->Location->getArrayIndex($location_id));
		$this->set('location', $this->LocationsCat->Location->getFieldsForBanner($location_id));
	}
	*/
	
	function mass_add() {
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['LocationsTopping']['location_id']));
		if(!empty($this->data)) {
			//check each form element to see if they have chosen a topping
			$overall_success = true;
			$count = 0;
			foreach($this->data['toppings'] as $i => $topping_id):
				if(!empty($topping_id)) {
					$success = true;
					//is this a new category or an existing one?
					if(!is_numeric($topping_id)) {
						//new topping to add, make sure this topping is not already in system
						$t = $this->LocationsTopping->Topping->findByTitle($topping_id);
						if(!$t) {
							$this->LocationsTopping->Topping->create();
							if(!$this->LocationsTopping->Topping->save(array('Topping' => array('title' => ucwords($topping_id))))) $success = false;
							$topping_id = $this->LocationsTopping->Topping->getLastInsertId();	
						} else { $topping_id = $t['Topping']['id']; }
					} 
					
					if($success) {
						//only save the new topping if it is not already in use
						$find_topping = $this->LocationsTopping->find('first', array('conditions' => array('LocationsTopping.location_id' => $this->data['LocationsTopping']['location_id'],
																										   'LocationsTopping.topping_id'  => $topping_id)));
						if(!$find_topping) {
							$save = array('LocationsTopping' => array('location_id' => $this->data['LocationsTopping']['location_id'],
																	  'topping_id'  => $topping_id,
																	  'topping_type_id' => $this->data['topping_types'][$i]));
							$this->LocationsTopping->create();
							if($this->LocationsTopping->save($save)) { $count++; } else { $overall_success = false; }	
						}
					} 
				}
			endforeach;
			
			if($overall_success) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('You have successfully added ('.$count.') new toppings.', true));
			} else {
				$this->Session->setFlash(__('There was a problem adding some of your new toppings. Please try again.', true));
			}
		}
		$this->redirect($this->referer());
	}
	
	function edit() {
		if(empty($this->data)) {
			$topping = $this->LocationsTopping->read(array('id', 'location_id', 'price_alter', 'topping_id', 'topping_type_id'), $this->params['form']['id']);
			$loc_id = $topping['LocationsTopping']['location_id'];
		} else { $loc_id = $this->data['LocationsTopping']['location_id']; }
		
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$loc_id));
		
		if(!empty($this->data)) {
			if(empty($this->data['LocationsTopping']['price_alter'])) $this->data['LocationsTopping']['price_alter'] = 0;
			
			if($this->LocationsTopping->save($this->data)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('You have successfully updated your topping.', true));
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields for the new topping.', true));
			}
			$this->redirect($this->referer().'#settingToppings');	
		}
		if(empty($this->data)) { $this->data = $topping; }
		$this->set('topping_list', $this->LocationsTopping->Topping->getToppingList());
		$this->set('topping_types', $this->LocationsTopping->ToppingType->find('list'));
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid topping.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$topping = $this->LocationsTopping->read('location_id', $id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$topping['LocationsTopping']['location_id']));
		
		if($this->LocationsTopping->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your topping has been deleted successfully.', true));
		} else {
			$this->Session->setFlash(__('There was a problem deleting your topping. Please try again.', true));
		}
		$this->redirect($this->referer().'#settingToppings');
	}
}
?>