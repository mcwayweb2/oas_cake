<?php
class LocationsCrustsController extends AppController {

	var $name = 'LocationsCrusts';
	
	//add a new crust type to the selectable list of crusts
	function add() {
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['LocationsCrust']['location_id']));
		
		if(!empty($this->data)) {
			//check to make sure there is not already a record for this crust type
			$crust = $this->LocationsCrust->find('first', array('conditions' => array('LocationsCrust.location_id' => $this->data['LocationsCrust']['location_id'],
																					  'LocationsCrust.crust_id'    => $this->data['LocationsCrust']['crust_id']),
																'recursive'  => '-1'));
			if($crust) {
				$this->Session->setFlash(__('You are already using this crust type.', true));
			} else {
				$this->LocationsCrust->create();
				if(empty($this->data['LocationsCrust']['price'])) $this->data['LocationsCrust']['price'] = 0;
				
				if($this->LocationsCrust->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully added your crust type.', true));
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a crust type.', true));
				}	
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a crust type.', true));
		}
		$this->redirect($this->referer().'#settingCrusts');
	}
	
	function edit() {
		if(empty($this->data)) {
			$crust = $this->LocationsCrust->read(array('id', 'location_id', 'price', 'crust_id'), $this->params['form']['id']);
			$loc_id = $crust['LocationsCrust']['location_id'];
		} else { $loc_id = $this->data['LocationsCrust']['location_id']; }
		
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$loc_id));
		
		if(!empty($this->data)) {
			if(empty($this->data['LocationsCrust']['price'])) $this->data['LocationsCrust']['price'] = 0;
			
			if($this->LocationsCrust->save($this->data)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('You have successfully updated your crust type.', true));
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields for the crust type.', true));
			}
			$this->redirect($this->referer().'#settingCrusts');	
		}
		if(empty($this->data)) { $this->data = $crust; }
		$this->set('crust_list', $this->LocationsCrust->Crust->getCrustList());
	}
	
	//we are not actually deleting the special records from the db, but archiving them to keep existing orders intact
	function delete($crust_id = null) {
		if (!$crust_id) {
			$this->Session->setFlash(__('Invalid crust type.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$crust = $this->LocationsCrust->read('location_id', $crust_id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$crust['LocationsCrust']['location_id']));
		
		if($this->LocationsCrust->delete($crust_id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your crust type has been deleted successfully.', true));
		} else {
			$this->Session->setFlash(__('There was a problem deleting your specialty pizza. Please try again.', true));
		}
		$this->redirect($this->referer().'#settingCrusts');
	}
}
?>