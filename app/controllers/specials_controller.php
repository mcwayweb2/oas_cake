<?php
class SpecialsController extends AppController {

	var $name = 'Specials';
	var $helpers = array('Html', 'Form');
	var $components = array('FileUpload');

	function verifyIdentity($special_id) {
		$special = $this->Special->read(array('location_id'), $special_id);
		$location = $this->Special->Location->read('advertiser_id', $special['Special']['location_id']);
		return ($this->Session->read('Advertiser.id') == $location['Location']['advertiser_id']) ? true : false;
	}
	/*
	function manage($menuid = null) {
		$params = $menuid ? array('type'=>'menu','value'=>$menuid) : array();
		$this->checkAdvertiserSession($params);
		$this->layout = "restaurant";
		
		if(!empty($this->data)) {
			$this->Special->set($this->data);
			if($this->Special->validates()) {
				
				$locations_ids = $this->getLocationIds($this->data['Special']['loc_id']); 
				$success = true;
				
				//handle the photo upload if necessary
		    	if (!empty($this->data['Special']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Special']['image']['name'], "/files/specialtyPizzas/");
				    $error = $this->FileUpload->handleFileUpload($this->data['Special']['image'], $fileName, "/specialtyPizzas/");
				    if(!$error) {
				    	$this->data['Special']['image'] = $fileName;
				    } else {
				    	//Something failed. Remove the image uploaded if any.
				        $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName);
				        $this->data['Special']['image'] = "";
				    }	
		    	} else {
		    		$this->data['Special']['image'] = "";
		    	}
				
				foreach($locations_ids as $locations_id):
					//add the size for each location id in the array
					$this->data['Special']['location_id'] = $locations_id;
					
					if(!$this->Special->save($this->data)) {
						$success = false;
					}
					//since we are saving mulitple records in succession we need to reset the size model, or it will update the last record inserted
					//instead of inserting a new one
					$this->Special->id = false;
					$special_id = $this->Special->getLastInsertId();
					
					//now save the concurrent records for the specials prices
					foreach($this->data['Price'] as $size => $price):
						//we now have a location_id, and a size, lookup proper size_id
						$size_info = $this->Special->Size->find('first', array('conditions' => array('Size.location_id' => $locations_id,
																									 'Size.size'        => $size)));
						$save_array = array('special_id' => $special_id,
											'size_id'    => $size_info['Size']['id'],
											'price'      => $price);
						if(!$this->Special->SpecialsSize->save($save_array)) {
							$success = false;	
						}
						$this->Special->SpecialsSize->id = false;
					endforeach;
				endforeach;
				
				$flash_text = $success ? "Your Specialty pizza has been added successfully." : "There was an error while trying to add your specialty pizza. Please try again.";
				$this->Session->setFlash(__($flash_text, true));
			} 
		}
		
		if($this->Session->read('Advertiser.universal_specials') == 1) {
			$loc = $this->Special->Location->find('first', array('conditions'=>array('Location.advertiser_id'=>$this->Session->read('Advertiser.id'))));
			$location_id = $loc['Location']['id'];
		} else { $location_id = $menuid; }
		$this->set('loc_id', $location_id);
		$this->set('specials', $this->Special->find('all', array('conditions' => array('Special.location_id'=>$location_id))));
		
		$this->set('sizes', $this->Special->Size->find('all', array('conditions' => array('Size.location_id'=>$location_id))));
	}
	*/
	
	function getLocationIds($location_id = null) {
		$locations_ids = array();
		
		//find out if we are managing all menus at once...
		if($this->Session->read('Advertiser.universal_specials') == 1) {
			//we need to manage records for all locations	
			$locations = $this->Special->Location->find('all', array('conditions'=>array('Location.advertiser_id'=>$this->Session->read('Advertiser.id'))));
			foreach ($locations as $location):
				$locations_ids[] = $location['Location']['id'];
			endforeach;
		} else {
			//managing an individual location
			$locations_ids[] = $location_id;
		}
		return $locations_ids;
	}
	
	function add($slug = null) {
		if (!$slug && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		
		if($slug) {
			//convert slug to id
			$l = $this->Special->Location->findBySlug($slug);
			if(!$l) {
				//slug not valid
				$this->Session->setFlash(__('Invalid restaurant.', true));
				$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
			} else {
				$location_id = $l['Location']['id'];
			}
		} else {
			//form already been submitted
			$location_id = $this->data['Special']['location_id'];
		}
		 
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			foreach($this->data['sizes'] as $id => $value):
				if($value == 0) unset($this->data['sizes'][$id]); 
			endforeach;
			
			$this->Special->set($this->data);
			if(!$this->Special->validates()) {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} else if(sizeof($this->data['sizes']) <= 0) {
				$this->Session->setFlash(__('Please select at least one size for your specialty pizza.', true));
			} else {
				//see if there is an uplaod to take care of
				if (!empty($this->data['Special']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Special']['image']['name'], "/files/specialtyPizzas/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Special']['image'], $fileName, "files/specialtyPizzas/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Special']['image'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		    	} else { unset($this->data['Special']['image']); }

		    	$this->Special->create();
		    	if($this->Special->save($this->data)) {
		    		//now save the sizes for the special
		    		$success = true;
					foreach($this->data['sizes'] as $id => $value):
						//make sure they have entered a price for the size, if not use the base price for that size
						if(empty($this->data['prices'][$id]) || !is_numeric($this->data['prices'][$id])) {
							$size = $this->Special->Size->read('base_price', $id);
							$price = $size['Size']['base_price'];
						} else { $price = $this->data['prices'][$id]; }
					
						$save = array('SpecialsSize' => array('special_id' => $this->Special->getLastInsertId(),
															  'size_id'    => $id,
															  'price'	   => $price));
						$this->Special->SpecialsSize->create();
						if(!$this->Special->SpecialsSize->save($save)) {
							$success = false;
						}
					endforeach;
		    		
		    		if($success) {
		    			$this->Session->write('show_warning', '1');
		    			$this->Session->setFlash(__('Your specialty pizza has been added successfully.', true));
		    			$this->redirect('/locations/edit_menu/'.$this->Special->Location->getSlugFromId($location_id).'#mnuPizzas');
		    		} else {
		    			$this->Session->setFlash(__('There was a problem adding your specialty pizza. Please try again.', true));
		    		}
		    		
		    	} else {
		    		$this->Session->setFlash(__('There was a problem adding your specialty pizza. Please try again.', true));
		    	}
			}
		}
		$this->set('location', $this->Special->Location->getFieldsForBanner($location_id));
		//get locations sizes
		$this->set('sizes', $this->Special->Location->getSizes($location_id));
	}
	
	function edit($special_id = null) {
		if (!$special_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid specialty pizza.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $special_id = $this->data['Special']['id'];
		$this->checkAdvertiserSession(array('type'=>'special','value'=>$special_id));
		$special = $this->Special->read(array('image', 'location_id'), $special_id);
		$this->set('location', $this->Special->Location->getFieldsForBanner($special['Special']['location_id']));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			foreach($this->data['sizes'] as $id => $value):
				if($value == 0) unset($this->data['sizes'][$id]); 
			endforeach;
			
			//$this->Special->set($this->data);
			 if(sizeof($this->data['sizes']) <= 0) {
				$this->Session->setFlash(__('Please select at least one size for your specialty pizza.', true));
			} else {
				//see if there is an upload to take care of
				if (!empty($this->data['Special']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Special']['image']['name'], "/files/specialtyPizzas/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Special']['image'], $fileName, "files/specialtyPizzas/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Special']['image'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		    	} else { unset($this->data['Special']['image']); }
				
				//$this->Special->set($this->data);
		    	if($this->Special->save($this->data)) {
		    		//now  old size records, save the new sizes for the special
		    		$this->Special->SpecialsSize->deleteAll(array('SpecialsSize.special_id'=>$special_id));
		    		
		    		$success = true;
					foreach($this->data['sizes'] as $id => $value):
						//make sure they have entered a price for the size, if not use the base price for that size
						if(empty($this->data['prices'][$id]) || !is_numeric($this->data['prices'][$id])) {
							$size = $this->Special->Size->read('base_price', $id);
							$price = $size['Size']['base_price'];
						} else { $price = $this->data['prices'][$id]; }
					
						$save = array('SpecialsSize' => array('special_id' => $special_id,
															  'size_id'    => $id,
															  'price'	   => $price));
						$this->Special->SpecialsSize->create();
						if(!$this->Special->SpecialsSize->save($save)) {
							$success = false;
						}
					endforeach;
		    		
		    		if($success) {
		    			$this->Session->write('show_warning', '1');
		    			$this->Session->setFlash(__('Your specialty pizza has been updated successfully.', true));
		    			$this->redirect('/locations/edit_menu/'.$this->Special->Location->getSlugFromId($special['Special']['location_id']).'#mnuPizzas');
		    		} else {
		    			$this->Session->setFlash(__('There was a problem updating your specialty pizza. Please try again.', true));
		    		}
		    		
		    	} else {
		    		$this->Session->setFlash(__('There was a problem updating your specialty pizza. Please try again.', true));
		    	}
			}
		}
		if(empty($this->data)) {
			$this->data = $this->Special->read(null, $special_id);
			unset($this->data['Special']['image']);
		}
		$this->set('special', $special);
		//get locations sizes
		$this->set('sizes', $this->Special->Location->getSizes($special['Special']['location_id']));
		$this->set('current_sizes', $this->Special->SpecialsSize->find('list', array('conditions' => array('SpecialsSize.special_id' => $special_id),
    																   				 'fields'  => array('size_id', 'price'))));
	}
	
	//we are not actually deleting the special records from the db, but archiving them to keep existing orders intact
	function delete($special_id = null) {
		if (!$special_id) {
			$this->Session->setFlash(__('Invalid Special.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'special','value'=>$special_id));
		
		$special = $this->Special->read('image', $special_id);
		if(!empty($special['Special']['image'])) {
			if(file_exists(WWW_ROOT.'files/specialtyPizzas/'.$special['Special']['image'])) {
				//delete image on server if necessary
				unlink(WWW_ROOT.'files/specialtyPizzas/'.$special['Special']['image']);
			}
		}
		
		if($this->Special->del($special_id)) {
			//now archive the sizes
			if($this->Special->SpecialsSize->deleteAll(array('SpecialsSize.special_id' => $special_id))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your specialty pizza has been deleted successfully.', true));	
			} else {
				$this->Session->setFlash(__('There was a problem deleting your specialty pizza. Please try again.', true));
			}
		} else {
			$this->Session->setFlash(__('There was a problem deleting your specialty pizza. Please try again.', true));
		}
		$this->redirect($this->referer());
	}

	function admin_index() {
		$this->Special->recursive = 0;
		$this->set('specials', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Special.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('special', $this->Special->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Special->create();
			if ($this->Special->save($this->data)) {
				$this->Session->setFlash(__('The Special has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Special could not be saved. Please, try again.', true));
			}
		}
		$sizes = $this->Special->Size->find('list');
		$locations = $this->Special->Location->find('list');
		$this->set(compact('sizes', 'locations'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Special', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Special->save($this->data)) {
				$this->Session->setFlash(__('The Special has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Special could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Special->read(null, $id);
		}
		$sizes = $this->Special->Size->find('list');
		$locations = $this->Special->Location->find('list');
		$this->set(compact('sizes','locations'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Special', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Special->del($id)) {
			$this->Session->setFlash(__('Special deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

} 
?>