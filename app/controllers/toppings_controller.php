<?php
class ToppingsController extends AppController {

	var $name = 'Toppings';
	
	function add() {
		$this->checkAdvertiserSession();
		
		if(!empty($this->data)) {
			//make sure this topping has not already been added
			$this->data['Topping']['title'] = ucwords($this->data['Topping']['title']);
			$topping = $this->Topping->findByTitle($this->data['Topping']['title']);
			if($topping) {
				$this->Session->setFlash(__('The topping you are trying to add is already available for use.', true));
			} else {
				$this->Topping->create();
				if($this->Topping->save($this->data)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('You have successfully added a new topping to the database. ', true));
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a topping.', true));
				}	
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a topping.', true));
		}
		$this->redirect($this->referer().'#settingToppings');
	}	

	function admin_index() {
		$this->Topping->recursive = 0;
		$this->set('toppings', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Topping.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('topping', $this->Topping->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Topping->create();
			if ($this->Topping->save($this->data)) {
				$this->Session->setFlash(__('The Topping has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Topping could not be saved. Please, try again.', true));
			}
		}
		$locations = $this->Topping->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Topping', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Topping->save($this->data)) {
				$this->Session->setFlash(__('The Topping has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Topping could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Topping->read(null, $id);
		}
		$locations = $this->Topping->Location->find('list');
		$this->set(compact('locations'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Topping', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Topping->delete($id)) {
			$this->Session->setFlash(__('Topping deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>