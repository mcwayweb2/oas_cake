<?php
class LocationsAdsController extends AppController {

	var $name = 'LocationsAds';
	var $components = array('FileUpload');
	
	function add() {
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$this->data['LocationsAd']['location_id']));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			$path = 'files/ads/'.strtolower($this->data['LocationsAd']['type']).'/';
			$fileName = $this->FileUpload->generateUniqueFilename($this->data['LocationsAd']['image']['name'], "/".$path);
      		$error = $this->FileUpload->handleFileUpload($this->data['LocationsAd']['image'], $fileName, $path);
		    if(!$error) {
		    	//file upload was ok
		    	$this->data['LocationsAd']['image'] = $fileName;
			    $this->LocationsAd->create();
		    	if($this->LocationsAd->save($this->data)) {
		    		$this->Session->write('show_warning', '1');
	    			$this->Session->setFlash(__('Your new advertisement has been uploaded successfully.', true));
		    	} else {
		    		$this->Session->setFlash(__('There was a problem uploading your advertisement. Please try again.', true));
		    	}	
		    } else { 
		    	$this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); 
		    	$this->Session->setFlash(__('There was a problem uploading your advertisement. Please try again.', true));
		    }
		}
		$this->redirect($this->referer().'#tab'.$this->data['LocationsAd']['type']);
	}
	
	function toggle_rotation($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('This advertisement does not exist.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		
		$ad = $this->LocationsAd->read(array('rotation', 'location_id', 'type'), $id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$ad['LocationsAd']['location_id']));
		
		$this->LocationsAd->id = $id;
		$new_val = ($ad['LocationsAd']['rotation'] == '1') ? '0' : '1';
		
		//if we are putting ad IN rotation, make sure we are not at type limit
		if($new_val == '1') {
			switch($ad['LocationsAd']['type']) {
				case "Home": $field = 'home_ads'; break;
				case "Sub":  $field = 'sub_ads';  break;
				case "Btn":  $field = 'btn_ads';  break;
			}
			$ad = $this->LocationsAd->find('count', array('LocationsAd.location_id' => $ad['LocationsAd']['location_id'],
														  'LocationsAd.rotation'    => '1',
														  'LocationsAd.type'        => $ad['LocationsAd']['type']));
			
			$loc = $this->LocationsAd->Location->read($field, $ad['LocationsAd']['location_id']);
			if($ad >= $loc['Location'][$field]) {
				//this ad type at type limit
				$this->Session->setFlash(__('You are already using all of your active subscriptions for this type of advertisement.', true));
				$this->redirect($this->referer());		
			} 
		}
		
		if($this->LocationsAd->saveField('rotation', $new_val)) {
			//toggle rotation value
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your advertisement has been updated successfully.', true));
		} else {
			$this->Session->setFlash(__('There was a problem updating your advertisement. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('This advertisement does not exist.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$ad = $this->LocationsAd->read(array('image', 'location_id', 'type'), $id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$ad['LocationsAd']['location_id']));

		//delete image on server if necessary
		if(!empty($ad['LocationsAd']['image']) && file_exists(WWW_ROOT.'files/ads/'.strtolower($ad['LocationsAd']['type']).'/'.$ad['LocationsAd']['image'])) {
			unlink('files/ads/'.strtolower($ad['LocationsAd']['type']).'/'.$ad['LocationsAd']['image']);
		} 
		
		if($this->LocationsAd->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your Advertisement has been successfully deleted.', true));	
		} else {
			$this->Session->setFlash(__('There was a problem removing your Advertisement. Please try again.', true));	
		}
		$this->redirect($this->referer());
	}
	
}
?>