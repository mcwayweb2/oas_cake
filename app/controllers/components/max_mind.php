<?php 
class MaxMindComponent extends Object {
	var $license = 't3uI1BiMnI7X';
	
	function getIpInfoByCurl() {
		$ch = curl_init("http://geoip3.maxmind.com/f?l=" . $this->license . "&i=" . (AppController::is_site('local')) ? '99.38.109.45' : $_SERVER['REMOTE_ADDR']);
		curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		
		$resp = curl_exec($ch); //execute post and get results
		curl_close ($ch);
		
		$fields = explode(",", $resp);
		return array('state'    => $fields[1],
					 'city'     => $fields[2],
					 'lat'      => $fields[4],
					 'long'     => $fields[5],
					 'areacode' => $fields[7],
					 'ip'       => $_SERVER['REMOTE_ADDR']);
	}
	
	function getIpInfoByDatDb() {
		include(WWW_ROOT . "inc/geoip.inc");
		include(WWW_ROOT . "inc/geoipcity.inc");

		$gi = geoip_open(WWW_ROOT . "csv/GeoLiteCity.dat", GEOIP_STANDARD);
		
		$record = geoip_record_by_addr($gi, (AppController::is_site('local')) ? '99.38.109.45' : $_SERVER['REMOTE_ADDR']);
		
		//CakeLog::write('debug', 'Get info from db: '.print_r($record, true));
		
		geoip_close($gi);
		
		return $record;
	}
}
?>