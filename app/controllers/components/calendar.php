<?php
class CalendarComponent extends Object {
	
	var $month_list = array('january', 'febuary', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');
	var $day_list = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
	var $base_url = '/events/public_calendar'; // NOT not used in the current helper version but used in the data array
	var $view_base_url = '/initech2/events';
	var	$data = null;
	
	function getMonthAndYear($year, $month) {
		if(!$year || !$month) {
			$year = date('Y');
			$month = date('M');
			$month_num = date('n');
			$item = null;
		}
 
		$flag = 0;
 
		for($i = 0; $i < 12; $i++) { // check the month is valid if set
			if(strtolower($month) == $this->month_list[$i]) {
				if(intval($year) != 0) {
					$flag = 1;
					$month_num = $i + 1;
					$month_name = $this->month_list[$i];
					break;
				}
			}
		}
 
		if($flag == 0) { // if no date set, then use the default values
			$year = date('Y');
			$month = date('M');
			$month_name = date('F');
			$month_num = date('m');
		}
		
		return $date_array = array('year'       => $year,
								   'month'      => $month,
								   'month_name' => $month_name,
								   'month_num'  => $month_num);
	}
	
	function getEvents($events) {
		$data = array();
		foreach($events as $v) {
			if(isset($v[0]['event_day'])) {
 
				$day = $v[0]['event_day'];
 
				if(isset($data[$day])) {
					$data[$day] .= '<br /><a href="' . $this->view_base_url . '/view/' . $v['Event']['id'] . '">' . $v['Event']['title'] . '</a>';
				} else {
					$data[$day] = '<a href="' . $this->view_base_url . '/view/' . $v['Event']['id'] . '">' . $v['Event']['title'] . '</a>';
				}
			}
		}
		return $data;
	}
	
}

