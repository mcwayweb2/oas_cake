<?php
/**
 * SMS component for CakePHP using the Clickatell HTTP API interface.
 * @author Doug Bromley <doug.bromley@gmail.com>
 * @copyright Doug Bromley
 * @link http://www.cakephp.org CakePHP
 * @link http://www.clickatell.com Clickatell
 */

class ClickatellComponent extends Object {
  var $api_user = null;
  var $api_pass = null;
  var $api_from = null;
  var $api_id = null;


  /**
  * The Clickatell XML API url
  */
  const API_XML_URL = 'http://api.clickatell.com/xml/xml';

  /**
  * The Clickatell HTTP API url for sending GET or POST requests too.
  */
  const API_HTTP_URL = 'http://api.clickatell.com/http/';


  /**
  * Post a message to the Clickatell servers for the number provided
  * @param string $tel The telephone number in international format.  Not inclduing a leading "+" or "00".
  * @param string $message The text message to send to the handset.
  * @return string
  * @see SmsComponent::api_id
  * @see SmsComponent::api_user
  * @see SmsComponent::api_pass
  * @see SmsComponent::api_from
  */
  function postSms($tel, $message) {
    $postdata = http_build_query(
      array(
        'api_id' => $this->api_id,
        'user' => $this->api_user,
        'password' => $this->api_pass,
        'from' => $this->api_from,
        'to' => $tel,
        'text' => $message
      )
    );

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
      )
    );

    $context  = stream_context_create($opts);
    $response = file_get_contents(self::API_HTTP_URL.'sendmsg', false, $context);
    return $response;
  }
  
  function formatPhone($phone) {
  	//strip all unnecessary chars from phone
  	$phone = preg_replace('/\D/', '', $phone);
  	
  	//add international code
  	if(strpos($phone, "1") != 0) {
  		return "1".$phone;
  	} else {
  		return $phone;
  	}
  }

  /**
  * Get the balance of your Clickatell account.
  * @return float
  * @see SmsComponent::api_id
  * @see SmsComponent::api_user
  * @see SmsComponent::api_pass
  * @see SmsComponent::api_from
  */
  function queryBalance() {
    $postdata = http_build_query(
      array(
        'api_id' => $this->api_id,
        'user' => $this->api_user,
        'password' => $this->api_pass
      )
    );

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
      )
    );

    $context  = stream_context_create($opts);
    $response = file_get_contents(self::API_HTTP_URL.'getbalance', false, $context);
    return $response;
  }
}
