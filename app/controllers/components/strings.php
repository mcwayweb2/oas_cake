<?php
class StringsComponent extends Object {
	//random string manipulation functions
	
	//	generates random string of default length 8
    function randomString($length = 8, $extension = null, $charset = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") {
		$i = 0;
		$string = "";
		
		while ($i < $length) { 
			$char = substr($charset, mt_rand(0, strlen($charset)-1), 1);

		    //check to make sure the char is not already in the random string
			if (!strstr($string, $char)) { 
	    		$string .= $char;
	      		$i++;
	    	}
		}
		if($extension) {
			$string .= ".".$extension;
		}
    	return $string;
    }
}