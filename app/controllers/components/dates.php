<?php
class DatesComponent extends Object {
	//random date and time manipulation functions
	
 	//this function checks if the birthdate passed, is at least as old as the age passed
    function checkAge($birthdate, $age = 18) {
    	if((date("Y") - $birthdate['year']) > $age) {
    		//no need for further validation
    		return true;
    	} else if((date("Y") - $birthdate['year']) == $age) {
    		//if current month is passed birthdate
	    	if(date("n") > $birthdate['month']) {
	    		return true;
	    	} else if(date("n") == $birthdate['month']) {
	    		//passed month is same as current month...gotta check day now.
	    		if(date("j") >= $birthdate['day']) {
	    			return true;	
	    		} else {
	    			return false;
	    		}
	    	} else {
	    		return false;
	    	}
    	} else {
    		return false;
    	}
    }
    
    function get_time_difference($start, $end) {
	    $timestamps['start']      =    strtotime($start);
	    $timestamps['end']        =    strtotime($end);
	    if($timestamps['start'] !== -1 && $timestamps['end'] !== -1) {
	        if($timestamps['end'] >= $timestamps['start']) {
	            $diff = $timestamps['end'] - $timestamps['start'];
	            if( $days=intval((floor($diff/86400))) )
	                $diff = $diff % 86400;
	            if( $hours=intval((floor($diff/3600))) )
	                $diff = $diff % 3600;
	            if( $minutes=intval((floor($diff/60))) )
	                $diff = $diff % 60;
	            $diff    =    intval( $diff );            
	            return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
	        } else { return false; }
	    } else { return false; }
	    return false;
	}
    
	
		
}