<?php 
class AuthorizeNetComponent extends Object {
	//////////// Controller Usage \\\\\\\\\\\\\
	/*
	$components = array('AuthorizeNet');
	
	$billinginfo = array("fname" => "First",
							"lname" => "Last",
							"address" => "123 Fake St. Suite 0",
							"city" => "City",
							"state" => "ST",
							"zip" => "90210",
							"country" => "USA");
	
	$shippinginfo = array("fname" => "First",
							"lname" => "Last",
							"address" => "123 Fake St. Suite 0",
							"city" => "City",
							"state" => "ST",
							"zip" => "90210",
							"country" => "USA");
	
	$response = $this->AuthorizeNet->chargeCard('########', '##############', '4111111111111111', '01', '2010', '123', true, 110, 5, 5, "Purchase of Goods", $billinginfo, "email@email.com", "555-555-5555", $shippinginfo);
	$response:Array = $this->AuthorizeNet->chargeCard($loginid:String, $trankey:String, $ccnum:String, $ccexpmonth:String, $ccexpyear:String, $ccver:String, $live:Boolean, $amount:Number, $tax:Number, $shipping:Number, $desc:String, $billinginfo:Array, $email:String, $phone:String, $shippinginfo:Array);
	
	// Important Response Values
	$response[1] = Response Code (1 = Approved, 2 = Declined, 3 = Error, 4 = Held for Review)
	$response[2] = Response Subcode (Code used for Internal Transaction Details)
	$response[3] = Response Reason Code (Code detailing response code)
	$response[4] = Response Reason Text (Text detailing response code and response reason code)
	$response[5] = Authorization Code (Authorization or approval code - 6 characters)
	$response[6] = AVS Response (Address Verification Service response code - A, B, E, G, N, P, R, S, U, W, X, Y, Z)
					(A, P, W, X, Y, Z are default AVS confirmation settings - Use your Authorize.net Merchant Interface to change these settings)
					(B, E, G, N, R, S, U are default AVS rejection settings - Use your Authorize.net Merchant Interface to change these settings)
	$response[7] = Transaction ID (Gateway assigned id number for the transaction)
	$response[38] = MD5 Hash (Gateway generated MD5 has used to authenticate transaction response)
	$response[39] = Card Code Response (CCV Card Code Verification response code - M = Match, N = No Match, P = No Processed, S = Should have been present, U = Issuer unable to process request)	
	*/
	

	// class variables go here
	var $loginid = "29wV4Xbd3";
	var $transactionkey = '23Gz9Ve8bX89b94D';
	var $host = "api.authorize.net";
	var $path = "/xml/v1/request.api";

	function startup(&$controller) {
		// This method takes a reference to the controller which is loading it.
		// Perform controller initialization here.
	}
	
	function parseResponse($resp, $delimiter = "|") {
		$text = $resp;
		$h = substr_count($text, $delimiter);
		$h++;
		$responsearray = array();

		for($j=1; $j <= $h; $j++){
			$p = strpos($text, $delimiter);

			if ($p === false) { // note: three equal signs
				$responsearray[$j] = $text;
			} else {
				$p++;
				//  get one portion of the response at a time
				$pstr = substr($text, 0, $p);
				$pstr_trimmed = substr($pstr, 0, -1); // removes "|" at the end

				if($pstr_trimmed==""){
					$pstr_trimmed="";
				}

				$responsearray[$j] = $pstr_trimmed;

				// remove the part that we identified and work with the rest of the string
				$text = substr($text, $p);

			} // end if $p === false

		} // end parsing for loop
		
		return $responsearray;
	}
	
	function chargeCard($ccnum, $ccexpmonth, $ccexpyear, $live, $amount, $tax, $shipping, $desc, $billinginfo, $invoice_num, $charge_type = "AUTH_CAPTURE") {
	
		// setup variables
		$ccexp = $ccexpmonth . '/' . $ccexpyear;
		
		$DEBUGGING					= 1;				# Display additional information to track down problems
		$TESTING					= 1;				# Set the testing flag so that transactions are not live
		$ERROR_RETRIES				= 2;				# Number of transactions to post if soft errors occur
	
		### $auth_net_url				= "https://certification.authorize.net/gateway/transact.dll";
		#  Uncomment the line ABOVE for test accounts or BELOW for live merchant accounts
		$auth_net_url				= "https://secure.authorize.net/gateway/transact.dll";
		
		$authnet_values				= array
		(
			"x_login"				=> $this->loginid,
			"x_version"				=> "3.1",
			"x_delim_char"			=> "|",
			"x_delim_data"			=> "TRUE",
			"x_url"					=> "FALSE",
			"x_type"				=> $charge_type,
			"x_method"				=> "CC",
			"x_tran_key"			=> $this->transactionkey,
			"x_relay_response"		=> "FALSE",
			"x_card_num"			=> str_replace(" ", "", $ccnum),
			"x_exp_date"			=> $ccexp,
			"x_description"			=> $desc,
			"x_amount"				=> $amount,
			"x_tax"					=> $tax,
			"x_freight"				=> $shipping,
			"x_first_name"			=> $billinginfo["fname"],
			"x_last_name"			=> $billinginfo["lname"],
			"x_address"				=> "",
			"x_city"				=> "",
			"x_state"				=> ""
		);
		
		if(isset($billinginfo["zip"])) {
			$authnet_values['x_zip'] = $billinginfo["zip"]; 	
		}
		
		if($invoice_num) {
			$authnet_values['x_invoice_num'] = $invoice_num;
		}
		
		$fields = "";
		foreach ( $authnet_values as $key => $value ) $fields .= "$key=" . urlencode( $value ) . "&";
		
		///////////////////////////////////////////////////////////
		
		// Post the transaction (see the code for specific information)
		
		
		### $ch = curl_init("https://certification.authorize.net/gateway/transact.dll");
		###  Uncomment the line ABOVE for test accounts or BELOW for live merchant accounts
		$ch = curl_init("https://secure.authorize.net/gateway/transact.dll");  
		### curl_setopt($ch, CURLOPT_URL, "https://secure.authorize.net/gateway/transact.dll");
		curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($ch, CURLOPT_POSTFIELDS, rtrim( $fields, "& " )); // use HTTP POST to send form data
   		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response. ###
		$resp = curl_exec($ch); //execute post and get results
		curl_close ($ch);
		
		// Parse through response string
		return $this->parseResponse($resp);
	} // end chargeCard function
	
	//create a recurring subscription, set to start 1 month from run date.
	function arb_create_subscription($price, $ccnum, $ccexp = 'MM/YY', $billinginfo, 
									 $subscription_name = 'Premium Membership Subscription', $desc = null, $ref_id = null, 
									 $free_month_trial = false, $interval_len = null, $interval_unit = null) {
									 	
		//if no interval specified, populate with 'monthly' as default
		if(!$interval_len || !$interval_unit) {
			$interval_len = '1';
			$interval_unit = 'months';
		}
		
		if($free_month_trial) {
			if(isset($free_month_trial['length']) && isset($free_month_trial['amt'])) {
				// an array of custom trial period parameters have been included
				$trial_months = $free_month_trial['length'];
				$trial_amt = $free_month_trial['amt'];
			} else {
				//set up for a standard 6 month trial
				$trial_months = '6';
				$trial_amt = '0.00';
			}
		}
		
		$content =
	        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
	        "<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
	        $this->MerchantAuthenticationBlock();
	    
	    $content .= 
	        "<subscription>".
		        "<name>" . $subscription_name . "</name>".
		        "<paymentSchedule>".
			        "<interval>".
			        	"<length>" . $interval_len . "</length>".
			        	"<unit>" . $interval_unit . "</unit>".
			        "</interval>".
			        "<startDate>" . (($free_month_trial) ? date("Y-m-d") : date("Y-m-d", strtotime("+1 month"))) . "</startDate>".
	    			"<totalOccurrences>9999</totalOccurrences>";
	        
       	if($free_month_trial) {
       		$content .= "<trialOccurrences>" . $trial_months . "</trialOccurrences>";	
        }

		$content .= 
		        "</paymentSchedule>".
		        "<amount>$price</amount>";
		
		if($free_month_trial) {
        	$content .= "<trialAmount>" . $trial_amt . "</trialAmount>";	
        }
		
        $content .= 
		        "<payment>".
			    "<creditCard>".
			        "<cardNumber>" . $ccnum . "</cardNumber>".
			        "<expirationDate>" . $ccexp . "</expirationDate>".
			    "</creditCard>".
			    "</payment>";
        
        if($desc || $ref_id) {
        	$content .=	"<order>";
        	if($ref_id) {
        		$content .= "<invoiceNumber>" . $ref_id . "</invoiceNumber>"; 
        	}
        	if($desc) {
        		$content .= "<description>" . $desc . "</description>"; 
        	}
        	$content .=	"</order>";
        }
        
        $content .= 
		        "<billTo>".
			        "<firstName>" . $billinginfo["fname"] . "</firstName>".
			        "<lastName>" . $billinginfo["lname"] . "</lastName>".
			        "<address></address>".
			        "<city></city>".
			        "<state></state>".
			        "<zip>" . $billinginfo["zip"] . "</zip>".
		        "</billTo>".
	        "</subscription>".
	        "</ARBCreateSubscriptionRequest>";
	        
	    $curl_response = $this->send_request_via_curl($content);
	    return $this->parse_arb_return($curl_response);	  
	}
	
	function arb_update_subscription() {
		
	}
	
	function arb_subscription_status() {
		
	}
	
	function arb_cancel_subscription($subscription_id = null) {
		$content =
	        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
	        "<ARBCancelSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
	        	$this->MerchantAuthenticationBlock().
	        	"<subscriptionId>". $subscription_id . "</subscriptionId>" . 
	        "</ARBCancelSubscriptionRequest>";
	        
	    $curl_response = $this->send_request_via_curl($content);
	    return $this->parse_arb_return($curl_response);	  
	}
	
	//function to parse arb response
	function parse_arb_return($content, $fields = null) {
		$return_array = array('refId' 			=> $this->substring_between($content,'<refId>','</refId>'),
							  'resultCode' 		=> $this->substring_between($content,'<resultCode>','</resultCode>'),
							  'code' 			=> $this->substring_between($content,'<code>','</code>'),
							  'text' 			=> $this->substring_between($content,'<text>','</text>'),
							  'subscriptionId' 	=> $this->substring_between($content,'<subscriptionId>','</subscriptionId>'));
		
		if($fields) {
			// additional fields specified to return
			foreach($fields as $tag):
				$return_array[$tag] = $this->substring_between($content, '<'.$tag.'>', '</'.$tag.'>');
			endforeach;
		}
		return $return_array;
	}
	
	// void a pending charge. If charge already settled, issue a refund
	function voidCharge($transaction_id, $masked_cc, $amt) {
		$auth_net_url				= "https://secure.authorize.net/gateway/transact.dll";
		$authnet_values				= array
		(
			"x_login"				=> $this->loginid,
			"x_version"				=> "3.1",
			"x_delim_char"			=> "|",
			"x_delim_data"			=> "TRUE",
			"x_url"					=> "FALSE",
			"x_type"				=> "VOID",
			"x_tran_key"			=> $this->transactionkey,
			"x_relay_response"		=> "FALSE",
			"x_trans_id"			=> $transaction_id
		);
		
		$fields = "";
		foreach ( $authnet_values as $key => $value ) $fields .= "$key=" . urlencode( $value ) . "&";
		
		### $ch = curl_init("https://certification.authorize.net/gateway/transact.dll");
		###  Uncomment the line ABOVE for test accounts or BELOW for live merchant accounts
		$ch = curl_init("https://secure.authorize.net/gateway/transact.dll");  
		### curl_setopt($ch, CURLOPT_URL, "https://secure.authorize.net/gateway/transact.dll");
		curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($ch, CURLOPT_POSTFIELDS, rtrim( $fields, "& " )); // use HTTP POST to send form data
   		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response. ###
		$resp = curl_exec($ch); //execute post and get results
		curl_close ($ch);
		
		$response = $this->parseResponse($resp);
		if($response[1] == '1') {
			// void was successful
			return $response;
		} else {
			// unsuccessful... assume transaction has been settled, and issue a refund
			$content = '<?xml version="1.0" encoding="utf-8"?>
			<createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
			  $this->MerchantAuthenticationBlock().
			'<transaction>
			    <profileTransRefund>
			      <amount>'.number_format($amt, 2).'</amount>
				  <creditCardNumberMasked>'.$masked_cc.'</creditCardNumberMasked>
				  <transId>'.$transaction_id.'</transId>
				</profileTransRefund>
			</transaction>
			</createCustomerProfileTransactionRequest>';
			
			$curl_response = $this->send_request_via_curl($content);
			return $this->parse_cim_return($curl_response);
			//return $content;
		}
	}
	
	//creates initial cim user profile
	function cim_create_profile($cust_id, $email, $description = null) {
		$content =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
				"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
				$this->MerchantAuthenticationBlock().
				"<profile>".
				"<merchantCustomerId>".$cust_id."</merchantCustomerId>".
				"<email>" . $email . "</email>";
		//if($description) $content .= "<description>".$description."</description>";
		$content .= 				
				"</profile>".
				"</createCustomerProfileRequest>";
				
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	function cim_get_profile($id) {
		$content = 
		'<?xml version="1.0" encoding="utf-8"?>
		<getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().
		  '<customerProfileId>'.$id.'</customerProfileId>
		</getCustomerProfileRequest>';
		return $this->send_request_via_curl($content);
	}
	
	function cim_get_all_profiles() {
		$content = 
		'<?xml version="1.0" encoding="utf-8"?>
		<getCustomerProfileIdsRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().'
		</getCustomerProfileIdsRequest>';
		return $this->send_request_via_curl($content);
	}
	
	function cim_create_payment_profile($id, $billing, $type = 'Credit Card') {
		$content = '<?xml version="1.0" encoding="utf-8"?>
		<createCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().
		'<customerProfileId>'.$id.'</customerProfileId>
		  <paymentProfile>';
		
		if($type == 'Credit Card') {
			$content .= 
		    '<billTo>
		      <firstName>'.$billing['fname'].'</firstName>
		      <lastName>'.$billing['lname'].'</lastName>
		      <address>'.$billing['address'].'</address>
		      <city>'.$billing['city'].'</city>
		      <state>'.$billing['state'].'</state>
		      <zip>'.$billing['zip'].'</zip>
		    </billTo>
		    <payment>
		      <creditCard>
		        <cardNumber>'.$billing['cc'].'</cardNumber>
		        <expirationDate>'.$billing['exp'].'</expirationDate>
		      </creditCard>
		    </payment>';	
		} else {
			//type bank account
			$content .= 
		    '<billTo>
		      <firstName>'.$billing['fname'].'</firstName>
		      <lastName>'.$billing['lname'].'</lastName>
		    </billTo>
		    <payment>
		      <bankAccount>
		        <routingNumber>'.$billing['routing'].'</routingNumber>
		        <accountNumber>'.$billing['account'].'</accountNumber>
		        <nameOnAccount>'.$billing['fname'].' '.$billing['lname'].'</nameOnAccount>
		      </bankAccount>
		    </payment>';
		}
		
		$content .= 
		  '</paymentProfile>
		  <validationMode>liveMode</validationMode>
		</createCustomerPaymentProfileRequest>';
		
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	function cim_update_payment_profile($profile_id, $payment_id, $billing) {
		$content = '<?xml version="1.0" encoding="utf-8"?>
		<updateCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().
		'<customerProfileId>'.$profile_id.'</customerProfileId>
		  <paymentProfile>
		    <billTo>
		      <firstName>'.$billing['fname'].'</firstName>
		      <lastName>'.$billing['lname'].'</lastName>
		      <address>'.$billing['address'].'</address>
		      <city>'.$billing['city'].'</city>
		      <state>'.$billing['state'].'</state>
		      <zip>'.$billing['zip'].'</zip>
		    </billTo>
		    <payment>
		      <creditCard>
		        <cardNumber>'.$billing['cc'].'</cardNumber>
		        <expirationDate>'.$billing['exp'].'</expirationDate>
		      </creditCard>
		    </payment>
		  <customerPaymentProfileId>'.$payment_id.'</customerPaymentProfileId>
		  </paymentProfile>
		</updateCustomerPaymentProfileRequest>';
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	function cim_validate_payment_profile($profile_id, $payment_id, $mode = 'testMode') {
		$content = 
		'<?xml version="1.0" encoding="utf-8"?>
		<validateCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().
		'<customerProfileId>'.$profile_id.'</customerProfileId>
		<customerPaymentProfileId>'.$payment_id.'</customerPaymentProfileId>
		<validationMode>'.$mode.'</validationMode>
		</validateCustomerPaymentProfileRequest>';
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	function cim_delete_payment_profile($profile_id, $payment_id) {
		$content = 
		'<?xml version="1.0" encoding="utf-8"?>
		<deleteCustomerPaymentProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		$this->MerchantAuthenticationBlock().
		'<customerProfileId>'.$profile_id.'</customerProfileId>
		<customerPaymentProfileId>'.$payment_id.'</customerPaymentProfileId>
		</deleteCustomerPaymentProfileRequest>';
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	function cim_create_transaction($profile_id, $payment_id, $amt = 0, $recurring = null, $order_info = null) {
		$content = '<?xml version="1.0" encoding="utf-8"?>
		<createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		  $this->MerchantAuthenticationBlock().
		'<transaction>
		    <profileTransAuthCapture>
		      <amount>'.number_format($amt, 2).'</amount>
		      <customerProfileId>'.$profile_id.'</customerProfileId>
			  <customerPaymentProfileId>'.$payment_id.'</customerPaymentProfileId>';
			  
		if($order_info) {
			$content .= '<order>';
			if(isset($order_info)) {
				foreach($order_info as $tag => $value):
					$content .= '<'.$tag.'>'.$value.'</'.$tag.'>';
				endforeach;
			}		
			$content .= '</order>';
		}
		if($recurring) $content .= '<recurringBilling>true</recurringBilling>';		  
			  
		$content .= '</profileTransAuthCapture>
		  </transaction>
		</createCustomerProfileTransactionRequest>';
		$curl_response = $this->send_request_via_curl($content);
		return $this->parse_cim_return($curl_response);
	}
	
	//void a pending charge. If charge already settled, issue a refund
	function cim_void_transaction($profile_id, $payment_profile_id, $transaction_id, $amt) {
		// try and void it first.
		$content = '<?xml version="1.0" encoding="utf-8"?>
		<createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
		  $this->MerchantAuthenticationBlock().
		'<transaction>
		    <profileTransVoid>
		      <customerProfileId>'.$profile_id.'</customerProfileId>
			  <customerPaymentProfileId>'.$payment_profile_id.'</customerPaymentProfileId>
			  <transId>'.$transaction_id.'</transId>
			</profileTransVoid>
		</transaction>
		</createCustomerProfileTransactionRequest>';
		  
		$curl_response = $this->send_request_via_curl($content);
		$resp = $this->parse_cim_return($curl_response);
		
		if($resp['resultcode'] == 'Ok') {
			// voided successfully
			return $resp;			
		} else {
			// voided threw an error, assume transaction has been settled and issue refund
			$content = '<?xml version="1.0" encoding="utf-8"?>
			<createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
			  $this->MerchantAuthenticationBlock().
			'<transaction>
			    <profileTransRefund>
			      <amount>'.number_format($amt, 2).'</amount>
			      <customerProfileId>'.$profile_id.'</customerProfileId>
				  <customerPaymentProfileId>'.$payment_profile_id.'</customerPaymentProfileId>
				  <transId>'.$transaction_id.'</transId>
				</profileTransRefund>
			</transaction>
			</createCustomerProfileTransactionRequest>';
			
			$curl_response = $this->send_request_via_curl($content);
			return $this->parse_cim_return($curl_response);
		} 
	}
	
	//issues an unlinked credit/refund to a card in a CIM profile 
	function cim_unlinked_refund($profile_id, $payment_profile_id, $amt, $inv_number = null) {
		$content = '<?xml version="1.0" encoding="utf-8"?>
			<createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">'.
			  $this->MerchantAuthenticationBlock().
			'<transaction>
			    <profileTransRefund>
			      <amount>'.number_format($amt, 2).'</amount>
			      <customerProfileId>'.$profile_id.'</customerProfileId>
				  <customerPaymentProfileId>'.$payment_profile_id.'</customerPaymentProfileId>';

		if($inv_number) {
			$content .= 
				'<order>
				  <invoiceNumber>'.$inv_number.'</invoiceNumber>
				</order>';
		}
			  
		$content .= 
				'</profileTransRefund>
			</transaction>
			</createCustomerProfileTransactionRequest>';
			
			$curl_response = $this->send_request_via_curl($content);
			return $this->parse_cim_return($curl_response);
	}
	
	function MerchantAuthenticationBlock() {
		return
	        "<merchantAuthentication>".
	        "<name>" . $this->loginid . "</name>".
	        "<transactionKey>" . $this->transactionkey . "</transactionKey>".
	        "</merchantAuthentication>";
	}
	
	function send_request_via_curl($content)
	{
		$posturl = "https://" . $this->host . $this->path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		return $response;
	}
	
	//function to parse cim response
	function parse_cim_return($content)
	{
		$paymentId = $this->substring_between($content,'<customerPaymentProfileId>','</customerPaymentProfileId>');
		$profileId = $this->substring_between($content,'<customerProfileId>','</customerProfileId>');
		$resultCode = $this->substring_between($content,'<resultCode>','</resultCode>');
		$code = $this->substring_between($content,'<code>','</code>');
		$text = $this->substring_between($content,'<text>','</text>');
		$fullResponse = $this->substring_between($content,'<directResponse>','</directResponse>');
		
		$response_parts = explode(',', $fullResponse);
		
		$response = array('resultcode' 		 => $resultCode,
						  'code'       		 => $code,
						  'text'             => $text,
						  'fullResponse'     => $fullResponse,
						  'paymentProfileId' => $paymentId,
						  'profileId'        => $profileId);
		if(isset($response_parts[6])) { $response['transactionId'] = $response_parts[6]; } 

		return $response;
	}
	
	//helper function for parsing response
	function substring_between($haystack,$start,$end) 
	{
		if (strpos($haystack,$start) === false || strpos($haystack,$end) === false) {
			return false;
		} else {
			$start_position = strpos($haystack,$start)+strlen($start);
			$end_position = strpos($haystack,$end);
			return substr($haystack,$start_position,$end_position-$start_position);
		}
	}
}
?>