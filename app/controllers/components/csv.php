<?php
/**
 * General CSV Component to help with creating csv files from the controller
 * @author Robert McWay <mcway.web.development@gmail.com>
 * @copyright Robert McWay
 * @link http://www.cakephp.org CakePHP
 */

class CsvComponent extends Object {
	var $filename = null;
	var $path = null;
	
	function write_file($data) {
		//if a filename has not been specified, create and set one
		if(!$this->filename) {
			$this->filename = $this->create_filename();
		}
		if(!$this->path) {
			$this->path = WWW_ROOT.'/files/';
		}
		
		$fp = fopen($this->path.$this->filename, 'w');
		foreach($data as $line):
			//write data to file one one at a time
			fputcsv($fp, $line);
		endforeach;
		fclose($fp);
	}
	
	function create_filename($length = 12, $extension = 'csv', $charset = "0123456789bcdfghjkmnpqrstvwxyz") {
	    $i = 0;
		$filename = "";
		
		while ($i < $length) { 
			$char = substr($charset, mt_rand(0, strlen($charset)-1), 1);

		    //check to make sure the char is not already in the random string
			if (!strstr($filename, $char)) { 
	    		$filename .= $char;
	      		$i++;
	    	}
		} 
    	return $filename.".".$extension;
	}

}
