<?php
	class FileUploadComponent extends Object {
		
		var $controller = null;
		var $validateFile = array(
                          'size' => 108432000
                          );
		
		function startup(&$controller) {
			$this->controller = $controller;
		}
		
		function generateUniqueFilename($fileName, $path="") {
		    $path = empty($path) ? WWW_ROOT.'files/' : WWW_ROOT.$path;
		    $no = 1;
		    $newFileName = $fileName;
		    while (file_exists("$path/".$newFileName)) {
		      $no++;
		      $newFileName = substr_replace($fileName, "_$no.", strrpos($fileName, "."), 1);
		    }
		    return $newFileName;
	  	}
	  	
	  	//this function will return the error of the process, or false if there was no error
  		function uploadViaFTP($ftp_server, $ftp_user, $ftp_pass, $ftp_dir, $filename, $tmp_name) {
				set_time_limit(0);
				$destination_file = $ftp_dir . $filename;
				
				$conn_id = ftp_connect($ftp_server);
			if(!$conn_id) {
				return 'Error connecting to FTP server.';
			}
			
			if(ftp_login($conn_id, $ftp_user, $ftp_pass)) {
				if(ftp_put($conn_id, $destination_file, $tmp_name, FTP_BINARY)) {
					$ch=ftp_site($conn_id,"chmod 777 ".$destination_file);
					ftp_close($conn_id); 
					return false;
				} else {
					return "Error uploading file.";
				}
			} else {
				return 'Error logging into FTP server.';
			}
		}
		
		function handleFileUpload($fileData, $fileName, $fileimgpath = '') {
	    	$error = false;
	 
		    //Get file type
		   //$typeArr = explode('/', $fileData['type']);
	 
		    //If size is provided for validation check with that size. Else compare the size with INI file
		    if (($this->validateFile['size'] && $fileData['size'] > $this->validateFile['size']) || $fileData['error'] == UPLOAD_ERR_INI_SIZE) {
		      $error = 'File is too large to upload';
		    }  else {
	        	//Data looks OK at this stage. Let's proceed.
		        if ($fileData['error'] == UPLOAD_ERR_OK) {
		        	//Oops!! File size is zero. Error!
		        	if ($fileData['size'] == 0) {
		          		$error = 'Zero size file found.';
		        	} else {
		          		if (is_uploaded_file($fileData['tmp_name'])) {
		            		//Finally we can upload file now. Let's do it and return without errors if success in moving.
		            		if (!move_uploaded_file($fileData['tmp_name'], WWW_ROOT.$fileimgpath.$fileName)) {
		              			$error = true;
		            		}
		          		} else {
		            		$error = true;
		          		}
		        	}
		      	}
	    	}
	    	return $error;
	  	}
	  	
	  	function deleteMovedFile($fileName) {
	    	if (!$fileName || !is_file($fileName)) {
	      		return true;
	    	} if(unlink($fileName)) {
	      		return true;
	    	}
	    	return false;
	  	}
		
	}
?>