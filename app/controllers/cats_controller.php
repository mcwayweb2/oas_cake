<?php
class CatsController extends AppController {

	var $name = 'Cats';
	
	function add() {
		$this->checkAdvertiserSession();
		
		if(!empty($this->data)) {
			$this->data['Cat']['title'] = ucwords($this->data['Cat']['title']);
			//check to first make sure this category doesnt already exist.
			$cat = $this->Cat->findByTitle($this->data['Cat']['title']);
			if($cat) {
				$this->Session->setFlash(__('This category already exists.', true));
			} else {
				$this->Cat->create();
				if($this->Cat->save($this->data)) {
					//now also add a category record for the location
					$this->Cat->LocationsCat->create();
					if($this->Cat->LocationsCat->save(array('LocationsCat' => array('cat_id'      => $this->Cat->getLastInsertId(),
																					'location_id' => $this->data['Cat']['location_id'])))) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('You have successfully added your new category.', true));	
					} else {
						$this->Session->setFlash(__('There was a problem adding your new category. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields to add a category.', true));
				}	
			}
		} else {
			$this->Session->setFlash(__('Please fill out all the required fields to add a category.', true));
		}
		$this->redirect($this->referer().'#mnuCategories');
	}
	
	function admin_index() {
		$this->checkAdminSession();
		$this->Cat->recursive = 0;
		$this->set('cats', $this->paginate());
	}

	function admin_add() {
		$this->checkAdminSession();
		if (!empty($this->data)) {
			$this->Cat->create();
			if ($this->Cat->save($this->data)) {
				$this->Session->setFlash(__('The Category has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Category could not be saved. Please, try again.', true));
			}
		}
		$menus = $this->Cat->Menu->find('list');
		$this->set(compact('menus'));
	}

	function admin_edit($id = null) {
		$this->checkAdminSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Category', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Cat->save($this->data)) {
				$this->Session->setFlash(__('The Category has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Category could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Cat->read(null, $id);
		}
		$menus = $this->Cat->Menu->find('list');
		$this->set(compact('menus'));
	}

	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Category', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Cat->delete($id)) {
			$this->Session->setFlash(__('Cat deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>