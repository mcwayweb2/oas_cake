<?php
class ContactsController extends AppController {

	var $name = 'Contacts';
	var $helpers = array('Html', 'Form');
	var $components = array('Email', 'MaxMind');
	
	function contactus() {
		if (!empty($this->data)) {
			$this->Contact->set($this->data);
			if($this->Contact->validates()) {
				$this->Contact->create();
				$this->data['Contact']['timestamp'] = date("Y-m-d h:i:s");
				
				if($this->Contact->save($this->data)) {
					if ($this->email()) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Thank you for your comments. We will get back to you shortly.', true));
						$this->redirect('/');
					} else {
						$this->Session->setFlash(__('The Email could not be sent. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields, and try again.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
		}
		$this->set('states', $this->Contact->State->find('list', array('fields' => array('State.name'))));
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Contact->State->Location->getFeatured($lat, $long));
	}
	
	function email() {
		$this->Email->to = "customer-service@orderaslice.com";
		$this->Email->replyTo = 'customer-service@orderaslice.com';
		$this->Email->from = $this->data['Contact']['email'];
		$this->Email->subject = 'Contact Form -> Customer Comment from OrderASlice!';
		
		//set email vars
		$this->set('contact',$this->data['Contact']);
		$this->set('state',$this->Contact->State->read('name', $this->data['Contact']['state_id']));
		
		
		$this->Email->sendAs = 'html';
		$this->Email->template = 'contactus';
		return ($this->Email->send()) ? true : false;
	}
	
	function admin_index() {
		$this->checkAdminSession();
		$this->paginate = array('Contact' => array('limit' => 20,
													'order' => array('timestamp' => 'desc')));
		$this->set('contacts', $this->paginate('Contact'));
	}
	
	function admin_view($id) {
		$this->checkAdminSession();
		if(!$id) {
			$this->Session->setFlash(__('Invalid comment id.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('contact', $this->Contact->read(null, $id));
	}
	
	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for comment', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contact->delete($id)) {
			$this->Session->setFlash(__('Comment successfully deleted.', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>