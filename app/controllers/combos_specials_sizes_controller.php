<?php
class CombosSpecialsSizesController extends AppController {

	var $name = 'CombosSpecialsSizes';
	
	function add($combo_id = null) {
		if (!$combo_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Combo Special.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $combo_id = $this->data['CombosSpecialsSize']['combo_id'];
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$combo_id));
		
		if(!empty($this->data)) {
			$this->CombosSpecialsSize->create();
			if($this->CombosSpecialsSize->save($this->data)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('The Specialty Pizza has been successfully added to your combo.', true));
			} else {
				$this->Session->setFlash(__('There was a problem adding the pizza to your combo. Please try again.', true));
			}	
			
			$this->redirect('/combos/edit/'.$this->data['CombosSpecialsSize']['combo_id']);
		}
		
		//$c = $this->CombosSpecialsSize->Combo->read('location_id', $combo_id);
		$this->set('combo_id', $combo_id);
	}
	
	function ajax_any_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$c = $this->CombosSpecialsSize->Combo->read('location_id', $combo_id);
				$this->set('sizes', $this->CombosSpecialsSize->Combo->Location->getSizeList($c['Combo']['location_id']));
				$this->set('specials', $this->CombosSpecialsSize->SpecialsSize->Special->find('list', 
																							  array('conditions' => array('Special.location_id' => $c['Combo']['location_id']),
																							  		'fields'	 => 'Special.title')));		
			}
		}
	}
	
	function ajax_single_add($combo_id = null) {
		if($combo_id && $this->RequestHandler->isAjax()) {
			if($this->requestAction('/combos/verifyIdentity/'.$combo_id)) {
				$c = $this->CombosSpecialsSize->Combo->read('location_id', $combo_id);
				$this->set('sizes', $this->CombosSpecialsSize->Combo->Location->getSizeList($c['Combo']['location_id']));
				$this->set('specials', $this->CombosSpecialsSize->SpecialsSize->Special->find('list', 
																							  array('conditions' => array('Special.location_id' => $c['Combo']['location_id']),
																							  		'fields'	 => 'Special.title')));		
			}
		}
	}
	
	function ajax_single_sizes_add() {
		if($this->RequestHandler->isAjax()) {
			$this->set('sizes', $this->CombosSpecialsSize->SpecialsSize->Special->getSizes($this->params['form']['specialId']));
		}
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('The page you are trying to reach does not exist.', true));
		} else {
			$c = $this->CombosSpecialsSize->read(array('combo_id'), $id);
			$this->checkAdvertiserSession(array('type'=>'combo','value'=>$c['CombosSpecialsSize']['combo_id']));
			
			//single record to delete
			if($this->CombosSpecialsSize->delete($id)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Specialty Pizza successfully removed from your combo.', true));		
			} else {
				$this->Session->setFlash(__('There was a problem removing the Specialty Pizza from your combo. Please try again.', true));	
			}
		}
		$this->redirect($this->referer());
	}
}