<?php
class ItemsController extends AppController {
	var $name = 'Items';
	var $components = array('FileUpload');
	
	function add($location_id = null) {
		if (!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $location_id = $this->data['Item']['location_id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->set('location_id', $location_id);
		
		if(!empty($this->data)) {
			$this->Item->set($this->data);
			if(!$this->Item->validates()) {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} else {
				//see if there is an upload to take care of
				if (!empty($this->data['Item']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Item']['image']['name'], "/files/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Item']['image'], $fileName, "files/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Item']['image'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		    	} else { unset($this->data['Item']['image']); }

		    	$this->Item->create();
		    	if($this->Item->save($this->data)) {
		    		//item saved successfully, are there additional options to save?
			    	$success = true;
					if(isset($this->data['labels'])) {
						if($this->data['Item']['new_series_limit'] == $this->data['Item']['new_series_qty']) {
							//add options as individual options...set add group to zero
							$add_group_id = 0;
							$add_group_limit = 0;
						} else {
							//get value for add_group_id
							$add_group = $this->Item->ItemsOption->find('first', array('conditions' => array('ItemsOption.item_id' => $this->Item->getLastInsertId()),
																				  	   'fields'     => array('MAX(ItemsOption.add_group_id) as add_group_id')));				
							$add_group_id = (!empty($add_group[0]['add_group_id'])) ? $add_group[0]['add_group_id'] + 1 : '1';
							$add_group_limit = $this->data['Item']['new_series_limit'];
						}
						
						foreach($this->data['labels'] as $i => $lab):
							if(!empty($lab)) {
								$save = array('ItemsOption' => array('item_id' => $this->Item->getLastInsertId(),
																	 'add_group_id'     => $add_group_id,
																     'add_group_limit'  => $add_group_limit,
																	 'label'            => $lab));
								if(!empty($this->data['prices'][$i]) && is_numeric($this->data['prices'][$i])) { 
									$save['ItemsOption']['price'] = $this->data['prices'][$i];
								}
								
								$this->Item->ItemsOption->create();
								if(!$this->Item->ItemsOption->save($save)) $success = false;
							}
						endforeach;
					}
					
					if($success) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your menu item has been added successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your menu item. Please try again.', true));
					}
		
				}
				$l = $this->Item->Location->read('slug', $location_id);
				$this->redirect('/restaurants/edit_menu/'.$l['Location']['slug'].'#mnuCategories');
			}
		}
		$this->set('cat_list', $this->Item->Location->getCatList($location_id));
	}
	
	function multiple_add($location_id = null) {
		if (!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $location_id = $this->data['Item']['location_id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			$success_ct = 0;
			$failure_ct = 0;
			$error_ct = 0;
			
			//loop through each item to add
			foreach($this->data['items'] as $item):
				if(!empty($item['price']) && is_numeric($item['price']) && !empty($item['title'])) {
					$success = true;
					//minimum requirements met to be able to save the record
					$save = array('Item' => array('location_id' => $this->data['Item']['location_id'],
												  'cat_id'      => $this->data['Item']['cat_id'],
												  'title'       => ucwords($item['title']),
												  'description' => $item['description'],
												  'price'		=> $item['price']));
					
					//see if there is an upload to take care of
					if (!empty($item['image']['tmp_name'])) {
			      		$fileName = $this->FileUpload->generateUniqueFilename($item['image']['name'], "/files/");
			      		$error = $this->FileUpload->handleFileUpload($item['image'], $fileName, "files/");
					    if(!$error) {
					    	//file upload was ok
					    	$save['Item']['image'] = $fileName;
					    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
			    	}
	
			    	$this->Item->create();
			    	if($this->Item->save($save)) {
			    		$success_ct++;
			    	} else { $failure_ct++;	}
				}
			endforeach;
			
			$cat = $this->Item->Cat->read('title', $this->data['Item']['cat_id']);
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('('.$success_ct.') Item(s) successfully added to: '.$cat['Cat']['title'].'.', true));
			
			$l = $this->Item->Location->read('slug', $location_id);
			$this->redirect('/restaurants/edit_menu/'.$l['Location']['slug'].'#mnuCategories');
		}
		$this->set('cat_list', $this->Item->Location->getCatList($location_id));
		//need to create an array of locations, with index numbers that correspond with indexes in ad_locations array
		$this->set('current_loc_index', $this->Item->Location->getArrayIndex($location_id));
		$this->set('location', $this->Item->Location->getFieldsForBanner($location_id));
	}
	
	function ajax_order() {
		$success = true;
		if($this->RequestHandler->isAjax()) {
			
			
			/* still need to validate that all item ids belong to logged in advertiser */
			foreach(current($this->params['url']) as $id):
				
			endforeach;
			
			
			
			
			unset($this->params['url']['url']);	
			foreach(current($this->params['url']) as $order => $id):
				$this->Item->id = $id;
				if(!$this->Item->saveField('list_order', $order)) $success = false;
			endforeach;
		}
		$this->set('result', (($success) ? "New Menu Item Order Saved!" : "Trouble Saving New Menu Item Order. Please try again!"));
	}
	
	function ajax_update_series_limit() {
		Configure::write('debug', '0');
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['qty']);
		}
	}
	
	function ajax_update_series_labels() {
		Configure::write('debug', '0');
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['qty']);
		}
	}
	
	function edit($item_id = null) {
		if (!$item_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) { $loc_id = $this->data['Item']['location_id']; } 
		else {
			$i = $this->Item->read('location_id', $item_id);
			$loc_id = $i['Item']['location_id'];	
		}
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$loc_id));
		
		if(!empty($this->data)) {
			$this->Item->set($this->data);
			if(!$this->Item->validates()) {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} else {
				//see if there is an upload to take care of
				if (!empty($this->data['Item']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Item']['image']['name'], "/files/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Item']['image'], $fileName, "files/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Item']['image'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		    	} else { unset($this->data['Item']['image']); }

		    	if($this->Item->save($this->data)) {
		    		//item saved successfully, are there new additional options to save?
			    	$success = true;
					if(isset($this->data['labels'])) {
						if($this->data['Item']['new_series_limit'] == $this->data['Item']['new_series_qty']) {
							//add options as individual options...set add group to zero
							$add_group_id = 0;
							$add_group_limit = 0;
						} else {
							//get value for add_group_id
							$add_group = $this->Item->ItemsOption->find('first', array('conditions' => array('ItemsOption.item_id' => $this->data['Item']['id']),
																				  	   'fields'     => array('MAX(ItemsOption.add_group_id) as add_group_id')));				
							$add_group_id = (!empty($add_group[0]['add_group_id'])) ? $add_group[0]['add_group_id'] + 1 : '1';
							$add_group_limit = $this->data['Item']['new_series_limit'];
						}
						
						foreach($this->data['labels'] as $i => $lab):
							if(!empty($lab)) {
								if(empty($this->data['prices'][$i]) || !is_numeric($this->data['prices'][$i])) $this->data['prices'][$i] = 0;
								
								$save = array('ItemsOption' => array('item_id' => $this->data['Item']['id'],
																	 'add_group_id'     => $add_group_id,
																     'add_group_limit'  => $add_group_limit,
																	 'label'            => $lab,
																	 'price' => $this->data['prices'][$i]));
								$this->Item->ItemsOption->create();
								if(!$this->Item->ItemsOption->save($save)) $success = false;
							}
						endforeach;
					}
					
					if($success) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your menu item has been updated successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem updating your menu item. Please try again.', true));
					}
		
				} else {
					$this->Session->setFlash(__('There was a problem updating your menu item. Please try again.', true));
				}	
			}
			$l = $this->Item->Location->read('slug', $loc_id);
			$this->redirect('/locations/edit_menu/'.$l['Location']['slug'].'#mnuCategories');
		} else {
			$this->Item->contain(array('Cat', 'ItemsOption'));
			$this->data = $this->Item->read(null, $item_id);
			$this->data['Item']['image_path'] = $this->data['Item']['image']; 
			unset($this->data['Item']['image']);
		}
		$this->set('cat_list', $this->Item->Location->getCatList($this->data['Item']['location_id']));
	}
	
	function delete($item_id = null) {
		if (!$item_id) {
			$this->Session->setFlash(__('Invalid menu item.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$item = $this->Item->read(array('image', 'location_id'), $item_id);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$item['Item']['location_id']));
		
		//delete image on server if necessary
		if(!empty($item['Item']['image']) && file_exists(WWW_ROOT.'files/'.$item['Item']['image'])) {
			unlink('files/'.$item['Item']['image']);	
		}
		
		$this->Item->id = $item_id;
		if($this->Item->delete($item_id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your menu item has been deleted successfully.', true));
		} else {
			$this->Session->setFlash(__('There was a problem deleting your menu item. Please try again.', true));
		}
		$this->redirect($this->referer().'#mnuCategories');
	}

	function delete_series($option_id = null) {
		if (!$option_id) {
			$this->Session->setFlash(__('Invalid Selection Series.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$opt = $this->Item->ItemsOption->read(null, $option_id);
		$category = $this->Item->read(array('location_id'), $opt['ItemsOption']['item_id']);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$category['Item']['location_id']));
		
		//is this a single option to delete, or an entire series to delete?
		if($opt['ItemsOption']['add_group_id'] > 0) {
			//...an entire series
			if($this->Item->ItemsOption->deleteAll(array('ItemsOption.item_id'      => $opt['ItemsOption']['item_id'],
														 'ItemsOption.add_group_id' => $opt['ItemsOption']['add_group_id']))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Selection series removed successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem removing the selection series. Please try again.', true));
			}	 
		} else {
			//...a single option
			if($this->Item->ItemsOption->delete($option_id)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Add-On Option removed successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem removing the Add-On Option. Please try again.', true));
			}
		}
		$this->redirect($this->referer());
	}

	function ajax_get_item_opts() {
		if($this->RequestHandler->isAjax()) {
			$this->set('ajax_item', $this->Item->getItemInfo($this->params['form']['id']));
			$this->set('combos_item_id', $this->params['form']['combosItemId']);
			
			if(isset($this->params['form']['array_index'])) {
				$this->set('array_index', $this->params['form']['array_index']); 
			}
		}
	}
	
	function ajax_multiple_items() {
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['numOpts']);
		}
	}
	
	/* PHASE TO DELETE 
	/*
	function ajax_multiple_prices() {
		if($this->RequestHandler->isAjax()) {
			$this->set('qty', $this->params['form']['numOpts']);
		}
	}
	
	
	function ajax_add_one_pricing_opt() {
		
	}
	* END DELETE
	*/
}
?>