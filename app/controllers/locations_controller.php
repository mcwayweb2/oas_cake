<?php
class LocationsController extends AppController {
	var $name = 'Locations';
	var $helpers = array('GoogleMap', 'Text');
	var $components = array('AuthorizeNet', 'Email', 'FileUpload', 'Security', 'MaxMind');
	var $paginate = array('limit'  => 10);
	
	function beforeFilter() {
		//only validate post for order review page
		if($this->params['action'] != 'order_review') {
			$this->Security->validatePost = false;
		}
		
		$this->Security->blackHoleCallback = 'forceSSL';
        $this->Security->requireSecure('order_review', 'upgrade');
	}
	
	function getLocationInfo($locationid) {
		$params = array('conditions' => array('Location.id' => $locationid),
						'recursive'  => 0);
		return $this->Location->find('first', $params);
	}
	
	function getUrlSlugs($id = null) {
		return isset($id) ? $this->Location->getUrlSlugs($id) : false; 
	}
	
	function getFeatured() {
		//find lat and long of user by GeoIP
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Location->getFeatured($lat, $long));
		
	}
	
	//pulls list of locations for the advertiser that is currently logged in
	function getAdvertiserLocations($type = 'all') {
		$params = array('conditions' => array('Location.advertiser_id' => $this->Session->read('Advertiser.id')),
						'fields'	 => array('Location.id', 'Location.name', 'Location.active','Location.menu_ready', 'Location.slug', 
											  'Location.logo', 'Location.phone', 'Location.fax', 'Location.sms', 'Location.acct_type'),
						'order'      => array('Location.name'),
						'recursive'  => 0);
		$locs = $this->Location->find($type, $params);
		$params['fields'] = array('Location.slug', 'Location.name');
		$locs['ad_loc_list'] = $this->Location->find('list', $params);
		return $locs;
 	}
	
	function verifyIdentity($id) {
		$loc = $this->Location->read('advertiser_id', $id);	
		
		return ($loc['Location']['advertiser_id'] == $this->Session->read('Advertiser.id')) ? true : false;
	}
	
	function sendGeocodeErrorEmail($id) {
		$location = $this->Location->read(null, $id);
		$this->Email->to = 'robert@mcwaywebdevelopment.com, admin@orderaslice.com';
		$this->Email->from = 'admin@orderaslice.com';
		$this->Email->subject = 'OrderASlice.com - LOCATION ADDRESS GEOCODE ERROR';
		$this->Email->sendAs = 'html';
		
		$this->set('location',$location);
		
		$this->Email->template = 'geocoded_error_location';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}

	function index($search = null) {
		//Configure::write('debug', '2');
		//temporarily hard coding the search radius to 15 miles
		/* THIS WILL CHANGE AFTER IMPLEMENTING A REAL SEARCH RADIUS PER LOCATION */
		$this->data['Search']['radius'] = 20;
		
		$conditions = array('Location.active' => '1', 
							'Location.menu_ready' => '1');
		
		$fields = array('Location.id','Location.latitude','Location.longitude','Location.advertiser_id','Location.name','Location.address1',
						'Location.address2','Location.city','Location.state_id','ZipCode.id','Location.phone','Location.fax','Location.sms',
						'Location.delivery_radius','Advertiser.logo','State.abbr', 'Location.logo', 'Location.use_logo_in_banner',  
						'Location.min_delivery_amt', 'Location.delivery_charge', 'Location.slogan', 'Location.accept_deliveries', 
						'Location.accept_pickups', 'Location.slug', 'Advertiser.slug', 'FoodCat.title');
		
		//are we searching with city/zip by url paramaters
		if(empty($search)) {
			if(isset($this->params['pass'][0])) { $zip_or_city_search = $this->params['pass'][0]; }
			else if(isset($this->params['search'])) { $zip_or_city_search = $this->params['search']; }
			else if(!empty($this->data['Search']['zip'])) { $zip_or_city_search = $this->data['Search']['zip']; }
			else { $search = null; }	
		}
		
		if($search) {
			//'desluggify' in case we are using a value from url
			$search = str_replace('-', ' ', $search);
				
			$searchCoords = $this->Location->Geocode($search);
			CakeLog::write('debug', 'SearchCoords Array: '.print_r($searchCoords, true));
			
			$conditionString = $this->Location->getDistanceFormula($searchCoords);
				
			$condition = $conditionString . " <= ";
			$conditions[$condition] = " ".$this->data['Search']['radius'];
				
			$distance = $conditionString . " as distance";
			$fields[] = $distance;
			$order = array('distance' => 'ASC');
			$this->set('search_zip', $search);
		} else { $order = array('name' => 'ASC'); }
		
		if(!empty($this->data['Search']['name'])) {
			$this->set('search_name', $this->data['Search']['name']);
			$conditions['OR'] = array('Location.name LIKE' => "%".$this->data['Search']['name']."%",
							  		  'Location.slogan LIKE' => "%".$this->data['Search']['name']."%",
							  		  'Advertiser.name LIKE' => "%".$this->data['Search']['name']."%",
						      		  'FoodCat.title LIKE' => "%".$this->data['Search']['name']."%");
			$this->set('search_keywords', $this->data['Search']['name']);
		}
		
		if(!empty($this->data['Search']['delivery_method'])) {
			if($this->data['Search']['delivery_method'] == 'Delivery Only') {
				$conditions['Location.accept_deliveries'] = '1';
			} else {
				//pickup restaurants only
				$conditions['Location.accept_pickups'] = '1';
			}
		} else {
			//check to see if we have already included an OR statement in the query
			if(isset($conditions['OR'])) {
				$conditions['AND'] = array('OR' => array('Location.accept_deliveries' => '1', 'Location.accept_pickups' => '1'));
			} else { $conditions['OR'] = array('Location.accept_deliveries' => '1', 'Location.accept_pickups' => '1'); }
		}
		
		//are we searching with a category by url paramaters
		if(isset($this->params['pass'][1]) || isset($this->params['category'])) {
			//validate that this category exists
			$search = isset($this->params['pass'][1]) ? $this->params['pass'][1] : $this->params['category'];
			$cat = $this->Location->FoodCat->findBySlug($search);
			if($cat) {
				$conditions['Location.food_cat_id'] = $cat['FoodCat']['id'];
				$this->set('search_cat', $cat['FoodCat']['title']);
			}
		} else if(!empty($this->data['Search']['food_cat_id'])) {
			$cat = $this->Location->FoodCat->read(array('title'), $this->data['Search']['food_cat_id']);
			if($cat) {
				$conditions['Location.food_cat_id'] = $this->data['Search']['food_cat_id'];
				$this->set('search_cat', $cat['FoodCat']['title']);
			}
		}
		
		//override pagination variable for custom pagination that adds distance
		$this->paginate = array('fields'    => $fields,
								'contain'   => array('Advertiser', 'State', 'ZipCode', 'FoodCat',
													 'LocationsTime' => array('conditions' => array('LocationsTime.day' => $this->getDayAbbr()))),
								'recursive' => 1,
								'order'     => $order);
		$locs = $this->paginate('Location', $conditions);
		foreach($locs as $i => $l):
			//$locs[$i]['Location']['open'] = $this->Location->isLocationOpen($l['Location']['id']);
			if(!$this->Location->isLocationOpen($l['Location']['id'])) unset($locs[$i]);
		endforeach;
		
		$this->set('locations', $locs);	
		//$this->set('radius', $this->data['Search']['radius']);
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Location->getFeatured($lat, $long));
	}

	function menu($locslug = null) {
		if (!$locslug) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(array('action' => 'index'));
		} 
		$this->layout = "restaurant";
		$loc_id = $this->Location->idBySlug($locslug);
		if (!$loc_id) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('locationid_for_layout', $loc_id);
		
		$loc = $this->Location->getFieldsForBanner($loc_id, array('Location.active', 'Location.menu_ready', 'Location.advertiser_id'));
		$this->set('location', $loc);
		
		//make sure this location is active before we start the session
		if($loc['Location']['active'] != 1 || $loc['Location']['menu_ready'] != 1) {
			 $this->Session->setFlash(__('This restaurant location is currently not accepting orders.', true));
			$this->redirect(array('action'=>'index'));
		}
		
		$this->set('specials', $this->Location->getSpecials($loc['Location']['id']));
		$this->set('sizes', $this->Location->getSizes($loc['Location']['id']));
		
		$this->paginate = array('fields'    => array('LocationsTopping.id', 'LocationsTopping.topping_id', 'LocationsTopping.order', 
												     'LocationsTopping.price_alter', 'Topping.title'),
								'recursive' => 0,
								'limit'		=> 100,
								'order'     => array('Topping.title'));
		$this->set('toppings', $this->paginate('LocationsTopping', array('LocationsTopping.location_id' => $loc['Location']['id'])));
			
		$this->set('cats', $this->Location->getAllItemsByCat($loc['Location']['id']));
		$this->set('crusts', $this->Location->getCrusts($loc['Location']['id']));
		$this->set('combos', $this->Location->getCombos($loc['Location']['id']));
		$this->set('crust_list', $this->Location->Crust->getCrustList());
		$this->set('topping_list', $this->Location->Topping->getToppingList());
		$this->set('cat_list', $this->Location->Cat->getCatList());
		
		//if an order has been started...
		if($this->Session->check('User.order_id')) {
			//check to make sure the location_id the order was started with, is the same as the current
			if($this->Session->read('User.location_id') == $loc['Location']['id']) {
				$this->set('order', $this->Location->Order->orderInfo($this->Session->read('User.order_id')));	
			} else {
				//not the same, kill the order, so they can start a new one with current location
				$this->Location->Order->killOrder($this->Session->read('User.order_id'));
				$this->Session->delete('User.order_id');
				$this->Session->delete('User.location_id');
			}
		}
	}
	
	function edit_menu($slug = null) {
		if (!$slug) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$loc_id = $this->Location->idBySlug($slug);
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$loc_id));
		$this->layout = "restaurant";
		
		// THESE QUERIES CAN BE REWRITTEN TO USE CONTAINABLE BEHAVIOR, AND USE ONLY 1 QUERY FOR ALL DATA
		// THESE QUERIES CAN BE REWRITTEN TO USE CONTAINABLE BEHAVIOR, AND USE ONLY 1 QUERY FOR ALL DATA
		// THESE QUERIES CAN BE REWRITTEN TO USE CONTAINABLE BEHAVIOR, AND USE ONLY 1 QUERY FOR ALL DATA
		$this->set('location', $this->Location->getFieldsForBanner($loc_id));
		$this->set('specials', $this->Location->getSpecials($loc_id));
		$this->set('sizes', $this->Location->getSizes($loc_id));
		
		$this->paginate = array('fields'    => array('LocationsTopping.id', 'LocationsTopping.topping_id', 'LocationsTopping.order', 
												     'LocationsTopping.price_alter', 'Topping.title', 'ToppingType.title'),
								'recursive' => 0,
								'limit'		=> 100,
								'order'     => array('Topping.title'));
		
		$this->set('toppings', $this->paginate('LocationsTopping', array('LocationsTopping.location_id' => $loc_id)));	
		$this->set('cats', $this->Location->getAllItemsByCat($loc_id));
		$this->set('crusts', $this->Location->getCrusts($loc_id));
		$this->set('combos', $this->Location->getCombos($loc_id));
		
		$this->set('crust_list', $this->Location->Crust->getCrustList());
		$this->set('topping_list', $this->Location->Topping->getToppingList());
		$this->set('cat_list', $this->Location->Cat->getCatList());
		$this->set('topping_types', $this->Location->LocationsTopping->ToppingType->find('list'));
		
		//need to create an array of locations, with index numbers that correspond with indexes in ad_locations array
		$this->set('current_loc_index', $this->Location->getArrayIndex($loc_id));
	}
	
	function order_manager($slug = null) {
		if (!$slug && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		
		$id = (empty($this->data)) ? $this->Location->idBySlug($slug) : $this->Location->idBySlug($this->data['Location']['slug']); 
		$this->checkAdvertiserSession(array('type' => 'location', 'value' => $id));
		$this->layout = "restaurant";
		
		if(!empty($this->data)) {
			//search criteria has been submitted
			if(empty($this->data['Location']['start_date']) || empty($this->data['Location']['end_date'])) {
				$this->Session->setFlash(__('Please enter a valid start date and end date to search your orders.', true));
				$hdr_txt = 'Please enter a valid start date and end date.';
			} else {
				$orders = $this->Location->getOrders($id, $this->data['Location']['start_date'], $this->data['Location']['end_date']);
				$hdr_txt = 'Orders From: <span class="italic red">'.date('D, M jS, Y', strtotime($this->data['Location']['start_date'])).'</span> To <span class="italic red">'.
						   date('D, M jS, Y', strtotime($this->data['Location']['end_date'])).'</span>';
			}
		} else {
			//other wise display all previous orders for past 30 days
			$orders = $this->Location->getOrders($id, $start_date = date('Y-m-d', strtotime("-30 days")), $end_date = date('Y-m-d'));
			$hdr_txt = 'Orders From: <span class="italic red">'.date('D, M jS, Y', strtotime($start_date)).'</span> To <span class="italic red">'.date('D, M jS, Y', strtotime($end_date)).'</span>';
		}
		$this->set('hdr_txt', $hdr_txt);
		
		$this->set('orders', $orders);
		$this->set('location', $this->Location->getFieldsForBanner($id));
	}
	
	function order_review() {
		$this->checkUserSession();
		$this->layout = "restaurant-order";
		
		if(!$this->Session->check('User.order_id')) {
			$this->Session->setFlash(__('You have not started an order yet!', true));
			$this->redirect(array('action' => 'index', 'controller'=>'locations'));
		}
		$order = $this->Location->Order->orderInfo($this->Session->read('User.order_id'));
		
		if($order['Order']['subtotal'] <= 0) {
			//empty order
			$this->Session->setFlash(__('Your order is empty!', true));
			$this->redirect('/menu/'.$this->Location->getUrlSlugs($this->Session->read('User.location_id')));
		}
		
		if($order['Location']['min_delivery_amt'] > 0 && $order['Location']['min_delivery_amt'] > $order['Order']['subtotal']) {
			//minimum delivery amount not reached yet
			$this->Session->setFlash(__('This restaurant has a minimum order amount of $'.number_format($order['Location']['min_delivery_amt'], 2).'.', true));
			$this->redirect('/menu/'.$this->Location->getUrlSlugs($this->Session->read('User.location_id')));
		}
		
		$fields = array('UsersAddr.id', 'UsersAddr.name', 'UsersAddr.address', 'UsersAddr.city', 'State.abbr', 'UsersAddr.default');
		$this->set('users', $this->Location->Order->User->UsersAddr->userAddresses($this->Session->read('User.id'), $fields));
		$this->set('order', $order);
		$this->set('states', $this->Location->State->find('list', array('fields' => array('abbr', 'name'))));
		
		$this->set('profiles', $this->Location->Order->User->UsersPaymentProfile->find('all', array('conditions' =>array('UsersPaymentProfile.user_id' => $this->Session->read('User.id')),
																									'recursive'  => -1)));
		
		$this->set('location', $this->Location->getFieldsForBanner($this->Session->read('User.location_id'), array('Location.active', 'Location.menu_ready', 'Location.advertiser_id', 'Location.id')));
	}
	
	function view($locslug = null) {
		if (!$locslug) {
			$this->Session->setFlash(__('You are trying to reach a restaurant that doesn\'t exist.', true));
			$this->redirect(array('action'=>'index'));
		}
		//even though we are only pulling one record we must use 'find all' to keep the array dimensions correct for google map data input
		$this->paginate = array('contain'   => array('Advertiser', 'State', 'ZipCode', 'FoodCat',
													 'LocationsTime' => array('conditions' => array('LocationsTime.day' => $this->getDayAbbr()))),
								'fields'    => array('Location.id','Location.latitude','Location.longitude','Location.advertiser_id','Location.name',
													 'Location.address1', 'Location.address2','Location.city','Location.state_id','ZipCode.id',
													 'Location.delivery_radius','Advertiser.logo','State.abbr', 'Location.logo', 'Location.use_logo_in_banner',  
												     'Location.min_delivery_amt', 'Location.delivery_charge', 'Location.slogan', 'Location.accept_deliveries', 
													 'Location.accept_pickups', 'Location.slug', 'Advertiser.slug', 'FoodCat.title'));
		$locs = $this->paginate('Location', array('Location.slug'=>$locslug));
		$locs[0]['Location']['currently_open'] = $this->Location->isLocationOpen($locs[0]['Location']['id']);
		$this->set('locations', $locs);
		
		/* Modified: 01/23/2010 - No longer show automatic directions for a restaurant, when a user is logged on
		 * We want to only display directions after an order has been placed!
		 */
		/*
		//if user is logged in, grab default address to show directions
		if($this->Session->check('User')) {
			$this->set('default_addr', $this->requestAction('/users_addrs/getDefaultAddress/'.$this->Session->read('User.id')));	
		}
		*/
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Location->getFeatured($lat, $long));
	}

	function add() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		if (!empty($this->data)) {
			$this->Location->set($this->data);
			if($this->Location->validates()) {
				$this->Location->create();
				$this->data['Location']['advertiser_id'] = $this->Session->read('Advertiser.id');
				if(empty($this->data['Location']['min_delivery_amt'])) unset($this->data['Location']['min_delivery_amt']);
				if(empty($this->data['Location']['delivery_charge'])) unset($this->data['Location']['delivery_charge']);
				if(empty($this->data['Location']['max_cash_amt'])) unset($this->data['Location']['max_cash_amt']);
				$this->data['Location']['address1'] = ucwords($this->data['Location']['address1']);
				$this->data['Location']['city'] = ucwords($this->data['Location']['city']);
				$this->data['Location']['contact_title'] = ucwords($this->data['Location']['contact_title']);
				
				$geocode_error = false;
				$coords = $this->Location->Geocode($this->data);
				if($coords['latitude']) {
					$this->data['Location']['latitude'] = $coords['latitude'];
					$this->data['Location']['longitude'] = $coords['longitude'];	
				} else {
					$geocode_error = true;
					$this->data['Location']['latitude'] = "0.00";
					$this->data['Location']['longitude'] = "0.00";
				}
				
				//check if logo is being uploaded and handle it if necessary
				if (!empty($this->data['Location']['logo']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Location']['logo']['name'], "/files/logos/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Location']['logo'], $fileName, "files/logos/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Location']['logo'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }	
		    	} else { $this->data['Location']['logo'] = ""; }
				
				if ($this->Location->save($this->data)) {
					if($geocode_error) {
						$this->sendGeocodeErrorEmail($this->Location->getLastInsertId());
					}
					
					//add records for opening and closing times
					$success = true;
					$order = 1;
					foreach($this->data['Location']['days'] as $abbr => $day):
						$this->Location->LocationsTime->create();
						$day['day'] = $abbr;
						$day['location_id'] = $this->Location->getLastInsertId();
						$day['order'] = $order++;
					
						//check if primary opening and closing times are properly set, if not clear data from rest of the fields for the day.
						if(empty($day['time_open']) || empty($day['time_closed']) || $day['closed'] == '1') {
							//store presumed closed, clear time fields
							$day['time_open'] = null; $day['time_closed'] = null; 
							$day['time_open2'] = null; $day['time_closed2'] = null;
							$day['closed'] = '1';
						} else if(empty($day['time_open2']) || empty($day['time_closed2'])) {
							//secondary not properly set, clear secondary fields
							$day['time_open2'] = null; $day['time_closed2'] = null;
						}
						//make sure to properly convert any times to military time for db
						if(!empty($day['time_open'])) $day['time_open'] = $this->timeToMilitary($day['time_open']);
						if(!empty($day['time_open2'])) $day['time_open2'] = $this->timeToMilitary($day['time_open2']);
						if(!empty($day['time_closed'])) $day['time_closed'] = $this->timeToMilitary($day['time_closed']);
						if(!empty($day['time_closed2'])) $day['time_closed2'] = $this->timeToMilitary($day['time_closed2']);
						
						if(!$this->Location->LocationsTime->save(array('LocationsTime' => $day))) $success = false;
					endforeach;
					
					if($success) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your new restaurant has been added successfully.', true));
						$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
					} else {
						$this->Location->del($this->Location->getLastInsertId());
						$this->Session->setFlash(__('There was a problem adding your new location. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('The Location could not be saved. Please, try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('Please fill out all the required form fields.', true));
			}
		}
		$this->set('states', $this->Location->State->activeStateList());
		$this->set('food_cats', $this->Location->FoodCat->find('list'));
		$this->set('banners', $this->Location->Banner->find('list', array('fields'=>'filename')));
	}

	function edit($slug = null) {
		if (!$slug && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
		}
		
		$loc_id = (!empty($this->data)) ? $this->data['Location']['id'] : $this->Location->idBySlug($slug);
		
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$loc_id));
		$this->layout = 'restaurant';
		
		if (!empty($this->data)) {
			$this->Location->set($this->data);
			if($this->Location->validates()) {
				if(empty($this->data['Location']['min_delivery_amt'])) unset($this->data['Location']['min_delivery_amt']);
				if(empty($this->data['Location']['delivery_charge'])) unset($this->data['Location']['delivery_charge']);
				if(empty($this->data['Location']['max_cash_amt'])) unset($this->data['Location']['max_cash_amt']);
				$this->data['Location']['address1'] = ucwords($this->data['Location']['address1']);
				$this->data['Location']['city'] = ucwords($this->data['Location']['city']);
				$this->data['Location']['contact_title'] = ucwords($this->data['Location']['contact_title']);
				
				$geocode_error = false;
				$coords = $this->Location->Geocode($this->data);
				if($coords['latitude']) {
					$this->data['Location']['latitude'] = $coords['latitude'];
					$this->data['Location']['longitude'] = $coords['longitude'];	
				} else {
					$geocode_error = true;
					$this->data['Location']['latitude'] = "0.00";
					$this->data['Location']['longitude'] = "0.00";
				}
				
				//check if logo is being uploaded and handle it if necessary
				if (!empty($this->data['Location']['logo_temp']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Location']['logo_temp']['name'], "/files/logos/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Location']['logo_temp'], $fileName, "files/logos/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Location']['logo'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }	
		    	}
				
				if ($this->Location->save($this->data)) {
					//send error email to admin about new saved addresses geocode error
					if($geocode_error) {
						$this->sendGeocodeErrorEmail($this->Location->getLastInsertId());
					}
					
					//update records for opening and closing times
					$success = true;
					foreach($this->data['Location']['days'] as $abbr => $day):		
						//check if primary opening and closing times are properly set, if not clear data from rest of the fields for the day.
						if(empty($day['time_open']) || empty($day['time_closed']) || $day['closed'] == '1') {
							//store presumed closed, clear time fields
							$day['time_open'] = null; $day['time_closed'] = null; 
							$day['time_open2'] = null; $day['time_closed2'] = null;
							$day['closed'] = '1';
						} else if(empty($day['time_open2']) || empty($day['time_closed2'])) {
							//secondary not properly set, clear secondary fields
							$day['time_open2'] = null; $day['time_closed2'] = null;
						} 
						//make sure to properly convert any times to military time for db
						if(!empty($day['time_open'])) $day['time_open'] = $this->timeToMilitary($day['time_open']);
						if(!empty($day['time_open2'])) $day['time_open2'] = $this->timeToMilitary($day['time_open2']);
						if(!empty($day['time_closed'])) $day['time_closed'] = $this->timeToMilitary($day['time_closed']);
						if(!empty($day['time_closed2'])) $day['time_closed2'] = $this->timeToMilitary($day['time_closed2']);
						
						if(!$this->Location->LocationsTime->save(array('LocationsTime' => $day))) $success = false;
					endforeach;
					
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Restaurant information successfully updated.', true));
					$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
				} else {
					$this->Session->setFlash(__('The restaurant information could not be saved. Please, try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
			$loc_id = $this->data['Location']['id'];
		}
		if (empty($this->data)) {
			//UPDATE THIS TO USE CONTAINABLE/BINDABLE BEHAVIOR
			$this->data = $this->Location->getLocationInfo($loc_id);
		}

		$this->set('states', $this->Location->State->activeStateList());
		$this->set('food_cats', $this->Location->FoodCat->find('list'));
		$this->set('location', $this->Location->getFieldsForBanner($loc_id));
		$this->set('banners', $this->Location->Banner->find('list', array('fields'=>'filename')));
		//need to create an array of locations, with index numbers that correspond with indexes in ad_locations array
		$this->set('current_loc_index', $this->Location->getArrayIndex($loc_id));
	}
	/*
	function subscription($id = null) {
		if(isset($this->data)) $id = $this->data['Location']['id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$id));
		$this->layout = 'restaurant';
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid id for Location', true));
			$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
		}
		
		if(!empty($this->data)) {
			//calculate the monthly bill amount & per order bill amount
			$this->data['Location']['arb_amt'] = 39;
			$this->data['Location']['per_order_amt'] = 0.27;
			
			if($this->data['Location']['accept_cc'] == 1) {
				$this->data['Location']['arb_amt'] += 10;
				$this->data['Location']['per_order_amt'] += 0.50;
			}
			if($this->data['Location']['notify_sms'] == 1) { 
				$this->data['Location']['per_order_amt'] += 0.09;
			}
			if($this->data['Location']['notify_fax'] == 1) { 
				$this->data['Location']['per_order_amt'] += 0.14; 
			}
			if($this->data['Location']['accept_discount_codes'] == 1) { 
				$this->data['Location']['arb_amt'] += 10; 
			}
			
			if($this->Location->save($this->data)) {
				$this->Session->write('Advertiser.subscription_location_id', $this->data['Location']['id']);
				$this->redirect(array('action'=>'subscription_review'));								
			} else {
				$this->Session->setFlash(__('There was a problem preparing your subscription. Please try again.', true));
			}
		}
		$this->set('location', $this->Location->read(null, $id));
	}
	
	function subscription_review() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		if(!$this->Session->check('Advertiser.subscription_location_id')) {
			$this->Session->setFlash(__('There was a problem preparing your subscription. Please try again.', true));
			$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
		}
		
		if(isset($this->data)) {
			if($this->data['Location']['agree'] == 0) {
				$this->Session->setFlash(__('Please check the box, indicating that you allow us to charge your card.', true));
			} else {
				//attempt to start a recurring billing subscription with the payment profile they have selected
				$profile = $this->Location->AdvertisersPaymentProfile->read('payment_profile_id', $this->data['Location']['advertisers_payment_profile_id']);
				$order_info = array('invoiceNumber' => 'ArbSub-AD'.$this->Session->read('Advertiser.id').'-LOC'.$this->Session->read('Advertiser.subscription_location_id'),
									'description'   => 'Recurring Billing Subscription Transaction');
				$response = $this->AuthorizeNet->cim_create_transaction($this->Session->read('Advertiser.profile_id'), 
																		$profile['AdvertisersPaymentProfile']['payment_profile_id'], 
																		$this->data['Location']['order_amt'],
																		$recurring = true, $order_info);
				$r = ($response['resultcode'] == 'Ok') ? 1 : 0;
				//Add the transaction table record
				$save = array('LocationsTransaction' => array('location_id'        => $this->Session->read('Advertiser.subscription_location_id'),
															  'payment_profile_id' => $profile['AdvertisersPaymentProfile']['payment_profile_id'],
															  'amount'             => $this->data['Location']['order_amt'],
															  'response'           => $r,
															  'timestamp'          => date("Y-m-d H:i:s"),
															  'invoice_number'     => $order_info['invoiceNumber']));
				$this->Location->LocationsTransaction->create();
				if($this->Location->LocationsTransaction->save($save)) {
					if($response['resultcode'] == 'Ok') {
						//recurring billing setup successfully. update location with new subscription id, and set location to active.
						$this->Location->id = $this->Session->read('Advertiser.subscription_location_id');
						//$this->Location->saveField('arb_subscription_id', $response['arbSubscriptionId']);
						if($this->Location->saveField('advertisers_payment_profile_id', $this->data['Location']['advertisers_payment_profile_id'])) {
							$this->Location->saveField('active', '1');
							$this->emailRegistrationSuccess($this->data['Location']['id']);
							$this->Session->setFlash(__('Your new restaurant subscription has been set up successfully, and is now active.', true));
							$this->Session->write('show_warning', '1');
							$this->redirect(array('action'=>'dashboard', 'controller'=>'advertisers'));
						} else {
							$this->Session->setFlash(__('There was a problem activating your subscription. Please try again.', true));	
						}	
					} else {
						$this->Session->setFlash(__('There was an error charging the payment profile: '.$response['text'].'.', true));
					}	
				} else {
					$this->Session->setFlash(__('There was an error adding the transaction record.', true));
				}
			}
			
		} 
		$loc = $this->Location->read(array('Location.name', 'Advertiser.name', 'Location.id', 'Location.arb_amt', 'Location.per_order_amt',
									 'Location.accept_cc', 'Location.notify_fax', 'Location.notify_sms'), 
									 $this->Session->read('Advertiser.subscription_location_id'));
		$this->set('location', $loc);
		
		$this->set('profiles', $this->Location->Advertiser->AdvertisersPaymentProfile->payProfileList($this->Session->read('Advertiser.id')));
	}
	*/
	
	function upgrade() {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		//if they have submitted the update membership form
		if(!empty($this->data)) { 
			//check if they are trying to use a reference code
			if(!empty($this->data['Location']['reference_code']) && $this->data['Location']['reference_code'] != FREE_TRIAL_CODE) {
				$this->Session->setFlash(__('The reference code you are trying to use is invalid.', true));
			} else {
				//first cycle through all restaurant checkboxes that are not currently disabled
				$active_ids = array();
				$success = true;
				$success_ct = 0;
				foreach($this->data['locations'] as $loc_id => $val):
					if($val == '1') { $active_ids[] = $loc_id; }
				endforeach;
				
				//make sure at least 1 restaurant is checked before we run the card
				if(sizeof($active_ids) > 0) {
					foreach($active_ids as $loc_id):
						$l = $this->Location->read(array('name', 'used_a_free_trial'), $loc_id);
						$cc_exp = $this->data['Location']['month']['month'] . '/' . substr($this->data['Location']['year']['year'], 2);
						
						$desc = 'Premium Account Upgrade - (#' . $this->Session->read('Advertiser.id') . ') ' . $this->Session->read('Advertiser.name') . ' - (#' . $loc_id . ') ' . $l['Location']['name'];
						$billing = array('fname' => $this->data['Location']['fname'], 
									 	 'lname' => $this->data['Location']['lname'],
									 	 'zip'   => $this->data['Location']['zip']);
						
						$valid_free_trial = ($this->data['Location']['reference_code'] == FREE_TRIAL_CODE && 
											 $l['Location']['used_a_free_trial'] == '0') ? true : false;
						
						//get next available transaction record id to thread as invoice number
						$next_id = $this->Location->Advertiser->AdvertisersPaymentProfile->AdvertisersPaymentProfilesTransaction->nextId();
						
						//authorize and charge first payment in subscription. if customer is using a free trial period, only authorize the amount
						$resp = $this->AuthorizeNet->chargeCard($this->data['Location']['cc_num'], $this->data['Location']['year']['year'], $this->data['Location']['month']['month'], 
																null, PREMIUM_ACCT_MONTHLY_FEE, '0', '0', $desc, $billing, 'OAS:'.$next_id, 
																(($valid_free_trial) ? "AUTH_ONLY" : "AUTH_CAPTURE"));					
						if($resp[1] != 1) {
							$success = false; //card was declined
							if($success_ct > 0) {
								$this->Session->setFlash(__($success_ct.' out of '.sizeof($active_ids).' processed successfully.', true));	
							} else {
								$this->Session->setFlash(__('There was a problem with your payment: '.$resp[4].'. Please try again.', true));
							}
							break;
						} else {
							//first charge successful, now set up the recurring billing to start next month
							$transaction_id = $resp[7];
							
							$resp = $this->AuthorizeNet->arb_create_subscription(PREMIUM_ACCT_MONTHLY_FEE, $this->data['Location']['cc_num'], $cc_exp, $billing, 
										 									 	 'Premium Membership Subscription '.$loc_id, $desc, 'OAS:'.$next_id, $valid_free_trial);
							if($resp['resultCode'] != 'Ok') {
								$success = false; //card was declined
								if($success_ct > 0) {
									$this->Session->setFlash(__($success_ct.' out of '.sizeof($active_ids).' processed successfully.', true));	
								} else {
									$this->Session->setFlash(__('There was a problem with your payment: '.$resp['text'].'. Please try again.', true));
								}
								break;
							} else {
								//entire transaction successful, subscription active
								$save = array('id' 						=> $loc_id,
											  'arb_subscription_id' 	=> $resp['subscriptionId'],
											  'acct_type' 				=> '2',
											  'featured'				=> '1',
											  'premium_acct_rate'		=> PREMIUM_ACCT_MONTHLY_FEE);
								if($valid_free_trial) {
									//mark off that they have used their free trial
									$save['used_a_free_trial'] = '1';
								}
								
								$this->Location->save(array('Location' => $save));
								
								$this->Location->Advertiser->AdvertisersPaymentProfile->saveTransaction(null, $transaction_id, PREMIUM_ACCT_MONTHLY_FEE, $loc_id, 
																										$desc.' Payment: 1.', 'Recurring');
								$success_ct++;
							}
						}	
					endforeach;
								
					if($success) {
						$this->Session->write('show_warning', '1');	
						$this->Session->setFlash(__('You have successfully updated '.$success_ct.' restaurants to Premium Memberships.', true));
						$this->redirect('/advertisers/dashboard');
					}
				} else {
					$this->Session->setFlash(__('Please select a least 1 new restaurant to proceed.', true));
				}
				//$this->set('resp', $resp);
			} 
		}
		
		$fields = array('Location.id', 'Location.name', 'Location.menu_ready', 'Location.active', 'Location.acct_type', 'Location.premium_acct_rate');
		$locs = $this->Location->allLocationsByAdvertiser($this->Session->read('Advertiser.id'), null, '-1', $fields);
		
		$ct = 0;
		$tot = 0;
		foreach($locs as $i => $l):
			if($l['Location']['acct_type'] == '2') {
				$ct++; $tot += $l['Location']['premium_acct_rate'];
			}
		endforeach;
		$locs['active_ct'] = $ct;
		$locs['active_tot'] = $tot;
		$this->set('locations', $locs);
		$this->set('states', $this->Location->State->find('list', array('fields' => array('abbr', 'name'))));
	}
	
	//unsubscribe a single restaurant from a premium membership
	function unsubscribe($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect($this->referer());
		}
		$this->checkAdvertiserSession(array('type' => 'location', 'value' => $id));
		
		//get subscription id
		$loc = $this->Location->read('arb_subscription_id', $id);
		
		$resp = $this->AuthorizeNet->arb_cancel_subscription($loc['Location']['arb_subscription_id']);
		
		if($resp['resultCode'] == 'Ok') {
			//cancelled successfully, set location back to free account
			$this->Location->save(array('Location' => array('id' 					=> $id,
															'arb_subscription_id' 	=> null,
															'premium_acct_rate'		=> null, 
															'featured'				=> '0',
															'acct_type'				=> '1')));
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('You have successfully cancelled your Premium Membership Subscription.', true));
		} else {
			$this->Session->setFlash(__('There was a problem cancelling your subscription. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function ajax_mass_add_form() {
		if($this->RequestHandler->isAjax()) {
			$this->set('amt', $this->params['form']['amt']);
			$this->set('default_label', $this->params['form']['label']);
			
			$this->set('topping_list', $this->Location->Topping->getToppingList());
			$this->set('topping_types', $this->Location->LocationsTopping->ToppingType->find('list'));	
		}
	}
	
	function mass_add_toppings($location_id) {
		if (!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->set('location_id', $location_id);
		$this->set('topping_types', $this->Location->LocationsTopping->ToppingType->find('list'));
	}
	
	function manage_categories($location_id) {
		if (!$location_id) {
			$this->Session->setFlash(__('Invalid Restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		
		$this->set('location_id', $location_id);
		$this->set('cats', $this->Location->getCats($location_id));
		$this->set('cat_list', $this->Location->Cat->getCatList());
	}
	/*
	function manage_ads($location_id = null) {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		$loc = $this->Location->find('first', array('conditions' => array('Location.id' => $location_id),
													'recursive'  => -1,
												    'fields'	 => array('Location.name', 'Location.id', 'Location.home_ads',
																		  'Location.sub_ads', 'Location.btn_ads')));
		$this->set('loc', $loc);
		$ads = $this->Location->LocationsAd->getAds($location_id);
		$rotation_count = array();
		foreach($ads as $type => $ad):
			$rotation_count[$type] = $this->Location->LocationsAd->find('count', array('conditions' => array('LocationsAd.location_id' => $location_id,
																											 'LocationsAd.rotation'    => '1',
																											 'LocationsAd.type'        => $type)));
		
		
			
		endforeach;
		$this->set('ads', $ads);
		$this->set('rotation_count', $rotation_count);
	}
	
	/*
	//process uploadify uploads for the locations controller
	function process_upload($upload_type, $filename) {
		$refresh = "";
		switch($upload_type) {
			case "HomeAd": $path = WWW_ROOT.'files/ads/home/';
				break;
		}
		
	    if (file_exists($path.$filename) === false) {
	        $this->Session->setFlash('The upload failed. Please try again. Path: '.$path.$filename);
	    } else {
	        $file =  file_get_contents($path.$filename);
	        if (!$file) {
	            $this->Session->setFlash('The upload failed because the file contents is malformed');
	        } else {
	            //
	            // DO STUFF HERE
	            //
	            $this->Session->setFlash('The file was uploaded succesfully');
	            //$refresh = "parent.location.reload();";
	            
	        }
	        //
	        // DELETE OR MOVE YOUR FILE HERE. MAKE SOME CHECKS BEFORE YOU DO THAT THOUGH.
	        //
	        //unlink("uploadify/files/".$filename);
	        //$this->set("refresh",$refresh);
	    }
		$this->redirect($this->referer());
	}
	*/
	
	function emailRegistrationSuccess($id) {
		$location = $this->Location->read(null, $id);
		$this->Email->to = $location['Location']['contact_email'];
		$this->Email->from = 'customer-service@orderaslice.com';
		$this->Email->subject = 'Congratulations! Your new Order A Slice restaurant is active!';
		$this->Email->sendAs = 'html';
		
		$this->set('location',$location);
		
		$this->Email->template = 'registration_success_location';
		if($this->Email->send()) {
			return true;
		} else { return false; }
	}
	
	function emailRegistrationActivationError($id) {
		$location = $this->Location->read(null, $id);
		$this->Email->to = 'admin@orderaslice.com';
		$this->Email->from = 'admin@orderaslice.com';
		$this->Email->subject = 'OrderASlice.com - LOCATION ACTIVATION ERROR';
		$this->Email->sendAs = 'html';
		
		$this->set('location',$location);
		
		$this->Email->template = 'registration_error_location.ctp';
		if($this->Email->send()) {
			return true;
		} else {
			return false;
		}
	}
	
	function toggle_menu_ready($id, $opt) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid Location.', true));
			$this->redirect(array('action'=>'dashboard', 'controller'=>'advertiser'));
		}
		$this->checkAdvertiserSession(array('type'=>'location', 'value'=>$id));
		$loc = $this->Location->read('active', $id);
		
		if($loc['Location']['active'] == '0') {
			$text = ($opt == '1') ? 'Thanks for letting us know your Online Menu is ready for review.' 
							  	  : 'Thanks for sending us your menu. We will have your Online Menu ready as soon as we can.';
		} else {
			$text = ($opt == '1') ? 'Your restaurant menu has been put back Online successfully.' 
							  	  : 'Your restaurant menu has been taken Offline successfully.';
		}
		
		$this->Location->id = $id;
		if($this->Location->saveField('menu_ready', $opt)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__($text, true));
		} else {
			$this->Session->setFlash(__('There was a problem updating your menu status. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function admin_toggle_menu_ready($id, $opt) {
		$this->checkAdminSession();
		$this->Location->id = $id;
		if($this->Location->saveField('menu_ready', $opt)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Restaurant menu status has been updated successfully.', true));
			
			//if we are saying that menu is ready for review, then send out 'Setting up a Billing Profile' email
			// ALSO WE ARE CURRENTLY AUTO UPGRADING RESTAURANTS TO A PREMIUM MEMBERSHIP
			if($opt == '1') {
				$location = $this->Location->read(array('contact_email', 'name'), $id);
				$this->Email->to = $location['Location']['contact_email'];
				$this->Email->from = 'customer-service@orderaslice.com';
				$this->Email->subject = 'Your Order a Slice menu has been created!';
				$this->Email->sendAs = 'html';
				
				$this->Email->template = 'logging_in_setting_up_billing_profile';
				$this->set('loc_name', $location['Location']['name']);
				
				if(!$this->Email->send()) {
					$this->Session->write('show_warning', false);
					$this->Session->setFlash(__('Restaurant menu status updated but email did not go out. Manually Send Email: "Logging in Setting up Billing Profile"', true));		
				}
				
				//AUTO UPGRADE TO PREMIUM MEMBERSHIP
				$save = array('id' 					=> $id,
							  'acct_type' 			=> '2',
							  'featured'			=> '1',
							  'premium_acct_rate' 	=> '0.00');
				if(!$this->Location->save($save)) {
					$this->Session->setFlash(__('Problem auto-upgrading to Premium Membership! Manually check to see WTF!', true));
				} 
			}
		} else {
			$this->Session->setFlash(__('There was a problem updating your menu status. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function toggle_active($id = null) {
		if(!$id) { $this->redirect('/advertisers/dashboard'); }
		$this->checkAdvertiserSession(array('type'=>'location', 'value'=>$id));
		
		
		$loc = $this->Location->read(array('active', 'Advertiser.is_billing_profile_set'), $id);
		$this->Location->id = $id;
		
		if($loc['Advertiser']['is_billing_profile_set'] == '1') {
			if($this->Location->saveField('active', (($loc['Location']['active'] == '1') ? '0' : '1'))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Restaurant status updated successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem updating the restaurant status. Please try again.', true));
			}	
		} else {
			$this->Session->setFlash(__('Restaurant can\'t be verified until a Billing Profile is set up.', true));
		}
		$this->redirect($this->referer());
	}
	
	function admin_toggle_active($id = null) {
		$this->checkAdminSession();
		if(!$id) { $this->redirect('/admin/advertisers/dashboard'); }
		
		$loc = $this->Location->read(array('active', 'Advertiser.is_billing_profile_set'), $id);
		$this->Location->id = $id;
		
		if($loc['Advertiser']['is_billing_profile_set'] == '1') {
			if($this->Location->saveField('active', (($loc['Location']['active'] == '1') ? '0' : '1'))) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Restaurant status updated successfully.', true));
			} else {
				$this->Session->setFlash(__('There was a problem updating the restaurant status. Please try again.', true));
			}	
		} else {
			$this->Session->setFlash(__('Restaurant can\'t be approved until a Billing Profile is set up.', true));
		}
		$this->redirect($this->referer());
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid restaurant. Please try again.', true));
			$this->redirect($this->referer());
		}
		$this->checkAdvertiserSession(array('type' => 'location', 'value' => $id));
		
		if ($this->Location->del($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your restaurant has been removed successfully.', true));
			$this->redirect('/advertisers/dashboard');
		} else {
			$this->Session->setFlash(__('There was a problem removing your restaurant. Please try again.', true));
		}
		$this->redirect($this->referer());
	}
	
	function admin_index() {
		$this->checkAdminSession();
		
		$conditions = array();
		if(!empty($this->data)) {
			$hdr_txt = '';
			//search by restaurant keywords AND city
			if(!empty($this->data['Location']['keywords']) && !empty($this->data['Location']['city'])) {
				$conditions['OR'] = array(array('Location.name LIKE' => "%".$this->data['Location']['keywords']."%",
							  		  	  		'Location.slogan LIKE' => "%".$this->data['Location']['keywords']."%",
							  		  	  		'Advertiser.name LIKE' => "%".$this->data['Location']['keywords']."%"),
										  array('Location.city LIKE' => "%".$this->data['Location']['city']."%",
										  		'Location.zip_code_id' => "%".$this->data['Location']['city']."%"));
				$hdr_txt .= 'Showing Restaurants in <span class="red">"'.$this->data['Location']['city'].'"</span> with keyword(s): <span class="red">"'.$this->data['Location']['keywords'].'"</span>.';
			} else if(!empty($this->data['Location']['keywords'])) {
				//search by restaurant keywords
				$conditions['OR'] = array('Location.name LIKE' => "%".$this->data['Location']['keywords']."%",
							  		  	  'Location.slogan LIKE' => "%".$this->data['Location']['keywords']."%",
							  		  	  'Advertiser.name LIKE' => "%".$this->data['Location']['keywords']."%");
				$hdr_txt .= 'Showing Restaurants with keyword(s): <span class="red">"'.$this->data['Location']['keywords'].'"</span>.';
			} else if(!empty($this->data['Location']['city'])) {
				//search by city or zip	
				$conditions['OR'] = array('Location.city LIKE' => "%".$this->data['Location']['city']."%",
										  'Location.zip_code_id' => "%".$this->data['Location']['city']."%");
				$hdr_txt .= 'Showing Restaurants in <span class="red">"'.$this->data['Location']['city'].'"</span>.';
			}
			
			//filter by menu status
			if($this->data['Location']['menu_status'] != '') {
				switch($this->data['Location']['menu_status']) {
					case '0':
					case '2':
						$conditions['Location.menu_ready'] = $this->data['Location']['menu_status'];	
						break;
					case '1':
						$conditions['Location.menu_ready'] = $this->data['Location']['menu_status'];	
						$conditions['Location.active'] = '0';
						break;
					case '3':
						$conditions['Location.menu_ready'] = '1';	
						$conditions['Location.active'] = '1';
						break;
				}	
			} else if($this->data['Location']['status'] != '') {
				//filter by restaurant status
				$conditions['Location.active'] = $this->data['Location']['status'];
			}
			
			//filter by menu cat
			if($this->data['Location']['cat_id'] != '') {
				$conditions['Location.food_cat_id'] = $this->data['Location']['cat_id'];
			}
		} else {
			$hdr_txt = 'Showing All Restaurants';
		}
		$locs = $this->Location->getLocsBySearch($conditions);
		
		$this->set('hdr_txt', $hdr_txt);
		$this->set('locations', $locs);
	}
	
	/*
	function admin_disable($id = null) {
		$this->checkAdminSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Location', true));
			$this->redirect(array('action'=>'index'));
		}
		if(empty($this->data)) {
			$this->data = $this->Location->read(null, $id);
		} 
		
		if($this->data['Location']['active'] == 1) {
			$this->data['Location']['active'] = 0;
			$active_text = "disabled!";
		} else {
			$this->data['Location']['active'] = 1;
			$active_text = "marked as paid!";
		}
		if ($this->Location->saveField('active', $this->data['Location']['active'])) {
			$this->Session->setFlash(__('The Location has been successfully '.$active_text, true));
		} else {
			$this->Session->setFlash(__('The Location could not be '.$active_text.'. Please, try again.', true));
		}
		$this->redirect(array('action'=>'index', $id));
	}
	*/
}
?>