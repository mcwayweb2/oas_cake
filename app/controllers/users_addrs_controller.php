<?php
class UsersAddrsController extends AppController {

	var $name = 'UsersAddrs';
	var $helpers = array('Html', 'Form', 'GoogleMap');
	var $components = array('Email');
	
	function getDefaultAddress($id) {
		return $this->UsersAddr->getDefault($id);
	}
	
	function manage_addresses() {
		$this->checkUserSession();
		$this->layout = "user";
		if(!empty($this->data)) {
			$this->UsersAddr->create();
			$this->UsersAddr->set($this->data);
			if($this->UsersAddr->validates($this->data)) {
				$coords = $this->UsersAddr->Geocode($this->data);	
				if($coords['latitude']) {
					$this->data['UsersAddr']['latitude'] = $coords['latitude'];
					$this->data['UsersAddr']['longitude'] = $coords['longitude'];
					$this->data['UsersAddr']['user_id'] = $this->Session->read('User.id');
					
					$default = $this->UsersAddr->find('first', array('conditions'=>array('UsersAddr.user_id'=>$this->Session->read('User.id'),
																						 'UsersAddr.default'=>1)));
					if(!$default) {
						//user has no address set yet...force default flag
						$this->data['UsersAddr']['default'] = 1;
					}
	
					if($this->UsersAddr->save($this->data)) {
						if($this->data['UsersAddr']['default'] == 1 && $default) {
							//remove old default flag
							$this->UsersAddr->id = $default['UsersAddr']['id'];
							$this->UsersAddr->saveField('default', 0);
						}
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Location successfully added.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your address.', true));
					}
				} else {
					$this->Session->setFlash(__('There was a problem locating your address. Please try again.', true));
				}
			}
		}
		$this->set('states', $this->UsersAddr->State->activeStateList());
		$this->set('addrs',  $this->paginate('UsersAddr', array('UsersAddr.user_id'=>$this->Session->read('User.id'))));
		
		$default = $this->UsersAddr->getDefault($this->Session->read('User.id'));
		if($default) { $this->set('default_addr',  $default); }
	}
	
	function set_default($id = null) {
		$this->checkUserSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for address.', true));
			$this->redirect($this->referer());
		}
		$default = $this->UsersAddr->find('first', array('conditions'=>array('UsersAddr.user_id'=>$this->Session->read('User.id'),
																						 'UsersAddr.default'=>1)));
		$this->UsersAddr->id = $id;
		if($this->UsersAddr->saveField('default', 1)) {
			$this->UsersAddr->id = $default['UsersAddr']['id'];
			$this->UsersAddr->saveField('default', 0);
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Your default delivery address has been updated successfully.', true));
		} else {
			$this->Session->setFlash(__('There was a problem updating your default delivery address.', true));
		}
		$this->redirect($this->referer());
	}
	
	function delete($id = null) {
		$this->checkUserSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User', true));
			$this->redirect($this->referer());
		}
		$addr = $this->UsersAddr->read(array('default', 'user_id'), $id);
		if($addr['UsersAddr']['user_id'] != $this->Session->read('User.id')) {
			$this->Session->setFlash(__('That address does not belong to you.', true));
			$this->redirect($this->referer());
		}
		if($addr['UsersAddr']['default'] == 1) {
			$this->Session->setFlash(__('You cannot delete your primary address.', true));
			$this->redirect($this->referer());
		}
		if ($this->UsersAddr->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Address successfully removed.', true));
		} else {
			$this->Session->setFlash(__('There was a problem deleting your address.', true));
		}
		$this->redirect($this->referer());
	}
	
}
?>