<?php
class OrdersItemsController extends AppController {

	var $name = 'OrdersItems';
	
	function verifyIdentity($item_id) {
		$item = $this->OrdersItem->read('order_id', $item_id);
		return ($item['OrdersItem']['order_id'] == $this->Session->read('User.order_id')) ? true : false;
	}

	function add($item_id = null) {
		$this->checkUserSession();
		if(!$item_id && empty($this->data)) {
			$this->Session->setFlash(__('Error adding item. Invalid item.', true));
			$this->redirect($this->referer());
		}
		
		if(!empty($this->data)) {
			$this->OrdersItem->create();
			
			$item = $this->OrdersItem->Item->read('price', $this->data['OrdersItem']['item_id']);
			$this->data['OrdersItem']['price'] = $item['Item']['price'];
			
			if($this->OrdersItem->save($this->data)) {
				$success = true;
				$opt_total = 0;
				//are there any item options to take care of?
				if(isset($this->data['ItemsOption'])) {
					foreach($this->data['ItemsOption'] as $add_group_id => $opts):
						if($add_group_id > 0) {
							// selection series
							foreach($opts as $add_group_index => $option_id):
								//check to see if this option has a price adjustment
								$item_opt = $this->OrdersItem->ItemsOption->read('price', $option_id);
								$opt_total += $item_opt['ItemsOption']['price'];
							
								$this->OrdersItem->OrdersItemsItemsOption->create();
								if(!$this->OrdersItem->OrdersItemsItemsOption->save(array('OrdersItemsItemsOption' => array('orders_item_id'  => $this->OrdersItem->getLastInsertId(),
																															'items_option_id' => $option_id,
																															'price'           => $item_opt['ItemsOption']['price'])))) { 
									$success = false;
								}
							endforeach;
						} else {
							// additional add-on options
							foreach($opts as $option_id => $val):
								if($val == '1') {
									//check to see if this option has a price adjustment
									$item_opt = $this->OrdersItem->ItemsOption->read('price', $option_id);
									$opt_total += $item_opt['ItemsOption']['price'];
									
									$this->OrdersItem->OrdersItemsItemsOption->create();
									if(!$this->OrdersItem->OrdersItemsItemsOption->save(array('OrdersItemsItemsOption' => array('orders_item_id'  => $this->OrdersItem->getLastInsertId(),
																																'items_option_id' => $option_id,
																																'price'			  => $item_opt['ItemsOption']['price'])))) { 
										$success = false;
									}
								}
							endforeach;
						}
					endforeach;
				}
				
				//are there any cat options to take care of?
				if(isset($this->data['CatsOption'])) {
					foreach($this->data['CatsOption'] as $add_group_id => $opts):
						if($add_group_id > 0) {
							// selection series
							foreach($opts as $add_group_index => $option_id):
								//check to see if this cat option has a price adjustment
								$cat_opt = $this->OrdersItem->LocationsCatsOption->read('additional_price', $option_id);	
								$opt_total += $cat_opt['LocationsCatsOption']['additional_price'];
							
								$this->OrdersItem->OrdersItemsLocationsCatsOption->create();
								if(!$this->OrdersItem->OrdersItemsLocationsCatsOption->save(array('OrdersItemsLocationsCatsOption' => 
																								  array('orders_item_id'           => $this->OrdersItem->getLastInsertId(),
																								  		'locations_cats_option_id' => $option_id,
																								  		'price'					   => $cat_opt['LocationsCatsOption']['additional_price'])))) { 
									$success = false;
								}
							endforeach;
						} else {
							// additional add-on options
							foreach($opts as $option_id => $val):
								if($val == '1') {
									//check to see if this option has a price adjustment
									$cat_opt = $this->OrdersItem->LocationsCatsOption->read('additional_price', $option_id);	
									$opt_total += $cat_opt['LocationsCatsOption']['additional_price'];
									
									$this->OrdersItem->OrdersItemsLocationsCatsOption->create();
									if(!$this->OrdersItem->OrdersItemsLocationsCatsOption->save(array('OrdersItemsLocationsCatsOption' => 
																								  	  array('orders_item_id'           => $this->OrdersItem->getLastInsertId(),
																								  			'locations_cats_option_id' => $option_id,
																								  	  		'price'					   => $cat_opt['LocationsCatsOption']['additional_price'])))) { 
										$success = false;
									}
								}
							endforeach;
						}
					endforeach;
				}
				
				if($success) {
					//update line total in the orders_item record
					$tot = $this->data['OrdersItem']['qty'] * ($this->data['OrdersItem']['price'] + $opt_total);
			
					if($this->OrdersItem->save(array('OrdersItem' => array('id' => $this->OrdersItem->getLastInsertId(),
																			'total' => $tot,
																			'additional_opts_price' => $opt_total)))) {
						//update order total in session
						if($this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $tot)) {
							$this->Session->write('show_warning', '1');
							$this->Session->setFlash(__('Item added to your order.', true));	
						} else {
							$this->Session->setFlash(__('There was a problem adding the item to your order. Please try again', true));
						}
					} else {
						$this->Session->setFlash(__('There was a problem adding the item to your order. Please try again', true));
					}	
				} else {
					//problem somewhere during the add record process, delete new record so user can try again
					$this->Session->setFlash(__('There was a problem adding the menu item to your order. Please try again.', true));
					$this->OrdersItem->delete($this->OrdersItem->getLastInsertId());
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding the menu item to your order. Please try again.', true));
			}
			$this->redirect($this->referer());
		}
		
		$this->set('item', $this->OrdersItem->Item->getItemInfo($item_id));
	}
	
	function edit($orders_item_id = null) {
		//Configure::write('debug', 0);
		$this->checkUserSession();
		if(!$orders_item_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid menu item.', true));
			$this->redirect('/menu/'.$this->OrdersItem->Order->Location->getUrlSlugs($this->Session->read('User.location_id')));
		}
		
		if(!empty($this->data)) {
			$item = $this->OrdersItem->Item->read('price', $this->data['OrdersItem']['item_id']);
			$this->data['OrdersItem']['price'] = $item['Item']['price'];
			$old_total = $this->data['OrdersItem']['total'] * -1;
			$this->data['OrdersItem']['total'] = $this->data['OrdersItem']['qty'] * $this->data['OrdersItem']['price'];
			
			if($this->OrdersItem->save($this->data)) {
				//subtract the old total before adding the new total
				$this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $old_total);
				
				if($this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $this->data['OrdersItem']['total'])) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Order item has been updated.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem editing your order item. Please try again', true));
				}
			} else {
				$this->Session->setFlash(__('There was a problem editing your order item. Please try again', true));
			}
			$this->redirect('/locations/order_review');
		} else {
			$this->data = $this->OrdersItem->read(null, $orders_item_id);
		}
		$this->set('item', $this->OrdersItem->Item->read(null, $this->data['OrdersItem']['item_id']));
	}
	
	function delete($item_id) {
		$this->checkUserSession(array('order_item' => $item_id));

		//get line total before deleting record to decrement order subtotal
		$item = $this->OrdersItem->read('total', $item_id);
		
		if($this->OrdersItem->delete($item_id)) {
			if($this->OrdersItem->Order->updateOrderTotals($this->Session->read('User.order_id'), $item['OrdersItem']['total'] * -1)) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Item removed from your order.', true));	
			} else {
				$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));
			}	
		} else {
			$this->Session->setFlash(__('There was a problem removing your order item. Please try again.', true));	
		}
		$this->redirect($this->referer());
	}

}
?>