<?php
class CombosController extends AppController {

	var $name = 'Combos';
	var $components = array('FileUpload');
	
	function verifyIdentity($combo_id) {
		$combo = $this->Combo->read(array('location_id'), $combo_id);
		$location = $this->Combo->Location->read('advertiser_id', $combo['Combo']['location_id']);
		return ($this->Session->read('Advertiser.id') == $location['Location']['advertiser_id']) ? true : false;
	}
	
	function add($location_id = null) {
		if (!$location_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid restaurant.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $location_id = $this->data['Combo']['location_id'];
		$this->checkAdvertiserSession(array('type'=>'location','value'=>$location_id));
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			$this->Combo->set($this->data);
			if(!$this->Combo->validates()) {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			} else {
				//see if there is an uplaod to take care of
				if (!empty($this->data['Combo']['image']['tmp_name'])) {
		      		$fileName = $this->FileUpload->generateUniqueFilename($this->data['Combo']['image']['name'], "/files/combos/");
		      		$error = $this->FileUpload->handleFileUpload($this->data['Combo']['image'], $fileName, "files/combos/");
				    if(!$error) {
				    	//file upload was ok
				    	$this->data['Combo']['image'] = $fileName;
				    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		    	} else { unset($this->data['Combo']['image']); }

		    	$this->Combo->create();
		    	if($this->Combo->save($this->data)) {
		    		$this->Session->write('show_warning', '1');
	    			$this->Session->setFlash(__('Your new combo has been created. Now build what is in the combo using the buttons below.', true));
	    			$this->redirect('/combos/edit/'.$this->Combo->getLastInsertId());
		    	} else {
		    		$this->Session->setFlash(__('There was a problem adding your Combo Special. Please try again.', true));
		    	}
			}
		}
		$this->set('location_slug', $this->Combo->Location->getSlugFromId($location_id));
		$this->set('location_id', $location_id);
	}
	
	function edit($combo_id = null) {
		//Configure::write('debug', '0');
		if (!$combo_id) {
			$this->Session->setFlash(__('Invalid Combo Special.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$combo_id));
		$this->layout = 'restaurant';
		$this->set('combo', $this->Combo->getComboInfo($combo_id));
	}
	
	function edit_image() {
		if(!empty($this->data['Combo']['image']['tmp_name'])) {
			$this->checkAdvertiserSession(array('type'=>'combo','value'=>$this->data['Combo']['id']));
			
			$fileName = $this->FileUpload->generateUniqueFilename($this->data['Combo']['image']['name'], "/files/combos/");
      		$error = $this->FileUpload->handleFileUpload($this->data['Combo']['image'], $fileName, "files/combos/");
		    if(!$error) {
		    	//file upload was ok
		    	$this->Combo->id = $this->data['Combo']['id'];
		    	if($this->Combo->saveField('image', $fileName)) {
		    		$this->Session->write('show_warning', '1');
		    		$this->Session->setFlash(__('Your combo image has been successfully updated.', true));
		    	} else {
		    		$this->Session->setFlash(__('There was a problem updating your combo image. Please try again.', true));
		    	}
		    } else { $this->FileUpload->deleteMovedFile(WWW_ROOT.IMAGES_URL.$fileName); }
		} else {
			$this->Session->setFlash(__('Please select an image file to upload.', true));
		}
		$this->redirect($this->referer());
	}
	
	function ajax_order() {
		$success = true;
		if($this->RequestHandler->isAjax()) {
			/* still need to validate that all combo ids belong to logged in advertiser */
			
			unset($this->params['url']['url']);	
			foreach(current($this->params['url']) as $order => $id):
				$this->Combo->id = $id;
				if(!$this->Combo->saveField('order', $order)) $success = false;
			endforeach;
		}
		$this->set('result', (($success) ? "New Combo Order Saved!" : "Trouble Saving New Combo Order. Please try again!"));
	}
	
	function ajax_edit() {
		if($this->RequestHandler->isAjax() && !empty($this->data['Combo']['id'])) {
			App::import('Core', 'sanitize');
			//strip the actual id value from the string.
			$parts = explode("_", $this->data['Combo']['id']);
			$field = $parts[1];
			$this->data['Combo']['id'] = $parts[2];
			
			if($this->verifyIdentity($this->data['Combo']['id'])) {
				$this->Combo->id = $this->data['Combo']['id'];
				$c = $this->Combo->read($field, $this->data['Combo']['id']);
				//sanitize data since we are not validating it.
				$this->data['Combo']['field'] = Sanitize::clean($this->data['Combo']['field']);
				
				//strip dollar sign if updating the price
				if($field == 'price') {
					$this->data['Combo']['field'] = str_replace('$', '', $this->data['Combo']['field']);
					//make sure price data is valid
					if(is_numeric($this->data['Combo']['field'])) {
						$value = ($this->Combo->saveField($field, $this->data['Combo']['field'])) ? $this->data['Combo']['field'] : $c['Combo']['field'];
					} else { $value = $c['Combo']['field']; } 
				} else {
					$value = ($this->Combo->saveField($field, $this->data['Combo']['field'])) ? $this->data['Combo']['field'] : $c['Combo']['field'];
				}
				
				//if we are updating the price field, we need to make sure to keep the dollar sign after the update
				if($field == 'price') { $value = number_format($value, 2); }
				$this->set('value', $this->data['Combo']['field']);
			}	
		}			
	}
		
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid pizza id.', true));
		} else {
			$this->checkAdvertiserSession(array('type'=>'combo','value'=>$id));
			
			$c = $this->Combo->read('image', $id);
			
			//delete image on server if necessary
			if(!empty($c['Combo']['image']) && file_exists(WWW_ROOT.'files/combos/'.$c['Combo']['image'])) {
				unlink('files/combos/'.$c['Combo']['image']);	
			}

			if($this->Combo->delete($id)) {
				//still have to delete any associated: items.
				$this->Combo->CombosItem->deleteAll(array('CombosItem.combo_id' => $id), false);
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('Your Menu Combo has been successfully deleted.', true));	
			} else {
				$this->Session->setFlash(__('There was a problem removing your Menu Combo. Please try again.', true));	
			}	
		}
		$this->redirect($this->referer().'#mnuCombos');
	}
}
?>