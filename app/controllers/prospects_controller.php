<?php
class ProspectsController extends AppController {

	var $name = 'Prospects';
	var $helpers = array('Html', 'Form', 'GoogleMap');
	var $components = array('Email', 'MaxMind');
	
	function import_csv($filename = null) {
		$this->checkAdminSession();
		Configure::write('debug', '1');
		
		if(!$filename) $filename = WWW_ROOT.'/csv/prospects/SanDiegoCounty_ItalianRestaurants.txt';
		$already_in_db = array();
		$errors = array();
		$success = array();
		$geocode_errors = 0;
		
		if (($handle = fopen($filename, "r")) !== FALSE) {
			//get header row
			$csv_headers = fgetcsv($handle, 0);
			
			while (($line = fgetcsv($handle, 0)) !== FALSE) {
				$save = array('Prospect' => array('name' 			=> $line[0], 
												  'contact_fname' 	=> $line[15], 
												  'contact_lname' 	=> $line[16], 
												  'contact_title' 	=> $line[17], 
							  					  'phone' 			=> str_replace(array('-',' '), array('',''), $line[8]), 
							  					  'fax' 			=> str_replace(array('-',' '), array('',''), $line[9]),  
							  					  'address' 		=> $line[3], 
							  					  'city' 			=> $line[4], 
							  					  'state_id' 		=> '5',
												  'zip_code_id' 	=> $line[6],
												  'owner_type'		=> $line[19],
												  'sic_desc'		=> $line[11], 
							  					  'lead_source'		=> "Import",   
							  					  'timestamp' 		=> date("Y-m-d H:i:s")));
				
				//check to see if this restaurant is already present in db
				$r = $this->Prospect->find('first', array('conditions' => array('Prospect.name' 		=> $save['Prospect']['name'],
																				'Prospect.address'  	=> $save['Prospect']['address'],
																				'Prospect.zip_code_id' 	=> $save['Prospect']['zip_code_id'])));
				if($r) {
					//already exists
					$already_in_db[] = $save;
				} else {
					//add to db, geocode for lat/long
					$coords = $this->Prospect->Geocode($save);
					if($coords['latitude']) {
						$save['Prospect']['latitude'] = $coords['latitude'];
						$save['Prospect']['longitude'] = $coords['longitude'];	
					} else {
						$geocode_errors++;
						$save['Prospect']['latitude'] = "0.00";
						$save['Prospect']['longitude'] = "0.00";
					}
					
					$this->Prospect->create();
					if($this->Prospect->save($save)) {
						$success[] = $save;
					} else {
						$errors[] = $save;
					}
				}
			}
			$this->set('success', $success);
			$this->set('errors', $errors);
			$this->set('already_in_db', $already_in_db);
			$this->set('geocode_errors', $geocode_errors);
			
			fclose($handle);		
		} else {
			$this->Session->setFlash(__('Error opening import file: '.$filename, true));
		}
	}
	
	function add() {
		if (!empty($this->data)) {
			$this->Prospect->set($this->data);
			if($this->Prospect->validates()) {
				$this->Prospect->create();
				$this->data['Prospect']['timestamp'] = date("Y-m-d h:i:s");
				
				if($this->Prospect->save($this->data)) {
					if ($this->email()) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Thanks. We will do our best to get your requested restaurant listed on our site.', true));
						$this->redirect('/locations/index');
					} else {
						$this->Session->setFlash(__('The Email could not be sent. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields, and try again.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
		}
		$this->set('states', $this->Prospect->State->find('list', array('fields' => array('State.name'))));
		
		//featured restaurants query based on visitors geolocation by ip
		$ip_info = $this->MaxMind->getIpInfoByDatDb();
		$lat = isset($ip_info->latitude) ?: DEFAULT_LATITUDE;
		$long = isset($ip_info->longitude) ?: DEFAULT_LONGITUDE;
		$this->set('featured_restaurants', $this->Prospect->State->Location->getFeatured($lat, $long));
	}
	
	function email() {
		$this->Email->to = "customer-service@orderaslice.com";
		$this->Email->replyTo = 'customer-service@orderaslice.com';
		$this->Email->from = 'no-reply@orderaslice.com';
		$this->Email->subject = 'Prospect Form -> Restaurant Prospect from OrderASlice!';
		
		//set email vars
		$this->set('prospect',$this->data['Prospect']);
		$this->set('state',$this->Prospect->State->read('name', $this->data['Prospect']['state_id']));
		
		
		$this->Email->sendAs = 'html';
		$this->Email->template = 'prospect';
		return ($this->Email->send()) ? true : false;
	}
	
	function admin_index($add = false) {
		//Configure::write('debug', '2');
		$this->checkAdminSession();
		
		
		$conditions = array();
		$order = array('Prospect.name');
		$hdr_txt = '';
		
		$fields = array('Prospect.id', 'Prospect.name', 'Prospect.contact_fname', 'Prospect.contact_lname', 'Prospect.contact_title',
						'Prospect.address', 'Prospect.city', 'Prospect.sic_desc', 'State.abbr', 'ZipCode.id', 'Prospect.contacted', 
						'Prospect.response_notes', 'Prospect.contacted_timestamp', 'Prospect.latitude', 'Prospect.longitude');
		
		if(!empty($this->data)) {
			if($this->data['Prospect']['print_view'] == '1') {
				$this->layout = 'admin-doc-view';
				$this->set('print_view_mode', true);
			}
			
			//search criteria submitted
			if(!empty($this->data['Prospect']['keywords'])) {
				$conditions['OR'] = array('Prospect.name LIKE' 					=> "%".$this->data['Prospect']['keywords']."%",
							  		  	  'Prospect.contact_fname LIKE' 		=> "%".$this->data['Prospect']['keywords']."%",
							  		  	  'Prospect.contact_lname LIKE' 		=> "%".$this->data['Prospect']['keywords']."%",
										  'Prospect.address LIKE' 				=> "%".$this->data['Prospect']['keywords']."%",
										  'Prospect.contacted_timestamp LIKE' 	=> "%".$this->data['Prospect']['keywords']."%",
										  'Prospect.sic_desc LIKE' 				=> "%".$this->data['Prospect']['keywords']."%",
							  		  	  'Prospect.response_notes LIKE' 		=> "%".$this->data['Prospect']['keywords']."%",
						      		  	  'Prospect.contact_title LIKE' 		=> "%".$this->data['Prospect']['keywords']."%");
				if(!empty($this->data['Prospect']['city'])) {
					//both search terms set
					$hdr_txt = 'Showing Restaurants in <span class="red">"'.$this->data['Prospect']['city'].'"</span> with keyword(s): <span class="red">"'.$this->data['Prospect']['keywords'].'"</span>. ';
				} else {
					$hdr_txt = 'Showing Restaurants with keyword(s): <span class="red">"'.$this->data['Prospect']['keywords'].'"</span>. ';					
				}
			}
			
			if(!empty($this->data['Prospect']['city'])) {
				//make sure they are searching with a zip code, if searching with radial ssearch
				if($this->data['Prospect']['search_type'] == 'Radial') {
					//search type is radial	
					$type_txt = 'near'; 
					
					$searchCoords = $this->Prospect->Geocode($this->data['Prospect']['city']);			
					$conditionString = $this->Prospect->State->Location->getDistanceFormula($searchCoords);
					
					$condition = $conditionString . " <?php ";
					$conditions[$condition] = " 100";
					$distance = $conditionString . " as distance";
					$fields[] = $distance;
					$order = array('distance' => 'ASC');
				} else {
					//search type is exact match
					if(isset($conditions['OR'])) {
						$conditions['AND'] = array('OR' => array('Prospect.city LIKE' 		 => "%".$this->data['Prospect']['city']."%", 
																 'Prospect.zip_code_id LIKE' => "%".$this->data['Prospect']['city']."%",
																 'Region.name LIKE' 		 => "%".$this->data['Prospect']['city']."%"));
					} else { $conditions['OR'] = array('Prospect.city LIKE' 	   => "%".$this->data['Prospect']['city']."%", 
													   'Prospect.zip_code_id LIKE' => "%".$this->data['Prospect']['city']."%",
													   'Region.name LIKE' 		   => "%".$this->data['Prospect']['city']."%"); }
					$type_txt = 'in';
				}
				
				if(!isset($hdr_txt)) {
					$hdr_txt = 'Showing Restaurants '.$type_txt.' <span class="red">"'.$this->data['Prospect']['city'].'"</span>. ';
				}
			}
			
			if($this->data['Prospect']['contacted'] != "") {
				$conditions['Prospect.contacted'] = $this->data['Prospect']['contacted']; 
			}
			
			if($this->data['Prospect']['show_map'] == '1') {
				$this->set('show_map', true);
			}
		}
		
		$this->paginate = array('order'  => $order,
								'fields' => $fields,
								'limit'  => (isset($this->data['Prospect']['page_limit']) && $this->data['Prospect']['page_limit'] > 0) ? $this->data['Prospect']['page_limit'] : '10');
		$prospects = $this->paginate('Prospect', $conditions);
		$hdr_txt .= 'Showing <span class="b">'.sizeof($prospects).'</span> records total.';
		$this->set('hdr_txt', $hdr_txt);
		$this->set('prospects', $prospects);
		$this->set('regions', $this->Prospect->Region->find('list'));
		$this->set('show_add_form', $add);
	}
	
	function admin_add() {
		Configure::write('debug', '0');
		$this->checkAdminSession();
		if (!empty($this->data)) {
			$this->Prospect->set($this->data);
			if($this->Prospect->validates()) {
				$this->Prospect->create();
				$this->data['Prospect']['timestamp'] = date("Y-m-d h:i:s");
				$this->data['Prospect']['lead_source'] = "Manual";
				
				//parse address field
				$parts = explode(",", str_replace(array("\r\n", "\n", "\r"), ",", $this->data['Prospect']['address']));
				$this->data['Prospect']['address'] = $parts[0];
				$this->data['Prospect']['name'] = trim($this->data['Prospect']['name']);
				$this->data['Prospect']['city'] = ltrim($parts[1]);
				$this->data['Prospect']['phone'] = trim($this->stripPhoneFormat($this->data['Prospect']['phone_temp']));
				$state_zip = ltrim($parts[2]);
				
				//check to make sure this address has not already been added.
				$prospect = $this->Prospect->find('first', array('conditions' => array('Prospect.address'     => $parts[0],
																					   'Prospect.zip_code_id' => substr($state_zip, 3, 5))));
				if($prospect) {
					//address already in use
					$this->Session->setFlash(__('This address is already in the database.', true));	
				} else {
					$state = $this->Prospect->State->findByAbbr(substr($state_zip, 0, 2));
					if($state) {
						$this->data['Prospect']['state_id'] = $state['State']['id'];
						$this->data['Prospect']['zip_code_id'] = substr($state_zip, 3, 5);
						
						//get lat/long
						$searchCoords = $this->Prospect->Geocode($this->data['Prospect']['address']);
						if($searchCoords) {
							$this->data['Prospect']['latitude'] = $searchCoords['latitude'];
							$this->data['Prospect']['longitude'] = $searchCoords['longitude'];
						}
						
						if($this->Prospect->save($this->data)) {
							$this->Session->write('show_warning', '1');
							$this->Session->setFlash(__('Restaurant Lead added successfully.', true));
							$this->redirect($this->referer().'/true');
						} else {
							$this->Session->setFlash(__('There was a problem adding the restaurant. Please try again.', true));
						}	
					} else {
						$this->Session->setFlash(__('Could not find state: "'.substr($state_zip, 0, 2).'". Please try again.', true));
					}
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
		}
		$this->redirect($this->referer());
	}
	
	function admin_delete($id = null) {
		$this->checkAdminSession();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Prospect', true));
			$this->redirect($this->referer());
		}
		if ($this->Prospect->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Restaurant Lead successfully removed.', true));
		} else {
			$this->Session->setFlash(__('There was a problem removing the Restaurant Lead. Please try again.', true));
		}
		$this->redirect($this->referer());
	} 
	
	function admin_multiple_delete() {
		$this->checkAdminSession();
		if(!empty($this->data['locs'])) {
			$success = true;
			$ct = 0;
			foreach($this->data['locs'] as $id => $val):
				if($val == '1') {
					if (!$this->Prospect->delete($id)) { $success = false; }
					else { $ct++; }
				}
			endforeach;
			
			if($success) {
				$this->Session->write('show_warning', '1');
				$this->Session->setFlash(__('('.$ct.') Restaurants deleted successfully!', true));
			} else {
				$this->Session->setFlash(__('Problem removing restaurants! Please try again.', true));
			}
		}
		$this->redirect($this->referer());
	}
	
	
	function ajax_admin_mark_contacted() {
		$this->checkAdminSession();
		if($this->RequestHandler->isAjax()) {
			$this->Prospect->id = $this->params['form']['id'];
			if ($this->Prospect->saveField('contacted', '1')) {
				$this->Prospect->saveField('contacted_timestamp', date('Y-m-d H:i:s'));
				
				$response = '<div class="base b pad-l5 txt-l">Contacted:<br />'.date('Y-m-d H:i:s').'</div>';
			} else {
				$response = '<span class="red b">Failure</span>';
			}	
		} else { $response = false; }
		$this->set('response', $response);
		$this->layout = null;
	}
	
	function ajax_admin_comment_response() {
		$this->checkAdminSession();
		if($this->RequestHandler->isAjax()) {
			$success = true;
			if(isset($this->params['form']['id']) && isset($this->params['form']['comment'])) {
				$this->Prospect->id = $this->params['form']['id'];
				if(!$this->Prospect->saveField('response_notes', $this->params['form']['comment'])) $success = false;
			} else { $success = false; }
		} else { $success = false; }
		$this->set('response', true);
	}
}
?>