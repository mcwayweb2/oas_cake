<?php
class ZipCodesController extends AppController {

	var $name = 'ZipCodes';
	
	function import_csv($filename = null) {
		Configure::write('debug', '1');
		//$this->checkAdminSession();
		
		if(!$filename) $filename = WWW_ROOT.'/csv/zip_codes_ca_2011_10.csv';
		//if(!$filename) $filename = WWW_ROOT.'/csv/zip_codes_hi.csv';
		
		if (($handle = fopen($filename, "r")) !== FALSE) {
			//remove all existing zip code records
			//$this->ZipCode->deleteAll(array('NOT' => array('ZipCode.id' => '00000')));
			
			//get header row
			$csv_headers = fgetcsv($handle, 0);
			
			$success = array();
			$lines = array();
			while (($line = fgetcsv($handle, 0)) !== FALSE) {
				if($line[16] == '1') {
					//this is the zip codes primary record
					$this->ZipCode->create();
					//save schema for California
					
					$save = array('ZipCode' => array('id' => $line[1], 'tax_rate' => $line[2], 'tax_rate_state' => $line[3], 'tax_rate_county' => $line[5], 
								  					 'tax_rate_city' => $line[7], 'tax_rate_special' => $line[9], 'city' => $line[11], 
								  					 'state' => $line[13], 'county' => $line[14], 'shipping_taxable' => $line[15]));
					/*
					//save schema for Hawaii
					$save = array('ZipCode' => array('id' => $line[1], 'tax_rate' => $line[2], 'tax_rate_state' => $line[3], 'tax_rate_county' => $line[5], 
								  					 'tax_rate_city' => $line[7], 'tax_rate_special' => $line[9], 'city' => $line[11], 
								  					 'state' => $line[13], 'county' => $line[14], 'shipping_taxable' => $line[15]));
					*/
					
					if($this->ZipCode->save($save)) {
						$success[] = $line[1];
					}	
				} else { $lines[] = $line; }
			}
			$this->set('zip_codes', $success);
			$this->set('lines', $lines);
			
			fclose($handle);		
		} else {
			$this->Session->setFlash(__('Error opening the csv file.', true));
		}
	}
	
	function slug_that_shit() {
		$zips = $this->ZipCode->find('all');
		
		$success = array();
		$failure = array();
		
		foreach($zips as $i => $zip):
			$this->ZipCode->id = $zip['ZipCode']['id'];
			if($this->ZipCode->saveField('slug', str_replace(' ', '-', $zip['ZipCode']['city']))) {
				$success[$zip['ZipCode']['id']] = str_replace(' ', '-', $zip['ZipCode']['city']);
			} else {
				$failure[$zip['ZipCode']['id']] = str_replace(' ', '-', $zip['ZipCode']['city']);
			}
		endforeach;
		$this->set('failure', $failure);
		$this->set('success', $success);
	}
	
}
 ?>