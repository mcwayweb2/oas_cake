<?php
class CombosPizzasController extends AppController {

	var $name = 'CombosPizzas';
	
	
	function add($combo_id = null) {
		if (!$combo_id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Combo Special.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/dashboard' : '/');
		}
		if(!empty($this->data)) $combo_id = $this->data['CombosPizza']['combo_id'];
		$this->checkAdvertiserSession(array('type'=>'combo','value'=>$combo_id));
		
		if(!empty($this->data)) {
			$this->CombosPizza->create();
			if($this->CombosPizza->save($this->data)) {
				//now add any toppings records for specific toppings chosen by advertiser
				$success = true;
				foreach($this->data['topping'] as $topping_id => $value):
					if($value != 'None') {
						$this->CombosPizza->CombosPizzasTopping->create();
						if(!$this->CombosPizza->CombosPizzasTopping->save(array('CombosPizzasTopping' => array('combos_pizza_id' => $this->CombosPizza->getLastInsertId(),
																						  					   'topping_id'      => $topping_id,
																						  					   'placement'       => $value)))) { $success = false; }
					}
				endforeach;
				
				if($success) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('The pizza has been successfully added to your combo.', true));	
				} else {
					$this->Session->setFlash(__('There was a problem adding the pizza to your combo. Please try again.', true));
					//make sure to delete the impartial record
					$this->CombosPizza->delete($this->CombosPizza->getLastInsertId());
				}
			} else {
				$this->Session->setFlash(__('There was a problem adding the pizza to your combo. Please try again.', true));
			}
			
			
			$this->redirect('/combos/edit/'.$this->data['CombosPizza']['combo_id']);
		}
		
		$c = $this->CombosPizza->Combo->read('location_id', $combo_id);
		$this->set('sizes', $this->CombosPizza->Combo->Location->getSizeList($c['Combo']['location_id']));
		$this->set('toppings', $this->CombosPizza->Combo->Location->getToppings($c['Combo']['location_id']));
		$this->set('combo_id', $combo_id);
	}
	
	function delete($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('The page you are trying to reach does not exist.', true));
		} else {
			$c = $this->CombosPizza->read(array('combo_id'), $id);
			$this->checkAdvertiserSession(array('type'=>'combo','value'=>$c['CombosPizza']['combo_id']));

			//just a single record to delete
			if($this->CombosPizza->delete($id)) {
				if($this->CombosPizza->CombosPizzasTopping->deleteAll(array('CombosPizzasTopping.combos_pizza_id' => $id), false)) {
					$this->Session->write('show_warning', '1');
					$this->Session->setFlash(__('Pizza successfully removed from your combo.', true));
				} else {
					$this->Session->setFlash(__('There was a problem removing the pizza from your combo. Please try again.', true));
				}	
			} else {
				$this->Session->setFlash(__('There was a problem removing the pizza from your combo. Please try again.', true));	
			}	
		}
		$this->redirect($this->referer());
	}
}