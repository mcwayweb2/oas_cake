<?php
class DiscountCodesController extends AppController {

	var $name = 'DiscountCodes';
	
	function ajax_check_code() {
		Configure::write('debug', 0);
		$code = $this->params['form']['code'];
		$c = $this->DiscountCode->findByCode($code);
		$color = 'red';
		
		//check against regular expression
		if(strlen($code) < 6 || strlen($code) > 12) {
			$msg = 'Code must be 6-12 characters in length.';
		} else if(!ctype_alnum($code)) {
			$msg = 'Invalid. Code must be alphanumeric.';
		} else if($c) {
			$msg = 'This code is already in use. Please choose another one.';
		} else {
			$msg = 'This promotion code is available.';
			$color = 'green';
		}
		
		$this->set('msg', $msg);
		$this->set('color', $color);
	}
	
	function check_code() {
		$this->redirect($this->referer());
	}
	
	function ajax_validate_code() {
		if($this->RequestHandler->isAjax()) {
			$this->checkUserSession();
			
			//get advertiser id for location, and make sure location has a premium subscription
			$loc = $this->DiscountCode->Advertiser->Location->read(array('advertiser_id', 'acct_type'), $this->Session->read('User.location_id'));
			$code = $this->DiscountCode->find('first', array('conditions' => array('DiscountCode.code'          => $this->params['form']['code'],
																				   'DiscountCode.advertiser_id' => $loc['Location']['advertiser_id']),
															 'recursive'  => -1));
			//if the code exists and they have a paid account
			if($code && $loc['Location']['acct_type'] != '1') {
				//code valid, now make sure the location is allowed to use it
				$check = $this->DiscountCode->LocationsDiscountCode->findByDiscountCodeId($code['DiscountCode']['id']);
				$order = $this->DiscountCode->Order->read('subtotal', $this->Session->read('User.order_id'));
				
				$valid = true;
				if(!$check) {
					//location not allowed to use it
					$msg = 'Invalid Promotion Code.';
					$valid = false;
				} else if($code['DiscountCode']['use_limit'] > 0) {
					//has use limit been reached
					$count = $this->DiscountCode->Order->find('count', array('conditions' => array('Order.discount_code_id' => $code['DiscountCode']['id'])));
					if($count >= $code['DiscountCode']['use_limit']) {
						$valid = false;
						$msg = 'Promotion code use limit reached.';
					}
				} else if(strtotime($code['DiscountCode']['exp']) < strtotime("now") && $code['DiscountCode']['exp'] != '0000-00-00') {
					//is the promotion code expired
					$valid = false;
					$msg = 'This promotion code is expired.';
				} else if($code['DiscountCode']['min_order_amt'] > 0 && $code['DiscountCode']['min_order_amt'] > $order['Order']['subtotal']) {
					//make sure the minimum order amt for the code has been reached.
					$valid = false;
					$msg = 'You must spend $'.number_format($code['DiscountCode']['min_order_amt'], 2).' to use this coupon.';
				} 
				
				if($valid) {
					//code is still valid...apply the discount
					$discount = ($code['DiscountCode']['discount_type'] == 'Percent') ? 
								$order['Order']['subtotal'] * $code['DiscountCode']['discount_amt'] : $code['DiscountCode']['discount_amt'];
								
					$this->DiscountCode->Order->id = $this->Session->read('User.order_id');
					if($this->DiscountCode->Order->save(array('Order' => array('discount_code_id' => $code['DiscountCode']['id'],
																			   'discount_amt'     => $discount)))) {
						$msg = 'Discount of $'.number_format($discount, 2).' applied.';
						//$this->Session->setFlash(__($msg, true));
						//$this->redirect('/orders/review');	
					} else {
						$valid = false;
						$msg = 'Discount code not valid.';
					}			 	
				}
			} else {
				$valid = false;
				$msg = 'Invalid promotion code.';
			}
			$this->set('valid', $valid);
			$this->set('msg', $msg);
		} 
	}
	
	//list existing codes, and form to add new code
	function manage() {
		//Configure::write('debug', '1');
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		$show_form = false;
		
		//add a new code...
		if(!empty($this->data)) {
			$this->DiscountCode->set($this->data);
			if($this->DiscountCode->validates()) {
				$this->DiscountCode->create();
				$this->data['DiscountCode']['advertiser_id'] = $this->Session->read('Advertiser.id');
				if(empty($this->data['DiscountCode']['use_limit'])) unset($this->data['DiscountCode']['use_limit']);
				if(empty($this->data['DiscountCode']['min_order_amt'])) unset($this->data['DiscountCode']['min_order_amt']);
				
				//convert the discount amt to decimal if a percentage type
				$this->data['DiscountCode']['discount_amt'] = ($this->data['DiscountCode']['discount_type'] == 'Percent') ? 
															   $this->data['DiscountCode']['discount_amt_unconverted'] / 100 : 
															   $this->data['DiscountCode']['discount_amt_unconverted']; 
				
				if($this->DiscountCode->save($this->data)) {
					//code saved successfully...now add the records for the individual locations
					$success = true;
					foreach($this->data['locations'] as $id => $value):
					if($value == 1) {
						//add a habtm record for this location
						$this->DiscountCode->LocationsDiscountCode->create();
						if(!$this->DiscountCode->LocationsDiscountCode->save(array('LocationsDiscountCode' => array('location_id'      => $id,
																													'discount_code_id' => $this->DiscountCode->getLastInsertId())))) {
							$success = false;
						}
					}	
					endforeach;
					
					if($success) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your new promotion code has been added successfully.', true));
					} else {
						$this->Session->setFlash(__('There was a problem adding your promotion code. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields, and try again.', true));
					$show_form = true;
				}
			} else {
				$show_form = true;
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
		}
		$this->set('show_form', $show_form);
		
		$this->set('states', $this->DiscountCode->Location->State->find('list', array('fields' => array('State.name'))));
		$this->paginate = array('recursive' => -1, 'order' => 'code', 'limit' => 5);
		$codes = $this->paginate('DiscountCode', array('DiscountCode.advertiser_id' => $this->Session->read('Advertiser.id')));
		foreach($codes as $index => $c):
			$codes[$index]['DiscountCode']['times_used'] = $this->DiscountCode->timesUsed($c['DiscountCode']['id']);
		endforeach;
		$this->set('discount_codes', $codes);
		
		$fields = array('Location.id', 'Location.name', 'Location.accept_discount_codes', 'Location.menu_ready', 'Location.active', 'Location.acct_type');
		$this->set('locations', $this->DiscountCode->Location->allLocationsByAdvertiser($this->Session->read('Advertiser.id'), null, '-1', $fields));
	}
	
	function edit($id = null) {
		$this->checkAdvertiserSession();
		$this->layout = 'restaurant';
		
		if(!empty($this->data)) {
			$this->DiscountCode->set($this->data);
			if($this->DiscountCode->validates()) {
				if(empty($this->data['DiscountCode']['use_limit'])) unset($this->data['DiscountCode']['use_limit']);
				if(empty($this->data['DiscountCode']['min_order_amt'])) unset($this->data['DiscountCode']['min_order_amt']);
				
				//convert the discount amt to decimal if a percentage type
				$this->data['DiscountCode']['discount_amt'] = ($this->data['DiscountCode']['discount_type'] == 'Percent') ? 
															   $this->data['DiscountCode']['discount_amt_unconverted'] / 100 : 
															   $this->data['DiscountCode']['discount_amt_unconverted']; 
				
				if($this->DiscountCode->save($this->data)) {
					//remove any existing Location records for this code
					$this->DiscountCode->LocationsDiscountCode->deleteAll(array('LocationsDiscountCode.discount_code_id' => $this->data['DiscountCode']['id']), false);
					
					//code saved successfully...now add the records for the individual locations
					$success = true;
					foreach($this->data['locations'] as $id => $value):
					if($value == 1) {
						//add a habtm record for this location
						$this->DiscountCode->LocationsDiscountCode->create();
						if(!$this->DiscountCode->LocationsDiscountCode->save(array('LocationsDiscountCode' => array('location_id'      => $id,
																													'discount_code_id' => $this->data['DiscountCode']['id'])))) {
							$success = false;
						}
					}	
					endforeach;
					
					if($success) {
						$this->Session->write('show_warning', '1');
						$this->Session->setFlash(__('Your promotion code has been updated successfully.', true));
						$this->redirect('/discount_codes/manage');
					} else {
						$this->Session->setFlash(__('There was a problem updating your promotion code. Please try again.', true));
					}
				} else {
					$this->Session->setFlash(__('Please fill out all the required fields, and try again.', true));
				}
			} else {
				$this->Session->setFlash(__('Please fill out all the required fields.', true));
			}
		}
		if(empty($this->data)) {
			$this->data  = $this->DiscountCode->read(null, $id);
			//convert the discount amount to a whole number if the discount type is a Percentage
			if($this->data['DiscountCode']['discount_type'] == 'Percent') { 
				$this->data['DiscountCode']['discount_amt_unconverted'] = $this->data['DiscountCode']['discount_amt'] * 100;
			} else { $this->data['DiscountCode']['discount_amt_unconverted'] = $this->data['DiscountCode']['discount_amt']; }
		}
		$fields = array('Location.id', 'Location.name', 'Location.accept_discount_codes', 'Location.menu_ready', 'Location.active', 'Location.acct_type');
		$this->set('locations', $this->DiscountCode->Location->allLocationsByAdvertiser($this->Session->read('Advertiser.id'), null, '-1', $fields));
		//get list of locations that are currently using this code
		$this->set('current_locs', $this->DiscountCode->getLocationsUsingCode($this->data['DiscountCode']['id']));
	}
	
	function verifyIdentity($id) {
		$c = $this->DiscountCode->read('advertiser_id', $id);
		return ($c['DiscountCode']['advertiser_id'] == $this->Session->read('Advertiser.id')) ? true : false;
	}
	
	function disable($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid promotion code.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/promotion_codes' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'discount_code', 'value'=>$id));
		
		$c = $this->DiscountCode->read('active', $id);
		$this->DiscountCode->id = $id;
		if($this->DiscountCode->saveField('active', ($c['DiscountCode']['active'] == 1) ? 0 : 1)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Promotion Code successfully updated.', true));
		} else {
			$this->Session->setFlash(__('Invalid promotion code.', true));
		}
		$this->redirect($this->referer());
	}
	
	function archive($id = null) {
		if(!$id) {
			$this->Session->setFlash(__('Invalid promotion code.', true));
			$this->redirect(($this->Session->check('Advertiser')) ? '/advertisers/promotion_codes' : '/');
		}
		$this->checkAdvertiserSession(array('type'=>'discount_code', 'value'=>$id));
		
		if($this->DiscountCode->delete($id)) {
			$this->Session->write('show_warning', '1');
			$this->Session->setFlash(__('Promotion Code successfully deleted.', true));
		} else {
			$this->Session->setFlash(__('Invalid promotion code.', true));
		}
		$this->redirect($this->referer());
	}
}
?>