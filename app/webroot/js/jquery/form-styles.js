 // jQuery Automatic Form Styles.
// 

$.widget("ui.form",{
	 _init:function() {
		 var object = this;
		 var form = this.element;
		 var inputs = form.find("input, select, textarea");
		 
		  form.find("fieldset").addClass("ui-widget-content ui-corner-all");
		  form.find("legend").addClass("ui-corner-all");
		  form.addClass("ui-widget");
		
		  $.each(inputs,function() {
			  $(this).addClass('ui-state-default ui-corner-all');
			  $(this).wrap("<label />");
			
			  if($(this).is("input[type='text']")||$(this).is("textarea")||$(this).is("input[type='password']")||
				 $(this).is("button")||$(this).is("input[type='button']")||$(this).is(":reset, :submit"))
				  object.textelements(this);
			
			  if($(this).hasClass("date")) {
				  $(this).datepicker();
			  }
		  }); 
	 },
	 textelements:function(element) {
		 if($(element).is("button")||$(element).is("input[type='button']")||$(element).is(":reset, :submit")) {
			 $(element).addClass("ui-button");
		 }
		 
		 $(element).bind( {
			 focusin: function() {
			 	$(this).toggleClass('ui-state-focus');
			 },
			 focusout: function() {
				$(this).toggleClass('ui-state-focus');
			 }
		 });
		 $(element).hover(function() {
			  $(this).addClass("ui-state-hover"); 
		  }, function() { 
			  $(this).removeClass("ui-state-hover");  
		  });
	 }
});

