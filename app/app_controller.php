<?php
class AppController extends Controller {
	var $helpers = array('Html', 'Form', 'Time', 'Misc', 'Number', 'Jquery', 'Session');
	var $components = array('RequestHandler', 'Strings', 'Session');
	
	function forceSSL() {
       $this->redirect('https://' . $_SERVER['SERVER_NAME'] . $this->here);
    }
	
	function beforeRender() {
		if($this->Session->check('Advertiser')) {
			//get all of the advertisers current locations...
			$ad_locs = $this->requestAction('/locations/getAdvertiserLocations');
			$this->set('ad_loc_list', array_pop($ad_locs));
			$this->set('ad_locations', $ad_locs);
			
			//get payment profile count
			//$this->set('payment_profile_count', $this->requestAction('/advertisers_payment_profiles/profileCount'));
		} 
		if($this->Session->check('User')) {
			$this->set('my_favs', $this->requestAction('/users/getMyFavs'));	
		}
		
		if($this->Session->check('show_warning')) {
			if($this->Session->read('show_warning') == '1') {
				//warning has been set, but not displayed
				$this->Session->write('show_warning', '0');
			} else if($this->Session->read('show_warning') == '0' || $this->Session->read('show_warning') == '') {
				//warning has been displayed, unset the warning.
				$this->Session->delete('show_warning');
			}
		}
		
		//set food categories
		$this->set('food_cats', $this->requestAction('/food_cats/list_cats'));
			
		$valid_pages = array('/', 'about-us', 'learn-more');
		if(isset($this->params['url']['url']) && in_array($this->params['url']['url'], $valid_pages)) {	
			$this->set('featured_restaurants', $this->requestAction('locations/getFeatured'));	
		}
	}
	
	static function getDayAbbr() {
		switch((date('l', time() + ($timezone_offset = -2) * 60 * 60))) {
			case 'Monday': return 'Mon';
			case 'Tuesday': return 'Tues';
			case 'Wednesday': return 'Wed';
			case 'Thursday': return 'Thur';
			case 'Friday': return 'Fri';
			case 'Saturday': return 'Sat';
			case 'Sunday': return 'Sun';
		}
	}
	
	static function is_site($check = 'local') {
		switch ($check) {
			//case 'live': return ($_SERVER['HTTP_HOST'] == '') ? true : false;
			//case 'demo': return ($_SERVER['HTTP_HOST'] == '') ? true : false;
	
			case 'local':
			default: return ($_SERVER['HTTP_HOST'] == 'orderaslice.localhost') ? true : false;
		}
	}
	
	static function timeToMilitary($time = null) {
		return (!$time) ? false : date("H:i:s", strtotime($time));
	}
	
	static function timeFromMilitary($time = null) {
		return (!$time) ? false : date("g:i a", strtotime($time));
	}
	
	//calculate the difference between server time and local time offset
	static function getLocalTimeOffset() {
		//for now just manually set back two hours for PST timezone
		return strtotime("-2 hours");
	}
	
	static function formatPhone($phone) {
		return '('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone, 6);
	}
	
	static function stripPhoneFormat($phone) {
		return str_replace(array("(", ")", " ", "-"), array("", "", "", ""), $phone);
		
	}
	
	//checks if passed number is a valid phone number
	static function checkPhoneFormat($phone) {
		return ((!$phone || !is_numeric($phone) || strlen($phone) > 10 || strlen($phone) < 10)) ? false : true; 
	}
	
    function checkAdminSession() {
        if (!$this->Session->check('Admin')) {
            // Force the user to login
            $this->redirect('/admin/users/login');
            exit();
        } else {
        	$this->layout = 'admin';
        }
    }
    
    function checkAdvertiserSession($check_params = null) {
        // If the session info hasn't been set...
        if (!$this->Session->check('Advertiser')) {
            // Force the user to login
            $this->Session->setFlash(__('You are not authorized to access this page without logging in. Please log in.', true));
            $this->redirect('/advertisers/login');
            exit();
        } else {
        	//do we need to verify that the page belongs to advertiser?
        	if($check_params) {
        		switch ($check_params['type']) {
        			case "location":
        				//verify identity by seeing if passed location_id belongs to advertiser
        				if(!$this->requestAction('/locations/verifyIdentity/'.$check_params['value'])) {
        					//location doesn't belong to advertiser
        					$this->Session->setFlash(__('This location does not belong to you.', true));
            				$this->redirect('/advertisers/dashboard');
           					 exit();
        				} 
        				break;
        				
        			case "special":
        				//verify identity by seeing if passed special_id belongs to advertiser
        				if(!$this->requestAction('/specials/verifyIdentity/'.$check_params['value'])) {
        					//special doesn't belong to advertiser
        					$this->Session->setFlash(__('This specialty pizza does not belong to your company.', true));
            				$this->redirect('/advertisers/dashboard');
           					 exit();
        				} 
        				break;
        				
        			case "combo":
        				//verify identity by seeing if passed special_id belongs to advertiser
        				if(!$this->requestAction('/combos/verifyIdentity/'.$check_params['value'])) {
        					//special doesn't belong to advertiser
        					$this->Session->setFlash(__('This Combo Special does not belong to your company: '.$check_params['value'], true));
            				$this->redirect('/advertisers/dashboard');
           					 exit();
        				} 
        				break;
        				
        			case "discount_code":
        				//verify identity by seeing if passed menuid belongs to advertiser
        				if(!$this->requestAction('/discount_codes/verifyIdentity/'.$check_params['value'])) {
        					//code doesn't belong to advertiser
        					$this->Session->setFlash(__('This code does not belong to you.', true));
            				$this->redirect('/advertisers/promotion_codes');
           					 exit();
        				} 
        				break;
        				
        			case "pay_profile":
        				//verify identity by seeing if passed payment profile id belongs to advertiser
        				if(!$this->requestAction('/advertisers_payment_profiles/verifyIdentity/'.$check_params['value'])) {
        					//payment profile doesn't belong to user
        					$this->Session->setFlash(__('That payment profile does not belong to you.', true));
            				$this->redirect('/advertisers/payment_profiles');
           					 exit();
        				} 
        				break;
        				
        			case "order_id":
        				//verify identity by seeing if passed order id belongs to advertiser
        				if(!$this->requestAction('/orders/verifyAdvertiserIdentity/'.$check_params['value'])) {
        					$this->Session->setFlash(__('This order does not belong to your restaurant.', true));
            				$this->redirect('/advertisers/dashboard');
           					 exit();
        				}
        				break;
        		}
        	}
        }
    }
    
    function checkUserSession($params = null) {
        // If the session info hasn't been set...
        if (!$this->Session->check('User')) {
            // Force the user to login
            $this->Session->setFlash(__('You are not authorized to access this page without logging in. Please log in.', true));
            $this->redirect('/users/login');
            exit();
        } else if($params) {
        	if(array_key_exists('order_item', $params)) {
        		//verify that the passed item id belongs to the logged in user
        		if(!$this->requestAction('/orders_items/verifyIdentity/'.$params['order_item'])) {
        			//order item does not belong to the current controller
        			$this->Session->setFlash(__('Invalid item. This item is not part of your current order.', true));
        			$this->redirect(($this->Session->check('User.location_id')) 
        							? '/menu/'.$this->requestAction('/locations/getUrlSlugs/'.$this->Session->read('User.location_id'))
            						: '/users/dashboard');
           			 exit();
        		}
        	} else if(array_key_exists('order_special', $params)) {
        		//verify that the passed special id belongs to the logged in user
        		if(!$this->requestAction('/orders_specials_sizes/verifyIdentity/'.$params['order_special'])) {
        			//order item does not belong to the current controller
        			$this->Session->setFlash(__('Invalid specialty pizza. This pizza is not part of your current order.', true));
        			$this->redirect(($this->Session->check('User.location_id')) 
        							? '/menu/'.$this->requestAction('/locations/getUrlSlugs/'.$this->Session->read('User.location_id'))
            						: '/users/dashboard');
           			 exit();
        		}
        	} else if(array_key_exists('order_pizza', $params)) {
        		//verify that the passed item id belongs to the logged in user
        		if(!$this->requestAction('/pizzas/verifyIdentity/'.$params['order_pizza'])) {
        			//order item does not belong to the current controller
        			$this->Session->setFlash(__('Invalid pizza. This pizza is not part of your current order.', true));
        			$this->redirect(($this->Session->check('User.location_id')) 
        							? '/menu/'.$this->requestAction('/locations/getUrlSlugs/'.$this->Session->read('User.location_id')) 
            						: '/users/dashboard');
           			 exit();
        		}
        	} else if(array_key_exists('order_id', $params)) {
        		//verify that the passed order id belongs to the logged in user
        		if(!$this->requestAction('/orders/verifyUserIdentity/'.$params['order_id'])) {
        			//order item does not belong to the current controller
        			$this->Session->setFlash(__('This order does not belong to you.', true));
        			$this->redirect('/users/dashboard');
           			exit();
        		}
        	} else if(array_key_exists('pay_profile_id', $params)) {
        		//verify that the passed payment profile id belongs to the logged in user
        		if(!$this->requestAction('/users_payment_profiles/verifyIdentity/'.$params['pay_profile_id'])) {
        			//pay profile does not belong to the current controller
        			$this->Session->setFlash(__('This payment profile does not belong to you.', true));
        			$this->redirect('/users/dashboard');
           			exit();
        		}
        	}
        }
    }
}
?>